/* jslint browser:true*/
/* global jQuery*/
/* global CKEDITOR*/
(function($) {
  'use strict';

  const prevAccount = fillAccount();
  const prevContact = fillContact();
  const prevCompany = fillCompany();
  const prevDevice = fillDevice();
  const prevService = fillService();
  const prevUpdateProposal = fillUpdateProposal();
  const prevAddProposal = fillAddProposal();
  const prevAddWorkorder = fillAddWorkorder();
  const prevEditingProposal = fillEditingProposal();
  const prevEditingInspectionInfo = fillEditingInspectionInfo();
  const prevDeviceDynamic = viewDynamic();
  const prevImage = checkImgStatus();
  let dynamicFlag = true;
  let changeFlag = true;
  let imgStatusFlag = true;

  function trueNumber(str) {
    if (str) {
      return str.replace(/[()-/ /]/g, '');
    }
  }

  function truePrice(str) {
    if (str) {
      return str.replace(/[$]/g, '');
    }
  }

  // Return filling account object
  function fillAccount() {
    return {
      name: $('input[name="account[name]"]').val(),
      clientType: $('#account_clientType option:selected').text(),
      address: $('input[name="account[address][address]"]').val(),
      city: $('input[name="account[address][city]"]').val(),
      state: $('#account_address_state option:selected').text(),
      zip: $('input[name="account[address][zip]"]').val(),
      // billingAddress: $('input[name="account[billingAddress][address]"]').val(),
      // billingCity: $('input[name="account[billingAddress][city]"]').val(),
      // billingState: $('#account_billingAddress_state option:selected').text(),
      // billingZip: $('input[name="account[billingAddress][zip]"]').val(),
      municipality: $('input[name="account[municipalityAutocomplete]"]').val(),
      website: $('input[name="account[website]"]').val(),
      specialDiscount: $('input[name="account[specialDiscount]"]').is(':checked'),
      notes: $('textarea[name="account[notes]"]').val(),
      payments: $('#account_paymentTerm option:selected').text(),
      buildingType: $('#account_buildingType option:selected').text()
    };
  }

  // Return filling company object
  function fillCompany() {
    return {
      name: $('input[name="company_form[company][name]"]').val(),
      address: $('input[name="company_form[company][address][address]"]').val(),
      city: $('input[name="company_form[company][address][city]"]').val(),
      state: $('#company_form_company_address_state option:selected').text(),
      zip: $('input[name="company_form[company][address][zip]"]').val(),
      website: $('input[name="company_form[company][website]"]').val(),
      notes: $('textarea[name="company_form[company][notes]"]').val(),
      billAddressAddress: $('textarea[name="company_form[billingAddress][address]"]').val(),
      billAddressCity: $('input[name="company_form[billingAddress][city]"]').val(),
      billAddressState: $('#company_form_billingAddress_state option:selected').text(),
      billAddressZip: $('input[name="company_form[billingAddress][zip]"]').val()
    };
  }

  // Return filling contact object
  function fillContact() {
    return {
      name: $('input[name="contact_person_form[contactPerson][firstName]"]').val(),
      lastName: $('input[name="contact_person_form[contactPerson][lastName]"]').val(),
      title: $('input[name="contact_person_form[contactPerson][title]"]').val(),
      email: $('input[name="contact_person_form[contactPerson][email]"]').val(),
      phone: trueNumber($('input[name="contact_person_form[contactPerson][phone]"]').val()),
      ext: $('input[name="contact_person_form[contactPerson][ext]"]').val(),
      cell: trueNumber($('input[name="contact_person_form[contactPerson][cell]"]').val()),
      fax: trueNumber($('input[name="contact_person_form[contactPerson][fax]"]').val()),
      cod: $('input[name="contact_person_form[contactPerson][cod]"]').is(':checked'),
      notes: $('textarea[name="contact_person_form[contactPerson][notes]"]').val(),
      address: $('input[name="contact_person_form[contactPerson][personalAddress][address]"]').val(),
      city: $('input[name="contact_person_form[contactPerson][personalAddress][city]"]').val(),
      state: $('#contact_person_form_billingAddress_state option:selected').text(),
      zip: $('input[name="contact_person_form[contactPerson][personalAddress][zip]"]').val(),
      billAddressAddress: $('textarea[name="contact_person_form[billingAddress][address]"]').val(),
      billAddressCity: $('input[name="contact_person_form[billingAddress][city]"]').val(),
      billAddressState: $('#contact_person_form_billingAddress_state option:selected').text(),
      billAddressZip: $('input[name="contact_person_form[billingAddress][zip]"]').val()
    };
  }

  // Return filling device object
  function fillDevice() {
    return {
      location: $('input[name="form_device[device][location]"]').val(),
      status: $('select[name="form_device[device][status]"]').val(),
      comments: $('textarea[name="form_device[device][comments]"]').val(),
      note: $('textarea[name="form_device[device][noteToTester]"]').val()
    };
  }

  // Return filling service object
  function fillService() {
    return {
      serviceName: $('select[name="admin_bundle_service[named]"]').val(),
      fixedFee: truePrice($('input[name="admin_bundle_service[fixedPrice]"]').val()),
      lastDate: $('input[name="admin_bundle_service[lastTested]"]').val(),
      dueDate: $('input[name="admin_bundle_service[inspectionDue]"]').val(),
      lastCompany: $('select[name="admin_bundle_service[companyLastTested]"]').val(),
      comment: $('textarea[name="admin_bundle_service[comment]"]').val(),
      device: $('input[name="admin_bundle_service[device]"]:checked').val()
    };
  }

  // Return filling update proposal object
  function fillUpdateProposal() {
    return {
      comments: $('textarea[name="repair_proposal[deviceInfo][comments]"]').val(),
      specialNotification: $('select[name="repair_proposal[deviceInfo][specialNotification]"]').val(),
      estimatedTime: $('input[name="repair_proposal[deviceInfo][estimationTime]"]').val(),
      price: truePrice($('input[name="repair_proposal[deviceInfo][price]"]').val()),
      checkboxes: $('input[name="repair_proposal[proposal][services][]"]:checked').length,
      additionalRepairs: $('#additional_repairs tbody tr td[data-bind="text: name"]').text()
    };
  }

  // Return filling add proposal object
  function fillAddProposal() {
    return {
      checkboxes: $('input[name="repair_proposal[proposal][services][]"]:checked').length,
      additionalRepairs: $('#additional_repairs tbody tr td[data-bind="text: name"]').text()
    };
  }

  // Return filling add workorder object
  function fillAddWorkorder() {
    return {
      checkboxes: $('input[name="repair_workorder[workorder][services][]"]:checked').length,
      additionalRepairs: $('#additional_repairs tbody tr td[data-bind="text: name"]').text()
    };
  }

  // Return filling editing general proposal information object
  function fillEditingProposal() {
    return {
      checkboxes: $('input[name="proposal_general_info[notIncludedItems][]"]:checked').length,
      comments: $('textarea[name="proposal_general_info[additionalComment]"]').val(),
      commentsInter: $('textarea[name="proposal_general_info[internalComments]"]').val(),
      afterHoursWorkCost: truePrice($('input[name="proposal_general_info[afterHoursWorkCost]"]').val()),
      discount: truePrice($('input[name="proposal_general_info[discount]"]').val()),
      additionalCost: $('textarea[name="proposal_general_info[additionalCostComment]"]').val()
    };
  }

  // Return filling editing general proposal information object
  function fillEditingInspectionInfo() {
    return {
      fixedPrice: $('input[name="inspections_information[fixedPrice]"]').val(),
      inspectionDue: $('input[name="inspections_information[inspectionDue]"]').val(),
      lastTested: $('input[name="inspections_information[lastTested]"]').val(),
      companyLastTested: $('select[name="inspections_information[companyLastTested]"]').val()
    };
  }

  $('#imgDelButton').on('click', () => {
    imgStatusFlag = false;
  });

  // Return true if img downloaded
  function checkImgStatus() {
    return $('#imgContent').children().length > 0;
  }

  // Get all dynamics fields values in array
  function getValueElements(selects, inputs) {
    const oldSelectsArr = $(selects);
    const oldInputsArr = $(inputs);
    const mergeArr = $.merge(oldSelectsArr, oldInputsArr);
    const resultArr = [];
    mergeArr.each(function(index, value) {
      if (value.innerText) {
        resultArr.push(value.innerText);
      } else {
        resultArr.push(value.value);
      }
    });
    return resultArr;
  }

  function viewDynamic() {
    return getValueElements('select.dynamic-form-element option:selected', 'input.dynamic-form-element');
  }

  // Compare init and result objects. Return false if not equal
  function compareObjects(obj1, obj2) {
    Object.keys(obj1).forEach(function(key) {
      if (obj1[key] !== obj2[key]) {
        changeFlag = false;
        return changeFlag;
      }
    });
  }

  // Compare init and result arrays of dynamic fields value
  function compareDynamic(prev, current) {
    dynamicFlag = prev.toString() === current.toString();
    return dynamicFlag;
  }

  // Compare init and results img flag
  function compareImage(prev, result) {
    return prev === result;
  }

  // Show warning modal if init and result objects equal
  function updateHandler(event, obj1, obj2) {
    compareObjects(obj1, obj2);
    if (changeFlag) {
      event.preventDefault();
      $('#noUpdateModal').modal('show');
    }
    changeFlag = true;
  }

  $('#account_update').click(event => {
    const resultAccount = fillAccount();
    updateHandler(event, prevAccount, resultAccount);
  });

  $('#contact_update').click(event => {
    const resultContact = fillContact();
    updateHandler(event, prevContact, resultContact);
  });

  $('#company_update').click(event => {
    const resultCompany = fillCompany();
    updateHandler(event, prevCompany, resultCompany);
  });

  $('#device_update').click(event => {
    const resultDevice = fillDevice();
    const resultDynamic = viewDynamic();
    const resultImage = checkImgStatus();
    compareObjects(prevDevice, resultDevice);
    compareDynamic(prevDeviceDynamic, resultDynamic);
    const imgCheck = compareImage(prevImage, resultImage);
    imgStatusFlag = (imgStatusFlag || (!prevImage && !resultImage));
    if (changeFlag && dynamicFlag && imgCheck && imgStatusFlag) {
      event.preventDefault();
      $('#noUpdateModal').modal('show');
    }
    dynamicFlag = true;
    changeFlag = true;
  });

  $('#service_update').click(event => {
    const resultService = fillService();
    updateHandler(event, prevService, resultService);
  });

  $('#adding_to_proposal_update').click(event => {
    const currentTextarea = $('#email_editor');
    const currentData = CKEDITOR.instances.email_editor.getData().trim();
    currentTextarea.val(currentData);
    const resultProposal = fillUpdateProposal();
    updateHandler(event, prevUpdateProposal, resultProposal);
  });

  $('#adding_to_proposal_add').click(event => {
    const resultProposal = fillAddProposal();
    updateHandler(event, prevAddProposal, resultProposal);
  });

  $('#adding_to_workorder_add').click(event => {
    const resultWorkorder = fillAddWorkorder();
    updateHandler(event, prevAddWorkorder, resultWorkorder);
  });

  $('#editing_general_proposal_information').click(event => {
    const resultProposal = fillEditingProposal();
    updateHandler(event, prevEditingProposal, resultProposal);
  });

  $('#change_inspection_information').click(event => {
    const resultEditingInspectionInfo = fillEditingInspectionInfo();
    updateHandler(event, prevEditingInspectionInfo, resultEditingInspectionInfo);
  });
})(jQuery);

