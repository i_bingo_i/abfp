/* jslint browser:true*/
/* global jQuery*/
/* global InstallTrigger */
/* global document */

(function($) {
  'use strict';

  const removeFile = $('.removeFile');
  const browseFile = $('.browseFile');

  removeFile.hide();

  $('.inputFile').on('change', e => {
    const thisInput = e.currentTarget;
    const $thisInput = $(thisInput);
    const ext = $thisInput.val().split('.').pop().toLowerCase();
    const error = $thisInput.parent().siblings('.fileError');
    const fileType = $thisInput.attr('data-type');
    if (thisInput.files.length !== 0 || thisInput.val() !== '') {
      error.removeClass('app-submit-error app-device-form__img-error').text('');
    }
    if (thisInput.files[0].size > 10000000) {
      $thisInput.val('');
      error.addClass('app-submit-error app-device-form__img-error').text('The file is too large. Allowed maximum size is 10Mb');
    } else if (validateTypeFile(ext, fileType)) {
      $thisInput.val('');
      error.addClass('app-submit-error app-device-form__img-error').text(`Please use ${fileType}`);
    } else {
      $thisInput.siblings('.browseFile').hide();
      $thisInput.siblings('.dragandropPlaceholder').hide();
      $thisInput.siblings('.fileName').text($thisInput[0].files[0].name);
      $thisInput.siblings('.removeFile').show();
    }
  });

  removeFile.click(e => {
    const target = $(e.currentTarget);
    e.stopPropagation();
    e.preventDefault();
    clearContent(target.siblings('.inputFile'), target.siblings('.fileName'), target, target.siblings('.dragandropPlaceholder'), target.siblings('.browseFile'));
  });

  browseFile.on('click', e => {
    e.stopPropagation();
    e.preventDefault();
    $(e.currentTarget).siblings('.inputFile').trigger('click');
  });

  $('.resetInputFile').click(() => {
    clearContent($('.inputFile'), $('.fileName'), removeFile, $('.dragandropPlaceholder'), browseFile);
    $('.fileError').removeClass('app-submit-error app-device-form__img-error').text('');
  });

  function validateTypeFile(ext, type) {
    switch (type) {
    case 'PDF file': {
      return $.inArray(ext, ['pdf']) === -1;
    }
    case 'DOC file': {
      return $.inArray(ext, ['doc', 'docx']) === -1;
    }
    case 'PDF or DOC file': {
      return $.inArray(ext, ['doc', 'docx', 'pdf']) === -1;
    }
    case 'PDF file or Image': {
      return $.inArray(ext, ['jpg', 'jpeg', 'png', 'pdf', '']) === -1;
    }
    default: {
      return false;
    }
    }
  }

  function clearContent(inputFile, fileName, removeFile, dragandropPlaceholder, browseFile) {
    inputFile.val('');
    fileName.text('');
    removeFile.hide();
    dragandropPlaceholder.show();
    browseFile.show();
  }

  $('.validateRequiredFile').submit(e => {
    e.stopPropagation();
    e.preventDefault();
    $(e.target).find('.dragandropContainer').each((index, element) => {
      const dropboxContainer = $(element);
      if (dropboxContainer.find('.inputFile')[0].files.length === 0) {
        const errorBlock = dropboxContainer.find('.fileError');
        errorBlock.addClass('app-submit-error app-device-form__img-error').text('This file is required');
      }
    });
    if ($('.app-submit-error').length === 0) {
      e.target.submit();
    }
    return false;
  });

  $('.validateCommentAndFile').submit(e => {
    e.stopPropagation();
    e.preventDefault();
    $(e.target).find('.dragandropContainer').each((index, element) => {
      const dropboxContainer = $(element);
      if (dropboxContainer.find('.inputFile')[0].files.length === 0) {
        const errorBlock = dropboxContainer.find('.fileError');
        errorBlock.addClass('app-submit-error app-device-form__img-error').text('This file is required');
      }
    });
    if ($('.app-submit-error').length === 0 && $('.validateCommentAndFile').find('textarea.error').length === 0) {
      e.target.submit();
      $(e.target).find('button[type="submit"]').attr('disabled', 'disabled');
    }
    return false;
  });

  function validateOneOfFields(e, field) {
    e.stopPropagation();
    e.preventDefault();
    $(e.target).find('.dragandropContainer').each((index, element) => {
      const dropboxContainer = $(element);
      const errorBlock = dropboxContainer.find('.fileError');
      let flag = field.val() ? !field.hasClass('error') : true;

      if (dropboxContainer.find('.inputFile')[0].files.length === 0 &&
        (!field.val() && !field.hasClass('error'))) {
        errorBlock.addClass('app-submit-error app-device-form__img-error').text('One of fields must be filled');
      } else {
        errorBlock.removeClass('app-submit-error app-device-form__img-error').text('');
      }

      if ($('.app-submit-error').length === 0 && flag) {
        $(e.target).find('button[type="submit"]').attr('disabled', 'disabled');
        e.target.submit();
        e.preventDefault();
      }
    });
    return false;
  }

  $('.validateOneOfFields').submit(e => {
    validateOneOfFields(e, $(e.target).find('textarea'));
  });

  function addEvent(element, eventName, callback) {
    if (element && element.addEventListener) {
      element.addEventListener(eventName, callback, false);
    } else if (element && element.attachEvent) {
      element.attachEvent('on' + eventName, callback);
    }
  }

  const dropboxes = document.getElementsByClassName('app-dragandrop');

  Array.prototype.forEach.call(dropboxes, dropbox => {
    addEvent(dropbox, 'dragenter', dragenter);
    addEvent(dropbox, 'dragover', dragover);
    addEvent(dropbox, 'drop', drop);
  });

  function dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  function dragover(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  function drop(e) {
    e.stopPropagation();
    e.preventDefault();
    const currentInput = $(e.currentTarget).children('.inputFile');
    const dt = e.dataTransfer;
    const files = dt.files;
    if (currentInput[0].files.length === 0 && files.length === 1) {
      currentInput[0].files = files;
      if (typeof InstallTrigger !== 'undefined') {
        currentInput.trigger('change');
      }
      return false;
    }
    return false;
  }
})(jQuery);
