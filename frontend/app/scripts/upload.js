/* jslint browser:true */
/* global jQuery */
/* global loadImage */
/* global InstallTrigger */

(function($) {
  'use strict';

  const imgTrigger = $('.imgTrigger');
  let imgUpload = $('#imgUpload');
  const imgContent = $('#imgContent');
  const imgDelete = $('#imgDelete');
  const browseImg = $('#browseImg');
  const imgPlace = $('#imgPlace');

  // Validation img, if ok - upload with loadImage function for rotate if need
  imgUpload.on('change', e => {
    clearContent();
    const thisInput = e.currentTarget;
    const $thisInput = $(thisInput);
    const ext = $thisInput.val().split('.').pop().toLowerCase();
    const error = $('#uploadError');
    if (thisInput.files[0].size > 10000000) {
      $thisInput.val('');
      error.addClass('app-submit-error app-device-form__img-error').text('The file is too large. Allowed maximum size is 10Mb');
    } else if ($.inArray(ext, ['png', 'jpg', 'jpeg']) === -1) {
      $thisInput.val('');
      error.addClass('app-submit-error app-device-form__img-error').text('Please use JPG, JPEG or PNG images only');
    } else {
      const loadingImage = loadImage(e.target.files[0], function(img) {
        imgContent.append(img);
      },
      {
        maxWidth: 373,
        maxHeight: 263,
        contain: true,
        canvas: true,
        orientation: true
      }
      );
      if (loadingImage) {
        browseImg.hide();
        $('#uploadError').removeClass('app-submit-error app-device-form__img-error').text('');
        imgPlace.removeClass('app-device-form__img-place');
        imgDelete.show();
      }
    }
  });

  // Show popup and delete img
  imgDelete.click(e => {
    e.stopPropagation();
    e.preventDefault();
    const modal = $('#deleteImage');
    modal.modal('show');
    modal.on('shown.bs.modal', function() {
      $('input').blur();
      $('#imgDelButton').click(function() {
        clearContent();
        $('#forceDeleteImage').val('1');
        imgUpload.val('');
        imgUpload.replaceWith(imgUpload = imgUpload.clone(true));
        imgPlace.addClass('app-device-form__img-place');
        imgDelete.hide();
        browseImg.show();
      });
    });
  });

  browseImg.on('click', () => {
    $('input[type="file"]').trigger('click');
  });

  function clearContent() {
    imgContent.removeClass('app-view--max-size');
    imgContent.empty();
  }

  function addEvent(element, eventName, callback) {
    if (element && element.addEventListener) {
      element.addEventListener(eventName, callback, false);
    } else if (element && element.attachEvent) {
      element.attachEvent('on' + eventName, callback);
    }
  }

  const dropbox = imgTrigger[0];
  addEvent(dropbox, 'dragenter', dragenter);
  addEvent(dropbox, 'dragover', dragover);
  addEvent(dropbox, 'drop', drop);

  function dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  function dragover(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  function drop(e) {
    e.stopPropagation();
    e.preventDefault();
    if ($('div.app-device-form__img_background').length > 0) {
      return false;
    }
    const dt = e.dataTransfer;
    const files = dt.files;
    if (imgUpload[0].files.length === 0 && files.length === 1) {
      imgUpload[0].files = files;
      if (typeof InstallTrigger !== 'undefined') {
        $('input[type="file"]').trigger('change');
      }
      return false;
    }
    return false;
  }
})(jQuery);
