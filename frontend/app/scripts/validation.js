/* jslint browser:true*/
/* global jQuery*/
/* global location*/
/* global CKEDITOR*/
(function($) {
  'use strict';

  // Common object with validation settings
  const validationForm = {
    focusCleanup: true,
    focusInvalid: false,
    onfocusout: false, // отключение валидации при потере фокуса с элемента
    onsubmit: true // валидация только по submit
  };

  // Define true place for outputing validation errors (add if form has validation on ckeditor or selectpicker)
  const errorPlaces = {
    errorPlacement: (error, element) => {
      if (element.attr('name') === 'letter[body]' || element.attr('name') === 'repair_proposal[deviceInfo][comments]' || element.attr('name') === 'repair_workorder[deviceInfo][comments]') {
        error.insertAfter('#cke_email_editor');
      } else if (element.attr('name') === 'admin_bundle_file[emailBody]') {
        error.insertAfter('#cke_admin_bundle_file_emailBody');
      } else if (element.hasClass('selectpicker')) {
        error.insertAfter(element.closest('.bootstrap-select'));
      } else {
        error.insertAfter(element);
      }
    }
  };

  // Define where was validation error border (add if form has validation on ckeditor or selectpicker)
  const errorStyles = {
    highlight: (element, errorClass) => {
      highlightStyles($(element), errorClass);
    },
    unhighlight: (element, errorClass) => {
      unhighlightStyles($(element), errorClass);
    }
  };

  function highlightStyles($element, errorClass) {
    $element.addClass(errorClass);
    if ($element.hasClass('selectpicker')) {
      $element.closest('.form-group').addClass('has-error');
    } else if ($element.hasClass('ckeditor')) {
      $('#cke_email_editor').addClass('app-error-ckeditor');
      $('#cke_admin_bundle_file_emailBody').addClass('app-error-ckeditor');
    }
  }

  function unhighlightStyles($element, errorClass) {
    $element.removeClass(errorClass);
    if ($element.hasClass('selectpicker')) {
      $element.closest('.form-group').removeClass('has-error');
    } else if ($element.hasClass('ckeditor')) {
      $('#cke_email_editor').removeClass('app-error-ckeditor');
      $('#cke_admin_bundle_file_emailBody').removeClass('app-error-ckeditor');
    }
  }

  // The shortest rules on required and length
  function ruleRequired(lengthMax, lengthMin) {
    const rule = {
      required: true,
      notOnlySpace: true
    };
    if (lengthMax) {
      rule.maxlength = lengthMax;
    }
    if (lengthMin) {
      rule.minlength = lengthMin;
    }
    return rule;
  }

  function ruleMaxLength(length) {
    return {
      maxlength: length
    };
  }

  // The shortest rules on numbers
  function ruleDigits() {
    return {
      digits: true,
      messages: {
        digits: 'Digits only.'
      }
    };
  }

  // The shortest rules on email
  function ruleEmail(required) {
    const rule = {
      email: true,
      trueEmail: true,
      maxlength: 50
    };
    if (required) {
      rule.required = true;
      rule.notOnlySpace = true;
    }
    return rule;
  }

  // Sabmited form and disabled button
  function sendForm(form, event) {
    $('.app-submit-error').removeClass('app-submit-error').html('');
    $(form).find('button[type="submit"]').attr('disabled', 'disabled');
    form.submit();
    event.preventDefault();
  }

  // Show confirm modal
  function showModal(form, event, selector) {
    const $selector = $(selector);
    $selector.modal('show');
    $selector.on('shown.bs.modal', () => {
      $('input').blur();
      $('a[data-cancel="Yes"]').click(() => {
        event.preventDefault();
      });
      $('a[data-submit="Yes"]').one('click', () => {
        sendForm(form, event);
      });
    });
  }

  // Change text in modal on update form
  function onSubmitHandler(form, event, selector, entity) {
    if ($('button[type="submit"]').attr('data-update') === 'Yes') {
      $(`${selector} .modal-body p`).text(`Are you sure you want to update this ${entity}?`);
    }
    showModal(form, event, selector);
  }

  // Change text in modal on link form
  function onLinkHandler(form, event, selector, entity, linkTo) {
    $('.modal-body p').text(`Are you sure you want to create new ${entity} and link it to the ${linkTo}?`);
    showModal(form, event, selector);
  }

  // Adding validation to account form
  $('#accountForm').validate($.extend({}, {
    rules: {
      'account[name]': ruleRequired(100),
      'account[address][address]': ruleRequired(100),
      'account[address][city]': ruleRequired(50),
      'account[address][zip]': {
        required: true,
        validationAddress: true,
        maxlength: 15
      },
      'account[municipalityAutocomplete]': ruleRequired(),
      'account[notes]': ruleMaxLength(1000),
      'account[address][state]': {required: true},
      'account[paymentTerm]': {required: true},
      'account[website]': ruleMaxLength(100)
    },
    submitHandler: (form, event) => {
      const linkTo = $('button[type="submit"]').attr('data-link');
      if (linkTo) {
        onLinkHandler(form, event, '#accountModal', 'Site Account', linkTo);
      } else {
        onSubmitHandler(form, event, '#accountModal', 'Account');
      }
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to company form
  $('#companyForm').validate($.extend({}, {
    rules: {
      'company_form[company][name]': ruleRequired(100),
      'company_form[company][address][address]': ruleMaxLength(100),
      'company_form[company][address][city]': ruleMaxLength(31),
      'company_form[company][address][zip]': {
        validationAddress: true,
        maxlength: 13
      },
      'company_form[company][website]': ruleMaxLength(100),
      'company_form[company][notes]': ruleMaxLength(255),
      'company_form[billingAddress][address]': {
        limitSymbols: true,
        notOnlySpace: true
      },
      'company_form[billingAddress][city]': ruleMaxLength(31),
      'company_form[billingAddress][zip]': {
        validationAddress: true,
        maxlength: 13
      }
    },
    submitHandler: (form, event) => {
      const linkTo = $('button[type="submit"]').attr('data-link');
      if (linkTo) {
        onLinkHandler(form, event, '#companyModal', 'Company', linkTo);
      } else {
        onSubmitHandler(form, event, '#companyModal', 'Company');
      }
    }
  }, validationForm));

  // Adding validation to search form
  $('#searchForm').validate($.extend({}, {
    rules: {
      searchPhrase: ruleRequired(100)
    },
    messages: {
      searchPhrase: {
        required: 'Please enter your search request.'
      }
    }
  }, validationForm));

  // Adding validation to contact form
  $('#contactForm').validate($.extend({}, {
    rules: {
      'contact_person_form[contactPerson][firstName]': ruleRequired(100),
      'contact_person_form[contactPerson][lastName]': ruleMaxLength(100),
      'contact_person_form[contactPerson][title]': ruleMaxLength(100),
      'contact_person_form[contactPerson][email]': ruleEmail(),
      'contact_person_form[contactPerson][ext]': ruleMaxLength(10),
      'contact_person_form[contactPerson][notes]': ruleMaxLength(255),
      'contact_person_form[contactPerson][personalAddress][address]': ruleMaxLength(100),
      'contact_person_form[contactPerson][personalAddress][city]': ruleMaxLength(31),
      'contact_person_form[contactPerson][personalAddress][zip]': {
        validationAddress: true,
        maxlength: 13
      },
      'contact_person_form[billingAddress][address]': {
        limitSymbols: true,
        notOnlySpace: true
      },
      'contact_person_form[billingAddress][city]': ruleMaxLength(31),
      'contact_person_form[billingAddress][zip]': {
        validationAddress: true,
        maxlength: 13
      }
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#contactModal', 'Contact Person');
    }
  }, validationForm));

  // Adding validation to municipality form
  $('#municipalityCreateForm').validate($.extend({}, {
    rules: {
      'municipality[name]': ruleRequired(255),
      'municipality[address][address]': ruleMaxLength(100),
      'municipality[address][city]': ruleMaxLength(50),
      'municipality[website]': ruleMaxLength(100),
      'municipality[address][zip]': {
        validationAddress: true,
        maxlength: 15
      }
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#municipalityCreateModal', 'Contact Person');
    }
  }, validationForm));

  // Adding addToProposal popup
  $('#repairForm').validate($.extend({}, {
    ignore: [],
    debug: false,
    rules: {
      'repair_proposal[deviceInfo][estimationTime]': {
        required: true,
        maxlength: 5,
        min: 0,
        checkOnlyNull: true,
        floatNumber: true
      },
      'repair_proposal[deviceInfo][comments]': {
        required: () => {
          function htmlSpecialChars(editorcontentCharLess) {
            editorcontentCharLess = editorcontentCharLess.replace(/&amp;/g, '&').replace(/&quot;/g, '"').replace(/&#039;/g, `'`).replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&rsquo;/g, '’').replace(/&lsquo;/g, '‘').replace(/&laquo;/g, '«').replace(/&raquo;/g, '»').replace(/&prime;/g, '′').replace(/&ldquo;/g, '“').replace(/&rdquo;/g, '”');
            return editorcontentCharLess;
          }
          CKEDITOR.instances.email_editor.updateElement();
          let currentTextarea = $('#email_editor');
          let editorcontent = currentTextarea.val().replace(/<[^>]*>/gi, '').replace(/&nbsp;/g, '').trim().replace(/\n/g, ' ').replace(/\s\s+/g, ' ').replace(/(\r\n\t|\n|\r\t)/g, '');
          editorcontent = htmlSpecialChars(editorcontent);
          currentTextarea.val(editorcontent);
          return false;
        },
        maxlength: 1000
      },
      'repair_proposal[deviceInfo][price]': {required: true}
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#repairModal');
    }
  }, errorStyles, errorPlaces, validationForm));

  // New Device Named
  $('#createDeviceNamedForm').validate($.extend({}, {
    rules: {
      'create_device_named[name]': ruleRequired(255),
      'create_device_named[alias]': ruleRequired(255)
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#createDeviceNamedModal');
    }
  }, validationForm));

  // New Service Named
  $('#freequencyServiceNamedForm').validate($.extend({}, {
    rules: {
      'admin_bundle_inspection_named_with_contractor[serviceNamed][name]': ruleRequired(255),
      'admin_bundle_inspection_named_with_contractor[contractorService][estimationTime]': {
        required: true,
        maxlength: 5,
        min: 0,
        checkOnlyNull: true,
        floatNumber: true
      }
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#serviceModal');
    }
  }, validationForm));

  // Adding addToWO popup
  $('#serviceToWoForm').validate($.extend({}, {
    ignore: [],
    debug: false,
    rules: {
      'repair_workorder[deviceInfo][estimationTime]': {
        required: true,
        maxlength: 5,
        min: 0,
        checkOnlyNull: true,
        floatNumber: true
      },
      'repair_workorder[deviceInfo][comments]': {
        required: () => {
          function htmlSpecialChars(editorcontentCharLess) {
            editorcontentCharLess = editorcontentCharLess.replace(/&amp;/g, '&').replace(/&quot;/g, '"').replace(/&#039;/g, `'`).replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&rsquo;/g, '’').replace(/&lsquo;/g, '‘').replace(/&laquo;/g, '«').replace(/&raquo;/g, '»').replace(/&prime;/g, '′').replace(/&ldquo;/g, '“').replace(/&rdquo;/g, '”');
            return editorcontentCharLess;
          }
          CKEDITOR.instances.email_editor.updateElement();
          let currentTextarea = $('#email_editor');
          let editorcontent = currentTextarea.val().replace(/<[^>]*>/gi, '').replace(/&nbsp;/g, '').trim().replace(/\n/g, ' ').replace(/\s\s+/g, ' ').replace(/(\r\n\t|\n|\r\t)/g, '');
          editorcontent = htmlSpecialChars(editorcontent);
          currentTextarea.val(editorcontent);
          return false;
        },
        maxlength: 1000
      },
      'repair_workorder[deviceInfo][price]': {required: true}
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#serviceToWoModal');
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding frequencyModal popup
  $('#frequencyForm').validate($.extend({}, {
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#frequencyModal');
    }
  }, validationForm));

  // Editing general workorder information
  $('#editGeneralWorkorderInfo').validate($.extend({}, {
    rules: {
      'workorder_general_info[directions]': ruleMaxLength(255),
      'workorder_general_info[additionalComments]': ruleMaxLength(255),
      'workorder_general_info[additionalCostComment]': ruleMaxLength(255),
      'workorder_general_info[internalComment]': ruleMaxLength(255)
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#editingGeneralWorkorderInfoModal');
    }
  }, validationForm));

  // Editing General Proposal Information
  $('#editGeneralProposalInfo').validate($.extend({}, {
    rules: {
      'proposal_general_info[additionalComment]': ruleMaxLength(255),
      'proposal_general_info[additionalCostComment]': ruleMaxLength(255),
      'proposal_general_info[internalComments]': ruleMaxLength(255),
      'proposal_general_info[afterHoursWorkCost]': {required: false},
      'proposal_general_info[discount]': {required: false}
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#editGeneralProposalInfoModal');
    }
  }, errorStyles, errorPlaces, validationForm));

  // Add New Repair Service Information
  $('#serviceNamedForm').validate($.extend({}, {
    rules: {
      'admin_bundle_repair_service_named_with_contractor[deficiency]': {
        required: true,
        notOnlySpace: true
      },
      'admin_bundle_repair_service_named_with_contractor[name]': {
        required: true,
        notOnlySpace: true
      },
      'admin_bundle_repair_service_named_with_contractor[deviceNamed]': {required: true}
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#serviceNamedFormModal');
    }
  }, validationForm));

  // Adding validation to set roles form
  $('#roleForm').validate($.extend({}, {
    rules: {
      'account_contact_person[notes]': ruleMaxLength(255),
      'account_contact_person[billingAddress][address]': {
        required: () => {
          if ($('#custom_billing_address').is(':checked')) {
            return true;
          } else {
            return $('#account_contact_person_payment').is(':checked');
          }
        },
        limitSymbols: true,
        notOnlySpace: true
      },
      'account_contact_person[billingAddress][city]': {
        required: () => {
          if ($('#custom_billing_address').is(':checked')) {
            return true;
          } else {
            return $('#account_contact_person_payment').is(':checked');
          }
        },
        maxlength: 31,
        notOnlySpace: true
      },
      'account_contact_person[billingAddress][state]': {
        required: () => {
          if ($('#custom_billing_address').is(':checked')) {
            return true;
          } else {
            return $('#account_contact_person_payment').is(':checked');
          }
        },
        maxlength: 255
      },
      'account_contact_person[billingAddress][zip]': {
        required: () => {
          if ($('#custom_billing_address').is(':checked')) {
            return true;
          } else {
            return $('#account_contact_person_payment').is(':checked');
          }
        },
        validationAddress: true,
        maxlength: 13,
        notOnlySpace: true
      }
    },
    submitHandler: (form, event) => {
      if ($('input[type="checkbox"][required="required"]:checked').length === 0) {
        $('#warningRoleModal').modal('show');
      } else {
        onSubmitHandler(form, event, '#roleModal');
      }
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to select device form
  $('#selectDeviceForm').validate($.extend({}, {
    rules: {
      deviceName: {required: true}
    },
    submitHandler: () => {
      const linkTo = $('select[name="deviceName"] option:selected').attr('data-link');
      $(location).attr('href', linkTo);
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to device form
  $('#deviceForm').validate($.extend({}, {
    rules: {
      'form_device[device][location]': {maxlength: 30},
      'form_device[device][noteToTester]': {maxlength: 255}
    },
    submitHandler: (form, event) => {
      const capsInput = $('#deviceForm input.app-font--uppercase');
      if (capsInput.length > 0) {
        capsInput.each((index, element) => {
          let currentElement = $(element);
          currentElement.val(currentElement.val().toUpperCase());
        });
      }
      onSubmitHandler(form, event, '#deviceModal', 'Device');
    }
  }, validationForm));

  // Adding validation to service form
  $('#serviceForm').validate($.extend({}, {
    rules: {
      'admin_bundle_service[named]': {required: true},
      'admin_bundle_service[lastTested]': {correctDate: true, lessDate: $('#admin_bundle_service_inspectionDue'), lessEqualDate: new Date()},
      'admin_bundle_service[inspectionDue]': {correctDate: true, required: true},
      'admin_bundle_service[comment]': {maxlength: 20}
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#serviceModal', 'Service');
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to Change inspections information form
  $('#servicesChangeInformationsForm').validate($.extend({}, {
    rules: {
      'inspections_information[named]': {required: true}
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#changeInspectionInfoModal');
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to workorder form
  $('#workorderForm').validate($.extend({}, {
    rules: {
      'authorizer': {required: true},
      'workorder[directions]': {maxlength: 255},
      'workorder[internalComment]': {maxlength: 255}
    },
    submitHandler: (form, event) => {
      let selectorId = '#workorderModal';

      if ($('#skipValid').data('target') !== '#skipService') {
        selectorId = '#workorderPreventDefault';
      }

      onSubmitHandler(form, event, selectorId, 'Workorder');
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to select agent form
  $('#selectAgentForm').validate($.extend({}, {
    rules: {
      agentName: {required: true}
    },
    submitHandler: () => {
      const agentId = $('select[name="agentName"]').val();
      const selector = $('#selectAgentForm button[type="submit"]');
      const departmentId = selector.attr('data-departmentId');
      const link = `/admin/municipality/attach/${departmentId}/${agentId}`;
      const linkTo = getEnvironment(selector, link);
      $(location).attr('href', linkTo);
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to select channel form
  $('#selectChannelForm').validate($.extend({}, {
    rules: {
      channelName: {required: true}
    },
    submitHandler: () => {
      const channelId = $('select[name="channelName"]').val();
      const selector = $('#selectChannelForm button[type="submit"]');
      const departmentId = selector.attr('data-departmentId');
      const agentId = selector.attr('data-agentId');
      const link = agentId ? `/admin/department/channel-for-agent/create/${departmentId}/${channelId}/${agentId}` :
        `/admin/department/channel/create/${departmentId}/${channelId}`;
      const linkTo = getEnvironment(selector, link);
      $(location).attr('href', linkTo);
    }
  }, errorStyles, errorPlaces, validationForm));

  // Return link for prod or dev
  function getEnvironment(selector, link) {
    const environment = selector.attr('data-environment');
    if (environment === 'dev') {
      return '/app_dev.php' + link;
    }
    return link;
  }

  // Adding validation to channel form
  $('#channelForm').validate($.extend({}, {
    rules: {
      'department_channel[fields][0][entityValue][zip]': {
        required: true,
        validationAddress: true,
        maxlength: 15
      }
    }
  }, errorStyles, validationForm, {
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#channelModal', 'Complience Channel');
    }
  }
  ));

  // Adding validation to datepicker
  $('.datepickerValidation').validate($.extend({}, {
    rules: {
      'date[from]': {correctDate: true},
      'date[to]': {correctDate: true},
      'date[createdFrom]': {correctDate: true},
      'date[createdTo]': {correctDate: true}
    },
    errorPlacement: () => {}
  }, validationForm));

  // Adding validation to email form
  $('#emailForm').validate($.extend({}, {
    ignore: [],
    rules: {
      'letter[from]': {required: true},
      'letter[emails]': ruleRequired(255),
      'letter[subject]': ruleRequired(255),
      'letter[body]': {required: () => {
        CKEDITOR.instances.email_editor.updateElement();
        let editorcontent = $('#email_editor').val().replace(/<[^>]*>/gi, '').replace(/&nbsp;/g, '').replace(/^\s*$/, '');
        $('#email_editor').val(editorcontent);
        return true;
      }}
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#emailModal');
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to WO technician form
  $('#woTechnicianForm').validate($.extend({}, {
    rules: {
      'schedule[user]': ruleRequired(),
      'schedule[startTime]': {required: true, compareTime: $('#scheduleEndTime')},
      'schedule[endTime]': {required: true},
      'schedule[startDate]': {correctDate: true, required: true, largerEqualDate: new Date().setHours(0, 0, 0, 0)}
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#woTechnicianModal');
    }
  }, errorStyles, errorPlaces, validationForm));

  $('#createCustomProposalForm').validate({
    rules: {
      'admin_bundle_custom_create[proposalFile]': {required: true}
    },
    submitHandler: (form, event) => {
      sendForm(form, event);
    }
  });

  // Adding validation to User form
  $('#userForm').validate($.extend({}, {
    rules: {
      'contractor_user[user][firstName]': ruleRequired(50),
      'contractor_user[user][lastName]': ruleRequired(50),
      'contractor_user[user][email]': ruleEmail(true),
      'contractor_user[user][plainPassword]': {
        required: true,
        notOnlySpace: true,
        maxlength: 20,
        minlength: 6,
        noSpace: true
      },
      'user[firstName]': ruleRequired(50),
      'user[lastName]': ruleRequired(50),
      'user[email]': ruleEmail(true),
      'user[plainPassword]': {
        required: true,
        notOnlySpace: true,
        maxlength: 20,
        minlength: 6,
        noSpace: true
      }
    },
    submitHandler: (form, event) => {
      onSubmitHandler(form, event, '#userModal', 'User');
    }
  }, validationForm));

  // Create WO Form Comments
  $('#workorderCommentForm').validate({
    rules: {
      'admin_bundle_workorder_comment[comment]': {
        required: false,
        maxlength: 500,
        notOnlySpace: true
      },
      'admin_bundle_workorder_comment[photo]': {
        required: false,
        commentFileValidate: true
      }
    },
    submitHandler: (form, event) => {
      sendForm(form, event);
    }
  });

  // Use custom PDF
  $('#uploadCustomInvoicePdf').validate({
    rules: {
      'form[comment]': {
        required: true,
        maxlength: 255,
        notOnlySpace: true
      },
      'form[image]': {
        required: true,
        validatePDFfile: true
      }
    },
    submitHandler: (form, event) => {
      sendForm(form, event);
    }
  });

  // // Create GenericWOForm
  // $('#createGenericWOForm').validate({
  //   rules: {
  //     'admin_bundle_custom_create[proposalFile]': {required: false}
  //   },
  //   submitHandler: (form, event) => {
  //     sendForm(form, event);
  //   }
  // });

  // Adding validation to popup form with comment field
  function commentFormValidation(formId, rule) {
    $(formId).validate($.extend({}, {
      rules: rule,
      submitHandler: (form, event) => {
        sendForm(form, event);
      }
    }, validationForm));
  }

  commentFormValidation('#callCommentForm', {comment: ruleRequired(255)});
  commentFormValidation('#rejectCommentForm', {comment: ruleRequired(255)});
  commentFormValidation('#discardServicesCommentForm', {comment: ruleRequired(255)});
  commentFormValidation('#deleteCommentForm', {comment: ruleRequired(255)});
  commentFormValidation('#reschedulingCommentForm', {comment: ruleRequired(255)});
  commentFormValidation('#resetCommentForm', {comment: ruleMaxLength(255)});
  commentFormValidation('#replaceCommentForm', {comment: ruleRequired(255)});
  commentFormValidation('#uploadBillForm', {'form[comment]': ruleMaxLength(255)});
  commentFormValidation('#workorderPaidForm', {'form[comment]': ruleMaxLength(255)});
  commentFormValidation('#workorderReportSentForm', {'form[comment]': ruleMaxLength(255)});
  commentFormValidation('#finishWorkorderForm', {comment: ruleMaxLength(255)});
  commentFormValidation('#invoceSentViaMailFax', {comment: ruleRequired(255)});
  commentFormValidation('#reportSentToAHJViaMailFaxForm', {comment: ruleRequired(255)});
  commentFormValidation('#finishServiceReport', {'form[reportFile]': {required: true}});
  commentFormValidation('#finishService', {});
  commentFormValidation('#sentReportsToAuthorizer', {comment: ruleRequired(255)});
  commentFormValidation('#pendingPayment', {comment: ruleRequired(255)});


  // Adding validation to Create WO popup
  $('#createWO').validate($.extend({}, {
    rules: {
      'workorder_log[authorizer]': {required: true},
      'workorder_log[comment]': ruleMaxLength(255)
    },
    submitHandler: (form, event) => {
      sendForm(form, event);
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to Create Custom WO popup
  $('#createCustomWO').validate($.extend({}, {
    rules: {
      'workorder_custom_log[authorizer]': {required: true},
      'workorder_custom_log[comment]': ruleMaxLength(255)
    },
    submitHandler: (form, event) => {
      sendForm(form, event);
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to edit deficiency comment
  $('#editRepairServiceCommentForm').validate($.extend({}, {
    rules: {
      comment: {
        maxlength: 255,
        notOnlySpace: true
      }
    },
    submitHandler: (form, event) => {
      sendForm(form, event);
    }
  }, validationForm));

  $('[name="admin_bundle_service[inspectionDue]"]').on('change', () => {
    $('[name="admin_bundle_service[lastTested]"]').valid();
  });

  // Class for validate numbers
  $('.app-validation-digits').each((index, element) => {
    $(element).rules('add', ruleDigits());
  });

  // Class for validate emails
  $('.app-validation-email').each((index, element) => {
    $(element).rules('add', ruleEmail(true));
  });

  // Class for validate dynamic fields on max length
  $('.app-validation-dynamicFields').each((index, element) => {
    $(element).rules('add', ruleMaxLength(30));
  });

  // Channel Address
  $('.app-validation-maxLength100').each((index, element) => {
    $(element).rules('add', $.extend({}, ruleMaxLength(100), ruleRequired()));
  });

  // Channel City
  $('.app-validation-maxLength50').each((index, element) => {
    $(element).rules('add', $.extend({}, ruleMaxLength(50), ruleRequired()));
  });

  // Class for validate required fields
  $('.app-validation-required').each((index, element) => {
    $(element).rules('add', ruleRequired());
  });

  // Class for validate pdf
  $('.app-validation-pdf').rules('add', {
    required: true,
    accept: 'pdf',
    messages: {
      accept: 'Please upload pdf.'
    }
  });
  // Class for validate image and text files
  function validateImageAndFile() {
    $('.app-validation-file-and-img').each((index, element) => {
      $(element).rules('add', {
        accept: ['jpg', 'jpeg', 'png', 'pdf'],
        messages: {
          accept: 'Please use PDF file or Image'
        }
      });
    });
  }

  function validatePDFfile() {
    $('.app-validation-pdf').rules('add', {
      required: true,
      accept: 'pdf',
      messages: {
        accept: 'Please upload pdf.'
      }
    });
  }

  function limitDigitsRule(maxLength) {
    return $.extend({}, ruleDigits(), ruleMaxLength(maxLength));
  }

  $('.app-validation-dynamicZip').each((index, element) => {
    $(element).rules('add', limitDigitsRule(50));
  });

  $('.app-limit-digits').each((index, element) => {
    $(element).rules('add', limitDigitsRule(2));
  });

  $('.app-limit-digits3').each((index, element) => {
    $(element).rules('add', limitDigitsRule(3));
  });

  $('.app-limit-digits4').each((index, element) => {
    $(element).rules('add', limitDigitsRule(4));
  });

  /* $('.app-limit-number').each((index, element) => {
    $(element).rules('add', {
      max: 99.99,
      min: 0.01,
      number: true,
      checkFloatingPoint: false
    });
  });*/

  // Class for validate phone
  $('.app-validation-phone').each((index, element) => {
    $(element).rules('add', {
      correctNumber: true
    });
  });

  // Add custom methods to validator
  jQuery.validator.addMethod('filesize', (value, element, param) => {
    return element.files[0].size <= param;
  }, 'PDF max length 5 MB.');

  jQuery.validator.addMethod('trueEmail', value => {
    return !value || (/^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/
      .test(value)
    );
  }, 'Please enter a valid email address.');

  jQuery.validator.addMethod('notOnlySpace', value => {
    return (!(/^\s*$/.test(value)) || value.length === 0);
  }, 'String contains only spaces');

  jQuery.validator.addMethod('correctNumber', value => {
    return (value.indexOf('_') === -1) || !(/\d/.test(value));
  }, 'Please enter a full number.');

  jQuery.validator.addMethod('checkOnlyNull', value => {
    return value.search(/00/i) !== 0;
  }, 'Number can not consists only "0".');

  jQuery.validator.addMethod('floatNumber', value => {
    return value.indexOf('.') === -1 ? /^[0-9]{1,2}$/.test(value) && value >= 0 : /^[0-9]{1,2}\.+[0-9]{1,2}$/.test(value) && value >= 0;
  }, 'Please enter valid estimate');

  jQuery.validator.addMethod('validationAddress', value => {
    return (/^[0-9-]+$/.test(value) || value === '');
  }, 'Use only digits and -');

  jQuery.validator.addMethod('checkFloatingPoint', value => {
    return value.split('.')[0].length > 0 && value.split('.')[1].length <= 2;
  }, 'No more than two symbols after dot.');

  // Check on exist date
  function isTrueDate(value) {
    const date = value.split('/').map(Number);
    const year = date[2];
    const month = date[0] - 1;
    const day = date[1];
    const trueDate = new Date(year, month, day);
    return (trueDate.getFullYear() === year && trueDate.getMonth() === month && trueDate.getDate() === day);
  }

  function isDate(date) {
    return date && date !== 'MM/DD/YYYY' && isTrueDate(date);
  }

  jQuery.validator.addMethod('correctDate', value => {
    return isTrueDate(value) || !value || value === 'MM/DD/YYYY';
  }, 'Please enter a valid date.');

  jQuery.validator.addMethod('app-validation-autocomplete', value => {
    const arrayText = [];
    $('.selectAutocomplete option').each((index, element) => {
      const optionText = $(element).text();
      arrayText.push(optionText);
    });
    return (arrayText.indexOf(value) > -1 || !value);
  }, 'Please select existing data from autocomplete');

  jQuery.validator.addMethod('lessDate', (value, element, params) => {
    const paramsValue = $(params).val();
    if (isDate(value) && isDate(paramsValue)) {
      return new Date(value) < new Date(paramsValue);
    }
    return true;
  }, 'Last Tested Date must be less than Inspection Due Date.');

  jQuery.validator.addMethod('lessEqualDate', (value, element, params) => {
    if (isDate(value)) {
      return new Date(value) <= params;
    }
    return true;
  }, 'Last Tested Date must be smaller or equal than Current Date.');

  jQuery.validator.addMethod('largerEqualDate', (value, element, params) => {
    if (isDate(value)) {
      return new Date(value) >= params;
    }
    return true;
  }, 'Schedule Date must be later or equal to the Current Date.');

  jQuery.validator.addMethod('compareTime', (value, element, params) => {
    let endTimeValue = Number($(params).find(':selected').val().replace(':', ''));
    let startTimeValue = Number($(element).find(':selected').val().replace(':', ''));
    return startTimeValue < endTimeValue;
  }, 'Start Time cannot be equal or later than End Time.');

  jQuery.validator.addMethod('commentFileValidate', () => {
    return validateImageAndFile();
  }, 'Load image or file.');

  jQuery.validator.addMethod('validatePDFfile', () => {
    return validatePDFfile();
  }, 'Load file.');

  jQuery.validator.addMethod('limitSymbols', (value, element) => {
    return !$(element).is('[data-symbols-oversized]');
  }, 'Each line must have no more than 41 characters.');

  $('#scheduleEndTime').on('change', () => {
    const starTime = $('#scheduleStartTime');
    if (starTime.valid()) {
      starTime.removeClass('invalid').addClass('success');
    }
  });

  jQuery.validator.addMethod('noSpace', value => {
    return value.indexOf(' ') < 0;
  }, 'No space please');

  // Limit a quantity of lines in Billing address and add attrr if maxlength of line is more than 41 symbols
  $('#account_contact_person_billingAddress_address').on('change input propertychange', function() {
    let maxlength = 41;
    let rows = $(this).attr('rows');
    let lines = $(this).val().split('\n');

    $(this).removeAttr('data-symbols-oversized');

    for (let i = 0; i < lines.length; i++) {
      if (lines[i].length > maxlength) {
        $(this).attr('data-symbols-oversized', 'true');
        break;
      }
    }

    while (lines.length > rows) {
      lines.pop();
    }

    $(this).val(lines.join('\n'));
  });

  // Added masks to inputs
  $('.app-validation-phone').mask('(999) 999-9999', {autoclear: false});
  $('.app-validation-date').mask('99/99/9999', {placeholder: 'MM/DD/YYYY', autoclear: false});

  // Adding validation to send invoice via email form
  $('#sendInvoiceViaEmailForm').validate($.extend({}, {
    ignore: [],
    rules: {
      'admin_bundle_file[from]': {
        required: true,
        maxlength: 100,
        notOnlySpace: true
      },
      'admin_bundle_file[to]': ruleEmail(true),
      'admin_bundle_file[emailSubject]': {
        required: true,
        maxlength: 100,
        notOnlySpace: true
      },
      'admin_bundle_file[emailBody]': {required: () => {
        CKEDITOR.instances.admin_bundle_file_emailBody.updateElement();
        let editorContent = $('#admin_bundle_file_emailBody').val().replace(/<[^>]*>/gi, '').replace(/&nbsp;/g, '').replace(/^\s*$/, '');
        $('#admin_bundle_file_emailBody').val(editorContent);
        return true;
      }}
    },
    submitHandler: (form, event) => {
      sendForm(form, event);
    }
  }, errorStyles, errorPlaces, validationForm));

  // Adding validation to invoiceCreate form
  $('#invoiceCreate').validate($.extend({}, {
    rules: {
      'invoice_date[date]': {
        correctDate: true,
        required: true
      },
      'invoice_terms[pono]': {
        maxlength: 25,
        notOnlySpace: true
        // noSpecialChars: true
      },
      'invoice_terms[inspector]': {
        maxlength: 13,
        notOnlySpace: true
      },
      'invoice_terms[memo]': {
        maxlength: 100,
        notOnlySpace: true
      },
      'invoice_billing_address_address': {
        limitSymbols: true,
        notOnlySpace: true,
        required: true
      },
      'invoice_billing_address_city': {
        maxlength: 31,
        notOnlySpace: true,
        required: true
      },
      'invoice_billing_address_state': {
        required: true
      },
      'invoice_billing_address_zip': {
        validationAddress: true,
        maxlength: 13,
        required: true
      }
    }
  }, errorStyles, errorPlaces, validationForm));
})(jQuery);
