/* global jQuery*/
/* global fetch */
(function($) {
  // Get token id
  const idToken = $('#access_token').attr('value');

  // Get inputs
  const companySelect = $('#admin_bundle_service_companyLastTested');
  const municipalitySelect = $('#account_municipality');
  const companyInput = $('#contractor-autocomplete');
  const municipalityInput = $('#municipality-autocomplete');
  const changeInspectionInfoSelect = $('#inspections_information_companyLastTested');
  const changeInspectionInfoInput = $('#change-company-last-tested-autocomplete');

  // Autocomplete
  function addAutocomplete(selector, url) {
    return selector.autocomplete({
      open: () => {
        $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
      },
      source: (request, response) => {
        $.ajax({
          url: url,
          dataType: 'json',
          data: {
            phrase: request.term
          },
          method: 'POST',
          headers: {
            'X-ACCESS-TOKEN': idToken
          },
          success: data => {
            response($.map(data, item => {
              return item.name;
            }));
          }
        });
      },
      minLength: 1
    }).focus(e => {
      $(e.currentTarget).autocomplete('search');
    });
  }

  addAutocomplete(companyInput, '/api/v1/contractor/search-by-name');
  addAutocomplete(municipalityInput, '/api/v1/municipality/search-by-name');
  addAutocomplete(changeInspectionInfoInput, '/api/v1/contractor/search-by-name');

  // Add attribute 'selected' for original select
  function addSelect(input, select) {
    input.on('blur', e => {
      const changeValue = $(e.currentTarget).val();
      const arrayOptions = select.find('option');
      arrayOptions.each(function(index, value) {
        $(value).removeAttr('selected');

        if (changeValue === $(value).text()) {
          $(value).attr('selected', true);
        }
      });
    });
  }

  addSelect(companyInput, companySelect);
  addSelect(municipalityInput, municipalitySelect);
  addSelect(changeInspectionInfoInput, changeInspectionInfoSelect);

  // Get contractor's price
  function getContractorPrice(contractorService, contractorCompany, outputBlock) {
    if (contractorService && contractorCompany) {
      $.ajax({
        method: 'GET',
        headers: {
          'X-ACCESS-TOKEN': idToken
        },
        url: `/api/v1/contractor-service/get-price?service=${contractorService}&contractor=${contractorCompany}`,
        success: result => {
          const resultPrice = result.fixedPrice ? `$${result.fixedPrice}` : '-';
          outputBlock.html(resultPrice);
        },
        error: () => {
          outputBlock.html('-');
        }
      });
    } else {
      outputBlock.html('-');
    }
    return;
  }

  // Get contractor's test fee on changed service name and company last tested
  function changeContractorPrice() {
    const contractorService = $('#admin_bundle_service_named').val();
    let contractorCompany;
    companySelect.find(':selected').each(function(index, element) {
      if (element.value) {
        contractorCompany = element.value;
      }
    });
    const outputBlock = $('#priceContractor');
    getContractorPrice(contractorService, contractorCompany, outputBlock);
  }

  // Get abfp test fee on changed service name
  function changeAbfpPrice() {
    const contractorService = $('#admin_bundle_service_named').val();
    const outputBlock = $('#priceABFP');
    const contractorCompany = outputBlock.attr('data-ABFP');
    getContractorPrice(contractorService, contractorCompany, outputBlock);
  }

  // Get frequency on changed service name
  function changeServiceFrequency() {
    const contractorService = $('#admin_bundle_service_named');
    const frequencyOutput = $('#serviceFrequency');
    if (contractorService.val()) {
      const serviceFrequence = contractorService.find('option:selected').attr('data-frequency');
      frequencyOutput.html(serviceFrequence);
    } else {
      frequencyOutput.html('-');
    }
  }

  $('#admin_bundle_service_named').on('change', () => {
    changeAbfpPrice();
    changeContractorPrice();
    changeServiceFrequency();
  });

  companyInput.on('blur', () => {
    changeContractorPrice();
  });

  // Set contractor's prices, service frequency, contractor's company on load
  changeAbfpPrice();
  changeContractorPrice();
  changeServiceFrequency();
  const selectedCompany = companySelect.find('option:selected');
  if (selectedCompany.val()) {
    const companyName = selectedCompany.text();
    companyInput.val(companyName);
  }

  // Get content for WO tooltip
  function getToolTipContent(event) {
    const placeholder = 'No Comment';
    const workorderId = $(event.target).attr('data-workorder-id');
    const api = `/api/v1/workorder-log-comment?id=${workorderId}`;
    const configApi = {
      method: 'GET',
      headers: {
        'X-ACCESS-TOKEN': idToken
      }
    };

    fetch(api, configApi)
      .then(response => response.json())
      .then(json => {
        $(event.target).attr('data-original-title', (json.comment || placeholder));
      });
  }
  $('.tooltipWoTotalFee').on('mouseenter', e => getToolTipContent(e));
})(jQuery);
