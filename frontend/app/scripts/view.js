/* jslint browser:true*/
/* global jQuery*/
/* global window*/
(function($, window) {
  'use strict';

  // Count blocks height on view pages
  function contactNotesHeight() {
    const contactDataHeight = $('#contactHeight1').innerHeight() + $('#contactHeight2').innerHeight();
    $('.app-contact-main__notes-text').innerHeight(contactDataHeight);
  }
  function setColumnHeight() {
    const trueHeight = $('.heightDetector').innerHeight();
    $('.app-contact-list__container--tbody').innerHeight(trueHeight - 37);
  }

  contactNotesHeight();
  setColumnHeight();

  $(window).resize(() => {
    contactNotesHeight();
    setColumnHeight();
  });

  // Click on Add Agent or Add Channel, set true id for generating link
  $('.get-municipalityData').click(e => {
    const target = $(e.target);
    const departmentId = target.attr('data-departmentId');
    const agentId = target.attr('data-agentId');
    const modalId = target.attr('data-target');
    const modalSelector = $(`${modalId} button[type="submit"]`);
    modalSelector.attr('data-departmentId', departmentId);
    if (agentId) {
      modalSelector.attr('data-agentId', agentId);
    }
  });

  // Setting ContactPerson Roles (unchecked, checked)
  function unchecked(checkedInput, uncheckedInput) {
    if (!checkedInput.checked) {
      $(uncheckedInput).prop('checked', false);
    }
  }

  function checked(checkedInput, uncheckedInput) {
    if (checkedInput.checked) {
      $(uncheckedInput).prop('checked', true);
    }
  }

  $('#account_contact_person_authorizer').change(e => {
    unchecked(e.currentTarget, '#account_contact_person_responsibilities_1');
    unchecked(e.currentTarget, '#account_contact_person_responsibilities_2');
    unchecked(e.currentTarget, '#account_contact_person_responsibilities_3');
    unchecked(e.currentTarget, '#account_contact_person_responsibilities_4');
  });

  $('.app-contactPerson-authorizer').change(e => {
    checked(e.currentTarget, '#account_contact_person_authorizer');
  });

  $('#account_contact_person_payment').change(e => {
    unchecked(e.currentTarget, '#account_contact_person_paymentResponsibilities_1');
    unchecked(e.currentTarget, '#account_contact_person_paymentResponsibilities_2');
    unchecked(e.currentTarget, '#account_contact_person_paymentResponsibilities_3');
    unchecked(e.currentTarget, '#account_contact_person_paymentResponsibilities_4');
  });

  $('.app-contactPerson-payment').change(e => {
    checked(e.currentTarget, '#account_contact_person_payment');
    if ($(e.currentTarget).is(':checked') && $('.app-role__label .app-required-label').length === 0) {
      addRequiredFieldsToBillingAdress();
    }
  });

  $('#account_contact_person_access').change(e => {
    unchecked(e.currentTarget, '#account_contact_person_accessPrimary');
  });

  $('#account_contact_person_accessPrimary').change(e => {
    checked(e.currentTarget, '#account_contact_person_access');
  });

  // Add required field labels to "Billing address"
  function addRequiredFieldsToBillingAdress() {
    $('.app-role__label').append('<span class="app-required-label">*</span>');
  }

  // Remove required field labels from "Billing address"
  function removeRequiredFieldsFromBillingAdress() {
    $('.app-role__label .app-required-label').remove();
  }

  // Add/Remove required field labels to "Billing address" after event load
  $(document).ready(function() {
    if ($('#account_contact_person_payment').is(':checked')) {
      addRequiredFieldsToBillingAdress();
    } else {
      removeRequiredFieldsFromBillingAdress();
    }
  });

  // Add/Remove required field labels to "Billing address" after event change
  $('#account_contact_person_payment').change(function() {
    if ($(this).is(':checked')) {
      addRequiredFieldsToBillingAdress();
    } else {
      removeRequiredFieldsFromBillingAdress();
    }
  });

  // Block amimation, from up to down (fast)
  $('.showBlockAnim').click(event => {
    event.preventDefault();
    let target = $(event.target).attr('data-target');
    $(target).slideToggle('fast');
  });

  // Enabling edit for billing address fields
  $('#custom_billing_address').change(event => {
    if ($(event.target).is(':checked') === true) {
      $('[name="account_contact_person[billingAddress][address]"]').removeClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][address]"]').attr('readonly', false);

      $('[name="account_contact_person[billingAddress][city]"]').removeClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][city]"]').attr('readonly', false);

      $('.app-role__select-state').removeClass('app-form--disabled disabled');
      $('.app-role__select-state button').removeClass('disabled');
      $('.app-role__select-state button').removeClass('app-role__select--white');
      $('.app-role__select-state button').attr('disabled', false);

      $('[name="account_contact_person[billingAddress][zip]"]').removeClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][zip]"]').attr('readonly', false);

      $('.copyAddressesButton').slideDown('fast');
    } else {
      $('[name="account_contact_person[billingAddress][city]"]').addClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][city]"]').attr('readonly', 'readonly');

      $('[name="account_contact_person[billingAddress][address]"]').addClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][address]"]').attr('readonly', 'readonly');

      $('.app-role__select-state').addClass('app-form--disabled');
      $('.app-role__select-state button').addClass('disabled');
      $('.app-role__select-state button').addClass('app-role__select--white');
      $('.app-role__select-state button').attr('disabled', 'disabled');

      $('[name="account_contact_person[billingAddress][zip]"]').addClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][zip]"]').attr('readonly', 'readonly');

      $('.copyAddressesButton').slideUp('fast');
      $('#addressesBlock').slideUp('fast');
      const currentCustomerJSON = $('.selectedCustomer[data-select-customer-select]').attr('data-select-customer-select');
      const currentCustomer = JSON.parse(currentCustomerJSON);
      transferAddresses(currentCustomer.data.billing_address);
      $('[data-copy-address]').show();
      // $('.app-role__select-state').change().blur();
    }
  });

  // Enabling edit for billing address fields on load
  $(document).ready(() => {
    if ($('#custom_billing_address').is(':checked') === true) {
      $('[name="account_contact_person[billingAddress][address]"]').removeClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][address]"]').attr('readonly', false);

      $('[name="account_contact_person[billingAddress][city]"]').removeClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][city]"]').attr('readonly', false);

      $('.app-role__select-state').removeClass('app-form--disabled disabled');
      $('.app-role__select-state button').removeClass('disabled');
      $('.app-role__select-state button').removeClass('app-role__select--white');
      $('.app-role__select-state button').attr('disabled', false);

      $('[name="account_contact_person[billingAddress][zip]"]').removeClass('app-form--disabled');
      $('[name="account_contact_person[billingAddress][zip]"]').attr('readonly', false);

      $('.copyAddressesButton').slideDown('fast');
    }
  });

  // Disabling select state button
  $('.app-role__select-state').siblings('button').attr('disabled', 'disabled');

  // Set Customer function
  function setCustomer(data, img, href) {
    $('.customerImg').attr('src', img);
    $('.customerName').text(data.data.name).attr('href', href);
    $('.customerId').text('(' + data.data.entity_type.name + ', ID ' + data.data.entityId + ')');
    $('#account_contact_person_customer option').each((index, element) => {
      if ($(element).val() === data.value) {
        $(element).attr('selected', 'selected');
      } else {
        $(element).attr('selected', false);
      }
    });
  }

  // Transfer addresses
  function transferAddresses(address, name) {
    if (!name && address.address) {
      $('[name="account_contact_person[billingAddress][address]"]').val(address.address).change().blur();
    } else if (!address.address && name) {
      $('[name="account_contact_person[billingAddress][address]"]').val(name).change().blur();
    } else if (address.address && name) {
      $('[name="account_contact_person[billingAddress][address]"]').val(name + '\n' + address.address).change().blur();
    } else {
      $('[name="account_contact_person[billingAddress][address]"]').val(name).change().blur();
    }
    $('[name="account_contact_person[billingAddress][city]"]').val(address.city).change().blur();

    if (address.state) {
      $('.app-role__select-state #account_contact_person_billingAddress_state option').each((index, element) => {
        if ($(element).val() === address.state.id) {
          $(element).attr('selected', 'selected');
          $('#account_contact_person_billingAddress_state').selectpicker(`val`, `${address.state.id}`);
        } else {
          $(element).attr('selected', false);
        }
      });
    } else {
      $('.app-role__select-state #account_contact_person_billingAddress_state option').each((index, element) => {
        if ($(element).text() === 'State') {
          $(element).attr('selected', 'selected');
        } else {
          $(element).attr('selected', false);
        }
      });
      $('.app-role__select-state button span.filter-option').text('State');
    }

    if ($('#custom_billing_address').is(':checked') === false) {
      $('.app-role__select-state button').addClass('app-role__select--white');
    }

    $('.app-role__select-state').change().blur();
    $('[name="account_contact_person[billingAddress][zip]"]').val(address.zip).change().blur();
    $('#roleForm').valid();
  }

  // Select customer on ACP page
  $('[data-select-customer-select]').click(e => {
    e.preventDefault();
    const customerDataJSON = $(e.target).attr('data-select-customer-select');
    const customerImg = $(e.target).attr('data-select-customer-img');
    const customerHref = $(e.target).attr('data-select-customer-href');
    const customerData = JSON.parse(customerDataJSON);

    $('[data-select-customer-select]').removeClass('selectedCustomer');
    $(e.target).addClass('selectedCustomer');
    setCustomer(customerData, customerImg, customerHref);
    if ($('#custom_billing_address').is(':checked') === false) {
      transferAddresses(customerData.data.billing_address);
    }
  });

  // Copy billing address
  $('[data-copy-address-billing]').click(e => {
    e.preventDefault();
    const copiedAddressJSON = $(e.target).attr('data-copy-address-billing');
    const copiedAddress = JSON.parse(copiedAddressJSON);

    transferAddresses(copiedAddress.billing);
  });

  // Copy personal address
  $('[data-copy-address-personal]').click(e => {
    e.preventDefault();
    const copiedAddressJSON = $(e.target).attr('data-copy-address-personal');
    const copiedName = $(e.target).attr('data-copy-name');
    const copiedAddress = JSON.parse(copiedAddressJSON);

    transferAddresses(copiedAddress.personal, copiedName);
  });

  // Validate all fields of Billing Address after onchange event on one of them
  $('#account_contact_person_billingAddress_address, #account_contact_person_billingAddress_city,' +
    ' #account_contact_person_billingAddress_state, #account_contact_person_billingAddress_zip').change(() => {
    $('#roleForm').valid();
  });

  // Edit deficiency comments
  $('a[data-target="#editRepairServiceComment"]').click(function() {
    let comment = $(this).attr('data-comment');
    let path = $(this).attr('data-action');

    $('#editRepairServiceCommentField').val(comment);
    $('#editRepairServiceCommentForm').attr('action', path);
  });

  // Refresh selectpicker on invoice create
  // $(function() {
  // });
})(jQuery, window);

import '../styles/main.scss';
