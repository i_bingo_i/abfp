/* jslint browser:true*/
/* global jQuery*/

(function($) {
  'use strict';

  $('tr[row-close="true"]').hide();

  // Remove border bottom from last row
  function lastRowStyles() {
    $('#deviceTable tr').removeClass('app-table__last-row');
    $('#deviceTable tr:visible:last').addClass('app-table__last-row');
  }

  function hideServiceHeader() {
    $('tr[service-header="service-header"]').hide();
  }

  // Show services headers
  function showServiceHeader() {
    hideServiceHeader();
    const visibleService = $('tr[type="service"]').filter(':visible');
    visibleService.each((index, element) => {
      const headerId = $(element).attr('header-devision');
      $(`#${headerId}`).show();
    });
  }

  hideServiceHeader();
  showServiceHeader();

  // Show rows for checked type
  function toggleCheckbox(etarget) {
    const targetElement = $(etarget);
    const target = targetElement.attr('id');
    const selector = $(`tr[device-type="${target}"]`);
    if (targetElement.is(':checked')) {
      selector.addClass('row-show');
      $(`tr[type="${target}"]`).show();
      arrowRotateBottom($(`tr[type="${target}"] a.arrow-clickable`));
    } else {
      selector.removeClass('row-show').hide();
    }
  }

  // If nothing device type checked hide devices rows
  function showNoDevision() {
    if ($('input[device="device"]:checked').length === 0) {
      $('#deviceTable').hide();
      $('#select-division').show();
      $('#all-devices').attr('show-device', 'true');
      $('#all-services').attr('show-service', 'true');
    } else {
      $('#deviceTable').show();
      $('#select-division').hide();
    }
  }

  // Return arrays of id checked device type
  function getCheckedArray() {
    const arrayChecked = [];
    $.each($('input[device="device"]'), (index, element) => {
      if ($(element).is(':checked')) {
        arrayChecked.push($(element).attr('id'));
      }
    });
    return arrayChecked;
  }

  // Hide and show blocks
  function toggleRezultBlock(removeBlock, displayBlock) {
    removeBlock.addClass('app-display--none');
    displayBlock.removeClass('app-display--none');
  }

  // Define what result and block will be shown
  function showNoItems(sum, sumAll, countBlock, elementsToggleBlock, noElementsBlock) {
    const resultBlock = $(countBlock);
    resultBlock.attr('data-countAll', sumAll);
    if (sum > 0) {
      toggleRezultBlock(noElementsBlock, elementsToggleBlock);
      resultBlock.html(sum);
    } else if (sum <= 0 && sumAll > 0) {
      toggleRezultBlock(noElementsBlock, elementsToggleBlock);
      resultBlock.html(0);
    } else {
      toggleRezultBlock(elementsToggleBlock, noElementsBlock);
    }
  }

  // Count all devices and serices
  function countAllItems(arrayChecked, countBlock, elementsToggleBlock, noElementsBlock, countAttribute) {
    let sum = 0;
    let sumAll = 0;
    $.each(arrayChecked, (index, element) => {
      const selector = $(`#${element}`);
      sum += Number(selector.attr(countAttribute));
      sumAll += Number(selector.attr(`${countAttribute}All`));
    });
    showNoItems(sum, sumAll, countBlock, elementsToggleBlock, noElementsBlock);
  }

  // Count all devices and serices by account
  function countAccountItems(arrayChecked, countBlock, noElementsAttribute) {
    $.each(countBlock, function(index, element) {
      let sumAccount = 0;
      let sumAll = 0;
      const parentId = $(element).parent().attr('id');
      const devicesElement = $(`#${parentId}`);
      const noDevicesElement = $(`span[${noElementsAttribute}="${parentId}"]`);
      $.each(arrayChecked, (index, checkedInput) => {
        const selector = $(element);
        sumAccount += Number(selector.attr(checkedInput));
        sumAll += Number(selector.attr(checkedInput + 'All'));
      });
      showNoItems(sumAccount, sumAll, element, devicesElement, noDevicesElement);
    });
  }

  function checkboxFunctionsCall(etarget) {
    const arrayChecked = getCheckedArray();
    countAllItems(arrayChecked, $('#deviceCount'), $('#all-devices'), $('span[no-devices="no-devices"]'), 'count-devices');
    countAllItems(arrayChecked, $('#serviceCount'), $('#all-services'), $('span[no-services="no-services"]'), 'count-services');
    countAccountItems(arrayChecked, $('span[count-devices-account="count-devices-account"]'), 'no-devices-account');
    countAccountItems(arrayChecked, $('span[count-services-account="count-services-account"]'), 'no-services-account');
    toggleCheckbox(etarget);
  }

  // click on checkbox
  $('input[device="device"]').on('change', e => {
    checkboxFunctionsCall(e.target);
    showNoDevision();
    showServiceHeader();
    lastRowStyles();
  });

  // click on arrow toggle services row
  // $('td[parent="parent"]').on('click', e => {
  //   const target = $(e.target).attr('id');
  //   const selector = $('tr[child="' + target + '"]');
  //   selector.toggle();
  //   showServiceHeader();
  //   lastRowStyles();
  // });

  // Return true if all amount row more than visible row
  function toggleCheck(target, selector, type, dataCount) {
    // const countItems = Number($(target).children('span').attr('data-countAll'));
    const countItems = Number($(target).children('span').attr(dataCount));
    const openItems = selector.filter(':visible').filter('[type="' + type + '"]').length;
    return countItems > openItems ? 'true' : 'false';
  }

  // click on devices button on type devices row
  /* $('a[toggle-device="toggle-device"]').on('click', e => {
    const target = $(e.target);
    const targetId = target.attr('id');
    const selector = $(`tr[device="${targetId}"]`);
    const flag = toggleCheck(target, selector, 'device');
    $(`tr[general-device="${targetId}"]`).hide();
    if (flag === 'true') {
      selector.each((index, element) => {
        const row = $(element);
        const selectorType = row.attr('type');
        if (selectorType === 'device') {
          row.show();
        }
      });
      arrowRotateBottom($(`tr[device="${targetId}"] td[parent="parent"].arrow-clickable`));
    } else {
      selector.hide();
    }
    showServiceHeader();
    lastRowStyles();
  });*/

  // click on services button on type devices row
  $('a[toggle-service="toggle-service"]').on('click', e => {
    const target = $(e.target);
    const targetId = target.attr('id');
    const selector = $(`tr[service="${targetId}"]`);
    const flag = toggleCheck(target, selector, 'service', 'data-countServices');
    const targetDevice = target.attr('device');
    const selectorDevice = $(`tr[device="${targetDevice}"]:not(tr[type="service"])`);
    const flagDevice = toggleCheck(target, selectorDevice, 'device', 'data-countDevices');
    $(`tr[general-service="${targetId}"]`).hide();

    if (selectorDevice.length <= 0) {
      return false;
    }

    if (flag === 'true' || flagDevice === 'true') {
      selector.show();
      $(`tr[general-service="${targetId}"]`).show();
      arrowRotateTop($(`#${targetId}`));
      // arrowRotateTop($(`tr[general-service="${targetId}"] td[parent="parent"].arrow-clickable`));
    } else {
      selector.each((index, element) => {
        const row = $(element);
        const selectorType = row.attr('type');
        if (selectorType === 'service') {
          row.hide();
        }
      });
      selectorDevice.each((index, element) => {
        $(element).hide();
      });
      arrowRotateBottom($(`#${targetId}`));
      // arrowRotateBottom($(`tr[service="${targetId}"] td[parent="parent"].arrow-clickable`));
    }
    showServiceHeader();
    lastRowStyles();
  });

  // Rotate arrow bottom
  function arrowRotateBottom(selector) {
    selector.removeClass('app-data__table-data--arrow-top')
      .addClass('app-data__table-data--arrow-bottom');
  }

  // Rotate arrow top
  function arrowRotateTop(selector) {
    selector.removeClass('app-data__table-data--arrow-bottom')
      .addClass('app-data__table-data--arrow-top');
  }

  // click on devices button on account row
  $('a[account-device="account-device"]').on('click', e => {
    const target = $(e.target);
    const targetId = target.attr('id');
    const selector = $(`tr[device-account="${targetId}"]`);
    const flag = toggleCheck(target, selector, 'device', 'data-countAll');
    $(`tr[general-device-account="${targetId}"]`).hide();
    if (flag === 'true') {
      selector.each((index, element) => {
        const row = $(element);
        const selectorType = row.attr('type');
        if (row.hasClass('row-show') && selectorType === 'device') {
          row.show();
        }
      });
      arrowRotateTop($(`a[service-accountDevice="${targetId}"]`));
      // arrowRotateBottom($(`tr[device-account="${targetId}"] td[parent="parent"].arrow-clickable`));
    } else {
      selector.hide();
      arrowRotateBottom($(`a[service-accountDevice="${targetId}"]`));
    }
    showServiceHeader();
    lastRowStyles();
  });

  // click on services button on account row
  $('a[account-service="account-service"]').on('click', e => {
    const target = $(e.target);
    const targetId = target.attr('id');
    const selector = $(`tr[service-account="${targetId}"]`);
    const flag = toggleCheck(target, selector, 'service', 'data-countAll');
    if (flag === 'true') {
      selector.each((index, element) => {
        const row = $(element);
        if (row.hasClass('row-show')) {
          row.show();
        }
      });
      arrowRotateTop($(`a[service-accountService="${targetId}"]`));
      // arrowRotateTop($(`tr[service-account="${targetId}"] td[parent="parent"].arrow-clickable`));
    } else {
      selector.each((index, element) => {
        const row = $(element);
        const selectorType = row.attr('type');
        if (selectorType === 'service' || selectorType === 'general-service') {
          row.hide();
        }
      });
      // arrowRotateBottom($(`a[service-accountService="${targetId}"]`));
      // arrowRotateBottom($(`tr[service-account="${targetId}"] td[parent="parent"].arrow-clickable`));
    }
    showServiceHeader();
    lastRowStyles();
  });

  // click on all devices button
  $('#all-devices').on('click', e => {
    const target = $(e.target);
    const selector = $('tr[type="device"]');
    const selectorAll = $.merge($.merge([], selector), $('tr[type="service"]'));
    const flag = toggleCheck(target, selector, 'device', 'data-countAll');
    $('tr[type="general-service"]').hide();
    if (flag === 'true') {
      selector.each((index, element) => {
        const row = $(element);
        if (row.hasClass('row-show')) {
          row.show();
        }
      });
      arrowRotateTop($(`a.arrow-clickable`));
      // arrowRotateBottom($('td[parent="parent"].arrow-clickable'));
    } else {
      $(selectorAll).hide();
      arrowRotateBottom($(`a.arrow-clickable`));
    }
    showServiceHeader();
    lastRowStyles();
  });

  // click on all services button
  $('#all-services').on('click', e => {
    const target = $(e.target);
    const selector = $('tr[type="service"]');
    const generalSelector = $('tr[type="general-service"]');
    const selectorAll = $.merge($.merge($.merge([], selector), $('tr[type="device"]')), generalSelector);
    const flag = toggleCheck(target, selector, 'service', 'data-countAll');
    if (flag === 'true') {
      $(selectorAll).each((index, element) => {
        const row = $(element);
        if (row.hasClass('row-show')) {
          row.show();
        }
      });
      arrowRotateTop($(`a.arrow-clickable`));
      // arrowRotateTop($('td[parent="parent"].arrow-clickable'));
    } else {
      selector.hide();
      generalSelector.hide();
    //  arrowRotateBottom($(`a.arrow-clickable`));
      // arrowRotateBottom($('td[parent="parent"].arrow-clickable'));
    }
    showServiceHeader();
    lastRowStyles();
  });

  // $('.app-data__table-data--arrow-top.arrow-clickable').click(e => {
  //   toggle(e.target, 'app-data__table-data--arrow-top', 'app-data__table-data--arrow-bottom');
  // });

  // $('.app-data__table-data--arrow-bottom.arrow-clickable').click(e => {
  //   toggle(e.target, 'app-data__table-data--arrow-bottom', 'app-data__table-data--arrow-top');
  // });

  // function toggle(target, className1, className2) {
  //   $(target).toggleClass(className1);
  //   $(target).toggleClass(className2);
  // }

  $.each($('input[device="device"]'), (index, element) => {
    checkboxFunctionsCall(element);
  });
  $('#select-division').hide();
  showNoDevision();
  lastRowStyles();
})(jQuery);
