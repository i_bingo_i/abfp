import Vue from 'Vue';
import invoiceApp from './invoice_app.vue';
import Vuelidate from 'vuelidate';

if ($('#invoice-block').length > 0) {
  Vue.config.devtools = true;
  Vue.use(Vuelidate);
  new Vue({
    render: h => h(invoiceApp)
  }).$mount('#invoice-block');
}
