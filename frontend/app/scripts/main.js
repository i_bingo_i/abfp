/* jslint browser:true*/
/* global jQuery*/
/* global window*/

(function($) {
  'use strict';

  // Pagination

  function removeActivePagination(target, className) {
    $('.app-active-page').removeClass('app-active-page');
    $('.app-active-arrow').removeClass('app-active-arrow');
    $(target).addClass(className);
  }

  $('.pagination a').not('.app-pagination-arrow').click(e => {
    removeActivePagination(e.target, 'app-active-page');
  });

  $('.app-pagination-arrow').click(e => {
    removeActivePagination(e.target, 'app-active-arrow');
  });

  // Tabs

  function setTab(activeNumber) {
    $('input[name="tab"]').val(activeNumber);
  }

  $('#tab1').click(() => {
    setTab(1);
  });

  $('#tab2').click(() => {
    setTab(2);
  });

  $('#tab3').click(() => {
    setTab(3);
  });

  // Tooltips and navigation

  $('a').tooltip({container: 'nav',
    viewport: {selector: 'nav', padding: 0}
  });

  $('.tooltipWarning').tooltip({container: '.app-main'});

  $('.tooltipWoTotalFee').tooltip({
    container: '.app-main',
    delay: 1000
  });

  $('.app-nav__icon').hover(e => {
    $('[data-toggle="tooltip"]').not($(e.currentTarget)).tooltip('hide');
  });

  // Links

  $('.link-preventDefault').click(e => {
    e.preventDefault();
  });

  // Advanced serch

  $('.app-search-panel__icon-settings').click(e => {
    const toggleBlock = $('#app-toggle-block');
    $(e.currentTarget).toggleClass('app-search-panel__icon-settings--active');
    toggleBlock.toggleClass('app-toggle-block--inactive');
    const advancedStatus = toggleBlock.hasClass('app-toggle-block--inactive') ? 0 : 1;
    $('input[name="advanced"]').val(advancedStatus);
  });

  // Selectpicker

  $('.selectpicker').selectpicker({
    size: 5,
    dropupAuto: false
  });

  $('.selectpicker').on('change', e => {
    $(e.currentTarget).valid();
  });

  // Capitalize

  $('.app-capitalize').on('keyup', e => {
    const code = (e.keyCode || e.which);
    if (code === 37 || code === 38 || code === 39 || code === 40 || code === 46 || code === 8) {
      return;
    }
    const input = e.currentTarget;
    const $input = $(input);
    const start = input.selectionStart;
    const end = input.selectionEnd;
    $input.val($input.val().substr(0, 1).toUpperCase() + $input.val().substr(1));
    input.setSelectionRange(start, end);
  });

  // Fixing bugs with reload

  $('input[device="device"]').attr('autocomplete', 'off');

  $(window).bind('pageshow', event => {
    if (event.originalEvent.persisted) {
      window.location.reload();
    }
  });

  // Custom Scroll

  $('.content-account').mCustomScrollbar({
    axis: 'y',
    scrollbarPosition: 'inside',
    advanced: {autoExpandHorizontalScroll: true}
  });
  $('.notes-account-content').mCustomScrollbar({
    axis: 'y',
    scrollbarPosition: 'outside',
    advanced: {autoExpandHorizontalScroll: true}
  });

  // Adding margin for content in notes when scroll line is displayed

  function checkDisplayScroll() {
    const $scrollBlock = $('.mCSB_scrollTools_vertical');
    const $scrollLine = $('.mCSB_outside');

    if ($scrollBlock.is(':hidden')) {
      $scrollLine.css({
        'margin-right': '0'
      });
    }

    if ($scrollBlock.is(':visible')) {
      $scrollLine.css({
        'margin-right': '10px'
      });
    }
  }

  // removed click on disabled link
  $('a[disabled="disabled"]').click(() => false);

  $(window).resize(() => {
    checkDisplayScroll();
  });

  checkDisplayScroll();

  // Remove class app-display--none after load DEdit multiple InspectionsOM
  $('[data-remove]').removeClass('app-display--none');

  // Disable of browser autocomple on inputs
  $('form :input').attr('autocomplete', 'off');

  // Transfer data-action from "Add to WO" button to modal
  let addToWOButton = $('.transferAction');

  addToWOButton.on('click', e => {
    transferWoDataAction(e);
  });

  function transferWoDataAction(event) {
    let dataAction = event.target.getAttribute('data-action');
    let transferTarget = $(event.target).attr('data-target');

    $(`${transferTarget} form`).attr('action', dataAction);
  }

  // Limit a quantity of lines in textarea and add attrr 'symbols-oversized' if maxlength of line is more than need
  function limitLinesInTextArea(rows, maxlength) {
    return function() {
      let lines = $(this).val().split('\n');

      $(this).removeAttr('data-symbols-oversized');

      for (let i = 0; i < lines.length; i++) {
        if (lines[i].length > maxlength) {
          $(this).attr('data-symbols-oversized', 'true');
          break;
        }
      }

      while (lines.length > rows) {
        lines.pop();
      }

      $(this).val(lines.join('\n'));
    };
  }

  $('#company_form_billingAddress_address').on('change input propertychange', limitLinesInTextArea(3, 41));
  $('#contact_person_form_billingAddress_address').on('change input propertychange', limitLinesInTextArea(3, 41));
  $('#account_contact_person_billingAddress_address').on('change input propertychange', limitLinesInTextArea(3, 41));
  // $(document).ready(function() {
  //   $('#invoice_billing_address_address').on('change input propertychange', limitLinesInTextArea(3, 41));
  // });

  // Transfer a value of state to billing address state
  function transferStateToBillingAddress(select, val) {
    let optionsArr = $(`${select} option`);

    for (let i = 0; i < optionsArr.length; i++) {
      if (optionsArr[i].value === val) {
        $(optionsArr[i]).attr('selected', 'selected');
      } else {
        $(optionsArr[i]).attr('selected', false);
      }
    }

    $(`${select}.selectpicker`).selectpicker('val', `${val}`);
  }

  // Transfer company address to billing address
  $('#companyForm #copyToBillAddress').click(function(event) {
    event.preventDefault();

    let select = '#company_form_billingAddress_state';
    let name = $('#company_form_company_name').val();
    let address = $('#company_form_company_address_address').val();
    let city = $('#company_form_company_address_city').val();
    let stateVal = $('#company_form_company_address_state option:selected').val();
    let zip = $('#company_form_company_address_zip').val();

    $('#company_form_billingAddress_address').val(name + '\n' + address).change().blur();
    $('#company_form_billingAddress_city').val(city);
    $('#company_form_billingAddress_zip').val(zip);
    transferStateToBillingAddress(select, stateVal);
  });

  // Transfer personal address to billing address
  $('#contactForm #copyToBillAddress').click(function(event) {
    event.preventDefault();

    let select = '#contact_person_form_billingAddress_state';
    let firstName = $('#contact_person_form_contactPerson_firstName').val();
    let lastName = $('#contact_person_form_contactPerson_lastName').val();
    let address = $('#contact_person_form_contactPerson_personalAddress_address').val();
    let city = $('#contact_person_form_contactPerson_personalAddress_city').val();
    let stateVal = $('#contact_person_form_contactPerson_personalAddress_state option:selected').val();
    let zip = $('#contact_person_form_contactPerson_personalAddress_zip').val();

    $('#contact_person_form_billingAddress_address').val(firstName + ' ' + lastName + '\n' + address).change().blur();
    $('#contact_person_form_billingAddress_city').val(city);
    $('#contact_person_form_billingAddress_zip').val(zip);
    transferStateToBillingAddress(select, stateVal);
  });

  // Activate datepicker on invoice create form
  $(document).ready(function() {
    $('#invoiceCreate .datepicker').datepicker();
  });
})(jQuery);
