/* jslint browser:true*/
/* global ko*/
/* global jQuery*/

(function($) {
  'use strict';

  const getServiceFee = (...args) => [...args].reduce((a, b) => a + feeToNumber(b), 0);

  function feeToNumber(fee) {
    return Number(fee ? fee.substr(1) : 0);
  }

  function feeToString(fee) {
    return `$${Math.round(fee * 100) / 100}`;
  }

  // Service model
  function Service() {
    this.serviceFee = ko.observable();
    this.municipalityFee = ko.observable();
    this.processingFee = ko.observable();
    this.id = ko.observable();
    this.included = ko.observable();
    this.servicePrice = ko.computed(() => {
      return feeToString(getServiceFee(this.serviceFee(), this.municipalityFee(), this.processingFee()));
    }, this);

    const serviceFeeModal = $('#serviceFeeModal');
    const feeErrorBlock = $('#changeFeeError');
    const idToken = $('#access_token').attr('value');

    const submitHandler = form => {
      const feeForm = $(form);
      const buttonSubmit = feeForm.find('button[type="submit"]');
      const feeInput = feeForm.find('input');
      const newFee = feeInput.val();
      buttonSubmit.attr('disabled', 'disabled');
      $.ajax({
        url: '/api/v1/service/fee',
        dataType: 'json',
        data: {
          id: this.id(),
          value: feeToNumber(newFee)
        },
        method: 'PUT',
        headers: {
          'X-ACCESS-TOKEN': idToken
        },
        success: () => {
          feeErrorBlock.removeClass('app-submit-error').text('');
          this.serviceFee(newFee);
          serviceFeeModal.modal('hide');
          buttonSubmit.removeAttr('disabled');
          feeForm.validate().destroy();
          feeInput.val('');
        },
        error: error => {
          const errorText = error.responseJSON.message ?
            `${error.status} ${error.responseJSON.message}` :
            `${error.status} ${error.statusText}`;
          feeErrorBlock.addClass('app-submit-error').text(errorText);
          buttonSubmit.removeAttr('disabled');
        }
      });
      return false;
    };

    // Validate form for changing service fee and set new service fee
    this.setServiceFee = () => {
      feeErrorBlock.removeClass('app-submit-error').text('');
      serviceFeeModal.modal('show');
      $('#serviceFeeForm').validate({
        rules: {
          fee: {
            required: true,
            min: '$0,01'
          }
        },
        submitHandler: submitHandler,
        focusCleanup: true,
        focusInvalid: false
      });
    };
  }

  // Create workorder(WO) model
  function WorkorderViewModel() {
    this.services = ko.observableArray([
    ]);

    const servicesCount = $('.app-workorder-form table').attr('data-serviceCount');

    // Push services to WO model
    for (let i = 0; i < servicesCount; i++) {
      this.services.push(new Service());
    }

    // Sum total service fee
    this.totalServicesFee = ko.computed(() => {
      let total = this.services().filter(service => service.included() === 'true')
        .reduce((accumulator, service) => accumulator + feeToNumber(service.serviceFee()), 0);
      return feeToString(total);
    });

    // Sum all fee
    this.grandTotal = ko.computed(() => {
      let total = this.services().filter(service => service.included() === 'true')
        .reduce((accumulator, service) => accumulator + getServiceFee(service.serviceFee(), service.municipalityFee(), service.processingFee()), 0);
      return feeToString(total);
    });

    // If total fee was changed show warning
    this.showFeeWarning = ko.computed(() => {
      const initTotalFee = $('#initTotal').attr('data-initTotal');
      if (initTotalFee !== this.grandTotal()) {
        return true;
      }
      return false;
    });

    // Set init value all services fees
    ko.bindingHandlers.initValue = {
      init: (element, valueAccessor) => {
        const property = valueAccessor();
        const value = element.innerText;
        property(value);
      }
    };

    // Set init services id
    ko.bindingHandlers.initId = {
      init: (element, valueAccessor) => {
        const property = valueAccessor();
        const value = element.attributes[1].value;
        property(value);
      }
    };

    // Set init services included flag
    ko.bindingHandlers.initIncluded = {
      init: (element, valueAccessor) => {
        const property = valueAccessor();
        const value = element.attributes[2].value;
        property(value);
      }
    };
  }

  // ADDING DEVICE TO PROPOSAL
  const conditionInput = $('#condition-autocomplete');
  let dataCurrentConditions = [];

  // Get Current Conditions
  if ($.contains(document.body, document.querySelector('[data-current-conditions]'))) {
    dataCurrentConditions = $('[data-current-conditions]').data().currentConditions;
  }

  // REPAIRS
  let addingRepairsForm = null;
  if ($('#repairForm').length) {
    addingRepairsForm = $('#repairForm');
  } else if ($('#serviceToWoForm').length) {
    addingRepairsForm = $('#serviceToWoForm');
  }
  const addingRepairsInput = $('#repairs-autocomplete');
  let dataAdditionalRepairs = [];
  let checkboxesValue = $('#checkboxesValue');
  let checkboxLength = [];
  let checkboxNav = $('#repair_proposal_proposal_services_nav');
  let checkboxes = $('input[name="repair_proposal[proposal][services][]"]');
  let checkboxNavWO = $('#add_to_wo_proposal_services_nav');
  let checkboxesWO = $('input[name="repair_workorder[workorder][services][]"]');
  let isFeesVary = false;

  // Get defaultDepartmentChannelWithFeesWillVary
  if ($('#defaultDepartmentChannelWithFeesWillVary').length) {
    isFeesVary = $('#defaultDepartmentChannelWithFeesWillVary').data().departmentchannelwithfeeswillvary;
  }

  // Get dataAdditionalRepairs
  if ($.contains(document.body, document.querySelector('[data-additional-repairs]'))) {
    dataAdditionalRepairs = $('[data-additional-repairs]').data().additionalRepairs;
  }

  // MODEL
  let itemId = false;
  let itemDeficiency = false;
  let statusFee = false;
  let totalFee = 0;
  let fee;

  // Get Fee
  if ($.contains(document.body, document.querySelector('[data-fee]'))) {
    fee = $('[data-fee]').data().fee || 0;
  }

  // Get Total Fee
  if ($.contains(document.body, document.querySelector('[data-total-fee]'))) {
    totalFee = $('[data-total-fee]').data().totalFee;
  }

  // Auto complete function
  function autoComplete(selector, data) {
    selector.autocomplete({
      source: (request, response) => {
        let res = [];

        function hasMatch(string) {
          return string.toLowerCase().indexOf(request.term.toLowerCase()) !== -1;
        }

        data.map(item => {
          if (hasMatch(`${item.name} - ${item.deficiency}`)) {
            return res.push({
              label: item.name,
              value: item.id,
              deficiency: item.deficiency,
              fee: item.isNeedMunicipalityFee
            });
          }
        });
        response(res);
      },
      minLength: 0,
      select: (event, ui) => {
        selector.val(ui.item.label);
        itemId = ui.item.value;
        itemDeficiency = ui.item.deficiency;
        statusFee = ui.item.fee;
        selector.change().blur();
        return false;
      },
      focus: (event, ui) => {
        event.preventDefault();
      },
      create: function() {
        $(this).data('ui-autocomplete')._renderItem = (ul, item) => {
          return $('<li>')
            .append(`<div>${item.label} - ${item.deficiency}</div>`)
            .appendTo(ul);
        };
      }
    }).focus(e => {
      $(e.currentTarget).autocomplete('search', '');
    });
  }
  autoComplete(conditionInput, dataCurrentConditions);
  autoComplete(addingRepairsInput, dataAdditionalRepairs);

  // Adding blur on press enter key
  function blurInputs(input) {
    input.on('keyup', e => {
      if (e.keyCode === 13) {
        input.blur();
      }
    });
  }

  blurInputs(conditionInput);
  blurInputs(addingRepairsInput);

  // Selecting all checkboxes
  function selectAllCheckboxes($selector, $checkboxesArray) {
    $selector.change(e => {
      $checkboxesArray.each((index, item) => {
        $(item).prop('checked', e.currentTarget.checked);
        if ($(item).parent().parent().attr('data-ismunic') === 'true') {
          calculateCheckboxFee($(item));
        }
      });
      checkboxesValue.val(checkboxLength.length).change();
    });
  }

  selectAllCheckboxes(checkboxNav, checkboxes);
  selectAllCheckboxes(checkboxNavWO, checkboxesWO);

  // Calculate checked checkboxes
  function calculateCheckboxFee(selector) {
    checkboxLength = [];
    selector.each((index, item) => {
      if ($(item).prop('checked')) {
        checkboxLength.push(true);
      }
    });

    checkboxesValue.val(checkboxLength.length).change();
  }

  // Observe checked checkboxes on click event
  if (addingRepairsForm) {
    addingRepairsForm.on('click', 'input[type="checkbox"]', e => {
      if ($(e.target).parent().parent().attr('data-ismunic') === 'true') {
        calculateCheckboxFee($(e.target));
      }
      checkboxesValue.val(checkboxLength.length).change();
    });
  }

  // ListAItemsModel
  function ListAItemsModel(autocompleteData) {
    let searchResultInput;
    let searchResultCurrentData;
    let currentFee;
    const self = this;

    self.itemsList = ko.observableArray();
    self.checkedInputList = ko.observableArray();
    self.itemValue = ko.observable('');
    self.totalFee = ko.observable(0);
    self.totalStatus = ko.observable(0);
    self.symbol = ko.observable('');
    self.star = ko.observable('');
    self.municMessage = ko.observable('* - Municipality fees will vary');
    self.totalStar = ko.observable('');
    self.checkboxesValue = ko.observable(checkboxLength.length);
    self.visiblePlaceholderText = ko.observable();
    self.visibleTable = ko.observable();

    // Add new Item to List and Select
    self.enterSearch = () => {
      searchResultInput = self.itemsList().map(prop => prop.id).indexOf(itemId);
      searchResultCurrentData = autocompleteData.map(prop => prop.name).indexOf(self.itemValue());

      // Adding symbol of dollar
      if (statusFee) {
        if (fee) {
          currentFee = fee;
          self.symbol('$');
          self.star('');
        } else if (fee && isFeesVary) {
          currentFee = fee;
          self.totalStatus(1);
          self.star('*');
          self.totalStar('*');
          self.symbol('');
        } else if (!fee && isFeesVary) {
          currentFee = '';
          self.totalStatus(1);
          self.star('*');
          self.totalStar('*');
          self.symbol('');
        } else {
          currentFee = '-';
          self.symbol('');
          self.star('');
        }
      } else {
        currentFee = '-';
        self.symbol('');
        self.star('');
      }

      if ((searchResultInput === -1 && searchResultCurrentData !== -1) && self.itemValue()) {
        self.itemsList.unshift({
          id: itemId,
          name: self.itemValue(),
          deficiency: itemDeficiency,
          symbol: self.symbol(),
          fee: currentFee,
          star: self.star()
        });

        self.checkedInputList.unshift({
          id: itemId,
          name: self.itemValue()
        });

        if (!isNaN(parseFloat(currentFee)) || statusFee) {
          self.totalFee(self.totalFee() + fee);
        }
      }
      self.itemValue('');
    };

    // Display placeholder
    self.showPlaceholder = ko.computed(() => {
      if (self.itemsList().length < 1) {
        self.visibleTable(false);
        self.visiblePlaceholderText(true);
      } else {
        self.visibleTable(true);
        self.visiblePlaceholderText(false);
      }
    });

    // Remove item and current option
    self.removeItem = e => {
      self.checkedInputList().forEach(item => {
        if (item.id === e.id) {
          if (e.id === '242') {
            self.totalStatus(0);
          }
          self.checkedInputList.remove(item);
        }
      });

      if (!isNaN(parseFloat(e.fee))) {
        self.totalFee(self.totalFee() - fee);
      }

      self.itemsList.remove(e);
      self.itemValue('');
    };

    // Total Munic. and Proc. fee
    self.getTotalFee = ko.computed(() => {
      if (self.totalStatus() && isFeesVary) {
        return '*';
      }
      return ('$' + (self.totalFee() + totalFee + parseFloat(self.checkboxesValue()) * fee));
    });
  }

  // Main model
  let ViewModel = function() {
    this.WorkorderViewModel = new WorkorderViewModel();
    this.ListConditionsModel = new ListAItemsModel(dataCurrentConditions);
    this.ListAdditionalRepairsModel = new ListAItemsModel(dataAdditionalRepairs);
  };

  ko.applyBindings(new ViewModel());
})(jQuery);
