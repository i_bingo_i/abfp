/* jslint browser:true*/
/* global jQuery*/
/* global window*/
/* global document*/
(function($, window) {
  'use strict';

  // Filling element select in add to wo form
  let workordersByAccountArr = {};

  $('.addingOptionsInSelect').on('click', e => {
    const workordersByAccountJSON = $('.tableWithDataArray').attr('data-workorders');
    workordersByAccountArr = $.parseJSON(workordersByAccountJSON);
    addingOptionsInSelect(e);
  });

  function addingOptionsInSelect(event) {
    let inspectionCategory = event.target.getAttribute('data-inspection-category');
    let workordersArray = workordersByAccountArr[inspectionCategory];
    if (!workordersArray) {
      $('<option>', {text: 'Add to new Workorder', class: 'workorder-options', value: '0'}).appendTo('.retestOpportunitySelect select');
      $('.app-modal__form select').val('').selectpicker('refresh');
      return;
    }
    for (let i = 0; i < workordersArray.length; i++) {
      for (let k in workordersArray[i]) {
        if (workordersArray[i].hasOwnProperty(k)) {
          $('<option>', {value: k, text: workordersArray[i][k], class: 'workorder-options'}).prependTo('.retestOpportunitySelect select');
        }
      }
    }
    $('<option>', {text: 'Add to new Workorder', class: 'workorder-options', value: '0', selected: 'selected'}).prependTo('.retestOpportunitySelect select');
    $('.app-modal__form select').val('').selectpicker('refresh');
  }

  // Click on delete button on popup. Allow only 1 click.
  $('a[data-oneClick="Yes"]').one('click', e => {
    e.preventDefault();
    window.location = $(e.currentTarget).attr('href');
    $(e.currentTarget).click(e => {
      e.preventDefault();
    });
  });

  // Allow only 1 click.
  $('button[data-oneClick="Yes"], a[data-oneClick="Yes"]').one('click', e => {
    $(e.currentTarget).click();
    e.target.setAttribute('disabled', 'disabled');
  });

  // Set link in select popup
  $('.app-modal-link').click(e => {
    const target = $(e.target);
    let path = target.attr('data-link');
    let modalId = target.attr('data-target');
    if (!path) {
      let thisElement = $(e.currentTarget);
      path = thisElement.attr('data-link');
      modalId = thisElement.attr('data-target');
    }
    $(`${modalId} [data-confirm="modalYes"]`).attr('href', path);
  });

  $('.app-modal-action').click(e => {
    const target = $(e.target);
    let path = target.attr('data-link');
    let modalId = target.attr('data-target');
    $(`${modalId} form`).attr('action', path);
  });

  // Set path to current alarm
  $('.app-modal-alarm-link').click(e => {
    const target = $(e.target);
    let path = target.attr('data-link');
    let modalId = target.attr('data-target');
    if (!path) {
      let thisElement = $(e.currentTarget);
      path = thisElement.attr('data-link');
      modalId = thisElement.attr('data-target');
    }
    $(`${modalId} [data-oneclick="Yes"]`).attr('href', path);
  });

  // Delete current technician
  $('[data-delete-technician]').click(e => {
    const target = $(e.target);
    let path = target.attr('data-link');
    let modalId = target.attr('data-target');
    if (!path) {
      let thisElement = $(e.currentTarget);
      path = thisElement.attr('data-link');
      modalId = thisElement.attr('data-target');
    }
    $(`${modalId} [data-oneclick="Yes"]`).attr('href', path);
  });

  // For popup with form, don't disapear after enter click
  $(document).on('keydown keyup', e => {
    if ($('.popupForm').is(':visible')) {
      if (e.which === 13) {
        e.preventDefault();
      }
    }
  });

  // Reset validation popup form on press Cancel
  $('.modalCancel').click(e => {
    const formId = $(e.target).closest('form').attr('id');
    $(`#${formId}`).validate().resetForm();
  });

  // Refresh select on press Cancel in popup form
  $('.refreshSelect').click(() => {
    $('.app-modal__form select').val('').selectpicker('refresh');
  });

  // Clear select on press Cancel in popup form
  $('.clearSelect').click(e => {
    let formId = $(e.target).closest('form').attr('id');
    $(`#${formId} select .workorder-options`).detach();
  });

  // Clear select on misclick in popup form
  $('#addInspectionToWOFormFromList').click(e => {
    if (e.target.id !== 'addInspectionToWOFormFromList') {
      return;
    }
    let formId = $(e.target).find('form').attr('id');

    $(`#${formId} select .workorder-options`).detach();
    e.stopPropagation();
  });

  // Refresh textarea on press Cancel in popup form
  $('.refreshTextarea').click(() => {
    $('.app-modal__form textarea').val('');
  });

  // Refresh input on press Cancel in popup form
  $('.refreshInput').click(() => {
    $('.app-modal__form input').val('');
  });

  // Check showing popup depends on flag
  function checkFlagPopup(selctor, attribute, flag, popupId) {
    const condition = selctor.attr(attribute);
    if (condition === flag) {
      selctor.attr('data-target', popupId);
    }
  }

  // Check Opportunities modal
  function checkOpportunitiesModal() {
    const selector = $('#createNotice');
    checkFlagPopup(selector, 'data-valid-filter', 'true', '#createNoticeModal');
    checkFlagPopup(selector, 'data-valid-filter', 'false', '#filterDateError');
  }

  // Check Opportunities modal
  function checkProposalsModal() {
    const selector = $('#sendEmails');
    checkFlagPopup(selector, 'data-valid-filter', 'true', '#sendMailsModal');
    checkFlagPopup(selector, 'data-valid-filter', 'false', '#sendMailsError');
  }

  // Check send email error modal
  function checkSendEmailError() {
    $('.sendEmail').each((index, element) => {
      checkFlagPopup($(element), 'data-empty-email', 'true', '#sendEmailError');
    });
  }

  // Check can update service or show error modal depends on flag
  $('.checkOnUpdateService').click(event => {
    if ($(event.currentTarget).attr('data-service-canUpdate') === 'false') {
      event.preventDefault();
      $('#updateServiceError').modal('show');
    }
  });

  // Check can delete service or show error modal depends on flag
  $('.checkOnDeleteService').click(event => {
    if ($(event.currentTarget).attr('data-service-canDelete') === 'false') {
      event.preventDefault();
      $('#deleteServiceError').modal('show');
    }
  });

  checkOpportunitiesModal();
  checkProposalsModal();
  checkSendEmailError();
})(jQuery, window);
