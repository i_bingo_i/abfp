/* jslint browser:true*/
/* global jQuery*/

(function($) {
  'use strict';

  // Common object with validation settings
  const validationForm = {
    focusCleanup: true,
    focusInvalid: false
  };

  // The shortest rules on required and length
  function ruleRequired(lengthMax, lengthMin) {
    const rule = {
      required: true,
      notOnlySpace: true
    };
    if (lengthMax) {
      rule.maxlength = lengthMax;
    }
    if (lengthMin) {
      rule.minlength = lengthMin;
    }
    return rule;
  }

  // The shortest rules on email
  function ruleEmail(required) {
    const rule = {
      email: true,
      trueEmail: true,
      maxlength: 50
    };
    if (required) {
      rule.required = true;
      rule.notOnlySpace = true;
    }
    return rule;
  }

  // Sabmited form and disabled button
  function sendForm(form, event) {
    $('.app-submit-error').removeClass('app-submit-error').html('');
    $(form).find('button[type="submit"]').attr('disabled', 'disabled');
    form.submit();
    event.preventDefault();
  }

  // Adding validation to login form
  $('#loginForm').validate($.extend({}, {
    rules: {
      _username: ruleEmail(true),
      _password: ruleRequired(undefined, 6)
    },
    submitHandler: (form, event) => {
      sendForm(form, event);
    }
  }, validationForm));

  jQuery.validator.addMethod('notOnlySpace', value => {
    return (!(/^\s*$/.test(value)) || value.length === 0);
  }, 'String contains only spaces');

  jQuery.validator.addMethod('trueEmail', value => {
    return !value || (/^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/
      .test(value)
    );
  }, 'Please enter a valid email address.');
})(jQuery);
