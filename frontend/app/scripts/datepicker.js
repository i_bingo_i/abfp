/* jslint browser:true*/
/* global jQuery*/
/* global window*/

(function($) {
  'use strict';

  const dateFormat = 'mm/dd/yy';
  const clearValue = $('.clear-input-value');
  const fromDate = $('.fromDates').datepicker(datepickerSettings());
  const toDate = $('.toDates').datepicker(datepickerSettings());
  const fromCreateDate = $('.fromCreateDates').datepicker(datepickerSettings());
  const toCreateDate = $('.toCreateDates').datepicker(datepickerSettings());

  // Return datepicker object settings
  function datepickerSettings() {
    const settings = {
      changeMonth: true,
      changeYear: true,
      numberOfMonths: 1,
      dateFormat: dateFormat
    };
    if ($('.minToday').length) {
      return Object.assign({}, settings, {minDate: 0});
    }
    return settings;
  }

  // Parse date
  function getDate(element) {
    let date;
    try {
      date = $.datepicker.parseDate(dateFormat, element.value);
    } catch (error) {
      date = null;
    }
    return date;
  }

  // Control datepicker validation classes (borders)
  function checkOnInputValid(element) {
    if (element.valid()) {
      element.removeClass('invalid').addClass('success');
    }
  }

  // Control datepicker validation error text
  function checkOnFormValid() {
    if ($('.datepickerCheckValid').valid()) {
      $('label.error').remove();
    }
  }

  function datePicikerConfig(from, to) {
    from.on('change', e => {
      clearValue.hide();
      to.datepicker('option', 'minDate', getDate(e.currentTarget));
      checkOnInputValid(from);
      if (!from.attr('required')) {
        checkOnInputValid(to);
      }
    });

    to.on('change', e => {
      clearValue.hide();
      from.datepicker('option', 'maxDate', getDate(e.currentTarget));
      checkOnInputValid(to);
      if (!to.attr('required')) {
        checkOnInputValid(from);
      }
    });

    // Show clear icon
    function onDateFocus(e) {
      const $this = $(e.currentTarget);
      if ($this.val()) {
        $this.siblings('.clear-input-value').show();
      }
    }

    from.focus(e => onDateFocus(e));

    to.focus(e => onDateFocus(e));

    // Set true position on mobile
    function setDatepickerPosition($input, e) {
      $('#ui-datepicker-div').css({
        left: $(e.currentTarget).offset().left + 'px'
      });
      $input.siblings('.clear-input-value').hide();
    }

    $(from).on('focus', e => {
      setDatepickerPosition($('.toDates'), e);
    });

    $(to).on('focus', e => {
      setDatepickerPosition($('.fromDates'), e);
    });

    $(from).on('focus', e => {
      setDatepickerPosition($('.toCreateDates'), e);
    });

    $(to).on('focus', e => {
      setDatepickerPosition($('.fromCreateDates'), e);
    });

    // Hide clear icon on different click
    $('body').on('click', e => {
      const target = $(e.target);
      if ((target.className !== 'clear-input-value' &&
        !target.hasClass('fromDates') &&
        !target.hasClass('toDates')) &&
      (target.className !== 'clear-input-value' &&
        !target.hasClass('fromCreateDates') &&
        !target.hasClass('toCreateDates'))) {
        clearValue.hide();
      }
    });

    // Reset datepicker settings after clear input
    function clearSettings() {
      if ($('.minToday').length) {
        return {minDate: 0,
          maxDate: null};
      }
      return {minDate: null,
        maxDate: null};
    }

    // Clear datepicker input value on click clear icon
    clearValue.on('click', e => {
      const $this = $(e.currentTarget);
      const input = $this.siblings('.hasDatepicker');
      from.datepicker('option', clearSettings());
      to.datepicker('option', clearSettings());
      input.val('');
      $this.hide();
      checkOnInputValid(input);
      checkOnFormValid();
    });

    // Hide datepicker for true position
    $(window).on('resize orientationchange', () => {
      $('.hasDatepicker').datepicker('hide').blur();
      $('.clear-input-value').hide();
    });
  }

  datePicikerConfig(fromDate, toDate);
  datePicikerConfig(fromCreateDate, toCreateDate);
})(jQuery);
