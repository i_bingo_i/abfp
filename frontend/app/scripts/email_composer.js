/* jslint browser:true*/
/* global jQuery*/
/* global InstallTrigger */
/* global window*/

(function($) {
  let fileIdCounter = 1;
  let totalCheckboxSize = 0;
  let totalFileSize = 0;
  $(function() {
    'use strict';

    calculateCheckboxesFileSize();
    $('input.app-checkbox-input').on('change', function() {
      calculateCheckboxesFileSize();
    });

    $('#formWrapperForInsert').on('click', '.multiBrowseFile', e => {
      e.stopPropagation();
      e.preventDefault();
      const targegMultiBrowseFile = $(e.currentTarget);
      const targetField = targegMultiBrowseFile.siblings('input.multiInputFile');
      targetField.trigger('click');
    });

    $('#formWrapperForInsert').on('change', '.multiInputFile', e => {
      const thisInput = e.currentTarget;
      const $thisInput = $(thisInput);
      const ext = $thisInput.val().split('.').pop().toLowerCase();
      const error = $thisInput.parent().siblings('.fileError');
      const innerContanier = $thisInput.closest('.multiInnerContanier');
      const fileType = $thisInput.attr('data-type');

      if (thisInput.files.length !== 0 || thisInput.val() !== '') {
        error.removeClass('app-submit-error app-device-form__img-error').text('');
      }
      if (thisInput.files[0].size > 26000000) {
        $thisInput.val('');
        error.addClass('app-submit-error app-device-form__img-error').text('The file is too large. Allowed maximum size is 25Mb');
      } else if (validateFileType(ext, fileType)) {
        $thisInput.val('');
        error.addClass('app-submit-error app-device-form__img-error').text(`Please use ${fileType}`);
      } else {
        const size = parseFloat(($thisInput[0].files[0].size / (1024 * 1024)).toFixed(2));
        innerContanier.addClass('drag-and-drop-filled');
        $thisInput.siblings('.multiBrowseFile').hide();
        $thisInput.siblings('.multiPdfFile').removeClass('app-display--none');
        $thisInput.siblings('.multiDragandropPlaceholder').hide();
        $thisInput.siblings('.multiFileName').text($thisInput[0].files[0].name);
        $thisInput.siblings('.multiRemoveFile').show();
        $thisInput.siblings('.multiFileSize').html('(' + size + ' Mb)');
        createNewField();
        initDropboxes();
        calculateFilesSize();
      }
    });
    $('#formWrapperForInsert').on('click', '.multiRemoveFile', e => {
      const target = $(e.currentTarget);
      target.closest('.multiDragandropContainer').remove();
      calculateFilesSize();
    });

    initDropboxes();
    // Forbiding to send Custom Email if no files attached
    $('.checkSendError').click(event => {
      if (totalFileSize + totalCheckboxSize === 0) {
        event.preventDefault();
        $('#sendCustomEmailError').modal('show');
      }
      if (totalFileSize + totalCheckboxSize > 25) {
        event.preventDefault();
        $('#sendCustomEmailErrorForBigForm').modal('show');
      }
    });
  });
  function createNewField() {
    const pdfPath = '/bundles/admin/dist/images/pdf.jpeg';
    const additionalBlock = '<div class="form-group app-text--align-left multiDragandropContainer" id="multiDragandDropContainer' + fileIdCounter + '">\n' +
        '                    <div class="app-dragandrop app-multi-dragandrop multiInnerContanier">\n' +
        '                        <div class="app-dragandrop__placeholder multiDragandropPlaceholder" id="multiDragandropPlaceholder' + fileIdCounter + '">Browse or Drag and Drop file here</div>\n' +
        '                        <a class="app-link__select multiBrowseFile" id="multiBrowseFile' + fileIdCounter + '" data-original-title="" title="">Browse file</a>\n' +
        '                        <div class="multiFileSize"></div>' +
        '                        <div class="multiFileName app-word-breack text-center"></div>\n' +
        '                        <img src="' + pdfPath + '" class="multiPdfFile app-form__img--pdf app-small-pdf app-display--none" alt="pdf logo">' +
        '                        <a class="app-link__select multiRemoveFile" id="multiRemoveFile' + fileIdCounter + '" style="display: none" data-original-title="" title=""><img alt="image delete" src="/bundles/admin/dist/images/delete.svg"></a>\n' +
        '                        <input type="file" id="admin_bundle_file_file' + fileIdCounter + '" name="admin_bundle_file[file][]" multiple="multiple" class="start-file-field multiInputFile" style="visibility:hidden;height:0" data-type="PDF DOC file or Image" autocomplete="off">\n' +
        '                    </div>\n' +
        '                    <div class="fileError" id="multiFileError' + fileIdCounter + '"></div>\n' +
        '                </div>';
    fileIdCounter++;
    $('#formWrapperForInsert').append(additionalBlock);
  }

  // function removeField() {
  //   $('#formWrapperForInsert .multiDragandropContainer:last-child').remove();
  //   calculateFilesSize();
  // }

  // function clearContent(inputFile, fileName, removeFile, dragandropPlaceholder, browseFile) {
  //   inputFile.val('');
  //   fileName.text('');
  //   removeFile.hide();
  //   dragandropPlaceholder.show();
  //   browseFile.show();
  // }

  function addEvent(element, eventName, callback) {
    if (element && element.addEventListener) {
      element.addEventListener(eventName, callback, false);
    } else if (element && element.attachEvent) {
      element.attachEvent('on' + eventName, callback);
    }
  }

  function dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  function dragover(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  function drop(e) {
    e.stopPropagation();
    e.preventDefault();
    const currentInput = $(e.currentTarget).children('.multiInputFile');
    const dt = e.dataTransfer;
    const files = dt.files;
    if (currentInput[0].files.length === 0 && files.length === 1) {
      currentInput[0].files = files;
      if (typeof InstallTrigger !== 'undefined') {
        currentInput.trigger('change');
      }
      return false;
    }
    return false;
  }

  function initDropboxes() {
    const dropboxes = document.getElementsByClassName('app-dragandrop');
    Array.prototype.forEach.call(dropboxes, dropbox => {
      addEvent(dropbox, 'dragenter', dragenter);
      addEvent(dropbox, 'dragover', dragover);
      addEvent(dropbox, 'drop', drop);
    });
  }

  function calculateCheckboxesFileSize() {
    totalCheckboxSize = 0;
    $('.attached_file_size').each((index, element) => {
      if ($(element).prop('checked') === true) {
        totalCheckboxSize += parseFloat($(element).data('size'));
      }
    });
    $('.total_files_size').html((totalCheckboxSize + totalFileSize).toFixed(2) + ' Mb');
  }

  function calculateFilesSize() {
    totalFileSize = 0;
    $('input.multiInputFile').each((index, element) => {
      const thisInput = $(element);
      if (thisInput[0].files.length !== 0) {
        const size = parseFloat((thisInput[0].files[0].size / (1024 * 1024)).toFixed(2));
        totalFileSize += size;
      }
    });
    $('.total_files_size').html((totalCheckboxSize + totalFileSize).toFixed(2) + ' Mb');
  }

  function validateFileType(ext, fileType) {
    switch (fileType) {
    case 'PDF file': {
      return $.inArray(ext, ['pdf']) === -1;
    }
    case 'DOC file': {
      return $.inArray(ext, ['doc', 'docx']) === -1;
    }
    case 'PDF or DOC file': {
      return $.inArray(ext, ['doc', 'docx', 'pdf']) === -1;
    }
    case 'PDF file or Image': {
      return $.inArray(ext, ['jpg', 'jpeg', 'png', 'pdf', '']) === -1;
    }
    case 'PDF DOC file or Image': {
      return $.inArray(ext, ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', '']) === -1;
    }
    default: {
      return false;
    }
    }
  }
})(jQuery);
