const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const WebpackMessages = require('webpack-messages');

const conf = {
  entry: './app/scripts/app.js',
  output: {
    path: path.resolve(__dirname, '../src/AdminBundle/Resources/public/dist/scripts/'),
    publicPath: '',
    filename: 'main.js'
  },
  devtool: 'source-map',
  watch: true,
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        exclude: '/node_modules/',
        use: [
          {
            loader: 'babel-loader'
          },
          {
            loader: 'eslint-loader',
            options: {
              emitError: true,
              failOnError: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        exclude: '/node_modules/',
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  autoprefixer({
                    browsers: ['ie >= 10',
                      'ie_mob >= 10',
                      'ff >= 30',
                      'chrome >= 34',
                      'safari >= 7',
                      'opera >= 23',
                      'ios >= 7',
                      'android >= 4.4',
                      'bb >= 10']
                  })
                ],
                sourceMap: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '../images/[name].[ext]',
              outputPath: '../images/'
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
              disable: true
            }
          }
        ]
      },
      { test: /\.(woff2?)$/, use: 'url-loader?limit=10000&name=../fonts/[name].[ext]' },
      { test: /\.(ttf|eot)$/, use: 'file-loader?name=../fonts/[name].[ext]' }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new ExtractTextPlugin('../styles/main.css'),
    new CopyWebpackPlugin([{
      from: './app/images/',
      to: '../images'
    }]),
    new WebpackMessages({
      name: 'client',
      logger: str => console.log(`>> ${str}`)
    })
  ]
}

module.exports = (env, argv) => {
  if (argv.mode === 'production') {
    conf.devtool = '';
    conf.watch = false;
  }
  return conf;
};