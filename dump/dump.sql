-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: erp
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `type` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `client_type_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `company` int(11) DEFAULT NULL,
  `municipality` int(11) DEFAULT NULL,
  `oldest_opportunity_date` date DEFAULT NULL,
  `oldest_opportunity_backflow_date` date DEFAULT NULL,
  `oldest_opportunity_fire_date` date DEFAULT NULL,
  `oldest_opportunity_plumbing_date` date DEFAULT NULL,
  `oldest_opportunity_alarm_date` date DEFAULT NULL,
  `oldest_proposal_date` date DEFAULT NULL,
  `oldest_proposal_backflow_date` date DEFAULT NULL,
  `oldest_proposal_fire_date` date DEFAULT NULL,
  `oldest_proposal_plumbing_date` date DEFAULT NULL,
  `oldest_proposal_alarm_date` date DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_system_mas_level_address_id` int(11) DEFAULT NULL,
  `old_account_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `building_type` int(11) DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `is_messages_processed` tinyint(1) DEFAULT '0',
  `old_link_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_send_to` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coordinate_id` int(11) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `special_discount` tinyint(1) NOT NULL DEFAULT '0',
  `comment` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7D3656A4F5B7AF75` (`address_id`),
  UNIQUE KEY `UNIQ_7D3656A4E5DE27B0` (`old_system_mas_level_address_id`),
  KEY `IDX_7D3656A48CDE5729` (`type`),
  KEY `IDX_7D3656A4727ACA70` (`parent_id`),
  KEY `IDX_7D3656A49771C8EE` (`client_type_id`),
  KEY `IDX_7D3656A44FBF094F` (`company`),
  KEY `IDX_7D3656A4C6F56628` (`municipality`),
  KEY `IDX_7D3656A42AFA207D` (`building_type`),
  KEY `IDX_7D3656A498BBE953` (`coordinate_id`),
  KEY `IDX_7D3656A417653B16` (`payment_term_id`),
  CONSTRAINT `FK_7D3656A417653B16` FOREIGN KEY (`payment_term_id`) REFERENCES `payment_term` (`id`),
  CONSTRAINT `FK_7D3656A42AFA207D` FOREIGN KEY (`building_type`) REFERENCES `account_building_type` (`id`),
  CONSTRAINT `FK_7D3656A44FBF094F` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_7D3656A4727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_7D3656A48CDE5729` FOREIGN KEY (`type`) REFERENCES `account_type` (`id`),
  CONSTRAINT `FK_7D3656A49771C8EE` FOREIGN KEY (`client_type_id`) REFERENCES `client_type` (`id`),
  CONSTRAINT `FK_7D3656A498BBE953` FOREIGN KEY (`coordinate_id`) REFERENCES `coordinate` (`id`),
  CONSTRAINT `FK_7D3656A4C6F56628` FOREIGN KEY (`municipality`) REFERENCES `municipality` (`id`),
  CONSTRAINT `FK_7D3656A4E5DE27B0` FOREIGN KEY (`old_system_mas_level_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_7D3656A4F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'Account one','Note: test','2018-05-25 05:08:03','2018-05-25 05:08:03',1,17,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(2,'Group account one','Note: test','2018-05-25 05:08:03','2018-05-25 05:08:03',2,18,NULL,2,0,NULL,2,'2018-04-25','2018-07-25','2018-04-25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(3,'Account two','Note: test','2018-05-25 05:08:03','2018-05-25 05:08:03',1,19,2,2,0,NULL,2,'2018-06-25',NULL,'2018-06-25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(4,'Account three','Note: test','2018-05-25 05:08:03','2018-05-25 05:08:03',1,20,2,2,0,NULL,2,'2018-04-25','2018-07-25','2018-04-25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(5,'Account five','Some notes','2018-05-25 05:08:03','2018-05-25 05:08:03',2,21,NULL,3,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(6,'McDonalds','','2018-05-25 05:08:03','2018-05-25 05:08:03',2,22,NULL,2,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'http://corporate.mcdonalds.com/mcd.html',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(7,'/\\ccount##1','','2018-05-25 05:08:03','2018-05-25 05:08:03',1,23,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(8,'Account six','qwerty!@#$&*()_','2018-05-25 05:08:03','2018-05-25 05:08:03',1,24,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(9,'Account seven','Illinois','2018-05-25 05:08:03','2018-05-25 05:42:31',1,25,10,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,'2001-01-01','2001-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,1,1,NULL),(10,'Test','2342342','2018-05-25 05:08:03','2018-05-25 05:42:31',2,26,NULL,2,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'2001-01-01',NULL,'2001-01-01',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(11,'Account 10','Test Note _ ( ) ^ dfmsisugvj dewqjk ehferwh hfekr		','2018-05-25 05:08:03','2018-05-25 05:08:03',2,27,NULL,2,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(12,'Account frue89','qwerty!@#$&*()_','2018-05-25 05:08:03','2018-05-25 05:08:03',2,28,NULL,1,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(13,'Account nine','Illinois','2018-05-25 05:08:03','2018-05-25 05:08:03',1,29,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(14,'Acc','defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ','2018-05-25 05:08:03','2018-05-25 05:49:42',1,30,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,'2001-01-01','2001-01-01','2001-01-01',NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(15,'BFH','defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ','2018-05-25 05:08:03','2018-05-25 05:08:03',1,31,NULL,1,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(16,'Deleted Account','Some notes for account some notes for account some notes for account some notes for account','2018-05-25 05:08:03','2018-05-25 05:08:03',1,32,NULL,1,1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(17,'Another Deleted Account','Some notes for account some notes for account some notes for account some notes for account','2018-05-25 05:08:03','2018-05-25 05:08:03',2,33,NULL,1,1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL),(18,'Account for changing ContactPersonRoles testing','account for changing contact person roles testing','2018-05-25 05:08:03','2018-05-25 05:08:03',1,34,NULL,1,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_building_type`
--

DROP TABLE IF EXISTS `account_building_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_building_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_building_type`
--

LOCK TABLES `account_building_type` WRITE;
/*!40000 ALTER TABLE `account_building_type` DISABLE KEYS */;
INSERT INTO `account_building_type` VALUES (1,'Residential','residential','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,'Commercial','commercial','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `account_building_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_contact_person`
--

DROP TABLE IF EXISTS `account_contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_person` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `authorizer` tinyint(1) DEFAULT '0',
  `access` tinyint(1) DEFAULT '0',
  `access_primary` tinyint(1) DEFAULT '0',
  `payment` tinyint(1) DEFAULT '0',
  `payment_primary` tinyint(1) DEFAULT '0',
  `sending_address_id` int(11) DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `notes` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_F6B922FDA44EE6F7` (`contact_person`),
  KEY `IDX_F6B922FD7D3656A4` (`account`),
  KEY `IDX_F6B922FD6510ABA8` (`sending_address_id`),
  CONSTRAINT `FK_F6B922FD6510ABA8` FOREIGN KEY (`sending_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_F6B922FD7D3656A4` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_F6B922FDA44EE6F7` FOREIGN KEY (`contact_person`) REFERENCES `contact_person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_contact_person`
--

LOCK TABLES `account_contact_person` WRITE;
/*!40000 ALTER TABLE `account_contact_person` DISABLE KEYS */;
INSERT INTO `account_contact_person` VALUES (1,5,14,0,'2018-05-25 05:25:16','2018-05-25 05:25:16',1,1,1,1,1,30,0,NULL),(2,5,10,0,'2018-05-25 05:35:46','2018-05-25 05:35:46',1,1,1,1,1,7,0,NULL),(3,4,9,0,'2018-05-25 05:42:20','2018-05-25 05:42:20',1,1,1,1,1,25,0,NULL);
/*!40000 ALTER TABLE `account_contact_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_contact_person_history`
--

DROP TABLE IF EXISTS `account_contact_person_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_contact_person_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `contact_person` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `authorizer` tinyint(1) DEFAULT '0',
  `access` tinyint(1) DEFAULT '0',
  `access_primary` tinyint(1) DEFAULT '0',
  `payment` tinyint(1) DEFAULT '0',
  `payment_primary` tinyint(1) DEFAULT '0',
  `sending_address_id` int(11) DEFAULT NULL,
  `sending_address_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_950F5F58835E0EEE` (`owner_entity_id`),
  KEY `IDX_950F5F58BDAFD8C8` (`author`),
  KEY `IDX_950F5F58A44EE6F7` (`contact_person`),
  KEY `IDX_950F5F587D3656A4` (`account`),
  KEY `IDX_950F5F586510ABA8` (`sending_address_id`),
  CONSTRAINT `FK_950F5F586510ABA8` FOREIGN KEY (`sending_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_950F5F587D3656A4` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_950F5F58835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_950F5F58A44EE6F7` FOREIGN KEY (`contact_person`) REFERENCES `contact_person_history` (`id`),
  CONSTRAINT `FK_950F5F58BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_contact_person_history`
--

LOCK TABLES `account_contact_person_history` WRITE;
/*!40000 ALTER TABLE `account_contact_person_history` DISABLE KEYS */;
INSERT INTO `account_contact_person_history` VALUES (1,1,22,5,14,0,'2018-05-25 05:25:16','2018-05-25 05:25:16',1,1,1,1,1,30,NULL,NULL),(2,2,22,5,10,0,'2018-05-25 05:35:46','2018-05-25 05:35:46',1,1,1,1,1,7,NULL,NULL),(3,3,22,4,9,0,'2018-05-25 05:42:20','2018-05-25 05:42:20',1,1,1,1,1,25,NULL,NULL);
/*!40000 ALTER TABLE `account_contact_person_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_contact_persons_responsibilities`
--

DROP TABLE IF EXISTS `account_contact_persons_responsibilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_contact_persons_responsibilities` (
  `account_contact_person_id` int(11) NOT NULL,
  `device_category_id` int(11) NOT NULL,
  PRIMARY KEY (`account_contact_person_id`,`device_category_id`),
  KEY `IDX_199EC2CECE52684D` (`account_contact_person_id`),
  KEY `IDX_199EC2CE60C6C924` (`device_category_id`),
  CONSTRAINT `FK_199EC2CE60C6C924` FOREIGN KEY (`device_category_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_199EC2CECE52684D` FOREIGN KEY (`account_contact_person_id`) REFERENCES `account_contact_person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_contact_persons_responsibilities`
--

LOCK TABLES `account_contact_persons_responsibilities` WRITE;
/*!40000 ALTER TABLE `account_contact_persons_responsibilities` DISABLE KEYS */;
INSERT INTO `account_contact_persons_responsibilities` VALUES (1,1),(1,2),(1,3),(1,4),(2,1),(2,2),(2,3),(2,4),(3,1);
/*!40000 ALTER TABLE `account_contact_persons_responsibilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_history`
--

DROP TABLE IF EXISTS `account_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `client_type_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `date_save` datetime NOT NULL,
  `company` int(11) DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address_id` int(11) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `special_discount` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_EE9164038CDE5729` (`type`),
  KEY `IDX_EE916403727ACA70` (`parent_id`),
  KEY `IDX_EE9164039771C8EE` (`client_type_id`),
  KEY `IDX_EE916403835E0EEE` (`owner_entity_id`),
  KEY `IDX_EE916403BDAFD8C8` (`author`),
  KEY `IDX_EE9164034FBF094F` (`company`),
  KEY `IDX_EE916403F5B7AF75` (`address_id`),
  KEY `IDX_EE91640379D0C0E4` (`billing_address_id`),
  KEY `IDX_EE91640317653B16` (`payment_term_id`),
  CONSTRAINT `FK_EE91640317653B16` FOREIGN KEY (`payment_term_id`) REFERENCES `payment_term` (`id`),
  CONSTRAINT `FK_EE9164034FBF094F` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_EE916403727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_EE91640379D0C0E4` FOREIGN KEY (`billing_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_EE916403835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_EE9164038CDE5729` FOREIGN KEY (`type`) REFERENCES `account_type` (`id`),
  CONSTRAINT `FK_EE9164039771C8EE` FOREIGN KEY (`client_type_id`) REFERENCES `client_type` (`id`),
  CONSTRAINT `FK_EE916403BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_EE916403F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_history`
--

LOCK TABLES `account_history` WRITE;
/*!40000 ALTER TABLE `account_history` DISABLE KEYS */;
INSERT INTO `account_history` VALUES (1,1,17,NULL,1,1,NULL,'Account one','Note: test','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(2,2,18,NULL,2,2,NULL,'Group account one','Note: test','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(3,1,19,2,2,3,NULL,'Account two','Note: test','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(4,1,20,2,2,4,NULL,'Account three','Note: test','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(5,2,21,NULL,3,5,NULL,'Account five','Some notes','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(6,2,22,NULL,2,6,NULL,'McDonalds','','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,'http://corporate.mcdonalds.com/mcd.html',NULL,NULL,0),(7,1,23,NULL,1,7,NULL,'/\\ccount##1','','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(8,1,24,NULL,1,8,NULL,'Account six','qwerty!@#$&*()_','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(9,1,25,NULL,1,9,NULL,'Account seven','Illinois','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(10,2,26,NULL,2,10,NULL,'Test','2342342','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(11,2,27,NULL,2,11,NULL,'Account 10','Test Note _ ( ) ^ dfmsisugvj dewqjk ehferwh hfekr		','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(12,2,28,NULL,1,12,NULL,'Account frue89','qwerty!@#$&*()_','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(13,1,29,NULL,1,13,NULL,'Account nine','Illinois','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(14,1,30,NULL,1,14,NULL,'Acc','defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(15,1,31,NULL,1,15,NULL,'BFH','defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(16,1,32,NULL,1,16,NULL,'Deleted Account','Some notes for account some notes for account some notes for account some notes for account','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(17,2,33,NULL,1,17,NULL,'Another Deleted Account','Some notes for account some notes for account some notes for account some notes for account','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(18,1,34,NULL,1,18,NULL,'Account for changing ContactPersonRoles testing','account for changing contact person roles testing','2018-05-25 05:08:03','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,NULL,NULL,0),(19,1,25,10,1,9,22,'Account seven','Illinois','2018-05-25 05:08:03','2018-05-25 05:35:21','2018-05-25 05:35:21',NULL,NULL,NULL,NULL,0),(20,1,25,10,1,9,22,'Account seven','Illinois','2018-05-25 05:08:03','2018-05-25 05:35:21','2018-05-25 05:35:21',NULL,NULL,NULL,NULL,0),(21,1,25,10,1,9,22,'Account seven','Illinois','2018-05-25 05:08:03','2018-05-25 05:38:26','2018-05-25 05:38:26',NULL,NULL,NULL,1,1);
/*!40000 ALTER TABLE `account_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_type`
--

DROP TABLE IF EXISTS `account_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_type`
--

LOCK TABLES `account_type` WRITE;
/*!40000 ALTER TABLE `account_type` DISABLE KEYS */;
INSERT INTO `account_type` VALUES (1,'Account','account','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,'Group account','group account','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `account_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_messages`
--

DROP TABLE IF EXISTS `accounts_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_messages` (
  `account_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  PRIMARY KEY (`account_id`,`message_id`),
  KEY `IDX_82D960F39B6B5FBA` (`account_id`),
  KEY `IDX_82D960F3537A1329` (`message_id`),
  CONSTRAINT `FK_82D960F3537A1329` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  CONSTRAINT `FK_82D960F39B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_messages`
--

LOCK TABLES `accounts_messages` WRITE;
/*!40000 ALTER TABLE `accounts_messages` DISABLE KEYS */;
INSERT INTO `accounts_messages` VALUES (2,1),(9,4);
/*!40000 ALTER TABLE `accounts_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `address_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D4E6F815D83CC1` (`state_id`),
  KEY `IDX_D4E6F819EA97B0B` (`address_type_id`),
  CONSTRAINT `FK_D4E6F815D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_D4E6F819EA97B0B` FOREIGN KEY (`address_type_id`) REFERENCES `address_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,32,'Old system address 111','New York','23345','2018-05-25 05:08:03','2018-05-25 05:08:03',7),(2,32,'Some address for addresses street One building 1','Kiev','0005','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(3,32,'Some address for addresses street One building 1','Kiev','0005','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(4,32,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(5,2,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(6,37,'Street and Building and other info','Lviv','121212','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(7,31,'Just info about address','Paris','54454','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(8,28,'fdfdseeewefafsdfsdfs sfsdfsdfv sefef34 dddd 33','Kiev','567890','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(9,2,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(10,2,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(11,2,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(12,2,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(13,2,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(14,2,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(15,13,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(16,13,NULL,NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',1),(17,21,'Test address','Boston','09000','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(18,21,'some Test address','Boston','09000','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(19,21,'Some address','Boston','09000','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(20,21,'Test address for account','New York','09000','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(21,1,'Test address for account','Kiev','354792','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(22,1,'some test address lines','Odessa','354792','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(23,13,'test address','some city','0','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(24,5,'test address','City','999999','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(25,2,'Str # 1234!','Some city','111111','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(26,1,'efuwygierwhfiheiwh','City','324234','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(27,1,'Tets Ndegw','defwikoiojgip90e','3424234','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(28,5,'Tets Ndegw','City','999999','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(29,2,'Str # 1234!','Some city fhs','111111','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(30,5,'Some address','City of fdskh','999999','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(31,5,'Str Alefwhfefrrnbndfv','Some city','111111','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(32,2,'Some address info for deleted account 11111','Kiev','121212','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(33,2,'Some address info for deleted account 22222','Lviv','3333333','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(34,1,'17A, Kharkivske Way','Kyiv','01999','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(35,6,'Some address for Company One street One building 1','Kiev','00005','2018-05-25 05:08:03','2018-05-25 05:08:03',4),(36,32,'Another address for Company Two street  street 1','New York','232323','2018-05-25 05:08:03','2018-05-25 05:08:03',4),(37,17,'Another address for Company Three','Lviv','222222','2018-05-25 05:08:03','2018-05-25 05:08:03',4),(38,29,'12 address for Company Four','Odessa','688900','2018-05-25 05:08:03','2018-05-25 05:08:03',4),(39,29,'Lyskivska, str. 7-a, app, 149','Berezan','1991','2018-05-25 05:08:03','2018-05-25 05:08:03',4),(40,13,'1540 N Old Rand Rd','Chicago','60084','2018-05-25 05:08:03','2018-05-25 05:08:03',5),(41,13,'600 East Broadway','Chicago','65201','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(42,13,'601 East Broadway','Chicago','65201','2018-05-25 05:08:03','2018-05-25 05:08:03',5),(43,13,'602 East Broadway','Chicago','65201','2018-05-25 05:08:03','2018-05-25 05:08:03',5),(44,13,'36 N. Apple','Chicago','60052','2018-05-25 05:08:03','2018-05-25 05:08:03',5),(45,13,'36 N. Apple','Chicago','60052','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(46,13,'36 N. Apple','Chicago','60052','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(47,13,'36 N. Apple','Chicago','60052','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(48,13,'Street 1','Chicago','1234','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(49,13,'Street 1','Chicago','1234','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(50,13,'Street 2','Chicago','1234','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(51,13,'Street 3','Chicago','1234','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(52,13,'Street 4','Chicago','1234','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(53,13,'Street 1','Lviv','222222','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(54,2,'Street or no street 23','Kiev','23456','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(55,32,'Build','Dnepr','88998','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(56,5,'Some address','City of fdskh','999999','2018-05-25 05:26:48','2018-05-25 05:26:48',2),(57,5,'Some address','City of fdskh','999999','2018-05-25 05:29:20','2018-05-25 05:29:20',2),(58,31,'Just info about address','Paris','54454','2018-05-25 05:38:40','2018-05-25 05:38:40',1),(59,2,'Str # 1234!','Some city','111111','2018-05-25 05:42:31','2018-05-25 05:42:31',2),(60,2,'Str # 1234!','Some city','111111','2018-05-25 05:42:39','2018-05-25 05:42:39',2),(61,2,'Str # 1234!','Some city','111111','2018-05-25 05:42:51','2018-05-25 05:42:51',2),(62,31,'Just info about address','Paris','54454','2018-05-25 05:44:02','2018-05-25 05:44:02',1),(63,31,'Just info about address','Paris','54454','2018-05-25 05:44:12','2018-05-25 05:44:12',1),(64,5,'Some address','City of fdskh','999999','2018-05-25 05:47:00','2018-05-25 05:47:00',2),(65,5,'Some address','City of fdskh','999999','2018-05-25 05:47:00','2018-05-25 05:47:00',2),(66,5,'Some address','City of fdskh','999999','2018-05-25 08:51:51','2018-05-25 08:51:51',2),(67,5,'Some address','City of fdskh','999999','2018-05-25 08:52:01','2018-05-25 08:52:01',2),(68,5,'Some address','City of fdskh','999999','2018-05-25 08:53:38','2018-05-25 08:53:38',2),(69,5,'Some address','City of fdskh','999999','2018-05-25 08:53:51','2018-05-25 08:53:51',2);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_type`
--

DROP TABLE IF EXISTS `address_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_type`
--

LOCK TABLES `address_type` WRITE;
/*!40000 ALTER TABLE `address_type` DISABLE KEYS */;
INSERT INTO `address_type` VALUES (1,'Personal Address','personal','2018-05-25 05:07:41','2018-05-25 05:07:41',3),(2,'Site Address','site address','2018-05-25 05:07:41','2018-05-25 05:07:41',2),(3,'Group Account Address','group account address','2018-05-25 05:07:41','2018-05-25 05:07:41',2),(4,'Company Address','company address','2018-05-25 05:07:41','2018-05-25 05:07:41',1),(5,'Contractor Address','contractor address','2018-05-25 05:07:41','2018-05-25 05:07:41',4),(6,'Contact Address','contact address','2018-05-25 05:07:41','2018-05-25 05:07:41',5),(7,'Old System Master Address','old system master address','2018-05-25 05:07:41','2018-05-25 05:07:41',6),(8,'Municipality Address','municipality','2018-05-25 05:07:41','2018-05-25 05:07:41',5);
/*!40000 ALTER TABLE `address_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent`
--

LOCK TABLES `agent` WRITE;
/*!40000 ALTER TABLE `agent` DISABLE KEYS */;
INSERT INTO `agent` VALUES (1,'Apple','http://www.google.com','1111111111'),(2,'Bell Labs','http://www.google.com','3112111111'),(3,'AT&T','https://www.att.com/','4113111111');
/*!40000 ALTER TABLE `agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_channel`
--

DROP TABLE IF EXISTS `agent_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named` int(11) DEFAULT NULL,
  `agent` int(11) DEFAULT NULL,
  `device_category` int(11) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_70AAECED71E0CC87` (`named`),
  KEY `IDX_70AAECED268B9C9D` (`agent`),
  KEY `IDX_70AAECED887840E1` (`device_category`),
  CONSTRAINT `FK_70AAECED268B9C9D` FOREIGN KEY (`agent`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_70AAECED71E0CC87` FOREIGN KEY (`named`) REFERENCES `channel_named` (`id`),
  CONSTRAINT `FK_70AAECED887840E1` FOREIGN KEY (`device_category`) REFERENCES `device_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_channel`
--

LOCK TABLES `agent_channel` WRITE;
/*!40000 ALTER TABLE `agent_channel` DISABLE KEYS */;
INSERT INTO `agent_channel` VALUES (1,1,1,1,1),(2,2,1,1,0),(3,3,1,2,1),(4,1,2,1,1),(5,4,1,2,0),(6,4,1,1,0),(7,4,3,2,1);
/*!40000 ALTER TABLE `agent_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_channel_dynamic_field_value`
--

DROP TABLE IF EXISTS `agent_channel_dynamic_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_channel_dynamic_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent_channel` int(11) DEFAULT NULL,
  `field` int(11) DEFAULT NULL,
  `entity_value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EAC5B2F870AAECED` (`agent_channel`),
  KEY `IDX_EAC5B2F85BF54558` (`field`),
  KEY `IDX_EAC5B2F85BD407E2` (`entity_value_id`),
  CONSTRAINT `FK_EAC5B2F85BD407E2` FOREIGN KEY (`entity_value_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_EAC5B2F85BF54558` FOREIGN KEY (`field`) REFERENCES `channel_dynamic_field` (`id`),
  CONSTRAINT `FK_EAC5B2F870AAECED` FOREIGN KEY (`agent_channel`) REFERENCES `agent_channel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_channel_dynamic_field_value`
--

LOCK TABLES `agent_channel_dynamic_field_value` WRITE;
/*!40000 ALTER TABLE `agent_channel_dynamic_field_value` DISABLE KEYS */;
INSERT INTO `agent_channel_dynamic_field_value` VALUES (1,'google@gmail.com',1,1,NULL),(2,NULL,2,2,53),(3,'1112223333',3,3,NULL),(4,'google@gmail.com',4,1,NULL),(5,'google.com',5,4,NULL),(6,'yahoo.com',6,4,NULL),(7,'yahoo.com',7,4,NULL);
/*!40000 ALTER TABLE `agent_channel_dynamic_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents_contacts`
--

DROP TABLE IF EXISTS `agents_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents_contacts` (
  `agent_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`agent_id`,`contact_id`),
  KEY `IDX_BFB535093414710B` (`agent_id`),
  KEY `IDX_BFB53509E7A1254A` (`contact_id`),
  CONSTRAINT `FK_BFB535093414710B` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_BFB53509E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents_contacts`
--

LOCK TABLES `agents_contacts` WRITE;
/*!40000 ALTER TABLE `agents_contacts` DISABLE KEYS */;
INSERT INTO `agents_contacts` VALUES (1,15),(2,9),(3,10),(3,11);
/*!40000 ALTER TABLE `agents_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` tinyint(1) NOT NULL DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DADD4A251E27F6BF` (`question_id`),
  KEY `IDX_DADD4A254BD2A4C0` (`report_id`),
  CONSTRAINT `FK_DADD4A251E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `FK_DADD4A254BD2A4C0` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_dropbox_choices`
--

DROP TABLE IF EXISTS `channel_dropbox_choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_dropbox_choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `option_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `select_default` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D7781A17443707B0` (`field_id`),
  CONSTRAINT `FK_D7781A17443707B0` FOREIGN KEY (`field_id`) REFERENCES `channel_dynamic_field` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_dropbox_choices`
--

LOCK TABLES `channel_dropbox_choices` WRITE;
/*!40000 ALTER TABLE `channel_dropbox_choices` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel_dropbox_choices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_dynamic_field`
--

DROP TABLE IF EXISTS `channel_dynamic_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_dynamic_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `validation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5D1EFF9772F5A1AA` (`channel_id`),
  KEY `IDX_5D1EFF97C54C8C93` (`type_id`),
  KEY `IDX_5D1EFF97A2274850` (`validation_id`),
  CONSTRAINT `FK_5D1EFF9772F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `channel_named` (`id`),
  CONSTRAINT `FK_5D1EFF97A2274850` FOREIGN KEY (`validation_id`) REFERENCES `dynamic_field_validation` (`id`),
  CONSTRAINT `FK_5D1EFF97C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `dynamic_field_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_dynamic_field`
--

LOCK TABLES `channel_dynamic_field` WRITE;
/*!40000 ALTER TABLE `channel_dynamic_field` DISABLE KEYS */;
INSERT INTO `channel_dynamic_field` VALUES (1,1,1,'Send to','send to','2018-05-25 05:08:03','2018-05-25 05:08:03',6),(2,2,3,'Send to','send to','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(3,3,1,'Send to','send to','2018-05-25 05:08:03','2018-05-25 05:08:03',4),(4,4,1,'Send to','send to','2018-05-25 05:08:03','2018-05-25 05:08:03',10);
/*!40000 ALTER TABLE `channel_dynamic_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_named`
--

DROP TABLE IF EXISTS `channel_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_named`
--

LOCK TABLES `channel_named` WRITE;
/*!40000 ALTER TABLE `channel_named` DISABLE KEYS */;
INSERT INTO `channel_named` VALUES (1,'Email','email','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,'Mail','mail','2018-05-25 05:08:03','2018-05-25 05:08:03'),(3,'Fax','fax','2018-05-25 05:08:03','2018-05-25 05:08:03'),(4,'Upload','upload','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `channel_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_type`
--

DROP TABLE IF EXISTS `client_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_type`
--

LOCK TABLES `client_type` WRITE;
/*!40000 ALTER TABLE `client_type` DISABLE KEYS */;
INSERT INTO `client_type` VALUES (1,'Active','active','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,'Inactive','inactive','2018-05-25 05:08:03','2018-05-25 05:08:03'),(3,'Potential','potential','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `client_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `old_company_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `source_entity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_4FBF094FF5B7AF75` (`address_id`),
  KEY `IDX_4FBF094F727ACA70` (`parent_id`),
  CONSTRAINT `FK_4FBF094F727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_4FBF094FF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,35,'Company One','google.com','Notes for Company one loren ipsum notes for Company one loren ipsum',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,NULL,NULL),(2,36,'Another Company Two','yandex.com','Notes for Company two notes notes notes notes notes notes notes notes notes notes',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,NULL,NULL),(3,37,'Very Good Company Three','mail.ru','Company Three notes notes notes notes notes ',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,NULL,NULL),(4,38,'Grate Company Four','yahoo.com','notes notes notes notes notes notes notes notes notes notes notes notes notes notes',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,NULL,NULL),(5,39,'McDonalds','https://www.mcdonalds.ua/ua.html','Cool company',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_history`
--

DROP TABLE IF EXISTS `company_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7E86A9C835E0EEE` (`owner_entity_id`),
  KEY `IDX_7E86A9CBDAFD8C8` (`author`),
  KEY `IDX_7E86A9CF5B7AF75` (`address_id`),
  CONSTRAINT `FK_7E86A9C835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_7E86A9CBDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_7E86A9CF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_history`
--

LOCK TABLES `company_history` WRITE;
/*!40000 ALTER TABLE `company_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4C62E638F5B7AF75` (`address_id`),
  KEY `IDX_4C62E6388CDE5729` (`type`),
  CONSTRAINT `FK_4C62E6388CDE5729` FOREIGN KEY (`type`) REFERENCES `contact_type` (`id`),
  CONSTRAINT `FK_4C62E638F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,3,'John','Johnson','Title for Contact 1','0905671221','0312121211','003','0434346767','contact1@gmail.com',3),(2,NULL,'Jam','Smith','Title for Contact 2','0905671222','0312121212','004','0434346768','contact2@gmail.com',3),(3,6,'William','Johnson','Title for Contact 3','0905671223','0312121213','005','0434346769','contact3@gmail.com',3),(4,NULL,'John','Snow',NULL,NULL,NULL,NULL,NULL,NULL,3),(5,41,'James','Bond','Title 1 for Contact for Municipality','0915671221','0322121211','007','0514346760','contact4@gmail.com',1),(6,17,'William','Taylor','Title 2 for Contact for Municipality','0915671222','0322121212','008','0514346761','contact5@gmail.com',1),(7,20,'Jack','Sparrow','Title 3 for Contact for Municipality','0915671223','0322121213','009','0514346762','contact6@gmail.com',1),(8,42,'Bill','Smith','Title 4 for Contact for Municipality','0915671224','0322121214','010','0514346763','contact7@gmail.com',1),(9,43,'Ralph','Gleason','Title 1 for Contact for Agent','0925671221','0332121211','011','0714346761','contact8@gmail.com',2),(10,44,'Paul','Mccartney','Title 2 for Contact for Agent','0925671222','0332121212','012','0714346762','contact9@gmail.com',2),(11,50,'Linda','Smith','Title 3 for Contact for Agent','0925671223','0332121213','013','0714346763','contact10@gmail.com',2),(12,51,'Jann','Wenner','Title 4 for Contact for Agent','0925671224','0332121214','014','0714346764','contact11@gmail.com',2),(13,NULL,'Jann','Winner','Title 5 for Contact for Department','0925671224',NULL,'014','0514345554',NULL,3),(14,51,'Jon','Doe','Some Department\'s title','0925671224',NULL,'014','0514345554',NULL,3),(15,51,'Harry','Potter','Some Agents\'s title','0925671224',NULL,'014','0514345554','contact14@gmail.com',2),(16,51,'Bruce','Willis','Some department\'s title','0925671224',NULL,'014','0514345554','contact14@gmail.com',3),(17,51,'Keira','Knightley','some title','0234571224',NULL,'014','0412345554','contact16@gmail.com',3),(18,51,'John','Depp','Some Agents\'s title','0925671224',NULL,'014','0514345554','contact17@gmail.com',3);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_person`
--

DROP TABLE IF EXISTS `contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cod` tinyint(1) DEFAULT '0',
  `notes` longtext COLLATE utf8_unicode_ci,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `company` int(11) DEFAULT NULL,
  `personal_address` int(11) DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `num_contact` int(11) DEFAULT NULL,
  `source_entity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A44EE6F74FBF094F` (`company`),
  KEY `IDX_A44EE6F78E6966AA` (`personal_address`),
  CONSTRAINT `FK_A44EE6F74FBF094F` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_A44EE6F78E6966AA` FOREIGN KEY (`personal_address`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_person`
--

LOCK TABLES `contact_person` WRITE;
/*!40000 ALTER TABLE `contact_person` DISABLE KEYS */;
INSERT INTO `contact_person` VALUES (1,'Eddard','Stark','Title for Contact Person','8905671221','003','1212121211','3434346767',1,'sdsd sdsdsee rerhrrhrtrt kldfkjjgdlfhgu fgf fgderew vccx','2018-05-25 05:08:03','2018-05-25 05:08:03','eddard.stark@gmail.com',0,NULL,3,0,NULL,NULL,NULL),(2,'Robb','Stark','Contact Persons title','4547765555','123','3434346767','5643433423',1,'wwwwww wwwww www w ww w w wwww','2018-05-25 05:08:03','2018-05-25 05:08:03','robb.stark@gmail.com',0,NULL,4,0,NULL,NULL,NULL),(3,'Tyrion','Lannister','Contact Persons title test','3435671234','333','5672440987','3437774444',0,'sdsdsd rrt','2018-05-25 05:08:03','2018-05-25 05:08:03','tyrion.lannister@gmail.com',0,2,5,0,NULL,NULL,NULL),(4,'Petyr','Bealish','Contact test Persons title','5671215657','123','3432222323','2137779765',0,'wwwwww wwwww www w ww w w wwww','2018-05-25 05:08:03','2018-05-25 05:08:03','petyr.bealish@gmail.com',0,NULL,6,0,NULL,NULL,NULL),(5,'Tony','Stark','Contact Persons title','3432140345','123','4562347890','5563455677',0,'wwwwww 1212 sdsd test dsdsd','2018-05-25 05:08:03','2018-05-25 05:08:03','tony.stark@gmail.com',0,NULL,7,0,NULL,NULL,NULL),(6,'Valentin','Strykhalo','Contact Persons deleted 1','4454324567','159','4215670000','4547873245',0,'some notes for deleted contact person 1','2018-05-25 05:08:03','2018-05-25 05:08:03','valentin.strykhalo@test.com',1,NULL,8,0,NULL,NULL,NULL),(7,'Nikola','Tesla','Contact Persons deleted 2','3330964321','741','1126531278','6782346789',1,'some notes for deleted contact person 2 some notes for deleted contact person 2 some notes for deleted contact person 2','2018-05-25 05:08:03','2018-05-25 05:08:03','nikola.tesla@test.com',1,NULL,9,0,NULL,NULL,NULL),(8,'Mailing Address Conflict!',NULL,NULL,NULL,NULL,NULL,NULL,0,'Some of the sites in the old system under this Group Account (formerly a Master) were using Master Address as mailing address to send retest notices.','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,NULL,NULL,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `contact_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_person_history`
--

DROP TABLE IF EXISTS `contact_person_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_person_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cod` tinyint(1) DEFAULT '0',
  `notes` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `company` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_76D94D02835E0EEE` (`owner_entity_id`),
  KEY `IDX_76D94D02BDAFD8C8` (`author`),
  KEY `IDX_76D94D024FBF094F` (`company`),
  CONSTRAINT `FK_76D94D024FBF094F` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_76D94D02835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `contact_person` (`id`),
  CONSTRAINT `FK_76D94D02BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_person_history`
--

LOCK TABLES `contact_person_history` WRITE;
/*!40000 ALTER TABLE `contact_person_history` DISABLE KEYS */;
INSERT INTO `contact_person_history` VALUES (1,1,NULL,'Eddard','Stark','Title for Contact Person','contact1@gmail.com','8905671221','003','1212121211','3434346767',1,'sdsd sdsdsee rerhrrhrtrt kldfkjjgdlfhgu fgf fgderew vccx','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(2,2,NULL,'Robb','Stark','Contact Persons title','robb.stark@gmail.com','4547765555','123','3434346767','5643433423',1,'wwwwww wwwww www w ww w w wwww','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(3,3,NULL,'Tyrion','Lannister','Contact Persons title test','tyrion.lannister@gmail.com','3435671234','333','5672440987','3437774444',0,'sdsdsd rrt','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(4,4,NULL,'Petyr','Bealish','Contact test Persons title','petyr.bealish@gmail.com','5671215657','123','3432222323','2137779765',0,'wwwwww wwwww www w ww w w wwww','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(5,5,NULL,'Tony','Stark','Contact Persons title','tony.stark@gmail.com','3432140345','123','4562347890','5563455677',0,'wwwwww 1212 sdsd test dsdsd','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(6,6,NULL,'Valentin','Strykhalo','Contact Persons deleted 1','valentin.strykhalo@test.com','4454324567','159','4215670000','4547873245',0,'some notes for deleted contact person 1','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(7,7,NULL,'Nikola','Tesla','Contact Persons deleted 2','nikola.tesla@test.com','3330964321','741','1126531278','6782346789',1,'some notes for deleted contact person 2 some notes for deleted contact person 2 some notes for deleted contact person 2','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(8,8,NULL,'Mailing Address Conflict!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'Some of the sites in the old system under this Group Account (formerly a Master) were using Master Address as mailing address to send retest notices.','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL);
/*!40000 ALTER TABLE `contact_person_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_type`
--

DROP TABLE IF EXISTS `contact_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_type`
--

LOCK TABLES `contact_type` WRITE;
/*!40000 ALTER TABLE `contact_type` DISABLE KEYS */;
INSERT INTO `contact_type` VALUES (1,'Municipality contact','municipality_contact','municipality_contact.svg'),(2,'Agent contact','agent_contact','agent_contact.svg'),(3,'Department contact','department_contact','municipality_contact.svg');
/*!40000 ALTER TABLE `contact_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_divisions`
--

DROP TABLE IF EXISTS `contacts_divisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_divisions` (
  `contact_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`contact_id`,`division_id`),
  KEY `IDX_767E9ABE7A1254A` (`contact_id`),
  KEY `IDX_767E9AB41859289` (`division_id`),
  CONSTRAINT `FK_767E9AB41859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_767E9ABE7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_divisions`
--

LOCK TABLES `contacts_divisions` WRITE;
/*!40000 ALTER TABLE `contacts_divisions` DISABLE KEYS */;
INSERT INTO `contacts_divisions` VALUES (1,1),(2,2),(3,2),(4,1),(5,2),(6,2),(7,1),(8,1),(9,1),(10,1),(10,2),(11,1),(12,2),(13,2),(14,2),(15,2),(16,2),(17,1),(18,1);
/*!40000 ALTER TABLE `contacts_divisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor`
--

DROP TABLE IF EXISTS `contractor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_license_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state_license_expiration_date` datetime NOT NULL,
  `coi_expiration` datetime DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_sistem_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_437BD2EFF5B7AF75` (`address_id`),
  KEY `IDX_437BD2EF8CDE5729` (`type`),
  CONSTRAINT `FK_437BD2EF8CDE5729` FOREIGN KEY (`type`) REFERENCES `contractor_type` (`id`),
  CONSTRAINT `FK_437BD2EFF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor`
--

LOCK TABLES `contractor` WRITE;
/*!40000 ALTER TABLE `contractor` DISABLE KEYS */;
INSERT INTO `contractor` VALUES (1,'Monarch','https://monarch.com','123321','2018-05-25 05:08:03','2018-05-25 05:08:03','/uploads/logo/company_a.png','2018-05-25 05:08:03','2018-05-25 05:08:03',44,1,'1234567890',NULL,NULL,NULL,NULL,0),(2,'Contractor2','https://www.yahoo.com/','123321','2018-05-25 05:08:03','2018-05-25 05:08:03','/uploads/logo/company_b.png','2018-05-25 05:08:03','2018-05-25 05:08:03',42,2,'0987654321',NULL,NULL,NULL,NULL,0),(3,'American Backflow & Fire Prevention','www.americanbackflowprevention.com','123321','2018-05-25 05:08:03','2018-05-25 05:08:03','/uploads/logo/abfp_logo_png.jpg','2018-05-25 05:08:03','2018-05-25 05:08:03',40,3,'7778888888',NULL,NULL,NULL,NULL,0),(4,'Contractor no coi date','http://www.google.com','123321','2018-05-25 05:08:03',NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',43,1,NULL,NULL,NULL,NULL,NULL,0),(5,'IDAP Group','http://idapgroup.com','123456','2018-05-25 05:08:03',NULL,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',34,1,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `contractor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_service`
--

DROP TABLE IF EXISTS `contractor_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fixed_price` double DEFAULT NULL,
  `hours_price` double DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `named_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `device_category` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `estimation_time` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8DCF8195457EB27E` (`named_id`),
  KEY `IDX_8DCF8195B0265DC7` (`contractor_id`),
  KEY `IDX_8DCF8195887840E1` (`device_category`),
  KEY `IDX_8DCF81955D83CC1` (`state_id`),
  CONSTRAINT `FK_8DCF8195457EB27E` FOREIGN KEY (`named_id`) REFERENCES `service_named` (`id`),
  CONSTRAINT `FK_8DCF81955D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_8DCF8195887840E1` FOREIGN KEY (`device_category`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_8DCF8195B0265DC7` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_service`
--

LOCK TABLES `contractor_service` WRITE;
/*!40000 ALTER TABLE `contractor_service` DISABLE KEYS */;
INSERT INTO `contractor_service` VALUES (1,100,100,'35 Ill. Adm. Code 607.104(a) and (b)','153.000',73,3,1,13,2.5),(2,100,100,'NFPA 25','153.000',75,3,2,13,20),(3,100,100,'NFPA 25','153.000',74,3,2,13,12.5),(4,100,100,'NFPA 25','153.000',76,3,2,13,40),(5,100,100,'NFPA 25','153.000',77,3,2,13,8.4),(6,100,100,'NFPA 25','153.000',78,3,2,13,21),(7,100,100,'NFPA 25','153.000',79,3,2,13,11.5),(8,100,100,'NFPA 25','153.000',81,3,2,13,15),(9,100,100,'NFPA 25','153.000',117,3,2,13,15),(10,120,120,'NFPA 25','153.000',73,1,1,13,12.5),(11,NULL,NULL,NULL,NULL,82,3,2,13,14),(12,NULL,NULL,NULL,NULL,83,3,2,13,8),(13,NULL,NULL,NULL,NULL,84,3,2,13,25.5),(14,NULL,NULL,NULL,NULL,85,3,2,13,20.5),(15,NULL,NULL,NULL,NULL,86,3,2,13,2),(16,NULL,NULL,NULL,NULL,87,3,2,13,12),(17,NULL,NULL,NULL,NULL,88,3,2,13,5),(18,NULL,NULL,NULL,NULL,89,3,2,13,15),(19,NULL,NULL,NULL,NULL,90,3,2,13,1),(20,NULL,NULL,NULL,NULL,1,3,1,13,3),(21,NULL,NULL,NULL,NULL,2,3,1,13,12),(22,NULL,NULL,NULL,NULL,3,3,1,13,18),(23,NULL,NULL,NULL,NULL,4,3,1,13,9),(24,NULL,NULL,NULL,NULL,5,3,1,13,32),(25,NULL,NULL,NULL,NULL,6,3,1,13,2),(26,NULL,NULL,NULL,NULL,7,3,1,13,19),(27,NULL,NULL,NULL,NULL,8,3,1,13,29),(28,NULL,NULL,NULL,NULL,9,3,1,13,14),(29,NULL,NULL,NULL,NULL,10,3,1,13,24),(30,NULL,NULL,NULL,NULL,11,3,1,13,31),(31,NULL,NULL,NULL,NULL,12,3,1,13,3),(32,NULL,NULL,NULL,NULL,13,3,1,13,23),(33,NULL,NULL,NULL,NULL,14,3,1,13,9),(34,NULL,NULL,NULL,NULL,15,3,1,13,16),(35,NULL,NULL,NULL,NULL,16,3,1,13,19),(36,NULL,NULL,NULL,NULL,17,3,1,13,7),(37,NULL,NULL,NULL,NULL,18,3,1,13,41),(38,NULL,NULL,NULL,NULL,19,3,1,13,34),(39,NULL,NULL,NULL,NULL,20,3,1,13,10),(40,NULL,NULL,NULL,NULL,21,3,1,13,11),(41,NULL,NULL,NULL,NULL,22,3,1,13,16),(42,NULL,NULL,NULL,NULL,23,3,1,13,19),(43,NULL,NULL,NULL,NULL,24,3,1,13,9),(44,NULL,NULL,NULL,NULL,25,3,1,13,3),(45,NULL,NULL,NULL,NULL,26,3,1,13,35),(46,NULL,NULL,NULL,NULL,27,3,1,13,3),(47,NULL,NULL,NULL,NULL,28,3,1,13,13),(48,NULL,NULL,NULL,NULL,29,3,1,13,6),(49,NULL,NULL,NULL,NULL,30,3,1,13,16),(50,NULL,NULL,NULL,NULL,31,3,1,13,5),(51,NULL,NULL,NULL,NULL,32,3,1,13,15),(52,NULL,NULL,NULL,NULL,33,3,1,13,4),(53,NULL,NULL,NULL,NULL,34,3,1,13,14),(54,NULL,NULL,NULL,NULL,35,3,1,13,3),(55,NULL,NULL,NULL,NULL,36,3,1,13,13),(56,NULL,NULL,NULL,NULL,37,3,1,13,2),(57,NULL,NULL,NULL,NULL,38,3,1,13,12),(58,NULL,NULL,NULL,NULL,39,3,1,13,1),(59,NULL,NULL,NULL,NULL,40,3,1,13,11),(60,NULL,NULL,NULL,NULL,41,3,1,13,21),(61,NULL,NULL,NULL,NULL,42,3,1,13,22),(62,NULL,NULL,NULL,NULL,43,3,1,13,41),(63,NULL,NULL,NULL,NULL,44,3,1,13,12),(64,NULL,NULL,NULL,NULL,45,3,1,13,6),(65,NULL,NULL,NULL,NULL,46,3,1,13,16),(66,NULL,NULL,NULL,NULL,47,3,1,13,23),(67,NULL,NULL,NULL,NULL,48,3,1,13,11),(68,NULL,NULL,NULL,NULL,49,3,1,13,11.5),(69,NULL,NULL,NULL,NULL,50,3,1,13,9),(70,NULL,NULL,NULL,NULL,51,3,1,13,20.5),(71,NULL,NULL,NULL,NULL,52,3,1,13,14),(72,NULL,NULL,NULL,NULL,53,3,1,13,13.5),(73,NULL,NULL,NULL,NULL,54,3,1,13,8.5),(74,NULL,NULL,NULL,NULL,55,3,1,13,26),(75,NULL,NULL,NULL,NULL,56,3,1,13,26.5),(76,NULL,NULL,NULL,NULL,57,3,1,13,6.5),(77,NULL,NULL,NULL,NULL,58,3,1,13,29),(78,NULL,NULL,NULL,NULL,60,3,1,13,2.5),(79,NULL,NULL,NULL,NULL,61,3,1,13,12.5),(80,NULL,NULL,NULL,NULL,62,3,1,13,19),(81,NULL,NULL,NULL,NULL,63,3,1,13,12),(82,NULL,NULL,NULL,NULL,64,3,1,13,22),(83,NULL,NULL,NULL,NULL,65,3,1,13,2),(84,NULL,NULL,NULL,NULL,66,3,1,13,29),(85,NULL,NULL,NULL,NULL,67,3,1,13,4),(86,NULL,NULL,NULL,NULL,68,3,1,13,42),(87,NULL,NULL,NULL,NULL,69,3,1,13,12),(88,NULL,NULL,NULL,NULL,70,3,1,13,22),(89,NULL,NULL,NULL,NULL,71,3,1,13,18.5),(90,NULL,NULL,NULL,NULL,72,3,1,13,18.5),(91,NULL,NULL,NULL,NULL,59,3,1,13,36.5),(92,100,100,'NFPA 25','153.000',117,3,2,13,40);
/*!40000 ALTER TABLE `contractor_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_type`
--

DROP TABLE IF EXISTS `contractor_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_type`
--

LOCK TABLES `contractor_type` WRITE;
/*!40000 ALTER TABLE `contractor_type` DISABLE KEYS */;
INSERT INTO `contractor_type` VALUES (1,'Partner','partner','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(2,'Competitor','competitor','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(3,'My Company','my_company','2018-05-25 05:08:03','2018-05-25 05:08:03',1);
/*!40000 ALTER TABLE `contractor_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_user`
--

DROP TABLE IF EXISTS `contractor_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contractor` int(11) DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_759F1C39F5B7AF75` (`address_id`),
  KEY `IDX_759F1C398D93D649` (`user`),
  KEY `IDX_759F1C39437BD2EF` (`contractor`),
  CONSTRAINT `FK_759F1C39437BD2EF` FOREIGN KEY (`contractor`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_759F1C398D93D649` FOREIGN KEY (`user`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_759F1C39F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_user`
--

LOCK TABLES `contractor_user` WRITE;
/*!40000 ALTER TABLE `contractor_user` DISABLE KEYS */;
INSERT INTO `contractor_user` VALUES (1,NULL,1,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(2,NULL,2,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(3,NULL,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(4,NULL,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(5,NULL,3,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(6,NULL,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(7,NULL,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(8,NULL,15,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(9,NULL,22,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(10,NULL,23,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(11,NULL,24,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(12,NULL,34,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0),(13,NULL,9,NULL,NULL,1,'CEO (Superadmin)',NULL,NULL,NULL,NULL,3,0),(14,NULL,10,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,3,0),(15,NULL,29,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,3,0),(16,NULL,30,NULL,NULL,1,'Big Boss',NULL,NULL,NULL,NULL,3,0),(17,NULL,31,NULL,NULL,1,'Super Super Admin',NULL,NULL,NULL,NULL,3,0),(18,NULL,28,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,3,0),(19,NULL,32,NULL,NULL,1,'Tester',NULL,NULL,NULL,NULL,3,0),(20,NULL,33,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,3,0),(21,44,17,NULL,NULL,1,'Senior Manager','john_doe@email.com','1112222222','1112222222','1112222222',1,0),(22,45,18,NULL,NULL,1,'Manager','john_bishop@email.com','1112222222','1112222222','1112222222',1,0),(23,46,19,'back-flow-testing.jpg','1500386316.png',1,'Tester','james_hatfield@email.com','1112222222','1112222222','1112222222',1,0),(24,47,20,'thmb_tester3.jpg','1500386336.png',1,'Tester','kirk_hammet@email.com','1112222222','1112222222','1112222222',1,0);
/*!40000 ALTER TABLE `contractor_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_user_history`
--

DROP TABLE IF EXISTS `contractor_user_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_user_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `contractor` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_save` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B40CE4F3F5B7AF75` (`address_id`),
  KEY `IDX_B40CE4F38D93D649` (`user`),
  KEY `IDX_B40CE4F3437BD2EF` (`contractor`),
  KEY `IDX_B40CE4F3BDAFD8C8` (`author`),
  KEY `IDX_B40CE4F3835E0EEE` (`owner_entity_id`),
  CONSTRAINT `FK_B40CE4F3437BD2EF` FOREIGN KEY (`contractor`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_B40CE4F3835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_B40CE4F38D93D649` FOREIGN KEY (`user`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_B40CE4F3BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_B40CE4F3F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_user_history`
--

LOCK TABLES `contractor_user_history` WRITE;
/*!40000 ALTER TABLE `contractor_user_history` DISABLE KEYS */;
INSERT INTO `contractor_user_history` VALUES (1,NULL,1,5,NULL,1,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(2,NULL,2,5,NULL,2,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(3,NULL,7,5,NULL,3,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(4,NULL,5,5,NULL,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(5,NULL,3,5,NULL,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(6,NULL,6,5,NULL,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(7,NULL,4,5,NULL,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(8,NULL,15,5,NULL,8,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(9,NULL,22,5,NULL,9,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(10,NULL,23,5,NULL,10,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(11,NULL,24,5,NULL,11,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(12,NULL,34,5,NULL,12,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(13,NULL,9,3,NULL,13,NULL,NULL,1,'CEO (Superadmin)',NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(14,NULL,10,3,NULL,14,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,'2018-05-25 05:08:03'),(15,44,17,1,NULL,21,NULL,NULL,1,'Senior Manager','john_doe@email.com','(111)222-2222','(111)222-2222','(111)222-2222','2018-05-25 05:08:03'),(16,45,18,1,NULL,22,NULL,NULL,1,'Manager','john_bishop@email.com','(111)222-2222','(111)222-2222','(111)222-2222','2018-05-25 05:08:03'),(17,46,19,1,NULL,23,'back-flow-testing.jpg','1500386316.png',1,'Tester','james_hatfield@email.com','(111)222-2222','(111)222-2222','(111)222-2222','2018-05-25 05:08:03'),(18,47,20,1,NULL,24,'thmb_tester3.jpg','1500386336.png',1,'Tester','kirk_hammet@email.com','(111)222-2222','(111)222-2222','(111)222-2222','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `contractor_user_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coordinate`
--

DROP TABLE IF EXISTS `coordinate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coordinate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coordinate`
--

LOCK TABLES `coordinate` WRITE;
/*!40000 ALTER TABLE `coordinate` DISABLE KEYS */;
INSERT INTO `coordinate` VALUES (1,40.7127837,-74.0059413),(2,34.0522342,-118.2436849);
/*!40000 ALTER TABLE `coordinate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `municipality_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CD1DE18AAE6F181C` (`municipality_id`),
  KEY `IDX_CD1DE18A41859289` (`division_id`),
  KEY `IDX_CD1DE18A3414710B` (`agent_id`),
  CONSTRAINT `FK_CD1DE18A3414710B` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_CD1DE18A41859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_CD1DE18AAE6F181C` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,2,1,'Chicago Water Supply Department',NULL),(2,2,2,'Chicago Fire Protection Department',NULL),(3,1,1,'New York Water Supply Department',NULL),(4,1,2,'New York Fire Protection Department',NULL),(5,3,1,'Portland Water Supply Department',NULL),(6,3,2,'Portland Fire Protection Department',NULL);
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_channel`
--

DROP TABLE IF EXISTS `department_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `upload_fee` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `processing_fee` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `owner_agent_id` int(11) DEFAULT NULL,
  `fees_basis_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `devision_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_1F5066A4457EB27E` (`named_id`),
  KEY `IDX_1F5066A422F4C8C8` (`owner_agent_id`),
  KEY `IDX_1F5066A4C6944BCB` (`fees_basis_id`),
  KEY `IDX_1F5066A4AE80F5DF` (`department_id`),
  KEY `IDX_1F5066A45BB97207` (`devision_id`),
  KEY `IDX_1F5066A47E3C61F9` (`owner_id`),
  CONSTRAINT `FK_1F5066A422F4C8C8` FOREIGN KEY (`owner_agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_1F5066A4457EB27E` FOREIGN KEY (`named_id`) REFERENCES `channel_named` (`id`),
  CONSTRAINT `FK_1F5066A45BB97207` FOREIGN KEY (`devision_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_1F5066A47E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `agent_channel` (`id`),
  CONSTRAINT `FK_1F5066A4AE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_1F5066A4C6944BCB` FOREIGN KEY (`fees_basis_id`) REFERENCES `fees_basis` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_channel`
--

LOCK TABLES `department_channel` WRITE;
/*!40000 ALTER TABLE `department_channel` DISABLE KEYS */;
INSERT INTO `department_channel` VALUES (1,1,'0','0',0,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,1,1,NULL,1),(2,2,'0','0',0,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,1,1,NULL,1),(3,3,'0','0',0,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,1,1,NULL,1),(4,4,'2','15',1,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,1,1,1,NULL,1),(5,1,'0','0',0,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,2,2,NULL,1),(6,2,'0','0',0,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,2,2,NULL,1),(7,3,'0','0',0,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,NULL,2,2,NULL,1),(8,4,'2','15',1,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,1,2,2,NULL,1),(9,4,'3','7',1,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,1,5,1,NULL,1),(10,1,'0','0',1,0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,1,6,2,NULL,1);
/*!40000 ALTER TABLE `department_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_channel_dynamic_field_value`
--

DROP TABLE IF EXISTS `department_channel_dynamic_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_channel_dynamic_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  `entity_value_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `department_chanel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_64EFA3443707B0` (`field_id`),
  KEY `IDX_64EFA3D957CA06` (`option_value_id`),
  KEY `IDX_64EFA35BD407E2` (`entity_value_id`),
  KEY `IDX_64EFA3FD76E0D2` (`department_chanel_id`),
  CONSTRAINT `FK_64EFA3443707B0` FOREIGN KEY (`field_id`) REFERENCES `channel_dynamic_field` (`id`),
  CONSTRAINT `FK_64EFA35BD407E2` FOREIGN KEY (`entity_value_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_64EFA3D957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `channel_dropbox_choices` (`id`),
  CONSTRAINT `FK_64EFA3FD76E0D2` FOREIGN KEY (`department_chanel_id`) REFERENCES `department_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_channel_dynamic_field_value`
--

LOCK TABLES `department_channel_dynamic_field_value` WRITE;
/*!40000 ALTER TABLE `department_channel_dynamic_field_value` DISABLE KEYS */;
INSERT INTO `department_channel_dynamic_field_value` VALUES (1,1,NULL,NULL,'mail@mail.com','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(2,2,NULL,48,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',2),(3,3,NULL,NULL,'1234567890','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(4,4,NULL,NULL,'http://web.cc.us/upload','2018-05-25 05:08:03','2018-05-25 05:08:03',4),(5,1,NULL,NULL,'mail-2@mail2.com','2018-05-25 05:08:03','2018-05-25 05:08:03',5),(6,2,NULL,48,NULL,'2018-05-25 05:08:03','2018-05-25 05:08:03',6),(7,3,NULL,NULL,'1234567890','2018-05-25 05:08:03','2018-05-25 05:08:03',7),(8,4,NULL,NULL,'http://web-2.cc.us/upload2','2018-05-25 05:08:03','2018-05-25 05:08:03',8),(9,4,NULL,NULL,'http://web.cc.us/upload','2018-05-25 05:08:03','2018-05-25 05:08:03',9),(10,1,NULL,NULL,'mail@mail.com','2018-05-25 05:08:03','2018-05-25 05:08:03',10);
/*!40000 ALTER TABLE `department_channel_dynamic_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_channel_history`
--

DROP TABLE IF EXISTS `department_channel_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_channel_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `owner_agent_id` int(11) DEFAULT NULL,
  `fees_basis_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `devision_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `upload_fee` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `processing_fee` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7BAD093B457EB27E` (`named_id`),
  KEY `IDX_7BAD093B22F4C8C8` (`owner_agent_id`),
  KEY `IDX_7BAD093BC6944BCB` (`fees_basis_id`),
  KEY `IDX_7BAD093BAE80F5DF` (`department_id`),
  KEY `IDX_7BAD093B5BB97207` (`devision_id`),
  KEY `IDX_7BAD093B7E3C61F9` (`owner_id`),
  KEY `IDX_7BAD093BBDAFD8C8` (`author`),
  KEY `IDX_7BAD093B835E0EEE` (`owner_entity_id`),
  CONSTRAINT `FK_7BAD093B22F4C8C8` FOREIGN KEY (`owner_agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_7BAD093B457EB27E` FOREIGN KEY (`named_id`) REFERENCES `channel_named` (`id`),
  CONSTRAINT `FK_7BAD093B5BB97207` FOREIGN KEY (`devision_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_7BAD093B7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `agent_channel` (`id`),
  CONSTRAINT `FK_7BAD093B835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `department_channel` (`id`),
  CONSTRAINT `FK_7BAD093BAE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_7BAD093BBDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_7BAD093BC6944BCB` FOREIGN KEY (`fees_basis_id`) REFERENCES `fees_basis` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_channel_history`
--

LOCK TABLES `department_channel_history` WRITE;
/*!40000 ALTER TABLE `department_channel_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `department_channel_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_history`
--

DROP TABLE IF EXISTS `department_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `municipality_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_save` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9A1398A8AE6F181C` (`municipality_id`),
  KEY `IDX_9A1398A841859289` (`division_id`),
  KEY `IDX_9A1398A83414710B` (`agent_id`),
  KEY `IDX_9A1398A8BDAFD8C8` (`author`),
  KEY `IDX_9A1398A8835E0EEE` (`owner_entity_id`),
  CONSTRAINT `FK_9A1398A83414710B` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_9A1398A841859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_9A1398A8835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_9A1398A8AE6F181C` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`),
  CONSTRAINT `FK_9A1398A8BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_history`
--

LOCK TABLES `department_history` WRITE;
/*!40000 ALTER TABLE `department_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `department_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments_contacts`
--

DROP TABLE IF EXISTS `departments_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments_contacts` (
  `department_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`department_id`,`contact_id`),
  KEY `IDX_9363C481AE80F5DF` (`department_id`),
  KEY `IDX_9363C481E7A1254A` (`contact_id`),
  CONSTRAINT `FK_9363C481AE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_9363C481E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments_contacts`
--

LOCK TABLES `departments_contacts` WRITE;
/*!40000 ALTER TABLE `departments_contacts` DISABLE KEYS */;
INSERT INTO `departments_contacts` VALUES (1,1),(1,4),(2,2),(2,3),(3,17),(3,18),(4,13),(4,16);
/*!40000 ALTER TABLE `departments_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `comments` longtext COLLATE utf8_unicode_ci,
  `note_to_tester` longtext COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `old_device_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `alarm_panel_id` int(11) DEFAULT NULL,
  `source_entity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_92FB68E457EB27E` (`named_id`),
  KEY `IDX_92FB68E6BF700BD` (`status_id`),
  KEY `IDX_92FB68E9B6B5FBA` (`account_id`),
  KEY `IDX_92FB68E727ACA70` (`parent_id`),
  KEY `IDX_92FB68EC226AA40` (`alarm_panel_id`),
  KEY `IDX_92FB68E7E5A734A` (`last_editor_id`),
  CONSTRAINT `FK_92FB68E457EB27E` FOREIGN KEY (`named_id`) REFERENCES `device_named` (`id`),
  CONSTRAINT `FK_92FB68E6BF700BD` FOREIGN KEY (`status_id`) REFERENCES `device_status` (`id`),
  CONSTRAINT `FK_92FB68E727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_92FB68E7E5A734A` FOREIGN KEY (`last_editor_id`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_92FB68E9B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_92FB68EC226AA40` FOREIGN KEY (`alarm_panel_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` VALUES (1,6,1,14,'1','/uploads/devices/1527243947_63438.jpeg','2018-05-25 05:25:47','2018-05-25 05:25:47','1','2',0,NULL,NULL,0,NULL,NULL,'RPDA',22),(2,6,1,14,'1','/uploads/devices/1527244463_43552.jpeg','2018-05-25 05:34:23','2018-05-25 05:34:23','1','2',0,NULL,NULL,0,NULL,NULL,'RPZ',22),(3,3,1,9,'1111','/uploads/devices/1527244580_69509.jpeg','2018-05-25 05:36:20','2018-05-25 05:36:20','3','333',0,NULL,NULL,0,NULL,NULL,'Fire System',22),(4,9,1,9,'11111','/uploads/devices/1527244612_48450.jpeg','2018-05-25 05:36:52','2018-05-25 05:36:52','2','3',0,3,NULL,0,NULL,NULL,'Dry Valve',22),(5,16,1,9,'77','/uploads/devices/1527244642_54111.jpeg','2018-05-25 05:37:22','2018-05-25 05:37:22','8','99',0,4,NULL,0,NULL,NULL,'Low Point',22),(6,6,1,9,'88','/uploads/devices/1527244870_49681.png','2018-05-25 05:41:10','2018-05-25 05:41:10','77','888',0,NULL,NULL,0,NULL,NULL,'RPDA',22),(7,5,1,14,'8','/uploads/devices/1527245156_97506.jpeg','2018-05-25 05:45:56','2018-05-25 05:45:56','8','8',0,NULL,NULL,0,NULL,NULL,'888 extinguisher(s)',22),(8,3,1,14,'11','/uploads/devices/1527256407_64084.jpeg','2018-05-25 08:53:27','2018-05-25 08:53:27','1','2',0,NULL,NULL,0,NULL,NULL,'Fire System',22);
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_category`
--

DROP TABLE IF EXISTS `device_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_category`
--

LOCK TABLES `device_category` WRITE;
/*!40000 ALTER TABLE `device_category` DISABLE KEYS */;
INSERT INTO `device_category` VALUES (1,'Backflow','backflow','2018-05-25 05:07:41','2018-05-25 05:07:41','backflow.svg',4),(2,'Fire','fire','2018-05-25 05:07:41','2018-05-25 05:07:41','fire.svg',3),(3,'Plumbing','plumbing','2018-05-25 05:07:41','2018-05-25 05:07:41','plumbing.svg',2),(4,'Alarm','alarm','2018-05-25 05:07:41','2018-05-25 05:07:41','alarm.svg',1);
/*!40000 ALTER TABLE `device_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_history`
--

DROP TABLE IF EXISTS `device_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` longtext COLLATE utf8_unicode_ci,
  `note_to_tester` longtext COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AD0CA9D6457EB27E` (`named_id`),
  KEY `IDX_AD0CA9D66BF700BD` (`status_id`),
  KEY `IDX_AD0CA9D69B6B5FBA` (`account_id`),
  KEY `IDX_AD0CA9D6835E0EEE` (`owner_entity_id`),
  KEY `IDX_AD0CA9D6BDAFD8C8` (`author`),
  KEY `IDX_AD0CA9D6727ACA70` (`parent_id`),
  CONSTRAINT `FK_AD0CA9D6457EB27E` FOREIGN KEY (`named_id`) REFERENCES `device_named` (`id`),
  CONSTRAINT `FK_AD0CA9D66BF700BD` FOREIGN KEY (`status_id`) REFERENCES `device_status` (`id`),
  CONSTRAINT `FK_AD0CA9D6727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_AD0CA9D6835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_AD0CA9D69B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_AD0CA9D6BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_history`
--

LOCK TABLES `device_history` WRITE;
/*!40000 ALTER TABLE `device_history` DISABLE KEYS */;
INSERT INTO `device_history` VALUES (1,6,1,14,1,22,'1','/uploads/devices/1527243947_63438.jpeg','1','2',0,'2018-05-25 05:25:47','2018-05-25 05:25:47',NULL),(2,6,1,14,2,22,'1','/uploads/devices/1527244463_43552.jpeg','1','2',0,'2018-05-25 05:34:23','2018-05-25 05:34:24',NULL),(3,3,1,9,3,22,'1111','/uploads/devices/1527244580_69509.jpeg','3','333',0,'2018-05-25 05:36:20','2018-05-25 05:36:20',NULL),(4,9,1,9,4,22,'11111','/uploads/devices/1527244612_48450.jpeg','2','3',0,'2018-05-25 05:36:52','2018-05-25 05:36:52',NULL),(5,16,1,9,5,22,'77','/uploads/devices/1527244642_54111.jpeg','8','99',0,'2018-05-25 05:37:22','2018-05-25 05:37:22',NULL),(6,6,1,9,6,22,'88','/uploads/devices/1527244870_49681.png','77','888',0,'2018-05-25 05:41:10','2018-05-25 05:41:10',NULL),(7,5,1,14,7,22,'8','/uploads/devices/1527245156_97506.jpeg','8','8',0,'2018-05-25 05:45:56','2018-05-25 05:45:56',NULL),(8,3,1,14,8,22,'11','/uploads/devices/1527256407_64084.jpeg','1','2',0,'2018-05-25 08:53:27','2018-05-25 08:53:27',NULL);
/*!40000 ALTER TABLE `device_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_named`
--

DROP TABLE IF EXISTS `device_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_B7652CE012469DE2` (`category_id`),
  KEY `IDX_B7652CE0727ACA70` (`parent_id`),
  CONSTRAINT `FK_B7652CE012469DE2` FOREIGN KEY (`category_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_B7652CE0727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `device_named` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_named`
--

LOCK TABLES `device_named` WRITE;
/*!40000 ALTER TABLE `device_named` DISABLE KEYS */;
INSERT INTO `device_named` VALUES (1,3,'Water Heater','water_heater','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0),(2,3,'Lift Station','lift_station','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0),(3,2,'Fire System','fire_system','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,3,0),(4,2,'Fire Pump','fire_pump','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,2,0),(5,2,'Fire Extinguishers','fire_extinguishers','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,1,0),(6,1,'Backflow Device','backflow_device','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0),(7,2,'Riser','riser','2018-05-25 05:08:03','2018-05-25 05:08:03',3,0,1),(8,2,'Anti-Freeze System','anti_freeze_system','2018-05-25 05:08:03','2018-05-25 05:08:03',3,0,1),(9,2,'Dry Valve','dry_valve','2018-05-25 05:08:03','2018-05-25 05:08:03',3,0,1),(10,2,'FDC Check Valve','fdc_check_valve','2018-05-25 05:08:04','2018-05-25 05:08:04',3,0,1),(11,2,'Fire Alarm Control Panel (FACP)','fire_alarm_control_panel','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0),(12,2,'Fire Extinguisher','fire_extinguisher','2018-05-25 05:08:04','2018-05-25 05:08:04',5,1,0),(13,2,'Range Hood System','range_hood_system','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0),(14,2,'Section','section','2018-05-25 05:08:04','2018-05-25 05:08:04',3,0,1),(15,2,'Air Compressor','air_compressor','2018-05-25 05:08:04','2018-05-25 05:08:04',9,0,2),(16,2,'Low Point','low_point','2018-05-25 05:08:04','2018-05-25 05:08:04',9,0,2);
/*!40000 ALTER TABLE `device_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_status`
--

DROP TABLE IF EXISTS `device_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_status`
--

LOCK TABLES `device_status` WRITE;
/*!40000 ALTER TABLE `device_status` DISABLE KEYS */;
INSERT INTO `device_status` VALUES (1,'Active','active','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,'Inactive','inactive','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `device_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field`
--

DROP TABLE IF EXISTS `dynamic_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `validation_id` int(11) DEFAULT NULL,
  `use_label` tinyint(1) DEFAULT '0',
  `label_after` tinyint(1) DEFAULT '0',
  `label_description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_show` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_FC831C2394A4C7D4` (`device_id`),
  KEY `IDX_FC831C23C54C8C93` (`type_id`),
  KEY `IDX_FC831C23A2274850` (`validation_id`),
  CONSTRAINT `FK_FC831C2394A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device_named` (`id`),
  CONSTRAINT `FK_FC831C23A2274850` FOREIGN KEY (`validation_id`) REFERENCES `dynamic_field_validation` (`id`),
  CONSTRAINT `FK_FC831C23C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `dynamic_field_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field`
--

LOCK TABLES `dynamic_field` WRITE;
/*!40000 ALTER TABLE `dynamic_field` DISABLE KEYS */;
INSERT INTO `dynamic_field` VALUES (1,3,2,'Size','fire_system_size','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(2,3,1,'Number of Risers','number_of_risers','2018-05-25 05:08:03','2018-05-25 05:08:03',3,1,1,'Riser',1),(3,3,1,'Number of Sections','number_of_sections','2018-05-25 05:08:03','2018-05-25 05:08:03',3,1,1,'Sections',1),(4,3,2,'Water Supply Source','water_supply_source','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(5,3,1,'Number of heads','number_of_heads','2018-05-25 05:08:03','2018-05-25 05:08:03',11,1,1,'Heads',1),(6,4,2,'Shaft','shaft','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(7,4,2,'Make','fire_pump_make','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(8,4,1,'Model','model','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(9,4,1,'Serial Number','serial_number','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(10,4,2,'Rated GPM','rated_gpm','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(11,4,1,'Rated PSI','rated_psi','2018-05-25 05:08:03','2018-05-25 05:08:03',11,0,0,NULL,1),(12,4,2,'Pump Listed','pump_listed','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(13,4,2,'Pump Approved','pump_approved','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(14,4,2,'Pump Suction','pump_suction','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(15,4,1,'Impeller Diameter','impeller_diameter','2018-05-25 05:08:03','2018-05-25 05:08:03',13,0,0,NULL,1),(16,4,2,'Discharge Pipe','discharge_pipe','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(17,4,2,'Suction Pipe','suction_pipe','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(18,4,2,'Rotation','rotation','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(19,4,1,'Test Header Location','test_header_location','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(20,5,1,'Number of Extinguishers','number_of_extinguishers','2018-05-25 05:08:03','2018-05-25 05:08:03',11,0,0,NULL,1),(21,6,2,'Size','backflow_size','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(22,6,2,'Make','backflow_make','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(23,6,1,'Model','model','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(24,6,2,'Type','type','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,0),(25,6,1,'Serial Number','serial_number','2018-05-25 05:08:03','2018-05-25 05:08:03',1,0,0,NULL,1),(26,6,2,'Hazard','hazard','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(27,6,2,'Orientation','orientation','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(28,6,2,'Shut Off Valve(s)','shut_off_valve','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(29,6,2,'Approved Installation','approved_installation','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(30,7,1,'Riser Number','riser_number','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(31,7,2,'Size','riser_size','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(32,7,1,'ITV Location','itv_location','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(33,7,2,'Gauge Size IPS','gauge_size_ips','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(34,8,2,'Size','anti_freeze_size','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(35,8,2,'Anti-Freeze System Type','anti_freeze_system_type','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(36,8,1,'Number of Heads','number_of_heads','2018-05-25 05:08:03','2018-05-25 05:08:03',3,0,0,NULL,1),(37,8,2,'Water Supply Source','water_supply_source','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(38,9,2,'Size','dry_valve_size','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(39,9,1,'Make','dry_valve_make','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(40,9,1,'Model','model','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(41,9,1,'Serial Number','serial_number','2018-05-25 05:08:03','2018-05-25 05:08:03',1,0,0,NULL,1),(42,9,1,'Number of Low Points','number_of_low_points','2018-05-25 05:08:03','2018-05-25 05:08:03',3,0,0,NULL,1),(43,9,2,'Water Supply Source','water_supply_source','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,0,0,NULL,1),(44,9,1,'ITV Location','itv_location','2018-05-25 05:08:03','2018-05-25 05:08:03',NULL,1,0,'ITV Location: ',1),(45,10,2,'Size','fdc_check_valve_size','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(46,10,1,'Make','make','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(47,10,2,'Connection','connection','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(48,10,2,'Drain Down','drain_down','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(49,11,2,'FACP Type','facp_type','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(50,11,2,'Make','fire_alarm_panel_make','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(51,11,1,'Model','model','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(52,11,1,'Alarm Monitoring Company','alarm_monitoring_company','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(53,11,1,'Passcode','passcode','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(54,11,1,'Position Number','position_number','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(55,11,1,'Phone Number','phone_number','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(56,12,2,'Fire Extinguisher Size','fire_extinguisher_size','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(57,12,2,'Fire Extinguisher Type','fire_extinguisher_type','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(58,14,1,'Section Number','section_number','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(59,14,2,'Size','section_size','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(60,14,1,'ITV Location','itv_location','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(61,14,2,'Gauge Size IPS','gauge_size_ips','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(62,15,1,'Make','make','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1),(63,15,1,'Model','model','2018-05-25 05:08:04','2018-05-25 05:08:04',NULL,0,0,NULL,1);
/*!40000 ALTER TABLE `dynamic_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_dropbox_value`
--

DROP TABLE IF EXISTS `dynamic_field_dropbox_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_dropbox_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `option_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `select_default` tinyint(1) DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1A16D482443707B0` (`field_id`),
  CONSTRAINT `FK_1A16D482443707B0` FOREIGN KEY (`field_id`) REFERENCES `dynamic_field` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_dropbox_value`
--

LOCK TABLES `dynamic_field_dropbox_value` WRITE;
/*!40000 ALTER TABLE `dynamic_field_dropbox_value` DISABLE KEYS */;
INSERT INTO `dynamic_field_dropbox_value` VALUES (1,1,'1\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_1'),(2,1,'1.25\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_1_dot_25'),(3,1,'1.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_1_dot_5'),(4,1,'2\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_2'),(5,1,'2.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_2_dot_5'),(6,1,'3\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_3'),(7,1,'4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_4'),(8,1,'6\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_6'),(9,1,'8\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_8'),(10,1,'10\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_10'),(11,1,'12\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_size_12'),(12,4,'City','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_water_supply_source_city'),(13,4,'Tank','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_water_supply_source_tank'),(14,4,'Pump','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_water_supply_source_pump'),(15,4,'Pond','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_water_supply_source_pond'),(16,4,'Well','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_water_supply_source_well'),(17,4,'Other','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_system_water_supply_source_other'),(18,6,'Horizontal Split Case','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_shaft_horizontal_split_case'),(19,6,'Vertical In-Line','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_shaft_vertical_in_line'),(20,6,'End Suction','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_shaft_end_suction'),(21,7,'Allis Chalmers','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_allis_chalmers'),(22,7,'Aurora','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_aurora'),(23,7,'Chicago','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_chicago'),(24,7,'Fairbanks Morse','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_fairbanks_morse'),(25,7,'Le Courtney','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_le_courtney'),(26,7,'Patterson','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_patterson'),(27,7,'Peerless','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_peerless'),(28,7,'Redi-Buffalo','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_redi_buffalo'),(29,7,'SPP','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_spp'),(30,7,'Vertical','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_vertical'),(31,7,'Marathon','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_marathon'),(32,7,'1500gpm','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_make_1500gpm'),(33,10,'25','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_25'),(34,10,'50','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_50'),(35,10,'75','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_75'),(36,10,'100','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_100'),(37,10,'150','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_150'),(38,10,'200','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_200'),(39,10,'250','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_250'),(40,10,'300','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_300'),(41,10,'400','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_400'),(42,10,'450','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_450'),(43,10,'500','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_500'),(44,10,'750','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_750'),(45,10,'1000','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_1000'),(46,10,'1250','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_1250'),(47,10,'1500','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_1500'),(48,10,'2000','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_2000'),(49,10,'2500','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_2500'),(50,10,'3000','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_3000'),(51,10,'3500','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_3500'),(52,10,'4000','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_4000'),(53,10,'4500','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_4500'),(54,10,'5000','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rated_gpm_5000'),(55,12,'FM','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_listed_fm'),(56,12,'UL','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_listed_ul'),(57,12,'ULC','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_listed_ulc'),(58,12,'None','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_listed_none'),(59,13,'YES','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_approved_yes'),(60,13,'NO','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_approved_no'),(61,14,'City','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_suction_city'),(62,14,'Tank','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_suction_tank'),(63,14,'Pond','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_suction_pond'),(64,14,'Other','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_pump_suction_other'),(65,16,'1.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_1_dot_5'),(66,16,'2\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_2'),(67,16,'2.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_2_dot_5'),(68,16,'3\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_3'),(69,16,'4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_4'),(70,16,'6\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_6'),(71,16,'8\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_8'),(72,16,'10\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_10'),(73,16,'12\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_discharge_pipe_12'),(74,17,'1.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_1_dot_5'),(75,17,'2\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_2'),(76,17,'2.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_2_dot_5'),(77,17,'3\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_3'),(78,17,'4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_4'),(79,17,'6\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_6'),(80,17,'8\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_8'),(81,17,'10\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_10'),(82,17,'12\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_suction_pipe_12'),(83,18,'Clockwise','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rotation_clockwise'),(84,18,'CC Clockwise','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'fire_pump_rotation_cc_clockwise'),(85,21,'1/4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_1_slash_4'),(86,21,'1/2\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_1_slash_2'),(87,21,'3/4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_3_slash_4'),(88,21,'1\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_1'),(89,21,'1.25\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_1_dot_25'),(90,21,'1.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_1_dot_5'),(91,21,'2\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_2'),(92,21,'2.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_2_dot_5'),(93,21,'3\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_3'),(94,21,'4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_4'),(95,21,'6\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_6'),(96,21,'8\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_8'),(97,21,'10\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_10'),(98,21,'12\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_size_12'),(99,22,'Ames','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_ames'),(100,22,'Febco','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_febco'),(101,22,'Watts','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_watts'),(102,22,'Wilkins','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_wilkins'),(103,22,'Rainbird','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_rainbird'),(104,22,'Conbraco','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_conbraco'),(105,22,'Apollo','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_apollo'),(106,22,'Colt','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_colt'),(107,22,'Maxim','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_maxim'),(108,22,'Beeco','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_beeco'),(109,22,'Backflow Direct','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_backflow_direct'),(110,22,'Flomatic','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_flomatic'),(111,22,'Swing CHK','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_swing_chk'),(112,22,'Zurn','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_zurn'),(113,22,'Verify','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_verify'),(114,22,'Cash Acme','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_cash_acme'),(115,22,'Globe','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_globe'),(116,22,'Cla-Val','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_cla_val'),(117,22,'Buckner','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_buckner'),(118,22,'Hersey','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_hersey'),(119,22,'A.R.I','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_ari'),(120,22,'Other','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_make_other'),(121,24,'RPZ','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_type_rpz'),(122,24,'RPDA','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_type_rpda'),(123,24,'DCDA','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_type_dcda'),(124,24,'DCV','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_type_dcv'),(125,24,'SVB','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_type_svb'),(126,24,'PVB','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_type_pvb'),(127,24,'Unapproved Device','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_type_unapproved_device'),(128,26,'Back Wash Supply','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_back_wash_supply'),(129,26,'Baptism Fountain','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_baptism_fountain'),(130,26,'Boiler Feed','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_boiler_feed'),(131,26,'Brine Tank','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_brine_tank'),(132,26,'Car Wash','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_car_wash'),(133,26,'Chemical Feeder','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_chemical_feeder'),(134,26,'Chiller Make Up','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_chiller_make_up'),(135,26,'Wok Range','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_wok_range'),(136,26,'Chlorine Line','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_chlorine_line'),(137,26,'Coffee Machine','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_coffee_machine'),(138,26,'Cold Feed Pedi Chair','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_cold_feed_pedi_chair'),(139,26,'Cold Water Supply','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_cold_water_supply'),(140,26,'Commercial Feed','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_commercial_feed'),(141,26,'Compressor','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_compressor'),(142,26,'Cooling Tower','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_cooling_tower'),(143,26,'Dental Equipment','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_dental_equipment'),(144,26,'Dialysis','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_dialysis'),(145,26,'Dishwasher','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_dishwasher'),(146,26,'Domestic','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_domestic'),(147,26,'Domestic By-Pass','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_domestic_by_pass'),(148,26,'Drinking Fountain','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_drinking_fountain'),(149,26,'Equipment Make-Up','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_equipment_make_up'),(150,26,'Film Processor','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_film_processor'),(151,26,'Filter System','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_filter_system'),(152,26,'Filter Water','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_filter_water'),(153,26,'Fire Anti-Freeze','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_fire_anti_freeze'),(154,26,'Fire By-Pass','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_fire_by_pass'),(155,26,'Fire Protection','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_fire_protection'),(156,26,'Radiant Heat','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_radiant_heat'),(157,26,'Fountain','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_fountain'),(158,26,'Garbage Disposal','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_garbage_disposal'),(159,26,'Generator','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_second_generator'),(160,26,'Hoodwash','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hoodwash'),(161,26,'Hose Bibb','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hose_bibb'),(162,26,'Hose Connection','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hose_connection'),(163,26,'Hose Reel','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hose_reel'),(164,26,'Hot Feed Pedi Chair','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hot_feed_pedi_chair'),(165,26,'Hot Tub','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hot_tub'),(166,26,'Hot Water Supply','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hot_water_supply'),(167,26,'Humidfier','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_humidfier'),(168,26,'HVAC Equip.','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hvac_equip'),(169,26,'Hydrant','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_hydrant'),(170,26,'Ice Machine','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_ice_machine'),(171,26,'Irrigation','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_irrigation'),(172,26,'Jockey Pump','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_jockey_pump'),(173,26,'Laboratory','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_laboratory'),(174,26,'Laundry Cold','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_laundry_cold'),(175,26,'Laundry Hot','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_laundry_hot'),(176,26,'Make Up Water','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_make_up_water'),(177,26,'Melt Tank','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_melt_tank'),(178,26,'Misters','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_misters'),(179,26,'MRI','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_mri'),(180,26,'Outside Pond','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_outside_pond'),(181,26,'Photo Developer','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_photo_developer'),(182,26,'Plaster Trimmer','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_plaster_trimmer'),(183,26,'Pond Feed','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_pond_feed'),(184,26,'Pool Feed','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_pool_feed'),(185,26,'Portable Hydrant','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_portable_hydrant'),(186,26,'Potable Water','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_potable_water'),(187,26,'Pressure Water','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_pressure_water'),(188,26,'Pressure Washer','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_pressure_washer'),(189,26,'Process Water','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_process_water'),(190,26,'Pressure Rectifier','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_pressure_rectifier'),(191,26,'Pressure Reducer','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_pressure_reducer'),(192,26,'RO System','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_ro_system'),(193,26,'SAA','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_saa'),(194,26,'Sanitation Flush','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_sanitation_flush'),(195,26,'Sillcock','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_sillcock'),(196,26,'Soap Dispenser','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_soap_dispenser'),(197,26,'Soda Equip','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_soda_equip'),(198,26,'Steamer','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_steamer'),(199,26,'Sterilizer','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_sterilizer'),(200,26,'Tank Fill','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_tank_fill'),(201,26,'Tank Washer','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_tank_washer'),(202,26,'Trash Compactor','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_trash_compactor'),(203,26,'Trash Shoot','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_trash_shoot'),(204,26,'Truck Fill','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_truck_fill'),(205,26,'Utility Sink','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_utility_sink'),(206,26,'Vaccum Pump','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_vaccum_pump'),(207,26,'Wall Hydrant','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_wall_hydrant'),(208,26,'Wash Hose','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_wash_hose'),(209,26,'Washroom','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_washroom'),(210,26,'Waste','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_waste'),(211,26,'Water Softner','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_water_softner'),(212,26,'X-Ray','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_x_ray'),(213,26,'Yard Hydrant','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_yard_hydrant'),(214,26,'Floor Heat','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_floor_heat'),(215,26,'Swim Lift','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_swim_lift'),(216,26,'Other','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_hazard_other'),(217,27,'Horizontal','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_orientation_horizontal'),(218,27,'Vertical','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_orientation_vertical'),(219,28,'Ball','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_shut_off_valve_ball'),(220,28,'Gate','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_shut_off_valve_gate'),(221,28,'OS&Y','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_shut_off_valve_os&y'),(222,28,'NRS','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_shut_off_valve_nrs'),(223,28,'Butterfly','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_shut_off_valve_butterfly'),(224,28,'Waffer','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_shut_off_valve_waffer'),(225,28,'Integrated','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_shut_off_valve_integrated'),(226,29,'Yes','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_approved_installation_yes'),(227,29,'No','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'backflow_device_approved_installation_no'),(228,31,'1\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_1'),(229,31,'1.25\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_1_dot_25'),(230,31,'1.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_1_dot_5'),(231,31,'2\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_2'),(232,31,'2.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_2_dot_5'),(233,31,'3\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_3'),(234,31,'4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_4'),(235,31,'6\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_6'),(236,31,'8\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_8'),(237,31,'10\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_10'),(238,31,'12\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_size_12'),(239,33,'1/8\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'riser_gauge_size_ips_1_slash_8'),(240,33,'1/4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',1,'riser_gauge_size_ips_1_slash_4'),(241,34,'1\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_1'),(242,34,'1.25\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_1_dot_25'),(243,34,'1.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_1_dot_5'),(244,34,'2\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_2'),(245,34,'2.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_2_dot_5'),(246,34,'3\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_3'),(247,34,'4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_4'),(248,34,'6\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_6'),(249,34,'8\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_8'),(250,34,'10\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_10'),(251,34,'12\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_size_12'),(252,35,'Propylene Glycol','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_type_propylene_glycol'),(253,35,'Glycerin','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_type_glycerin'),(254,37,'City','2018-05-25 05:08:03','2018-05-25 05:08:03',1,'anti_freeze_system_water_supply_source_city'),(255,37,'Tank','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_water_supply_source_tank'),(256,37,'Well','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_water_supply_source_well'),(257,37,'Pond','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_water_supply_source_pond'),(258,37,'Other','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'anti_freeze_system_water_supply_source_other'),(259,38,'1\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_size_1'),(260,38,'1.25\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_size_125'),(261,38,'2\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_size_2'),(262,38,'2.5\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_size_25'),(263,38,'3\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_size_3'),(264,38,'4\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_size_4'),(265,38,'6\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_size_6'),(266,38,'8\"','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_size_8'),(267,43,'City','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_water_supply_source_city'),(268,43,'Tank','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_water_supply_source_tank'),(269,43,'Pump','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_water_supply_source_pump'),(270,43,'Pond','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_water_supply_source_pond'),(271,43,'Other','2018-05-25 05:08:03','2018-05-25 05:08:03',0,'dry_valve_water_supply_source_other'),(272,45,'1.5\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_size_1_dot_5'),(273,45,'2\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_size_2'),(274,45,'2.5\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_size_2_dot_5'),(275,45,'3\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_size_3'),(276,45,'4\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_size_4'),(277,45,'6\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_size_6'),(278,47,'Flanged','2018-05-25 05:08:04','2018-05-25 05:08:04',1,'fdc_check_valve_connection_flanged'),(279,47,'Threaded','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_connection_threaded'),(280,47,'Groove','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_connection_groove'),(281,47,'Other','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_connection_other'),(282,48,'Ball Drip','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_drain_down_ball_drip'),(283,48,'Out FDC','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_drain_down_out_fdc'),(284,48,'Other','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fdc_check_valve_drain_down_other'),(285,49,'Addressable','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'addressable'),(286,49,'Conventional','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'conventional'),(287,50,'Silent Knight','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_silent_knight'),(288,50,'Fire-Lite','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_fire_lite'),(289,50,'Simplex','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_simplex'),(290,50,'Edwards','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_edwards'),(291,50,'Notifier','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_notifier'),(292,50,'Kiddie','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_kiddie'),(293,50,'Siemens','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_siemens'),(294,50,'ADT','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_adt'),(295,50,'Deelat','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_deelat'),(296,50,'Ademco','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_ademco'),(297,50,'Napco','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_napco'),(298,50,'Potter','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_potter'),(299,50,'Other','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'make_other'),(300,56,'1 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_1_lb'),(301,56,'2 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_2_lb'),(302,56,'2.5 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_2_dot_5_lb'),(303,56,'4 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_4_lb'),(304,56,'5 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_5_lb'),(305,56,'6 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_6_lb'),(306,56,'10 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_10_lb'),(307,56,'20 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_20_lb'),(308,56,'30 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_30_lb'),(309,56,'50 LB','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_size_50_lb'),(310,57,'Class A','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_type_class_a'),(311,57,'Class B','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_type_class_b'),(312,57,'Class C','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_type_class_c'),(313,57,'Class D','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_type_class_d'),(314,57,'Class K','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'fire_extinguisher_fire_extinguisher_type_class_k'),(315,59,'1\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_1'),(316,59,'1.25\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_1_dot_25'),(317,59,'1.5\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_1_dot_5'),(318,59,'2\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_2'),(319,59,'2.5\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_2_dot_5'),(320,59,'3\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_3'),(321,59,'4\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_4'),(322,59,'6\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_6'),(323,59,'8\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_8'),(324,59,'10\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_10'),(325,59,'12\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_size_12'),(326,61,'1/8\"','2018-05-25 05:08:04','2018-05-25 05:08:04',0,'section_gauge_size_ips_1_slash_8'),(327,61,'1/4\"','2018-05-25 05:08:04','2018-05-25 05:08:04',1,'section_gauge_size_ips_1_slash_4');
/*!40000 ALTER TABLE `dynamic_field_dropbox_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_type`
--

DROP TABLE IF EXISTS `dynamic_field_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_type`
--

LOCK TABLES `dynamic_field_type` WRITE;
/*!40000 ALTER TABLE `dynamic_field_type` DISABLE KEYS */;
INSERT INTO `dynamic_field_type` VALUES (1,'Text','text','2018-05-25 05:07:41','2018-05-25 05:07:41'),(2,'Dropdown','dropdown','2018-05-25 05:07:41','2018-05-25 05:07:41'),(3,'Entity','entity','2018-05-25 05:07:41','2018-05-25 05:07:41');
/*!40000 ALTER TABLE `dynamic_field_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_validation`
--

DROP TABLE IF EXISTS `dynamic_field_validation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_validation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `validation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_validation`
--

LOCK TABLES `dynamic_field_validation` WRITE;
/*!40000 ALTER TABLE `dynamic_field_validation` DISABLE KEYS */;
INSERT INTO `dynamic_field_validation` VALUES (1,'app-font--uppercase','app-font--uppercase',NULL),(2,'app-validation-digits','app-validation-digits',NULL),(3,'app-limit-digits','app-limit-digits',NULL),(4,'app-validation-phone','app-validation-phone',NULL),(5,'app-validation-currency','app-validation-currency',NULL),(6,'app-validation-email','app-validation-email',NULL),(7,'app-validation-maxLength100','app-validation-maxlength100',NULL),(8,'app-validation-dynamicZip','app-validation-dynamiczip',NULL),(9,'app-validation-maxLength100','app-validation-maxlength100',NULL),(10,'app-validation-maxLength100','app-validation-maxlength100',NULL),(11,'app-limit-digits4','app-limit-digits4',NULL),(12,'app-limit-digits3','app-limit-digits3',NULL),(13,'app-limit-number','app-limit-number',NULL);
/*!40000 ALTER TABLE `dynamic_field_validation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_value`
--

DROP TABLE IF EXISTS `dynamic_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `device_id` int(11) DEFAULT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F0552AD2443707B0` (`field_id`),
  KEY `IDX_F0552AD294A4C7D4` (`device_id`),
  KEY `IDX_F0552AD2D957CA06` (`option_value_id`),
  CONSTRAINT `FK_F0552AD2443707B0` FOREIGN KEY (`field_id`) REFERENCES `dynamic_field` (`id`),
  CONSTRAINT `FK_F0552AD294A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_F0552AD2D957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `dynamic_field_dropbox_value` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_value`
--

LOCK TABLES `dynamic_field_value` WRITE;
/*!40000 ALTER TABLE `dynamic_field_value` DISABLE KEYS */;
INSERT INTO `dynamic_field_value` VALUES (1,21,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,85),(2,22,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,101),(3,23,'1','2018-05-25 05:25:47','2018-05-25 05:25:47',1,NULL),(4,24,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,122),(5,25,'1','2018-05-25 05:25:47','2018-05-25 05:25:47',1,NULL),(6,26,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,128),(7,27,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,218),(8,28,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,220),(9,29,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,226),(10,21,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,87),(11,22,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,100),(12,23,'1','2018-05-25 05:34:23','2018-05-25 05:34:23',2,NULL),(13,24,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,121),(14,25,'1','2018-05-25 05:34:23','2018-05-25 05:34:23',2,NULL),(15,26,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,129),(16,27,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,217),(17,28,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,220),(18,29,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,227),(19,1,NULL,'2018-05-25 05:36:20','2018-05-25 05:36:20',3,3),(20,2,'2','2018-05-25 05:36:20','2018-05-25 05:36:20',3,NULL),(21,3,'2','2018-05-25 05:36:20','2018-05-25 05:36:20',3,NULL),(22,4,NULL,'2018-05-25 05:36:20','2018-05-25 05:36:20',3,14),(23,5,'2','2018-05-25 05:36:20','2018-05-25 05:36:20',3,NULL),(24,38,NULL,'2018-05-25 05:36:52','2018-05-25 05:36:52',4,261),(25,39,'3','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(26,40,'222','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(27,41,'2','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(28,42,'1','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(29,43,NULL,'2018-05-25 05:36:52','2018-05-25 05:36:52',4,268),(30,44,'2','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(31,21,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,86),(32,22,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,100),(33,23,'7','2018-05-25 05:41:10','2018-05-25 05:41:10',6,NULL),(34,24,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,122),(35,25,'9','2018-05-25 05:41:10','2018-05-25 05:41:10',6,NULL),(36,26,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,128),(37,27,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,218),(38,28,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,219),(39,29,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,226),(40,20,'888','2018-05-25 05:45:56','2018-05-25 05:45:56',7,NULL),(41,1,NULL,'2018-05-25 08:53:27','2018-05-25 08:53:27',8,5),(42,2,'2','2018-05-25 08:53:27','2018-05-25 08:53:27',8,NULL),(43,3,'2','2018-05-25 08:53:27','2018-05-25 08:53:27',8,NULL),(44,4,NULL,'2018-05-25 08:53:27','2018-05-25 08:53:27',8,12),(45,5,'2','2018-05-25 08:53:27','2018-05-25 08:53:27',8,NULL);
/*!40000 ALTER TABLE `dynamic_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_value_history`
--

DROP TABLE IF EXISTS `dynamic_field_value_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_value_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `device_id` int(11) DEFAULT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_68704F9C443707B0` (`field_id`),
  KEY `IDX_68704F9C835E0EEE` (`owner_entity_id`),
  KEY `IDX_68704F9C94A4C7D4` (`device_id`),
  KEY `IDX_68704F9CD957CA06` (`option_value_id`),
  CONSTRAINT `FK_68704F9C443707B0` FOREIGN KEY (`field_id`) REFERENCES `dynamic_field` (`id`),
  CONSTRAINT `FK_68704F9C835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `dynamic_field_value` (`id`),
  CONSTRAINT `FK_68704F9C94A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device_history` (`id`),
  CONSTRAINT `FK_68704F9CD957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `dynamic_field_dropbox_value` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_value_history`
--

LOCK TABLES `dynamic_field_value_history` WRITE;
/*!40000 ALTER TABLE `dynamic_field_value_history` DISABLE KEYS */;
INSERT INTO `dynamic_field_value_history` VALUES (1,21,1,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,85),(2,22,2,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,101),(3,23,3,'1','2018-05-25 05:25:47','2018-05-25 05:25:47',1,NULL),(4,24,4,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,122),(5,25,5,'1','2018-05-25 05:25:47','2018-05-25 05:25:47',1,NULL),(6,26,6,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,128),(7,27,7,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,218),(8,28,8,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,220),(9,29,9,NULL,'2018-05-25 05:25:47','2018-05-25 05:25:47',1,226),(10,21,10,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,87),(11,22,11,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,100),(12,23,12,'1','2018-05-25 05:34:23','2018-05-25 05:34:23',2,NULL),(13,24,13,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,121),(14,25,14,'1','2018-05-25 05:34:23','2018-05-25 05:34:23',2,NULL),(15,26,15,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,129),(16,27,16,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,217),(17,28,17,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,220),(18,29,18,NULL,'2018-05-25 05:34:23','2018-05-25 05:34:23',2,227),(19,1,19,NULL,'2018-05-25 05:36:20','2018-05-25 05:36:20',3,3),(20,2,20,'2','2018-05-25 05:36:20','2018-05-25 05:36:20',3,NULL),(21,3,21,'2','2018-05-25 05:36:20','2018-05-25 05:36:20',3,NULL),(22,4,22,NULL,'2018-05-25 05:36:20','2018-05-25 05:36:20',3,14),(23,5,23,'2','2018-05-25 05:36:20','2018-05-25 05:36:20',3,NULL),(24,38,24,NULL,'2018-05-25 05:36:52','2018-05-25 05:36:52',4,261),(25,39,25,'3','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(26,40,26,'222','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(27,41,27,'2','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(28,42,28,'1','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(29,43,29,NULL,'2018-05-25 05:36:52','2018-05-25 05:36:52',4,268),(30,44,30,'2','2018-05-25 05:36:52','2018-05-25 05:36:52',4,NULL),(31,21,31,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,86),(32,22,32,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,100),(33,23,33,'7','2018-05-25 05:41:10','2018-05-25 05:41:10',6,NULL),(34,24,34,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,122),(35,25,35,'9','2018-05-25 05:41:10','2018-05-25 05:41:10',6,NULL),(36,26,36,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,128),(37,27,37,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,218),(38,28,38,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,219),(39,29,39,NULL,'2018-05-25 05:41:10','2018-05-25 05:41:10',6,226),(40,20,40,'888','2018-05-25 05:45:56','2018-05-25 05:45:56',7,NULL),(41,1,41,NULL,'2018-05-25 08:53:27','2018-05-25 08:53:27',8,5),(42,2,42,'2','2018-05-25 08:53:27','2018-05-25 08:53:27',8,NULL),(43,3,43,'2','2018-05-25 08:53:27','2018-05-25 08:53:27',8,NULL),(44,4,44,NULL,'2018-05-25 08:53:27','2018-05-25 08:53:27',8,12),(45,5,45,'2','2018-05-25 08:53:27','2018-05-25 08:53:27',8,NULL);
/*!40000 ALTER TABLE `dynamic_field_value_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `make` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_calibrated_date` datetime DEFAULT NULL,
  `next_calibrated_date` datetime DEFAULT NULL,
  `company_supplied_gauge` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `contractor_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D338D583C54C8C93` (`type_id`),
  KEY `IDX_D338D583CB3CC8D2` (`contractor_user_id`),
  CONSTRAINT `FK_D338D583C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `equipment_type` (`id`),
  CONSTRAINT `FK_D338D583CB3CC8D2` FOREIGN KEY (`contractor_user_id`) REFERENCES `contractor_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` VALUES (1,1,'Gauge','Turbo','23232232323','2014-12-10 00:00:00','2015-12-10 00:00:00',1,'2018-05-25 05:08:03','2018-05-25 05:08:03',13),(2,1,'Gauge 1','Super','ER244234334','2016-12-10 00:00:00','2017-12-10 00:00:00',1,'2018-05-25 05:08:03','2018-05-25 05:08:03',21),(3,1,'New Gauge','Lightsaber','86654542323','2016-02-19 00:00:00','2017-02-19 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',21),(4,1,'Good Gauge 2','Death Star','45434534535','2017-07-22 00:00:00','2018-07-22 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',NULL),(5,1,'Not Bad Gauge','Millennium Falcon','TP56734334','2017-03-01 00:00:00','2018-03-01 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',22),(6,1,'Very Good Gauge','R2D2','7754543453','2016-12-12 00:00:00','2017-12-12 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',23),(7,1,'Super Good Gauge','R2D2','4545457876','2017-12-12 00:00:00','2018-12-12 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',19),(8,1,'Strange Device. Don\'t touch this','C-3PO','54654645','2018-02-02 00:00:00','2019-02-02 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',18),(9,1,'Super Device for everyone','RoboCop','5667689890','2018-03-03 00:00:00','2019-03-03 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',17),(10,1,'Main Gauge','T-1000','8785544398','2018-04-04 00:00:00','2019-04-04 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',16),(11,1,'Main Gauge for SuperAdmin','T-1000','AA0001AA','2018-05-05 00:00:00','2019-05-05 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',13),(12,1,'Main Gauge for SuperAdmin','T-800','AA0987OI','2018-03-02 00:00:00','2019-03-02 00:00:00',0,'2018-05-25 05:08:03','2018-05-25 05:08:03',15);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment_type`
--

DROP TABLE IF EXISTS `equipment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment_type`
--

LOCK TABLES `equipment_type` WRITE;
/*!40000 ALTER TABLE `equipment_type` DISABLE KEYS */;
INSERT INTO `equipment_type` VALUES (1,'Backflow Equipment','backflow equipment','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `equipment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `coordinate_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `is_final` tinyint(1) NOT NULL DEFAULT '0',
  `can_finish_wo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_3BAE0AA7A76ED395` (`user_id`),
  KEY `IDX_3BAE0AA798BBE953` (`coordinate_id`),
  KEY `IDX_3BAE0AA7C54C8C93` (`type_id`),
  KEY `IDX_3BAE0AA72C1C3467` (`workorder_id`),
  CONSTRAINT `FK_3BAE0AA72C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_3BAE0AA798BBE953` FOREIGN KEY (`coordinate_id`) REFERENCES `coordinate` (`id`),
  CONSTRAINT `FK_3BAE0AA7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_3BAE0AA7C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `event_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (2,17,1,1,1,'2018-05-25 11:30:00','2018-05-25 12:00:00','2018-05-25 05:30:29','2018-05-25 05:30:29',0,1),(3,17,1,1,2,'2018-05-31 07:00:00','2018-05-31 19:30:00','2018-05-25 05:31:53','2018-05-25 05:31:53',0,1),(4,17,1,1,3,'2018-05-25 07:00:00','2018-05-25 08:00:00','2018-05-25 05:40:27','2018-05-25 05:40:27',0,1),(5,17,1,1,4,'2018-05-25 07:00:00','2018-05-25 08:00:00','2018-05-25 05:43:41','2018-05-25 05:46:42',0,0),(6,17,1,1,5,'2018-05-25 07:00:00','2018-05-25 08:00:00','2018-05-25 05:44:48','2018-05-25 05:44:48',0,1),(7,16,1,1,4,'2018-05-25 07:00:00','2018-05-25 23:30:00','2018-05-25 05:46:42','2018-05-25 05:46:42',0,1),(8,17,1,1,6,'2018-05-25 07:00:00','2018-05-25 08:00:00','2018-05-25 05:48:33','2018-05-25 05:48:33',0,1),(9,17,1,1,7,'2018-05-25 07:00:00','2018-05-25 08:00:00','2018-05-25 05:50:59','2018-05-25 05:50:59',0,1),(10,17,1,1,2,'2018-05-25 07:00:00','2018-05-25 08:00:00','2018-05-25 07:39:25','2018-05-25 07:39:25',0,0),(11,15,1,1,1,'2018-05-25 07:00:00','2018-05-25 09:00:00','2018-05-25 07:59:58','2018-05-25 07:59:58',0,0),(12,15,1,1,2,'2018-05-25 07:00:00','2018-05-25 11:30:00','2018-05-25 08:26:07','2018-05-25 08:26:07',0,0),(13,15,1,1,8,'2018-05-25 07:00:00','2018-05-25 08:00:00','2018-05-25 08:52:51','2018-05-25 08:52:51',0,1),(14,15,1,1,9,'2018-05-25 07:00:00','2018-05-25 08:00:00','2018-05-25 08:54:29','2018-05-25 08:54:29',0,1);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_type`
--

LOCK TABLES `event_type` WRITE;
/*!40000 ALTER TABLE `event_type` DISABLE KEYS */;
INSERT INTO `event_type` VALUES (1,'Workorder','workorder'),(2,'Time off','time_off');
/*!40000 ALTER TABLE `event_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees_basis`
--

DROP TABLE IF EXISTS `fees_basis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fees_basis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees_basis`
--

LOCK TABLES `fees_basis` WRITE;
/*!40000 ALTER TABLE `fees_basis` DISABLE KEYS */;
INSERT INTO `fees_basis` VALUES (1,'per Device','per device','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,'per Site','per site','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `fees_basis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user`
--

LOCK TABLES `fos_user` WRITE;
/*!40000 ALTER TABLE `fos_user` DISABLE KEYS */;
INSERT INTO `fos_user` VALUES (1,'schur.maksim@gmail.com','schur.maksim@gmail.com','schur.maksim@gmail.com','schur.maksim@gmail.com',1,NULL,'$2y$13$GEPlrvMJD8U1IC4PtrNG/uzP6v5FcYlM2aFu/BumJ41u5zqzOKmGK',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Maksim','Schur','be3e36b5f19d9fe62e27aa54b7e2311a'),(2,'oleksandr.korchak@idapgroup.com','oleksandr.korchak@idapgroup.com','oleksandr.korchak@idapgroup.com','oleksandr.korchak@idapgroup.com',1,NULL,'$2y$13$oVnE4tZOlHqf1KVz.9skSOo07yg6qKc93kSCAwpG7t02n/GQRhL2y',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Oleksandr','Korchak','f1ebe96327d7d7462cc7ccdf471c94af'),(3,'daria.novikova@idapgroup.com','daria.novikova@idapgroup.com','daria.novikova@idapgroup.com','daria.novikova@idapgroup.com',1,NULL,'$2y$13$QG6v3rrL18yffNTud4HcxeROCcXj6tw.AijTVi3Ur.GC6dvaqVHmC',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Daria','Novikova','0cb6fd961c0a058eb706a0b42ef3d9c3'),(4,'taras.turbovets@idapgroup.com','taras.turbovets@idapgroup.com','taras.turbovets@idapgroup.com','taras.turbovets@idapgroup.com',1,NULL,'$2y$13$UFQUH0Mu0yB5Efy.hKRsT.gmRogVclDlYUqFOPY3dHE64tHui2AEu',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Taras','Turbovets','88ae44ccfb2a12c5722f8663d509a25e'),(5,'kocmohabty@yandex.ru','kocmohabty@yandex.ru','kocmohabty@yandex.ru','kocmohabty@yandex.ru',1,NULL,'$2y$13$9dOlJ55MBXec4QuYVtIEiOrLm/L0txpfchXVDRj2dWwOWjDVNVS5K','2018-05-25 08:28:57',NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Igor','Yaitskikh','753c2f2e5dbf66b9cd3a1eaf84e7cb96'),(6,'mykola.shevchuk@idapgroup.com','mykola.shevchuk@idapgroup.com','mykola.shevchuk@idapgroup.com','mykola.shevchuk@idapgroup.com',1,NULL,'$2y$13$XaSpwFsuOAjaa/4Ovf/Iaue9yqOtNm7.4NMx80VeEyfIAaVlONB4e',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Mykola','Shevchuk','f0168f1f5d5f5a942caffb720b0037dd'),(7,'pavlo.pashchevskyi@idapgroup.com','pavlo.pashchevskyi@idapgroup.com','pavlo.pashchevskyi@idapgroup.com','pavlo.pashchevskyi@idapgroup.com',1,NULL,'$2y$13$MZQv9FAGRiblyDMWghKG3uxmpceGjX94LMWWk2aWTCWzbmU216CEe',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Pavlo','Pashchevskyi','f0ce5b75ac3637a7e46696b931213de6'),(8,'anastasiya.mashoshyna@idapgroup.com','anastasiya.mashoshyna@idapgroup.com','anastasiya.mashoshyna@idapgroup.com','anastasiya.mashoshyna@idapgroup.com',1,NULL,'$2y$13$BRbJyHKo0myAJDf3jgg3Lu4xFYBH0emHyOzfGEbpwDXvvP6OX97pW',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Anastasiya','Mashoshyna','9f2ffb402e69c3e078aecc350e88e6ea'),(9,'dharbut@gmail.com','dharbut@gmail.com','dharbut@gmail.com','dharbut@gmail.com',1,NULL,'$2y$13$vT717bxnaORv1qAloWLgd.b3d1GqaqGKo9ZdzG07/VyArcYYzjfm.',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Dan','Harbut','c292f0bcc2d878fdf6694a796a5dce78'),(10,'superadmin@americanbackflowprevention.com','superadmin@americanbackflowprevention.com','superadmin@americanbackflowprevention.com','superadmin@americanbackflowprevention.com',1,NULL,'$2y$13$nyE0d4imEgqwQHRcnClqpuusGQ280WLd9MWl8oKRJxQ3PIOE0nVta',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','ABFP','SuperAdmin','e49f58cd84dc5764b790be4803ba77d4'),(11,'api@test.com','api@test.com','api@test.com','api@test.com',1,NULL,'$2y$13$YoxuDSrLh30UgOiDc0asT.kjjabkfUHia.CYshdWwbceNS2eY1VlO',NULL,NULL,NULL,'a:1:{i:0;s:8:\"ROLE_API\";}','Maksim','Schur','9c588ea5d075f3ad292b010cfab33265'),(12,'test_api1@test.com','test_api1@test.com','test_api1@test.com','test_api1@test.com',1,NULL,'$2y$13$pfMnUlNY5fNX8UrJmR4z0OlCl1T4UszBs2RZO8scilnPxSTHmA3S2',NULL,NULL,NULL,'a:1:{i:0;s:8:\"ROLE_API\";}','Test1','Test1','27ac37edf81e32abe7de0824f6da60ed'),(13,'test_api2@test.com','test_api2@test.com','test_api2@test.com','test_api2@test.com',1,NULL,'$2y$13$LPXGr3o0BegEmnbZkjBLWe76hTgTnljliPWOILX8f9k8hG9esV9.q',NULL,NULL,NULL,'a:1:{i:0;s:8:\"ROLE_API\";}','Test2','Test2','e8987bb097699e2d380b2f4c776539c7'),(14,'api2@test.com','api2@test.com','api2@test.com','api2@test.com',1,NULL,'$2y$13$p1wRt99Cu7JUKCYS6II7V.Bl5hGM0xOQ6G9xvzwAr/Q4t2PX1f0E6',NULL,NULL,NULL,'a:1:{i:0;s:8:\"ROLE_API\";}','Igor','Yaitskikh','beed1844c61e5399c90bc80c641ad08c'),(15,'ibabich88i@gmail.com','ibabich88i@gmail.com','ibabich88i@gmail.com','ibabich88i@gmail.com',1,NULL,'$2y$13$gQ7K.KRNNeWb09pJbTHq4OdW.OT/HhlNmwpd6tbrzq.y656RnvZa2',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Alex','Babich','487de1ff7a0c8db8a38fbac419aea187'),(16,'john.doe@example.com','john.doe@example.com','john.doe@example.com','john.doe@example.com',1,NULL,'$2y$13$8uokApGEoa8eUdbOK05Yu.nx0QwQUJZgTlTqjVoGVUAFopXPw4fYO',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','John','Doe','43ccc72274873618f25493668ebeeebe'),(17,'john_doe_admin@email.com','john_doe_admin@email.com','john_doe_admin@email.com','john_doe_admin@email.com',1,NULL,'$2y$13$L41HyQQuvqs2skeXZcgCY.k1z3Yn6ly9EMnJM7ROCtd4mLzv7joPG',NULL,NULL,NULL,'a:2:{i:0;s:10:\"ROLE_ADMIN\";i:1;s:12:\"ROLE_CONTACT\";}','John','Doe','9d7cb91e59da7c9407da153ab458a72d'),(18,'john_bishop@email.com','john_bishop@email.com','john_bishop@email.com','john_bishop@email.com',1,NULL,'$2y$13$CUrNueB9off5nt.N7mGQxunlGDJhqfUboSTRMKTJbjYigHAlAqQZi',NULL,NULL,NULL,'a:1:{i:0;s:12:\"ROLE_CONTACT\";}','John','Bishop','643970cd14e33015dee8cdfd6301bceb'),(19,'james_hatfield@email.com','james_hatfield@email.com','james_hatfield@email.com','james_hatfield@email.com',1,NULL,'$2y$13$g2Ly8QFEo8sgjpm9NMyy3u6EOpvtjFFrH2CRltNH6DUhoOs.tHsRS',NULL,NULL,NULL,'a:1:{i:0;s:11:\"ROLE_TESTER\";}','James','Hatfield','b0fffcdc311dc3337f6b9e6485e1a9ac'),(20,'kirk_hammet@email.com','kirk_hammet@email.com','kirk_hammet@email.com','kirk_hammet@email.com',1,NULL,'$2y$13$TOxP8RJPI8doyZdOxQmfW.lczGN.EuV.p2tEjkjUrb9IcsjUFGr5W',NULL,NULL,NULL,'a:1:{i:0;s:11:\"ROLE_TESTER\";}','Kirk','Hammet','6e896d02e3e8d72d73c9f24a653d47ab'),(21,'loes5307@comcast.net','loes5307@comcast.net','loes5307@comcast.net','loes5307@comcast.net',1,NULL,'$2y$13$4BIK4gzx7Ohsw9yS44prg.6/wgIZodLZyoIOX14qj7SBIuqOLMfSG',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','David','Loes','f91da350490e91ae190d66c32e2f8121'),(22,'svetlana.kovalchuk@idapgroup.com','svetlana.kovalchuk@idapgroup.com','svetlana.kovalchuk@idapgroup.com','svetlana.kovalchuk@idapgroup.com',1,NULL,'$2y$13$N7NeSvTpP9jtLgk4kyLPkOSABnyhqd/y9klGcSFSWViTG1LCGiTka','2018-05-25 08:51:31',NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Svetlana','Kovalchuk','50e26a3245e371dac36b8bb488988d0b'),(23,'yurii@test.com','yurii@test.com','yurii@test.com','yurii@test.com',1,NULL,'$2y$13$3umwzrW/JKn1ZjWfgYMayuCNsAeKUrdJGW0gkdSguYoj6Kum9xPh.',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Yurii','Trokhymchuk','f8512341c5f2f0153d50991f9192680a'),(24,'vlad@test.com','vlad@test.com','vlad@test.com','vlad@test.com',1,NULL,'$2y$13$0vnJSrQ5tUsMoSlyQal3VOYK.QCuudR7zHB0xFQsy7ACfnJjw7aX6','2018-05-25 07:55:01',NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Vlad','Emets','a38cac48a6b7b4a4e905bce1b15e3df2'),(25,'nosik@test.com','nosik@test.com','nosik@test.com','nosik@test.com',1,NULL,'$2y$13$BctVjy855bvkGVWb8fRgDOngjQKxrJTGmvtchdKE44i0ulj2X2yWa',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',NULL,NULL,'bbceb9f664a5d2ee457d9745807e41ca'),(26,'nikityuk@test.com','nikityuk@test.com','nikityuk@test.com','nikityuk@test.com',1,NULL,'$2y$13$Nf8DH8nHfxcId9KNLOOnV.lFqz2ODmoDlVWyzv6eis4ZgAL2zbFRm',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',NULL,NULL,'459e57b6926980d4b2ca3d9edf08a07a'),(27,'leesogonich@test.com','leesogonich@test.com','leesogonich@test.com','leesogonich@test.com',1,NULL,'$2y$13$elwSTFkKl9xz2CjKGW7/feo4y42VBzc70Jbk.j/M5cUYU0923HzMe',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',NULL,NULL,'9e7ff8b46f61b2cde712be484eb2bfd1'),(28,'markzuck@gmail.com','markzuck@gmail.com','markzuck@gmail.com','markzuck@gmail.com',1,NULL,'$2y$13$BRJsZCdvQzvovdZOu2W51.Xe6.2mmta3ta1/fNpOCDegld9bVkMf6',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Mark','Zuckerberg','328b9ac1f5d6ddf35e4253495e4842fc'),(29,'elonmusk@gmail.com','elonmusk@gmail.com','elonmusk@gmail.com','elonmusk@gmail.com',1,NULL,'$2y$13$mXvi8tCtyAGemk6gxI6X8usUDjLt3lrqurmL0KTDcV7aDenRXRKea','2018-05-25 08:54:06',NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Mark','Elon','60afd3780b25f3bf5c0d6475620bf380'),(30,'billgates@gmail.com','billgates@gmail.com','billgates@gmail.com','billgates@gmail.com',1,NULL,'$2y$13$lClwULuVGReomyDyYDdpB.LmVR/DuHJuit3XuDuieSLlfABhqr/ji','2018-05-25 08:47:47',NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Bill','Gates','27c4975fa22faa1bfa82e05c1c99459f'),(31,'sergeybrin@gmail.com','sergeybrin@gmail.com','sergeybrin@gmail.com','sergeybrin@gmail.com',1,NULL,'$2y$13$X44Ue22JBQ2P9t6qq4xgyuS0QLGkh.E1R8fLIXzYWl5xrKTkBk5gC','2018-05-25 07:41:12',NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Sergey','Brin','1811c506e496027d782493d3ed20079b'),(32,'gabenewell@gmail.com','gabenewell@gmail.com','gabenewell@gmail.com','gabenewell@gmail.com',1,NULL,'$2y$13$tNuBWKT9xyyG85jCxi5ub.P7/kh3ilOP7B8wA/y8AF84nHKAFZyPa','2018-05-25 05:59:31',NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Gabe','Newell','64297fb4ad832bcf3674f7440ed17b17'),(33,'jakfresko@gmail.com','jakfresko@gmail.com','jakfresko@gmail.com','jakfresko@gmail.com',1,NULL,'$2y$13$Yxa1/xlh.nXaSLVjIwS2WuXTyOfbClYGDgfSxIvAt0wQL8BGH15W.',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Jak','Fresko','bf721c1880de5b0d79dac5c5ec7551ca'),(34,'vdovychenko.dmytro@gmail.com','vdovychenko.dmytro@gmail.com','vdovychenko.dmytro@gmail.com','vdovychenko.dmytro@gmail.com',1,NULL,'$2y$13$/j/6RIb0UXqgAxxXlmPL/.3CM.WrFZmGJRnFMuQkwLnpp.rR4Zt7i',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Dmytro','Vdovychenko','62662e465368cf3e0dc66503907fee03');
/*!40000 ALTER TABLE `fos_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frozen_proposal`
--

DROP TABLE IF EXISTS `frozen_proposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frozen_proposal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C26948E59B6B5FBA` (`account_id`),
  KEY `IDX_C26948E5E92F8F78` (`recipient_id`),
  KEY `IDX_C26948E561220EA6` (`creator_id`),
  CONSTRAINT `FK_C26948E561220EA6` FOREIGN KEY (`creator_id`) REFERENCES `contractor_user_history` (`id`),
  CONSTRAINT `FK_C26948E59B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account_history` (`id`),
  CONSTRAINT `FK_C26948E5E92F8F78` FOREIGN KEY (`recipient_id`) REFERENCES `account_contact_person_history` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frozen_proposal`
--

LOCK TABLES `frozen_proposal` WRITE;
/*!40000 ALTER TABLE `frozen_proposal` DISABLE KEYS */;
INSERT INTO `frozen_proposal` VALUES (1,14,1,9),(2,14,1,9),(3,10,2,9),(4,21,3,9),(5,21,3,9),(6,10,2,9),(7,14,1,9),(8,14,1,9),(9,14,1,9),(10,14,1,9);
/*!40000 ALTER TABLE `frozen_proposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frozen_service_for_proposal`
--

DROP TABLE IF EXISTS `frozen_service_for_proposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frozen_service_for_proposal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_history_id` int(11) DEFAULT NULL,
  `frozen_proposal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C57D54678854BDE6` (`service_history_id`),
  KEY `IDX_C57D5467C5A6CB48` (`frozen_proposal_id`),
  CONSTRAINT `FK_C57D54678854BDE6` FOREIGN KEY (`service_history_id`) REFERENCES `service_history` (`id`),
  CONSTRAINT `FK_C57D5467C5A6CB48` FOREIGN KEY (`frozen_proposal_id`) REFERENCES `frozen_proposal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frozen_service_for_proposal`
--

LOCK TABLES `frozen_service_for_proposal` WRITE;
/*!40000 ALTER TABLE `frozen_service_for_proposal` DISABLE KEYS */;
INSERT INTO `frozen_service_for_proposal` VALUES (1,12,1),(2,11,1),(3,10,1),(4,9,1),(5,25,3),(6,24,3),(7,23,3),(8,49,4),(9,48,4),(10,47,4),(11,40,5),(12,39,5),(13,38,5),(14,37,5),(15,36,5),(16,55,6),(17,54,6),(18,59,7),(19,58,7),(20,63,8),(21,60,8),(22,68,9),(23,83,10),(24,82,10),(25,81,10),(26,80,10),(27,79,10);
/*!40000 ALTER TABLE `frozen_service_for_proposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_error_messages`
--

DROP TABLE IF EXISTS `import_error_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_error_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_error_messages`
--

LOCK TABLES `import_error_messages` WRITE;
/*!40000 ALTER TABLE `import_error_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_error_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workorder_id` int(11) DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6A2F2F952C1C3467` (`workorder_id`),
  CONSTRAINT `FK_6A2F2F952C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `letter`
--

DROP TABLE IF EXISTS `letter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `letter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8E02EE0AF4792058` (`proposal_id`),
  KEY `IDX_8E02EE0A78CED90B` (`from_id`),
  KEY `IDX_8E02EE0A30354A65` (`to_id`),
  CONSTRAINT `FK_8E02EE0A30354A65` FOREIGN KEY (`to_id`) REFERENCES `account_contact_person_history` (`id`),
  CONSTRAINT `FK_8E02EE0A78CED90B` FOREIGN KEY (`from_id`) REFERENCES `contractor_user_history` (`id`),
  CONSTRAINT `FK_8E02EE0AF4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `letter`
--

LOCK TABLES `letter` WRITE;
/*!40000 ALTER TABLE `letter` DISABLE KEYS */;
/*!40000 ALTER TABLE `letter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses`
--

DROP TABLE IF EXISTS `licenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `renewal_date` datetime DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `contractor_user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_7F320F3F457EB27E` (`named_id`),
  KEY `IDX_7F320F3F5D83CC1` (`state_id`),
  KEY `IDX_7F320F3FCB3CC8D2` (`contractor_user_id`),
  CONSTRAINT `FK_7F320F3F457EB27E` FOREIGN KEY (`named_id`) REFERENCES `licenses_named` (`id`),
  CONSTRAINT `FK_7F320F3F5D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_7F320F3FCB3CC8D2` FOREIGN KEY (`contractor_user_id`) REFERENCES `contractor_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses`
--

LOCK TABLES `licenses` WRITE;
/*!40000 ALTER TABLE `licenses` DISABLE KEYS */;
INSERT INTO `licenses` VALUES (1,1,1,'863650921346','2018-08-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',23,0),(2,2,NULL,'761355732338','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',24,0),(3,3,6,'863650921346','2018-07-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',23,0),(4,4,NULL,'521355789531','2018-09-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',24,0),(5,5,2,'345673289519','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',23,0),(6,2,NULL,'764355232345','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',18,0),(7,1,NULL,'784325537345','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',15,0),(8,1,NULL,'712325537115','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',20,0),(9,2,NULL,'712325537115','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',20,0),(10,1,NULL,'2342344323112','2018-09-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',16,0),(11,2,NULL,'612327767115','2018-10-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',16,0),(12,1,NULL,'21212124356','2018-09-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',17,0),(13,2,NULL,'12544565990','2018-10-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',17,0),(14,1,NULL,'99000876552','2018-09-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',18,0),(15,1,NULL,'99000876552','2018-09-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',19,0),(16,2,NULL,'345345343212','2018-10-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',19,0),(17,3,NULL,'712325537115','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',20,0),(18,4,NULL,'712325537115','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',20,0),(19,3,NULL,'784325537345','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',15,0),(20,4,NULL,'784325537345','2018-06-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',15,0),(21,1,NULL,'2324354435455','2018-09-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',13,0),(22,2,NULL,'3324565677231','2018-07-25 05:08:04','2018-05-25 05:08:04','2018-05-25 05:08:04',13,0);
/*!40000 ALTER TABLE `licenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_category`
--

DROP TABLE IF EXISTS `licenses_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_category`
--

LOCK TABLES `licenses_category` WRITE;
/*!40000 ALTER TABLE `licenses_category` DISABLE KEYS */;
INSERT INTO `licenses_category` VALUES (1,'Backflow','backflow','backflow_blue.svg','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,'Fire','fire','fire_blue.svg','2018-05-25 05:08:03','2018-05-25 05:08:03'),(3,'Plumbing','plumbing','tool_blue.svg','2018-05-25 05:08:03','2018-05-25 05:08:03'),(4,'Alarm','alarm','alarm_blue.svg','2018-05-25 05:08:03','2018-05-25 05:08:03'),(5,'Driver','driver','circle.svg','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `licenses_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_category_relations`
--

DROP TABLE IF EXISTS `licenses_category_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_category_relations` (
  `licensed_named_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`licensed_named_id`,`category_id`),
  KEY `IDX_4FAEBA5D43EC7B4F` (`licensed_named_id`),
  KEY `IDX_4FAEBA5D12469DE2` (`category_id`),
  CONSTRAINT `FK_4FAEBA5D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `licenses_category` (`id`),
  CONSTRAINT `FK_4FAEBA5D43EC7B4F` FOREIGN KEY (`licensed_named_id`) REFERENCES `licenses_named` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_category_relations`
--

LOCK TABLES `licenses_category_relations` WRITE;
/*!40000 ALTER TABLE `licenses_category_relations` DISABLE KEYS */;
INSERT INTO `licenses_category_relations` VALUES (1,1),(2,2),(3,3),(4,2),(4,4),(5,5);
/*!40000 ALTER TABLE `licenses_category_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_dropbox_choices`
--

DROP TABLE IF EXISTS `licenses_dropbox_choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_dropbox_choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `option_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `select_default` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EF4F4CF4443707B0` (`field_id`),
  CONSTRAINT `FK_EF4F4CF4443707B0` FOREIGN KEY (`field_id`) REFERENCES `licenses_dynamic_field` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_dropbox_choices`
--

LOCK TABLES `licenses_dropbox_choices` WRITE;
/*!40000 ALTER TABLE `licenses_dropbox_choices` DISABLE KEYS */;
INSERT INTO `licenses_dropbox_choices` VALUES (1,1,'Fire Alarms',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(2,1,'Inspection and Testing alarms',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(3,1,'Special Hazards',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(4,1,'Inspection and Testing WBS',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(5,1,'Water Based Systems Layout',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(6,2,'I',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(7,2,'II',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(8,2,'III',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(9,2,'IV',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(10,3,'Portable Fire Extinguishers',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(11,3,'Pre-Engineered Kitchen Fire Extinguishing Systems',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(12,3,'Pre-Engineered Industrial Fire Extinguishing Systems',0,'2018-05-25 05:08:04','2018-05-25 05:08:04'),(13,3,'Engineered Fire Suppression Systems',0,'2018-05-25 05:08:04','2018-05-25 05:08:04');
/*!40000 ALTER TABLE `licenses_dropbox_choices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_dynamic_field`
--

DROP TABLE IF EXISTS `licenses_dynamic_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_dynamic_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `license_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `is_show` tinyint(1) DEFAULT '1',
  `use_label` tinyint(1) DEFAULT '0',
  `label_after` tinyint(1) DEFAULT '0',
  `label_description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1C2977D4460F904B` (`license_id`),
  KEY `IDX_1C2977D4C54C8C93` (`type_id`),
  CONSTRAINT `FK_1C2977D4460F904B` FOREIGN KEY (`license_id`) REFERENCES `licenses_named` (`id`),
  CONSTRAINT `FK_1C2977D4C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `dynamic_field_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_dynamic_field`
--

LOCK TABLES `licenses_dynamic_field` WRITE;
/*!40000 ALTER TABLE `licenses_dynamic_field` DISABLE KEYS */;
INSERT INTO `licenses_dynamic_field` VALUES (1,4,2,'NICET Certification','nicet certification','2018-05-25 05:08:04','2018-05-25 05:08:04',1,0,0,NULL),(2,4,2,'NICET Level','nicet level','2018-05-25 05:08:04','2018-05-25 05:08:04',1,1,0,'Level: '),(3,2,2,'NAFED Certification','nafed certification','2018-05-25 05:08:04','2018-05-25 05:08:04',1,0,0,NULL);
/*!40000 ALTER TABLE `licenses_dynamic_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_dynamic_field_value`
--

DROP TABLE IF EXISTS `licenses_dynamic_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_dynamic_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `license_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9D146CEE443707B0` (`field_id`),
  KEY `IDX_9D146CEE460F904B` (`license_id`),
  KEY `IDX_9D146CEED957CA06` (`option_value_id`),
  CONSTRAINT `FK_9D146CEE443707B0` FOREIGN KEY (`field_id`) REFERENCES `licenses_dynamic_field` (`id`),
  CONSTRAINT `FK_9D146CEE460F904B` FOREIGN KEY (`license_id`) REFERENCES `licenses` (`id`),
  CONSTRAINT `FK_9D146CEED957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `licenses_dropbox_choices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_dynamic_field_value`
--

LOCK TABLES `licenses_dynamic_field_value` WRITE;
/*!40000 ALTER TABLE `licenses_dynamic_field_value` DISABLE KEYS */;
INSERT INTO `licenses_dynamic_field_value` VALUES (1,1,4,NULL,'2018-05-25 05:08:04','2018-05-25 05:08:04',2),(2,2,4,NULL,'2018-05-25 05:08:04','2018-05-25 05:08:04',9),(3,3,2,NULL,'2018-05-25 05:08:04','2018-05-25 05:08:04',11);
/*!40000 ALTER TABLE `licenses_dynamic_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_named`
--

DROP TABLE IF EXISTS `licenses_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_named`
--

LOCK TABLES `licenses_named` WRITE;
/*!40000 ALTER TABLE `licenses_named` DISABLE KEYS */;
INSERT INTO `licenses_named` VALUES (1,'Backflow License','backflow license','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,'NAFED License','nafed license','2018-05-25 05:08:03','2018-05-25 05:08:03'),(3,'Plumbers License','plumbers license','2018-05-25 05:08:03','2018-05-25 05:08:03'),(4,'NICET License','nicet license','2018-05-25 05:08:03','2018-05-25 05:08:03'),(5,'Drivers License','drivers license','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `licenses_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__gallery`
--

DROP TABLE IF EXISTS `media__gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__gallery`
--

LOCK TABLES `media__gallery` WRITE;
/*!40000 ALTER TABLE `media__gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `media__gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__gallery_media`
--

DROP TABLE IF EXISTS `media__gallery_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  KEY `IDX_80D4C541EA9FDD75` (`media_id`),
  CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__gallery_media`
--

LOCK TABLES `media__gallery_media` WRITE;
/*!40000 ALTER TABLE `media__gallery_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media__gallery_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__media`
--

DROP TABLE IF EXISTS `media__media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media__media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_identifier` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__media`
--

LOCK TABLES `media__media` WRITE;
/*!40000 ALTER TABLE `media__media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media__media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6BD307FC54C8C93` (`type_id`),
  CONSTRAINT `FK_B6BD307FC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `message_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,1,'warning.svg','no_autorizer','Authorizer for some divisions under this account is missing'),(2,1,'warning.svg','authorizer_invalid_address','Some Authorizers under this Account have invalid mailing address.'),(3,1,'warning.svg','no_municipality_compliance_channel_backflow','Information about upload fees is missing for some Divisions under this Account (some Municipalities related to this Account have no Compliance Channels for some divisions under this account).'),(4,2,'warning.svg','site_has_own_authorizer','This Site Account has its own Authoriser for some Divisions even thought it is under a Group Account.'),(5,1,'warning.svg','service_no_fee','Service Fee information for some of the Services under this Account is missing.');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_type`
--

DROP TABLE IF EXISTS `message_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_type`
--

LOCK TABLES `message_type` WRITE;
/*!40000 ALTER TABLE `message_type` DISABLE KEYS */;
INSERT INTO `message_type` VALUES (1,'Error','error'),(2,'Warning','warning');
/*!40000 ALTER TABLE `message_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('0001_Create_User_Entity'),('0002_Add_Field_Access_Token_For_User_Entity'),('0003_Create_Account_Entity'),('0004_Create_Account_Type_Entity'),('0005_Create_State_Entity'),('0006_Add_Field_Parent_For_Account_Entity'),('0007_Change_Not_Required_Field_Notes_For_Account'),('0008_Create_Client_Type_Entity'),('0009_Add_Deleted_Flag_To_Account_Entity'),('0010_Add_AccountHistory_Entity'),('0011_Update_AccountHistory_Entity_Author_Nullable_True'),('0012_Add_Code_To_State_Entity'),('0013_Create_Contact_Person_Entity'),('0014_Add_Field_Email_Contact_Person_Entity'),('0015_Add_Address_Entity'),('0016_Add_Field_Addreses_For_Contact_Person_Entity'),('0017_Add_AddressType_Entity'),('0018_Update_Address_Entity_Add_AddressType'),('0019_Null_Default_Field_Notes_For_ContactPerson'),('0020_Add_Null_Default_Value_For_Fields_ContactPerson'),('0021_Add_Null_Default_Value_For_Fields_Address'),('0022_Add_Null_Default_Value_For_Fields_Phone_And_Fax_Address'),('0023_Create_ContactPersonHistory_Entity'),('0024_Updated_ContactPerson_Entity_Change_Notes_Type'),('0025_Update_ContactPerson_Entity_Add_Deleted_Property'),('0026_Create_Company_Entity'),('0027_Create_CompanyHistory_Entity'),('0028_Update_Company_Entity_Change_City_Size'),('0029_Update_Company_Entity_Set_Website_Nullable_True'),('0030_Update_CompanyHistory_Entity_Set_Website_Nullable_True'),('0031_Update_Company_Entity_Set_Address_City_Zip_Nullable_True'),('0032_Update_Company_History_Entity_Set_Address_City_Zip_Nullable_True'),('0035_Add_Field_Company_For_Account_Entity'),('0036_Add_Field_Company_For_ContactPerson_Entity'),('0037_Add_Field_Company_For_ContactPersonHistory_Entity'),('0038_Add_Field_Company_For_AccountHistory_Entity'),('0039_Create_Role_Entity'),('0040_Create_Responsibility_Entity'),('0041_Create_Account_Contact_Person_Entity'),('0042_Create_Account_Contact_Person_History_Entity'),('0043_Create_DeviceCategory_Entity'),('0044_Create_DynamicFieldType_Entity'),('0045_Create_DiveceNamed_Entity_With_Relation_To_DeviceCategory_Entity'),('0046_Create_DeviceStatus_Entity'),('0047_Create_DynamicField_Add_Relation_To_DeviceNamed_And_DynamicField_Type'),('0048_Create_DynamicFieldValue_Entity_And_Relation_To_DynamicField'),('0049_Create_Device_Entity_And_Relate_To_All'),('0050_Update_Device_Entity_Add_Comment_And_NoteToTester'),('0051_Add_Icon_To_DeviceCategory_Entity'),('0052_Add_Deleted_Property_To_Device_Entity'),('0053_Create_DynamicFieldValueHistory_Entity'),('0054_Create_DeviceHistory_Entity_And_Relate_With_DynamicFieldHistory'),('0055_Create_DynamicFieldDropboxValue_And_Linked_To_DynamicField'),('0056_Linked_DynamicFieldValue_To_DynamicFieldDropboxChoices'),('0057_Linked_DynamicFieldValueHistory_To_DynamicFieldDropboxValue'),('0058_Create_DynamicFieldValidation_Entity'),('0059_Added_Parent_And_Children_Relation_To_Device_Entity'),('0060_Added_Parent_And_Children_Relation_To_DeviceHistory_Entity'),('0061_Add_Parent_Relation_To_DeviceNamed_Entity'),('0062_Add_Validation_To_DynamicField_Entity'),('0063_Add_SelectDefault_To_DynamicFieldDropboxValue_Entity'),('0064_Adde_UseLabel_LabelAfter_LabelDescription_To_DynamicField_Entity'),('0065_Add_IsShow_To_DynamicField_Entity'),('0065_Create_Contractor_Entity'),('0066_Add_Field_Address_For_Contractor'),('0067_Create_ContractorType_Entity'),('0068_Add_Field_Type_For_Contractor'),('0069_Create_ServiceFrequency_Entity'),('0070_Create_Service_Named_Entity'),('0071_Add_Relationship_To_Service_Named'),('0072_Create_ContractorService_Entity'),('0073_Add_Relationship_To_ContractorService'),('0074_Create_Service_Entity'),('0075_Add_Relationship_To_Service'),('0076_Add_ContactPersonAdresses_Table_And_Relations_To_ContactPerson'),('0077_Drop_Relation_From_Adress_To_ContactPerson_Entities'),('0078_Refactor_Account_Entity_Add_Address_Relation'),('0079_Add_Field_Sort_For_ContractorType'),('0080_Refactor_Company_Entity_Add_Address_Entity_Relation'),('0081_Refactored_AccountHistory_Entity'),('0082_Refactored_CompanyHistory_Entity'),('0083_Update_Contractor_Entity_Coi_Expiration_Nullable_True'),('0084_Add_Field_Comment_For_Service_Entity'),('0085_Update_CompanyHistory_Change_Address_Relation'),('0086_Update_AccountHistory_Change_Address_Relation'),('0087_Change_Fixed_And_Hourly_Price_And_LastTest_For_Service'),('0088_Add_ServiceHistory_Entity'),('0089_Update_ContractorService_Entity_Change_Price_Fields_Nullable_True_Add_Field_State_Many_To_One_Relation'),('0090_Set_Logo_And_Website_Nullable_For_Contractor_Entity'),('0091_Create_ContractorUser_Entity'),('0092_Add_Contractor_Field_To_ContractorUser_Entity_With_Relation_Many_To_One_To_Contractor_Entity'),('0093_Create_Municipality_Entity'),('0094_Create_LicensesNamed_Entity'),('0095_Create_Licenses_Entity_And_Relate_To_LicensesNamed_And_State'),('0096_Relate_Licenses_And_ContractorUser_Entities'),('0097_Add_Deleted_To_Licenses_Entity'),('0098_Create_Media_Gallery_GalleryMedia_Entities'),('0099_Create_LicensesCategory_Entity'),('0100_Relate_LicensesNamed_To_LicensesCategory_Entity'),('0101_Create_LicensesDynamicField_Entity_And_Relate'),('0102_Create_LicensesDynamicFieldValue_Entity_And_Relate'),('0103_Create_LicensesDropboxChoices_Entity'),('0104_Relate_LicensesDynamicFieldValue_To_LicensesDropboxChoices'),('0105_Update_LicenseNamed_Entity_Realion_To_Category'),('0106_Add_Field_Phone_For_Contractor_Entity'),('0107_Create_ContractorUserHistory_Entity'),('0108_Create_MunicipalityHistory_Entity'),('0109_Remove_Icon_From_LicensesNamed_Entity'),('0110_Add_Label_To_LicensesDynamicField'),('0111_Create_Agent_Entity'),('0112_Create_Department_Entity'),('0113_Create_ChannelNamed_Entity'),('0114_Create_ChannelDynamicField_Entity_And_Relate'),('0115_Create_ChannelDropboxChoices_And_Relate_ChannelDynamicField'),('0116_Create_FeesBasis_Entity'),('0117_Create_DepartmentChannelDynamicFieldValue_Entity_And_Relate'),('0118_Create_DepartmentChannel_Entity'),('0119_Relate_DepartmentChannelDynamicFieldValue_DepartmentChanel'),('0120_Create_Contact_Entity'),('0121_Refactor_DepartmentChannel_Entity'),('0122_Create_AgentChannel_Entity'),('0123_Create_AgentChannelDynamicFieldValue_Entity'),('0124_Create_Relation_Between_Department_And_Agent_Entities'),('0125_Add_Relations_For_AgentChannel_Entity'),('0126_Add_Relations_For_AgentChannelDynamicFieldValue_Entity'),('0127_Create_DepartmentHistory_Entity'),('0128_Updated_DepartmentChannel_Entity'),('0129_Update_AgentChannel_Entity_Field_Default'),('0130_Create_Many_To_Many_Relation_Between_Department_And_Contact_Entities_With_Department_Master'),('0131_Create_Many_To_Many_Relation_Between_Municipality_And_Contact_Entities_With_Municipality_Master'),('0132_Create_ContactType_Entity'),('0133_Create_Many_To_One_Relation_Between_Contact_And_ContactType_Entities_With_Contact_Master'),('0134_Create_Many_To_Many_Relation_Between_Agent_And_Contact_Entities_With_Agent_Master'),('0135_Change_Relation_Between_Contact_And_DeviceCategory_Entities_From_Many_To_One_To_Many_To_Many'),('0136_Add_Owner_Field_To_DepartmentChannel_Entity_Related_To_AgentChannel_Entity_As_Many_To_One'),('0137_Add_Validation_To_ChannelDynamicField_Entity'),('0138_Add_ForiginKey_Cascade_DepartmentChannelDynamicFieldValue_Entity'),('0139_Add_entityValue_Field_To_AgentChannelDynamicFieldValue_Entity'),('0140_Set_Nullable_TRUE_For_Value_Field_For_AgentChannelDynamicFieldValue_Entity'),('0141_Set_NULLABLE_TRUE_For_isDefault_Field_AgentChannel_Entity'),('0142_Added_Municipality_Field_To_Account_Entity_And_Relationship'),('0143_Create_EquipmentType_Entity'),('0144_Create_Equipment_Entity_And_Relate_EquipmentType'),('0145_Relate_Equipment_And_ContractorUser_Entities'),('0146_Create_Opportunity_Entity'),('0147_Create_Opportunity_Type_And_Status_Entities'),('0148_Add_Relationship_Opportunity_Entity'),('0149_Add_Filds_And_Relationship_Service_And_Opportunity_Entities'),('0150_Add_Nullable_Price_Field_Opportunity_Entity'),('0151_Add_OldestOpportunityDate_Field_Account_Entity'),('0152_Create_ProposalType_Entity'),('0153_Create_Proposal_Entity'),('0154_Add_Proposal_Many_To_One_Relation_To_Service_Entity'),('0155_Create_ProposalStatus_Entity'),('0156_Add_ProposalStatus_Many_To_One_Relation_To_Proposal_Entity'),('0157_Add_ParentAccount_Field_Opportunity_Entity'),('0158_Add_Oldest_Opp_Divicion_Fields_Account_Entity'),('0159_Add_Some_Fields_Proposal_Entity'),('0160_Add_Fields_DateCreate_And_DateUpdate_For_Proposal_Entity'),('0161_Add_OldestProposalDate_Fields_Account_Entity'),('0162_Add_Recipient_Field_Proposal_Entity'),('0163_Add_DepartmentChannel_Field_Service_Entity'),('0164_Create_MessageType_Entity'),('0165_Create_Message_Entity'),('0166_Add_SendingAddress_To_Proposal_Entity'),('0167_Add_Messages_Many_To_Many_Relation_To_Account_Entity'),('0168_Add_SendingAddressDescription_To_Proposal_Entity'),('0169_Create_ProposalLogType_Entity'),('0170_Create_ProposalAction_Entity'),('0171_Create_ProposalLog_Entity'),('0172_Add_Actions_Field_To_ProposalLog_Entity_For_Many_To_Many_Relation_To_ProposalAction_Entity'),('0173_Change_Relations_To_History_In_Proposal_Entity'),('0174_Add_Fields_Authorizer_Access_AccessPrimary_Payment_PaymentPrimary'),('0175_Add_ManyToMany_Field_Responsibilities_To_DeviceCategory_For_AccountContactPerson_Entity'),('0176_Delete_Fields_Role_And_Responsibility_For_AccountContactPerson_Entity'),('0177_Add_Fields_Authorizer_Access_AccessPrimary_Payment_PaymentPrimary_For_History'),('0178_Delete_ContactPersonRole_Entity'),('0179_Delete_ContactPersonResponsibility_Entity'),('0180_Add_Field_Weight_For_DeviceCategory_Entity'),('0181_Add_Field_DateCreate_ServiceHistory_Entity'),('0182_Change_ContactPerson_On_AccountContactPerson_Proposal_Entity'),('0183_Remove_SendingAdress_And_SendingAdressDescription_From_Proposal_Entity'),('0184_Add_SendingAddress_And_SendingAddressDescription_To_AccountContactPerson_Entity'),('0185_Add_SendingAddress_And_SendingAddressDescription_To_AccountContactPersonHistory_Entity'),('0186_Add_Field_Active_For_DepartmentChanel_Entity'),('0187_Add_SendingAddress_And_SendingAddressDescription_To_Proposal_Entity'),('0188_Reemove_SendingAddressDescription_From_AccountContactPerson_Entity'),('0189_Add_Field_Report_For_Proposal_Entity'),('0190_Delete_Field_Addresses_For_ContactPerson_Entity'),('0191_Add_Sort_Field_To_AddressType_Entity'),('0192_Add_Field_PersonalAddress_To_ContactPerson_Entity'),('0193_Create_Letter_Entity'),('0194_Add_Website_Field_To_Account_Entity'),('0195_Add_Website_Field_To_AccountHistory_Entity'),('0196_Add_FirstSendingDate_Field_To_Proposal_Entity'),('0197_Change_From_To_Fields_Letter_Entity'),('0198_Create_ProposalHistory_Entity'),('0199_Create_ProcessingStats_Entity'),('0200_Create_ProcessingStatsType'),('0201_Relate_ProcessingStats_And_ProcessingStatsType_Entities'),('0202_Add_Priority_Integer_Field_To_ProposalStatus_Entity'),('0203_Add_Letter_Field_To_ProposalLog_Entity'),('0204_Add_Message_String_Field_To_ProposalLog_Entity'),('0205_Create_ServiceStatus_Entity'),('0206_Add_Status_Field_To_Service_Entity_For_ServiceStatus_Many_To_One_Relation'),('0207_Remove_Message_From_ProposalLogType_Entity'),('0208_Align_Service_And_ServiceHistory_Entities'),('0209_Delete_Relation_Opportunity_To_ServiceHistory_Entity'),('0210_Create_WorkorderStatus_Entity'),('0211_Create_WorkorderLog_Entity'),('0212_Create_Workorder_Entity'),('0213_Add_Workorder_Field_To_WorkorderLog_Entity_For_Workorder_Many_To_One_Relation'),('0214_Add_Workorder_Field_To_ServiceHistory_Entity_For_Workorder_Many_To_One_Relation'),('0217_Add_Opportunity_Field_To_ServiceHistory_Entity_For_Opportunity_Many_To_One_Relation'),('0218_Create_DepartmentChannelHistory_Entity'),('0219_Field_Attachment_Became_Nullable_Letter_Entity'),('0220_Create_PaymentMethod_Entity'),('0221_Changed_Field_Contact_Person_To_Contact_Person_History_AccountContactPersonHistory_Entity'),('0222_Update_Workorder_Service_And_ServiceHistory_Entity'),('0223_Add_InternalComment_Property_To_WorkOrder_Entity'),('0227_Add_Field_Billing_Address_To_Account_AccountHistory_Entities'),('0228_Add_Authorizer_Property_To_Workorder_Entity'),('0229_Add_PaymentMethod_Property_To_Workorder_Entity'),('0230_Create_FrozenServiceForProposal_Entity'),('0231_Delete_Service_Field_From_FrozenServiceForProposal_Entity'),('0232_Add_Cod_Boolean_Field_To_Account_Entity'),('0233_Add_Cod_Boolean_Field_To_AccountHistory_Entity'),('0234_Add_DateScheduled_Field_To_WorkOrder_Entity'),('0235_Create_Coordinate_Entity'),('0236_Create_WorkorderLogType_Entity'),('0237_Add_Status_Property_To_WorkorderLog_Entity'),('0238_Change_Account_Field_Type_In_Workorder_Entity_From_AccountHistory_To_Account'),('0239_Change_Workorder_Entity_Add_StartScheduledDate_And_FinishScheduledDate'),('0240_Refactor_Workorder_Entity_Add_ScheduledFrom_And_ScheduledTo_Properties'),('0241_Add_Field_Invoice_To_Workorder_Entity'),('0242_Add_FrozenServiceForWorkorder_Entity_And_Linked_To_Other_Entities'),('0243_Related_Workorder_And_FrozenServiceForWorkorder_Entities'),('0244_Refactor_Relate_Workorder_And_FrozenServiceForWorkorder_Entities'),('0245_Create_EventType_Entity'),('0246_Create_Event_Entity'),('0247_Create_WorkorderFrozenContent_Entity'),('0248_Create_Invoices_Entity'),('0249_ReRelate_WorkorderFrozenContent_Entity_To_DeviceHistory_Entity'),('0250_Added_IsFrozen_To_Workorder_Entity'),('0251_Add_Default_Zero_Value_To_Upload_And_Processing_Fees_Of_DepartmentChannel_Entity'),('0252_Add_Default_Zero_Value_To_Upload_And_Processing_Fees_Of_DepartmentChannelHistory_Entity'),('0253_Add_AccountAuthorizer_To_Workorder_Entity'),('0254_Add_OldCmpanyId_To_Company_Entity'),('0255_Add_OldAccountId_To_Account_Entity'),('0257_Create_ImportErrorMessages_Entity'),('0258_Create_AccountBuildingType_Entity'),('0259_Add_IsHistoryProcessed_To_Company_Entity'),('0260_Add_IsHistoryProcessed_And_IsMessagesProcessed_To_Account_Entity'),('0261_Add_IsHistoryProcessed_To_ContactPerson_Entity'),('0262_Add_IsHistoryProcessed_To_AccountContactPerson_Entity'),('0263_Add_OldLinkId_To_Account_Entity'),('0264_Add_SiteSendTo_To_Account_Entity'),('0265_Add_Address_MunicipalityPhone_MunicipalityFax_MunicipalityOldId_To_Municipality_Entity'),('0266_Add_OldSistemId_Comment_Fax_Email_To_Contractor_Entity'),('0267_Make_Website_And_Phone_For_Agent_Entity_Nullable_True'),('0268_Change_BillingAddress_To_OldSystemMasLevelAddress_Account_Entity'),('0269_Make_LastName_For_Contact_Entity_Nullable_True'),('0270_Add_Field_OldDeviceId_To_Device_Entity'),('0271_Update_MunicipalityHistory_Entity_To_Actual_State'),('0272_Add_IsHistoryProcessing_To_Device_Municipality_Contractor_ContractorUser_Entities'),('0273_Add_OldBackflowwDepartmentId_OldFireDepartmentId_IsMixedMunicipality_To_Municipality_Entity'),('0274_Add_Alias_To_DynamicFieldDropboxChoices_Entity'),('0279_Add_AlarmPanel_Field_To_Device_Entity'),('0280_Add_Coordinate_To_Account_Entity'),('0281_Add_SourceEntity_Parent_To_Company_Entity'),('0282_Add_ReportStatus_Entity'),('0283_Add_Report_Entity'),('0284_Add_IsNeedRepair_For_Report_Entity'),('0285_Add_LastReport_Relation_To_Service_And_ServiceHistory_Entity'),('0286_Add_QuestionType_Entity'),('0287_Add_Question_Entity'),('0288_Add_Relation_Question_To_QuestionType'),('0289_Add_Answer_Entity'),('0290_Add_Relation_Answer_To_Question'),('0291_Add_Relation_Answer_To_Report'),('0292_Add_RangeQuestionStatus_Entity'),('0293_Add_RangeQuestion_Entity'),('0294_Add_positiveStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0295_Add_lowerNegativeStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0296_Add_upperNegativeStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0297_Add_failAnywayStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0298_Add_ServicePatern_Entity'),('0299_Add_servicePatern_Field_To_RangeQuestion_Entity_To_Relate_It_To_ServicePatern_Entity_As_Many_To_One'),('0300_Add_RangeResult_Entity'),('0301_Add_rangeQuestion_Field_To_RangeResult_Entity_To_Relate_It_To_RangeQuestion_Entity_As_Many_To_One'),('0302_Add_rangeQuestionStatus_Field_To_RangeResult_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0303_Add_report_Field_To_RangeResult_Entity_To_Relate_It_To_Report_Entity_As_Many_To_One'),('0304_Add_deficiency_Field_To_Service_Entity'),('0305_Add_IsFrozen_Boolean_Field_To_Proposal_Entity'),('0306_Add_Nullable_Default_For_Fields_ServicesCount_DevicesCount_SitesCount_To_Proposal_Entity'),('0307_Add_Nullable_Default_For_Fields_ServicesCount_DevicesCount_SitesCount_To_Proposal_Entity'),('0310_Add_Clone_Field_To_Proposal_Entity_To_Relate_It_To_Itself_As_Many_To_One'),('0311_Add_Cloned_Field_To_Proposal_Entity_To_Relate_It_To_Itself_As_Many_To_One'),('0312_Create_NotIncluded_Entity'),('0313_Create_SpecialNotification_Entity'),('0314_Add_division_Field_To_Relate_It_To_DeviceCategory_Entity_As_Many_To_One_Add_proposals_Field_To_Relate_It_To_Proposal_Entity_As_Many_To_Many'),('0315_Add_specialNotification_Field_To_Relate_It_To_Proposal_Entity_And_SpecialNotification_Entity_As_Many_To_Many'),('0316_Add_Clone_Field_To_ProposalHistory_Entity_To_Relate_It_To_Proposal_Entity_As_Many_To_One'),('0317_Add_Cloned_Field_To_ProposalHistory_Entity_To_Relate_It_To_Proposal_Entity_As_Many_To_One'),('0318_Create_PaymentTerm_Entity'),('0319_Add_PaymentTerm_Field_To_Account_Entity_To_Relate_It_To_PaymentTerm_Entity_As_Many_To_One'),('0320_Add_PaymentTerm_Field_To_Workorder_Entity_To_Relate_It_To_PaymentTerm_Entity_As_Many_To_One'),('0321_Add_CssClass_Nullable_Field_To_ServiceStatus_Entity'),('0321_Delete_Deficiency_To_Service_Add_To_ServiceName_Entity'),('0322_Add_IsNeedSendMunicipalityReport_To_ServiceNamed_Entity'),('0323_Add_Some_Fields_To_Proposal_Entity'),('0324_Fix_ServiceHistory_Entity'),('0325_Change_Relationship_SpecialNotification_And_Proposal_Entities'),('0326_Create_RepairDeviceInfo_Entity'),('0327_Drop_Field_RepairDeviceInfo_Entity'),('0328_Add_Properties_Field_To_Proposal_Entity'),('0329_Add_CssClass_Field_To_WorkorderStatus_Entity'),('0330_Add_CssClass_Field_To_ProposalStatus_Entity'),('0331_Add_PaymentTerms_To_AccountHistory_Entity'),('0332_Change_Relations_In_Proposal_Entity_From_History_To_Actual'),('0334_Drop_TypeId_Field_From_ProposalLog_Entity'),('0335_Drop_ProposalAction_Entity_Drop_ProposalLogsActionTable'),('0336_Create_ProposalLogsActions_Table'),('0337_Add_Link_Field_Add_IsActive_Field_Drop_Route_Field_To_ProposalLoaAction_Entity'),('0338_Drop_ProposalLogType_Table'),('0339_Create_Proposal_Log_Action_Named_Table'),('0340_Add_Named_Field_To_Relate_It_To_ProposalLogAction_Entity_As_Many_To_One'),('0341_Add_Title_Field_To_Device_Entity'),('0342_Add_SubtotalMunicAndProcFee_DeviceTotal_RepairDeviceInfo_Entity'),('0343_Add_Sort_Field_to_DeviceNamed_Entity'),('0344_Create_FrozenProposal_Entity'),('0345_Replace_Proposal_Many_To_One_Relation_With_FrozenProposal_Many_To_One_Relation_In_FrozenServiceForProposal_Entity'),('0346_Add_Frozen_Field_To_Proposal_Entity_To_Relate_It_To_FrozenProposal_Entity_As_Many_To_One'),('0347_Change_History_Relations_To_Actual_Relations_In_ProposalHistory_Entity'),('0348_Add_EstimationTime_Field_To_ContactorService_Entity'),('0349_Add_IsNeedMunicipalityFee_Field_To_ServiceNamed_Entity'),('0350_Create_ProposalLifeCycle_Entity'),('0351_Add_lifeCycle_Field_To_Proposal_Entity_To_Relate_It_To_ProposalLifeCycle_Entity_As_Many_To_One'),('0352_Add_AllTotalMunicAndProcFee_AllTotalRepairsPrice_AllGrandTotalPrice_Fields_Proposal_Entity'),('0353_Add_Relationship_Between_Proposal_And_NotIncluded_Entities'),('0354_Add_CanFinishWO_To_Event_Entity'),('0355_Add_Field_ContractorService_To_Service_Entity'),('0356_Add_Field_ContractorService_To_ServiceHistory_Entity'),('0357_Changing_Relation_Between_Workorder_And_Proposal_Entities'),('0358_Add_LastEditor_Property_To_Device_Entity'),('0359_Add_Relationship_Proposal_WorkorderLog_Entities'),('0360_Create_WorkorderDeviceInfo_Entity'),('0361_Add_MounthFrequency_To_ServiceFriquency_Entity'),('0362_Remove_Cod_Filed_From_Account_Entity'),('0363_Add_SpecialDiscount_Field_To_Account_Entity'),('0364_Remove_Cod_Filed_From_AccountHistory_Entity'),('0365_Add_SpecialDiscount_Field_To_AccountHistory_Entity'),('0366_Add_LastTester_Field_To_Service_Entity'),('0367_Add_Field_Level_To_DeviceNamed_Entity'),('0368_Change_Link_Proposal_To_ProposalHistory_For_ServiceHistory_Entity'),('0369_Add_Field_TotalEstimationTime_To_Proposal_Entity'),('0370_Add_Field_TotalEstimationTime_To_ProposalHistory_Entity'),('0371_Add_Fields_TotalEstimationTimeInpections_And_Total_Estimation_Time_Repairs_To_WorkOrder_Entity'),('0373_Add_FinishTime_Field_To_Report_Entity'),('0374_Add_AdditionalComment_Field_To_Workorder_Entity'),('0375_Add_AfterHoursWorkCost_Field_To_Workorder_Entity'),('0376_Add_AdditionalServicesCost_Field_To_Workorder_Entity'),('0377_Add_Discount_Field_To_Workorder_Entity'),('0378_Add_Signature_Location_FinishTime_Fields_Workorder_Entity'),('0379_Add_ManyToMany_Field_notIncludedItems_To_WorkOrderEntity'),('0380_Add_TotalInspectionsFee_Field_To_Workorder_Entity'),('0381_Add_TotalRepairsPrice_Field_To_Workorder_Entity'),('0382_Add_TotalMunicAndProcFee_Field_To_Workorder_Entity'),('0383_Create_WorkorderType_Entity'),('0384_Remove_TotalInspectionsFee_Field_To_Workorder_Entity'),('0385_Add_TotalInspectionsCount_Field_To_Workorder_Entity'),('0386_Add_TotalRepairsCount_Field_To_Workorder_Entity'),('0387_Add_TotalPrice_Field_To_Workorder_Entity'),('0389_Add_DateCreate_And_DateUpdate_To_Service_Entity'),('0390_Add_DateUpdate_To_ServiceHistory_Entity'),('0391_Add_Notes_Field_To_AccountContactPerson_Entity'),('0392_Add_Notes_Field_To_AccountContactPersonHistory_Entity'),('0396_Add_Comment_Field_To_Account_Entity'),('0398_Add_PrintTemplate_Property_To_Report_Entity'),('0399_Add_Finisher_Property_To_Workorder_Entity'),('056_Add_NumContact_sourceEntity_SourceID_To_ContactPerson_Entity');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipalities_contacts`
--

DROP TABLE IF EXISTS `municipalities_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `municipalities_contacts` (
  `municipality_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`municipality_id`,`contact_id`),
  KEY `IDX_20E5CE8CAE6F181C` (`municipality_id`),
  KEY `IDX_20E5CE8CE7A1254A` (`contact_id`),
  CONSTRAINT `FK_20E5CE8CAE6F181C` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`),
  CONSTRAINT `FK_20E5CE8CE7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipalities_contacts`
--

LOCK TABLES `municipalities_contacts` WRITE;
/*!40000 ALTER TABLE `municipalities_contacts` DISABLE KEYS */;
INSERT INTO `municipalities_contacts` VALUES (1,6),(1,7),(2,5);
/*!40000 ALTER TABLE `municipalities_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipality`
--

DROP TABLE IF EXISTS `municipality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `municipality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `municipality_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `municipality_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_municipality_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `old_backflow_department_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_fire_department_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_mixed_municipality` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_C6F56628F5B7AF75` (`address_id`),
  CONSTRAINT `FK_C6F56628F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipality`
--

LOCK TABLES `municipality` WRITE;
/*!40000 ALTER TABLE `municipality` DISABLE KEYS */;
INSERT INTO `municipality` VALUES (1,'City of New York','http://www1.nyc.gov/',NULL,NULL,NULL,NULL,0,NULL,NULL,0),(2,'City of Kyiv','https://kyivcity.gov.ua/',NULL,NULL,NULL,NULL,0,NULL,NULL,0),(3,'City of Portland','http://www.cityofportland.org',NULL,NULL,NULL,NULL,0,NULL,NULL,0);
/*!40000 ALTER TABLE `municipality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipality_history`
--

DROP TABLE IF EXISTS `municipality_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `municipality_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_save` datetime NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `municipality_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `municipality_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B20D04DBBDAFD8C8` (`author`),
  KEY `IDX_B20D04DB835E0EEE` (`owner_entity_id`),
  KEY `IDX_B20D04DBF5B7AF75` (`address_id`),
  CONSTRAINT `FK_B20D04DB835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `municipality` (`id`),
  CONSTRAINT `FK_B20D04DBBDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_B20D04DBF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipality_history`
--

LOCK TABLES `municipality_history` WRITE;
/*!40000 ALTER TABLE `municipality_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `municipality_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notIncludedItems_proposals`
--

DROP TABLE IF EXISTS `notIncludedItems_proposals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notIncludedItems_proposals` (
  `proposal_id` int(11) NOT NULL,
  `not_included_id` int(11) NOT NULL,
  PRIMARY KEY (`proposal_id`,`not_included_id`),
  KEY `IDX_1C5646E9F4792058` (`proposal_id`),
  KEY `IDX_1C5646E9C1CB14A1` (`not_included_id`),
  CONSTRAINT `FK_1C5646E9C1CB14A1` FOREIGN KEY (`not_included_id`) REFERENCES `not_included` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1C5646E9F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notIncludedItems_proposals`
--

LOCK TABLES `notIncludedItems_proposals` WRITE;
/*!40000 ALTER TABLE `notIncludedItems_proposals` DISABLE KEYS */;
INSERT INTO `notIncludedItems_proposals` VALUES (9,1),(10,5),(10,8);
/*!40000 ALTER TABLE `notIncludedItems_proposals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notIncludedItems_workorders`
--

DROP TABLE IF EXISTS `notIncludedItems_workorders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notIncludedItems_workorders` (
  `workorder_id` int(11) NOT NULL,
  `not_included_id` int(11) NOT NULL,
  PRIMARY KEY (`workorder_id`,`not_included_id`),
  KEY `IDX_E384424C2C1C3467` (`workorder_id`),
  KEY `IDX_E384424CC1CB14A1` (`not_included_id`),
  CONSTRAINT `FK_E384424C2C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E384424CC1CB14A1` FOREIGN KEY (`not_included_id`) REFERENCES `not_included` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notIncludedItems_workorders`
--

LOCK TABLES `notIncludedItems_workorders` WRITE;
/*!40000 ALTER TABLE `notIncludedItems_workorders` DISABLE KEYS */;
INSERT INTO `notIncludedItems_workorders` VALUES (4,1),(4,2),(4,3),(5,4),(5,5),(5,6),(5,7),(5,8),(6,3),(8,1),(9,5),(9,8);
/*!40000 ALTER TABLE `notIncludedItems_workorders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `not_included`
--

DROP TABLE IF EXISTS `not_included`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `not_included` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AC36D2A341859289` (`division_id`),
  CONSTRAINT `FK_AC36D2A341859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `not_included`
--

LOCK TABLES `not_included` WRITE;
/*!40000 ALTER TABLE `not_included` DISABLE KEYS */;
INSERT INTO `not_included` VALUES (1,1,'Service Valves Issues','service_valves_issues','2018-05-25 05:08:03','2018-05-25 05:08:03'),(2,1,'Permits or Drawings','permits_or_drawings','2018-05-25 05:08:03','2018-05-25 05:08:03'),(3,1,'Hards Parts (if needed)','hards_parts_if_needed','2018-05-25 05:08:03','2018-05-25 05:08:03'),(4,2,'Any tamper switches','any_tamper_switches','2018-05-25 05:08:03','2018-05-25 05:08:03'),(5,2,'Insulation','insulation','2018-05-25 05:08:03','2018-05-25 05:08:03'),(6,2,'Painting of pipes','painting_of_pipes','2018-05-25 05:08:03','2018-05-25 05:08:03'),(7,2,'System piping issues','system_piping_issues','2018-05-25 05:08:03','2018-05-25 05:08:03'),(8,2,'Hydraulic calculations','hydraulic_calculations','2018-05-25 05:08:03','2018-05-25 05:08:03'),(9,3,'Plumbing Proposal Not Included Item one','plumbing_proposal_not_included_item_one','2018-05-25 05:08:03','2018-05-25 05:08:03'),(10,3,'Plumbing Proposal Not Included Item two','plumbing_proposal_not_included_item_two','2018-05-25 05:08:03','2018-05-25 05:08:03'),(11,3,'Plumbing Proposal Not Included Item three','plumbing_proposal_not_included_item_three','2018-05-25 05:08:03','2018-05-25 05:08:03'),(12,3,'Plumbing Proposal Not Included Item four','plumbing_proposal_not_included_item_four','2018-05-25 05:08:03','2018-05-25 05:08:03'),(13,3,'Plumbing Proposal Not Included Item five','plumbing_proposal_not_included_item_five','2018-05-25 05:08:03','2018-05-25 05:08:03'),(14,4,'Alarm Proposal Not Included Item one','alarm_proposal_not_included_item_one','2018-05-25 05:08:03','2018-05-25 05:08:03'),(15,4,'Alarm Proposal Not Included Item two','alarm_proposal_not_included_item_two','2018-05-25 05:08:03','2018-05-25 05:08:03'),(16,4,'Alarm Proposal Not Included Item three','alarm_proposal_not_included_item_three','2018-05-25 05:08:03','2018-05-25 05:08:03'),(17,4,'Alarm Proposal Not Included Item four','alarm_proposal_not_included_item_four','2018-05-25 05:08:03','2018-05-25 05:08:03'),(18,4,'Alarm Proposal Not Included Item five','alarm_proposal_not_included_item_five','2018-05-25 05:08:03','2018-05-25 05:08:03');
/*!40000 ALTER TABLE `not_included` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunity`
--

DROP TABLE IF EXISTS `opportunity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `services_count` int(11) NOT NULL,
  `devices_count` int(11) NOT NULL,
  `sites_count` int(11) NOT NULL,
  `price` double DEFAULT NULL,
  `execution_services_date` date NOT NULL,
  `division` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `parent_account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8389C3D710174714` (`division`),
  KEY `IDX_8389C3D78CDE5729` (`type`),
  KEY `IDX_8389C3D77B00651C` (`status`),
  KEY `IDX_8389C3D77D3656A4` (`account`),
  KEY `IDX_8389C3D7F7E22E2` (`parent_account`),
  CONSTRAINT `FK_8389C3D710174714` FOREIGN KEY (`division`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_8389C3D77B00651C` FOREIGN KEY (`status`) REFERENCES `opportunity_status` (`id`),
  CONSTRAINT `FK_8389C3D77D3656A4` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_8389C3D78CDE5729` FOREIGN KEY (`type`) REFERENCES `opportunity_type` (`id`),
  CONSTRAINT `FK_8389C3D7F7E22E2` FOREIGN KEY (`parent_account`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunity`
--

LOCK TABLES `opportunity` WRITE;
/*!40000 ALTER TABLE `opportunity` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunity_status`
--

DROP TABLE IF EXISTS `opportunity_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunity_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alisa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunity_status`
--

LOCK TABLES `opportunity_status` WRITE;
/*!40000 ALTER TABLE `opportunity_status` DISABLE KEYS */;
INSERT INTO `opportunity_status` VALUES (1,'Past Due','past_due',1),(2,'RN To Be Created','rn_to_be_created',2);
/*!40000 ALTER TABLE `opportunity_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunity_type`
--

DROP TABLE IF EXISTS `opportunity_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunity_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alisa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunity_type`
--

LOCK TABLES `opportunity_type` WRITE;
/*!40000 ALTER TABLE `opportunity_type` DISABLE KEYS */;
INSERT INTO `opportunity_type` VALUES (1,'Retest','retest'),(2,'Repair','repair');
/*!40000 ALTER TABLE `opportunity_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_method`
--

DROP TABLE IF EXISTS `payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_method`
--

LOCK TABLES `payment_method` WRITE;
/*!40000 ALTER TABLE `payment_method` DISABLE KEYS */;
INSERT INTO `payment_method` VALUES (1,'Bill','bill',1),(2,'Cash','cash',0),(3,'Check','check',0),(4,'Credit Card','credit_card',0);
/*!40000 ALTER TABLE `payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_term`
--

DROP TABLE IF EXISTS `payment_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_term`
--

LOCK TABLES `payment_term` WRITE;
/*!40000 ALTER TABLE `payment_term` DISABLE KEYS */;
INSERT INTO `payment_term` VALUES (1,'Net 15 days. Our price is firm for 30 days.','net_fifteen_days'),(2,'Net 30 days. Our price is firm for 30 days.','net_thirty_days'),(3,'COD','cod'),(4,'Paid in full before work can be started.','full_paid_before_started'),(5,'50% Deposit Die Before Work Can Start.','fifty_percent_deposit');
/*!40000 ALTER TABLE `payment_term` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processing_stats`
--

DROP TABLE IF EXISTS `processing_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processing_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2935D2C7C54C8C93` (`type_id`),
  CONSTRAINT `FK_2935D2C7C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `processing_stats_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processing_stats`
--

LOCK TABLES `processing_stats` WRITE;
/*!40000 ALTER TABLE `processing_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `processing_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processing_stats_type`
--

DROP TABLE IF EXISTS `processing_stats_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processing_stats_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processing_stats_type`
--

LOCK TABLES `processing_stats_type` WRITE;
/*!40000 ALTER TABLE `processing_stats_type` DISABLE KEYS */;
INSERT INTO `processing_stats_type` VALUES (1,'Info','info'),(2,'Warning','warning'),(3,'Error','error');
/*!40000 ALTER TABLE `processing_stats_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal`
--

DROP TABLE IF EXISTS `proposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `date_of_proposal` date DEFAULT NULL,
  `grand_total` double DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `reference_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_count` int(11) DEFAULT NULL,
  `devices_count` int(11) DEFAULT NULL,
  `sites_count` int(11) DEFAULT NULL,
  `earliest_due_date` date DEFAULT NULL,
  `service_total_fee` double DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `sending_address_id` int(11) DEFAULT NULL,
  `sending_address_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_sending_date` datetime DEFAULT NULL,
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0',
  `clone_id` int(11) DEFAULT NULL,
  `cloned_id` int(11) DEFAULT NULL,
  `after_hours_work_cost` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_comment` longtext COLLATE utf8_unicode_ci,
  `frozen_id` int(11) DEFAULT NULL,
  `life_cycle_id` int(11) DEFAULT NULL,
  `all_total_munic_and_proc_fee` double DEFAULT NULL,
  `all_total_repairs_price` double DEFAULT NULL,
  `all_grand_total_price` double DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `total_estimation_time` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BFE5947241859289` (`division_id`),
  KEY `IDX_BFE594729B6B5FBA` (`account_id`),
  KEY `IDX_BFE59472C54C8C93` (`type_id`),
  KEY `IDX_BFE5947261220EA6` (`creator_id`),
  KEY `IDX_BFE594726BF700BD` (`status_id`),
  KEY `IDX_BFE594724F8A983C` (`contact_person_id`),
  KEY `IDX_BFE594726510ABA8` (`sending_address_id`),
  KEY `IDX_BFE594723D083110` (`clone_id`),
  KEY `IDX_BFE594723735CA56` (`cloned_id`),
  KEY `IDX_BFE59472E3690CE9` (`frozen_id`),
  KEY `IDX_BFE594726C4CA05D` (`life_cycle_id`),
  KEY `IDX_BFE594722C1C3467` (`workorder_id`),
  CONSTRAINT `FK_BFE594722C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_BFE594723735CA56` FOREIGN KEY (`cloned_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_BFE594723D083110` FOREIGN KEY (`clone_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_BFE5947241859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_BFE594724F8A983C` FOREIGN KEY (`contact_person_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_BFE5947261220EA6` FOREIGN KEY (`creator_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_BFE594726510ABA8` FOREIGN KEY (`sending_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_BFE594726BF700BD` FOREIGN KEY (`status_id`) REFERENCES `proposal_status` (`id`),
  CONSTRAINT `FK_BFE594726C4CA05D` FOREIGN KEY (`life_cycle_id`) REFERENCES `proposal_life_cycle` (`id`),
  CONSTRAINT `FK_BFE594729B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_BFE59472C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `proposal_type` (`id`),
  CONSTRAINT `FK_BFE59472E3690CE9` FOREIGN KEY (`frozen_id`) REFERENCES `frozen_proposal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal`
--

LOCK TABLES `proposal` WRITE;
/*!40000 ALTER TABLE `proposal` DISABLE KEYS */;
INSERT INTO `proposal` VALUES (1,1,14,1,9,'2018-05-25',838,17,'MAY180001',4,1,1,'2001-01-01',770,'2018-05-25 05:26:48','2018-05-25 05:27:48',NULL,56,NULL,'Backflow_Retest_Notice_05-25-2018_id_MAY180001.pdf',NULL,1,NULL,NULL,NULL,NULL,NULL,1,2,NULL,NULL,NULL,1,10),(2,1,14,3,9,'2018-05-25',NULL,17,'MAY180002',NULL,NULL,NULL,'2018-05-25',NULL,'2018-05-25 05:29:20','2018-05-25 05:29:41',NULL,57,NULL,'Backflow_Custom_Proposal_05-25-2018_id_MAY180002.pdf',NULL,1,NULL,NULL,NULL,NULL,NULL,2,1,NULL,NULL,NULL,2,NULL),(3,2,10,1,9,'2018-05-25',520,17,'MAY180003',3,3,1,'2001-01-01',520,'2018-05-25 05:38:40','2018-05-25 05:38:54',NULL,58,NULL,'Fire_Retest_Notice_05-25-2018_id_MAY180003.pdf',NULL,1,NULL,NULL,NULL,NULL,NULL,3,2,NULL,NULL,NULL,3,40.9),(4,1,9,1,9,'2018-05-25',856,17,'MAY180004',5,1,1,'2001-01-01',771,'2018-05-25 05:42:31','2018-05-25 05:43:09',NULL,59,NULL,'Backflow_Retest_Notice_05-25-2018_id_MAY180004.pdf',NULL,1,NULL,NULL,NULL,NULL,NULL,5,2,NULL,NULL,NULL,4,12.5),(5,1,9,2,9,'2018-05-25',0,17,'MAY180005',3,1,1,'2018-05-25',0,'2018-05-25 05:42:51','2018-05-25 05:43:01',NULL,61,NULL,'Backflow_Proposal_05-25-2018_id_MAY180005.pdf',NULL,1,NULL,NULL,NULL,NULL,NULL,4,1,0,120,120,4,NULL),(6,2,10,2,9,'2018-05-25',34,17,'MAY180006',2,1,1,'2018-05-25',0,'2018-05-25 05:44:12','2018-05-25 05:44:21',NULL,63,NULL,'Fire_Proposal_05-25-2018_id_MAY180006.pdf',NULL,1,NULL,NULL,NULL,NULL,NULL,6,1,34,120,154,5,NULL),(7,1,14,1,9,'2018-05-25',534,17,'MAY180007',2,1,1,'2001-01-01',500,'2018-05-25 05:47:00','2018-05-25 05:47:42',NULL,64,NULL,'Backflow_Retest_Notice_05-25-2018_id_MAY180007.pdf',NULL,1,NULL,NULL,NULL,NULL,NULL,7,2,NULL,NULL,NULL,6,5),(8,2,14,1,9,'2018-05-25',988,17,'MAY180008',2,1,1,'2001-01-01',988,'2018-05-25 05:47:00','2018-05-25 05:50:45',NULL,65,NULL,'Fire_Retest_Notice_05-25-2018_id_MAY180008.pdf',NULL,1,NULL,NULL,NULL,NULL,NULL,8,2,NULL,NULL,NULL,7,NULL),(9,1,14,2,9,'2018-05-25',17,17,'MAY180009',1,1,1,'2018-05-25',0,'2018-05-25 08:52:01','2018-05-25 08:52:27',NULL,67,NULL,'Backflow_Proposal_05-25-2018_id_MAY180009.pdf',NULL,1,NULL,NULL,'1','10','2321123',9,1,17,100,108,8,NULL),(10,2,14,2,9,'2018-05-25',34,17,'MAY180010',5,1,1,'2018-05-25',0,'2018-05-25 08:53:51','2018-05-25 08:54:17',NULL,69,NULL,'Fire_Proposal_05-25-2018_id_MAY180010.pdf',NULL,1,NULL,NULL,'11','1','1111',10,1,34,111,155,9,NULL);
/*!40000 ALTER TABLE `proposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_history`
--

DROP TABLE IF EXISTS `proposal_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `sending_address_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `reference_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_count` int(11) DEFAULT NULL,
  `devices_count` int(11) DEFAULT NULL,
  `sites_count` int(11) DEFAULT NULL,
  `date_of_proposal` date DEFAULT NULL,
  `earliest_due_date` date DEFAULT NULL,
  `grand_total` double DEFAULT NULL,
  `service_total_fee` double DEFAULT NULL,
  `sending_address_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `clone_id` int(11) DEFAULT NULL,
  `cloned_id` int(11) DEFAULT NULL,
  `after_hours_work_cost` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_comment` longtext COLLATE utf8_unicode_ci,
  `total_estimation_time` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_721E400041859289` (`division_id`),
  KEY `IDX_721E40009B6B5FBA` (`account_id`),
  KEY `IDX_721E4000C54C8C93` (`type_id`),
  KEY `IDX_721E400061220EA6` (`creator_id`),
  KEY `IDX_721E40006BF700BD` (`status_id`),
  KEY `IDX_721E40004F8A983C` (`contact_person_id`),
  KEY `IDX_721E40006510ABA8` (`sending_address_id`),
  KEY `IDX_721E4000835E0EEE` (`owner_entity_id`),
  KEY `IDX_721E4000BDAFD8C8` (`author`),
  KEY `IDX_721E40003D083110` (`clone_id`),
  KEY `IDX_721E40003735CA56` (`cloned_id`),
  CONSTRAINT `FK_721E40003735CA56` FOREIGN KEY (`cloned_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_721E40003D083110` FOREIGN KEY (`clone_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_721E400041859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_721E40004F8A983C` FOREIGN KEY (`contact_person_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_721E400061220EA6` FOREIGN KEY (`creator_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_721E40006510ABA8` FOREIGN KEY (`sending_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_721E40006BF700BD` FOREIGN KEY (`status_id`) REFERENCES `proposal_status` (`id`),
  CONSTRAINT `FK_721E4000835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_721E40009B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_721E4000BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_721E4000C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `proposal_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_history`
--

LOCK TABLES `proposal_history` WRITE;
/*!40000 ALTER TABLE `proposal_history` DISABLE KEYS */;
INSERT INTO `proposal_history` VALUES (1,1,14,1,9,1,1,56,1,22,'MAY180001',4,1,1,'2018-05-25','2001-01-01',838,770,NULL,NULL,'2018-05-25 05:26:48','2018-05-25 05:26:48',NULL,NULL,NULL,NULL,NULL,NULL),(2,1,14,1,9,17,NULL,56,1,22,'MAY180001',4,1,1,'2018-05-25','2001-01-01',838,770,NULL,'Backflow_Retest_Notice_05-25-2018_id_MAY180001.pdf','2018-05-25 05:27:48','2018-05-25 05:27:48',NULL,NULL,NULL,NULL,NULL,10),(3,1,14,3,9,2,NULL,57,2,22,'MAY180002',NULL,NULL,NULL,'2018-05-25','2018-05-25',NULL,NULL,NULL,'Backflow_Custom_Proposal_05-25-2018_id_MAY180002.pdf','2018-05-25 05:29:20','2018-05-25 05:29:20',NULL,NULL,NULL,NULL,NULL,NULL),(4,1,14,3,9,17,NULL,57,2,22,'MAY180002',NULL,NULL,NULL,'2018-05-25','2018-05-25',NULL,NULL,NULL,'Backflow_Custom_Proposal_05-25-2018_id_MAY180002.pdf','2018-05-25 05:29:41','2018-05-25 05:29:41',NULL,NULL,NULL,NULL,NULL,NULL),(5,1,14,3,9,17,NULL,57,2,22,'MAY180002',NULL,NULL,NULL,'2018-05-25','2018-05-25',NULL,NULL,NULL,'Backflow_Custom_Proposal_05-25-2018_id_MAY180002.pdf','2018-05-25 05:29:41','2018-05-25 05:29:41',NULL,NULL,NULL,NULL,NULL,NULL),(6,2,10,1,9,1,2,58,3,22,'MAY180003',3,3,1,'2018-05-25','2001-01-01',520,520,NULL,NULL,'2018-05-25 05:38:40','2018-05-25 05:38:40',NULL,NULL,NULL,NULL,NULL,NULL),(7,2,10,1,9,17,NULL,58,3,22,'MAY180003',3,3,1,'2018-05-25','2001-01-01',520,520,NULL,'Fire_Retest_Notice_05-25-2018_id_MAY180003.pdf','2018-05-25 05:38:54','2018-05-25 05:38:54',NULL,NULL,NULL,NULL,NULL,40.9),(8,1,9,1,9,1,3,59,4,22,'MAY180004',5,1,1,'2018-05-25','2001-01-01',856,771,NULL,NULL,'2018-05-25 05:42:31','2018-05-25 05:42:31',NULL,NULL,NULL,NULL,NULL,NULL),(9,1,9,2,9,1,3,61,5,22,'MAY180005',NULL,NULL,NULL,'2018-05-25',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:42:51','2018-05-25 05:42:51',NULL,NULL,NULL,NULL,NULL,NULL),(10,1,9,2,9,17,NULL,61,5,22,'MAY180005',3,1,1,'2018-05-25','2018-05-25',0,0,NULL,'Backflow_Proposal_05-25-2018_id_MAY180005.pdf','2018-05-25 05:43:01','2018-05-25 05:43:01',NULL,NULL,NULL,NULL,NULL,NULL),(11,1,9,1,9,17,NULL,59,4,22,'MAY180004',5,1,1,'2018-05-25','2001-01-01',856,771,NULL,'Backflow_Retest_Notice_05-25-2018_id_MAY180004.pdf','2018-05-25 05:43:09','2018-05-25 05:43:09',NULL,NULL,NULL,NULL,NULL,12.5),(12,2,10,2,9,1,2,63,6,22,'MAY180006',NULL,NULL,NULL,'2018-05-25',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:44:12','2018-05-25 05:44:12',NULL,NULL,NULL,NULL,NULL,NULL),(13,2,10,2,9,17,NULL,63,6,22,'MAY180006',2,1,1,'2018-05-25','2018-05-25',34,0,NULL,'Fire_Proposal_05-25-2018_id_MAY180006.pdf','2018-05-25 05:44:21','2018-05-25 05:44:21',NULL,NULL,NULL,NULL,NULL,NULL),(14,1,14,1,9,1,1,64,7,22,'MAY180007',2,1,1,'2018-05-25','2001-01-01',534,500,NULL,NULL,'2018-05-25 05:47:00','2018-05-25 05:47:00',NULL,NULL,NULL,NULL,NULL,NULL),(15,2,14,1,9,1,1,65,8,22,'MAY180008',1,1,1,'2018-05-25','2001-01-01',888,888,NULL,NULL,'2018-05-25 05:47:00','2018-05-25 05:47:00',NULL,NULL,NULL,NULL,NULL,NULL),(16,1,14,1,9,17,NULL,64,7,22,'MAY180007',2,1,1,'2018-05-25','2001-01-01',534,500,NULL,'Backflow_Retest_Notice_05-25-2018_id_MAY180007.pdf','2018-05-25 05:47:42','2018-05-25 05:47:42',NULL,NULL,NULL,NULL,NULL,5),(17,2,14,1,9,17,NULL,65,8,22,'MAY180008',2,1,1,'2018-05-25','2001-01-01',988,988,NULL,'Fire_Retest_Notice_05-25-2018_id_MAY180008.pdf','2018-05-25 05:50:45','2018-05-25 05:50:45',NULL,NULL,NULL,NULL,NULL,NULL),(18,1,14,2,9,1,1,67,9,22,'MAY180009',NULL,NULL,NULL,'2018-05-25',NULL,NULL,NULL,NULL,NULL,'2018-05-25 08:52:01','2018-05-25 08:52:01',NULL,NULL,NULL,NULL,NULL,NULL),(19,1,14,2,9,1,1,67,9,22,'MAY180009',1,1,1,'2018-05-25','2018-05-25',17,0,NULL,NULL,'2018-05-25 08:52:19','2018-05-25 08:52:19',NULL,NULL,NULL,NULL,NULL,NULL),(20,1,14,2,9,17,NULL,67,9,22,'MAY180009',1,1,1,'2018-05-25','2018-05-25',17,0,NULL,'Backflow_Proposal_05-25-2018_id_MAY180009.pdf','2018-05-25 08:52:27','2018-05-25 08:52:27',NULL,NULL,NULL,NULL,NULL,NULL),(21,2,14,2,9,1,1,69,10,22,'MAY180010',NULL,NULL,NULL,'2018-05-25',NULL,NULL,NULL,NULL,NULL,'2018-05-25 08:53:51','2018-05-25 08:53:51',NULL,NULL,NULL,NULL,NULL,NULL),(22,2,14,2,9,1,1,69,10,22,'MAY180010',5,1,1,'2018-05-25','2018-05-25',34,0,NULL,NULL,'2018-05-25 08:54:11','2018-05-25 08:54:11',NULL,NULL,NULL,NULL,NULL,NULL),(23,2,14,2,9,17,NULL,69,10,22,'MAY180010',5,1,1,'2018-05-25','2018-05-25',34,0,NULL,'Fire_Proposal_05-25-2018_id_MAY180010.pdf','2018-05-25 08:54:17','2018-05-25 08:54:17',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `proposal_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_life_cycle`
--

DROP TABLE IF EXISTS `proposal_life_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_life_cycle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_life_cycle`
--

LOCK TABLES `proposal_life_cycle` WRITE;
/*!40000 ALTER TABLE `proposal_life_cycle` DISABLE KEYS */;
INSERT INTO `proposal_life_cycle` VALUES (1,'Proposal Life Cycle','proposal_life_cycle'),(2,'Retest Notice Life Cycle','retest_notice_life_cycle');
/*!40000 ALTER TABLE `proposal_life_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_log`
--

DROP TABLE IF EXISTS `proposal_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `letter_id` int(11) DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_81AFA0B0F4792058` (`proposal_id`),
  KEY `IDX_81AFA0B0F675F31B` (`author_id`),
  KEY `IDX_81AFA0B04525FF26` (`letter_id`),
  CONSTRAINT `FK_81AFA0B04525FF26` FOREIGN KEY (`letter_id`) REFERENCES `letter` (`id`),
  CONSTRAINT `FK_81AFA0B0F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_81AFA0B0F675F31B` FOREIGN KEY (`author_id`) REFERENCES `contractor_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_log`
--

LOCK TABLES `proposal_log` WRITE;
/*!40000 ALTER TABLE `proposal_log` DISABLE KEYS */;
INSERT INTO `proposal_log` VALUES (1,1,9,'2018-05-25 05:26:48',NULL,NULL,'Retest Notice was created'),(2,1,9,'2018-05-25 05:27:48','Authorized by Tony Stark. ',NULL,'Retest Notice was added to Workorder'),(3,2,9,'2018-05-25 05:29:20',NULL,NULL,'Custom Proposal was created'),(4,2,9,'2018-05-25 05:29:41','Authorized by Tony Stark. ',NULL,'Proposal was added to Workorder'),(5,3,9,'2018-05-25 05:38:40',NULL,NULL,'Retest Notice was created'),(6,3,9,'2018-05-25 05:38:54','Authorized by Tony Stark. ',NULL,'Retest Notice was added to Workorder'),(7,4,9,'2018-05-25 05:42:31',NULL,NULL,'Retest Notice was created'),(8,5,9,'2018-05-25 05:42:51',NULL,NULL,'Proposal was created'),(9,5,9,'2018-05-25 05:43:01','Authorized by Petyr Bealish. ',NULL,'Proposal was added to Workorder'),(10,4,9,'2018-05-25 05:43:09','Authorized by Petyr Bealish. ',NULL,'Retest Notice was added to Workorder'),(11,6,9,'2018-05-25 05:44:12',NULL,NULL,'Proposal was created'),(12,6,9,'2018-05-25 05:44:21','Authorized by Tony Stark. ',NULL,'Proposal was added to Workorder'),(13,7,9,'2018-05-25 05:47:00',NULL,NULL,'Retest Notice was created'),(14,8,9,'2018-05-25 05:47:00',NULL,NULL,'Retest Notice was created'),(15,7,9,'2018-05-25 05:47:42','Authorized by Tony Stark. ',NULL,'Retest Notice was added to Workorder'),(16,8,9,'2018-05-25 05:50:45','Authorized by Tony Stark. ',NULL,'Retest Notice was added to Workorder'),(17,9,9,'2018-05-25 08:52:01',NULL,NULL,'Proposal was created'),(18,9,9,'2018-05-25 08:52:27','Authorized by Tony Stark. ',NULL,'Proposal was added to Workorder'),(19,10,9,'2018-05-25 08:53:51',NULL,NULL,'Proposal was created'),(20,10,9,'2018-05-25 08:54:17','Authorized by Tony Stark. ',NULL,'Proposal was added to Workorder');
/*!40000 ALTER TABLE `proposal_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_log_action`
--

DROP TABLE IF EXISTS `proposal_log_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_log_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `named_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2004EC46457EB27E` (`named_id`),
  CONSTRAINT `FK_2004EC46457EB27E` FOREIGN KEY (`named_id`) REFERENCES `proposal_log_action_named` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_log_action`
--

LOCK TABLES `proposal_log_action` WRITE;
/*!40000 ALTER TABLE `proposal_log_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `proposal_log_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_log_action_named`
--

DROP TABLE IF EXISTS `proposal_log_action_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_log_action_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_for_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_log_action_named`
--

LOCK TABLES `proposal_log_action_named` WRITE;
/*!40000 ALTER TABLE `proposal_log_action_named` DISABLE KEYS */;
INSERT INTO `proposal_log_action_named` VALUES (1,'Send via email','send_via_email','admin_letter_proposal',1),(2,'Notice was sent via Mail','notice_was_sent_via_mail','admin_letter_mail_proposal',1),(3,'Client was called','client_was_called','admin_proposal_client_was_called',1),(4,'Reset Services to Next Time','reset_services_to_next_time','admin_proposal_reset_services_next_time',1),(5,'Send Email Reminder','send_email_reminder','admin_letter_email_reminder',1),(6,'Link','link',NULL,1);
/*!40000 ALTER TABLE `proposal_log_action_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_logs_actions`
--

DROP TABLE IF EXISTS `proposal_logs_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_logs_actions` (
  `proposal_log_id` int(11) NOT NULL,
  `proposal_action_id` int(11) NOT NULL,
  PRIMARY KEY (`proposal_log_id`,`proposal_action_id`),
  KEY `IDX_BD2367AE4B713931` (`proposal_log_id`),
  KEY `IDX_BD2367AEA86603F3` (`proposal_action_id`),
  CONSTRAINT `FK_BD2367AE4B713931` FOREIGN KEY (`proposal_log_id`) REFERENCES `proposal_log` (`id`),
  CONSTRAINT `FK_BD2367AEA86603F3` FOREIGN KEY (`proposal_action_id`) REFERENCES `proposal_log_action` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_logs_actions`
--

LOCK TABLES `proposal_logs_actions` WRITE;
/*!40000 ALTER TABLE `proposal_logs_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `proposal_logs_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_status`
--

DROP TABLE IF EXISTS `proposal_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_status`
--

LOCK TABLES `proposal_status` WRITE;
/*!40000 ALTER TABLE `proposal_status` DISABLE KEYS */;
INSERT INTO `proposal_status` VALUES (1,'Draft','draft',0,'app-status-label--yellow'),(2,'To Be Sent','to_be_sent',1,'app-status-label--yellow'),(3,'Replacing','replacing',1,'app-status-label--yellow'),(4,'Sent via Mail','sent_via_mail',2,'app-status-label--green'),(5,'Sent via Email','sent_via_email',2,'app-status-label--green'),(6,'Sent via Mail and Email','sent_via_mail_and_email',3,'app-status-label--green'),(7,'Call Needed','call_needed',4,'app-status-label--red'),(8,'Client Was Called','client_was_called',5,'app-status-label--green'),(9,'Send Email Reminder','send_email_reminder',6,'app-status-label--red'),(10,'Email Reminder Sent','email_reminder_sent',7,'app-status-label--green'),(11,'Past Due','past_due',8,'app-status-label--red'),(12,'Sent Past Due Email','send_past_due_email',9,'app-status-label--red'),(13,'Rejected By Client','rejected_by_client',11,'app-status-label--red'),(14,'No Response','no_response',10,'app-status-label--red'),(15,'Deleted','deleted',12,'app-status-label--gray'),(16,'Services Were Reset','reset_services_to_next_time',12,'app-status-label--gray'),(17,'Workorder created','workorder_created',12,'app-status-label--gray'),(18,'Services Discarded','services_discarded',12,'app-status-label--gray'),(19,'Replaced','replaced',12,'app-status-label--gray');
/*!40000 ALTER TABLE `proposal_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_type`
--

DROP TABLE IF EXISTS `proposal_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_type`
--

LOCK TABLES `proposal_type` WRITE;
/*!40000 ALTER TABLE `proposal_type` DISABLE KEYS */;
INSERT INTO `proposal_type` VALUES (1,'Retest','retest'),(2,'Repair','repair'),(3,'Custom','custom');
/*!40000 ALTER TABLE `proposal_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isSelectByDefault` tinyint(1) NOT NULL DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `question_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6F7494ECB90598E` (`question_type_id`),
  CONSTRAINT `FK_B6F7494ECB90598E` FOREIGN KEY (`question_type_id`) REFERENCES `question_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_type`
--

DROP TABLE IF EXISTS `question_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_type`
--

LOCK TABLES `question_type` WRITE;
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
INSERT INTO `question_type` VALUES (1,'Green','green','2018-05-25 05:07:41','2018-05-25 05:07:41'),(2,'Blue','blue','2018-05-25 05:07:41','2018-05-25 05:07:41'),(3,'Not Testable','not_testable','2018-05-25 05:07:41','2018-05-25 05:07:41');
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `range_question`
--

DROP TABLE IF EXISTS `range_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `range_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_value` int(11) DEFAULT '0',
  `max_value` int(11) DEFAULT '0',
  `lower_bound` int(11) DEFAULT '0',
  `upper_bound` int(11) DEFAULT '0',
  `sort` int(11) DEFAULT '0',
  `date_cretea` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `positive_status_id` int(11) DEFAULT NULL,
  `lower_negative_status_id` int(11) DEFAULT NULL,
  `upper_negative_status_id` int(11) DEFAULT NULL,
  `fail_anyway_status_id` int(11) DEFAULT NULL,
  `service_patern_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F8A3E6ED8C4651E9` (`positive_status_id`),
  KEY `IDX_F8A3E6EDDAA3558D` (`lower_negative_status_id`),
  KEY `IDX_F8A3E6EDE44B10AD` (`upper_negative_status_id`),
  KEY `IDX_F8A3E6ED14B8E846` (`fail_anyway_status_id`),
  KEY `IDX_F8A3E6EDD38F0878` (`service_patern_id`),
  CONSTRAINT `FK_F8A3E6ED14B8E846` FOREIGN KEY (`fail_anyway_status_id`) REFERENCES `range_question_status` (`id`),
  CONSTRAINT `FK_F8A3E6ED8C4651E9` FOREIGN KEY (`positive_status_id`) REFERENCES `range_question_status` (`id`),
  CONSTRAINT `FK_F8A3E6EDD38F0878` FOREIGN KEY (`service_patern_id`) REFERENCES `service_patern` (`id`),
  CONSTRAINT `FK_F8A3E6EDDAA3558D` FOREIGN KEY (`lower_negative_status_id`) REFERENCES `range_question_status` (`id`),
  CONSTRAINT `FK_F8A3E6EDE44B10AD` FOREIGN KEY (`upper_negative_status_id`) REFERENCES `range_question_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `range_question`
--

LOCK TABLES `range_question` WRITE;
/*!40000 ALTER TABLE `range_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `range_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `range_question_status`
--

DROP TABLE IF EXISTS `range_question_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `range_question_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_fail` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `range_question_status`
--

LOCK TABLES `range_question_status` WRITE;
/*!40000 ALTER TABLE `range_question_status` DISABLE KEYS */;
INSERT INTO `range_question_status` VALUES (1,'Closed Tight/Held','closed_tight_held',0,'2018-05-25 05:07:41','2018-05-25 05:07:41'),(2,'Leaked','leaked',1,'2018-05-25 05:07:41','2018-05-25 05:07:41'),(3,'Open','open',0,'2018-05-25 05:07:41','2018-05-25 05:07:41'),(4,'Did Not Open','did_not_open',1,'2018-05-25 05:07:41','2018-05-25 05:07:41');
/*!40000 ALTER TABLE `range_question_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `range_result`
--

DROP TABLE IF EXISTS `range_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `range_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `result_value` int(11) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `range_question_id` int(11) DEFAULT NULL,
  `range_question_status_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3AEC1F6380802003` (`range_question_id`),
  KEY `IDX_3AEC1F63DE084BD1` (`range_question_status_id`),
  KEY `IDX_3AEC1F634BD2A4C0` (`report_id`),
  CONSTRAINT `FK_3AEC1F634BD2A4C0` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FK_3AEC1F6380802003` FOREIGN KEY (`range_question_id`) REFERENCES `range_question` (`id`),
  CONSTRAINT `FK_3AEC1F63DE084BD1` FOREIGN KEY (`range_question_status_id`) REFERENCES `range_question_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `range_result`
--

LOCK TABLES `range_result` WRITE;
/*!40000 ALTER TABLE `range_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `range_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repair_device_info`
--

DROP TABLE IF EXISTS `repair_device_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repair_device_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `special_notification_id` int(11) DEFAULT NULL,
  `proposal_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estimation_time` double NOT NULL,
  `price` double NOT NULL,
  `subtotal_munic_and_proc_fee` double NOT NULL,
  `device_total` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_97B48F4CA2485031` (`special_notification_id`),
  KEY `IDX_97B48F4CF4792058` (`proposal_id`),
  KEY `IDX_97B48F4C94A4C7D4` (`device_id`),
  CONSTRAINT `FK_97B48F4C94A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_97B48F4CA2485031` FOREIGN KEY (`special_notification_id`) REFERENCES `special_notification` (`id`),
  CONSTRAINT `FK_97B48F4CF4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repair_device_info`
--

LOCK TABLES `repair_device_info` WRITE;
/*!40000 ALTER TABLE `repair_device_info` DISABLE KEYS */;
INSERT INTO `repair_device_info` VALUES (1,NULL,1,1,NULL,10,770,68,838),(2,NULL,3,3,NULL,12.5,300,17,317),(3,NULL,3,4,NULL,20,100,17,117),(4,NULL,3,5,NULL,8.4,120,17,137),(5,NULL,4,6,NULL,12.5,771,85,856),(6,NULL,5,6,'огпогамог шанегнакнев',11,120,0,120),(7,NULL,6,5,'8888',9,120,34,154),(8,NULL,7,2,NULL,5,500,34,534),(9,NULL,8,7,NULL,11.5,988,34,1022),(10,NULL,9,2,'орогр',10,100,17,117),(11,NULL,10,8,'ро',10,111,34,145);
/*!40000 ALTER TABLE `repair_device_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `tester_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_report` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `is_need_repair` tinyint(1) DEFAULT '0',
  `finish_time` datetime DEFAULT NULL,
  `print_template` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_C42F77846BF700BD` (`status_id`),
  KEY `IDX_C42F7784ED5CA9E6` (`service_id`),
  KEY `IDX_C42F7784B0265DC7` (`contractor_id`),
  KEY `IDX_C42F7784979A21C1` (`tester_id`),
  KEY `IDX_C42F77842C1C3467` (`workorder_id`),
  CONSTRAINT `FK_C42F77842C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_C42F77846BF700BD` FOREIGN KEY (`status_id`) REFERENCES `report_status` (`id`),
  CONSTRAINT `FK_C42F7784979A21C1` FOREIGN KEY (`tester_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_C42F7784B0265DC7` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_C42F7784ED5CA9E6` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` VALUES (1,1,1,3,15,1,NULL,'/uploads/reports/1527253372_49372.pdf','2018-05-25 08:02:50','2018-05-25 08:02:52',NULL,'2018-05-25 08:02:15',NULL),(2,1,2,3,15,1,NULL,'/uploads/reports/1527254142_34476.pdf','2018-05-25 08:15:40','2018-05-25 08:15:42',NULL,'2018-05-25 08:15:08',NULL);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_status`
--

DROP TABLE IF EXISTS `report_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_status`
--

LOCK TABLES `report_status` WRITE;
/*!40000 ALTER TABLE `report_status` DISABLE KEYS */;
INSERT INTO `report_status` VALUES (1,'Pass','pass'),(2,'Fail','fail'),(3,'Not Testable','not_testable'),(4,'Completed','completed');
/*!40000 ALTER TABLE `report_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fixed_price` double DEFAULT NULL,
  `hourly_price` double DEFAULT NULL,
  `last_tested` datetime DEFAULT NULL,
  `inspection_due` datetime NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `named` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `company_last_tested` int(11) DEFAULT NULL,
  `comment` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opportunity` int(11) DEFAULT NULL,
  `test_date` date DEFAULT NULL,
  `department_channel` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `proposal_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `last_report_id` int(11) DEFAULT NULL,
  `contractor_service_id` int(11) DEFAULT NULL,
  `last_tester_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E19D9AD271E0CC87` (`named`),
  KEY `IDX_E19D9AD294A4C7D4` (`device_id`),
  KEY `IDX_E19D9AD29B6B5FBA` (`account_id`),
  KEY `IDX_E19D9AD29B52D956` (`company_last_tested`),
  KEY `IDX_E19D9AD28389C3D7` (`opportunity`),
  KEY `IDX_E19D9AD21F5066A4` (`department_channel`),
  KEY `IDX_E19D9AD26BF700BD` (`status_id`),
  KEY `IDX_E19D9AD2F4792058` (`proposal_id`),
  KEY `IDX_E19D9AD22C1C3467` (`workorder_id`),
  KEY `IDX_E19D9AD25C1D7BC6` (`last_report_id`),
  KEY `IDX_E19D9AD2B4DAC98D` (`contractor_service_id`),
  KEY `IDX_E19D9AD28055FEC7` (`last_tester_id`),
  CONSTRAINT `FK_E19D9AD21F5066A4` FOREIGN KEY (`department_channel`) REFERENCES `department_channel` (`id`),
  CONSTRAINT `FK_E19D9AD22C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_E19D9AD25C1D7BC6` FOREIGN KEY (`last_report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FK_E19D9AD26BF700BD` FOREIGN KEY (`status_id`) REFERENCES `service_status` (`id`),
  CONSTRAINT `FK_E19D9AD271E0CC87` FOREIGN KEY (`named`) REFERENCES `service_named` (`id`),
  CONSTRAINT `FK_E19D9AD28055FEC7` FOREIGN KEY (`last_tester_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_E19D9AD28389C3D7` FOREIGN KEY (`opportunity`) REFERENCES `opportunity` (`id`),
  CONSTRAINT `FK_E19D9AD294A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_E19D9AD29B52D956` FOREIGN KEY (`company_last_tested`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_E19D9AD29B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_E19D9AD2B4DAC98D` FOREIGN KEY (`contractor_service_id`) REFERENCES `contractor_service` (`id`),
  CONSTRAINT `FK_E19D9AD2F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,200,NULL,'2018-05-25 08:02:50','2002-01-01 00:00:00',0,73,1,14,3,NULL,NULL,'2002-01-01',4,3,1,1,1,1,NULL,'2018-05-25 05:26:02','2018-05-25 08:02:50'),(2,120,NULL,'2018-05-25 08:15:40','2002-01-01 00:00:00',0,73,1,14,3,NULL,NULL,'2002-01-01',4,3,1,1,2,1,NULL,'2018-05-25 05:26:09','2018-05-25 08:15:40'),(3,300,NULL,NULL,'2001-01-01 00:00:00',0,73,1,14,NULL,NULL,NULL,'2001-01-01',4,3,1,1,NULL,1,NULL,'2018-05-25 05:26:16','2018-05-25 05:27:48'),(4,150,NULL,NULL,'2001-01-01 00:00:00',0,73,1,14,NULL,NULL,NULL,'2001-01-01',4,3,1,1,NULL,1,NULL,'2018-05-25 05:26:24','2018-05-25 05:27:48'),(5,200,NULL,NULL,'2001-01-01 00:00:00',0,73,2,14,NULL,NULL,NULL,'2001-01-01',4,3,7,6,NULL,1,NULL,'2018-05-25 05:34:41','2018-05-25 05:47:42'),(6,300,NULL,NULL,'2001-01-01 00:00:00',0,73,2,14,NULL,NULL,NULL,'2001-01-01',4,3,7,6,NULL,1,NULL,'2018-05-25 05:34:53','2018-05-25 05:47:42'),(7,300,NULL,NULL,'2001-01-01 00:00:00',0,74,3,9,NULL,NULL,NULL,'2001-01-01',NULL,3,3,3,NULL,3,NULL,'2018-05-25 05:37:38','2018-05-25 05:38:54'),(8,100,NULL,NULL,'2001-01-01 00:00:00',0,75,4,9,NULL,NULL,NULL,'2001-01-01',NULL,3,3,3,NULL,2,NULL,'2018-05-25 05:37:49','2018-05-25 05:38:54'),(9,120,NULL,NULL,'2001-01-01 00:00:00',0,77,5,9,NULL,NULL,NULL,'2001-01-01',NULL,3,3,3,NULL,5,NULL,'2018-05-25 05:38:04','2018-05-25 05:38:54'),(10,150,NULL,NULL,'2001-01-01 00:00:00',0,73,6,9,NULL,NULL,NULL,'2001-01-01',4,3,4,4,NULL,1,NULL,'2018-05-25 05:41:21','2018-05-25 05:43:09'),(11,200,NULL,NULL,'2001-01-01 00:00:00',0,73,6,9,NULL,NULL,NULL,'2001-01-01',4,3,4,4,NULL,1,NULL,'2018-05-25 05:41:28','2018-05-25 05:43:09'),(12,200,NULL,NULL,'2001-01-01 00:00:00',0,73,6,9,NULL,NULL,NULL,'2001-01-01',4,3,4,4,NULL,1,NULL,'2018-05-25 05:41:35','2018-05-25 05:43:09'),(13,120,NULL,NULL,'2001-01-01 00:00:00',0,73,6,9,NULL,NULL,NULL,'2001-01-01',4,3,4,4,NULL,1,NULL,'2018-05-25 05:41:41','2018-05-25 05:43:09'),(14,101,NULL,NULL,'2001-01-01 00:00:00',0,73,6,9,NULL,NULL,NULL,'2001-01-01',4,3,4,4,NULL,1,NULL,'2018-05-25 05:41:50','2018-05-25 05:43:09'),(15,NULL,NULL,NULL,'2018-05-25 05:42:51',0,1,6,9,NULL,NULL,NULL,'2018-05-25',4,3,5,4,NULL,NULL,NULL,'2018-05-25 05:42:51','2018-05-25 05:43:01'),(16,NULL,NULL,NULL,'2018-05-25 05:42:51',0,2,6,9,NULL,NULL,NULL,'2018-05-25',4,3,5,4,NULL,NULL,NULL,'2018-05-25 05:42:51','2018-05-25 05:43:01'),(17,NULL,NULL,NULL,'2018-05-25 05:42:51',0,3,6,9,NULL,NULL,NULL,'2018-05-25',4,3,5,4,NULL,NULL,NULL,'2018-05-25 05:42:51','2018-05-25 05:43:01'),(18,NULL,NULL,NULL,'2018-05-25 05:44:12',0,111,5,9,NULL,NULL,NULL,'2018-05-25',8,3,6,5,NULL,NULL,NULL,'2018-05-25 05:44:12','2018-05-25 05:44:21'),(19,NULL,NULL,NULL,'2018-05-25 05:44:12',0,112,5,9,NULL,NULL,NULL,'2018-05-25',8,3,6,5,NULL,NULL,NULL,'2018-05-25 05:44:12','2018-05-25 05:44:21'),(20,888,NULL,NULL,'2001-01-01 00:00:00',0,80,7,14,NULL,NULL,NULL,'2001-01-01',8,3,8,7,NULL,NULL,NULL,'2018-05-25 05:46:15','2018-05-25 05:50:45'),(21,100,NULL,NULL,'2001-01-01 00:00:00',0,79,7,14,NULL,NULL,NULL,'2001-01-01',8,3,8,7,NULL,7,NULL,'2018-05-25 05:49:23','2018-05-25 05:50:45'),(22,NULL,NULL,NULL,'2018-05-25 08:52:01',0,72,2,14,NULL,NULL,NULL,'2018-05-25',4,3,9,8,NULL,NULL,NULL,'2018-05-25 08:52:01','2018-05-25 08:52:27'),(23,NULL,NULL,NULL,'2018-05-25 08:53:51',0,84,8,14,NULL,NULL,NULL,'2018-05-25',8,3,10,9,NULL,NULL,NULL,'2018-05-25 08:53:51','2018-05-25 08:54:17'),(24,NULL,NULL,NULL,'2018-05-25 08:53:51',0,85,8,14,NULL,NULL,NULL,'2018-05-25',8,3,10,9,NULL,NULL,NULL,'2018-05-25 08:53:51','2018-05-25 08:54:17'),(25,NULL,NULL,NULL,'2018-05-25 08:53:51',0,86,8,14,NULL,NULL,NULL,'2018-05-25',8,3,10,9,NULL,NULL,NULL,'2018-05-25 08:53:51','2018-05-25 08:54:17'),(26,NULL,NULL,NULL,'2018-05-25 08:53:51',0,87,8,14,NULL,NULL,NULL,'2018-05-25',8,3,10,9,NULL,NULL,NULL,'2018-05-25 08:53:51','2018-05-25 08:54:17'),(27,NULL,NULL,NULL,'2018-05-25 08:53:51',0,88,8,14,NULL,NULL,NULL,'2018-05-25',8,3,10,9,NULL,NULL,NULL,'2018-05-25 08:53:51','2018-05-25 08:54:17');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_frequency`
--

DROP TABLE IF EXISTS `service_frequency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_frequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mounth_frequency` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_frequency`
--

LOCK TABLES `service_frequency` WRITE;
/*!40000 ALTER TABLE `service_frequency` DISABLE KEYS */;
INSERT INTO `service_frequency` VALUES (1,'1 month','1_month','1'),(2,'3 months','3_months','3'),(3,'6 months','6_months','6'),(4,'1 year','1_year','12'),(5,'2 years','2_years','24'),(6,'5 years','5_years','60'),(7,'6 years','6_years','72'),(8,'10 years','10_years','120'),(9,'non-repeatable','non_repeatable','0');
/*!40000 ALTER TABLE `service_frequency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_history`
--

DROP TABLE IF EXISTS `service_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `company_last_tested` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `fixed_price` double DEFAULT NULL,
  `hourly_price` double DEFAULT NULL,
  `last_tested` datetime DEFAULT NULL,
  `inspection_due` datetime NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `comment` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `proposal_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `test_date` date DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `last_report_id` int(11) DEFAULT NULL,
  `opportunity_id` int(11) DEFAULT NULL,
  `department_channel_id` int(11) DEFAULT NULL,
  `contractor_service_id` int(11) DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E83E22D771E0CC87` (`named`),
  KEY `IDX_E83E22D794A4C7D4` (`device_id`),
  KEY `IDX_E83E22D79B6B5FBA` (`account_id`),
  KEY `IDX_E83E22D79B52D956` (`company_last_tested`),
  KEY `IDX_E83E22D7835E0EEE` (`owner_entity_id`),
  KEY `IDX_E83E22D7BDAFD8C8` (`author`),
  KEY `IDX_E83E22D7F4792058` (`proposal_id`),
  KEY `IDX_E83E22D76BF700BD` (`status_id`),
  KEY `IDX_E83E22D72C1C3467` (`workorder_id`),
  KEY `IDX_E83E22D75C1D7BC6` (`last_report_id`),
  KEY `IDX_E83E22D79A34590F` (`opportunity_id`),
  KEY `IDX_E83E22D7E0FCAD46` (`department_channel_id`),
  KEY `IDX_E83E22D7B4DAC98D` (`contractor_service_id`),
  CONSTRAINT `FK_E83E22D72C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_E83E22D75C1D7BC6` FOREIGN KEY (`last_report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FK_E83E22D76BF700BD` FOREIGN KEY (`status_id`) REFERENCES `service_status` (`id`),
  CONSTRAINT `FK_E83E22D771E0CC87` FOREIGN KEY (`named`) REFERENCES `service_named` (`id`),
  CONSTRAINT `FK_E83E22D7835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `service` (`id`),
  CONSTRAINT `FK_E83E22D794A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_E83E22D79A34590F` FOREIGN KEY (`opportunity_id`) REFERENCES `opportunity` (`id`),
  CONSTRAINT `FK_E83E22D79B52D956` FOREIGN KEY (`company_last_tested`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_E83E22D79B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_E83E22D7B4DAC98D` FOREIGN KEY (`contractor_service_id`) REFERENCES `contractor_service` (`id`),
  CONSTRAINT `FK_E83E22D7BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_E83E22D7E0FCAD46` FOREIGN KEY (`department_channel_id`) REFERENCES `department_channel` (`id`),
  CONSTRAINT `FK_E83E22D7F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal_history` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_history`
--

LOCK TABLES `service_history` WRITE;
/*!40000 ALTER TABLE `service_history` DISABLE KEYS */;
INSERT INTO `service_history` VALUES (1,73,1,14,NULL,1,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:02',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:02'),(2,73,1,14,NULL,1,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:02',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:02'),(3,73,1,14,NULL,2,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:09',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:09'),(4,73,1,14,NULL,2,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:09',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:09'),(5,73,1,14,NULL,3,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:16',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:16'),(6,73,1,14,NULL,3,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:16',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:16'),(7,73,1,14,NULL,4,22,150,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:24',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:24'),(8,73,1,14,NULL,4,22,150,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:24',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:24'),(9,73,1,14,NULL,1,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:48',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:48'),(10,73,1,14,NULL,2,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:48',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:48'),(11,73,1,14,NULL,3,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:48',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:48'),(12,73,1,14,NULL,4,22,150,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:26:48',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:26:48'),(13,73,2,14,NULL,5,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:34:42',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:34:41'),(14,73,2,14,NULL,5,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:34:42',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:34:42'),(15,73,2,14,NULL,6,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:34:53',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:34:53'),(16,73,2,14,NULL,6,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:34:53',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:34:53'),(17,74,3,9,NULL,7,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:37:38',NULL,NULL,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:37:38'),(18,74,3,9,NULL,7,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:37:38',NULL,1,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:37:38'),(19,75,4,9,NULL,8,22,100,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:37:49',NULL,NULL,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:37:49'),(20,75,4,9,NULL,8,22,100,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:37:49',NULL,1,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:37:49'),(21,77,5,9,NULL,9,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:38:04',NULL,NULL,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:38:04'),(22,77,5,9,NULL,9,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:38:05',NULL,1,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:38:05'),(23,74,3,9,NULL,7,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:38:40',NULL,2,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:38:40'),(24,75,4,9,NULL,8,22,100,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:38:40',NULL,2,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:38:40'),(25,77,5,9,NULL,9,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:38:40',NULL,2,'2001-01-01',NULL,NULL,NULL,NULL,NULL,'2018-05-25 05:38:40'),(26,73,6,9,NULL,10,22,150,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:21',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:21'),(27,73,6,9,NULL,10,22,150,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:21',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:21'),(28,73,6,9,NULL,11,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:28',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:28'),(29,73,6,9,NULL,11,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:28',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:28'),(30,73,6,9,NULL,12,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:35',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:35'),(31,73,6,9,NULL,12,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:35',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:35'),(32,73,6,9,NULL,13,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:41',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:41'),(33,73,6,9,NULL,13,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:41',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:41'),(34,73,6,9,NULL,14,22,101,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:50',NULL,NULL,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:50'),(35,73,6,9,NULL,14,22,101,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:41:50',NULL,1,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:41:50'),(36,73,6,9,NULL,10,22,150,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:42:31',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:31'),(37,73,6,9,NULL,11,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:42:31',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:31'),(38,73,6,9,NULL,12,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:42:31',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:31'),(39,73,6,9,NULL,13,22,120,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:42:31',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:31'),(40,73,6,9,NULL,14,22,101,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:42:31',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:31'),(41,1,6,9,NULL,15,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',NULL,NULL,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(42,1,6,9,NULL,15,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',9,6,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(43,2,6,9,NULL,16,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',NULL,NULL,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(44,2,6,9,NULL,16,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',9,6,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(45,3,6,9,NULL,17,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',NULL,NULL,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(46,3,6,9,NULL,17,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',9,6,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(47,1,6,9,NULL,15,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',9,6,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(48,2,6,9,NULL,16,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',9,6,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(49,3,6,9,NULL,17,22,NULL,NULL,NULL,'2018-05-25 05:42:51',0,NULL,'2018-05-25 05:42:51',9,6,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 05:42:51'),(50,111,5,9,NULL,18,22,NULL,NULL,NULL,'2018-05-25 05:44:12',0,NULL,'2018-05-25 05:44:12',NULL,NULL,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 05:44:12'),(51,111,5,9,NULL,18,22,NULL,NULL,NULL,'2018-05-25 05:44:12',0,NULL,'2018-05-25 05:44:12',12,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 05:44:12'),(52,112,5,9,NULL,19,22,NULL,NULL,NULL,'2018-05-25 05:44:12',0,NULL,'2018-05-25 05:44:12',NULL,NULL,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 05:44:12'),(53,112,5,9,NULL,19,22,NULL,NULL,NULL,'2018-05-25 05:44:12',0,NULL,'2018-05-25 05:44:12',12,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 05:44:12'),(54,111,5,9,NULL,18,22,NULL,NULL,NULL,'2018-05-25 05:44:12',0,NULL,'2018-05-25 05:44:12',12,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 05:44:12'),(55,112,5,9,NULL,19,22,NULL,NULL,NULL,'2018-05-25 05:44:12',0,NULL,'2018-05-25 05:44:12',12,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 05:44:12'),(56,80,7,14,NULL,20,22,888,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:46:15',NULL,NULL,'2001-01-01',NULL,NULL,NULL,8,NULL,'2018-05-25 05:46:15'),(57,80,7,14,NULL,20,22,888,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:46:15',NULL,1,'2001-01-01',NULL,NULL,NULL,8,NULL,'2018-05-25 05:46:15'),(58,73,2,14,NULL,5,22,200,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:47:00',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:47:00'),(59,73,2,14,NULL,6,22,300,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:47:00',NULL,2,'2001-01-01',NULL,NULL,NULL,4,NULL,'2018-05-25 05:47:00'),(60,80,7,14,NULL,20,22,888,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:47:00',NULL,2,'2001-01-01',NULL,NULL,NULL,8,NULL,'2018-05-25 05:47:00'),(61,79,7,14,NULL,21,22,100,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:49:23',NULL,NULL,'2001-01-01',NULL,NULL,NULL,8,NULL,'2018-05-25 05:49:23'),(62,79,7,14,NULL,21,22,100,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:49:23',NULL,1,'2001-01-01',NULL,NULL,NULL,8,NULL,'2018-05-25 05:49:23'),(63,79,7,14,NULL,21,22,100,NULL,NULL,'2001-01-01 00:00:00',0,NULL,'2018-05-25 05:49:42',15,2,'2001-01-01',NULL,NULL,NULL,8,NULL,'2018-05-25 05:49:42'),(64,73,1,14,3,1,29,200,NULL,'2018-05-25 08:02:50','2002-01-01 00:00:00',0,NULL,'2018-05-25 08:02:50',NULL,3,'2002-01-01',1,1,NULL,4,NULL,'2018-05-25 08:02:50'),(65,73,1,14,3,2,29,120,NULL,'2018-05-25 08:15:40','2002-01-01 00:00:00',0,NULL,'2018-05-25 08:15:40',NULL,3,'2002-01-01',1,2,NULL,4,NULL,'2018-05-25 08:15:40'),(66,72,2,14,NULL,22,22,NULL,NULL,NULL,'2018-05-25 08:52:01',0,NULL,'2018-05-25 08:52:01',NULL,NULL,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 08:52:01'),(67,72,2,14,NULL,22,22,NULL,NULL,NULL,'2018-05-25 08:52:01',0,NULL,'2018-05-25 08:52:01',18,6,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 08:52:01'),(68,72,2,14,NULL,22,22,NULL,NULL,NULL,'2018-05-25 08:52:01',0,NULL,'2018-05-25 08:52:01',18,6,'2018-05-25',NULL,NULL,NULL,4,NULL,'2018-05-25 08:52:01'),(69,84,8,14,NULL,23,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',NULL,NULL,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(70,84,8,14,NULL,23,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(71,85,8,14,NULL,24,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',NULL,NULL,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(72,85,8,14,NULL,24,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(73,86,8,14,NULL,25,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',NULL,NULL,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(74,86,8,14,NULL,25,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(75,87,8,14,NULL,26,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',NULL,NULL,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(76,87,8,14,NULL,26,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(77,88,8,14,NULL,27,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',NULL,NULL,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(78,88,8,14,NULL,27,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(79,84,8,14,NULL,23,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(80,85,8,14,NULL,24,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(81,86,8,14,NULL,25,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(82,87,8,14,NULL,26,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51'),(83,88,8,14,NULL,27,22,NULL,NULL,NULL,'2018-05-25 08:53:51',0,NULL,'2018-05-25 08:53:51',21,6,'2018-05-25',NULL,NULL,NULL,8,NULL,'2018-05-25 08:53:51');
/*!40000 ALTER TABLE `service_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_named`
--

DROP TABLE IF EXISTS `service_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `for_account` tinyint(1) NOT NULL DEFAULT '0',
  `frequency_id` int(11) DEFAULT NULL,
  `deficiency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_need_send_municipality_report` tinyint(1) NOT NULL DEFAULT '0',
  `is_need_municipality_fee` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_D892216094879022` (`frequency_id`),
  CONSTRAINT `FK_D892216094879022` FOREIGN KEY (`frequency_id`) REFERENCES `service_frequency` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_named`
--

LOCK TABLES `service_named` WRITE;
/*!40000 ALTER TABLE `service_named` DISABLE KEYS */;
INSERT INTO `service_named` VALUES (1,'Rebuild check valve',0,NULL,'Check valve #1 is leaking/failing',0,0),(2,'Rebuild check valve',0,NULL,'Check valve #2 is leaking/failing',0,0),(3,'Replace shut-off valve',0,NULL,'Shut-off valve #1 Inoperative',0,0),(4,'Replace shut-off valve',0,NULL,'Shut-off valve #2 Inoperative',0,0),(5,'Repack valve packing',0,NULL,'Shut-off valve #1 packing leaking',0,0),(6,'Repack valve packing',0,NULL,'Shut-off valve #2 packing leaking',0,0),(7,'Rebuild relief valve',0,NULL,'Air inlet is leaking',0,0),(8,'Unplug/rebuild relief valve',0,NULL,'Air inlet is plugged',0,0),(9,'Total HARD parts rebuild',0,NULL,'Both check valves and relief valve are leaking/failing',0,0),(10,'Total SOFT parts rebuild',0,NULL,'Both check valves and relief valve are leaking/failing',0,0),(11,'Total HARD parts rebuild',0,NULL,'Both check valves are leaking/failing',0,0),(12,'Total SOFT parts rebuild',0,NULL,'Both check valves are leaking/failing',0,0),(13,'Replace poppet assembly',0,NULL,'Broken #1 poppet assembly',0,0),(14,'Replace poppet assembly',0,NULL,'Broken #2 poppet assembly',0,0),(15,'Install new device',0,NULL,'By-Pass device missing',0,0),(16,'Install new device',0,NULL,'By-Pass device frozen/damaged',0,0),(17,'Install new sensing line',0,NULL,'Damaged sensing line',0,0),(18,'Install new test cock #1',0,NULL,'Damaged test cock #1',0,0),(19,'Install new test cock #2',0,NULL,'Damaged test cock #2',0,0),(20,'Install new test cock #3',0,NULL,'Damaged test cock #3',0,0),(21,'Install new test cock #4',0,NULL,'Damaged test cock #4',0,0),(22,'Repair valve body',0,NULL,'Device leaking from valve body',0,0),(23,'Replace device',0,NULL,'Device leaking from valve body',0,0),(24,'Install new device',0,NULL,'Device Missing',0,0),(25,'Replace O-Rings with new',0,NULL,'Device O-Rings missing or damaged',0,0),(26,'Replace device',0,NULL,'Freeze damage',0,0),(27,'Install correctly',0,NULL,'Improper Installation',0,0),(28,'Total HARD parts rebuild',0,NULL,'Low test results',0,0),(29,'Total SOFT parts rebuild',0,NULL,'Low test results',0,0),(30,'Install new test cock #1',0,NULL,'Missing test cock #1',0,0),(31,'Install new test cock #2',0,NULL,'Missing test cock #2',0,0),(32,'Install new test cock #3',0,NULL,'Missing test cock #3',0,0),(33,'Install new test cock #4',0,NULL,'Missing test cock #4',0,0),(34,'Replace spring assembly',0,NULL,'Missing/broken spring on #1 check valve',0,0),(35,'Replace spring assembly',0,NULL,'Missing/broken spring on #2 check valve',0,0),(36,'Replace spring assembly',0,NULL,'Missing/broken spring on relief valve',0,0),(37,'Install air-gap',0,NULL,'No air-gap',0,0),(38,'Install drain line',0,NULL,'No drain line',0,0),(39,'Rod floor drain to clear debris',0,NULL,'Floor drain backing up',0,0),(40,'Add Y-Strainer',0,NULL,'No Y-Strainer, debris entering device',0,0),(41,'Install new sensing line',0,NULL,'Plugged sensing line',0,0),(42,'Rebuild relief valve',0,NULL,'Relief valve is leaking/failing',0,0),(43,'Rebuild relief valve',0,NULL,'Relief valve is not opening',0,0),(44,'Replace with new',0,NULL,'Warped check covers',0,0),(45,'Replace with new',0,NULL,'Y-Strainer damaged internally',0,0),(46,'Replace with new',0,NULL,'Y-Strainer missing screen',0,0),(47,'Install new by-pass device and meter',0,NULL,'By-Pass meter and device have freeze damage',0,0),(48,'Install new meter',0,NULL,'By-Pass meter has freeze damage',0,0),(49,'Install new approved device',0,NULL,'Repair parts are no longer made',0,0),(50,'Install new approved device',0,NULL,'Currently no backflow preventer installed',0,0),(51,'Install pressure rectifier',0,NULL,'Major pressure fluctuation',0,0),(52,'Replace relief lid',0,NULL,'Relief lid is pitted and leaking',0,0),(53,'Install new approved device',0,NULL,'Brass seats are pitted and chipped',0,0),(54,'Clean and service Y-Strainer',0,NULL,'Wye strainer is plugged',0,0),(55,'Install hammer arrestor',0,NULL,'Water hammer is damaging device',0,0),(56,'Total HARD parts rebuild',0,NULL,'Broken hard parts',0,0),(57,'Replace with new',0,NULL,'Broken fitting',0,0),(58,'Replace bulk head fitting (special order)',0,NULL,'Broken bulk head',0,0),(59,'Repair as needed',0,NULL,'Device is leaking and not from relief valve',0,0),(60,'Rebuild reilef valve (hard parts)',0,NULL,'Relief valve VT broken/warped',0,0),(61,'File decommission report',0,NULL,'Device no longer in use',0,0),(62,'Install vacuum breaker',0,NULL,'Site needs a vacuum breaker Installed',0,0),(63,'Install new shut off valve',0,NULL,'Shut off valve #1 missing',0,0),(64,'Install new shut off valve',0,NULL,'Shut off valve #2 missing',0,0),(65,'Clean inter valve body ports',0,NULL,'Supply line plugged',0,0),(66,'Soft parts rebuild and valve body cleaning',0,NULL,'Hydraulic moan coming from device',0,0),(67,'Replace/rebuild check valve',0,NULL,'Booster pump check failing',0,0),(68,'Repair leaking pipe upstream',0,NULL,'Main supply leaking',0,0),(69,'Repair leaking pipe downstream',0,NULL,'Piping downstream of backflow leaking',0,0),(70,'Install new approved device',0,NULL,'Obsolete device',0,0),(71,'Install new approved device',0,NULL,'Device is not repairable',0,0),(72,'Post-repair Backflow Inspection',0,NULL,'Device requires post-repair test',1,1),(73,'Annual Backflow Inspection',0,4,'Deficiency1',1,1),(74,'Annual Fire Sprinkler System Inspection',0,4,'Deficiency7',1,1),(75,'5 Year Obstruction Investigation',0,6,'Deficiency8',1,1),(76,'Annual Fire Pump Inspection',0,4,'Deficiency9',1,1),(77,'Annual Anti-Freeze Test & Inspection',0,4,'Deficiency10',1,1),(78,'Annual Dry Valve Trip Test',0,4,'Deficiency11',1,1),(79,'Annual Fire Extinguisher Inspection',0,4,'Deficiency12',1,0),(80,'6 Year Hydrotest',0,7,'Deficiency46',1,0),(81,'Annual Fire Alarm System Inspection',0,4,'Deficiency13',1,1),(82,'Test Install New Test Cock',0,NULL,'Deficiency14',0,0),(83,'Test Repair Valve Body',0,NULL,'Deficiency15',1,1),(84,'Test Replace Device',0,NULL,'Deficiency16',0,0),(85,'Test Install New Device',0,NULL,'Deficiency17',1,1),(86,'Test Replace O Rings With New',0,NULL,'Deficiency18',0,0),(87,'Test Install Correctly',0,NULL,'Deficiency19',1,1),(88,'Test Total HARD Parts Rebuild',0,NULL,'Deficiency20',0,0),(89,'Test Replace Device',0,NULL,'Deficiency21',1,1),(90,'Test Install Correctly',0,NULL,'Deficiency22',0,0),(91,'Test Replace Device',0,NULL,'Deficiency30',1,1),(92,'Test Replace Spring Assembly',0,NULL,'Deficiency31',0,0),(93,'Test Install Correctly',0,NULL,'Deficiency32',1,1),(94,'Test Replace Device',0,NULL,'Deficiency33',0,0),(95,'Lift Station non-frequency service 1',0,NULL,'Lift Station deficiency 1',1,1),(96,'Lift Station non-frequency service 2',0,NULL,'Lift Station deficiency 2',1,1),(97,'Lift Station non-frequency service 3',0,NULL,'Lift Station deficiency 3',1,1),(98,'Water Heater non-frequency service 1',0,NULL,'Water Heater deficiency 1',1,1),(99,'Water Heater non-frequency service 2',0,NULL,'Water Heater deficiency 2',1,1),(100,'Water Heater non-frequency service 3',0,NULL,'Water Heater deficiency 3',1,1),(101,'Test Install New Device',0,NULL,'Deficiency34',1,1),(102,'Test Install Correctly',0,NULL,'Deficiency35',1,1),(103,'Test Install Correctly',0,NULL,'Deficiency36',1,1),(104,'Test Install New Device',0,NULL,'Deficiency37',1,1),(105,'Test Install Correctly',0,NULL,'Deficiency38',1,1),(106,'Test Install New Device',0,NULL,'Deficiency39',1,1),(107,'Test Install Correctly',0,NULL,'Deficiency40',1,1),(108,'Test Install New Device',0,NULL,'Deficiency41',1,1),(109,'Test Install Correctly',0,NULL,'Deficiency42',1,1),(110,'Test Install New Device',0,NULL,'Deficiency43',1,1),(111,'Test Install Correctly',0,NULL,'Deficiency44',1,1),(112,'Test Install New Device',0,NULL,'Deficiency45',1,1),(113,'Test One Water Heater Device',0,4,NULL,1,1),(114,'Test Two Water Heater Device',0,4,NULL,1,1),(115,'Test One Lift Station Device',0,4,NULL,1,1),(116,'Test Two Lift Station Device',0,4,NULL,1,1),(117,'Bi-Annual Range Hood Inspection',0,3,NULL,1,1);
/*!40000 ALTER TABLE `service_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_named_device_named`
--

DROP TABLE IF EXISTS `service_named_device_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_named_device_named` (
  `service_named_id` int(11) NOT NULL,
  `device_named_id` int(11) NOT NULL,
  PRIMARY KEY (`service_named_id`,`device_named_id`),
  KEY `IDX_ADE484A8BC08E943` (`service_named_id`),
  KEY `IDX_ADE484A8CB4AEB5E` (`device_named_id`),
  CONSTRAINT `FK_ADE484A8BC08E943` FOREIGN KEY (`service_named_id`) REFERENCES `service_named` (`id`),
  CONSTRAINT `FK_ADE484A8CB4AEB5E` FOREIGN KEY (`device_named_id`) REFERENCES `device_named` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_named_device_named`
--

LOCK TABLES `service_named_device_named` WRITE;
/*!40000 ALTER TABLE `service_named_device_named` DISABLE KEYS */;
INSERT INTO `service_named_device_named` VALUES (1,6),(2,6),(3,6),(4,6),(5,6),(6,6),(7,6),(8,6),(9,6),(10,6),(11,6),(12,6),(13,6),(14,6),(15,6),(16,6),(17,6),(18,6),(19,6),(20,6),(21,6),(22,6),(23,6),(24,6),(25,6),(26,6),(27,6),(28,6),(29,6),(30,6),(31,6),(32,6),(33,6),(34,6),(35,6),(36,6),(37,6),(38,6),(39,6),(40,6),(41,6),(42,6),(43,6),(44,6),(45,6),(46,6),(47,6),(48,6),(49,6),(50,6),(51,6),(52,6),(53,6),(54,6),(55,6),(56,6),(57,6),(58,6),(59,6),(60,6),(61,6),(62,6),(63,6),(64,6),(65,6),(66,6),(67,6),(68,6),(69,6),(70,6),(71,6),(72,6),(73,6),(74,3),(74,7),(74,8),(74,9),(74,10),(74,14),(74,15),(74,16),(75,3),(75,7),(75,8),(75,9),(75,10),(75,14),(75,15),(75,16),(76,4),(77,3),(77,7),(77,8),(77,9),(77,10),(77,14),(77,15),(77,16),(78,3),(78,7),(78,8),(78,9),(78,10),(78,14),(78,15),(78,16),(79,5),(79,12),(80,5),(80,12),(81,11),(82,12),(83,12),(84,3),(85,3),(86,3),(87,3),(88,3),(89,4),(90,4),(91,9),(92,9),(93,15),(94,15),(95,2),(96,2),(97,2),(98,1),(99,1),(100,1),(101,10),(102,10),(103,7),(104,7),(105,14),(106,14),(107,9),(108,9),(109,8),(110,8),(111,16),(112,16),(113,1),(114,1),(115,2),(116,2),(117,13);
/*!40000 ALTER TABLE `service_named_device_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_patern`
--

DROP TABLE IF EXISTS `service_patern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_patern` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_patern`
--

LOCK TABLES `service_patern` WRITE;
/*!40000 ALTER TABLE `service_patern` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_patern` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_status`
--

DROP TABLE IF EXISTS `service_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_status`
--

LOCK TABLES `service_status` WRITE;
/*!40000 ALTER TABLE `service_status` DISABLE KEYS */;
INSERT INTO `service_status` VALUES (1,'RETEST OPPORTUNITY','under_retest_opportunity','app-status-label--yellow'),(2,'UNDER RETEST NOTICE','under_retest_notice','app-status-label--gray'),(3,'UNDER WORKORDER','under_workorder','app-status-label--gray'),(4,'DELETED','deleted','app-status-label--red'),(5,'REPAIR OPPORTUNITY','repair_opportunity','app-status-label--yellow'),(6,'UNDER PROPOSAL','under_proposal','app-status-label--gray'),(7,'RESOLVED','resolved','app-status-label--gray'),(8,'CANCELED','canceled','app-status-label--gray'),(9,'DISCARDED','discarded','app-status-label--gray'),(10,'OMITTED','omitted','app-status-label--gray');
/*!40000 ALTER TABLE `service_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `special_notification`
--

DROP TABLE IF EXISTS `special_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `special_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `division_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6792D8C341859289` (`division_id`),
  CONSTRAINT `FK_6792D8C341859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `special_notification`
--

LOCK TABLES `special_notification` WRITE;
/*!40000 ALTER TABLE `special_notification` DISABLE KEYS */;
INSERT INTO `special_notification` VALUES (1,'Domestic Water to be off for 1 hour','domestic_water_to_be_off_for_1_hour','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(2,'Domestic Water to be off for 2 Hrs','domestic_water_to_be_off_for_2_hrs','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(3,'Domestic Water to be off for 4 Hrs','domestic_water_to_be_off_for_4_hrs','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(4,'Domestic Water to be off for 8 Hrs','domestic_water_to_be_off_for_8_hrs','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(5,'Domestic Water to be off for 24 Hrs','domestic_water_to_be_off_for_24_hrs','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(6,'Domestic Water Will Not be Affected','domestic_water_will_not_be_affected','2018-05-25 05:08:03','2018-05-25 05:08:03',1),(7,'Test Special Notification Fire One','test_special_notification_fire_one','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(8,'Test Special Notification Fire Two','test_special_notification_fire_two','2018-05-25 05:08:03','2018-05-25 05:08:03',2),(9,'Test Special Notification Plumbing One','test_special_notification_plumbing_one','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(10,'Test Special Notification Plumbing Two','test_special_notification_plumbing_two','2018-05-25 05:08:03','2018-05-25 05:08:03',3),(11,'Test Special Notification Alarm One','test_special_notification_alarm_one','2018-05-25 05:08:03','2018-05-25 05:08:03',4),(12,'Test Special Notification Alarm Two','test_special_notification_alarm_two','2018-05-25 05:08:03','2018-05-25 05:08:03',4);
/*!40000 ALTER TABLE `special_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Alabama','alabama','2018-05-25 05:07:41','2018-05-25 05:07:41','AL'),(2,'Alaska','alaska','2018-05-25 05:07:41','2018-05-25 05:07:41','AK'),(3,'Arizona','arizona','2018-05-25 05:07:41','2018-05-25 05:07:41','AZ'),(4,'Arkansas','arkansas','2018-05-25 05:07:41','2018-05-25 05:07:41','AR'),(5,'California','california','2018-05-25 05:07:41','2018-05-25 05:07:41','CA'),(6,'Colorado','colorado','2018-05-25 05:07:41','2018-05-25 05:07:41','CO'),(7,'Connecticut','connecticut','2018-05-25 05:07:41','2018-05-25 05:07:41','CT'),(8,'Delaware','delaware','2018-05-25 05:07:41','2018-05-25 05:07:41','DE'),(9,'Florida','florida','2018-05-25 05:07:41','2018-05-25 05:07:41','FL'),(10,'Georgia','georgia','2018-05-25 05:07:41','2018-05-25 05:07:41','GA'),(11,'Hawaii','hawaii','2018-05-25 05:07:41','2018-05-25 05:07:41','HI'),(12,'Idaho','idaho','2018-05-25 05:07:41','2018-05-25 05:07:41','ID'),(13,'Illinois','illinois','2018-05-25 05:07:41','2018-05-25 05:07:41','IL'),(14,'Indiana','indiana','2018-05-25 05:07:41','2018-05-25 05:07:41','IN'),(15,'Iowa','iowa','2018-05-25 05:07:41','2018-05-25 05:07:41','IA'),(16,'Kansas','kansas','2018-05-25 05:07:41','2018-05-25 05:07:41','KS'),(17,'Kentucky','kentucky','2018-05-25 05:07:41','2018-05-25 05:07:41','KY'),(18,'Louisiana','louisiana','2018-05-25 05:07:41','2018-05-25 05:07:41','LA'),(19,'Maine','maine','2018-05-25 05:07:41','2018-05-25 05:07:41','ME'),(20,'Maryland','maryland','2018-05-25 05:07:41','2018-05-25 05:07:41','MD'),(21,'Massachusetts','massachusetts','2018-05-25 05:07:41','2018-05-25 05:07:41','MA'),(22,'Michigan','michigan','2018-05-25 05:07:41','2018-05-25 05:07:41','MI'),(23,'Minnesota','minnesota','2018-05-25 05:07:41','2018-05-25 05:07:41','MN'),(24,'Mississippi','mississippi','2018-05-25 05:07:41','2018-05-25 05:07:41','MS'),(25,'Missouri','missouri','2018-05-25 05:07:41','2018-05-25 05:07:41','MO'),(26,'Montana','montana','2018-05-25 05:07:41','2018-05-25 05:07:41','MT'),(27,'Nebraska','nebraska','2018-05-25 05:07:41','2018-05-25 05:07:41','NE'),(28,'Nevada','nevada','2018-05-25 05:07:41','2018-05-25 05:07:41','NV'),(29,'New Hampshire','new hampshire','2018-05-25 05:07:41','2018-05-25 05:07:41','NH'),(30,'New Jersey','new jersey','2018-05-25 05:07:41','2018-05-25 05:07:41','NJ'),(31,'New Mexico','new mexico','2018-05-25 05:07:41','2018-05-25 05:07:41','NM'),(32,'New York','new york','2018-05-25 05:07:41','2018-05-25 05:07:41','NY'),(33,'North Carolina','north carolina','2018-05-25 05:07:41','2018-05-25 05:07:41','NC'),(34,'North Dakota','north dakota','2018-05-25 05:07:41','2018-05-25 05:07:41','ND'),(35,'Ohio','ohio','2018-05-25 05:07:41','2018-05-25 05:07:41','OH'),(36,'Oklahoma','oklahoma','2018-05-25 05:07:41','2018-05-25 05:07:41','OK'),(37,'Oregon','oregon','2018-05-25 05:07:41','2018-05-25 05:07:41','OR'),(38,'Pennsylvania','pennsylvania','2018-05-25 05:07:41','2018-05-25 05:07:41','PA'),(39,'Rhode Island','rhode island','2018-05-25 05:07:41','2018-05-25 05:07:41','RI'),(40,'South Carolina','south carolina','2018-05-25 05:07:41','2018-05-25 05:07:41','SC'),(41,'South Dakota','south dakota','2018-05-25 05:07:41','2018-05-25 05:07:41','SD'),(42,'Tennessee','tennessee','2018-05-25 05:07:41','2018-05-25 05:07:41','TN'),(43,'Texas','texas','2018-05-25 05:07:41','2018-05-25 05:07:41','TX'),(44,'Utah','utah','2018-05-25 05:07:41','2018-05-25 05:07:41','UT'),(45,'Vermont','vermont','2018-05-25 05:07:41','2018-05-25 05:07:41','VT'),(46,'Virginia','virginia','2018-05-25 05:07:41','2018-05-25 05:07:41','VA'),(47,'Washington','washington','2018-05-25 05:07:41','2018-05-25 05:07:41','WA'),(48,'West Virginia','west virginia','2018-05-25 05:07:41','2018-05-25 05:07:41','WV'),(49,'Wisconsin','wisconsin','2018-05-25 05:07:41','2018-05-25 05:07:41','WI'),(50,'Wyoming','wyoming','2018-05-25 05:07:41','2018-05-25 05:07:41','WY');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder`
--

DROP TABLE IF EXISTS `workorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `grand_total` double DEFAULT NULL,
  `total_service_fee` double DEFAULT NULL,
  `directions` longtext COLLATE utf8_unicode_ci,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `internal_comment` longtext COLLATE utf8_unicode_ci,
  `authorizer_id` int(11) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `scheduled_from` datetime DEFAULT NULL,
  `scheduled_to` datetime DEFAULT NULL,
  `is_frozen` tinyint(1) DEFAULT '0',
  `account_authorizer_id` int(11) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `total_estimated_time_inspections` double DEFAULT NULL,
  `total_estimated_time_repairs` double DEFAULT NULL,
  `additional_comments` longtext COLLATE utf8_unicode_ci,
  `after_hours_work_cost` double DEFAULT NULL,
  `additional_services_cost` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `total_repairs_price` double DEFAULT NULL,
  `total_munic_and_proc_fee` double DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_inspections_count` int(11) DEFAULT NULL,
  `total_repairs_count` int(11) DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `finisher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_51CF52BB9B6B5FBA` (`account_id`),
  KEY `IDX_51CF52BB6BF700BD` (`status_id`),
  KEY `IDX_51CF52BB41859289` (`division_id`),
  KEY `IDX_51CF52BB3CEFA549` (`authorizer_id`),
  KEY `IDX_51CF52BB5AA1164F` (`payment_method_id`),
  KEY `IDX_51CF52BBA3C999FA` (`account_authorizer_id`),
  KEY `IDX_51CF52BB17653B16` (`payment_term_id`),
  KEY `IDX_51CF52BB64D218E` (`location_id`),
  KEY `IDX_51CF52BBC54C8C93` (`type_id`),
  KEY `IDX_51CF52BB95D2E802` (`finisher_id`),
  CONSTRAINT `FK_51CF52BB17653B16` FOREIGN KEY (`payment_term_id`) REFERENCES `payment_term` (`id`),
  CONSTRAINT `FK_51CF52BB3CEFA549` FOREIGN KEY (`authorizer_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_51CF52BB41859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_51CF52BB5AA1164F` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`),
  CONSTRAINT `FK_51CF52BB64D218E` FOREIGN KEY (`location_id`) REFERENCES `coordinate` (`id`),
  CONSTRAINT `FK_51CF52BB6BF700BD` FOREIGN KEY (`status_id`) REFERENCES `workorder_status` (`id`),
  CONSTRAINT `FK_51CF52BB95D2E802` FOREIGN KEY (`finisher_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_51CF52BB9B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_51CF52BBA3C999FA` FOREIGN KEY (`account_authorizer_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_51CF52BBC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `workorder_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder`
--

LOCK TABLES `workorder` WRITE;
/*!40000 ALTER TABLE `workorder` DISABLE KEYS */;
INSERT INTO `workorder` VALUES (1,14,3,1,838,770,NULL,'2018-05-25 05:27:47','2018-05-25 07:59:58',NULL,NULL,1,'2018-05-25 07:00:00','2018-05-25 12:00:00',0,NULL,NULL,10,0,NULL,0,NULL,0,NULL,NULL,NULL,0,68,2,NULL,4,NULL,770,NULL),(2,14,3,1,NULL,NULL,NULL,'2018-05-25 05:29:41','2018-05-25 07:39:25',NULL,NULL,1,'2018-05-25 07:00:00','2018-05-31 19:30:00',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(3,10,3,2,571,520,NULL,'2018-05-25 05:38:53','2018-05-25 05:40:27',NULL,NULL,1,'2018-05-25 07:00:00','2018-05-25 08:00:00',0,NULL,NULL,40.9,0,NULL,0,NULL,0,NULL,NULL,NULL,0,51,2,NULL,3,NULL,520,NULL),(4,9,3,1,991,771,'8789789797','2018-05-25 05:43:00','2018-05-25 05:46:42','23132112332',3,4,'2018-05-25 07:00:00','2018-05-25 23:30:00',0,NULL,NULL,12.5,11,'5465445645456',8,9,2,NULL,NULL,NULL,120,85,2,NULL,5,3,891,NULL),(5,10,3,2,154,0,'888','2018-05-25 05:44:19','2018-05-25 05:44:48','555',2,3,'2018-05-25 07:00:00','2018-05-25 08:00:00',0,NULL,NULL,0,9,'999',2,1,3,NULL,NULL,NULL,120,34,2,NULL,NULL,2,120,NULL),(6,14,3,1,534,500,'88','2018-05-25 05:47:41','2018-05-25 05:48:33','55',NULL,1,'2018-05-25 07:00:00','2018-05-25 08:00:00',0,NULL,NULL,5,0,'99',1,2,3,NULL,NULL,NULL,0,34,2,NULL,2,NULL,500,NULL),(7,14,3,2,1022,988,NULL,'2018-05-25 05:50:44','2018-05-25 05:50:59',NULL,NULL,1,'2018-05-25 07:00:00','2018-05-25 08:00:00',0,NULL,NULL,11.5,0,NULL,0,NULL,0,NULL,NULL,NULL,0,34,2,NULL,2,NULL,988,NULL),(8,14,3,1,108,0,NULL,'2018-05-25 08:52:26','2018-05-25 08:52:51',NULL,NULL,1,'2018-05-25 07:00:00','2018-05-25 08:00:00',0,NULL,NULL,0,10,'2321123',1,NULL,10,NULL,NULL,NULL,100,17,2,NULL,NULL,1,100,NULL),(9,14,3,2,155,0,NULL,'2018-05-25 08:54:16','2018-05-25 08:54:29',NULL,NULL,1,'2018-05-25 07:00:00','2018-05-25 08:00:00',0,NULL,NULL,0,10,'1111',11,NULL,1,NULL,NULL,NULL,111,34,2,NULL,NULL,5,111,NULL);
/*!40000 ALTER TABLE `workorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_device_info`
--

DROP TABLE IF EXISTS `workorder_device_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_device_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repair_special_notification_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `repair_comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_estimation_time` double DEFAULT NULL,
  `inspection_estimation_time` double DEFAULT NULL,
  `subtotal_inspections_fee` double DEFAULT NULL,
  `repair_parts_and_labour_price` double DEFAULT NULL,
  `subtotal_munic_and_proc_fee` double DEFAULT NULL,
  `device_total` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D53ECF8555272D6` (`repair_special_notification_id`),
  KEY `IDX_D53ECF852C1C3467` (`workorder_id`),
  KEY `IDX_D53ECF8594A4C7D4` (`device_id`),
  CONSTRAINT `FK_D53ECF852C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_D53ECF8555272D6` FOREIGN KEY (`repair_special_notification_id`) REFERENCES `special_notification` (`id`),
  CONSTRAINT `FK_D53ECF8594A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_device_info`
--

LOCK TABLES `workorder_device_info` WRITE;
/*!40000 ALTER TABLE `workorder_device_info` DISABLE KEYS */;
INSERT INTO `workorder_device_info` VALUES (1,NULL,1,1,NULL,NULL,10,770,NULL,68,838),(2,NULL,3,3,NULL,NULL,12.5,300,NULL,17,317),(3,NULL,3,4,NULL,NULL,20,100,NULL,17,117),(4,NULL,3,5,NULL,NULL,8.4,120,NULL,17,137),(5,NULL,4,6,'огпогамог шанегнакнев',11,12.5,771,120,85,976),(6,NULL,5,5,'8888',9,NULL,NULL,120,34,154),(7,NULL,6,2,NULL,NULL,5,500,NULL,34,534),(8,NULL,7,7,NULL,NULL,11.5,988,NULL,34,1022),(9,NULL,8,2,'орогр',10,NULL,NULL,100,17,117),(10,NULL,9,8,'ро',10,NULL,NULL,111,34,145);
/*!40000 ALTER TABLE `workorder_device_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_frozen_content`
--

DROP TABLE IF EXISTS `workorder_frozen_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_frozen_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_history_id` int(11) DEFAULT NULL,
  `device_history_id` int(11) DEFAULT NULL,
  `service_history_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `municipality_fee` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `processing_fee` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AE06C4E7E29E76F6` (`account_history_id`),
  KEY `IDX_AE06C4E79936C794` (`device_history_id`),
  KEY `IDX_AE06C4E78854BDE6` (`service_history_id`),
  KEY `IDX_AE06C4E72C1C3467` (`workorder_id`),
  CONSTRAINT `FK_AE06C4E72C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_AE06C4E78854BDE6` FOREIGN KEY (`service_history_id`) REFERENCES `service_history` (`id`),
  CONSTRAINT `FK_AE06C4E79936C794` FOREIGN KEY (`device_history_id`) REFERENCES `device_history` (`id`),
  CONSTRAINT `FK_AE06C4E7E29E76F6` FOREIGN KEY (`account_history_id`) REFERENCES `account_history` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_frozen_content`
--

LOCK TABLES `workorder_frozen_content` WRITE;
/*!40000 ALTER TABLE `workorder_frozen_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `workorder_frozen_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_log`
--

DROP TABLE IF EXISTS `workorder_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `proposal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3711E899F675F31B` (`author_id`),
  KEY `IDX_3711E8992C1C3467` (`workorder_id`),
  KEY `IDX_3711E899C54C8C93` (`type_id`),
  KEY `IDX_3711E8992989F1FD` (`invoice_id`),
  KEY `IDX_3711E899F4792058` (`proposal_id`),
  CONSTRAINT `FK_3711E8992989F1FD` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  CONSTRAINT `FK_3711E8992C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_3711E899C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `workorder_log_type` (`id`),
  CONSTRAINT `FK_3711E899F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_3711E899F675F31B` FOREIGN KEY (`author_id`) REFERENCES `contractor_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_log`
--

LOCK TABLES `workorder_log` WRITE;
/*!40000 ALTER TABLE `workorder_log` DISABLE KEYS */;
INSERT INTO `workorder_log` VALUES (1,9,'2018-05-25 05:27:48','Authorized by Tony Stark. ','Retest Notice was added to Workorder',1,1,NULL,1),(2,9,'2018-05-25 05:29:41','Authorized by Tony Stark. ','Custom proposal was added to Generic Workorder',2,1,NULL,2),(3,NULL,'2018-05-25 05:29:58',NULL,'Scheduled date has come. Workorder is to be done today.',2,6,NULL,NULL),(4,9,'2018-05-25 05:29:58',NULL,'Brin Sergey was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',2,2,NULL,NULL),(5,NULL,'2018-05-25 05:30:29',NULL,'Scheduled date has come. Workorder is to be done today.',1,6,NULL,NULL),(6,9,'2018-05-25 05:30:29',NULL,'Brin Sergey was assigned to the workorder to 05/25/2018, 11:30 AM - 12:00 PM',1,2,NULL,NULL),(7,NULL,'2018-05-25 05:31:53',NULL,'Scheduled date has come. Workorder is to be done today.',2,6,NULL,NULL),(8,9,'2018-05-25 05:31:53',NULL,'Brin Sergey was assigned to the workorder to 05/31/2018, 07:00 AM - 07:30 PM',2,2,NULL,NULL),(9,9,'2018-05-25 05:31:59',NULL,'Brin Sergey was unassigned from the workorder from 05/25/2018, 07:00 AM - 08:00 AM',2,NULL,NULL,NULL),(10,9,'2018-05-25 05:38:54','Authorized by Tony Stark. ','Retest Notice was added to Workorder',3,1,NULL,3),(11,NULL,'2018-05-25 05:40:27',NULL,'Scheduled date has come. Workorder is to be done today.',3,6,NULL,NULL),(12,9,'2018-05-25 05:40:27',NULL,'Brin Sergey was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',3,2,NULL,NULL),(13,9,'2018-05-25 05:43:01','Authorized by Petyr Bealish. ','Repair Proposal was added to Workorder',4,1,NULL,5),(14,9,'2018-05-25 05:43:09','Authorized by Petyr Bealish. ','Retest Notice was added to Workorder',4,1,NULL,4),(15,NULL,'2018-05-25 05:43:41',NULL,'Scheduled date has come. Workorder is to be done today.',4,6,NULL,NULL),(16,9,'2018-05-25 05:43:41',NULL,'Brin Sergey was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',4,2,NULL,NULL),(17,9,'2018-05-25 05:44:21','Authorized by Tony Stark. ','Repair Proposal was added to Workorder',5,1,NULL,6),(18,NULL,'2018-05-25 05:44:48',NULL,'Scheduled date has come. Workorder is to be done today.',5,6,NULL,NULL),(19,9,'2018-05-25 05:44:48',NULL,'Brin Sergey was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',5,2,NULL,NULL),(20,NULL,'2018-05-25 05:46:42',NULL,'Scheduled date has come. Workorder is to be done today.',4,6,NULL,NULL),(21,16,'2018-05-25 05:46:42',NULL,'Gates Bill was assigned to the workorder to 05/25/2018, 07:00 AM - 11:30 PM',4,2,NULL,NULL),(22,9,'2018-05-25 05:47:42','Authorized by Tony Stark. ','Retest Notice was added to Workorder',6,1,NULL,7),(23,NULL,'2018-05-25 05:48:33',NULL,'Scheduled date has come. Workorder is to be done today.',6,6,NULL,NULL),(24,9,'2018-05-25 05:48:33',NULL,'Brin Sergey was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',6,2,NULL,NULL),(25,9,'2018-05-25 05:50:45','Authorized by Tony Stark. ','Retest Notice was added to Workorder',7,1,NULL,8),(26,NULL,'2018-05-25 05:50:59',NULL,'Scheduled date has come. Workorder is to be done today.',7,6,NULL,NULL),(27,9,'2018-05-25 05:50:59',NULL,'Brin Sergey was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',7,2,NULL,NULL),(28,NULL,'2018-05-25 07:39:25',NULL,'Scheduled date has come. Workorder is to be done today.',2,6,NULL,NULL),(29,9,'2018-05-25 07:39:25',NULL,'Brin Sergey was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',2,2,NULL,NULL),(30,NULL,'2018-05-25 07:59:58',NULL,'Scheduled date has come. Workorder is to be done today.',1,6,NULL,NULL),(31,11,'2018-05-25 07:59:58',NULL,'Elon Mark was assigned to the workorder to 05/25/2018, 07:00 AM - 09:00 AM',1,2,NULL,NULL),(32,NULL,'2018-05-25 08:26:07',NULL,'Scheduled date has come. Workorder is to be done today.',2,6,NULL,NULL),(33,11,'2018-05-25 08:26:07',NULL,'Elon Mark was assigned to the workorder to 05/25/2018, 07:00 AM - 11:30 AM',2,2,NULL,NULL),(34,9,'2018-05-25 08:52:27','Authorized by Tony Stark. ','Repair Proposal was added to Workorder',8,1,NULL,9),(35,NULL,'2018-05-25 08:52:51',NULL,'Scheduled date has come. Workorder is to be done today.',8,6,NULL,NULL),(36,9,'2018-05-25 08:52:51',NULL,'Elon Mark was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',8,2,NULL,NULL),(37,9,'2018-05-25 08:54:17','Authorized by Tony Stark. ','Repair Proposal was added to Workorder',9,1,NULL,10),(38,NULL,'2018-05-25 08:54:29',NULL,'Scheduled date has come. Workorder is to be done today.',9,6,NULL,NULL),(39,9,'2018-05-25 08:54:29',NULL,'Elon Mark was assigned to the workorder to 05/25/2018, 07:00 AM - 08:00 AM',9,2,NULL,NULL);
/*!40000 ALTER TABLE `workorder_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_log_type`
--

DROP TABLE IF EXISTS `workorder_log_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_log_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_log_type`
--

LOCK TABLES `workorder_log_type` WRITE;
/*!40000 ALTER TABLE `workorder_log_type` DISABLE KEYS */;
INSERT INTO `workorder_log_type` VALUES (1,'WORKORDER WAS CREATED','workorder_was_created'),(2,'WORKORDER WAS SCHEDULED','workorder_was_scheduled'),(3,'Reports sent','reports_sent'),(4,'Payment received','payment_received'),(5,'Bill Sent / Invoice Created','bill_sent_invoice_created'),(6,'Scheduled date has come','scheduled_date_has_come'),(7,'Scheduled date has passed','scheduled_date_has_passed'),(8,'DONE','done');
/*!40000 ALTER TABLE `workorder_log_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_status`
--

DROP TABLE IF EXISTS `workorder_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_status`
--

LOCK TABLES `workorder_status` WRITE;
/*!40000 ALTER TABLE `workorder_status` DISABLE KEYS */;
INSERT INTO `workorder_status` VALUES (1,'To Be Scheduled','to_be_scheduled',1,'app-status-label--red'),(2,'Scheduled','scheduled',2,'app-status-label--green'),(3,'TO BE DONE TODAY','to_be_done_today',3,'app-status-label--yellow'),(4,'NOT COMPLETED','not_completed',4,'app-status-label--red'),(5,'PENDING PAYMENT','pending_payment',6,'app-status-label--green'),(6,'Send Reports','send_reports',7,'app-status-label--red'),(7,'Send Bill/Create Invoice','send_bill_create_invoice',5,'app-status-label--red'),(8,'DONE','done',8,'app-status-label--gray');
/*!40000 ALTER TABLE `workorder_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_type`
--

DROP TABLE IF EXISTS `workorder_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_type`
--

LOCK TABLES `workorder_type` WRITE;
/*!40000 ALTER TABLE `workorder_type` DISABLE KEYS */;
INSERT INTO `workorder_type` VALUES (1,'Generic Workorder','generic_workorder'),(2,'Workorder','simple_workorder');
/*!40000 ALTER TABLE `workorder_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-25  8:56:35
