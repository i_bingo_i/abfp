<?php

namespace ServiceBundle\Services;

use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\ServiceHistoryRepository;

class CalculateTotals
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        $this->serviceHistoryRepository = $this->objectManager->getRepository('AppBundle:ServiceHistory');
    }

    /**
     * @param Service[] $services
     * @return float|int
     */
    public function calculateSubtotalMunicAndProcFee($services)
    {
        $subtotalMunicAndProcFee = 0;

        /** @var Service $service */
        foreach ($services as $service) {
            /** @var ServiceHistory $lastServiceHistory */
            $lastServiceHistory = $this->serviceHistoryRepository->getLastRecord($service);
            $serviceHistoryMunAndProcFee = $lastServiceHistory->getMunicipalityFee();

            if ($serviceHistoryMunAndProcFee and !$lastServiceHistory->getDepartmentChannelWithFeesWillVary()) {
                $subtotalMunicAndProcFee += $serviceHistoryMunAndProcFee;
            }
        }

        return $subtotalMunicAndProcFee;
    }

    /**
     * @param Service[] $services
     * @return float
     */
    public function calcTotalFee($services) : float
    {
        $totalFee = 0;
        /** @var Service $service */
        foreach ($services as $service) {
            $totalFee += $service->getFixedPrice();
        }

        return $totalFee;
    }

    /**
     * @param $services
     * @return float|int
     */
    public function calcServicesFixedPrice($services)
    {
        $total = 0;

        /** @var Service $service */
        foreach ($services as $service) {
            $total += (float)$service->getFixedPrice();
        }

        return $total;
    }
}
