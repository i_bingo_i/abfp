<?php

namespace ServiceBundle\Services;

use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\ServiceHistoryRepository;

class ServiceHelper
{
    /** @var ContainerInterface  */
    private $container;
    /** @var  ObjectManager */
    private $objectManager;

    /** @var ServiceManager */
    private $serviceManager;
    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;

    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->serviceManager = $this->container->get('app.service.manager');
        $this->serviceHistoryRepository = $this->objectManager->getRepository('AppBundle:ServiceHistory');
    }

    /**
     * @param Service[] $services
     */
    public function updateServicesAndHistory($services)
    {
        /** @var Service $service */
        foreach ($services as $service) {
            $this->serviceManager->update($service);
        }
    }

    /**
     * @param $services
     * @return bool
     */
    public function checkIfServicesHaveDepartmentChannelWithFeesWillVary($services)
    {
        $ifServicesHaveDepartmentChannelWithFeesWillVary = false;

        /** @var Service $service */
        foreach ($services as $service) {
            /** @var ServiceHistory $lastServiceHistory */
            $lastServiceHistory = $this->serviceHistoryRepository->getLastRecord($service);

            if ($lastServiceHistory->getDepartmentChannelWithFeesWillVary()) {
                $ifServicesHaveDepartmentChannelWithFeesWillVary = true;
            }
        }

        return $ifServicesHaveDepartmentChannelWithFeesWillVary;
    }
}
