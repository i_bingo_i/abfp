<?php

namespace ServiceBundle\Services;

use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\TrashRequests\Creator;

class RepairDiscardService
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var Creator */
    private $trashRequestCreator;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var ServiceManager */
    private $serviceManager;

    private $errorMessages = [];

    /**
     * DeviceFileAttacher constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->trashRequestCreator = $this->serviceContainer->get('app.trash_requests.creator.service');
        $this->serviceManager = $this->serviceContainer->get('app.service.manager');

        $this->serviceRepository = $this->objectManager->getRepository("AppBundle:Service");
    }

    /**
     * @param $params
     * @param $route
     * @return Service|bool
     */
    public function discardByParams($params, $route)
    {
        $parseData = $this->parseDataForDiscard($params);
        $this->checkingForDiscard($parseData);

        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]
            );

            return false;
        }

        /** @var Service $service */
        $service = $parseData["service"];
        $this->serviceManager->discard($service);

        return $service;
    }

    /**
     * @param $data
     * @return array
     */
    private function parseDataForDiscard($data)
    {
        $parseData = [];

        $parseData["service"] = null;
        if (isset($data["serviceID"])) {
            $parseData["service"] = $this->serviceRepository->find($data["serviceID"]);
        }

        return $parseData;
    }

    /**
     * @param $parseData
     */
    private function checkingForDiscard($parseData)
    {
        $this->errorMessages = [];

        if (is_null($parseData["service"])) {
            $this->errorMessages[] = "Repair service not found";
        }

        if (!is_null($parseData["service"]) and
            $parseData["service"]->getStatus()->getAlias() == $this->serviceManager::STATUS_UNDER_WORKORDER) {
            $this->errorMessages[] = "Repair service under workorder";
        }

        if (!is_null($parseData["service"]) and
            $parseData["service"]->getStatus()->getAlias() == $this->serviceManager::STATUS_UNDER_PROPOSAL) {
            $this->errorMessages[] = "Repair service under proposal";
        }

        if (!is_null($parseData["service"]) and
            $parseData["service"]->getStatus()->getAlias() == $this->serviceManager::STATUS_RESOLVED) {
            $this->errorMessages[] = "Repair service is resolved";
        }

        if (!is_null($parseData["service"]) and
            $parseData["service"]->getDeleted()) {
            $this->errorMessages[] = "Repair service is deleted";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages =
                array_merge(["<b>Non-frequency service can not be discarded</b>.<br>"], $this->errorMessages);
        }
    }
}
