<?php

namespace ServiceBundle\Services;

use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\ReportRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ReportStatus;
use AppBundle\Services\TrashRequests\Creator;
use ServiceBundle\Factories\ReportFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Report;
use AppBundle\Entity\EntityManager\ReportManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Services\Files;

class ReportFileAttacher
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var ReportManager */
    private $reportManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var Creator */
    private $trashRequestCreator;
    /** @var ReportRepository */
    private $reportRepository;

    private $errorMessages = [];

    /**
     * ReportCreator constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->reportManager = $this->serviceContainer->get('app.report.manager');
        $this->trashRequestCreator = $this->serviceContainer->get('app.trash_requests.creator.service');
        $this->reportRepository = $this->objectManager->getRepository('AppBundle:Report');
    }

    /**
     * @param $reportID
     * @param null|UploadedFile $file
     * @param $route
     * @param $accessToken
     * @return Report|bool
     */
    public function attachById($reportID, ?UploadedFile $file, $route, $accessToken)
    {
        /** @var Report $report */
        $report = $this->reportRepository->find($reportID);

        $this->checkingForAttach($report, $file);

        if (!empty($this->errorMessages)) {
            $fullPath = "";
            if ($file instanceof UploadedFile) {
                /** @var Files $filesService */
                $filesService = $this->serviceContainer->get('app.files');

                $fileName = $filesService->getRandomFileName($file, 'report_');
                $savePath = "/uploads/bad_request_storage/";
                $filesService->saveUploadFile($file, $savePath, $fileName);
                $fullPath = $savePath . $fileName;
            }

            $this->trashRequestCreator->create(
                $route,
                json_encode(["reportID" => $reportID]),
                $this->errorMessages,
                $fullPath,
                $accessToken
            );
            return false;
        }

        $saveImagePath = "/uploads/reports";
        $filePath = $this->serviceContainer->get('app.images')->saveUploadFile($file, $saveImagePath);

        $report->setFileReport($saveImagePath."/".$filePath);

        $this->reportManager->save($report);

        return $report;
    }

    /**
     * @param Report|null $report
     * @param null|UploadedFile $file
     */
    private function checkingForAttach(?Report $report, ?UploadedFile $file)
    {
        $this->errorMessages = [];

        if (is_null($report)) {
            $this->errorMessages[] = "Report not found";
        }

        if (is_null($file)) {
            $this->errorMessages[] = "File has not received";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Report file can not be uploaded</b>.<br>"], $this->errorMessages);
        }

    }
}
