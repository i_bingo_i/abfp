<?php

namespace ServiceBundle\Services;

use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Service;
use AppBundle\Entity\ReportStatus;
use AppBundle\Services\TrashRequests\Creator;
use ServiceBundle\Factories\ReportFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Report;
use AppBundle\Entity\EntityManager\ReportManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Debug\Exception\ContextErrorException;

class ReportCreator
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var ReportManager */
    private $reportManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var Creator */
    private $trashRequestCreator;

    private $errorMessages = [];

    /**
     * ReportCreator constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->reportManager = $this->serviceContainer->get('app.report.manager');
        $this->serviceManager = $this->serviceContainer->get('app.service.manager');
        $this->trashRequestCreator = $this->serviceContainer->get('app.trash_requests.creator.service');
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->reportStatusRepository = $this->objectManager->getRepository('AppBundle:ReportStatus');
    }

    /**
     * @param array $params
     * @param $route
     * @return Report|bool
     */
    public function createByParams(array $params, $route)
    {
        $parsedData = $this->parseData($params);
        $this->checkingForCreate($parsedData);

        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]
            );
            return false;
        }

        /** @var Report $report */
        $report = ReportFactory::makeByData($parsedData);
        $this->reportManager->create($report);

        /** @var Service $service */
        $service = $report->getService();
        $this->serviceManager->finishServiceActions($service, $report);

        return $report;
    }


    /**
     * @param $data
     * @return array
     */
    private function parseData($data)
    {
        $parsedData = [];

        $parsedData['tester'] = null;
        if (isset($data['userID'])) {
            $parsedData['tester'] = $this->contractorUserRepository->find($data['userID']);
        }

        $parsedData['contractor'] = null;
        if (isset($parsedData['tester'])) {
            $parsedData['contractor'] = $parsedData['tester']->getContractor();
        }

        $parsedData['service'] = null;
        if (isset($data['serviceID'])) {
            $parsedData['service'] = $this->serviceRepository->find($data['serviceID']);
        }

        $parsedData['workOrder'] = null;
        if (isset($parsedData['service'])) {
            $parsedData['workOrder'] = $parsedData['service']->getWorkorder();
        }

        $parsedData['status'] = null;
        if (isset($parsedData['service'], $data['statusAlias'])) {
            $parsedData['status'] = $this->findReportStatusByService($parsedData['service'], $data['statusAlias']);
        }

        if (isset($data['finishTime']) and $data['finishTime']) {
            try {
                $parsedData['finishTime'] = (new \DateTime())->setTimestamp($data['finishTime']);
            } catch (ContextErrorException $exception) {
                $parsedData['finishTime'] = null;
                $this->errorMessages[] = $exception->getMessage();
            }
        }

        if (isset($data['needRepair'])) {
            $parsedData['needRepair'] = $data['needRepair'];
        }

        if (isset($data['printTemplate'])) {
            $parsedData['printTemplate'] = $data['printTemplate'];
        }

        if (isset($data['textReport'])) {
            $parsedData['textReport'] = $data['textReport'];
        }

        if (isset($data['htmlReport'])) {
            $parsedData['htmlReport'] = $data['htmlReport'];
        }

        return $parsedData;
    }

    /**
     * @param $parsedData
     */
    private function checkingForCreate($parsedData)
    {
        $this->errorMessages = [];

        if (is_null($parsedData['contractor'])) {
            $this->errorMessages[] = "Tester not found";
        }

        if (is_null($parsedData['service'])) {
            $this->errorMessages[] = "Service not found";
        }

        if (is_null($parsedData['workOrder'])) {
            $this->errorMessages[] = "You can't  finish service without workorder";
        }

        if (!isset($parsedData['finishTime']) or !$parsedData['finishTime']) {
            $this->errorMessages[] = "FinishTime is required ";
        }

        if (is_null($parsedData['status'])) {
            $this->errorMessages[] = "Report status not found";
        }
    }

    /**
     * @param Service $service
     * @param $dataStatusAlias
     * @return ReportStatus|null|object
     */
    private function findReportStatusByService(Service $service, $dataStatusAlias)
    {
        if (!$service->getNamed()->getFrequency()
            and !$service->getLastReport()
            and !$service->getNamed()->getIsNeedMunicipalityFee()) {
            return $this->reportStatusRepository->findOneBy(["alias" => "completed"]);
        } else {
            return $this->reportStatusRepository->findOneBy(["alias" => $dataStatusAlias]);
        }
    }
}
