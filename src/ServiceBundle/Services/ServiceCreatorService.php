<?php

namespace ServiceBundle\Services;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorType;
use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\OpportunityManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\ContractorTypeRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\ServiceNamed;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Debug\Exception\ContextErrorException;
use AppBundle\Services\TrashRequests\Creator;
use ServiceBundle\Factories\ServiceFactory;
use AppBundle\Entity\Service;

class ServiceCreatorService
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var Creator */
    private $trashRequestCreator;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var ContractorRepository */
    private $contractorRepository;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var DepartmentChannelManager */
    private $departmentChannelManager;
    /** @var ContractorServiceRepository */
    private $contractorServiceRepository;
    /** @var ContractorTypeRepository  */
    private $contractorTypeRepository;
    /** @var ContractorType */
    private $myCompanyType;
    /** @var Contractor  */
    private $mainCompany;
    /** @var OpportunityManager */
    private $opportunityManager;
    /** @var ServiceRepository */
    private $serviceRepository;

    private $errorMessages = [];

    /**
     * ServiceCreatorService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->trashRequestCreator = $this->serviceContainer->get('app.trash_requests.creator.service');
        $this->serviceManager = $this->serviceContainer->get('app.service.manager');
        $this->departmentChannelManager = $this->serviceContainer->get('app.department_channel.manager');

        $this->deviceRepository = $this->objectManager->getRepository("AppBundle:Device");
        $this->serviceNamedRepository = $this->objectManager->getRepository("AppBundle:ServiceNamed");
        $this->contractorRepository = $this->objectManager->getRepository("AppBundle:Contractor");
        $this->contractorServiceRepository = $this->objectManager->getRepository("AppBundle:ContractorService");
        $this->contractorTypeRepository = $this->objectManager->getRepository("AppBundle:ContractorType");
        $this->myCompanyType = $this->contractorTypeRepository->findOneBy(['alias' => 'my_company']);
        $this->mainCompany = $this->contractorRepository->findOneBy(['type' => $this->myCompanyType]);
        $this->opportunityManager = $this->serviceContainer->get('app.opportunity.manager');
        $this->serviceRepository = $this->objectManager->getRepository("AppBundle:Service");
    }

    /**
     * @param array $params
     * @param $route
     * @return Service|bool
     */
    public function createInspectionByParams(array $params, $route)
    {
        $parsedData = $this->parseData($params);
        $this->checkingForCreate($parsedData);

        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]

            );

            return false;
        }

        $service = ServiceFactory::makeByData($parsedData);
        $this->serviceManager->checkAndCreateContractorService($service);
        $service->setDepartmentChannel($this->departmentChannelManager->getChannelByDivision($service));
        $contractorService = $this->contractorServiceRepository->findOneBy([
            'named' => $service->getNamed(),
            'contractor' => $this->mainCompany
        ]);
        $service->setContractorService($contractorService);
        $this->serviceManager->create($service);
        $this->serviceManager->attachOpportunity($service);

        return $service;
    }

    /**
     * @param $servicesName
     * @param Device $device
     */
    public function createByServicesName($servicesName, Device $device)
    {
        /** @var ServiceNamed $serviceName */
        foreach ($servicesName as $serviceName) {
            $service = $this->serviceManager->getDiscardAndResolvedService($device, $serviceName);
            if (empty($service)) {
                $service = $this->createNonFrequencyByServiceName($device, $serviceName);
            }
            $service->setInspectionDue(new \DateTime());
            $this->serviceManager->attachOpportunity($service, $this->opportunityManager::TYPE_REPAIR);
        }
    }

    /**
     * @param ServiceNamed $serviceNamed
     * @param Device $device
     * @return Service
     */
    public function createNonFrequencyByServiceName(Device $device, ServiceNamed $serviceNamed)
    {
        $newService = new Service();
        $newService->setNamed($serviceNamed);
        $newService->setDevice($device);
        $newService->setInspectionDue(new \DateTime());
        $newService->setAccount($device->getAccount());
        $newService->setDepartmentChannel($this->departmentChannelManager->getChannelByDivision($newService));

        $this->serviceManager->create($newService);

        return $newService;
    }

    /**
     * @param $servicesName
     * @param Device $device
     * @return null
     */
    public function checkingForCreateNonFrequency($servicesName, Device $device)
    {
        $result = null;
        /** @var ServiceNamed $serviceName */
        foreach ($servicesName as $serviceName) {
            /** @var Service $service */
            $service = $this->serviceRepository->findOneBy(["device" => $device, "named" => $serviceName]);

            if ($service instanceof Service) {
                $serviceStatusAlias = $service->getStatus()->getAlias();
                /** @var ServiceNamed $serviceNamed */
                $serviceNamed = $service->getNamed();
                $partErrorMessage = 'Repair Service is under ';
                $lastPartErrorMessage = 'you can not edit it( ' .
                    $serviceNamed->getName() . " - " . $serviceNamed->getDeficiency() . ' )';

                if ($serviceStatusAlias == $this->serviceManager::STATUS_REPAIR_OPPORTUNITY) {
                    $result['errorMessages'][] = $partErrorMessage . 'Opportunity, ' . $lastPartErrorMessage;
                } elseif ($serviceStatusAlias == $this->serviceManager::STATUS_UNDER_WORKORDER) {
                    $result['errorMessages'][] = $partErrorMessage . 'Workorder, '. $lastPartErrorMessage;
                } elseif ($serviceStatusAlias == $this->serviceManager::STATUS_UNDER_PROPOSAL) {
                    $result['errorMessages'][] = $partErrorMessage . 'Proposal, ' . $lastPartErrorMessage;
                } else {
                    $result['servicesName'][] = $serviceName;
                }
            } else {
                $result['servicesName'][] = $serviceName;
            }
        }

        $result = $this->prepareErrorMessageForCreateNonFrequency($result);

        return $result;
    }

    /**
     * @param $result
     * @return mixed
     */
    private function prepareErrorMessageForCreateNonFrequency($result)
    {
        if (isset($result['errorMessages'])) {
            $preparedError = null;
            foreach ($result['errorMessages'] as $message) {
                $preparedError .= $message . ";   ";
            }
            $result['errorMessages'] = $preparedError;
        }

        return $result;
    }

    /**
     * @param $data
     * @return array
     */
    private function parseData($data)
    {
        $parsedData = [];

        $parsedData["device"] = null;
        if (isset($data["deviceID"])) {
            /** Device $parsedData["device"] */
            $parsedData["device"] = $this->deviceRepository->find($data["deviceID"]);
        }

        $parsedData["account"] = null;
        if (!is_null($parsedData["device"])) {
            $parsedData["account"] = $parsedData["device"]->getAccount();
        }

        $parsedData["checkNamed"] = null;
        if (isset($data["serviceNameID"])) {
            $parsedData["checkNamed"] = $this->serviceNamedRepository->find($data["serviceNameID"]);
        }

        $parsedData["named"] = null;
        if (!is_null($parsedData["device"]) and !is_bool($parsedData["device"]) and isset($data["serviceNameID"])) {
            $parsedData["named"] = $this->serviceNamedRepository->getServiceNamedByIdAndDevice(
                $data["serviceNameID"],
                $parsedData["device"]->getNamed()
            );
        }

        $parsedData["inspectionDue"] = null;
        if (isset($data["inspectionDue"])) {
            try {
                $parsedData['inspectionDue'] = (new \DateTime())->setTimestamp($data['inspectionDue']);
            } catch (ContextErrorException $exception) {
                $parsedData['inspectionDue'] = null;
                $this->errorMessages[] = $exception->getMessage();
            }
        }

        $parsedData["lastTested"] = null;
        if (isset($data["lastTested"])) {
            try {
                $parsedData['lastTested'] = (new \DateTime())->setTimestamp($data['lastTested']);
            } catch (ContextErrorException $exception) {
                $parsedData['lastTested'] = null;
                $this->errorMessages[] = $exception->getMessage();
            }
        }

        $parsedData["companyLastTested"] = null;
        if (isset($data["lastTester"])) {
            $parsedData["companyLastTested"] = $this->contractorRepository->find($data["lastTester"]);
        }

        $parsedData["comment"] = null;
        if (isset($data["comment"])) {
            $parsedData["comment"] = $data["comment"];
        }

        return $parsedData;
    }

    /**
     * @param $parsedData
     */
    private function checkingForCreate($parsedData)
    {
        $this->errorMessages = [];

        if (is_null($parsedData['device'])) {
            $this->errorMessages[] = "Device not found";
        }

        if (is_null($parsedData['checkNamed'])) {
            $this->errorMessages[] = "There is no ServicePattern with such ID";
        }

        if (is_null($parsedData['named'])) {
            $this->errorMessages[] = "This ServicePattern can not be used for current device";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Service can not be added</b>.<br>"], $this->errorMessages);
        }
    }
}
