<?php

namespace ServiceBundle\Services;

use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderStatus;
use AppBundle\Services\TrashRequests\Creator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ServiceUpdater
{
    /** @var ObjectManager  */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var ContractorRepository */
    private $contractorRepository;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var Creator  */
    private $trashRequestCreator;
    private $errorMessages;
    /**
     * ServiceUpdater constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;


        $this->serviceManager = $this->serviceContainer->get("app.service.manager");
        $this->trashRequestCreator = $this->serviceContainer->get("app.trash_requests.creator.service");

        $this->serviceRepository = $this->objectManager->getRepository("AppBundle:Service");
        $this->deviceRepository = $this->objectManager->getRepository("AppBundle:Device");
        $this->serviceNamedRepository = $this->objectManager->getRepository("AppBundle:ServiceNamed");
        $this->contractorRepository = $this->objectManager->getRepository("AppBundle:Contractor");
    }

    /**
     * @param $params
     * @param $route
     * @return Service|bool
     *
     */
    public function updateByParams($params, $route)
    {
        $parsedData = $this->parseDataForUpdate($params);
        $this->checkingForUpdate($parsedData);

        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]
            );
            return false;
        }

        return $this->updateService($parsedData);
    }

    /**
     * @param $params
     * @param $route
     * @return Service|bool
     */
    public function updateRepairServiceByParams($params, $route)
    {
        $parsedData = $this->parseDataForUpdate($params);
        $this->checkingForUpdateRepairService($parsedData);

        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]
            );
            return reset($this->errorMessages);
        }

        return $this->updateService($parsedData, 'repair');
    }

    /**
     * @param Service $service
     * @param $comment
     */
    public function updateRepairServiceComment(Service $service, $comment)
    {
        $service->setComment($comment);
        $this->serviceManager->update($service);
    }


    /**
     * @param $parseData
     * @param string $opportunityType
     * @return Service
     */
    private function updateService($parseData, $opportunityType = 'retest')
    {
        /** @var Service $serviceForUpdate */
        $serviceForUpdate = $parseData["service"];
        if (!is_null($parseData["device"])) {
            $serviceForUpdate->setDevice($parseData["device"]);
        }
        if (!is_null($parseData["inspectionDue"])) {
            $serviceForUpdate->setInspectionDue($parseData["inspectionDue"]);
        }
        if (!is_null($parseData["companyLastTested"])) {
            $serviceForUpdate->setCompanyLastTested($parseData["companyLastTested"]);
        }
        if (!is_null($parseData["lastTested"])) {
            $serviceForUpdate->setLastTested($parseData["lastTested"]);
        }
        if (!is_null($parseData["comment"])) {
            $serviceForUpdate->setComment($parseData["comment"]);
        }

        $this->serviceManager->checkAndCreateContractorService($serviceForUpdate);

        $saveStatus = $serviceForUpdate->getStatus();

        if ($saveStatus->getAlias() != $this->serviceManager::STATUS_OMITTED) {
            if (!empty($serviceForUpdate->getOpportunity())) {
                $this->serviceManager->resetOpportunity($serviceForUpdate);
            }

            $this->serviceManager->attachOpportunity($serviceForUpdate, $opportunityType);
        }

        $this->serviceManager->update($serviceForUpdate);


        return $serviceForUpdate;
    }

    /**
     * @param $data
     * @return array
     */
    private function parseDataForUpdate($data)
    {
        $parsedData = [];

        $parsedData["service"] = null;
        if (isset($data["serviceID"])) {
            $parsedData["service"] = $this->serviceRepository->find($data["serviceID"]);
        }

        $parsedData["device"] = null;
        if (isset($data["deviceID"])) {
            $parsedData["deviceID"] = $data["deviceID"];
            $parsedData["device"] = $this->deviceRepository->find($data["deviceID"]);
        }

        $parsedData["inspectionDue"] = null;
        if (isset($data["inspectionDue"])) {
            $inspectionDue = new \DateTime();
            $inspectionDue->setTimestamp($data["inspectionDue"]);
            $parsedData["inspectionDue"] = $inspectionDue;
        }

        $parsedData["companyLastTested"] = null;
        if (isset($data["lastTester"])) {
            $parsedData["companyLastTested"] = $this->contractorRepository->find($data["lastTester"]);
        }

        $parsedData["lastTested"] = null;
        if (isset($data["lastTested"])) {
            $lastTested = new \DateTime();
            $lastTested->setTimestamp($data["lastTested"]);
            $parsedData["lastTested"] = $lastTested;
        }

        $parsedData["comment"] = null;
        if (isset($data["comment"])) {
            $parsedData["comment"] = $data["comment"];
        }

        return $parsedData;
    }

    /**
     * @param $parsedData
     */
    private function checkingForUpdate($parsedData)
    {
        $this->errorMessages = [];

        $this->checkService($parsedData["service"]);
        $this->checkDevice($parsedData);

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Service can not be edited</b>.<br>"], $this->errorMessages);
        }
    }

    /**
     * @param $parsedData
     */
    private function checkingForUpdateRepairService($parsedData)
    {
        $this->errorMessages = [];

        $this->checkRepairService($parsedData["service"]);
        $this->checkDevice($parsedData, true);
    }

    /**
     * @param Service|null $service
     */
    private function checkService(?Service $service)
    {
        if (is_null($service)) {
            $this->errorMessages[] = "There is no Inspection with such ID";
        } else {
            if (!$this->checkIfEditableInspection($service)) {
                $this->errorMessages[] = "You can't edit this Inspection";
            }

            if ($service->getDeleted()) {
                $this->errorMessages[] = "Inspection is deleted";
            }
        }
    }

    /**
     * @param Service|null $service
     */
    private function checkRepairService(?Service $service)
    {
        if (is_null($service)) {
            $this->errorMessages[] =
                "<b>Non-frequency service can not be edited</b>.<br> There is no Repair Service with such ID";
        } else {
            $this->checkIfEditableRepairService($service);
        }
    }

    /**
     * @param $parsedData
     */
    private function checkDevice($parsedData, $canNotBeEmpty = false)
    {
        if (isset($parsedData["deviceID"]) and is_null($parsedData["device"])) {
            $this->errorMessages[] = "There is no Device with such ID";
        } elseif (isset($parsedData["deviceID"])
            and !is_null($parsedData["device"]) and !is_null($parsedData["service"])) {
            $serviceNamed = $this->serviceNamedRepository->getServiceNamedByIdAndDevice(
                $parsedData["service"]->getNamed()->getId(),
                $parsedData["device"]->getNamed()
            );

            if (!$serviceNamed) {
                $this->errorMessages[] = "You can't add this service to this device";
            }
        }

        if ($canNotBeEmpty and is_null($parsedData["device"])) {
            $this->errorMessages[] = "There is no Device with such ID";
        } elseif ($canNotBeEmpty and !is_null($parsedData["device"]) and !is_null($parsedData["service"])) {
            /** @var Device $serviceDevice */
            $serviceDevice = $parsedData["service"]->getDevice();

            if ($parsedData["device"]->getId() != $serviceDevice->getId()) {
                $this->errorMessages[] = "No such service under this Device";
            }
        }
    }

    /**
     * @param Service $inspection
     * @return bool
     */
    private function checkIfEditableInspection(Service $inspection)
    {
        if (($inspection->getProposal()
                and ($inspection->getStatus()->getAlias() === $this->serviceManager::STATUS_UNDER_RETEST_NOTICE
                    or $this->serviceManager::STATUS_UNDER_PROPOSAL)) or
            ($inspection->getWorkorder()
                and $inspection->getStatus()->getAlias() === $this->serviceManager::STATUS_UNDER_WORKORDER)
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param Service $repairService
     */
    private function checkIfEditableRepairService(Service $repairService)
    {
        $serviceStatusAlias = $repairService->getStatus()->getAlias();

        if ($serviceStatusAlias == $this->serviceManager::STATUS_DELETED) {
            $this->errorMessages[] =
                "<b>Non-frequency service can not be edited</b>.<br> This Repair Service is deleted";
        }

        if ($serviceStatusAlias == $this->serviceManager::STATUS_DISCARDED) {
            $this->errorMessages[] =
                "<b>Non-frequency service can not be edited</b>.<br> This Repair Service is discarded";
        }

        if ($serviceStatusAlias == $this->serviceManager::STATUS_RESOLVED) {
            $this->errorMessages[] =
                "<b>Non-frequency service can not be edited</b>.<br> This Repair Service is resolved";
        }

        if ($serviceStatusAlias == $this->serviceManager::STATUS_UNDER_WORKORDER) {
            $this->errorMessages[] =
                "<b>Non-frequency service can not be edited</b>.<br> This Repair Service under workorder";
        }
        if ($serviceStatusAlias == $this->serviceManager::STATUS_UNDER_PROPOSAL) {
            $this->errorMessages[] =
                "<b>Non-frequency service can not be edited</b>.<br> This Repair Service under proposal";
        }
    }
}
