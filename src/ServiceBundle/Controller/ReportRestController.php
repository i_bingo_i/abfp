<?php

namespace ServiceBundle\Controller;

use ApiBundle\Services\ServiceRestService;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Report;
use AppBundle\Entity\Service;
use ServiceBundle\Factories\ReportFactory;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Persistence\ObjectManager;
use ServiceBundle\Services\ReportCreator;
use ServiceBundle\Services\ReportFileAttacher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReportRestController extends FOSRestController
{
    /**
     * Create Post fail
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "updatedAt": (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Post fail",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Create Post fail",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'inspectionID OR comment' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="inspectionID", nullable=false, strict=true, description="Inspection ID")
     * @RequestParam(name="comment", nullable=false,  strict=true, description="Text of comment")
     * @RequestParam(name="alarmID", nullable=true,  strict=true, description="Alarm ID")
     *
     * @Rest\Post("/post-fail")
     * @return View
     */
    public function postCreatePostFailAction(ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        return new View(
            [
                'id' => (string) rand(1, 10),
                'updatedAt' => new \DateTime()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Create report for service
     *
     * ### Response OK ###
     *     {
     *          "id": (string) "2",
     *          "updatedAt": (int) 156676766
     *     }
     *
     * ### Response FAIL ###
     *     {
     *          "statusCode": (string) "601",
     *          "message": (string) "Report has not created"
     *     }
     *
     * @ApiDoc(
     *   section = "Report",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Create report for service",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="serviceID", "dataType"="integer", "required"=true, "description"="Service id"},
     *       {"name"="statusAlias", "dataType"="string", "required"=true, "description"="Service report status alias"},
     *       {"name"="finishTime", "dataType"="string", "required"=true, "description"="Finish service time"},
     *       {"name"="needRepair", "dataType"="boolean", "required"=false, "description"="Is need repair"},
     *       {"name"="printTemplate", "dataType"="string", "required"=false, "description"="Info for print"},
     *       {"name"="userID", "dataType"="integer", "required"=true, "description"="Service report status alias"},
     *       {"name"="textReport", "dataType"="text", "required"=false, "description"="Text report"},
     *       {"name"="htmlReport", "dataType"="text", "required"=false, "description"="Html report"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Post("/service/report/add")
     * @return View
     */
    public function postServiceReportAction(Request $request)
    {
        /** @var ReportCreator $reportCreatorService */
        $reportCreatorService = $this->get("service.report_creator.service");

        $params = $request->request->all();
        $route = $request->getRequestUri();
        $params["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");

        /** @var Report $report */
        $report = $reportCreatorService->createByParams($params, $route);

        if (!$report instanceof Report) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Report has not created'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$report->getId(),
                'updatedAt' => $report->getDateCreate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }


    /**
     * Add pdf file to report for service
     *
     * ### Response OK ###
     *     {
     *          "id": (string) "12",
     *          "updatedAt": (int) 154356783,
     *          "pathFile": (string) "/uploads/reports/845485454859993.pdf"
     *     }
     *
     * @ApiDoc(
     *   section = "Report",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Add pdf file to report for service",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="reportID", "dataType"="string", "required"=true, "description"="Report id"},
     *       {"name"="reportFile", "dataType"="file", "required"=true, "description"="Report file"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     *
     * @param Request $request
     *
     * @Rest\Post("/service/report/upload")
     * @return View
     */
    public function postServiceReportFileAction(Request $request)
    {
        /** @var ReportFileAttacher $reportCreatorService */
        $reportFileAttachService = $this->get("service.report_file_attach.service");

        $reportId = $request->request->get("reportID");
        $route = $request->getRequestUri();
        $file = $request->files->get("reportFile");
        $accessToken = $request->headers->get("X-ACCESS-TOKEN");

        $report = $reportFileAttachService->attachById($reportId, $file, $route, $accessToken);

        if (!$report instanceof Report) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Report file can not be uploaded'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$report->getId(),
                'pathFile' => $report->getFileReport(),
                'updatedAt' => $report->getDateCreate()->getTimestamp()
            ],
            Response::HTTP_OK
        );

    }
}
