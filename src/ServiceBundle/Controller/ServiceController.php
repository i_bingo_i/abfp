<?php

namespace ServiceBundle\Controller;

use AppBundle\Entity\Account;
use AdminBundle\Form\ServiceNameType;
use AdminBundle\Form\ServiceType;
use AdminBundle\Form\InspectionsInformationType;
use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorService;
use AppBundle\Entity\ContractorType;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\OpportunityManager;
use AppBundle\Entity\EntityManager\RepairDeviceInfoManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Message;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Report;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Repository\ServiceHistoryRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use AppBundle\Security\ServiceVoter;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Services\Account\Authorizer;
use AppBundle\Services\Coordinates\Helper;
use AppBundle\Services\PreviousPageService;
use ServiceBundle\Services\ServiceCreatorService;
use ServiceBundle\Services\ServiceUpdater;
use WorkorderBundle\Services\Creator;
use WorkorderBundle\Services\IncludeService;
use Doctrine\Common\Persistence\ObjectManager;
use ServiceBundle\Services\ServiceHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ContractorTypeRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\Serializer;
use AppBundle\Services\Proposal\ProposalService;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\RepairDeviceInfo as RepairDeviceInfoRepository;
use AppBundle\Security\WorkOrderVoter;
use AppBundle\Security\ErrorByAccountVoter;

class ServiceController extends Controller
{
    /**
     * @param Device $device
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Device $device, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->get('app.department_channel.manager');
        /** @var ContractorRepository $contractorRepository */
        $contractorRepository = $objectManager->getRepository('AppBundle:Contractor');
        /** @var ContractorTypeRepository $contractorTypeRepository */
        $contractorTypeRepository = $objectManager->getRepository('AppBundle:ContractorType');
        /** @var ContractorType $myCompanyType */
        $myCompanyType = $contractorTypeRepository->findOneBy(['alias' => 'my_company']);
        /** @var Contractor $mainCompany */
        $mainCompany = $contractorRepository->findOneBy(['type' => $myCompanyType]);
        /** @var Account $account */
        $account = $device->getAccount();
        /** @var Municipality $municipality */
        $municipality = $account->getMunicipality();
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $device->getNamed()->getCategory();
        /** @var Service $newService */
        $newService = new Service();
        $newService->setDevice($deviceManager->serialize($device));

        $form = $this->createForm(ServiceType::class, $newService, ['validation_groups' => ['create']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceManager->checkAndCreateContractorService($newService);
            $newService->setAccount($account);
            $newService->setDepartmentChannel(
                $departmentChannelManager->getDefaultChannel($municipality, $deviceCategory)
            );

            /** @var ContractorServiceRepository $contractorServiceRepository */
            $contractorServiceRepository = $objectManager->getRepository('AppBundle:ContractorService');
            /** @var ContractorService $contractorService */
            $contractorService = $contractorServiceRepository->findOneBy([
                'named' => $newService->getNamed(),
                'contractor' => $mainCompany
            ]);
            $newService->setContractorService($contractorService);

            $serviceManager->create($newService);

            // TODO It is string will need be on event
            $serviceManager->attachOpportunity($newService);

            return $this->redirectToRoute('admin_device_view', [
                'device' => $device->getId()
            ]);
        }

        return $this->render('@Admin/Service/create.html.twig', [
            'form' => $form->createView(),
            'mainCompany' => $mainCompany
        ]);
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteAction(Service $service, Request $request)
    {
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var string $previousPage */
        $previousPage = $request->headers->get('referer');

        if (($service->getProposal()
                && $service->getStatus()->getAlias() === $serviceManager::STATUS_UNDER_RETEST_NOTICE) ||
            ($service->getWorkorder()
                && $service->getStatus()->getAlias() === $serviceManager::STATUS_UNDER_WORKORDER)
        ) {
            return $this->redirect($previousPage);
        }

        $serviceManager->delete($service);
        // TODO It is string will need be on event
        if (!empty($service->getOpportunity())) {
            $serviceManager->resetOpportunity($service);
        }

        if (!is_null($service->getDevice())) {
            return $this->redirect($previousPage);
        }

        return $this->redirectToRoute('admin_dashboard');
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updateAction(Service $service, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ContractorRepository $contractorRepository */
        $contractorRepository = $objectManager->getRepository('AppBundle:Contractor');
        /** @var ContractorTypeRepository $contractorTypeRepository */
        $contractorTypeRepository = $objectManager->getRepository('AppBundle:ContractorType');
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var Session $session */
        $session = $this->get('session');
        // added case when no session previos page and no referer and add previous page - service's device

        /** @var PreviousPageService $previousPageService */
        $previousPageService = $this->get("app.previous_page.service");

        $previousPage = $previousPageService->getForInspectionByRequestAndDevice($request, $service->getDevice());
        
        if (($service->getProposal()
                && $service->getStatus()->getAlias() === $serviceManager::STATUS_UNDER_RETEST_NOTICE) ||
            ($service->getWorkorder()
                && $service->getStatus()->getAlias() === $serviceManager::STATUS_UNDER_WORKORDER)
        ) {
            return $this->redirect($previousPage);
        }

        /** @var ContractorType $myCompanyType */
        $myCompanyType = $contractorTypeRepository->findOneBy(['alias' => 'my_company']);

        $service->setDevice($deviceManager->serialize($service->getDevice()));
        $formUpdateService = $this->createForm(ServiceType::class, $service, ['validation_groups' => ['create']]);

        $formUpdateService->handleRequest($request);

        if ($formUpdateService->isSubmitted() && $formUpdateService->isValid()) {
            $serviceManager->checkAndCreateContractorService($service);
            $serviceManager->update($service);

            // TODO It is strings will need be on event
            if (!empty($service->getOpportunity())) {
                $serviceManager->resetOpportunity($service);
            }
            $serviceManager->attachOpportunity($service);

            return $this->redirect($previousPage);
        }

        return $this->render('@Admin/Service/update.html.twig', [
            'form' => $formUpdateService->createView(),
            'mainCompany' => $contractorRepository->findOneBy(['type' => $myCompanyType]),
            'prevPage' => $previousPage
        ]);
    }

    /**
     * @param Service $service
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function resetServiceToNextTimeAction(Service $service, Proposal $proposal, Request $request)
    {
        if (!$this->isGranted(ServiceVoter::CAN_NOT_REMOVE_SERVICE_FROM_PROPOSALS, $service)) {
            return $this->redirect($request->headers->get('referer'));
        }
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var ContractorRepository $contractorRepository */
        $contractorRepository = $objectManager->getRepository('AppBundle:Contractor');
        /** @var ContractorTypeRepository $contractorTypeRepository */
        $contractorTypeRepository = $objectManager->getRepository('AppBundle:ContractorType');
        /** @var ProposalService $proposalService */
        $proposalService = $this->get('app.proposal_service.service');
        /** @var \AppBundle\Services\RepairDeviceInfo\Creator $repairDeviceInfoCreatorService */
        $repairDeviceInfoCreatorService = $this->get('app.device_info_creator.service');
        $proposalTypeAlias = $proposal->getType()->getAlias();

        $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";
        $previousPage = $this->generateUrl("$proposalRoute", ['proposal' => $proposal->getId()], true);

        $service = $serviceManager->unlinkServiceFromProposalAndResetStatus($service);
        $service = $serviceManager->resetServiceToNextTime($service);
        /** @var ContractorType $myCompanyType */
        $myCompanyType = $contractorTypeRepository->findOneBy(['alias' => 'my_company']);
        /** @var Device $device */
        $device = $service->getDevice();

        $formUpdateService = $this->createForm(ServiceType::class, $service, ['validation_groups' => ['create']]);
        $formUpdateService->handleRequest($request);

        if ($formUpdateService->isSubmitted() && $formUpdateService->isValid()) {
            $serviceManager->checkAndCreateContractorService($service);
            $serviceManager->update($service);

            // TODO It is strings will need be on event
            if (!empty($service->getOpportunity())) {
                $serviceManager->resetOpportunity($service);
            }
            $serviceManager->attachOpportunity($service);

            $repairDeviceInfoCreatorService->checkEventsOnChangeInfo($device, $proposal);

            $proposal = $proposalService->setMinEarliestDueDate($proposal);
            $proposalService->calculateAndSetAllProposalTotals($proposal);

            return $this->redirect($previousPage);
        }

        return $this->render('@Admin/Service/reset_next_time.html.twig', [
            'form' => $formUpdateService->createView(),
            'mainCompany' => $contractorRepository->findOneBy(['type' => $myCompanyType]),
            'prevPage' => $previousPage
        ]);
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function skipAction(Service $service, Request $request)
    {
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        $serviceManager->attachOpportunity($service);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Device $device
     * @return Response
     */
    public function getServiceHistoryWithWorkorderByDeviceAction(Device $device)
    {
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');

        $serviceHistoryWithWorkorder = $serviceManager->getServiceHistoryWithWorkorderByDevice($device);

        return $this->render(
            'AdminBundle:Service:get_history_with_workorder_by_device.html.twig',
            [
                'serviceHistoryWithWorkorder' => $serviceHistoryWithWorkorder,
            ]
        );
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function omitAction(Service $service, Request $request)
    {
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        $serviceManager->omit($service);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function discardAction(Service $service, Request $request)
    {
        if (!$this->isGranted(ServiceVoter::OPPORTUNITY_TO_DISCARD_REPAIR_SERVICE, $service)) {
            return $this->redirect($request->headers->get('referer'));
        }
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        $serviceManager->discard($service);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function restoreAction(Service $service, Request $request)
    {
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        $serviceManager->restore($service);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Device $device
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addNonFrequencyAction(Device $device, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $objectManager->getRepository('AppBundle:Service');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var ServiceCreatorService $serviceCreator */
        $serviceCreator = $this->get('service.creator.service');
        /** @var Session $session */
        $session = $this->get('session');

        $form = $this->createForm(
            ServiceNameType::class,
            [],
            [
                'validation_groups' => ['create'],
                'device' => $device
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $servicesName = $form['name']->getData();
            if ($servicesName) {
                $checkingResult = $serviceCreator->checkingForCreateNonFrequency($servicesName, $device);

                if (isset($checkingResult['servicesName'])) {
                    $serviceCreator->createByServicesName($checkingResult['servicesName'], $device);
                }

                if (isset($checkingResult['errorMessages'])) {
                    $session->getFlashBag()->add(
                        "failed",
                        $checkingResult['errorMessages']
                    );
                }
            }

            return $this->redirectToRoute('admin_device_view', ['device' => $device->getId()]);
        }

        $isSummit = $request->request->get('isSubmit');
        if ($isSummit) {
            $session->getFlashBag()->add(
                "failed",
                'Please add at least one Repair Service'
            );
        }

        $serviceNames = $serializer->serialize(
            $serviceManager->getNonFrequencyServiceNames($device),
            'json'
        );

        return $this->render('@Admin/Service/add_non_frequency.html.twig', [
            'device' => $deviceManager->serialize($device),
            'deviceNonFrequencyServices' => $serviceRepository->getNoFrequencyByDevice($device),
            'serviceNames' => $serviceNames,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @param Device $device
     * @param Request $request
     * @return Response
     */
    public function listByHistoryOfDeviceAction(Device $device, Request $request)
    {

        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ServiceHistoryRepository $serviceHistoryRepository */
        $serviceHistoryRepository = $objectManager->getRepository('AppBundle:ServiceHistory');
        /** @var Session $session */
        $session = $this->get('session');
        if ($session->has('previousPage')) {
            $previousPage = $session->get('previousPage');
        } elseif ($request->headers->has('referer')) {
            $previousPage = $request->headers->get('referer');
            $session->set('previousPage', $previousPage);
        } else {
            $previousPage = $this->generateUrl('admin_device_view', ["device" => $device->getId()], true);
            $session->set('previousPage', $previousPage);
        }

        $inspection = $serviceHistoryRepository->getDeviceServicesHistories($device);


        return $this->render('@Admin/Service/list_by_history_of_device.html.twig', [
            'inspections' => $inspection,
            'device' => $device,
            'previousPage' => $previousPage
        ]);
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteRepairServiceFromProposalAction(Service $service, Request $request)
    {
        if (!$this->isGranted(ServiceVoter::CAN_NOT_REMOVE_SERVICE_FROM_PROPOSALS, $service)) {
            return $this->redirect($request->headers->get('referer'));
        }
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalService $proposalService */
        $proposalService = $this->get('app.proposal_service.service');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $objectManager->getRepository('AppBundle:Service');
        /** @var RepairDeviceInfoManager $repairDeviceInfoManager */
        $repairDeviceInfoManager = $this->get('app.repair_device_info.manager');
        /** @var \AppBundle\Services\RepairDeviceInfo\Creator $repairDeviceInfoCreatorService */
        $repairDeviceInfoCreatorService = $this->get('app.device_info_creator.service');
        /** @var RepairDeviceInfoRepository $repairDeviceInfoRepository */
        $repairDeviceInfoRepository = $objectManager->getRepository('AppBundle:RepairDeviceInfo');
        /** @var Device $device */
        $device = $service->getDevice();
        $proposal = $proposalService->deleteServiceFromProposal($service);
        /** @var \AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo */
        $repairDeviceInfo = $repairDeviceInfoRepository->findOneBy(
            ['device'=>$device, 'proposal'=>$proposal]
        );
        $checkIfGlueRepairDeviceInfo = $repairDeviceInfo->getIsGlued();

        if (empty(
            $serviceRepository->findBy(['proposal'=>$proposal, 'device'=>$device, 'deleted'=>false])
        )
        ) {
            $repairDeviceInfoManager->delete($proposal, $device);
        }

        if ($checkIfGlueRepairDeviceInfo) {
            $repairDeviceInfoCreatorService->reCalculateGlueAfterDeletedLatestRepairServices($repairDeviceInfo);
        }

        $proposalService->calculateAndSetAllProposalTotals($proposal);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteServiceFromRetestNoticeAction(Service $service, Request $request)
    {
        if (!$this->isGranted(ServiceVoter::CAN_NOT_REMOVE_SERVICE_FROM_PROPOSALS, $service)) {
            return $this->redirect($request->headers->get('referer'));
        }
        /** @var ProposalService $proposalService */
        $proposalService = $this->get('app.proposal_service.service');
        $repairDeviceInfoCreatorService = $this->get('app.device_info_creator.service');
        /** @var Device $device */
        $device = $service->getDevice();

        $proposal = $proposalService->deleteServiceFromProposal($service);
        $proposal = $proposalService->setMinEarliestDueDate($proposal);

        $repairDeviceInfoCreatorService->checkEventsOnChangeInfo($device, $proposal);

        $proposalService->calculateAndSetAllProposalTotals($proposal);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function changeInspectionsInformationAction(Account $account, Request $request)
    {
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');

        $defaultData['named'] = $serviceManager->getAccountInspectionsNamed($account);
        $form = $this->createForm(InspectionsInformationType::class, $defaultData);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceManager->changeInspectionsInformation($account, $form->getData());

            return $this->redirectToRoute('admin_account_get', ['accountId' => $account->getId()]);
        }

        return $this->render('@Admin/Service/change_inspections_information.html.twig', [
            'form' => $form->createView(),
            'account' => $account
        ]);
    }

    /**
     * @param Report $report
     * @return Response
     */
    public function textReportInformationAction(Report $report)
    {
        return $this->render('@Admin/Service/text_report_information.html.twig', [
            'report' => $report
        ]);
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function addInspectionToWorkorderAction(Service $service, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var IncludeService $woIncludeService */
        $woIncludeService = $this->get("workorder.include_service.service");
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository("AppBundle:Workorder");
        /** @var Creator $workorderCreatorService */
        $workorderCreatorService = $this->get('workorder.creator.service');
        /** @var RepairDeviceInfoManager $repairDeviceInfoManager */
        $repairDeviceInfoManager = $this->get('app.repair_device_info.manager');
        /** @var \AppBundle\Services\RepairDeviceInfo\Creator $repairDeviceInfoCreatorService */
        $repairDeviceInfoCreatorService = $this->get('app.device_info_creator.service');
        /** @var PreviousPageService $previousPageService */
        $previousPageService = $this->get('app.previous_page.service');
        /** @var Authorizer $accountAuthorizerService */
        $accountAuthorizerService = $this->get('app.account_authorizer.service');
        /** @var Helper $coordinateHelper */
        $coordinateHelper = $this->get('app.coordinates_helper.service');
        /** @var Device $device */
        $device = $service->getDevice();
        /** @var Account $account */
        $account = $device->getAccount();
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $params['division'] = $device->getNamed()->getCategory();
        /** @var Account $ownerAccount */
        $ownerAccount = $params['ownerAccount'] = $accountAuthorizerService->getAccountWithAuthorizerForDivision(
            $account,
            $deviceCategory
        );
        $params['service'] = $service;
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        /** @var ServiceHelper $serviceHelper */
        $serviceHelper = $this->get("service.service_helper.service");
        // added case when no session previos page and no referer and add previous page - service's device
        $previousPage = $previousPageService->getForInspectionByRequestAndDevice($request, $device);
        $workorderId = $request->get('workorder')['workorderId'];

        if (!$this->isGranted(ErrorByAccountVoter::ADD_INSPECTION_TO_WO, $params)) {
            return $this->redirect($request->headers->get('referer'));
        }

        if (!$this->isGranted(ServiceVoter::CAN_NOT_ADD_INSPECTION_TO_WO, $service)) {
            return $this->redirect($request->headers->get('referer'));
        }

        if ($workorderId and $workorderId != 0) {
            /** @var Workorder $workorder */
            $workorder = $workorderRepository->find($workorderId);
        } else {
            /** @var Workorder $workorder */
            $workorder = $workorderCreatorService->create($ownerAccount, $deviceCategory, $user);
        }

        if (!$this->isGranted(WorkOrderVoter::CAN_NOT_ADD_INSPECTION_TO_FROZEN_WO, $workorder)) {
            return $this->redirect($request->headers->get('referer'));
        }

        $servicesArray[] = $service;
        // we must recreate the history of the service to update departmentChannel price
        $serviceHelper->updateServicesAndHistory($servicesArray);

        /** @var \AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo */
        $repairDeviceInfo =
            $repairDeviceInfoCreatorService->createInfoForInspectionsWithoutProposal($device, $servicesArray);
        /** @var Workorder $workorder */
        $workorder = $woIncludeService->addServices($device, $servicesArray, $repairDeviceInfo, $workorder);

        //set coordinates by Account Address
        $coordinateHelper->refreshByAccount($workorder->getAccount());
        //creating logs for inspection
        $woIncludeService->creatingInspectionLogs($workorder, $service);

        //remove RepairDeviceInfo after added to WORepairInfo
        $repairDeviceInfoManager->remove($repairDeviceInfo);

        return $this->redirect($previousPage);
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function editRepairServiceCommentAction(Service $service, Request $request)
    {
        if (!$this->isGranted(ServiceVoter::OPPORTUNITY_TO_CHANGE_REPAIR_SERVICE_COMMENT, $service)) {
            return $this->redirect($request->headers->get('referer'));
        }
        /** @var ServiceUpdater $serviceUpdater */
        $serviceUpdater = $this->get('service.updater.service');
        $comment = $request->request->get('comment');

        $serviceUpdater->updateRepairServiceComment($service, $comment);

        return $this->redirect($request->headers->get('referer'));
    }
}
