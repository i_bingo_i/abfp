<?php

namespace ServiceBundle\Controller;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorService;
use AppBundle\Entity\EntityManager\ContractorServiceManager;
use AppBundle\Entity\EntityManager\ServiceNamedManager;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\State;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\InspectionServiceNamedWithContractor;
use AdminBundle\Form\RepairServiceNamedWithContractor;
use Symfony\Component\HttpFoundation\Session\Session;

class ServiceNamedController extends Controller
{
    /**
     * @param Contractor $contractor
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createRepairAction(Contractor $contractor, Request $request)
    {
        /** @var ServiceNamedManager $serviceNamedManager */
        $serviceNamedManager = $this->get('app.service_named.manager');
        /** @var ContractorServiceManager $contractorServiceManager */
        $contractorServiceManager = $this->get('app.contractor_service.manager');

        $defaultData = array('contractor' => $contractor);

        $form = $this->createForm(
            RepairServiceNamedWithContractor::class,
            $defaultData,
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form->getData();

            /** @var ServiceNamed $serviceNamed */
            $serviceNamed = new ServiceNamed();
            $serviceNamed = $serviceNamedManager->createRepairService($serviceNamed, $data);

            if ($serviceNamed instanceof ServiceNamed) {
                /** @var ContractorService $contractorService */
                $contractorService = new ContractorService();
                $contractorServiceManager->createForRepairService($contractorService, $serviceNamed, $data);

                $selectedContractorId = $data['contractor'];
                return $this->redirectToRoute('admin_contractor_view', [
                    'contractor' => $selectedContractorId
                ]);
            } else {
                /** @var Session $session */
                $session = $this->get('session');
                $session->getFlashBag()->add(
                    "failed",
                    'For this device repair service is already exists'
                );
            }
        }
        return $this->render('@Admin/ServiceNamed/create_repair.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createInspectionAction(Request $request)
    {
        /** @var ServiceNamedManager $serviceNamedManager */
        $serviceNamedManager = $this->get('app.service_named.manager');
        /** @var ContractorServiceManager $contractorServiceManager */
        $contractorServiceManager = $this->get('app.contractor_service.manager');

        /** @var ServiceNamed $serviceNamed */
        $serviceNamed = new ServiceNamed();
        /** @var ContractorService $contractorService */
        $contractorService = $contractorServiceManager->prepareCreateForInspectionService();

        $form = $this->createForm(
            InspectionServiceNamedWithContractor::class,
            [
                'serviceNamed' => $serviceNamed,
                'contractorService' => $contractorService
            ],
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $serviceNamed = $serviceNamedManager->createInspectionService($serviceNamed, $data['deviceNamed']);

            if ($serviceNamed instanceof ServiceNamed) {
                $contractorServiceManager->createForInspectionService(
                    $contractorService,
                    $serviceNamed,
                    $data['deviceNamed']
                );

                return $this->redirectToRoute('admin_contractor_view', [
                    'contractor' => $contractorService->getContractor()->getId()
                ]);
            }

            /** @var Session $session */
            $session = $this->get('session');
            $session->getFlashBag()->add(
                "failed",
                'For this device frequency service is already exists'
            );
        }
        return $this->render('@Admin/ServiceNamed/create_inspection.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
