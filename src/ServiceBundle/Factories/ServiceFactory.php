<?php

namespace ServiceBundle\Factories;

use AppBundle\Entity\Service;

class ServiceFactory
{
    /**
     * @param $parsedData
     * @return Service
     */
    public static function makeByData($parsedData)
    {
        $service = new Service();
        if (isset($parsedData['named'])) {
            $service->setNamed($parsedData['named']);
        }

        if (isset($parsedData['fixedPrice'])) {
            $service->setFixedPrice($parsedData['fixedPrice']);
        }

        if (isset($parsedData['hourlyPrice'])) {
            $service->setHourlyPrice($parsedData['hourlyPrice']);
        }

        if (isset($parsedData['lastTested'])) {
            $service->setLastTested($parsedData['lastTested']);
        }

        if (isset($parsedData['inspectionDue'])) {
            $service->setInspectionDue($parsedData['inspectionDue']);
        }

        if (isset($parsedData['comment'])) {
            $service->setComment($parsedData['comment']);
        }

        if (isset($parsedData['testDate'])) {
            $service->setTestDate($parsedData['testDate']);
        }

        if (isset($parsedData['directlyWO'])) {
            $service->setDirectlyWO($parsedData['directlyWO']);
        }

        if (isset($parsedData['addedFromIos'])) {
            $service->setAddedFromIos($parsedData['addedFromIos']);
        }

        if (isset($parsedData['device'])) {
            $service->setDevice($parsedData['device']);
        }

        if (isset($parsedData['account'])) {
            $service->setAccount($parsedData['account']);
        }

        if (isset($parsedData['companyLastTested'])) {
            $service->setCompanyLastTested($parsedData['companyLastTested']);
        }

        if (isset($parsedData['opportunity'])) {
            $service->setOpportunity($parsedData['opportunity']);
        }

        if (isset($parsedData['departmentChannel'])) {
            $service->setDepartmentChannel($parsedData['departmentChannel']);
        }

        if (isset($parsedData['workorder'])) {
            $service->setWorkorder($parsedData['workorder']);
        }

        if (isset($parsedData['proposal'])) {
            $service->setProposal($parsedData['proposal']);
        }

        if (isset($parsedData['status'])) {
            $service->setStatus($parsedData['status']);
        }

        if (isset($parsedData['lastReport'])) {
            $service->setLastReport($parsedData['lastReport']);
        }

        if (isset($parsedData['contractorService'])) {
            $service->setContractorService($parsedData['contractorService']);
        }

        if (isset($parsedData['lastTester'])) {
            $service->setLastTester($parsedData['lastTester']);
        }

        return $service;
    }
}
