<?php

namespace ServiceBundle\Factories;

use AppBundle\Entity\Report;

class ReportFactory
{
    /**
     * @param $parsedData
     * @return Report
     */
    public static function makeByData($parsedData)
    {
        $report = new Report();
        $report->setTester($parsedData['tester']);
        $report->setContractor($parsedData['contractor']);
        $report->setStatus($parsedData['status']);
        $report->setService($parsedData['service']);
        $report->setWorkorder($parsedData['workOrder']);
        $report->setFinishTime($parsedData['finishTime']);

        if (isset($parsedData['needRepair'])) {
            $report->setIsNeedRepair($parsedData['needRepair']);
        }

        if (isset($parsedData['printTemplate'])) {
            $report->setPrintTemplate($parsedData['printTemplate']);
        }

        if (isset($parsedData['textReport'])) {
            $report->setTextReport($parsedData['textReport']);
        }

        if (isset($parsedData['htmlReport'])) {
            $report->setHtmlReport($parsedData['htmlReport']);
        }

        return $report;
    }
}
