<?php

namespace WorkorderBundle\DTO;

class FinishRequestDTO
{
    public $workorder = null;
    public $finisher = null;
    public $comment = null;
    public $coordinates = null;
    public $finishTime = null;
}
