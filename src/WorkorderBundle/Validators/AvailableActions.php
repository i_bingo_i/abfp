<?php

namespace WorkorderBundle\Validators;

use AppBundle\Entity\WorkorderStatus;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\WorkorderStatusRepository;

class AvailableActions
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var  WorkorderStatusRepository */
    private $workorderStatusRepository;

    /** @var array $errorMessages */
    protected $errorMessages = [];

    /**
     * AvailableActions constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        $this->workorderStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");
    }

    /**
     * @param WorkorderStatus $currentAction
     * @param string $newAction
     * @return bool
     */
    public function validate(WorkorderStatus $currentAction, string $newAction)
    {
        /** @var WorkorderStatus $workorderNewAction */
        $workorderNewAction = $this->workorderStatusRepository->findOneBy(['alias' => $newAction]);

        if (in_array($workorderNewAction, $currentAction->getAvailableActions()->toArray())) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return array_merge(["<b>You can not do this action</b>.<br>"], $this->errorMessages);
    }
}
