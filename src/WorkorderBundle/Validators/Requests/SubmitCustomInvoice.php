<?php

namespace WorkorderBundle\Validators\Requests;

use AppBundle\Entity\Workorder;
use InvoiceBundle\DataTransportObject\Requests\CreateCustomInvoiceDTO;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WorkorderBundle\Validators\AvailableActions;

class SubmitCustomInvoice
{
    const SUBMITTED_INVOICE_STATUS_ALIAS = 'send_invoice';

    /** @var ContainerInterface  */
    private $container;
    /** @var AvailableActions */
    private $availableActionsValidator;

    /** @var array $errorMessages */
    protected $errorMessages = [];

    /**
     * SubmitCustomInvoice constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->availableActionsValidator = $this->container->get('workorder.available_actions.validator');
    }

    /**
     * @param CreateCustomInvoiceDTO $customInvoiceDTO
     * @return bool
     */
    public function validate(CreateCustomInvoiceDTO $customInvoiceDTO)
    {
        if (!$customInvoiceDTO->getFile() instanceof UploadedFile) {
            $this->errorMessages[] = "You did not attach Invoice PDF file";

            return false;
        }

        if (!$customInvoiceDTO->getComment()) {
            $this->errorMessages[] = "Comment field is required for this action";

            return false;
        }

        /** @var Workorder $workorder */
        $workorder = $customInvoiceDTO->getWorkorder();
        if (!$this->availableActionsValidator->validate(
            $workorder->getStatus(),
            self::SUBMITTED_INVOICE_STATUS_ALIAS
        )) {
            $this->errorMessages[] = "Workorder with this status can not be submitted";

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }
}
