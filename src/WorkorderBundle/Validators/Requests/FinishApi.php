<?php

namespace WorkorderBundle\Validators\Requests;

use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\Workorder;
use WorkorderBundle\DTO\FinishRequestDTO;

class FinishApi extends Finish
{
    /**
     * @param FinishRequestDTO $finishRequestDTO
     * @return bool
     */
    public function validate(FinishRequestDTO $finishRequestDTO)
    {
        if (!$finishRequestDTO->finisher instanceof ContractorUser) {
            $this->errorMessages[] = "Contractor User does not found.";

            return false;
        }

        return parent::validate($finishRequestDTO);
    }
}
