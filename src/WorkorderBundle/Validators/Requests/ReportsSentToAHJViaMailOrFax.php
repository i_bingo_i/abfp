<?php

namespace WorkorderBundle\Validators\Requests;

use AppBundle\Entity\Workorder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WorkorderBundle\Validators\AvailableActions;

class ReportsSentToAHJViaMailOrFax
{
    const SENT_REPORTS_TO_AHJ_STATUS_ALIAS = 'pending_payment';

    /** @var ContainerInterface  */
    private $container;
    /** @var AvailableActions */
    private $availableActionsValidator;

    /** @var array $errorMessages */
    protected $errorMessages = [];

    /**
     * InvoiceSentViaMailOrFax constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->availableActionsValidator = $this->container->get('workorder.available_actions.validator');
    }

    /**
     * @param Workorder $workorder
     * @param null $comment
     * @return bool
     */
    public function validate(Workorder $workorder, $comment = null)
    {
        if (!$comment) {
            $this->errorMessages[] = "Comment field is required for this action";

            return false;
        }

        if (!$this->availableActionsValidator->validate(
            $workorder->getStatus(),
            self::SENT_REPORTS_TO_AHJ_STATUS_ALIAS
        )) {
            $this->errorMessages[] = "Workorder with this status can not be send reports to AHJ";

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }
}
