<?php

namespace WorkorderBundle\Validators;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use \AppBundle\Entity\WorkorderStatus;

class AvailableStatus
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var  WorkorderStatusRepository */
    private $workorderStatusRepository;

    /** @var array $errorMessages */
    protected $errorMessages = [];

    /**
     * WorkorderStatus constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        $this->workorderStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");
    }

    /**
     * @param WorkorderStatus $workorderStatus
     * @param WorkorderStatus $changeableStatusAlias
     * @return bool
     */
    public function canChangeStatus(WorkorderStatus $workorderStatus, WorkorderStatus $changeableStatusAlias)
    {
        if (in_array($workorderStatus, $changeableStatusAlias->getAvailableStatuses()->toArray())) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return array_merge(["<b>You can not do this action</b>.<br>"], $this->errorMessages);
    }
}
