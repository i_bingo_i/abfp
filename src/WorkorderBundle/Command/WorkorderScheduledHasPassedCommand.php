<?php

namespace WorkorderBundle\Command;

use AppBundle\Entity\ProcessingStats;
use AppBundle\Entity\ProcessingStatsType;
use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\ProposalsProcessing;
use Throwable;

class WorkorderScheduledHasPassedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('workorders:scheduled:has_passed')
            ->setDescription("It command would running with changing some application data depending on conditions")
            ->setHelp("It command would running with changing some application data depending on conditions");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $processingStatsManager = $this->getContainer()->get('app.processing_stats.manager');

        try {
            $workorderProcesingService = $this->getContainer()->get('workorder.processing.service');
            $processingStatsManager->createMessageByAlias("Start workorders scheduled date has passed Processing");
            $workorderProcesingService->scheduledDateHasPassed();
            $processingStatsManager->createMessageByAlias("Finish Processing");
        } catch (Throwable $e) {
            $processingStatsManager->createMessageByAlias(
                "Error during workorders processing. Message: ".$e->getMessage(),
                "error"
            );
        }
    }
}
