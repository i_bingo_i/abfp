<?php

namespace WorkorderBundle\Services;

use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Services\Letter\LetterActionManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SendInvoiceViaEmail
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var LetterActionManager */
    private $letterActionManager;
    /** @var AfterSendInvoiceChecker */
    private $afterSendInvoiceChecker;


    /**
     * AfterSendInvoiceChecker constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->letterActionManager = $this->container->get("letter.action.manager");
        $this->afterSendInvoiceChecker = $this->container->get("workorder.after_send_invoice_checker.service");

    }

    /**
     * @param SendFormViaEmailDTO $sendInvoiceViaEmailDTO
     */
    public function sendInvoiceViaEmailProcess(SendFormViaEmailDTO $sendInvoiceViaEmailDTO)
    {
        $result = $this->letterActionManager->sendViaEmailForWorkorder($sendInvoiceViaEmailDTO);

        if ($result) {
            $this->afterSendInvoiceChecker->createLog(
                $sendInvoiceViaEmailDTO->getWorkorder(),
                WorkorderLogType::TYPE_INVOICE_SENT_VIA_MAIL,
                $result
            );
            $this->afterSendInvoiceChecker->addStepInvoiceWasSend($sendInvoiceViaEmailDTO);
            $this->afterSendInvoiceChecker->checkIfReportsWereSendWithInvoice($sendInvoiceViaEmailDTO);
        }
    }
}