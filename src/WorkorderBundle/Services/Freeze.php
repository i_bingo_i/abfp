<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountHistory;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceHistory;
use AppBundle\Entity\Repository\ServiceHistoryRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderFrozenContent;
use AppBundle\Factories\WorkorderFrozenContentFactory;
use AppBundle\Services\Account\AccountHistoryService;
use AppBundle\Services\AccountContactPerson\Authorizer;
use AppBundle\Services\Device\DeviceHistoryService;
use AppBundle\Services\Service\Unlinked;
use AppBundle\Services\Workorder\PDFManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Freeze
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $container;

    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;

    /** @var Authorizer */
    private $acpAuthorizerService;
    /** @var AccountHistoryService */
    private $accountHistoryService;
    /** @var DeviceHistoryService */
    private $deviceHistoryService;
    /** @var PDFManager */
    private $woPdfManager;
    /** @var \AppBundle\Services\ServiceHistory\Creator */
    private $serviceHistoryCreator;
    /** @var Unlinked */
    private $serviceUnlinked;

    /**
     * Freeze constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->serviceHistoryRepository = $this->objectManager->getRepository('AppBundle:ServiceHistory');

        $this->acpAuthorizerService = $this->container->get('app.account_contact_person_authorizer.service');
        $this->accountHistoryService = $this->container->get('app.account_history.service');
        $this->deviceHistoryService = $this->container->get('app.device_history.service');
        $this->woPdfManager = $this->container->get('workorder.pdf.manager');
        $this->serviceHistoryCreator = $this->container->get('app.service_history_creator.service');
        $this->serviceUnlinked = $this->container->get('app.service_unlinked.service');
    }

    /**
     * @param Workorder $workorder
     */
    public function freezing(Workorder $workorder)
    {
        $servicesCollection = $workorder->getServices();
        $servicesArray = $servicesCollection->toArray();

        $serviceHistoryArray = $this->serviceHistoryRepository->getLastByService($servicesArray);
        $serviceHistoryArray =
            $this->serviceHistoryCreator->checkAndGetServicesHistory($servicesArray, $serviceHistoryArray);

        $accountAuthorizer = $this->acpAuthorizerService->getAccountAuthorizerForWorkorder($workorder);
        $workorder->setAccountAuthorizer($accountAuthorizer);

        foreach ($serviceHistoryArray as $serviceHistory) {
            $workorder = $this->setFrozenContentItemForWorkorder($serviceHistory, $workorder);
        }

        $workorder->setIsFrozen(true);

        // Creating pdf
        $pdfReport = $this->woPdfManager->save($workorder);
        $workorder->setPdf($pdfReport);

        if ($workorder->getServices()) {
            $this->serviceUnlinked->unlinkFromWorkorderAndResetStatus($workorder);
        }
    }

    /**
     * @param ServiceHistory $serviceHistory
     * @param Workorder $workorder
     * @return Workorder
     */
    private function setFrozenContentItemForWorkorder(ServiceHistory $serviceHistory, Workorder $workorder)
    {
        /** @var Service $service */
        $service = $serviceHistory->getOwnerEntity();
        /** @var Account $account */
        $account = $service->getAccount();
        /** @var Device $device */
        $device = $service->getDevice();

        /** @var AccountHistory $accountHistory */
        $accountHistory = $this->accountHistoryService->checkAndGetAccountHistory($account);
        /** @var DeviceHistory $deviceHistory */
        $deviceHistory = $this->deviceHistoryService->checkAndGetDeviceHistory($device);

        /** @var WorkorderFrozenContent $workorderFrozenContent */
        $workorderFrozenContent =
            WorkorderFrozenContentFactory::make($workorder, $accountHistory, $deviceHistory, $serviceHistory);

        if (!empty($service->getDepartmentChannel()) and
            !empty($service->getDepartmentChannel()->getFeesBasis()) and
            $service->getDepartmentChannel()->getFeesBasis()->getAlias() == "per device" and
            !empty($service->getDepartmentChannel()->getUploadFee())
        ) {
            $workorderFrozenContent->setMunicipalityFee($service->getDepartmentChannel()->getUploadFee());
        }

        if (!empty($service->getDepartmentChannel()) and
            !empty($service->getDepartmentChannel()->getFeesBasis()) and
            $service->getDepartmentChannel()->getFeesBasis()->getAlias() == "per device" and
            !empty($service->getDepartmentChannel()->getProcessingFee())
        ) {
            $workorderFrozenContent->setProcessingFee($service->getDepartmentChannel()->getProcessingFee());
        }

        $this->objectManager->persist($workorderFrozenContent);
        $this->objectManager->flush();

        return $workorder;
    }
}
