<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\EntityManager\StepManager;
use AppBundle\Entity\Step;
use AppBundle\Entity\StepNamed;
use AppBundle\Entity\StepStatus;
use AppBundle\Entity\Workorder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WorkorderBundle\Factories\WorkorderStepFactory;

class StepCreator
{
    /** @var ContainerInterface  */
    private $container;
    /** @var StepManager */
    private $stepManager;

    /**
     * StepCreator constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->stepManager = $container->get('app.step.manager');
    }

    /**
     * @param Workorder $workorder
     * @param StepNamed $stepNamed
     * @param StepStatus $stepStatus
     * @return Step
     */
    public function create(Workorder $workorder, StepNamed $stepNamed, StepStatus $stepStatus)
    {
        /** @var Step $step */
        $step = WorkorderStepFactory::make($workorder, $stepStatus, $stepNamed);
        $this->stepManager->save($step);

        return $step;
    }
}
