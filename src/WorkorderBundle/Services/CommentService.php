<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\Repository\WorkorderCommentRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderComment;
use AppBundle\Services\Files;
use AppBundle\Services\PDFService;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use AppBundle\Services\TrashRequests\Creator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CommentService
{
    /** @var ObjectManager */
    private $em;
    /** @var ContainerInterface */
    private $container;
    /** @var Files */
    private $fileService;
    /** @var PDFService */
    private $pdfService;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var Creator  */
    private $trashRequestCreator;
    /** @var UserRepository */
    private $userRepository;
    /** @var WorkorderCommentRepository */
    private $workorderCommentRepository;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var Serializer  */
    private $serializer;

    private $errorMessages = [];
    /**
     * CommentService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->em = $objectManager;
        $this->container = $serviceContainer;
        $this->fileService = $this->container->get('app.files');
        $this->pdfService = $this->container->get('app.pdf_service');
        $this->serviceManager = $this->container->get('app.service.manager');
        $this->serializer = $this->container->get("jms_serializer");


        $this->trashRequestCreator = $this->container->get('app.trash_requests.creator.service');
        $this->workorderRepository = $this->em->getRepository("AppBundle:Workorder");
        $this->userRepository = $this->em->getRepository("AppBundle:User");
        $this->workorderCommentRepository = $this->em->getRepository("AppBundle:WorkorderComment");
    }

    public function createByParams($params, $route)
    {
        $parsedData = $this->parseForCreate($params);
        $this->checkingForCreate($parsedData);
        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]
                );

            return false;
        }

        $comment = new WorkorderComment();
        $comment->setWorkorder($parsedData["workorder"]);
        $comment->setComment($parsedData["comment"]);
        if (!is_null($parsedData["user"])) {$comment->setAuthor($parsedData["user"]);}

        $this->em->persist($comment);
        $this->em->flush();

        return $comment;
    }

    /**
     * @param $orderID
     * @param $route
     * @return array|bool|mixed
     */
    public function getById($orderID, $route)
    {
        $workorder = $this->workorderRepository->find($orderID);

        if (!$workorder) {
            $this->trashRequestCreator->create($route, json_encode(["orderID" => $orderID]), "Workorder not found");

            return false;
        }

        $comments = $this->workorderCommentRepository->findBy(["workorder" => $workorder]);
        $serializedComments = $this->serializer->toArray(
            $comments,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        $filteredComments = $this->filteringComments($serializedComments);

        return $filteredComments;
    }


    /**
     * @param $commentID
     * @param null|UploadedFile $file
     * @param $route
     * @param $accessToken
     * @return WorkorderComment|bool
     */
    public function attachFileByParams($commentID, ?UploadedFile $file, $route, $accessToken)
    {
        /** @var WorkorderComment $comment */
        $comment = $this->workorderCommentRepository->find($commentID);

        $this->checkingForAttach($comment, $file);

        if (!empty($this->errorMessages)) {
            $fullPath = "";
            if ($file instanceof UploadedFile) {
                /** @var Files $filesService */
                $filesService = $this->container->get('app.files');

                $fileName = $filesService->getRandomFileName($file, 'workorder_comment_');
                $savePath = "/uploads/bad_request_storage/";
                $filesService->saveUploadFile($file, $savePath, $fileName);
                $fullPath = $savePath . $fileName;
            }

            $this->trashRequestCreator->create(
                $route,
                json_encode(["commentID" => $commentID]),
                $this->errorMessages,
                $fullPath,
                $accessToken
            );

            return false;
        }

        $saveImagePath = "/uploads/workorder_comments";
        $filePath = $this->container->get('app.images')->saveUploadFile($file, $saveImagePath);

        $comment->setPhoto($saveImagePath."/".$filePath);

        $this->em->persist($comment);
        $this->em->flush();

        return $comment;
    }


    /**
     * @param Form $form
     * @param Workorder $workorder
     */
    public function creatingComment(Form $form, Workorder $workorder)
    {
        /** @var WorkorderComment $workorderComment */
        $workorderComment = $form->getData();
        $photo = null;
        $fileName = null;

        if ($workorderComment->getPhoto()) {
            if (in_array($workorderComment->getPhoto()->getMimeType(), ['application/pdf', 'application/x-pdf'])) {
                $fileName = $workorderComment->getPhoto()->getClientOriginalName();

            }

            $path = '/uploads/workorder_comments/';
            $photo = $path . $this->fileService->saveUploadFile($workorderComment->getPhoto(), $path, $fileName);
        }

        $this->create($workorderComment, $workorder, $photo);
    }

    /**
     * @param $comment
     * @param $workorder
     * @param $user
     * @return WorkorderComment
     */
    public function createCommentFromIOS($comment, $workorder, $user)
    {
        return $this->create($comment, $workorder, null, $user);
    }

    /**
     * @param WorkorderComment $workorderComment
     * @param Workorder $workorder
     * @param null $photo
     * @param User|null $user
     * @return WorkorderComment
     */
    private function create(WorkorderComment $workorderComment, Workorder $workorder, $photo = null, ?User $user = null)
    {
        if (!$user) {
            $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();
        } else {
            $currentUser = $user;
        }
        $workorderComment->setComment($workorderComment->getComment() ?? '');
        $workorderComment->setWorkorder($workorder);
        $workorderComment->setPhoto($photo);
        $workorderComment->setAuthor($currentUser);

        $this->em->persist($workorderComment);
        $this->em->flush();

        return $workorderComment;
    }

    /**
     * @param $data
     * @return array
     */
    private function parseForCreate($data)
    {
        $parsedData = [];

        $parsedData["workorder"] = null;
        if (isset($data["orderID"])) {
            $parsedData["workorder"] = $this->workorderRepository->find($data["orderID"]);
        }

        $parsedData["comment"] = null;
        if (isset($data["comment"])) {
            $parsedData["comment"] = $data["comment"];
        }

        $parsedData["user"] = null;
        if (isset($data["accessToken"])) {
            $parsedData["user"] = $this->userRepository->findOneBy(["accessToken" => $data["accessToken"]]);
        }

        return $parsedData;
    }

    private function checkingForAttach(?WorkorderComment $comment, ?UploadedFile $file)
    {
        $this->errorMessages = [];

        if (is_null($comment)) {
            $this->errorMessages[] = "Workorder comment not found";
        }

        if (is_null($file)) {
            $this->errorMessages[] = "File has not received";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Workorder attachment image can not be uploaded</b>.<br>"], $this->errorMessages);
        }
    }

    /**
     * @param $parsedData
     */
    private function checkingForCreate($parsedData)
    {
        $this->errorMessages = [];

        if (is_null($parsedData["workorder"])) {
            $this->errorMessages[] = "Workorder not found";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Workorder comment can not be added</b>.<br>"], $this->errorMessages);
        }
    }

    /**
     * @param array|null $comments
     * @return array
     */
    private function filteringComments(?array $comments)
    {
        $filteredComments = [];
        foreach ($comments as $comment) {
            if (!isset($comment["forAdminUsersOnly"]) or $comment["forAdminUsersOnly"] === false) {
                $filteredComments[] = $comment;
            }
        }

        return $filteredComments;

    }

}