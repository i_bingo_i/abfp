<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderStatus;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\WorkorderRepository;

class Grouping
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var WorkorderRepository */
    private $workorderRepository;

    /**
     * Grouping constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        $this->workorderRepository = $this->objectManager->getRepository("AppBundle:Workorder");
    }

    /**
     * @param Account $account
     * @return string
     */
    public function byCategoryUnderAccount(Account $account)
    {
        $workorders = $this->workorderRepository->findSuitableWOsForCreatingByAccount($account);

        return $this->getJsonWOsResultForAddInspectionToWo($workorders);
    }

    /**
     * @param Account $account
     * @param Device $device
     * @return string
     */
    public function byDeviceUnderAccount(Account $account, Device $device)
    {
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $device->getNamed()->getCategory();
        $workorders = $this->workorderRepository->findSuitableWOsForCreating($account, $deviceCategory);

        return $this->getJsonWOsResultForAddInspectionToWo($workorders);
    }

    /**
     * @param $workorders
     * @return string
     */
    private function getJsonWOsResultForAddInspectionToWo($workorders)
    {
        $result = [];

        /** @var Workorder $workorder */
        foreach ($workorders as $workorder) {
            /** @var DeviceCategory $woCategory */
            $woCategory = $workorder->getDivision();
            $worCategoryName = $woCategory->getName();
            $woCategoryAlias = $woCategory->getAlias();
            $woId = $workorder->getId();
            /** @var WorkorderStatus $woStatus */
            $woStatus = $workorder->getStatus();
            $woStatusName = $woStatus->getName();

            $resultName = $worCategoryName . " " . "Workorder # " . $woId . " " . "(" . $woStatusName . ")";

            $result["$woCategoryAlias"][] = [$woId => $resultName];
        }

        return json_encode($result);
    }
}
