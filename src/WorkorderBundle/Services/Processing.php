<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\EntityManager\ProcessingStatsManager;
use AppBundle\Entity\EntityManager\WorkorderLogManager;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Repository\WorkorderLogTypeRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderLogType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WorkorderBundle\Services\LogCreator;
use WorkorderBundle\Services\LogMessage;

class Processing
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var ProcessingStatsManager */
    private $processingStatsManager;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var WorkorderStatusRepository */
    private $workorderStatusRepository;
    /** @var WorkorderLogTypeRepository */
    private $workorderLogTypeRepository;
    /** @var WorkorderManager */
    private $workorderManager;
    /** @var WorkorderLogManager */
    private $workorderLogManager;

    /**
     * Processing constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->processingStatsManager = $this->container->get('app.processing_stats.manager');
        $this->processingStatsManager = $this->container->get('app.processing_stats.manager');
        $this->workorderRepository = $this->objectManager->getRepository('AppBundle:Workorder');
        $this->workorderStatusRepository = $this->objectManager->getRepository('AppBundle:WorkorderStatus');
        $this->workorderLogTypeRepository = $this->objectManager->getRepository('AppBundle:WorkorderLogType');
        $this->workorderManager = $this->container->get('app.work_order.manager');
        $this->workorderLogManager = $this->container->get('app.workorder_log.manager');
    }

    /**
     * System Action “Scheduled date has come”
     *
     * @throws \Exception
     */
    public function scheduledDateHasCome()
    {
        $today = new \DateTime();
        $status = $this->workorderStatusRepository->findOneBy(
            ['alias' => $this->workorderManager::STATUS_TO_BE_DONE_TODAY]
        );
        $notInStatusesAliases = ['reschedule'];
        $workorders = $this->workorderRepository->getWhichDoneToday($status, $today, $notInStatusesAliases);

        if (!empty($workorders)) {
            $success = 0;
            /** @var Workorder $workorder */
            foreach ($workorders as $workorder) {
                $this->workorderManager->setStatus($workorder, $this->workorderManager::STATUS_TO_BE_DONE_TODAY);
                $this->workorderManager->update($workorder);

                // Create log record in WO log
                /** @var LogMessage $logMessage */
                $logMessage = $this->container->get('workorder.log_message');
                /** @var LogCreator $woLogCreator */
                $woLogCreator = $this->container->get('workorder.log_creator');
                $woLogCreator->createRecord($workorder, [
                    'type' => WorkorderLogType::TYPE_SCHEDULED_DATE_HAS_COME,
                    'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_SCHEDULED_DATE_HAS_COME)
                ]);

                $success++;
            }
            $this->processingStatsManager->createMessageByAlias(
                "Success finish in Scheduled date has come check. Count of Workorder: " . count($workorders) . ". Success processing: " . $success
            );
        } else {
            $this->processingStatsManager->createMessageByAlias("Nothing to do in Scheduled date has come check. No Workorder for processing.");
        }
    }

    /**
     * Scheduled date has passed. Workorder was not completed.
     *
     * @throws \Exception
     */
    public function scheduledDateHasPassed()
    {
        $today = new \DateTime();
        $status = $this->workorderStatusRepository->findOneBy(
            ['alias' => $this->workorderManager::STATUS_NOT_COMPLETED]
        );
        $notInStatusesAliases = ['reschedule'];
        $workorders = $this->workorderRepository->getScheduledDateWhichPassed($status, $today, $notInStatusesAliases);

        if (!empty($workorders)) {
            $success = 0;
            /** @var Workorder $workorder */
            foreach ($workorders as $workorder) {
                $this->workorderManager->setStatus($workorder, $this->workorderManager::STATUS_NOT_COMPLETED);
                $this->workorderManager->update($workorder);

                // Create log record in WO log
                /** @var LogMessage $logMessage */
                $logMessage = $this->container->get('workorder.log_message');
                /** @var LogCreator $woLogCreator */
                $woLogCreator = $this->container->get('workorder.log_creator');
                $woLogCreator->createRecord($workorder, [
                    'type' => WorkorderLogType::TYPE_SCHEDULED_DATE_HAS_PASSED,
                    'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_SCHEDULED_DATE_HAS_PASSED)
                ]);

                $success++;
            }
            $this->processingStatsManager->createMessageByAlias(
                "Success finish in Scheduled date has passed, check. Count of Workorder: " . count($workorders) . ". Success processing: " . $success
            );
        } else {
            $this->processingStatsManager->createMessageByAlias("Nothing to do in Scheduled date has passed check. No Workorder for processing.");
        }
    }
}
