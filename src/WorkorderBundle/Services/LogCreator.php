<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\EntityManager\WorkorderLogManager;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\WorkorderLogTypeRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderLog;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Services\SystemUser;
use WorkorderBundle\Factories\WorkorderLogFactory;

use Doctrine\Common\Persistence\ObjectManager;
use WorkorderBundle\Services\Exeptions\LogRecordTypeCantBeNull;

class LogCreator
{
    /** @var ObjectManager  */
    private $objectManager;
    /** @var SystemUser */
    private $systemUser;
    /** @var WorkorderLogManager */
    private $workorderLogManager;
    /** @var WorkorderLogTypeRepository */
    private $workorderLogTypeRepository;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;

    /**
     * LogCreator constructor.
     * @param ObjectManager $objectManager
     * @param SystemUser $systemUser
     * @param WorkorderLogManager $workorderLogManager
     */
    public function __construct(
        ObjectManager $objectManager,
        SystemUser $systemUser,
        WorkorderLogManager $workorderLogManager
    ) {
        $this->objectManager = $objectManager;
        $this->systemUser = $systemUser;
        $this->workorderLogManager = $workorderLogManager;
        $this->workorderLogTypeRepository = $this->objectManager->getRepository('AppBundle:WorkorderLogType');
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');
    }

    /**
     * Options params[]:
     * message <string> null by default
     * comment <string> null by default
     * invoice <string> (invoiceId), null by default
     * proposal <string> (proposalId), null by default
     * changingStatus <bool> false by default
     *
     * @param Workorder $workorder
     * @param array $params
     *
     * @throws LogRecordTypeCantBeNull
     */
    public function createRecord(Workorder $workorder, $params = [])
    {
        $parsedParams = $this->parseParams($params);
        /** @var WorkorderLogType $wororderLogType */
        $workorderLogType = $this->workorderLogTypeRepository->findOneBy(['alias' => $parsedParams['type']]);

        $this->zeroingOutOldLogRecordWithChangingStatus($workorder, $parsedParams['changingStatus']);
        $logRecord = WorkorderLogFactory::make($workorder, $workorderLogType, $parsedParams);

        $this->workorderLogManager->save($logRecord);
    }

    /**
     * @param $params
     * @return mixed
     * @throws LogRecordTypeCantBeNull
     */
    private function parseParams($params)
    {
        $parsedParams['comment'] = $params['comment'] ?? null;
        $parsedParams['letter'] = $params['letter'] ?? null;
        $parsedParams['invoice'] = $params['invoice'] ?? null;
        $parsedParams['proposal'] = $params['proposal'] ?? null;
        $parsedParams['changingStatus'] = $params['changingStatus'] ?? false;
        $parsedParams['message'] = $params['message'] ?? null;

        $parsedParams['type'] = $params['type'] ?? null;
        if (!$parsedParams['type']) {
            throw new LogRecordTypeCantBeNull("Workorder log record Type cant be null.");
        }

        $author = $params['author'] ?? null;
        $user = $this->systemUser->getCurrent($params['token'] ?? null);
        if ($user and $author != 'system') {
            $parsedParams['author'] = $this->contractorUserRepository->findOneBy(['user' => $user]);
        } else {
            $parsedParams['author'] = null;
        }

        return $parsedParams;
    }

    /**
     * @param Workorder $workorder
     * @param bool $changingStatus
     */
    private function zeroingOutOldLogRecordWithChangingStatus(Workorder $workorder, $changingStatus)
    {
        if ($changingStatus) {
            $workorderLogComments = $workorder->getLogs()->filter(function (WorkorderLog $log) {
                return !empty($log->getChangingStatus());
            });

            /** @var WorkorderLog $workorderLogComment */
            foreach ($workorderLogComments as $workorderLogComment) {
                $workorderLogComment->setChangingStatus(false);
                $this->objectManager->persist($workorderLogComment);
            }
        }
    }
}
