<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderLogType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Services\Exeptions\LogRecordTypeCantBeNull;
use AppBundle\Entity\WorkorderStatus;
use WorkorderBundle\Validators\AvailableStatus;

class PaymentReceived
{
    public const PAYMENT_WAS_RECEIVED = 'done';

    /** @var ObjectManager */
    private $objectManager;

    /** @var WorkorderStatusRepository */
    private $workorderStatusRepository;
    /** @var WorkorderManager */
    private $woManager;
    /** @var LogCreator */
    private $woLogCreator;
    /** @var LogMessage $logMessage */
    private $logMessage;
    /** @var Session */
    private $session;
    /** @var ContainerInterface  */
    private $container;
    /** @var WorkorderStatus|null|object  */
    private $paymentWasReceivedStatus;
    /** @var AvailableStatus */
    private $availableStatusService;

    /**
     * VerifyJobResults constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->workorderStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");
        $this->paymentWasReceivedStatus =
            $this->workorderStatusRepository->findOneBy(['alias' => self::PAYMENT_WAS_RECEIVED]);

        $this->woManager = $this->container->get('app.work_order.manager');
        $this->woLogCreator = $this->container->get('workorder.log_creator');
        $this->logMessage = $this->container->get('workorder.log_message');
        $this->session = $this->container->get('session');
        $this->availableStatusService = $this->container->get('workorder.available_status.validator');
    }

    /**
     * @param Workorder $workorder
     * @param string $comment
     */
    public function received(Workorder $workorder, string $comment)
    {
        if ($this->availableStatusService->canChangeStatus($workorder->getStatus(), $this->paymentWasReceivedStatus)) {
            $workorder->setStatus($this->paymentWasReceivedStatus);
        }
        $this->woManager->update($workorder);

        try {
            $this->woLogCreator->createRecord($workorder, [
                'type' => WorkorderLogType::TYPE_PAYMENT_WAS_RECEIVED,
                'message' => $this->logMessage->makeByLogType(WorkorderLogType::TYPE_PAYMENT_WAS_RECEIVED),
                'changingStatus' => true,
                'comment' => $comment
            ]);
        } catch (LogRecordTypeCantBeNull $exception) {
            $this->session->getFlashBag()->add('error', $exception->getMessage());
        }
    }
}
