<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderLogType;
use InvoiceBundle\Entity\Invoices;

class WorkorderLogHandler
{
    /** @var LogCreator */
    private $workorderLogCreator;
    /** @var LogMessage */
    private $workorderLogMessage;

    /**
     * WorkorderLogHandler constructor.
     * @param LogMessage $workorderLogMessage
     * @param LogCreator $workorderLogCreator
     */
    public function __construct(
        LogMessage $workorderLogMessage,
        LogCreator $workorderLogCreator
    ) {
        $this->workorderLogMessage = $workorderLogMessage;
        $this->workorderLogCreator = $workorderLogCreator;
    }

    /**
     * @param Workorder $workorder
     * @param Invoices $invoice
     * @throws \Exception
     */
    public function invoiceDraftWasSave(Workorder $workorder, Invoices $invoice)
    {
        $this->workorderLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_INVOICE_DRAFT_WAS_SAVE,
            'message' => $this->workorderLogMessage->makeByInvoice(WorkorderLogType::TYPE_INVOICE_DRAFT_WAS_SAVE),
            'changingStatus' => false,
            'invoice' => $invoice
        ]);
    }

    /**
     * @param Workorder $workorder
     * @param Invoices $invoice
     * @throws \Exception
     */
    public function invoiceSubmittedToAccountingSystem(Workorder $workorder, Invoices $invoice)
    {
        $this->workorderLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_INVOICE_SUBMITTED,
            'message' => $this->workorderLogMessage->makeByInvoice(WorkorderLogType::TYPE_INVOICE_SUBMITTED),
            'changingStatus' => true,
            'invoice' => $invoice
        ]);
    }

    /**
     * @param Workorder $workorder
     * @param Invoices $invoice
     * @throws Exeptions\LogRecordTypeCantBeNull
     */
    public function invoicePaymentsOverrideWasEdited(Workorder $workorder, Invoices $invoice)
    {
        $this->workorderLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_INVOICE_PAYMENT_OVERRIDE,
            'message' => $this->workorderLogMessage->makeByLogType(
                WorkorderLogType::TYPE_INVOICE_PAYMENT_OVERRIDE,
                [$invoice->getOverridePayment()]
            ),
            'changingStatus' => true,
            'invoice' => $invoice
        ]);
    }
}