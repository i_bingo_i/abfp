<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Event;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\User;
use AppBundle\Entity\WorkorderLogType;
use InvoiceBundle\Entity\Invoices;

class LogMessage
{
    private const MESSAGES = [
        WorkorderLogType::TYPE_BILL_SENT_INVOICE_CREATE => 'Workorder was finished. '
            . 'Please send bill to the client or create invoice in the Quickbooks.',
        WorkorderLogType::TYPE_PAYMEN_RECEIVED => 'Bill was sent to the Client OR Invoice was created. '
            . 'Please inform the system when the payment is received.',
        WorkorderLogType::TYPE_REPORTS_SENT
        => 'Payment received. Please send reports to the Municipalities and to the Client.',
        WorkorderLogType::TYPE_SCHEDULED_DATE_HAS_COME => 'Scheduled date has come. Workorder is to be done today.',
        WorkorderLogType::TYPE_DONE
        => 'Reports were sent to the Municipalities and to the Client. Workorder is completely done. Nice job!',
        WorkorderLogType::TYPE_SCHEDULED_DATE_HAS_PASSED
        => 'Scheduled date has passed. Workorder was not completed!',
        WorkorderLogType::TYPE_WORKORDER_WAS_SCHEDULED => '%s was assigned to the workorder to %s, %s',
        WorkorderLogType::TYPE_WORKORDER_TO_BE_SCHEDULED => '%s was unassigned from the workorder from %s, %s',
        WorkorderLogType::TYPE_DELETE => 'Workorder was deleted.',
        WorkorderLogType::TYPE_RESCHEDULE => 'Workorder needs to be rescheduled.',
        WorkorderLogType::TYPE_INVOICE_DRAFT_WAS_SAVE
        => 'QB Invoice Draft was saved to be submitted to Quickbooks later.',
        WorkorderLogType::TYPE_INVOICE_SUBMITTED => 'Invoice submitted to Quickbooks. Please send invoice to client.',
        WorkorderLogType::TYPE_VERIFY_JOB_RESULTS
        => 'Workorder was finished. Please review job results and verify them.',
        WorkorderLogType::TYPE_SUBMIT_INVOICE => 'Workorder verified. Please submit invoice.',
        WorkorderLogType::TYPE_USE_CUSTOM_PDF => 'Custom Invoice submitted. Please send invoice to client.',
        WorkorderLogType::TYPE_INVOICE_SENT_VIA_MAIL => 'Invoice sent via Email.',
        WorkorderLogType::TYPE_INVOICE_SENT_VIA_MAIL_OR_FAX => 'Invoice sent via Mail/Fax',
        WorkorderLogType::TYPE_SEND_REPORTS_TO_ACCOUNT_AUTHORIZER => 'Please send Reports to Account Authorizer.',
        WorkorderLogType::TYPE_REPORTS_SENT_ALONG_WITH_INVOICE_VIA_MAIL_OR_FAX =>
            'Reports were sent to Client along with Invoice.',
        WorkorderLogType::TYPE_REPORTS_SENT_TO_CLIENT_VIA_MAIL_OR_FAX => 'Reports sent to client via Mail/Fax.',
        WorkorderLogType::TYPE_REPORTS_SENT_TO_CLIENT_VIA_EMAIL => 'Reports sent to client via Email.',
        WorkorderLogType::TYPE_REPORTS_SEND_TO_AHJ_VIA_MAIL_OR_FAX => 'Please send Reports to AHJ.',
        WorkorderLogType::TYPE_REPORTS_SENT_TO_AHJ_VIA_MAIL_OR_FAX => 'Reports sent to AHJ via Mail/Fax.',
        WorkorderLogType::TYPE_REPORTS_SENT_TO_AHJ_VIA_EMAIL => 'Reports sent to AHJ via Email.',
        WorkorderLogType::TYPE_PAYMENTS_NOT_FOUND_IN_QUICKBOOKS =>
            'Payment not found or not full in Quickbooks or Custom Invoice is used. Please check and process payment.',
        WorkorderLogType::TYPE_PAYMENT_WAS_RECEIVED =>
            'Payments obtaining is confirmed manually. Workorder is completely done. Nice job!',
        WorkorderLogType::TYPE_INVOICE_PAYMENT_OVERRIDE =>
            'Payments override was edited for Invoice. New override value is $%s'
    ];

    /**
     * @param $message
     * @param array $params
     *
     * @return string
     */
    public function makeCustom($message, $params = [])
    {
        return sprintf($message, ...$params);
    }

    /**
     * @param $type
     * @param array $params
     *
     * @return mixed
     */
    public function makeByLogType($type, $params = [])
    {
        return $this->makeCustom(self::MESSAGES[$type], $params);
    }

    /**
     * @param Event $event
     *
     * @return string
     */
    public function makeByCreateEvent(Event $event)
    {
        /** @var User $user */
        $user = $event->getUser()->getUser();

        return $this->makeCustom('%s was assigned to the workorder to %s, %s', [
            $user->getLastName() . ' ' . $user->getFirstName(),
            $event->getFromDate()->format('m/d/Y'),
            $event->getFromDate()->format('h:i A')." - ".$event->getToDate()->format('h:i A')
        ]);
    }

    /**
     * @param Event $event
     *
     * @return string
     */
    public function makeByDeleteEvent(Event $event)
    {
        /** @var User $user */
        $user = $event->getUser()->getUser();

        return $this->makeCustom('%s was unassigned from the workorder from %s, %s', [
            $user->getLastName() . ' ' . $user->getFirstName(),
            $event->getFromDate()->format('m/d/Y'),
            $event->getFromDate()->format('h:i A')." - ".$event->getToDate()->format('h:i A')
        ]);
    }

    /**
     * @param Service $service
     *
     * @return string
     */
    public function makeByService(Service $service)
    {
        /** @var ServiceNamed $serviceNamed */
        $serviceNamed = $service->getNamed();
        $serviceName = $serviceNamed->getName();
        $serviceNameString = '"' . $serviceName . '"';

        return $serviceNameString . " " . "service was added to Workorder";
    }

    /**
     * @param $repairs
     *
     * @return string
     */
    public function makeByRepairs($repairs)
    {
        $servicesNames = null;
        $servicesCount = count($repairs);
        $countIteration = 0;

        /** @var Service $repair */
        foreach ($repairs as $repair) {
            $countIteration++;
            /** @var ServiceNamed $serviceNamed */
            $serviceNamed = $repair->getNamed();
            $serviceName = $serviceNamed->getName();
            $serviceNameString = '"' . $serviceName . '"';

            if ($servicesCount > 1 and $countIteration != $servicesCount) {
                $servicesNames .= $serviceNameString . ", ";
            } else {
                $servicesNames .= $serviceNameString;
            }
        }

        if ($servicesCount > 1) {
            $lastPartMessage = "services were added to Workorder";
        } else {
            $lastPartMessage = "service was added to Workorder";
        }

        return $servicesNames . " " . $lastPartMessage;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function makeByInvoice($type)
    {
        return self::MESSAGES[$type];
    }
}
