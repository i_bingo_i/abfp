<?php


namespace WorkorderBundle\Services;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DepartmentChannelDynamicFieldValue;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\DepartmentChannelDynamicFieldValueRepository;
use AppBundle\Entity\Repository\DepartmentChannelRepository;
use AppBundle\Entity\Workorder;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GetLetterRecipientService
{
    /** @var ContainerInterface */
    private $container;
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonRepository  */
    private $accountContactPersonRepository;
    /** @var DepartmentChannelDynamicFieldValueRepository */
    private $departmentChannelValueRepository;



    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->accountContactPersonRepository = $this->objectManager->getRepository(
            "AppBundle:AccountContactPerson"
        );
        $this->departmentChannelValueRepository = $this->objectManager->getRepository(
            "AppBundle:DepartmentChannelDynamicFieldValue"
        );
    }

    /**
     * @param Workorder $workorder
     * @return bool|null
     */
    public function getPaymentEmail(Workorder $workorder)
    {
        /** @var DeviceCategory $division */
        $division = $workorder->getDivision();
        $account = $workorder->getAccount();

        $payment = $this->accountContactPersonRepository->findPrimaryPaymentByDivisionForAccount($account, $division);

        if ($payment instanceof AccountContactPerson) {

            return $payment->getContactPerson()->getEmail() ?? false;
        }

        return false;
    }

    /**
     * @param Workorder $workorder
     * @return bool|null
     */
    public function getAuthorizerEmail(Workorder $workorder)
    {
        /** @var DeviceCategory $division */
        $division = $workorder->getDivision();
        $account = $workorder->getAccount();

        $payment = $this->accountContactPersonRepository->findAuthorizerByDivision($account, $division);

        if ($payment instanceof AccountContactPerson) {

            return $payment->getContactPerson()->getEmail() ?? false;
        }

        return false;
    }


    /**
     * @param Workorder $workorder
     * @return bool|string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMunicipalityEmail(Workorder $workorder)
    {
        /** @var DeviceCategory $division */
        $division = $workorder->getDivision();
        $municipality = $workorder->getAccount()->getMunicipality();
        /** @var DepartmentChannelDynamicFieldValue $channel */
        $channel = $this->departmentChannelValueRepository->getEmailDepartmentChannelValByMunAndDiv($municipality, $division);

        if ($channel instanceof DepartmentChannelDynamicFieldValue) {

            return $channel->getValue() ?? false;
        }

        return false;
    }

    /**
     * @param Workorder $workorder
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function composeAllEmails(Workorder $workorder)
    {
        $recipients = [];
        $recipients["payment"] = $this->getPaymentEmail($workorder);
        $recipients["authorizer"] = $this->getAuthorizerEmail($workorder);
        $recipients["municipality"] = $this->getMunicipalityEmail($workorder);

        return $recipients;
    }


}