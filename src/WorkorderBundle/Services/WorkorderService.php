<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Device;
use AppBundle\Entity\Event;
use AppBundle\Entity\Repository\EventRepository;
use AppBundle\Entity\Repository\ProposalRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderFrozenContent;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Entity\WorkorderStatus;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use ServiceBundle\Services\ServiceCreatorService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\TrashRequests\Creator;

class WorkorderService
{
    public const STATUS_RESCHEDULE = 'reschedule';
    public const STATUS_PENDING_PAYMENT = 'pending_payment';
    public const STATUS_SEND_REPORTS = 'send_reports';
    public const STATUS_DONE = 'done';
    public const STATUS_DELETE = 'delete';

    public const VALID_STATUSES_FOR_RESCHEDULE =
        ['to_be_scheduled', 'scheduled', 'to_be_done_today', 'verify_job_results'];

    /** @var ObjectManager */
    private $objectManager;
    /** @var EventRepository */
    private $eventRepository;
    /** @var WorkorderStatusRepository */
    private $workorderStatusRepository;
    /** @var ProposalRepository */
    private $proposalRepository;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ContainerInterface */
    private $container;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var Creator  */
    private $trashRequestCreator;
    /** @var ServiceCreatorService */
    private $serviceCreatorService;

    private $errorMessages;
    /**
     * WorkorderService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->eventRepository = $this->objectManager->getRepository('AppBundle:Event');
        $this->workorderStatusRepository = $this->objectManager->getRepository('AppBundle:WorkorderStatus');
        $this->proposalRepository = $this->objectManager->getRepository('AppBundle:Proposal');
        $this->workorderRepository = $this->objectManager->getRepository('AppBundle:Workorder');

        $this->serviceManager = $this->container->get('app.service.manager');
        $this->trashRequestCreator = $this->container->get('app.trash_requests.creator.service');
        $this->serviceCreatorService = $this->container->get('service.creator.service');
    }

    /**
     * @param Workorder $workorder
     * @param Event $event
     * @return array
     */
    public function setScheduledDate(Workorder $workorder, Event $event)
    {
        $theEarliestEvent = null;
        $theOldestEvent = null;
        $isExistEventInWorkorder = $workorder->getEvents()->filter(function (Event $item) use ($event) {
            return $item->getId() != $event->getId();
        })->count();

        if ($workorder->getScheduledFrom()->getTimestamp() == $event->getFromDate()->getTimestamp()
            && !empty($isExistEventInWorkorder)
        ) {
            $theEarliestEvent = $this->eventRepository->getTheEarliestFromEvent($workorder, $event);
            $workorder->setScheduledFrom($theEarliestEvent->getFromDate());
            if ($event->getCanFinishWO()) {
                $theOldestEvent = $this->eventRepository->getTheOldestToDateEvent($workorder, $event);
                $theOldestEvent->setCanFinishWO(true);
                $this->objectManager->persist($theOldestEvent);
            }
        }

        if ($workorder->getScheduledTo()->getTimestamp() == $event->getToDate()->getTimestamp()
            && !empty($isExistEventInWorkorder)
        ) {
            if ($event->getCanFinishWO()) {
                $theOldestEvent = $this->eventRepository->getTheOldestToDateEvent($workorder, $event);
                $workorder->setScheduledTo($theOldestEvent->getToDate());
                $theOldestEvent->setCanFinishWO(true);
                $this->objectManager->persist($theOldestEvent);
            }
        }

        return [
            'isExistEventInWorkorder' => $isExistEventInWorkorder,
            'theEarliestEvent' => $theEarliestEvent,
            'theOldestEvent' => $theOldestEvent
        ];
    }

    /**
     * @param Workorder $workorder
     * @param string $status
     * @param int|null $isNotExist
     * @return bool
     */
    public function setStatusIfEventNotExist(Workorder $workorder, $status, $isNotExist = null)
    {
        $isChangedStatus = false;
        if (empty($isNotExist)) {
            $workorderStatus = $this->workorderStatusRepository->findOneBy(
                ['alias' => $status]
            );
            $workorder->setStatus($workorderStatus);
            $workorder->setScheduledFrom(null);
            $workorder->setScheduledTo(null);
            $isChangedStatus = true;
        }

        return $isChangedStatus;
    }

    /**
     * @param Workorder $workorder
     * @param $status
     * @param null $isNotExist
     * @return bool
     */
    public function setStatusIfEventByTodayNotExist(Workorder $workorder, $status, $isNotExist = null)
    {
        $isChangedStatus = false;
        if (empty($this->eventRepository->getEventByToday($workorder)) && !empty($isNotExist)) {
            $workorderStatus = $this->workorderStatusRepository->findOneBy(
                ['alias' => $status]
            );
            $workorder->setStatus($workorderStatus);
            $isChangedStatus = true;
        }

        return $isChangedStatus;
    }

    /**
     * @param $repairsNamed
     * @param Device $device
     * @return array
     */
    public function addRepairs($repairsNamed, Device $device)
    {
        $servicesArray = array();

        /** @var ServiceNamed $repairNamed */
        foreach ($repairsNamed as $repairNamed) {
            $service = $this->serviceManager->getDiscardAndResolvedService($device, $repairNamed);
            if (empty($service)) {
                $service = $this->serviceCreatorService->createNonFrequencyByServiceName($device, $repairNamed);
            }
            $service = $this->serviceManager->setStatus($service, 'repair_opportunity');
            $service->setInspectionDue(new \DateTime());
            $this->serviceManager->update($service);

            $servicesArray[] = $service;
        }

        return $servicesArray;
    }


    /**
     * @param $repairNamed
     * @param Device $device
     * @return \AppBundle\Entity\Service|null|object
     */
    public function addRepair($repairNamed, Device $device)
    {
        $service = $this->serviceManager->getOmitedService($device, $repairNamed);
        if (empty($service)) {
            $service = $this->serviceCreatorService->createNonFrequencyByServiceName($device, $repairNamed);
        }
        $service = $this->serviceManager->setStatus($service, 'repair_opportunity');
        $service->setInspectionDue(new \DateTime());
        $this->serviceManager->update($service);

        return $service;
    }

    /**
     * @param $params
     * @param $route
     * @return Workorder|bool
     * @throws \Exception
     */
    public function rescheduleByParams($params, $route)
    {
        /** @var Workorder $workorder */
        $workorder = $this->workorderRepository->find($params["orderID"]);

        $this->checkingForReschedule($workorder);
        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]
            );

            return false;
        }

        $this->reschedule($workorder, $params['comment']);
        $this->objectManager->persist($workorder);
        $this->objectManager->flush();

        return $workorder;
    }

    /**
     * @param Workorder $workorder
     * @return \DateTime
     */
    public function getFrozenDate(Workorder $workorder)
    {
        $result = new \DateTime();

        if (!empty($workorder->getFrozenContent()->count())) {
            /** @var WorkorderFrozenContent $lastFrozenItem */
            $lastFrozenItem = $workorder->getFrozenContent()->last();
            $result = $lastFrozenItem->getDateCreate();
        }

        return $result;
    }

    /**
     * @param Workorder|null $workorder
     */
    private function checkingForReschedule(?Workorder $workorder)
    {
        $this->errorMessages = [];

        if (is_null($workorder) or is_bool($workorder)) {
            $this->errorMessages[] = "Workorder not found";
        }

        if ($workorder instanceof Workorder) {
            $workorderCurrentStatus = $workorder->getStatus();

            if (!in_array($workorderCurrentStatus->getAlias(), self::VALID_STATUSES_FOR_RESCHEDULE)) {
                $this->errorMessages[] = "Workorder with current status can not be reschedule. 
                Workorder current status: " . $workorderCurrentStatus->getName();
            }
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages =
                array_merge(["<b>Workorder can not receive reschedule request</b>.<br>"], $this->errorMessages);
        }
    }

    /**
     * @param $workorder
     * @param $comment
     * @throws \Exception
     */
    private function reschedule(Workorder $workorder, $comment)
    {
        $workorderNewStatus = $this->workorderStatusRepository->findOneBy(['alias' => self::STATUS_RESCHEDULE]);
        /** @var WorkorderStatus $workorderCurrentStatus */
        $workorderCurrentStatus = $workorder->getStatus();

        if ($workorderNewStatus->getAlias() === self::STATUS_RESCHEDULE and
            !in_array($workorderCurrentStatus->getAlias(), [
                self::STATUS_PENDING_PAYMENT,
                self::STATUS_SEND_REPORTS,
                self::STATUS_DELETE,
                self::STATUS_DONE
            ])) {
            $workorder->setStatus($workorderNewStatus);
        }
        /** @var LogMessage $logMessage */
        $logMessage = $this->container->get('workorder.log_message');
        /** @var LogCreator $woLogCreator */
        $woLogCreator = $this->container->get('workorder.log_creator');
        $woLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_RESCHEDULE,
            'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_RESCHEDULE),
            'comment' => $comment,
            'changingStatus' => true
        ]);
    }
}
