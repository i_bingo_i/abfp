<?php

namespace WorkorderBundle\Services;

use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Letter;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\LetterRepository;
use AppBundle\Entity\Repository\StepNamedRepository;
use AppBundle\Entity\Repository\StepStatusRepository;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use AppBundle\Entity\StepNamed;
use AppBundle\Entity\StepStatus;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Entity\WorkorderStatus;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Services\Exeptions\LogRecordTypeCantBeNull;
use WorkorderBundle\Validators\AvailableStatus;

class AfterSendInvoiceChecker
{
    private const STEP_NAME_SEND_INVOICE_ALIAS = "send_invoice";
    private const STEP_NAME_SEND_REPORT_TO_CLIENT_ALIAS = "send_reports_to_client";
    private const STEP_STATUS_DONE_ALIAS = "done";
    private const PIECE_OF_REPORT_PATH = "merged_reports";
    private const SEND_REPORT_TO_CLIENT_STATUS_ALIAS = "send_reports_to_client";
    private const SEND_REPORT_TO_AHG_STATUS_ALIAS = "send_reports_to_ahj";

    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var  StepNamedRepository*/
    private $stepNamedRepository;
    /** @var StepStatusRepository */
    private $stepStatusRepository;
    /** @var StepCreator */
    private $stepCreator;
    /** @var LetterRepository */
    private $letterRepository;
    /** @var AccountContactPersonRepository */
    private $acpRepository;
    /** @var AvailableStatus  */
    private $availableStatusService;
    /** @var WorkorderManager  */
    private $woManager;
    /** @var WorkorderStatusRepository */
    private $woStatusRepository;
    /** @var LogCreator  */
    private $woLogCreator;
    /** @var LogMessage  */
    private $logMessage;
    /** @var Session  */
    private $session;

    /**
     * AfterSendInvoiceChecker constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->stepCreator = $this->container->get("workorder.step_creator.service");

        $this->stepNamedRepository = $this->objectManager->getRepository("AppBundle:StepNamed");
        $this->stepStatusRepository = $this->objectManager->getRepository("AppBundle:StepStatus");
        $this->letterRepository = $this->objectManager->getRepository("AppBundle:Letter");
        $this->acpRepository = $this->objectManager->getRepository("AppBundle:AccountContactPerson");
        $this->availableStatusService = $this->container->get('workorder.available_status.validator');
        $this->woManager = $this->container->get('app.work_order.manager');
        $this->woLogCreator = $this->container->get('workorder.log_creator');
        $this->logMessage = $this->container->get('workorder.log_message');
        $this->session = $this->container->get('session');

        $this->woStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");

    }

    /**
     * @param SendFormViaEmailDTO $sendFormViaEmailDTO
     * @return bool
     */
    public function addStepInvoiceWasSend(SendFormViaEmailDTO $sendFormViaEmailDTO)
    {
        /** @var StepStatus $stepStatusDone */
        $stepStatusDone = $this->stepStatusRepository->findOneBy(["alias" => self::STEP_STATUS_DONE_ALIAS]);
        /** @var StepNamed $stepNameSendInvoice */
        $stepNameSendInvoice = $this->stepNamedRepository->findOneBy(["alias" => self::STEP_NAME_SEND_INVOICE_ALIAS]);

        if ($this->stepCreator->create($sendFormViaEmailDTO->getWorkorder(), $stepNameSendInvoice, $stepStatusDone)) {
            return true;
        }

        return false;
    }

    /**
     * @param SendFormViaEmailDTO $sendFormViaEmailDTO
     * @return bool
     */
    public function addStepReportsToClintWereSend(SendFormViaEmailDTO $sendFormViaEmailDTO)
    {
        /** @var StepStatus $stepStatusDone */
        $stepStatusDone = $this->stepStatusRepository->findOneBy(["alias" => self::STEP_STATUS_DONE_ALIAS]);
        /** @var StepNamed $stepNameSendReportsToClient */
        $stepNameSendReportsToClient = $this->stepNamedRepository->findOneBy(["alias" => self::STEP_NAME_SEND_REPORT_TO_CLIENT_ALIAS]);

        if ($this->stepCreator->create(
            $sendFormViaEmailDTO->getWorkorder(),
            $stepNameSendReportsToClient,
            $stepStatusDone)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param SendFormViaEmailDTO $sendFormViaEmailDTO
     * @return Letter|bool
     */
    public function checkIfReportsWasSent(SendFormViaEmailDTO $sendFormViaEmailDTO)
    {
        /** @var Letter $result */
        $result = $this->letterRepository->checkIfNeedleTypeWasSent(
            $sendFormViaEmailDTO->getWorkorder(),
            self::PIECE_OF_REPORT_PATH
        );

        if ($result instanceof Letter) {

            return $result;
        }

        return false;
    }

    /**
     * @param SendFormViaEmailDTO $sendFormViaEmailDTO
     * @return bool
     */
    public function checkIfSendReportsAndAddStep(SendFormViaEmailDTO $sendFormViaEmailDTO)
    {
        $letter = $this->checkIfReportsWasSent($sendFormViaEmailDTO);
        if ($this->checkIfReportsWasSent($sendFormViaEmailDTO)) {
            $this->addStepReportsToClintWereSend($sendFormViaEmailDTO);

            return true;
        }

        return false;
    }


    public function checkIfReportsWereSendWithInvoice(SendFormViaEmailDTO $sendFormViaEmailDTO)
    {
        /** @var AccountContactPerson $result */
        $result = $this->acpRepository->ifPaymentHasAuthorizerResponsibilityByDivisionForAccount(
            $sendFormViaEmailDTO->getWorkorder()->getAccount(),
            $sendFormViaEmailDTO->getWorkorder()->getDivision()
        );
        /** First step of check */
        if ($result instanceof AccountContactPerson) {
            if ($this->checkIfSendReportsAndAddStep($sendFormViaEmailDTO)) {
                /** proceed to "Send Reports To AHJ" status of Workoder */
                $this->checkStatusAndSetNewByAlias(
                    $sendFormViaEmailDTO->getWorkorder(),
                    self::SEND_REPORT_TO_AHG_STATUS_ALIAS
                );
                /** add message "Reports were sent to Client along with Invoice" */
                $this->createLog(
                    $sendFormViaEmailDTO->getWorkorder(),
                    WorkorderLogType::TYPE_REPORTS_SENT_ALONG_WITH_INVOICE_VIA_MAIL_OR_FAX
                );
                /** add message "Please send Reports to AHJ." */
                $this->createLog(
                    $sendFormViaEmailDTO->getWorkorder(),
                    WorkorderLogType::TYPE_REPORTS_SEND_TO_AHJ_VIA_MAIL_OR_FAX
                );
            } else {
                /** proceed to "Send Reports To CLIENT" status */
                $this->checkStatusAndSetNewByAlias(
                    $sendFormViaEmailDTO->getWorkorder(),
                    self::SEND_REPORT_TO_CLIENT_STATUS_ALIAS
                );
                /** add message "Please send Reports to Account Authorizer" */
                $this->createLog(
                    $sendFormViaEmailDTO->getWorkorder(),
                    WorkorderLogType::TYPE_SEND_REPORTS_TO_ACCOUNT_AUTHORIZER
                );
            }
        } else {
            /** proceed to "Send Reports To CLIENT" status */
            $this->checkStatusAndSetNewByAlias(
                $sendFormViaEmailDTO->getWorkorder(),
                self::SEND_REPORT_TO_CLIENT_STATUS_ALIAS
            );
            /** add message "Please send Reports to Account Authorizer" */
            $this->createLog(
                $sendFormViaEmailDTO->getWorkorder(),
                WorkorderLogType::TYPE_SEND_REPORTS_TO_ACCOUNT_AUTHORIZER
            );
        }

    }

    //TODO: Replace this method from this service to LogCreator

    /**
     * @param Workorder $workorder
     * @param string $workorderLogTypeAlias
     * @param null $letter
     */
    public function createLog(Workorder $workorder, string $workorderLogTypeAlias, $letter = null)
    {
        try {
            $this->woLogCreator->createRecord($workorder, [
                'type' => $workorderLogTypeAlias,
                'message' => $this->logMessage->makeByLogType(
                    $workorderLogTypeAlias
                ),
                'changingStatus' => false,
                'letter' => $letter
            ]);
        } catch (LogRecordTypeCantBeNull $exception) {
            $this->session->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    //TODO: Replace this method from this service to WorkorderService
    /**
     * @param Workorder $workorder
     * @param $newStatusAlias
     */
    private function checkStatusAndSetNewByAlias(Workorder $workorder, $newStatusAlias)
    {
        /** @var WorkorderStatus $newWorkorderStatus */
        $newWorkorderStatus = $this->woStatusRepository->findOneBy(["alias" => $newStatusAlias]);

        if ($this->availableStatusService->canChangeStatus($workorder->getStatus(), $newWorkorderStatus)) {
            $workorder->setStatus($newWorkorderStatus);
            $this->woManager->update($workorder);
        }
    }
}