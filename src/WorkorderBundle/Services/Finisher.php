<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\WorkorderStatus;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\WorkorderLogType;
use WorkorderBundle\DTO\FinishRequestDTO;
use WorkorderBundle\Services\Exeptions\LogRecordTypeCantBeNull;
use WorkorderBundle\Validators\AvailableStatus;

class Finisher
{
    const FINISHED_STATUS_ALIAS = 'verify_job_results';

    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $container;

    /** @var WorkorderManager */
    private $workorderManager;
    /** @var LogCreator */
    private $woLogCreator;
    /** @var LogMessage $logMessage */
    private $logMessage;

    /** @var WorkorderStatusRepository */
    private $workorderStatusRepository;
    /** @var WorkorderStatus */
    private $finishedStatus;
    /** @var Session */
    private $session;
    /** @var AvailableStatus */
    private $availableStatusService;

    /**
     * Finisher constructor.
     *
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->workorderManager = $this->container->get('app.work_order.manager');
        $this->woLogCreator = $this->container->get('workorder.log_creator');
        $this->logMessage = $this->container->get('workorder.log_message');

        $this->workorderStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");
        $this->finishedStatus = $this->workorderStatusRepository->findOneBy(['alias' => self::FINISHED_STATUS_ALIAS]);
        $this->session = $this->container->get('session');
        $this->availableStatusService = $this->container->get('workorder.available_status.validator');
    }

    /**
     * @param FinishRequestDTO $finishRequest
     */
    public function finish(FinishRequestDTO $finishRequest)
    {
        /** @var Workorder $workorder */
        $workorder = $finishRequest->workorder;
        $workorder->setFinishTime(new \DateTime());
        if ($this->availableStatusService->canChangeStatus($workorder->getStatus(), $this->finishedStatus)) {
            $workorder->setStatus($this->finishedStatus);
        }

        $this->workorderManager->update($workorder);

        try {
            $this->woLogCreator->createRecord($workorder, [
                'type' => WorkorderLogType::TYPE_VERIFY_JOB_RESULTS,
                'message' => $this->logMessage->makeByLogType(WorkorderLogType::TYPE_VERIFY_JOB_RESULTS),
                'changingStatus' => true,
                'comment' => $finishRequest->comment
            ]);
        } catch (LogRecordTypeCantBeNull $exception) {
            $this->session->getFlashBag()->add('error', $exception->getMessage());
        }
    }
}
