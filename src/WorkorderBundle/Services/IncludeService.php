<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\WorkorderLogType;

use Doctrine\Common\Persistence\ObjectManager;
use ServiceBundle\Services\ServiceCreatorService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class IncludeService
{
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var ObjectManager */
    private $objectManager;

    /** @var WorkorderManager */
    private $workorderManager;
    /** @var DeviceInfoService */
    private $workorderDeviceInfoService;
    /** @var CalculateTotals */
    private $workorderCalculateTotalService;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var object|Creator  */
    private $workorderCreator;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var \AppBundle\Services\RepairDeviceInfo\Creator */
    private $creatorRepairDeviceInfoService;
    /** @var LogMessage */
    private $logMessage;
    /** @var LogCreator */
    private $woLogCreator;
    /** @var ServiceCreatorService */
    private $serviceCreatorService;

    /**
     * IncludeService constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer, ObjectManager $objectManager)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->workorderManager = $this->serviceContainer->get('app.work_order.manager');
        $this->workorderDeviceInfoService = $this->serviceContainer->get('workorder.device_info.service');
        $this->workorderCalculateTotalService = $this->serviceContainer->get('workorder.calculate_totals.service');
        $this->workorderRepository = $this->objectManager->getRepository("AppBundle:Workorder");
        $this->workorderCreator = $this->serviceContainer->get('workorder.creator.service');
        $this->serviceManager = $this->serviceContainer->get('app.service.manager');
        $this->creatorRepairDeviceInfoService = $this->serviceContainer->get('app.device_info_creator.service');
        $this->logMessage = $this->serviceContainer->get('workorder.log_message');
        $this->woLogCreator = $this->serviceContainer->get('workorder.log_creator');
        $this->serviceCreatorService = $this->serviceContainer->get('service.creator.service');
    }

    /**
     * @param Device $device
     * @param array $services
     * @param RepairDeviceInfo $repairDeviceInfo
     * @param Workorder $workorder
     * @param bool $fromAPI
     * @return Workorder
     */
    public function addServices(
        Device $device,
        array $services,
        RepairDeviceInfo $repairDeviceInfo,
        Workorder $workorder,
        $fromAPI = false
    ) {
        $this->creatorRepairDeviceInfoService->updateInfoForRepairServicesWithoutProposal(
            $repairDeviceInfo,
            $device,
            $services
        );

        $workorder = $this->workorderManager->addUnderOpportunityServices($services, $workorder, $fromAPI);
        $this->workorderDeviceInfoService->setFromRepairDeviceInfoWithoutProposal($workorder, $repairDeviceInfo);

        $workorder = $this->setWorkorderTotals($workorder, $repairDeviceInfo);
        $this->workorderManager->save($workorder);

        return $workorder;
    }

    /**
     * @param Workorder $workorder
     * @param array $services
     * @throws \Exception
     */
    public function creatingRepairServicesLogs(Workorder $workorder, array $services)
    {
        $this->woLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_SERVICE_WERE_ADDED,
            'message' => $this->logMessage->makeByRepairs($services)
        ]);
    }

    /**
     * @param Workorder $workorder
     * @param Service $service
     * @throws \Exception
     */
    public function creatingInspectionLogs(Workorder $workorder, Service $service)
    {
        $this->woLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_SERVICE_WERE_ADDED,
            'message' => $this->logMessage->makeByService($service)
        ]);
    }

    public function addRepairs($repairsNamed, Device $device)
    {
        $servicesArray = array();

        /** @var ServiceNamed $repairNamed */
        foreach ($repairsNamed as $repairNamed) {
            $service = $this->serviceManager->getDiscardAndResolvedService($device, $repairNamed);
            if (empty($service)) {
                $service = $this->serviceCreatorService->createNonFrequencyByServiceName($device, $repairNamed);
            }
            $service = $this->serviceManager->setStatus($service, 'repair_opportunity');
            $service->setInspectionDue(new \DateTime());
            $this->serviceManager->update($service);

            $servicesArray[] = $service;
        }

        return $servicesArray;
    }

    /**
     * @param array $addedServices
     * @param array $addedUnderOportunityServices
     * @return array
     */
    public function mergingServicesNamed(array $addedServices, array $addedUnderOportunityServices)
    {
        /** @var Service $service */
        foreach ($addedUnderOportunityServices as $service) {
            $addedServices[] = $service;
        }

        return $addedServices;
    }

    /**
     * @param Workorder $workorder
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return Workorder
     */
    private function setWorkorderTotals(Workorder $workorder, RepairDeviceInfo $repairDeviceInfo)
    {
        // Set Service Total
        $woTotalServiceFee =
            $this->workorderCalculateTotalService->calculateServiceTotalFeeByDeviceInfo($workorder, $repairDeviceInfo);
        $workorder->setTotalServiceFee($woTotalServiceFee);

        //Set Total estimated time for Inspections
        $woTotalEstimationTimeInspections =
            $this->workorderCalculateTotalService->calculateTotalEstimatedTimeInspections($workorder);
        $workorder->setTotalEstimatedTimeInspections($woTotalEstimationTimeInspections);

        //Set Total estimated time for Repairs
        $woDeviceInfoTotalEstimationTimeRepairs =
            $this->workorderCalculateTotalService->calculateTotalEstimationTimeRepairs($workorder);
        $workorder->setTotalEstimatedTimeRepairs($woDeviceInfoTotalEstimationTimeRepairs);

        //Set Total repairs price
        $woTotalRepairsPrice = $this->workorderCalculateTotalService->calculateTotalRepairsPrice($workorder);
        $workorder->setTotalRepairsPrice($woTotalRepairsPrice);

        //Set Total Municipality and Processing Fees
        $woTotalMunAndProcFee =
            $this->workorderCalculateTotalService->calculateTotalMunicipalityAndProcessingFee($workorder);
        $workorder->setTotalMunicAndProcFee($woTotalMunAndProcFee);

        //Set totals services count
        $woServicesCount = $this->workorderCalculateTotalService->calculateServicesCount($workorder);
        $workorder->setTotalInspectionsCount($woServicesCount['countInspections']);
        $workorder->setTotalRepairsCount($woServicesCount['countRepairServices']);

        //Set Total Price
        $woTotalPrice = $this->workorderCalculateTotalService->calculateTotalPrice($workorder);
        $workorder->setTotalPrice($woTotalPrice);

        // Set Grand Total
        $woGrandTotal = $this->workorderCalculateTotalService->calculateGrandTotal($workorder);
        $workorder->setGrandTotal($woGrandTotal);

        //Set isDepartmentChannelWithFeesWillVary
        $this->workorderCalculateTotalService->setWODepartmentChannelWithFeesWillVary($workorder);

        return $workorder;
    }
}
