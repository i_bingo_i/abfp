<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Device;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\RepairDeviceInfo as RepairDeviceInfoRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\WorkorderDeviceInfo as WorkorderDeviceInfoRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderDeviceInfo;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ServiceBundle\Services\ServiceHelper;
use ServiceBundle\Services\CalculateTotals;

class DeviceInfoService
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var WorkorderDeviceInfoRepository */
    private $workorderDeviceInfoRepository;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var RepairDeviceInfoRepository */
    private $repairDeviceInfoRepository;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var ContainerInterface */
    private $container;
    /** @var ServiceHelper */
    private $serviceHelperService;
    /** @var CalculateTotals */
    private $serviceCalculateTotals;

    /**
     * DeviceInfoService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->workorderDeviceInfoRepository = $this->objectManager->getRepository(
            'AppBundle:WorkorderDeviceInfo'
        );
        $this->deviceRepository = $this->objectManager->getRepository('AppBundle:Device');
        $this->repairDeviceInfoRepository = $this->objectManager->getRepository('AppBundle:RepairDeviceInfo');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->serviceHelperService = $this->container->get('service.service_helper.service');
        $this->serviceCalculateTotals = $this->container->get('service.calculate_totals.service');
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @param Device $device
     * @param RepairDeviceInfo $deviceInfo
     * @return WorkorderDeviceInfo
     */
    public function create(Workorder $workorder, Proposal $proposal, Device $device, RepairDeviceInfo $deviceInfo)
    {
        $ifGlueRepairDeviceInfo = $deviceInfo->getIsGlued();

        $workorderDeviceInfo = new WorkorderDeviceInfo();
        $workorderDeviceInfo->setWorkorder($workorder);
        $workorderDeviceInfo->setDevice($device);
        $workorderDeviceInfo->setRepairSpecialNotification(
            $this->setRepairField($proposal, $deviceInfo->getSpecialNotification())
        );
        $workorderDeviceInfo->setRepairComments($this->setRepairField($proposal, $deviceInfo->getComments()));
        $workorderDeviceInfo->setRepairEstimationTime(
            $this->setRepairField($proposal, $deviceInfo->getEstimationTime())
        );

        if ($ifGlueRepairDeviceInfo) {
            $workorderDeviceInfo = $this->createFieldFromGlueRepairDeviceInfo(
                $workorderDeviceInfo,
                $proposal,
                $deviceInfo
            );
        } else {
            $workorderDeviceInfo = $this->createFieldFromUnGlueRepairDeviceInfo(
                $workorderDeviceInfo,
                $proposal,
                $deviceInfo
            );
        }

        $workorderDeviceInfo->setRepairPartsAndLabourPrice($this->setRepairField($proposal, $deviceInfo->getPrice()));
        $this->calculateWorkorderInfoFeeAndDeviceTotal($workorderDeviceInfo);

        $this->objectManager->persist($workorderDeviceInfo);
        $this->objectManager->flush();

        return $workorderDeviceInfo;
    }

    /**
     * @param Workorder $workorder
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return WorkorderDeviceInfo
     */
    public function createFromRepairDeviceInfoWithoutProposal(Workorder $workorder, RepairDeviceInfo $repairDeviceInfo)
    {
        /** @var Device $device */
        $device = $repairDeviceInfo->getDevice();

        $workorderDeviceInfo = new WorkorderDeviceInfo();
        $workorderDeviceInfo->setWorkorder($workorder);
        $workorderDeviceInfo->setDevice($device);
        $workorderDeviceInfo->setRepairSpecialNotification($repairDeviceInfo->getSpecialNotification());
        $workorderDeviceInfo->setRepairComments($repairDeviceInfo->getComments());
        $workorderDeviceInfo->setRepairEstimationTime($repairDeviceInfo->getEstimationTime());

        $workorderDeviceInfo->setInspectionEstimationTime($repairDeviceInfo->getInspectionEstimationTime());
        $workorderDeviceInfo->setSubtotalInspectionsFee($repairDeviceInfo->getInspectionPrice());

        $workorderDeviceInfo->setRepairPartsAndLabourPrice($repairDeviceInfo->getPrice());
        $this->calculateWorkorderInfoFeeAndDeviceTotal($workorderDeviceInfo);

        $this->objectManager->persist($workorderDeviceInfo);
        $this->objectManager->flush();

        return $workorderDeviceInfo;
    }

    /**
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     * @param Proposal $proposal
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return WorkorderDeviceInfo
     */
    private function createFieldFromGlueRepairDeviceInfo(
        WorkorderDeviceInfo $workorderDeviceInfo,
        Proposal $proposal,
        RepairDeviceInfo $repairDeviceInfo
    ) {
        $workorderDeviceInfo->setInspectionEstimationTime(
            $this->setInspectionField($proposal, $repairDeviceInfo->getInspectionEstimationTime(), true)
        );
        $workorderDeviceInfo->setSubtotalInspectionsFee(
            $this->setInspectionField($proposal, $repairDeviceInfo->getInspectionPrice(), true)
        );

        return $workorderDeviceInfo;
    }

    /**
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     * @param Proposal $proposal
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return WorkorderDeviceInfo
     */
    private function createFieldFromUnGlueRepairDeviceInfo(
        WorkorderDeviceInfo $workorderDeviceInfo,
        Proposal $proposal,
        RepairDeviceInfo $repairDeviceInfo
    ) {
        $workorderDeviceInfo->setInspectionEstimationTime(
            $this->setInspectionField($proposal, $repairDeviceInfo->getEstimationTime())
        );
        $workorderDeviceInfo->setSubtotalInspectionsFee(
            $this->setInspectionField($proposal, $repairDeviceInfo->getPrice())
        );

        return $workorderDeviceInfo;
    }

    /**
     * @param Proposal $proposal
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     * @param RepairDeviceInfo $repairDeviceInfo
     */
    private function update(
        Proposal $proposal,
        WorkorderDeviceInfo $workorderDeviceInfo,
        RepairDeviceInfo $repairDeviceInfo
    ) {
        $ifGlueRepairDeviceInfo = $repairDeviceInfo->getIsGlued();

        $workorderDeviceInfo->setRepairSpecialNotification(
            $this->addToRepairFields(
                $proposal,
                $workorderDeviceInfo->getRepairSpecialNotification(),
                $repairDeviceInfo->getSpecialNotification()
            )
        );

        $workorderDeviceInfo->setRepairComments(
            $this->addToRepairFields(
                $proposal,
                $workorderDeviceInfo->getRepairComments(),
                $repairDeviceInfo->getComments()
            )
        );

        $workorderDeviceInfo->setRepairEstimationTime(
            $this->addToRepairFields(
                $proposal,
                $workorderDeviceInfo->getRepairEstimationTime(),
                $repairDeviceInfo->getEstimationTime()
            )
        );

        if ($ifGlueRepairDeviceInfo) {
            $workorderDeviceInfo = $this->updateFieldFromGlueRepairDeviceInfo(
                $workorderDeviceInfo,
                $proposal,
                $repairDeviceInfo
            );
        } else {
            $workorderDeviceInfo = $this->updateFieldFromUnGlueRepairDeviceInfo(
                $workorderDeviceInfo,
                $proposal,
                $repairDeviceInfo
            );
        }

        $workorderDeviceInfo->setRepairPartsAndLabourPrice(
            $this->addToRepairFields(
                $proposal,
                $workorderDeviceInfo->getRepairPartsAndLabourPrice(),
                $repairDeviceInfo->getPrice()
            )
        );

        $this->calculateWorkorderInfoFeeAndDeviceTotal($workorderDeviceInfo);

        $this->objectManager->persist($workorderDeviceInfo);
        $this->objectManager->flush();
    }

    /**
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     */
    public function calculateWorkorderInfoFeeAndDeviceTotal(WorkorderDeviceInfo $workorderDeviceInfo)
    {
        $services = $this->getWorkorderDeviceInfoServices($workorderDeviceInfo);
        $ifServicesHaveDepartmentChannelWithFeesWillVary =
            $this->serviceHelperService->checkIfServicesHaveDepartmentChannelWithFeesWillVary($services);

        if ($ifServicesHaveDepartmentChannelWithFeesWillVary) {
            $subtotalMunicAndProcFee = 0;
            $workorderDeviceInfo->setDepartmentChannelWithFeesWillVary(true);
        } else {
            $subtotalMunicAndProcFee = $this->serviceCalculateTotals->calculateSubtotalMunicAndProcFee($services);
            $workorderDeviceInfo->setDepartmentChannelWithFeesWillVary(false);
        }
        $workorderDeviceInfo->setSubtotalMunicAndProcFee($subtotalMunicAndProcFee);
        $workorderDeviceInfo->setDeviceTotal(
            $subtotalMunicAndProcFee +
            $workorderDeviceInfo->getRepairPartsAndLabourPrice() +
            $workorderDeviceInfo->getSubtotalInspectionsFee()
        );
    }

    /**
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     * @param RepairDeviceInfo $repairDeviceInfo
     */
    private function updateFromRepairDeviceInfoWithoutProposal(
        WorkorderDeviceInfo $workorderDeviceInfo,
        RepairDeviceInfo $repairDeviceInfo
    ) {
        $workorderDeviceInfo->setRepairSpecialNotification($repairDeviceInfo->getSpecialNotification());
        $workorderDeviceInfo->setRepairComments($this->setDeviceInfoComment($workorderDeviceInfo, $repairDeviceInfo));
        $workorderDeviceInfo->setRepairEstimationTime(
            $workorderDeviceInfo->getRepairEstimationTime() + $repairDeviceInfo->getEstimationTime()
        );
        $workorderDeviceInfo->setInspectionEstimationTime(
            $workorderDeviceInfo->getInspectionEstimationTime() + $repairDeviceInfo->getInspectionEstimationTime()
        );
        $workorderDeviceInfo->setSubtotalInspectionsFee(
            $workorderDeviceInfo->getSubtotalInspectionsFee() + $repairDeviceInfo->getInspectionPrice()
        );
        $workorderDeviceInfo->setRepairPartsAndLabourPrice(
            $workorderDeviceInfo->getRepairPartsAndLabourPrice() + $repairDeviceInfo->getPrice()
        );

        $this->calculateWorkorderInfoFeeAndDeviceTotal($workorderDeviceInfo);

        $this->objectManager->persist($workorderDeviceInfo);
        $this->objectManager->flush();
    }

    /**
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return string
     */
    private function setDeviceInfoComment(WorkorderDeviceInfo $workorderDeviceInfo, RepairDeviceInfo $repairDeviceInfo)
    {
        $repairDeviceInfoComment = (string)$repairDeviceInfo->getComments();
        $currentWoRepairComment = (string)$workorderDeviceInfo->getRepairComments();

        if ($repairDeviceInfoComment) {
            $comment = empty($currentWoRepairComment) ? $repairDeviceInfoComment : $currentWoRepairComment
                . $repairDeviceInfoComment;

            $currentWoRepairComment = $comment;
        }

        return $currentWoRepairComment;
    }

    /**
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     * @param Proposal $proposal
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return WorkorderDeviceInfo
     */
    private function updateFieldFromGlueRepairDeviceInfo(
        WorkorderDeviceInfo $workorderDeviceInfo,
        Proposal $proposal,
        RepairDeviceInfo $repairDeviceInfo
    ) {
        $workorderDeviceInfo->setInspectionEstimationTime(
            $this->addToInspectionFields(
                $proposal,
                $workorderDeviceInfo->getInspectionEstimationTime(),
                $repairDeviceInfo->getInspectionEstimationTime(),
                true
            )
        );

        $workorderDeviceInfo->setSubtotalInspectionsFee(
            $this->addToInspectionFields(
                $proposal,
                $workorderDeviceInfo->getSubtotalInspectionsFee(),
                $repairDeviceInfo->getInspectionPrice(),
                true
            )
        );

        return $workorderDeviceInfo;
    }

    /**
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     * @param Proposal $proposal
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return WorkorderDeviceInfo
     */
    private function updateFieldFromUnGlueRepairDeviceInfo(
        WorkorderDeviceInfo $workorderDeviceInfo,
        Proposal $proposal,
        RepairDeviceInfo $repairDeviceInfo
    ) {
        $workorderDeviceInfo->setInspectionEstimationTime(
            $this->addToInspectionFields(
                $proposal,
                $workorderDeviceInfo->getInspectionEstimationTime(),
                $repairDeviceInfo->getEstimationTime()
            )
        );

        $workorderDeviceInfo->setSubtotalInspectionsFee(
            $this->addToInspectionFields(
                $proposal,
                $workorderDeviceInfo->getSubtotalInspectionsFee(),
                $repairDeviceInfo->getPrice()
            )
        );

        return $workorderDeviceInfo;
    }

    /**
     * @param Workorder $workOrder
     * @param Proposal $proposal
     */
    public function setWorkorderDeviceInfo(Workorder $workOrder, Proposal $proposal)
    {
        $services = $proposal->getServices();
        $workorderDeviceInfo = null;
        $devices = [];
        /** @var Service $service */
        foreach ($services as $service) {
            $devices[$service->getDevice()->getId()] = $service->getDevice();
        }
        /** @var Device $device */
        foreach ($devices as $device) {
            $workorderDeviceInfo = $this->workorderDeviceInfoRepository->findOneBy(
                ['workorder' => $workOrder, 'device' => $device]
            );

            $repairDeviceInfo = $this->repairDeviceInfoRepository->findOneBy(
                ['proposal' => $proposal, 'device' => $device]
            );

            if (!$workorderDeviceInfo instanceof WorkorderDeviceInfo) {
                $workorderDeviceInfo = $this->create($workOrder, $proposal, $device, $repairDeviceInfo);

                $this->addWorkOrderDeviceInfoToWoCollection($workOrder, $workorderDeviceInfo);
            } else {
                $this->update($proposal, $workorderDeviceInfo, $repairDeviceInfo);
            }
        }
    }

    /**
     * @param Workorder $workorder
     * @param RepairDeviceInfo $repairDeviceInfo
     */
    public function setFromRepairDeviceInfoWithoutProposal(Workorder $workorder, RepairDeviceInfo $repairDeviceInfo)
    {
        /** @var Device $device */
        $device = $repairDeviceInfo->getDevice();
        $workorderDeviceInfo = $this->workorderDeviceInfoRepository->findOneBy(
            ['workorder' => $workorder, 'device' => $device]
        );

        if (!$workorderDeviceInfo instanceof WorkorderDeviceInfo) {
            $workorderDeviceInfo = $this->createFromRepairDeviceInfoWithoutProposal($workorder, $repairDeviceInfo);

            $this->addWorkOrderDeviceInfoToWoCollection($workorder, $workorderDeviceInfo);
        } else {
            $this->updateFromRepairDeviceInfoWithoutProposal($workorderDeviceInfo, $repairDeviceInfo);
        }
    }

    /**
     * @param Workorder $workorder
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     */
    private function addWorkOrderDeviceInfoToWoCollection(
        Workorder $workorder,
        WorkorderDeviceInfo $workorderDeviceInfo
    ) {
        if ($workorder->getWorkorderDeviceInfo()) {
            if (!in_array($workorderDeviceInfo, $workorder->getWorkorderDeviceInfo()->toArray())) {
                $workorder->addWorkorderDeviceInfo($workorderDeviceInfo);
            }
        } else {
            $workorder->addWorkorderDeviceInfo($workorderDeviceInfo);
        }
    }

    /**
     * @param Proposal $proposal
     * @param mixed $deviceInfoField
     * @return mixed
     */
    private function setRepairField(Proposal $proposal, $deviceInfoField = null)
    {
        $result = null;
        if ($proposal->getType()->getAlias() == 'repair' && !empty($deviceInfoField)) {
            $result = $deviceInfoField;
        }

        return $result;
    }

    /**
     * @param Proposal $proposal
     * @param null $deviceInfoField
     * @param bool $ifGluedDeviceInfo
     * @return null
     */
    private function setInspectionField(Proposal $proposal, $deviceInfoField = null, $ifGluedDeviceInfo = false)
    {
        $result = null;
        if (($proposal->getType()->getAlias() == 'retest' && !empty($deviceInfoField))
            or ($ifGluedDeviceInfo && !empty($deviceInfoField))) {
            $result = $deviceInfoField;
        }

        return $result;
    }

    /**
     * @param Proposal $proposal
     * @param $workorderDeviceInfoField
     * @param $deviceInfoField
     * @return int|null|string
     */
    private function addToRepairFields(
        Proposal $proposal,
        $workorderDeviceInfoField,
        $deviceInfoField
    ) {
        $result = $workorderDeviceInfoField ?? null;
        if ($proposal->getType()->getAlias() == 'repair' && !empty($deviceInfoField)) {
            if (is_numeric($deviceInfoField)) {
                $result += $deviceInfoField;
            } elseif (is_string($deviceInfoField)) {
                $result = empty($result) ? $deviceInfoField : $result . $deviceInfoField;
            } elseif (is_object($deviceInfoField)) {
                $result = $deviceInfoField;
            }
        }

        return $result;
    }

    /**
     * @param Proposal $proposal
     * @param $workorderDeviceInfoField
     * @param null $deviceInfoField
     * @param bool $ifGluedDeviceInfo
     * @return int|null|string
     */
    private function addToInspectionFields(
        Proposal $proposal,
        $workorderDeviceInfoField,
        $deviceInfoField = null,
        $ifGluedDeviceInfo = false
    ) {
        $result = $workorderDeviceInfoField ?? null;
        if (($proposal->getType()->getAlias() == 'retest' && !empty($deviceInfoField))
            or ($ifGluedDeviceInfo && !empty($deviceInfoField))) {
            if (is_numeric($deviceInfoField)) {
                $result += $deviceInfoField;
            }
        }

        return $result;
    }

    /**
     * @param WorkorderDeviceInfo $workorderDeviceInfo
     * @return Service[]|array
     */
    private function getWorkorderDeviceInfoServices(WorkorderDeviceInfo $workorderDeviceInfo)
    {
        $services = $this->serviceRepository->findBy(
            ['workorder' => $workorderDeviceInfo->getWorkorder(), 'device' => $workorderDeviceInfo->getDevice()]
        );

        return $services;
    }
}
