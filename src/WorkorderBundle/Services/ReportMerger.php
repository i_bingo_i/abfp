<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\ServiceHistory;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use PDFMerger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AppBundle\Entity\EntityManager\WorkorderManager;
use Symfony\Component\Filesystem\Filesystem;
use Xthiago\PDFVersionConverter\Converter\GhostscriptConverterCommand;
use Xthiago\PDFVersionConverter\Converter\GhostscriptConverter;
use AppBundle\Entity\Repository\ServiceRepository;
use Doctrine\Common\Persistence\ObjectManager;

class ReportMerger
{
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var ObjectManager */
    private $objectManager;

    /** @var TokenStorageInterface */
    private $securityTokenStorage;
    /** @var WorkorderManager */
    private $workorderManager;

    private $reportsPath;
    private $mergedReportsPath;
    private $workordersPath;

    /** @var ServiceRepository */
    private $serviceRepository;

    /**
     * ReportMerger constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $this->serviceContainer->get('app.entity_manager');

        $kernel = $this->serviceContainer->get('kernel');
        $this->workorderManager = $this->serviceContainer->get('app.work_order.manager');

        $this->reportsPath = $kernel->getRootDir()."/../web";
        $this->workordersPath = $kernel->getRootDir()."/../web/uploads/workorders/";
        $this->mergedReportsPath = $kernel->getRootDir()."/../web/uploads/workorders_merged_reports/";
        $this->securityTokenStorage = $this->serviceContainer->get('security.token_storage');
        $this->copyReportsPath = $kernel->getRootDir()."/../web/uploads/copy_reports/";
        $this->uploadsReportPath = $kernel->getRootDir()."/../web/uploads/reports/";
        /** @var ServiceRepository serviceRepository */
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
    }

    /**
     * @param Workorder $workorder
     * @param string $fileName
     * @return string
     * @throws \exception
     */
    public function merge(Workorder $workorder, $fileName = "")
    {
        /** @var PDFMerger $pdfMerger */
        $pdfMerger = new PDFMerger();
        $copiedFiles = array();

        $serviceItems = $this->serviceRepository->getServicesWithReportByWorkorder($workorder);
        if ($workorder->getIsFrozen()) {
            $serviceItems = $this->workorderManager->getServicesHistoryWithReportByFrozenWorkorder($workorder);
        }

        /** @var ServiceHistory $item */
        foreach ($serviceItems as $item) {
            $fullReportPath = $this->reportsPath . $item->getLastReport()->getFileReport();
            //copy original report file to copied folder
            $copiedReportFile = $this->copyReportFileToAnotherFolder($fullReportPath);
            //you need to convert the file to version 1.4 !!!!
            $this->conversionPdfToNewVersion($copiedReportFile, "1.4");
            $pdfMerger->addPDF($copiedReportFile, 'all');

            $copiedFiles[] = $copiedReportFile;
        }

        if (!file_exists("$this->mergedReportsPath")) {
            mkdir("$this->mergedReportsPath", 0777, true);
        }

        if (!$fileName) {
            $mergedReportName = $this->generateReportName($workorder);
        } else {
            $mergedReportName = $fileName;
        }
        $fullMergedReportPath = $this->mergedReportsPath . $mergedReportName;
        $pdfMerger->merge('file', $fullMergedReportPath);

        $this->unlinkCopiedFiles($copiedFiles);

        return $mergedReportName;
    }

    /**
     * @param Workorder $workorder
     * @return string
     */
    private function generateReportName(Workorder $workorder)
    {
        /** @var User $user */
        $user = $this->securityTokenStorage->getToken()->getUser();
        $userLabel = 'User' . $user->getId() . '_' . $user->getFirstName();
        if ($user->getLastName()) {
            $userLabel .= '_' . $user->getLastName();
        }

        $workorderId = $workorder->getId();

        return $userLabel . '_merged_report_' . $workorderId . '.pdf';
    }

    /**
     * @param $fullPdfPath
     * @param $version
     */
    public function conversionPdfToNewVersion($fullPdfPath, $version)
    {
        $command = new GhostscriptConverterCommand();
        $filesystem = new Filesystem();

        $converter = new GhostscriptConverter($command, $filesystem);
        $converter->convert("$fullPdfPath", "$version");
    }

    /**
     * @param string $fullReportPath
     * @return string
     */
    public function copyReportFileToAnotherFolder(string $fullReportPath)
    {
        $copyFileDestination = $this->copyReportsPath .
            $this->getReportFileName($this->getReportFileName($fullReportPath));

        if (!file_exists("$this->copyReportsPath")) {
            mkdir("$this->copyReportsPath", 0777, true);
        }

        copy($fullReportPath, $copyFileDestination);

        return $copyFileDestination;
    }

    /**
     * @param array $filesSource
     * @return bool
     */
    public function unlinkCopiedFiles(array $filesSource)
    {
        foreach ($filesSource as $key => $file) {
            unlink($file);
        }

        return true;
    }

    /**
     * @param string $fileReport
     * @return mixed
     */
    public function getReportFileName(string $fileReport)
    {
        $resultName = str_replace($this->uploadsReportPath, "", $fileReport);

        return $resultName;
    }
}
