<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\PaymentMethod;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Repository\PaymentMethodRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\Repository\WorkorderTypeRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderStatus;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use AppBundle\Entity\WorkorderType;
use Doctrine\Common\Persistence\ObjectManager;
use WorkorderBundle\Factories\WorkorderFactory;

class Creator
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var  WorkorderStatusRepository */
    private $workorderStatusRepository;
    /** @var PaymentMethodRepository */
    private $paymentMethodRepository;
    /** @var WorkorderManager */
    private $workorderManager;
    /** @var WorkorderTypeRepository */
    private $workorderTypeRepository;

    /** @var PaymentMethod|null|object  */
    private $paymentMethodBill;
    /** @var WorkorderStatus|null|object  */
    private $woStatusToBeScheduled;

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param WorkorderManager $workorderManager
     */
    public function __construct(ObjectManager $objectManager, WorkorderManager $workorderManager)
    {
        $this->objectManager = $objectManager;
        $this->workorderManager = $workorderManager;

        $this->workorderStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");
        $this->paymentMethodRepository = $this->objectManager->getRepository("AppBundle:PaymentMethod");
        $this->workorderTypeRepository = $this->objectManager->getRepository('AppBundle:WorkorderType');

        $this->paymentMethodBill =
            $this->paymentMethodRepository->findOneBy(['alias' => $this->workorderManager::PAYMENT_METHOD_BILL]);
        $this->woStatusToBeScheduled =
            $this->workorderStatusRepository->findOneBy(['alias' => $this->workorderManager::STATUS_TO_BE_SCHEDULED]);
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @param User $user
     * @return Workorder
     */
    public function create(Account $account, DeviceCategory $division, User $user)
    {
        /** @var WorkorderType $workorderType */
        $woSimpleType = $this->workorderTypeRepository->findOneBy(['alias' => $this->workorderManager::TYPE_SIMPLE]);

        /** @var Workorder $workorder */
        $workorder = WorkorderFactory::make(
            $account,
            $division,
            $this->paymentMethodBill,
            $this->woStatusToBeScheduled,
            $woSimpleType,
            $user
        );
        $this->workorderManager->save($workorder);

        return $workorder;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @param null $additionalProperties
     * @return Workorder
     */
    public function createGeneric(Account $account, DeviceCategory $division, $additionalProperties = null)
    {
        /** @var WorkorderType $workorderType */
        $woGenericType = $this->workorderTypeRepository->findOneBy(['alias' => $this->workorderManager::TYPE_GENERIC]);
        /** @var User|null $currentUser */
        $user = $additionalProperties['currentUser'] ?? null;
        $file = $additionalProperties['file'] ?? null;
        /** @var Proposal|null $proposal */
        $proposal = $additionalProperties['proposal'] ?? null;
        $internalComment = $proposal && $proposal->getInternalComments() ? $proposal->getInternalComments() : null;

        /** @var Workorder $workorder */
        $workorder = WorkorderFactory::make(
            $account,
            $division,
            $this->paymentMethodBill,
            $this->woStatusToBeScheduled,
            $woGenericType,
            $user
        );
        $workorder->setPdf($file);
        $workorder->setInternalComment($internalComment);
        $this->workorderManager->save($workorder);

        return $workorder;
    }
}
