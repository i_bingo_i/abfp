<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\Repository\StepNamedRepository;
use AppBundle\Entity\Repository\StepStatusRepository;
use AppBundle\Entity\Step;
use AppBundle\Entity\StepNamed;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderStatus;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use AppBundle\Entity\EntityManager\WorkorderManager;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Services\Exeptions\LogRecordTypeCantBeNull;
use AppBundle\Entity\WorkorderLogType;
use WorkorderBundle\Validators\AvailableStatus;

class SendViaMailOrFax
{
    const SENT_INVOICE_STATUS_ALIAS = 'send_reports_to_client';
    const SENT_REPORTS_TO_CLIENT_STATUS_ALIAS = 'send_reports_to_ahj';
    const SENT_REPORTS_TO_AHJ_STATUS_ALIAS = 'pending_payment';

    const WORKORDER_STEP_STATUS_ALIAS_DONE = 'done';
    const WORKORDER_STEP_NAMED_ALIAS_INVOICE_SENT_TO_CLIENT = 'send_reports_to_client';
    const WORKORDER_STEP_NAMED_ALIAS_REPORTS_SENT_TO_CLIENT = 'send_reports_to_ahj';
    const WORKORDER_STEP_NAMED_ALIAS_REPORTS_SENT_TO_AHJ = 'pending_payment';

    /** @var ContainerInterface  */
    private $container;
    /** @var ObjectManager */
    private $objectManager;
    /** @var WorkorderStatusRepository */
    private $workorderStatusRepository;
    /** @var WorkorderManager */
    private $workorderManager;
    /** @var WorkorderStatus|null|object  */
    private $sentInvoiceStatus;
    /** @var WorkorderStatus|null|object  */
    private $sentReportsToClientStatus;
    /** @var WorkorderStatus|null|object  */
    private $sentReportsToAHJStatus;
    /** @var LogCreator */
    private $woLogCreator;
    /** @var LogMessage $logMessage */
    private $logMessage;
    /** @var Session */
    private $session;
    /** @var StepStatusRepository */
    private $stepStatusRepository;
    /** @var StepNamedRepository */
    private $stepNamedRepository;
    /** @var \AppBundle\Entity\StepStatus|null|object  */
    private $woStepDoneStatus;
    /** @var \AppBundle\Entity\StepNamed|null|object  */
    private $woStepNamedStatusInvoiceWasSent;
    /** @var StepCreator */
    private $stepCreator;
    /** @var \AppBundle\Entity\StepNamed|null|object  */
    private $woStepNamedStatusReportsSentToClient;
    /** @var \AppBundle\Entity\StepNamed|null|object  */
    private $woStepNamedStatusReportsSentToAHJ;
    /** @var AvailableStatus */
    private $availableStatusService;

    /**
     * SendViaMailOrFax constructor.
     * @param ContainerInterface $container
     * @param ObjectManager $objectManager
     */
    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->workorderManager = $this->container->get('app.work_order.manager');
        $this->woLogCreator = $this->container->get('workorder.log_creator');
        $this->logMessage = $this->container->get('workorder.log_message');
        $this->session = $this->container->get('session');
        $this->stepCreator = $this->container->get('workorder.step_creator.service');
        $this->availableStatusService = $this->container->get('workorder.available_status.validator');

        $this->workorderStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");
        $this->stepStatusRepository = $this->objectManager->getRepository('AppBundle:StepStatus');
        $this->stepNamedRepository = $this->objectManager->getRepository('AppBundle:StepNamed');

        $this->sentInvoiceStatus =
            $this->workorderStatusRepository->findOneBy(['alias' => self::SENT_INVOICE_STATUS_ALIAS]);
        $this->sentReportsToClientStatus =
            $this->workorderStatusRepository->findOneBy(['alias' => self::SENT_REPORTS_TO_CLIENT_STATUS_ALIAS]);
        $this->sentReportsToAHJStatus =
            $this->workorderStatusRepository->findOneBy(['alias' => self::SENT_REPORTS_TO_AHJ_STATUS_ALIAS]);

        $this->woStepDoneStatus =
            $this->stepStatusRepository->findOneBy(['alias' => self::WORKORDER_STEP_STATUS_ALIAS_DONE]);
        $this->woStepNamedStatusInvoiceWasSent = $this->stepNamedRepository->findOneBy(
            [
                'alias' => self::WORKORDER_STEP_NAMED_ALIAS_INVOICE_SENT_TO_CLIENT
            ]
        );
        $this->woStepNamedStatusReportsSentToClient = $this->stepNamedRepository->findOneBy(
            [
                'alias' => self::WORKORDER_STEP_NAMED_ALIAS_REPORTS_SENT_TO_CLIENT
            ]
        );
        $this->woStepNamedStatusReportsSentToAHJ = $this->stepNamedRepository->findOneBy(
            [
                'alias' => self::WORKORDER_STEP_NAMED_ALIAS_REPORTS_SENT_TO_AHJ
            ]
        );
    }

    /**
     * @param Workorder $workorder
     * @param string $comment
     * @param null $reportsWereSentTo
     */
    public function sendInvoice(Workorder $workorder, string $comment, $reportsWereSentTo = null)
    {
        $this->sendingDocumentsAction(
            $workorder,
            $this->sentInvoiceStatus,
            $this->woStepNamedStatusInvoiceWasSent
        );

        $this->createLogForSendingDocument($workorder, WorkorderLogType::TYPE_INVOICE_SENT_VIA_MAIL_OR_FAX, $comment);

        if (!$reportsWereSentTo) {
            $this->createLogForSendingDocument($workorder, WorkorderLogType::TYPE_SEND_REPORTS_TO_ACCOUNT_AUTHORIZER);
        }
    }

    /**
     * @param Workorder $workorder
     */
    public function sentReportsAlongWithInvoice(Workorder $workorder)
    {
        $this->sendingDocumentsAction(
            $workorder,
            $this->sentReportsToClientStatus,
            $this->woStepNamedStatusReportsSentToClient
        );

        $this->createLogForSendingDocument(
            $workorder,
            WorkorderLogType::TYPE_REPORTS_SENT_ALONG_WITH_INVOICE_VIA_MAIL_OR_FAX
        );

        $this->createLogForSendingDocument($workorder, WorkorderLogType::TYPE_REPORTS_SEND_TO_AHJ_VIA_MAIL_OR_FAX);
    }

    /**
     * @param Workorder $workorder
     * @param string $comment
     */
    public function sentReportsToClient(Workorder $workorder, string $comment)
    {
        $this->sendingDocumentsAction(
            $workorder,
            $this->sentReportsToClientStatus,
            $this->woStepNamedStatusReportsSentToClient
        );

        $this->createLogForSendingDocument(
            $workorder,
            WorkorderLogType::TYPE_REPORTS_SENT_TO_CLIENT_VIA_MAIL_OR_FAX,
            $comment
        );

        $this->createLogForSendingDocument($workorder, WorkorderLogType::TYPE_REPORTS_SEND_TO_AHJ_VIA_MAIL_OR_FAX);
    }

    /**
     * @param Workorder $workorder
     * @param string $comment
     */
    public function sentReportsToAHJ(Workorder $workorder, string $comment)
    {
        $this->sendingDocumentsAction(
            $workorder,
            $this->sentReportsToAHJStatus,
            $this->woStepNamedStatusReportsSentToAHJ
        );

        $this->createLogForSendingDocument(
            $workorder,
            WorkorderLogType::TYPE_REPORTS_SENT_TO_AHJ_VIA_MAIL_OR_FAX,
            $comment
        );

        if (!$this->checkPaymentsInQuickbooks()) {
            $this->createLogForSendingDocument(
                $workorder,
                WorkorderLogType::TYPE_PAYMENTS_NOT_FOUND_IN_QUICKBOOKS
            );
        }
    }

    /**
     * @return bool
     */
    private function checkPaymentsInQuickbooks()
    {
        //TODO: NWL16.1 system shall be triggered to check if there is a Payment in Quickbooks for the last Invoice
        return false;
    }

    /**
     * @param Workorder $workorder
     * @param WorkorderStatus $newStatus
     * @param StepNamed $woStepNamed
     */
    private function sendingDocumentsAction(Workorder $workorder, WorkorderStatus $newStatus, StepNamed $woStepNamed)
    {
        /** @var Step $woStep */
        $woStep = $this->stepCreator->create($workorder, $woStepNamed, $this->woStepDoneStatus);

        $workorder->addStep($woStep);
        if ($this->availableStatusService->canChangeStatus($workorder->getStatus(), $newStatus)) {
            $workorder->setStatus($newStatus);
        }
        $this->workorderManager->update($workorder);
    }

    /**
     * @param Workorder $workorder
     * @param string $workorderLogTypeAlias
     * @param null $comment
     */
    private function createLogForSendingDocument(Workorder $workorder, string $workorderLogTypeAlias, $comment = null)
    {
        try {
            $this->woLogCreator->createRecord($workorder, [
                'type' => $workorderLogTypeAlias,
                'message' => $this->logMessage->makeByLogType(
                    $workorderLogTypeAlias
                ),
                'changingStatus' => true,
                'comment' => $comment
            ]);
        } catch (LogRecordTypeCantBeNull $exception) {
            $this->session->getFlashBag()->add('error', $exception->getMessage());
        }
    }
}
