<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\EntityManager\LetterManager;
use AppBundle\Entity\File;
use AppBundle\Entity\Letter;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\LetterAttachment;
use AppBundle\Services\Files;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Session\Session;

class ComposerFormService
{
    private CONST ATTACHMENT_SAVE_PATH = "/uploads/workorders/attaches/";
    private CONST ROOT_PATH = __DIR__."/../../../web";
    /** @var ContainerInterface */
    private $container;
    /** @var  ObjectManager */
    private $objectManager;
    /** @var Files */
    private $fileService;
    /** @var LetterManager */
    private $letterManager;
    /** @var Session */
    private $session;

    private $attachmentItems = [];

    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->fileService = $this->container->get("app.files");
        $this->letterManager = $this->container->get("app.letter.manager");
        $this->session = $this->container->get('session');
    }

    /**
     * @param $formData
     */
    public function parseFormData($formData)
    {
        if (isset($formData["file"]) and !empty($formData["file"])) {
            $this->parseUploadFiles($formData);
        }

        if (isset($formData["attachedFiles"]) and !empty($formData["attachedFiles"])) {
            $this->parseExistFiles($formData["attachedFiles"]);
        }

        $newLetter = $this->createLetter($formData);
        $this->objectManager->flush();

        $letterResponse = $this->letterManager->sendWorkorderEmail($newLetter, "send_compose_form_to_authorizer.html.twig");

        if ($letterResponse=== true) {
            $this->session->getFlashBag()->add("success", 'Message was sent');
        }

    }

    /**
     * @param $formData
     */
    private function parseUploadFiles($formData)
    {
        $files = $formData["file"];
        /** @var UploadedFile $file */
        foreach ($files as $file) {
            if ($file) {
                $date = (new \DateTime())->format("m_d_Y");
                $prefix = "Workorder_{$formData["workorder"]->getId()}_{$date}";
                $fileName = $this->fileService->getRandomFileName($file, $prefix);
                $filePath = $this->fileService->saveUploadFile($file, self::ATTACHMENT_SAVE_PATH, $fileName);
                $fileSize = $file->getSize();
                $this->createAttachment(self::ATTACHMENT_SAVE_PATH.$filePath, $fileSize);
            }
        }
    }

    /**
     * @param $attachedFiles
     */
    private function parseExistFiles($attachedFiles)
    {
        foreach ($attachedFiles as $filePath) {
            if ($filePath) {
                $this->createAttachment($filePath, $this->fileService->getExistFileSize($filePath));
            }
        }
    }

    /**
     * @param $filePath
     * @param int $fileSize
     */
    private function createAttachment($filePath, $fileSize = 0)
    {
        $newAttach = new LetterAttachment();
        $file = $this->createFileItem($filePath, $fileSize);
        $newAttach->setFile($file);

        $this->objectManager->persist($newAttach);
        $this->attachmentItems[] = $newAttach;
    }

    /**
     * @param $filePath
     * @param int $fileSize
     * @return File
     */
    private function createFileItem($filePath, $fileSize = 0)
    {
        $newFile = new File();
        $newFile->setFile($filePath);
        $newFile->setSize($fileSize);

        $this->objectManager->persist($newFile);

        return $newFile;
    }

    /**
     * @param $formData
     * @return Letter
     */
    private function createLetter($formData) {
        $newLetter = new Letter();
        /** @var LetterAttachment $attachmentItem */
        foreach ($this->attachmentItems as $attachmentItem) {
            $newLetter->addAttachment($attachmentItem);
        }

        $newLetter->setWorkorder($formData["workorder"]);
        $newLetter->setBody($formData["emailBody"]);
        $newLetter->setSubject($formData["emailSubject"]);
        $newLetter->setFrom($formData["from"]);
        $newLetter->setWorkorderTo($formData["to"]);
        $newLetter->setEmails($formData["to"]);

        $this->objectManager->persist($newLetter);

        return $newLetter;
    }

}