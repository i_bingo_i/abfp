<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Files;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Services\TrashRequests\Creator;

class SignatureAttacherService
{
    /** @var ObjectManager  */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var WorkorderManager */
    private $workorderManager;
    /** @var Creator */
    private $trashRequestCreator;
    /** @var AccountContactPerson */
    private $accountContactPersonRepository;

    private $errorMessages = [];

    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->trashRequestCreator =  $this->serviceContainer->get("app.trash_requests.creator.service");
        $this->workorderManager =  $this->serviceContainer->get("app.work_order.manager");

        $this->workorderRepository = $this->objectManager->getRepository("AppBundle:Workorder");
        $this->accountContactPersonRepository = $this->objectManager->getRepository("AppBundle:AccountContactPerson");
    }


    public function attachByParams($params, ?UploadedFile $file, $route)
    {
        $parsedData = $this->parseForAttach($params, $file);
        $this->checkingForAttach($parsedData);
        if (!empty($this->errorMessages)) {
            $fullPath = "";
            if ($file instanceof UploadedFile) {
                /** @var Files $filesService */
                $filesService = $this->serviceContainer->get('app.files');

                $fileName = $filesService->getRandomFileName($file, 'report_');
                $savePath = "/uploads/bad_request_storage/";
                $filesService->saveUploadFile($file, $savePath, $fileName);
                $fullPath = $savePath . $fileName;
            }

            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                $fullPath,
                $params["accessToken"]
            );
            return false;
        }

        return $this->setSignature($parsedData);


    }

    /**
     * @param $data
     * @param null|UploadedFile $file
     * @return array
     */
    private function parseForAttach($data, ?UploadedFile $file = null)
    {
        $parsedData = [];

        $parsedData["workorder"] = null;
        if ($data["orderID"]) {
            $parsedData["workorder"] = $this->workorderRepository->find($data["orderID"]);
        }

        $parsedData["file"] = null;
        if ($file instanceof UploadedFile) {
            $parsedData["file"] = $file;
        }

        $parsedData["contact"] = null;
        if ($data["contactID"]) {
            $parsedData["contact"] = $this->accountContactPersonRepository->find($data["contactID"]);
        }

        return $parsedData;
    }

    /**
     * @param $parsedData
     */
    private function checkingForAttach($parsedData)
    {
        $this->errorMessages = [];

        if (is_null($parsedData["workorder"])) {
            $this->errorMessages[] = "Workorder not found";
        }

        if (is_null($parsedData["file"])) {
            $this->errorMessages[] = "File has not received";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Workorder signature can not be uploaded</b>.<br>"], $this->errorMessages);
        }
    }

    /**
     * @param $parsedData
     * @return Workorder
     */
    private function setSignature($parsedData)
    {
        /** @var Workorder $workorder */
        $workorder = $parsedData["workorder"];

        if (!is_null($parsedData["contact"])) {$workorder->setSignatureOwner($parsedData["contact"]);}

        $filePath = $this->uploadSignature($parsedData["file"]);
        $workorder->setSignature($filePath);
        $this->workorderManager->save($workorder);

        return $workorder;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function uploadSignature(UploadedFile $file)
    {
        $saveImagePath = "/uploads/signatures";
        $filePath = $this->serviceContainer->get('app.images')->saveUploadFile($file, $saveImagePath);

        return $saveImagePath.'/'.$filePath;
    }
}