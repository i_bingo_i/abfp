<?php

namespace WorkorderBundle\Services;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Repository\InvoicesRepository;
use AppBundle\Entity\Repository\ReportRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Files;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WorkorderFilesComposer
{
    private const CHOICES_KEY = "choices";
    /** @var ContainerInterface  */
    private $container;
    /** @var Files  */
    private $fileService;
    private $filesArray =[];
    /** @var InvoicesRepository */
    private $invoicesRepository;
    /** @var ObjectManager */
    private $om;
    /** @var  ReportMerger*/
    private $workorderMergeReportService;
    /** @var ReportRepository */
    private $reportRepository;

    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {

        $this->container = $container;
        $this->om = $objectManager;

        $this->fileService = $this->container->get("app.files");
        $this->workorderMergeReportService = $this->container->get('workorder.report_merger.service');
        $this->invoicesRepository = $this->om->getRepository("InvoiceBundle:Invoices");
        $this->reportRepository = $this->om->getRepository("AppBundle:Report");
    }

    /**
     * @param Workorder $workorder
     * @return array
     * @throws \exception
     */
    public function composeForSendInvoice(Workorder $workorder)
    {
        $this->clearFilesArray();
        $this->composeLastInvoice($workorder);
        /** ******** **/

        $this->composeMergeReport($workorder);

        /** ******** **/
        if ($workorder->getPdf()) {
            $savePath = "/uploads/workorders/";
            $this->filesArray["paths"][] = $this->makePath($workorder->getPdf(), $savePath);
            $fileName = $this->fileService->getFileNameByFullPath($workorder->getPdf());
            $this->createParseItem($savePath, $fileName);

        }

        return $this->filesArray;
    }

    /**
     * @param Workorder $workorder
     * @return array
     * @throws \exception
     */
    public function composeForSendReportsToClients(Workorder $workorder)
    {
        $this->clearFilesArray();
        /** ******** **/
        $this->composeMergeReport($workorder);

        /** ******** **/
        if ($workorder->getPdf()) {
            $savePath = "/uploads/workorders/";
            $this->filesArray["paths"][] = $this->makePath($workorder->getPdf(), $savePath);
            $fileName = $this->fileService->getFileNameByFullPath($workorder->getPdf());
            $this->createParseItem($savePath, $fileName);

        }
        $this->composeLastInvoice($workorder);

        return $this->filesArray;
    }

    /**
     * @param Workorder $workorder
     * @return array
     * @throws \exception
     */
    public function composeForSendReportsToAHJ(Workorder $workorder)
    {
        $this->clearFilesArray();
        /** ******** **/
        $this->composeMergeReport($workorder);

        /** ******** **/

        return $this->filesArray;
    }

    /**
     *
     */
    private function clearFilesArray()
    {
        $this->filesArray =[];
        $this->filesArray[self::CHOICES_KEY] =[];
    }

    /**
     * @param Workorder $workorder
     */
    private function composeLastInvoice(Workorder $workorder)
    {
        $savePath = "/uploads/invoices/";
        /** @var Invoices $invoice */
        $invoice = $this->invoicesRepository->getLastInvoiceForWorkorder($workorder);
        if ($invoice) {
            $this->filesArray["paths"][] = $this->makePath($invoice->getFile(), $savePath);
            $fileName = $this->fileService->getFileNameByFullPath($invoice->getFile());
            $this->createParseItem($savePath, $fileName);
        }

    }

    /**
     * @param Workorder $workorder
     * @throws \exception
     */
    private function composeMergeReport(Workorder $workorder)
    {
        if ($this->getReportsCount($workorder)) {
            $savePath = "/uploads/workorders_merged_reports/";
            $fileReportName = $this->workorderMergeReportService->merge(
                $workorder,
                $this->generateMergedReportsFileName($workorder)
            );

            $this->filesArray["paths"][] = $this->makePath(
                $fileReportName,
                $savePath
            );

            $this->createParseItem($savePath, $fileReportName);
        }
    }


    /**
     * @param Workorder $workorder
     * @return int
     */
    private function getReportsCount(Workorder $workorder)
    {
        return count($this->reportRepository->getWorkOrderReports($workorder));
    }

    /**
     * @param $filename
     * @param $path
     * @return string
     */
    private function makePath($filename, $path)
    {
        if (stristr($filename, "/")!== false or stristr($filename, $path)!== false) {
            return $filename;
        }

        return $path.$filename;
    }

    /**
     * @param $savePath
     * @param $fileName
     * @param string $key
     */
    private function createParseItem($savePath, $fileName, $key = self::CHOICES_KEY)
    {
        $size = $this->fileService->getExistFileSize($savePath.$fileName);
        $size = round($size/(1024*1024), 2);

        $this->filesArray["{$key}"][] = [
            "path" => $savePath.$fileName,
            "size" => $size
        ];
    }

    /**
     * @param Workorder $workorder
     * @return string
     */
    private function generateMergedReportsFileName(Workorder $workorder)
    {
        return "ABFP_{$workorder->getDivision()->getName()}_Workorder_{$workorder->getId()}_Reports.pdf";
    }
}
