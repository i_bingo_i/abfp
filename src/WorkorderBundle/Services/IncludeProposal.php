<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\EntityManager\ProposalLogManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Workorder;
use AppBundle\Services\LogComments;
use AppBundle\Services\Proposal\Freeze;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Entity\Repository\WorkorderRepository;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerInterface;

class IncludeProposal
{
    /** @var ContainerInterface */
    private $serviceContainer;

    /** @var ProposalManager */
    private $proposalManager;
    /** @var WorkorderManager */
    private $workorderManager;
    /** @var object|DeviceInfoService  */
    private $workorderDeviceInfoService;
    /** @var Freeze */
    private $freezeProposal;
    /** @var object|CalculateTotals  */
    private $workorderCalculateTotalService;
    /** @var object|Creator  */
    private $workorderCreator;
    /** @var LogComments */
    private $logComments;
    /** @var ProposalLogManager */
    private $proposalLogManager;
    /** @var LogCreator */
    private $workorderLogCreator;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var ObjectManager */
    private $objectManager;

    /**
     * IncludeProposal constructor.
     * @param ContainerInterface $serviceContainer
     * @param ObjectManager $objectManager
     */
    public function __construct(ContainerInterface $serviceContainer, ObjectManager $objectManager)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->proposalManager = $this->serviceContainer->get('app.proposal.manager');
        $this->workorderManager = $this->serviceContainer->get('app.work_order.manager');
        $this->workorderDeviceInfoService = $this->serviceContainer->get('workorder.device_info.service');
        $this->freezeProposal = $this->serviceContainer->get('app.proposal.freeze.service');
        $this->workorderCalculateTotalService = $this->serviceContainer->get('workorder.calculate_totals.service');
        $this->workorderCreator = $this->serviceContainer->get('workorder.creator.service');
        $this->logComments = $this->serviceContainer->get('app.log.comments');
        $this->proposalLogManager = $this->serviceContainer->get('app.proposal_log.manager');
        $this->workorderLogCreator = $this->serviceContainer->get('workorder.log_creator');
        $this->workorderRepository = $this->objectManager->getRepository("AppBundle:Workorder");
    }

    /**
     * @param Proposal $proposal
     * @param Workorder $workorder
     * @return Workorder
     */
    public function addProposal(Proposal $proposal, Workorder $workorder)
    {
        if (!$proposal->getIsFrozen()) {
            $this->freezeProposal->freezing($proposal);
        }
        $workorder->addProposal($proposal);
        $proposal->setWorkorder($workorder);
        $this->proposalManager->setStatus($proposal, 'workorder_created');

        $workorder = $this->workorderManager->addServices($proposal->getServices(), $workorder);
        $this->workorderDeviceInfoService->setWorkorderDeviceInfo($workorder, $proposal);

        $workorder = $this->setWorkorderTotals($workorder, $proposal);

        $workorder = $this->workorderManager->setNotIncludedItemsFromProposal($workorder, $proposal);

        $this->workorderManager->save($workorder);

        return $workorder;
    }

    /**
     * @param Proposal $proposal
     * @param array $formData
     * @throws \Exception
     */
    public function creatingLogs(Proposal $proposal, array $formData)
    {
        /** @var Workorder $workorder */
        $workorder = $proposal->getWorkorder();
        $proposalTypeAlias = $proposal->getType()->getAlias();
        $comment = $this->logComments->parseByParams($formData);

        //log records
        $proposalLogMailMessages = [
            "retest" => "Retest Notice was added to Workorder",
            "repair" => "Proposal was added to Workorder"
        ];
        $this->proposalLogManager->create(
            $proposal,
            $proposalLogMailMessages["$proposalTypeAlias"],
            $comment
        );

        //create WO Log
        $proposalTitle = 'Retest Notice';
        if ($proposalTypeAlias == 'repair') {
            $proposalTitle = 'Repair Proposal';
        }
        $changingStatus = false;
        if ($workorder->getLogs()->isEmpty()) {
            $changingStatus = true;
        }

        $this->workorderLogCreator->createRecord($workorder, [
            'proposal' => $proposal,
            'changingStatus' => $changingStatus,
            'message' => "$proposalTitle was added to Workorder",
            'comment' => $comment,
            'type' => WorkorderLogType::TYPE_WORKORDER_WAS_CREATED
        ]);
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return Workorder
     */
    private function setWorkorderTotals(Workorder $workorder, Proposal $proposal)
    {
        // Set Service Total
        $woTotalServiceFee = $this->workorderCalculateTotalService->calculateServiceTotalFee($workorder, $proposal);
        $workorder->setTotalServiceFee($woTotalServiceFee);

        //Set Total estimated time for Inspections
        $woTotalEstimationTimeInspections =
            $this->workorderCalculateTotalService->calculateTotalEstimatedTimeInspections($workorder);
        $workorder->setTotalEstimatedTimeInspections($woTotalEstimationTimeInspections);

        //Set Total estimated time for Repairs
        $woDeviceInfoTotalEstimationTimeRepairs =
            $this->workorderCalculateTotalService->calculateTotalEstimationTimeRepairs($workorder);
        $workorder->setTotalEstimatedTimeRepairs($woDeviceInfoTotalEstimationTimeRepairs);

        //Set Total repairs price
        $woTotalRepairsPrice = $this->workorderCalculateTotalService->calculateTotalRepairsPrice($workorder);
        $workorder->setTotalRepairsPrice($woTotalRepairsPrice);

        //Set Total Municipality and Processing Fees
        $woTotalMunAndProcFee =
            $this->workorderCalculateTotalService->calculateTotalMunicipalityAndProcessingFee($workorder);
        $workorder->setTotalMunicAndProcFee($woTotalMunAndProcFee);

        //Set totals services count
        $woServicesCount = $this->workorderCalculateTotalService->calculateServicesCount($workorder);
        $workorder->setTotalInspectionsCount($woServicesCount['countInspections']);
        $workorder->setTotalRepairsCount($woServicesCount['countRepairServices']);

        //Set After Hours Work Cost
        $afterHoursWorkCost =
            $this->workorderCalculateTotalService->calculateAfterHoursWorkCount($workorder, $proposal);
        $workorder->setAfterHoursWorkCost($afterHoursWorkCost);

        //Set Discount
        $sumDiscount = $this->workorderCalculateTotalService->calculateDiscount($workorder, $proposal);
        $workorder->setDiscount($sumDiscount);

        //Set AdditionalComments
        $additionalComments = $this->workorderCalculateTotalService->calculateAdditionalComments($workorder, $proposal);
        $workorder->setAdditionalComments($additionalComments);

        //Additional Cost Comment
        $additionalCostComment = $this->workorderCalculateTotalService->calculateAdditionalCostComments(
            $workorder,
            $proposal
        );
        $workorder->setAdditionalCostComment($additionalCostComment);

        //Set Additional Cost
        $additionalCost = $this->workorderCalculateTotalService->calculateTotalAdditionalCost($workorder, $proposal);
        $workorder->setAdditionalServicesCost($additionalCost);

        //Set Total Price
        $woTotalPrice = $this->workorderCalculateTotalService->calculateTotalPrice($workorder);
        $workorder->setTotalPrice($woTotalPrice);

        // Set Grand Total
        $woGrandTotal = $this->workorderCalculateTotalService->calculateGrandTotal($workorder, $proposal);
        $workorder->setGrandTotal($woGrandTotal);

        // Set Internal Comment
        $additionalComments = $this->workorderCalculateTotalService->calculateInternalComments($workorder, $proposal);
        $workorder->setInternalComment($additionalComments);

        //Set isDepartmentChannelWithFeesWillVary
        $this->workorderCalculateTotalService->setWODepartmentChannelWithFeesWillVary($workorder);

        return $workorder;
    }
}
