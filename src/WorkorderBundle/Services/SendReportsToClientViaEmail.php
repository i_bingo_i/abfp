<?php

namespace WorkorderBundle\Services;

use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Repository\StepNamedRepository;
use AppBundle\Entity\Repository\StepStatusRepository;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use AppBundle\Entity\StepNamed;
use AppBundle\Entity\StepStatus;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Entity\WorkorderStatus;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Services\Exeptions\LogRecordTypeCantBeNull;
use WorkorderBundle\Validators\AvailableStatus;

class SendReportsToClientViaEmail
{
    private const STEP_NAME_SEND_REPORT_TO_CLIENT_ALIAS = "send_reports_to_client";
    private const STEP_STATUS_DONE_ALIAS = "done";
    private const SEND_REPORT_TO_AHG_STATUS_ALIAS = "send_reports_to_ahj";

    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var LogCreator  */
    private $woLogCreator;
    /** @var LogMessage  */
    private $logMessage;
    /** @var Session  */
    private $session;
    /** @var AvailableStatus  */
    private $availableStatusService;
    /** @var WorkorderManager  */
    private $woManager;
    /** @var WorkorderStatusRepository */
    private $woStatusRepository;
    /** @var  StepNamedRepository*/
    private $stepNamedRepository;
    /** @var StepStatusRepository */
    private $stepStatusRepository;
    /** @var StepCreator */
    private $stepCreator;

    /**
     * SendReportsToClientViaEmail constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->stepCreator = $this->container->get("workorder.step_creator.service");

        $this->stepNamedRepository = $this->objectManager->getRepository("AppBundle:StepNamed");
        $this->stepStatusRepository = $this->objectManager->getRepository("AppBundle:StepStatus");
        $this->availableStatusService = $this->container->get('workorder.available_status.validator');
        $this->woManager = $this->container->get('app.work_order.manager');
        $this->woLogCreator = $this->container->get('workorder.log_creator');
        $this->logMessage = $this->container->get('workorder.log_message');
        $this->session = $this->container->get('session');

        $this->woStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");

    }

    /**
     * @param SendFormViaEmailDTO $sendFormViaEmailDTO
     */
    public function sendReportsToClientViaEmailProcess(SendFormViaEmailDTO $sendFormViaEmailDTO)
    {
        $letter = $this->container->get("letter.action.manager")->sendViaEmailForWorkorder($sendFormViaEmailDTO);
        if ($letter) {
            $this->createLog(
                $sendFormViaEmailDTO->getWorkorder(),
                WorkorderLogType::TYPE_REPORTS_SENT_TO_CLIENT_VIA_EMAIL,
                $letter
            );
            $this->addStepReportsToClintWereSent($sendFormViaEmailDTO);

            $this->checkStatusAndSetNewByAlias(
                $sendFormViaEmailDTO->getWorkorder(),
                self::SEND_REPORT_TO_AHG_STATUS_ALIAS
            );

            $this->createLog(
                $sendFormViaEmailDTO->getWorkorder(),
                WorkorderLogType::TYPE_REPORTS_SEND_TO_AHJ_VIA_MAIL_OR_FAX
            );
        }
    }

    /**
     * @param SendFormViaEmailDTO $sendFormViaEmailDTO
     * @return bool
     */
    private function addStepReportsToClintWereSent(SendFormViaEmailDTO $sendFormViaEmailDTO)
    {
        /** @var StepStatus $stepStatusDone */
        $stepStatusDone = $this->stepStatusRepository->findOneBy(["alias" => self::STEP_STATUS_DONE_ALIAS]);
        /** @var StepNamed $stepNameSendReportsToClient */
        $stepNameSendReportsToClient = $this->stepNamedRepository->findOneBy(["alias" => self::STEP_NAME_SEND_REPORT_TO_CLIENT_ALIAS]);

        if ($this->stepCreator->create(
            $sendFormViaEmailDTO->getWorkorder(),
            $stepNameSendReportsToClient,
            $stepStatusDone)
        ) {
            return true;
        }

        return false;
    }

    //TODO: Replace this method from this service to LogCreator
    /**
     * @param Workorder $workorder
     * @param string $workorderLogTypeAlias
     * @param null $comment
     */
    private function createLog(Workorder $workorder, string $workorderLogTypeAlias, $letter = null)
    {
        try {
            $this->woLogCreator->createRecord($workorder, [
                'type' => $workorderLogTypeAlias,
                'message' => $this->logMessage->makeByLogType(
                    $workorderLogTypeAlias
                ),
                'changingStatus' => false,
                'letter' => $letter
            ]);
        } catch (LogRecordTypeCantBeNull $exception) {
            $this->session->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    //TODO: Replace this method from this service to WorkorderService
    /**
     * @param Workorder $workorder
     * @param $newStatusAlias
     */
    private function checkStatusAndSetNewByAlias(Workorder $workorder, $newStatusAlias)
    {
        /** @var WorkorderStatus $newWorkorderStatus */
        $newWorkorderStatus = $this->woStatusRepository->findOneBy(["alias" => $newStatusAlias]);

        if ($this->availableStatusService->canChangeStatus($workorder->getStatus(), $newWorkorderStatus)) {
            $workorder->setStatus($newWorkorderStatus);
            $this->woManager->update($workorder);
        }
    }

}