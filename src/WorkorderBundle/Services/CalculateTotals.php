<?php

namespace WorkorderBundle\Services;

use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Service;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\WorkorderDeviceInfo;
use AppBundle\Entity\Repository\ServiceRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CalculateTotals
{
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var ObjectManager */
    private $objectManager;
    /** @var ServiceRepository */
    private $serviceRepository;

    /**
     * CalculateTotals constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $this->serviceContainer->get('app.entity_manager');

        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
    }

    /**
     * @param Workorder $workorder
     * @param Proposal|null $proposal
     * @return float|int
     */
    public function calculateGrandTotal(Workorder $workorder, Proposal $proposal = null)
    {
//        $woGrandTotal = $workorder->getGrandTotal();
//
//        if ($proposal->getType()->getAlias() == 'retest') {
//            $proposalGT = $proposal->getGrandTotal();
//        } else {
//            $proposalGT = $proposal->getAllGrandTotalPrice();
//        }
//
//        $woGrandTotal += (float) $proposalGT - (float) $workorder->getDiscount();
        $result = 0;
        $result += (float) $workorder->getTotalServiceFee();
        $result += (float) $workorder->getTotalRepairsPrice();
        $result += (float) $workorder->getTotalMunicAndProcFee();
        $result += (float) $workorder->getAfterHoursWorkCost();
        $result += (float) $workorder->getAdditionalServicesCost();
        $result -= (float) $workorder->getDiscount();

        return $result;
    }

    /**
     * @param Workorder $workorder
     * @return float
     */
    public function calculateTotalEstimatedTimeInspections(Workorder $workorder)
    {
        $woTotalEstimationTimeInspections = 0;

        /** @var WorkorderDeviceInfo $info */
        foreach ($workorder->getWorkorderDeviceInfo() as $info) {
            $woDeviceInfoTotalEstimationTimeInspection = $info->getInspectionEstimationTime();
            $woTotalEstimationTimeInspections += $woDeviceInfoTotalEstimationTimeInspection;
        }

        return $woTotalEstimationTimeInspections;
    }

    /**
     * @param Workorder $workorder
     * @return float
     */
    public function calculateTotalEstimationTimeRepairs(Workorder $workorder)
    {
        $woTotalEstimationTimeRepairs = 0;

        /** @var WorkorderDeviceInfo $info */
        foreach ($workorder->getWorkorderDeviceInfo() as $info) {
            $woDeviceInfoTotalEstimationTimeRepairs = $info->getRepairEstimationTime();
            $woTotalEstimationTimeRepairs += $woDeviceInfoTotalEstimationTimeRepairs;
        }

        return $woTotalEstimationTimeRepairs;
    }

    /**
     * @param Workorder $workorder
     * @return float
     */
    public function calculateTotalRepairsPrice(Workorder $workorder)
    {
        $woTotalRepairsPrice = 0;

        /** @var WorkorderDeviceInfo $info */
        foreach ($workorder->getWorkorderDeviceInfo() as $info) {
            $woDeviceTotalRepairsPrice = $info->getRepairPartsAndLabourPrice();
            $woTotalRepairsPrice += $woDeviceTotalRepairsPrice;
        }

        return $woTotalRepairsPrice;
    }

    /**
     * @param Workorder $workorder
     * @return float|int
     */
    public function calculateTotalMunicipalityAndProcessingFee(Workorder $workorder)
    {
        $woTotalMunAndProcFee = 0;

        /** @var WorkorderDeviceInfo $info */
        foreach ($workorder->getWorkorderDeviceInfo() as $info) {
            $woDeviceTotalMunAndProcFee = $info->getSubtotalMunicAndProcFee();
            $woTotalMunAndProcFee += $woDeviceTotalMunAndProcFee;
        }

        return $woTotalMunAndProcFee;
    }

    /**
     * @param Workorder $workorder
     */
    public function setWODepartmentChannelWithFeesWillVary(Workorder $workorder)
    {
        $isMunicipalityFessWillVary = false;

        /** @var WorkorderDeviceInfo $info */
        foreach ($workorder->getWorkorderDeviceInfo() as $info) {
            if ($info->getDepartmentChannelWithFeesWillVary()) {
                $isMunicipalityFessWillVary = true;
            }
        }

        $workorder->setIsMunicipalityFessWillVary($isMunicipalityFessWillVary);
    }

    /**
     * @param Workorder $workorder
     * @return float
     */
    public function calculateTotalPrice(Workorder $workorder)
    {
        $totalServiceFee = $workorder->getTotalServiceFee();
        $totalRepairsPrice = $workorder->getTotalRepairsPrice();
        $totalPrice = $totalServiceFee + $totalRepairsPrice;

        return $totalPrice;
    }

    /**
     * @param Workorder $workorder
     * @return mixed
     */
    public function calculateServicesCount(Workorder $workorder)
    {
        $countInspections = 0;
        $countRepairServices = 0;
        $woServices = $this->serviceRepository->findBy(['workorder' => $workorder]);

        /** @var Service $service */
        foreach ($woServices as $service) {
            if ($service->getNamed()->getFrequency()) {
                $countInspections++;
            } else {
                $countRepairServices++;
            }
        }

        $arrayCounts['countInspections'] = $countInspections;
        $arrayCounts['countRepairServices'] = $countRepairServices;

        return $arrayCounts;
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return float
     */
    public function calculateServiceTotalFee(Workorder $workorder, Proposal $proposal)
    {
        $woTotalServiceFee = $workorder->getTotalServiceFee();
        $woTotalServiceFee += $proposal->getServiceTotalFee();

        return $woTotalServiceFee;
    }

    /**
     * @param Workorder $workorder
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return float
     */
    public function calculateServiceTotalFeeByDeviceInfo(Workorder $workorder, RepairDeviceInfo $repairDeviceInfo)
    {
        $woTotalServiceFee = $workorder->getTotalServiceFee();
        $woTotalServiceFee += $repairDeviceInfo->getInspectionPrice();

        return $woTotalServiceFee;
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return float|string
     */
    public function calculateAfterHoursWorkCount(Workorder $workorder, Proposal $proposal)
    {
        $afterHoursWorkCost = (float) $workorder->getAfterHoursWorkCost();
        $afterHoursWorkCost += (float) $proposal->getAfterHoursWorkCost();

        return $afterHoursWorkCost;
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return float
     */
    public function calculateDiscount(Workorder $workorder, Proposal $proposal)
    {
        $discount = (float) $workorder->getDiscount();
        $discount += (float) $proposal->getDiscount();

        return $discount;
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return string
     */
    public function calculateAdditionalComments(Workorder $workorder, Proposal $proposal)
    {
        $comment = $workorder->getAdditionalComments();

        if (!empty($proposal->getAdditionalComment())) {
            $comment = empty($comment)
                ? $proposal->getAdditionalComment()
                : $comment.', '.$proposal->getAdditionalComment();
        }

        return $comment;
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return string
     */
    public function calculateInternalComments(Workorder $workorder, Proposal $proposal)
    {
        $comment = $workorder->getInternalComment();

        if (!empty($proposal->getInternalComments())) {
            $comment = empty($comment)
                ? $proposal->getInternalComments()
                : $comment.', '.$proposal->getInternalComments();
        }

        return $comment;
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return float
     */
    public function calculateTotalAdditionalCost(Workorder $workorder, Proposal $proposal)
    {
        $woTotalAdditionalCost = $workorder->getAdditionalServicesCost();
        $woTotalAdditionalCost += $proposal->getAdditionalCost();

        return $woTotalAdditionalCost;
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return string
     */
    public function calculateAdditionalCostComments(Workorder $workorder, Proposal $proposal)
    {
        $comment = $workorder->getAdditionalCostComment();

        if (!empty($proposal->getAdditionalCostComment())) {
            $comment = empty($comment)
                ? $proposal->getAdditionalCostComment()
                : $comment.', '.$proposal->getAdditionalCostComment();
        }

        return $comment;
    }
}
