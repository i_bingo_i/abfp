<?php

namespace WorkorderBundle\Factories;

use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderLog;
use AppBundle\Entity\WorkorderLogType;
use Exception;

class WorkorderLogFactory
{
    /**
     * @param Workorder $workorder
     * @param WorkorderLogType $type
     * @param $params
     *
     * @return WorkorderLog
     * @throws Exception
     */
    public static function make(Workorder $workorder, WorkorderLogType $type, $params)
    {
        $workorderLogRecord = new WorkorderLog();
        $workorderLogRecord->setType($type);
        $workorderLogRecord->setDate(new \DateTime());
        $workorderLogRecord->setWorkorder($workorder);

        if ($params['message']) {
            $workorderLogRecord->setMessage($params['message']);
        }

        if ($params['author']) {
            $workorderLogRecord->setAuthor($params['author']);
        }

        if ($params['comment']) {
            $workorderLogRecord->setComment($params['comment']);
        }

        if ($params['invoice']) {
            $workorderLogRecord->setInvoice($params['invoice']);
        }

        if ($params['letter']) {
            $workorderLogRecord->setLetter($params['letter']);
        }

        if ($params['proposal']) {
            $workorderLogRecord->setProposal($params['proposal']);
        }

        if ($params['changingStatus']) {
            $workorderLogRecord->setChangingStatus($params['changingStatus']);
        }

        return $workorderLogRecord;
    }
}
