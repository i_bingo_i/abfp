<?php

namespace WorkorderBundle\Factories;

use AppBundle\Entity\Account;
use AppBundle\Entity\Coordinate;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\PaymentMethod;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderStatus;
use AppBundle\Entity\WorkorderType;

class WorkorderFactory
{
    /**
     * @param array $data
     * @return Workorder
     */
    public static function make(
        Account $account,
        DeviceCategory $deviceCategory,
        PaymentMethod $paymentMethod,
        WorkorderStatus $status,
        WorkorderType $type,
        User $user = null
    ) {
        /** @var Coordinate $accountCoordinate */
        $accountCoordinate = $account->getCoordinate();

        /** @var Workorder $workorder */
        $workorder = new Workorder();
        $workorder->setAccount($account);
        $workorder->setLocation($accountCoordinate);
        $workorder->setDivision($deviceCategory);
        $workorder->setCreatedBy($user);
        $workorder->setPaymentMethod($paymentMethod);
        $workorder->setStatus($status);
        $workorder->setType($type);

        return $workorder;
    }
}
