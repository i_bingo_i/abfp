<?php

namespace WorkorderBundle\Factories;

use AppBundle\Entity\Step;
use AppBundle\Entity\StepNamed;
use AppBundle\Entity\StepStatus;
use AppBundle\Entity\Workorder;

class WorkorderStepFactory
{
    /**
     * @param Workorder $workorder
     * @param StepStatus $stepStatus
     * @param StepNamed $stepNamed
     * @return Step
     */
    public static function make(Workorder $workorder, StepStatus $stepStatus, StepNamed $stepNamed)
    {
        /** @var Step $step */
        $step = new Step();
        $step->setStatus($stepStatus);
        $step->setWorkorder($workorder);
        $step->setNamed($stepNamed);

        return $step;
    }
}
