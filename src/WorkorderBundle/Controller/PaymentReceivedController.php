<?php

namespace WorkorderBundle\Controller;

use AppBundle\Entity\Workorder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Services\PaymentReceived as PaymentReceivedService;
use WorkorderBundle\Validators\Requests\PaymentReceived;

class PaymentReceivedController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function paymentReceivedAction(Workorder $workorder, Request $request)
    {
        /** @var Session $session */
        $session = $this->get('session');
        /** @var PaymentReceived $paymentReceivedValidator */
        $paymentReceivedValidator = $this->get('workorder.request_payment_received.validator');
        /** @var PaymentReceivedService $paymentReceived */
        $paymentReceived = $this->get('workorder.payment_received.service');

        $comment = $request->request->get('comment');

        if (!$paymentReceivedValidator->validate($workorder, $comment)) {
            $messages = implode("\n", $paymentReceivedValidator->getErrorMessages());
            $session->getFlashBag()->add('failed', $messages);
        } else {
            $paymentReceived->received($workorder, $comment);
        }

        return $this->redirectToRoute('admin_workorder_view', [
            "workorder" => $workorder->getId()
        ]);
    }
}
