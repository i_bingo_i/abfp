<?php

namespace WorkorderBundle\Controller;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Workorder;
use AppBundle\Services\AccountContactPerson\Authorizer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Services\SendViaMailOrFax;
use WorkorderBundle\Validators\Requests\InvoiceSentViaMailOrFax;

class InvoiceSentViaMailOrFaxController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function invoiceSentViaMailOrFaxAction(Workorder $workorder, Request $request)
    {
        /** @var Session $session */
        $session = $this->get('session');
        /** @var InvoiceSentViaMailOrFax $invoiceSentViaMailOrFaxValidator */
        $invoiceSentViaMailOrFaxValidator = $this->get('workorder.request_invoice_sent_via_mail_or_fax.validator');
        /** @var SendViaMailOrFax $sendViaMailOrFax */
        $sendViaMailOrFax = $this->get('workorder.send_via_mail_or_fax.service');
        /** @var Authorizer $acpAuthorizerService */
        $acpAuthorizerService = $this->get('app.account_contact_person_authorizer.service');
        /** @var array $authorizeCollection */
        $authorizeCollection = $acpAuthorizerService->getPrimaryAuthorizersByAccountForDivisions($workorder);
        /** @var AccountContactPerson $primaryPayment */
        $primaryPayment = $acpAuthorizerService->getPrimaryPaymentByAccountForDivisions($workorder);
        /** @var AccountContactPerson $authorizer */
        $authorizer = array_shift($authorizeCollection);

        $reportsWereSentTo = $request->request->get('reportsSentToo');
        $comment = $request->request->get('comment');

        if (!$invoiceSentViaMailOrFaxValidator->validate($workorder, $comment)) {
            $messages = implode("\n", $invoiceSentViaMailOrFaxValidator->getErrorMessages());
            $session->getFlashBag()->add('failed', $messages);

        } elseif (!empty($primaryPayment) && !empty($authorizer)
            && ($primaryPayment->getId() == $authorizer->getId())
        ) {
            if ($reportsWereSentTo) {
                $sendViaMailOrFax->sentReportsAlongWithInvoice($workorder);
            } else {
                $sendViaMailOrFax->sendInvoice($workorder, $comment, $reportsWereSentTo);
            }

        } else {
            $sendViaMailOrFax->sendInvoice($workorder, $comment);
        }

        return $this->redirectToRoute('admin_workorder_view', [
            "workorder" => $workorder->getId()
        ]);
    }
}
