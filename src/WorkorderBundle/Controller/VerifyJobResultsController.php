<?php

namespace WorkorderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Workorder;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Services\VerifyJobResults;
use WorkorderBundle\Validators\AvailableActions;

class VerifyJobResultsController extends Controller
{

    /**
     * @param Workorder $workorder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function verifyJobResultsAction(Workorder $workorder)
    {
        /** @var Session $session */
        $session = $this->get('session');
        /** @var VerifyJobResults $verifyService */
        $verifyService = $this->get('workorder.verify_job_results.service');
        /** @var AvailableActions $availableActionsValidator */
        $availableActionsValidator = $this->get('workorder.available_actions.validator');

        if (!$availableActionsValidator->validate($workorder->getStatus(), $verifyService::VERIFIED_STATUS_ALIAS)) {
            $messages = implode("\n", $availableActionsValidator->getErrorMessages());
            $session->getFlashBag()->add('failed', $messages);
        } else {
            $verifyService->verify($workorder);
        }

        return $this->redirectToRoute('admin_workorder_view', [
            "workorder" => $workorder->getId()
        ]);
    }
}
