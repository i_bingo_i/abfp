<?php

namespace WorkorderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\DTO\FinishRequestDTO;
use AppBundle\Entity\Workorder;
use Symfony\Component\HttpFoundation\Request;
use WorkorderBundle\Services\Finisher;
use Symfony\Component\HttpFoundation\RedirectResponse;
use WorkorderBundle\Validators\Requests\Finish;

class FinishController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse
     */
    public function finishAction(Workorder $workorder, Request $request)
    {
        /** @var Finisher $woFinisher */
        $woFinisher = $this->get('workorder.finisher');
        /** @var Session $session */
        $session = $this->get('session');
        /** @var Finish $finishValidator */
        $finishValidator = $this->get('workorder.request_finish.validator');

        /** @var FinishRequestDTO $finishRequest */
        $finishRequest = new FinishRequestDTO();
        $finishRequest->workorder = $workorder;
        $finishRequest->comment = $request->request->get('comment');

        if (!$finishValidator->validate($finishRequest)) {
            $messages = implode("\n", $finishValidator->getErrorMessages());
            $session->getFlashBag()->add('failed', $messages);
        } else {
            $woFinisher->finish($finishRequest);
        }

        return $this->redirectToRoute('admin_workorder_view', [
            "workorder" => $workorder->getId()
        ]);
    }
}
