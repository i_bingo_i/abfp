<?php

namespace WorkorderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ViewCustomInvoiceController extends Controller
{
    /**
     * @param string $fileName
     * @return BinaryFileResponse
     */
    public function viewCustomInvoicePdfAction(string $fileName)
    {
        $appPath = $this->getParameter('kernel.root_dir');
        $uploadsPath = realpath($appPath . '/../web/uploads/invoices');

        return new BinaryFileResponse("$uploadsPath/$fileName");
    }
}
