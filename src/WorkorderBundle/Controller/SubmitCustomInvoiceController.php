<?php

namespace WorkorderBundle\Controller;

use AppBundle\Entity\Workorder;
use InvoiceBundle\ActionManagers\InvoiceActionManager;
use InvoiceBundle\DataTransportObject\Requests\CreateCustomInvoiceDTO;
use InvoiceBundle\Parsers\Invoice\Request\CreateCustomInvoiceParser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Validators\Requests\SubmitCustomInvoice;

class SubmitCustomInvoiceController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function submitCustomInvoiceAction(Workorder $workorder, Request $request)
    {
        /** @var Session $session */
        $session = $this->get('session');
        /** @var SubmitCustomInvoice $submitInvoiceValidator */
        $submitCustomInvoiceValidator = $this->get('workorder.request_submit_custom_invoice.validator');
        /** @var InvoiceActionManager $invoiceActionManager */
        $invoiceActionManager = $this->get('invoice.invoice.action.manager');
        /** @var CreateCustomInvoiceParser $createCustomInvoiceParser */
        $createCustomInvoiceParser = $this->get('invoice.create_custom.parser');
        /** @var CreateCustomInvoiceDTO $customInvoiceDto */
        $customInvoiceDto = $createCustomInvoiceParser->parse($request);

        if (!$submitCustomInvoiceValidator->validate($customInvoiceDto)) {
            $messages = implode("\n", $submitCustomInvoiceValidator->getErrorMessages());
            $session->getFlashBag()->add('failed', $messages);
        } else {
            $invoiceActionManager->submitCustom($customInvoiceDto);
        }

        return $this->redirectToRoute('admin_workorder_view', [
            "workorder" => $workorder->getId()
        ]);
    }
}
