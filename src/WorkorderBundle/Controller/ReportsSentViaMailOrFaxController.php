<?php

namespace WorkorderBundle\Controller;

use AppBundle\Entity\Workorder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use WorkorderBundle\Services\SendViaMailOrFax;
use WorkorderBundle\Validators\Requests\ReportsSentToAHJViaMailOrFax;
use WorkorderBundle\Validators\Requests\ReportsSentToClientViaMailOrFax;
use Symfony\Component\HttpFoundation\Request;

class ReportsSentViaMailOrFaxController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function reportsSentToClientViaMailOrFaxAction(Workorder $workorder, Request $request)
    {
        /** @var Session $session */
        $session = $this->get('session');
        /** @var ReportsSentToClientViaMailOrFax $reportsSentToClientViaMailOrFaxValidator */
        $reportsSentToClientViaMailOrFaxValidator =
            $this->get('workorder.request_reports_sent_to_client_via_mail_or_fax.validator');
        /** @var SendViaMailOrFax $sendViaMailOrFax */
        $sendViaMailOrFax = $this->get('workorder.send_via_mail_or_fax.service');

        $comment = $request->request->get('comment');

        if (!$reportsSentToClientViaMailOrFaxValidator->validate($workorder, $comment)) {
            $messages = implode("\n", $reportsSentToClientViaMailOrFaxValidator->getErrorMessages());
            $session->getFlashBag()->add('failed', $messages);
        } else {
            $sendViaMailOrFax->sentReportsToClient($workorder, $comment);
        }

        return $this->redirectToRoute('admin_workorder_view', [
            "workorder" => $workorder->getId()
        ]);
    }

    /**
     * @param Workorder $workorder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function reportsSentToAHJViaMailOrFaxAction(Workorder $workorder, Request $request)
    {
        /** @var Session $session */
        $session = $this->get('session');
        /** @var ReportsSentToAHJViaMailOrFax $reportsSentToAHJViaMailOrFaxValidator */
        $reportsSentToAHJViaMailOrFaxValidator =
            $this->get('workorder.request_reports_sent_to_ahj_via_mail_or_fax.validator');
        /** @var SendViaMailOrFax $sendViaMailOrFax */
        $sendViaMailOrFax = $this->get('workorder.send_via_mail_or_fax.service');

        $comment = $request->request->get('comment');

        if (!$reportsSentToAHJViaMailOrFaxValidator->validate($workorder, $comment)) {
            $messages = implode("\n", $reportsSentToAHJViaMailOrFaxValidator->getErrorMessages());
            $session->getFlashBag()->add('failed', $messages);
        } else {
            $sendViaMailOrFax->sentReportsToAHJ($workorder, $comment);
        }

        return $this->redirectToRoute('admin_workorder_view', [
            "workorder" => $workorder->getId()
        ]);
    }
}
