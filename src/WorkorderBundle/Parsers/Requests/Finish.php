<?php

namespace WorkorderBundle\Parsers\Requests;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Services\CoordinateService;
use Symfony\Component\HttpFoundation\Request;
use WorkorderBundle\DTO\FinishRequestDTO;

class Finish
{
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;
    /** @var CoordinateService */
    private $coordinateService;

    /**
     * Finish constructor.
     *
     * @param ObjectManager $objectManager
     * @param CoordinateService $coordinateService
     */
    public function __construct(ObjectManager $objectManager, CoordinateService $coordinateService)
    {
        $this->coordinateService = $coordinateService;

        $this->workorderRepository = $objectManager->getRepository('AppBundle:Workorder');
        $this->contractorUserRepository = $objectManager->getRepository('AppBundle:ContractorUser');
    }

    /**
     * @param Request $request
     *
     * @return FinishRequestDTO
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function parse(Request $request)
    {
        $params = $request->request->all();
        $finishRequestDTO = new FinishRequestDTO();

        if (isset($params["orderID"])) {
            $finishRequestDTO->workorder = $this->workorderRepository->find($params["orderID"]);
        }

        if (isset($params["technicianID"])) {
            $finishRequestDTO->finisher = $this->contractorUserRepository->find($params["technicianID"]);
        }

        if (isset($params["comment"])) {
            $finishRequestDTO->comment = $params["comment"];
        }

        $lat = $params['lat'] ?? 0;
        $lng = $params['lng'] ?? 0;
        $finishRequestDTO->coordinates = $this->coordinateService->getCoordinate(trim($lat), trim($lng));

        return $finishRequestDTO;
    }
}
