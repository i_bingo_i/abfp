<?php

namespace AppBundle\Twig;

use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderDeviceInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\ServiceHistoryRepository;

class PriceDisplayingExtension extends \Twig_Extension
{
    /** @var ContainerInterface */
    private $container;
    /** @var DepartmentChannelManager */
    private $departmentChannelManager;
    /** @var ObjectManager */
    private $objectManager;
    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;

    /**
     * PriceDisplayingExtension constructor.
     * @param ContainerInterface $container
     * @param ObjectManager $objectManager
     */
    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->departmentChannelManager = $this->container->get('app.department_channel.manager');
        $this->serviceHistoryRepository = $this->objectManager->getRepository('AppBundle:ServiceHistory');
    }

    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter(
                'get_view_result_for_service_municipality_fee',
                array($this, 'getViewResultForServiceMunicipalityFee')
            ),
            new \Twig_SimpleFilter(
                'get_view_result_for_device_info_sub_total_mun_and_proc_fee',
                array($this, 'getViewResultForDeviceInfoSubTotalMunAndProcFee')
            ),
            new \Twig_SimpleFilter('get_device_total', array($this, 'getDeviceTotal')),
            new \Twig_SimpleFilter(
                'get_proposal_total_munic_and_proc_fee',
                array($this, 'getProposalTotalMunicAndProcFee')
            ),
            new \Twig_SimpleFilter('get_proposal_grand_total', array($this, 'getProposalGrandTotal')),
            new \Twig_SimpleFilter(
                'get_workorder_total_munic_and_proc_fee',
                array($this, 'getWorkorderTotalMunicAndProcFee')
            ),
            new \Twig_SimpleFilter('get_workorder_grand_total', array($this, 'getWorkorderGrandTotal')),
            new \Twig_SimpleFilter('get_subtotal_fee', array($this, 'getSubtotalFee')),
            new \Twig_SimpleFilter('get_retest_notice_grand_total', array($this, 'getRetestNoticeGrandTotal')),
            new \Twig_SimpleFilter('get_view_wo_payments_method', array($this, 'getViewWoPaymentsMethod')),
        );
    }

     /**
     * @param $service
     * @param $repairDeviceInfo
     * @return string
     */
    public function getViewResultForServiceMunicipalityFee($service, $repairDeviceInfo)
    {
        //$repairDeviceInfo can be RepairDeviceInfo or WorkorderDeviceInfo
        //$service can be either Service or ServiceHistory

        if ($service instanceof ServiceHistory) {
            /** @var ServiceHistory $serviceHistory */
            $serviceHistory = $service;
        } else {
            /** @var ServiceHistory $serviceHistory */
            $serviceHistory = $this->serviceHistoryRepository->getLastRecord($service);
        }
        /** @var Service $service */
        $service = $serviceHistory->getOwnerEntity();

        if (!$service->getNamed()->getIsNeedMunicipalityFee()) {
            return "-";
        }

        $departmentChannelWithFeesWillVary = $repairDeviceInfo->getDepartmentChannelWithFeesWillVary();
        if ($departmentChannelWithFeesWillVary) {
            return "*";
        }

        $municipalityFee = $serviceHistory->getMunicipalityFee();
        if ($municipalityFee) {
            return "$" . number_format($municipalityFee, 2);
        } else {
            return "-";
        }
    }

    /**
     * @param $repairDeviceInfo
     * @return string
     */
    public function getViewResultForDeviceInfoSubTotalMunAndProcFee($repairDeviceInfo)
    {
        if ($repairDeviceInfo instanceof RepairDeviceInfo or $repairDeviceInfo instanceof WorkorderDeviceInfo) {
            $departmentChannelWithFeesWillVary = $repairDeviceInfo->getDepartmentChannelWithFeesWillVary();

            if ($departmentChannelWithFeesWillVary) {
                return "*";
            }

            $subtotalMunicAndProcFee = $repairDeviceInfo->getSubtotalMunicAndProcFee();

            return "$" . number_format($subtotalMunicAndProcFee, 2);
        }

        return "$0";
    }

    /**
     * @param $repairDeviceInfo
     * @return string
     */
    public function getDeviceTotal($repairDeviceInfo)
    {
        if ($repairDeviceInfo instanceof RepairDeviceInfo or $repairDeviceInfo instanceof  WorkorderDeviceInfo) {
            $departmentChannelWithFeesWillVary = $repairDeviceInfo->getDepartmentChannelWithFeesWillVary();

            $deviceTotal = $repairDeviceInfo->getDeviceTotal();
            $result = "$" . number_format($deviceTotal, 2);

            if ($departmentChannelWithFeesWillVary) {
                $result .= "*";
            }

            return $result;
        }

        return "$0";
    }

    /**
     * @param Proposal $proposal
     * @return string
     */
    public function getProposalTotalMunicAndProcFee(Proposal $proposal)
    {
        $departmentChannelWithFeesWillVary = $proposal->getIsMunicipalityFessWillVary();

        $allTotalMunicAndProcFee = $proposal->getAllTotalMunicAndProcFee();
        if ($allTotalMunicAndProcFee) {
            $result = "$" . number_format($allTotalMunicAndProcFee, 2);
        } else {
            $result = "$0";
        }

        if ($departmentChannelWithFeesWillVary) {
            $result .= "*";
        }

        return $result;
    }

    /**
     * @param Workorder $workorder
     * @return string
     */
    public function getWorkorderTotalMunicAndProcFee(Workorder $workorder)
    {
        $departmentChannelWithFeesWillVary = $workorder->getIsMunicipalityFessWillVary();

        $allTotalMunicAndProcFee = $workorder->getTotalMunicAndProcFee();

        if ($allTotalMunicAndProcFee) {
            $result = "$" . number_format($allTotalMunicAndProcFee, 2);
        } else {
            $result = "$0";
        }

        if ($departmentChannelWithFeesWillVary and $allTotalMunicAndProcFee) {
            $result .= "*";
        }

        if ($departmentChannelWithFeesWillVary and !$allTotalMunicAndProcFee) {
            $result = "*";
        }

        return $result;
    }

    /**
     * @param Proposal $proposal
     * @return string
     */
    public function getProposalGrandTotal(Proposal $proposal)
    {
        $proposalServiceTotalFee = $proposal->getServiceTotalFee();
        $proposalAllGrandTotalPrice = $proposal->getAllGrandTotalPrice();
        $departmentChannelWithFeesWillVary = $proposal->getIsMunicipalityFessWillVary();

        if ($proposalServiceTotalFee) {
            $total = $proposalServiceTotalFee + $proposalAllGrandTotalPrice;
            $result = "$" . number_format($total, 2);
        } else {
            if ($proposalAllGrandTotalPrice) {
                $result = "$" . number_format($proposalAllGrandTotalPrice, 2);
            } else {
                $result = "$0";
            }
        }

        if ($departmentChannelWithFeesWillVary) {
            $result .= "*";
        }

        return $result;
    }

    /**
     * @param Proposal $proposal
     * @return string
     */
    public function getRetestNoticeGrandTotal(Proposal $proposal)
    {
        $grandTotal = $proposal->getGrandTotal();
        $departmentChannelWithFeesWillVary = $proposal->getIsMunicipalityFessWillVary();

        $result = "$" . number_format($grandTotal, 2);
        if ($departmentChannelWithFeesWillVary) {
            $result .= "*";
        }

        return $result;
    }

    /**
     * @param Workorder $workorder
     * @return string
     */
    public function getWorkorderGrandTotal(Workorder $workorder)
    {
        $workorderGrandTotal = $workorder->getGrandTotal();
        $departmentChannelWithFeesWillVary = $workorder->getIsMunicipalityFessWillVary();

        $result = "$";

        if ($workorderGrandTotal) {
            $result .= number_format($workorderGrandTotal, 2);
        } else {
            $result .= "0";
        }

        if ($departmentChannelWithFeesWillVary and $workorderGrandTotal) {
            $result .= "*";
        }

        if ($departmentChannelWithFeesWillVary and !$workorderGrandTotal) {
            $result = "*";
        }

        return $result;
    }

    /**
     * @param Workorder $workorder
     * @return string
     */
    public function getViewWoPaymentsMethod(Workorder $workorder)
    {
        $paymentMethodName = $workorder->getPaymentMethod()->getName();
        $departmentChannelWithFeesWillVary = $workorder->getIsMunicipalityFessWillVary();
        $addFromIos = false;

        $woServices = $workorder->getServices();
        /** @var Service $service */
        foreach ($woServices as $service) {
            if ($service->getAddedFromIos()) {
                $addFromIos = true;
            }
        }

        if ($departmentChannelWithFeesWillVary or $addFromIos) {
            $result = $paymentMethodName . ":" . " " . "Call Office.";
        } else {
            $woGrangTotalResult = $this->getWorkorderGrandTotal($workorder);
            $result = $paymentMethodName . "," . " " . $woGrangTotalResult;
        }

        return $result;
    }

    /**
     * @param $service
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return float|string
     */
    public function getSubtotalFee($service, RepairDeviceInfo $repairDeviceInfo)
    {
        //$service can be either Service or ServiceHistory
        if ($service instanceof ServiceHistory) {
            /** @var ServiceHistory $serviceHistory */
            $serviceHistory = $service;
        } else {
            /** @var ServiceHistory $serviceHistory */
            $serviceHistory = $this->serviceHistoryRepository->getLastRecord($service);
        }

        $fixedPrice = $serviceHistory->getFixedPrice();
        $departmentChannelWithFeesWillVary = $repairDeviceInfo->getDepartmentChannelWithFeesWillVary();
        $munAndProcFee = $serviceHistory->getMunicipalityFee();

        if (!$departmentChannelWithFeesWillVary) {
            $result = $munAndProcFee + $fixedPrice;
            $result = "$" . number_format($result, 2);
        } else {
            $result =  "$" . number_format($fixedPrice, 2) . "*";
        }

        return $result;
    }
}
