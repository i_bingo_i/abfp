<?php

namespace AppBundle\Twig;

class MoneyFormatExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('money_format', array($this, 'moneyFormat')),
        );
    }

    public function moneyFormat($price)
    {
        $formattedNum = number_format($price, 2);
        return $formattedNum;
    }
}
