<?php
namespace AppBundle\Twig;

use Symfony\Bridge\Doctrine\RegistryInterface;
use AppBundle\Entity\ProposalLog;
use AppBundle\Entity\Repository\ProposalLogRepository;

class ProposalLogExtension extends \Twig_Extension
{
    protected $doctrine;
    /** @var  ProposalLogRepository */
    protected $proposalLogRepository;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->proposalLogRepository = $this->doctrine->getRepository('AppBundle:ProposalLog');
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('is_use_action', array($this, 'isUseAction')),
        );
    }

    /**
     * @param ProposalLog $log
     * @param null $action
     * @return bool
     */
    public function isUseAction(ProposalLog $log, $action = null)
    {
        if ($log->getMessage() === 'Call needed!') {
            return $this->checkLog($log, 'Client was called');
        }

        if (in_array($log->getMessage(), array('Retest Notice was created',
                'Custom Proposal was created', 'Proposal was created',
                'This Proposal was created to replace another one',
                'This Retest Notice was created to replace another one'), true) and $action == 'email') {
            return $this->checkLog($log, $this->findEmailMessageByProposalLog($log));
        }

        if (in_array($log->getMessage(), array('Retest Notice was created',
                'Custom Proposal was created',
                'Proposal was created',
                'This Proposal was created to replace another one',
                'This Retest Notice was created to replace another one'), true) and $action == 'mail') {
            return $this->checkLog($log, $this->findMailMessageByProposalLog($log));
        }

        if ($log->getMessage() === 'Send email reminder!') {
            return $this->checkLog($log, 'Email reminder was sent');
        }

        return false;
    }

    /**
     * @param ProposalLog $log
     * @param $logRecordTypeAlias
     * @return bool
     */
    private function checkLog(ProposalLog $log, $logRecordTypeAlias)
    {
        /** @var ProposalLog[] $logRecords */
        $logRecords = $this->proposalLogRepository->findBy([
            'message' => $logRecordTypeAlias,
            'proposal' => $log->getProposal()
        ]);

        if (count($logRecords)) {
            return true;
        }

        return false;
    }

    /**
     * @param ProposalLog $log
     * @return mixed
     */
    public function findMailMessageByProposalLog(ProposalLog $log)
    {
        $proposalTypeAlias = $log->getProposal()->getType()->getAlias();

        $proposalLogMailMessages = [
            "retest" => "Retest Notice was sent via mail",
            "repair" => "Proposal was sent via mail.",
            "custom" => "Proposal was sent via mail."
        ];

        return $proposalLogMailMessages["$proposalTypeAlias"];
    }

    /**
     * @param ProposalLog $log
     * @return mixed
     */
    public function findEmailMessageByProposalLog(ProposalLog $log)
    {
        $proposalTypeAlias = $log->getProposal()->getType()->getAlias();

        $proposalLogMailMessages = [
            "retest" => "Letter was sent",
            "repair" => "Proposal was sent via email.",
            "custom" => "Proposal was sent via email."
        ];

        return $proposalLogMailMessages["$proposalTypeAlias"];
    }
}
