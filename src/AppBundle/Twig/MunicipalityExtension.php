<?php
namespace AppBundle\Twig;

use AppBundle\Entity\Department;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\ServiceHistoryRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use Symfony\Bridge\Doctrine\RegistryInterface;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\Repository\DepartmentRepository;
use AppBundle\Entity\Repository\DepartmentChannelRepository;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MunicipalityExtension extends \Twig_Extension
{
    /** @var ContainerInterface */
    private $container;
    /** @var RegistryInterface  */
    protected $doctrine;
    /** @var  DepartmentRepository */
    protected $departmentRepository;
    /** @var  DepartmentChannelRepository */
    protected $departmentChannelRepository;
    /** @var DeviceCategoryRepository */
    protected $deviceCategoryRepository;
    /** @var DepartmentChannelManager */
    private $departmentChannelManager;
    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;

    /**
     * MunicipalityExtension constructor.
     * @param RegistryInterface $doctrine
     * @param ContainerInterface $container
     */
    public function __construct(RegistryInterface $doctrine, ContainerInterface $container)
    {
        $this->doctrine = $doctrine;
        $this->container = $container;

        $this->departmentRepository = $this->doctrine->getRepository('AppBundle:Department');
        $this->departmentChannelRepository = $this->doctrine->getRepository('AppBundle:DepartmentChannel');
        $this->deviceCategoryRepository = $this->doctrine->getRepository('AppBundle:DeviceCategory');
        $this->departmentChannelManager = $this->container->get('app.department_channel.manager');
        $this->serviceHistoryRepository = $this->doctrine->getRepository('AppBundle:ServiceHistory');
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('municipality_fee', array($this, 'municipalityFee')),
            new \Twig_SimpleFilter('get_default_compliance_channel', array($this, 'getDefaultComplianceChannel')),
            new \Twig_SimpleFilter('get_department_channels', array($this, 'getDepartmentChannels')),
            new \Twig_SimpleFilter(
                'check_if_channel_has_fees_will_vary',
                array($this, 'checkIfChannelHasFeesWillVary')
            ),
            new \Twig_SimpleFilter(
                'get_municipality_fee_result_for_create_proposal_and_wo_by_service',
                array($this, 'getMunicipalityFeeResultForCreateProposalAndWoByService')
            ),
        );
    }

    /**
     * @param $department
     * @return float|int
     */
    public function municipalityFee($department)
    {
        $municipalityFee = 0;
        if (!$department) {
            return $municipalityFee;
        }

        $processingFee = $department->getProcessingFee();
        if ($processingFee) {
            $municipalityFee += (float)$processingFee;
        }

        $uploadFee = $department->getUploadFee();
        if ($uploadFee) {
            $municipalityFee += (float)$uploadFee;
        }

        return $municipalityFee;
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $division
     * @return DepartmentChannel|null
     */
    public function getDefaultComplianceChannel(Municipality $municipality, DeviceCategory $division)
    {
        $defaultChannel = null;

        //gently crutch
        $divisionAlias = $division->getAlias();
        if ($divisionAlias == "alarm") {
            $division = $this->deviceCategoryRepository->findOneBy(['alias' => "fire"]);
        }

        /** @var Department $department */
        $department = $this->departmentRepository->findOneBy(
            [
                'municipality' => $municipality,
                'division' => $division
            ]
        );

        /** @var DepartmentChannel[] $departmentChannels */
        $departmentChannels = $this->getDepartmentChannels($department);

        /** @var DepartmentChannel $channel */
        foreach ($departmentChannels as $channel) {
            if ($channel->getIsDefault()) {
                $defaultChannel = $channel;
            }
        }

        return $defaultChannel;
    }

    /**
     * @param Department $department
     * @return DepartmentChannel[]
     */
    public function getDepartmentChannels(Department $department)
    {
        $departmentChannels = $this->departmentChannelRepository->getDepartmentChannels($department);

        return $departmentChannels;
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $deviceCategory
     * @return \AppBundle\Entity\DepartmentChannel|bool
     */
    public function checkIfChannelHasFeesWillVary(Municipality $municipality, DeviceCategory $deviceCategory)
    {
        return $this->departmentChannelManager->getDefaultDepartmentChannelWithFeesWillVary(
            $municipality,
            $deviceCategory
        );
    }

    /**
     * @param Service $service
     * @return string
     */
    public function getMunicipalityFeeResultForCreateProposalAndWoByService(Service $service)
    {
        /** @var DepartmentChannel $departmentChannel */
        $departmentChannel = $service->getDepartmentChannel();
        /** @var ServiceHistory $serviceHistory */
        $serviceHistory = $this->serviceHistoryRepository->getLastRecord($service);
        /** @var Municipality $munAndProcFee */
        $municipality = $serviceHistory->getAccount()->getMunicipality();
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $serviceHistory->getDevice()->getNamed()->getCategory();
        /** @var DepartmentChannel $departmentChannelWithFeesWillVary */
        $departmentChannelWithFeesWillVary = $this->checkIfChannelHasFeesWillVary($municipality, $deviceCategory);
        $isNeedMunicipalityFee = $serviceHistory->getNamed()->getIsNeedMunicipalityFee();

        if ($departmentChannelWithFeesWillVary instanceof DepartmentChannel and $isNeedMunicipalityFee) {
            return "*";
        }

        $munFee = $this->municipalityFee($departmentChannel);

        if ($munFee and $service->getNamed()->getIsNeedMunicipalityFee()) {
            return "$" . $munFee;
        }

        return "-";
    }
}
