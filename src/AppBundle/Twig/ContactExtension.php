<?php
namespace AppBundle\Twig;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\DeviceCategory;

class ContactExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('is_responsibility', array($this, 'isResponsibility')),
            new \Twig_SimpleFilter('invalid_address', array($this, 'invalidAddress')),
        );
    }

    /**
     * @param AccountContactPerson $contact
     * @param $alias
     * @return bool
     */
    public function isResponsibility(
        AccountContactPerson $contact,
        $alias,
        $responsibilityForAuthorizer = false,
        $responsibilityForPayments = false
    ) {
        if ($responsibilityForAuthorizer) {
            /** @var DeviceCategory $authorizerResponsibility */
            foreach ($contact->getResponsibilities() as $authorizerResponsibility) {
                if ($authorizerResponsibility->getAlias() == $alias) {
                    return true;
                }
            }
        }

        if ($responsibilityForPayments) {
            /** @var DeviceCategory $paymentResponsibility */
            foreach ($contact->getPaymentResponsibilities() as $paymentResponsibility) {
                if ($paymentResponsibility->getAlias() == $alias) {
                    return true;
                }
            }
        }

        return false;
    }

    // TODO: Логіка дублюється з AddressManager -> isInvalid метод (потрібно прибрати дублювання)
    /**
     * @param Address $address
     * @return bool
     */
    public function invalidAddress(Address $address)
    {
        if (empty($address->getAddress())
            or empty($address->getCity())
            or empty($address->getState())
            or empty($address->getZip())
        ) {
            return true;
        }

        return false;
    }
}
