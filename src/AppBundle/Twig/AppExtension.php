<?php
namespace AppBundle\Twig;

use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\DeviceHistory;
use AppBundle\Entity\DynamicFieldValueHistory;
use AppBundle\Entity\Equipment;
use InvoiceBundle\Entity\Invoices;
use AppBundle\Entity\Licenses;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\LicensesDynamicFieldValue;
use AppBundle\Entity\Message;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Account\Authorizer;
use AppBundle\Services\Device\DynamicFieldsService;
use InvoiceBundle\Entity\InvoiceStatus;
use InvoiceBundle\Repository\InvoicesRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\WorkorderRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class AppExtension extends \Twig_Extension
{
    /** @var RegistryInterface  */
    protected $doctrine;
    /** @var ContainerInterface */
    private $container;
    /** @var AccountManager */
    private $accountManager;
    /** @var Authorizer */
    private $accountAuthorizerService;
    /** @var WorkorderRepository */
    protected $workorderRepository;
    /** @var DynamicFieldsService */
    private $dynamicFieldsService;
    /** @var Serializer */
    private $serializer;
    /** @var InvoicesRepository */
    private $invoiceRepository;


    /**
     * AppExtension constructor.
     * @param ContainerInterface $container
     * @param RegistryInterface $doctrine
     */
    public function __construct(ContainerInterface $container, RegistryInterface $doctrine)
    {
        $this->container = $container;
        $this->doctrine = $doctrine;

        $this->accountManager = $this->container->get('app.account.manager');
        $this->accountAuthorizerService = $this->container->get('app.account_authorizer.service');
        $this->dynamicFieldsService = $this->container->get('app.dynamic_fields.service');
        $this->workorderRepository = $this->doctrine->getRepository('AppBundle:Workorder');
        $this->serializer = $container->get("jms_serializer");
        $this->invoiceRepository = $this->doctrine->getRepository('InvoiceBundle:Invoices');
    }

    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('phone', [$this, 'phoneFilter']),
            new \Twig_SimpleFilter('fix_path', array($this, 'fixPath')),
            new \Twig_SimpleFilter('address', [$this, 'addressFilter']),
            new \Twig_SimpleFilter('billingAddress', [$this, 'billingAddressFilter']),
            new \Twig_SimpleFilter('address_array', [$this, 'addressArrayFilter']),
            new \Twig_SimpleFilter('deviceInfo', [$this, 'deviceInfoFilter']),
            new \Twig_SimpleFilter('deviceHistoryInfo', [$this, 'deviceHistoryInfoFilter']),
            new \Twig_SimpleFilter('deviceInfoWW', [$this, 'deviceInfoFilterWithoutWarning']),
            new \Twig_SimpleFilter('licenseInfo', [$this, 'licenseInfoFilter']),
            new \Twig_SimpleFilter('date_difference', [$this, 'dateDifference']),
            new \Twig_SimpleFilter('equipmentInfo', [$this, 'equipmentInfoFilter']),
            new \Twig_SimpleFilter('address_company', [$this, 'addressCompanyFilter']),
            new \Twig_SimpleFilter('url_format', [$this, 'urlFormatFilter']),
            new \Twig_SimpleFilter('is_division', [$this, 'isDivision']),
            new \Twig_SimpleFilter('is_message', [$this, 'isMessage']),
            new \Twig_SimpleFilter('serialize_object_to_array', [$this, 'serializeObjectToArray']),
            new \Twig_SimpleFilter('get_file_name_by_path', array($this, 'getFileNameByPath')),
            new \Twig_SimpleFilter('is_block_adding_device_to_proposal', [$this, 'isBlockAddingDeviceToProposal']),
            new \Twig_SimpleFilter(
                'is_block_adding_custom_proposal_by_address_error',
                [$this, 'isBlockAddingCustomProposalByAddressError']
            ),
            new \Twig_SimpleFilter('alarm_fire_filter_ids', [$this, 'getFireAndAlarmDivisionIdsToFilter']),
            new \Twig_SimpleFilter(
                'get_workorder_total_price_with_discount',
                [$this, 'getWorkorderTotalPriceWithDiscount']
            ),
            new \Twig_SimpleFilter('ucfirst', [$this, 'ucFirst'], ['needs_environment' => true]),
            new \Twig_SimpleFilter('get_current_draft_invoice_for_wo', [$this, 'getCurrentDraftInvoiceForWorkorder'])
        ];
    }

    /**
     * @param $numberPhone
     * @return string
     */
    public function phoneFilter($numberPhone)
    {
        $formatedNumberPhone = '';
        if (!empty($numberPhone)) {
            $firstChain = substr($numberPhone, 0, 3);
            $secondChain = substr($numberPhone, 3, 3);
            $thirdChain = substr($numberPhone, 6, 4);
            $formatedNumberPhone = '(' . $firstChain . ') ' . $secondChain . '-' . $thirdChain;
        }

            return $formatedNumberPhone;
    }

    /**
     * @param null $address
     * @return string
     */
    public function addressFilter($address = null)
    {
        //$address can be Address or AddressHistory

        $formatedAddress = '';
        if ($address) {
            if (!is_null($address->getAddress())) {
                $formatedAddress .= $address->getAddress();
                $formatedAddress .= ($address->getCity() or $address->getState() or $address->getZip()) ? ', ' : ' ';
            }

            if (!is_null($address->getCity())) {
                $formatedAddress .= $address->getCity();
                $formatedAddress .= ($address->getState() or $address->getZip()) ? ', ' : ' ';
            }

            $formatedAddress .= !is_null($address->getState()) ? $address->getState()->getCode() . ' ' : '';
            $formatedAddress .= !is_null($address->getZip()) ? $address->getZip() . ' ' : '';
        }
        return trim($formatedAddress);
    }

    /**
     * @param null $address
     * @return string
     */
    public function billingAddressFilter($address = null)
    {
        $formatedAddress = '';

        if ($address) {
            if (!is_null($address->getAddress())) {
                $formatedAddress .= nl2br($address->getAddress());
                $formatedAddress .= ($address->getCity() or $address->getState() or $address->getZip()) ? '<br>' : ' ';
            }

            if (!is_null($address->getCity())) {
                $formatedAddress .= $address->getCity();
                $formatedAddress .= ($address->getState() or $address->getZip()) ? ', ' : ' ';
            }

            $formatedAddress .= !is_null($address->getState()) ? $address->getState()->getCode() . ' ' : '';
            $formatedAddress .= !is_null($address->getZip()) ? $address->getZip() . ' ' : '';
        }

        return $formatedAddress ? trim($formatedAddress) : 'No Billing Address';
    }

    /**
     * @param $addressArray
     * @return string
     */
    public function addressArrayFilter($addressArray)
    {
        $formatedAddress = '';
        if (isset($addressArray['address'])) {
            $formatedAddress .= $addressArray['address'];
            $formatedAddress .= (isset($addressArray['city']) or
                isset($addressArray['state']) or
                isset($addressArray['zip'])) ? ', ' : ' ';
        }

        if (isset($addressArray['city'])) {
            $formatedAddress .= $addressArray['city'];
            $formatedAddress .= (isset($addressArray['state']) or isset($addressArray['zip'])) ? ', ' : ' ';
        }

        $formatedAddress .= isset($addressArray['state']['code']) ? $addressArray['state']['code'] . ' ' : '';
        $formatedAddress .= isset($addressArray['zip']) ? $addressArray['zip'] . ' ' : '';

        return trim($formatedAddress);
    }

    /**
     * @param DeviceHistory $deviceHistory
     * @return bool|string
     */
    public function deviceHistoryInfoFilter(DeviceHistory $deviceHistory)
    {
        $deviceInfo = '';
        $showWarning = false;

        if ($deviceHistory->getFields()) {
            /** @var DynamicFieldValueHistory $field */
            foreach ($deviceHistory->getFields() as $field) {
                if ($field->getField()->getIsShow()) {
                    if (!is_null($field->getValue()) or !is_null($field->getOptionValue())) {
                        $deviceInfo = $this->checkDynamicHistoryField($field, $deviceInfo);
                    } elseif (empty($field->getValue()) and empty($field->getOptionValue())) {
                        $showWarning = true;
                    }
                }
            }
            $deviceInfo = (strlen($deviceInfo)>3) ? substr($deviceInfo, 0, strlen($deviceInfo)-2) : $deviceInfo;
        } else {
            $showWarning = true;
        }

        if (!$deviceHistory->getPhoto()) {
            $showWarning = true;
        }

        if ($deviceHistory->getLocation()) {
            $deviceInfo .= (strlen($deviceInfo)>=1) ?
                (", ".$deviceHistory->getLocation()) : $deviceHistory->getLocation();
        } else {
            $showWarning = true;
        }

        if ($deviceHistory->getStatus()) {
            $deviceInfo .= (strlen($deviceInfo)>=1) ? (", ".
                $deviceHistory->getStatus()->getName()) : $deviceHistory->getStatus()->getName();
        } else {
            $showWarning = true;
        }

        if ($showWarning) {
            $deviceInfo .= ' <img class="app-contact-list__warning" alt="warning image" '.
                'src="/bundles/admin/dist/images/warning.svg">';
        }

        return $deviceInfo;
    }

    /**
     * @param Device $device
     * @param bool $viewStatus
     * @param bool $displayingWarnings
     * @return string
     */
    public function deviceInfoFilter(Device $device, $viewStatus = true, $displayingWarnings = false)
    {
        $deviceInfo = '';
        $showWarning = false;

        if ($device->getFields()) {
            extract($this->dynamicFieldsService->dynamicValueInField($device), EXTR_OVERWRITE);
        } else {
            $showWarning = true;
        }

        if (!$device->getPhoto()) {
            $showWarning = true;
        }

        if ($device->getLocation()) {
            $deviceInfo .= (strlen($deviceInfo)>=1) ? (", ".$device->getLocation()) : $device->getLocation();
        } else {
            $showWarning = true;
        }

        if ($viewStatus) {
            if ($device->getStatus()) {
                $deviceInfo .= (strlen($deviceInfo)>=1) ? (", ".
                    $device->getStatus()->getName()) : $device->getStatus()->getName();
            } else {
                $showWarning = true;
            }
        }

        if ($showWarning and $displayingWarnings) {
            $deviceInfo .= ' <img class="app-contact-list__warning" alt="warning image" '.
                'src="/bundles/admin/dist/images/warning.svg">';
        }

        return $deviceInfo;
    }

    /**
     * @param DynamicFieldValueHistory $field
     * @param $deviceInfo
     * @return string
     */
    private function checkDynamicHistoryField(DynamicFieldValueHistory $field, $deviceInfo)
    {
        if (!empty($field->getValue())) {
            $deviceInfo = $this->addHistoryLabel($field->getValue(), $deviceInfo, $field);
        } elseif (!empty($field->getOptionValue())) {
            $deviceInfo = $this->addHistoryLabel($field->getOptionValue()->getOptionValue(), $deviceInfo, $field);
        }

        return $deviceInfo;
    }

    /**
     * @param $value
     * @param $deviceInfo
     * @param DynamicFieldValueHistory $field
     * @return string
     */
    private function addHistoryLabel($value, $deviceInfo, DynamicFieldValueHistory $field)
    {

        if ($field->getField()->getUseLabel() and !$field->getField()->getLabelAfter()) {
            $deviceInfo = $this->dynamicFieldsService
                ->addLabelBeforeValue($value, $field->getField()->getLabelDescription(), $deviceInfo);
        } elseif ($field->getField()->getLabelAfter()) {
            $deviceInfo = $this->dynamicFieldsService
                ->addLabelAfterValue($value, $field->getField()->getLabelDescription(), $deviceInfo);
        } elseif (!$field->getField()->getUseLabel()) {
            $deviceInfo .= $value.", ";
        }

        return $deviceInfo;
    }

    /**
     * @param $address
     * @return bool|string
     */
    public function addressCompanyFilter($address)
    {
        $formatedAddress = '';
        if ($address['address']) {
            $formatedAddress .= $address['address'];
            $formatedAddress .= ($address['city'] or $address['state'] or $address['zip']) ? ', ' : ' ';
        }

        if ($address['city']) {
            $formatedAddress .= $address['city'];
            $formatedAddress .= ($address['state'] or $address['zip']) ? ', ' : ' ';
        }

        $formatedAddress .= $address['state'] ? $address['state'] . ' ' : '';
        $formatedAddress .= $address['zip'] ? $address['zip'] . ' ' : '';

        return trim($formatedAddress) ? trim($formatedAddress) : 'No Address';
    }

    /**
     * @param $url
     * @return string
     */
    public function urlFormatFilter($url)
    {
        if (!stristr($url, 'http://') and !stristr($url, 'https://')) {
            return 'http://' . $url;
        }

        return $url;
    }

    /**
     * @param Licenses $license
     * @return string
     */
    public function licenseInfoFilter(Licenses $license)
    {
        $licenseInfo = "";

        if ($license->getCode()) {
            $licenseInfo .= "ID: ".$license->getCode().", ";
        }
        if ($license->getState()) {
            $licenseInfo .= $license->getState()->getCode().", ";
        }
        if ($license->getFields()) {
            foreach ($license->getFields() as $field) {
                $licenseInfo = $this->checkLicensesDynamicField($field, $licenseInfo);
            }
//            $licenseInfo = (strlen($licenseInfo)>3) ? substr($licenseInfo, 0, strlen($licenseInfo)-2) : $licenseInfo;
        }

        if ($license->getRenewalDate()) {
            $licenseInfo .= "Renewal Date: ".$license->getRenewalDate()->format("m/d/Y");
        }

        return $licenseInfo;
    }

    /**
     * @param DeviceCategory[] $divisions
     * @param $alias
     * @return bool
     */
    public function isDivision($divisions, $alias)
    {
        /** @var DeviceCategory $division */
        foreach ($divisions as $division) {
            if ($division->getAlias() == $alias) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Equipment $equipment
     * @return bool|string
     */
    public function equipmentInfoFilter(Equipment $equipment)
    {
        $equipmentInfo = '';
        $showWarning = false;

        // TODO: Dont remove comment code because we dont know when show yellow zlp

        if ($equipment->getModel()) {
            $equipmentInfo .= $equipment->getModel();
//        } else {
//            $showWarning = true;
        }

        if ($equipment->getSerialNumber()) {
            $equipmentInfo .= (strlen($equipmentInfo)>=1) ?
                (", S/N: ".$equipment->getSerialNumber()) :
                ("S/N: ".$equipment->getSerialNumber());
//        } else {
//            $showWarning = true;
        }

        if ($equipment->getLastCalibratedDate()) {
            $equipmentInfo .= (strlen($equipmentInfo)>=2) ?
                (", Last calibrated Date: ".$equipment->getLastCalibratedDate()->format('m/d/Y')) :
                ("Last calibrated Date: ".$equipment->getLastCalibratedDate()->format('m/d/Y'));
//        } else {
//            $showWarning = true;
        }

        if ($equipment->getLastCalibratedDate()) {
            $equipmentInfo .= (strlen($equipmentInfo)>=2) ?
                (", Next calibrated Date: ".$equipment->getNextCalibratedDate()->format('m/d/Y')) :
                ("Next calibrated Date: ".$equipment->getNextCalibratedDate()->format('m/d/Y'));

            if ($equipment->getNextCalibratedDate() < new \DateTime()) {
                $showWarning = true;
            }
//        } else {
//            $showWarning = true;
        }

        if ($equipment->getCompanySuppliedGauge()) {
            $equipmentInfo .= (strlen($equipmentInfo)>=2) ?
                ", Company Supplied Gauge: Yes" :
                "Company Supplied Gauge: Yes";
        } else {
            $equipmentInfo .= (strlen($equipmentInfo)>=2) ?
                ", Company Supplied Gauge: No" :
                "Company Supplied Gauge: No";
        }

        if ($showWarning) {
            $equipmentInfo .= ' <img class="app-contact-list__warning" alt="warning image" '.
                'src="/bundles/admin/dist/images/warning.svg">';
        }

        return $equipmentInfo;
    }

    /**
     * @param $endDate
     * @return bool|int
     */
    public function dateDifference($endDate)
    {
        $currentDate = new \DateTime();
        $interval = $currentDate->diff($endDate);

        if ($interval->days == 0 and $interval->invert == 1) {
            $daysInterval = 0;
        } elseif ($interval->days == 0 and $interval->h > 0 and $interval->invert == 0) {
            $daysInterval = 1;
        } else {
            $daysInterval = $interval->format("%r%a");
        }

        return (int)$daysInterval;
    }

    /**
     * @param Account $account
     * @return bool
     */
    public function isBlockAddingCustomProposalByAddressError(Account $account)
    {

        return $this->accountAuthorizerService->checkForErrorAddress($account);
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @param bool $groupAccountPageLabel
     * @return bool
     */
    public function isBlockAddingDeviceToProposal(
        Account $account,
        DeviceCategory $division,
        $groupAccountPageLabel = true
    ) {
        if ($groupAccountPageLabel) {
            if ($account->getParent()) {
                $account = $account->getParent();
            }
        }

        $checkOwnAuthorizerByDivision = $this->accountAuthorizerService->checkOwnAuthorizerByDivision(
            $account,
            $division
        );

        if (!$checkOwnAuthorizerByDivision) {
            return true;
        }

        $errorMessages = ['authorizer_invalid_address', 'no_municipality_compliance_channel_backflow'];
        foreach ($account->getMessages() as $message) {
            if (in_array($message->getAlias(), $errorMessages)) {
                return true;
            }
        }

        return false;
    }


        /**
     * @param Account $account
     * @param $aliasMessage
     * @return bool|string
     */
    public function isMessage(Account $account, $aliasMessage)
    {
        /** @var Message $message */
        foreach ($account->getMessages() as $message) {
            if ($message->getAlias() == $aliasMessage) {
                return $this->returnedMessage($message, $aliasMessage, $account);
            }
        }
        return false;
    }


    /**
     * @param Device $device
     * @return bool|string
     */
    public function deviceInfoFilterWithoutWarning(Device $device)
    {
        $deviceInfo = '';

        if ($device->getFields()) {
            /** @var DynamicFieldValue $field */
            foreach ($device->getFields() as $field) {
                if ($field->getField()->getIsShow()) {
                    if (!empty($field->getValue()) || !empty($field->getOptionValue())) {
                        $deviceInfo = $this->dynamicFieldsService->checkDynamicField($field, $deviceInfo);
                    }
                }
            }
            $deviceInfo = (strlen($deviceInfo)>3) ? substr($deviceInfo, 0, strlen($deviceInfo)-2) : $deviceInfo;
        }

        if ($device->getLocation()) {
            $deviceInfo .= (strlen($deviceInfo)>=1) ? (", ".$device->getLocation()) : $device->getLocation();
        }

        return str_replace(', ', '   ', $deviceInfo);
    }

    /**
     * @param $divisions
     * @return string
     */
    public function getFireAndAlarmDivisionIdsToFilter($divisions)
    {
        $fireDivisionId = null;
        $alarmDivisionId = null;

        /** @var DeviceCategory $division */
        foreach ($divisions as $division) {
            if ($division->getAlias() == 'fire') {
                /** @var DeviceCategory $fireDivision */
                $fireDivisionId = $division->getId();
            }
            if ($division->getAlias() == 'alarm') {
                /** @var DeviceCategory $alarmDivision */
                $alarmDivisionId = $division->getId();
            }
        }

        return $fireDivisionId .",". $alarmDivisionId;
    }

    /**
     * @param Workorder $workorder
     * @return float
     */
    public function getWorkorderTotalPriceWithDiscount(Workorder $workorder)
    {
        $grandTotalPrice = $workorder->getGrandTotal();
        $discount = $workorder->getDiscount();

        $result = $grandTotalPrice - $discount;

        return $result;
    }

    /**
     * @param $path
     * @return null|string|string[]
     */
    public function fixPath($path)
    {
        return ltrim($path, "/");
    }

    /**
     * @param \Twig_Environment $env
     * @param $string
     * @return string
     */
    public function ucFirst(\Twig_Environment $env, $string)
    {
        if (null !== $charset = $env->getCharset()) {
            $prefix = mb_strtoupper(mb_substr($string, 0, 1, $charset), $charset);
            $suffix = mb_substr($string, 1, mb_strlen($string, $charset));
            return sprintf('%s%s', $prefix, $suffix);
        }
        return ucfirst(strtolower($string));
    }

    /**
     * @param $object
     * @param string $serializationGroup
     * @return array|mixed
     */
    public function serializeObjectToArray($object, $serializationGroup = "setroles")
    {
        $returnSerializationObject = $object;
        if (gettype($object) === "object") {
//            if ($object instanceof ChoiceView) {
            if (get_class($object) == "Symfony\Component\Form\ChoiceList\View\ChoiceView") {
                $returnSerializationObject = [];
                $returnSerializationObject["label"] = $object->label;
                $returnSerializationObject["value"] = $object->value;
                $returnSerializationObject["data"] = $object->data;
            }
        }

        return $this->serializer->toArray(
            $returnSerializationObject,
            SerializationContext::create()->setGroups([$serializationGroup])->enableMaxDepthChecks()
        );
    }

    /**
     * @param $path
     * @return mixed
     */
    public function getFileNameByPath($path)
    {
        if (stristr($path, "/") !== false) {
            $array = explode("/", $path);

            return array_pop($array);
        }

        return $path;
    }

    /**
     * @param Workorder $workorder
     * @return bool|Invoices
     */
    public function getCurrentDraftInvoiceForWorkorder(Workorder $workorder)
    {
        /** @var Invoices $invoice */
        $invoice = $this->invoiceRepository->getLastForWorkorderByStatusAlias($workorder, InvoiceStatus::STATUS_DRAFT);

        if (!$invoice instanceof Invoices) {
            return false;
        }

        return $invoice;
    }

    /**
     * @param Message $message
     * @param $aliasMessage
     * @param Account $account
     * @return string
     */
    private function returnedMessage(Message $message, $aliasMessage, Account $account)
    {

        $returnedMessage = "<span class=\"tooltip-warning-container\">
                          <img class=\"app-contact-list__warning app-contact-list__warning--middle 
                           app-opportunities-table-warning tooltipWarning\"
                               alt=\"warning image\"
                               data-toggle=\"tooltip\"
                               data-placement=\"top\"
                               title=\"{$message->getMessage()}\"
                               data-original-title=\"No data\"
                               src=\"/bundles/admin/dist/images/{$message->getIcon()}\">
                        </span>";

        if ($aliasMessage == 'no_municipality_compliance_channel_backflow') {
            $municipalityId = $account->getMunicipality()->getId();
            $router =$this->container->get('router')->generate(
                'admin_municipality_view',
                [
                    'municipality' => $municipalityId
                ]
            );

            return "<a href='$router' target='_blank'>$returnedMessage</a>";
        }

        return $returnedMessage;
    }

    /**
     * @param LicensesDynamicFieldValue $field
     * @param $licenseInfo
     * @return string
     */
    private function checkLicensesDynamicField(LicensesDynamicFieldValue $field, $licenseInfo)
    {
        if (!empty($field->getValue())) {
            $licenseInfo = $this->addLicensesLabel($field->getValue(), $licenseInfo, $field);
        } elseif (!empty($field->getOptionValue())) {
            $licenseInfo = $this->addLicensesLabel($field->getOptionValue()->getOptionValue(), $licenseInfo, $field);
        }

        return $licenseInfo;
    }

    /**
     * @param $value
     * @param $licenseInfo
     * @param LicensesDynamicFieldValue $field
     * @return string
     */
    private function addLicensesLabel($value, $licenseInfo, LicensesDynamicFieldValue $field)
    {

        if ($field->getField()->getUseLabel() and !$field->getField()->getLabelAfter()) {
            $licenseInfo = $this->addLicensesLabelBeforeValue(
                $value,
                $field->getField()->getLabelDescription(),
                $licenseInfo
            );
        } elseif ($field->getField()->getLabelAfter()) {
            $licenseInfo = $this->addLicensesLabelAfterValue(
                $value,
                $field->getField()->getLabelDescription(),
                $licenseInfo
            );
        } elseif (!$field->getField()->getUseLabel()) {
            $licenseInfo .= $value.", ";
        }

        return $licenseInfo;
    }

    /**
     * @param $value
     * @param $label
     * @param $licenseInfo
     * @return string
     */
    private function addLicensesLabelBeforeValue($value, $label, $licenseInfo)
    {
        $licenseInfo .= $label . $value.", ";

        return $licenseInfo;
    }

    /**
     * @param $value
     * @param $label
     * @param $licenseInfo
     * @return string
     */
    private function addLicensesLabelAfterValue($value, $label, $licenseInfo)
    {
        $licenseInfo .= $value." ".$label.", ";

        return $licenseInfo;
    }
}
