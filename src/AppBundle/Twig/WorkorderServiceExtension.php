<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Service;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderFrozenContent;

class WorkorderServiceExtension extends \Twig_Extension
{
    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter(
                'is_added_from_ios',
                array($this, 'isAddedFromIos')
            ),
        );
    }

    /**
     * @param Workorder $workorder
     * @return bool
     */
    public function isAddedFromIos(Workorder $workorder)
    {
        if ($workorder->getIsFrozen()) {
            /** @var WorkorderFrozenContent[] $services */
            $services = $workorder->getFrozenContent();

            /** @var WorkorderFrozenContent $service */
            foreach ($services as $service) {
                $serviceHistory = $service->getServiceHistory();
                if ($serviceHistory->getAddedFromIos()) {
                    return true;
                }
            }
        } else {
            /** @var Service[] $services */
            $services = $workorder->getServices();

            /** @var Service $service */
            foreach ($services as $service) {
                if ($service->getAddedFromIos()) {
                    return true;
                }
            }
        }

        return false;
    }
}
