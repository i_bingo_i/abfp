<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Proposal;
use AppBundle\Entity\Service;
use AppBundle\Entity\Workorder;

class RepairServiceExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('can_edit_repair_service_comment', [$this, 'opportunityToEditComment']),
            new \Twig_SimpleFilter(
                'blocked_edit_repair_service_comment_under_proposal',
                [$this, 'blockedToEditCommentUnderProposal']
            ),
        ];
    }

    /**
     * @param Service $service
     * @return bool
     */
    public function opportunityToEditComment(Service $service)
    {
        /** @var Proposal $proposal */
        $proposal = $service->getProposal();
        /** @var Workorder $workorder */
        $workorder = $service->getWorkorder();
        $serviceStatusAlias = $service->getStatus()->getAlias();

        $denyStatuses = ['deleted', 'discarded', 'resolved'];

        if (in_array($serviceStatusAlias, $denyStatuses)) {
            return false;
        }

        if (($proposal instanceof Proposal and $proposal->getIsFrozen()) and !$workorder instanceof Workorder) {
            return false;
        }

        if ($workorder instanceof Workorder and $workorder->getIsFrozen()) {
            return false;
        }

        return true;
    }

    /**
     * @param Service $service
     * @return bool
     */
    public function blockedToEditCommentUnderProposal(Service $service)
    {
        /** @var Proposal $proposal */
        $proposal = $service->getProposal();
        /** @var Workorder $workorder */
        $workorder = $service->getWorkorder();

        if (($proposal instanceof Proposal and $proposal->getIsFrozen()) and !$workorder instanceof Workorder) {
            return true;
        }

        return false;
    }
}
