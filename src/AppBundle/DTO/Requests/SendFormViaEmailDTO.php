<?php

namespace AppBundle\DTO\Requests;

use AppBundle\Entity\LetterAttachment;
use AppBundle\Entity\Workorder;

class SendFormViaEmailDTO
{
    private $attachments = [];
    private $workorder;
    private $emailBody;
    private $emailSubject;
    private $from;
    private $to;

    /**
     * @param LetterAttachment $attachment
     */
    public function addAttachment(LetterAttachment $attachment)
    {
        $this->attachments[] = $attachment;
    }

    /**
     * @return array
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param Workorder $workorder
     */
    public function setWorkorder(Workorder $workorder)
    {
        $this->workorder = $workorder;
    }

    /**
     * @return mixed
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @param $emailBody
     */
    public function setEmailBody($emailBody)
    {
        $this->emailBody = $emailBody;
    }

    /**
     * @return mixed
     */
    public function getEmailBody()
    {
        return $this->emailBody;
    }

    /**
     * @param $emailSubject
     */
    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;
    }

    /**
     * @return mixed
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * @param $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }
}