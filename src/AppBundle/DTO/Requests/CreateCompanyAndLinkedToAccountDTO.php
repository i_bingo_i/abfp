<?php

namespace AppBundle\DTO\Requests;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\Account;

class CreateCompanyAndLinkedToAccountDTO
{
    /** @var Company */
    private $company;
    /** @var Address */
    private $billingAddress;
    /** @var Account */
    private $account;

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Address $billingAddress
     */
    public function setBillingAddress(Address $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param Account $account
     *
     * @return bool
     */
    public function setAccount(Account $account)
    {
        $this->account = $account;

        return true;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }
}
