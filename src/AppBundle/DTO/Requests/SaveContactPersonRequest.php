<?php

namespace AppBundle\DTO\Requests;

use AppBundle\Entity\Address;
use AppBundle\Entity\ContactPerson;

class SaveContactPersonRequest
{
    /** @var ContactPerson */
    private $contactPerson;
    /** @var Address */
    private $billingAddress;

    /**
     * @param ContactPerson $contactPerson
     */
    public function setContactPerson(ContactPerson $contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return ContactPerson
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param Address $billingAddress
     */
    public function setBillingAddress(Address $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }
}
