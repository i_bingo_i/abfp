<?php

namespace AppBundle\DTO\Requests;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;

class CreateCompanyAndLinkedToCPDTO
{
    /** @var Company */
    private $company;
    /** @var Address */
    private $billingAddress;
    /** @var ContactPerson */
    private $contactPerson;

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Address $billingAddress
     */
    public function setBillingAddress(Address $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param ContactPerson $contactPerson
     *
     * @return bool
     */
    public function setContactPerson(ContactPerson $contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return true;
    }

    /**
     * @return ContactPerson
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }
}
