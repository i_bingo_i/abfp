<?php

namespace AppBundle\DTO\Requests;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;

class SaveCompanyRequest
{
    /** @var Company */
    private $company;
    /** @var Address */
    private $billingAddress;

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Address $billingAddress
     */
    public function setBillingAddress(Address $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }
}
