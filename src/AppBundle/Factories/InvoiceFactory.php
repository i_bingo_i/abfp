<?php

namespace AppBundle\Factories;

use InvoiceBundle\Entity\Invoices;
use AppBundle\Entity\Workorder;

class InvoiceFactory
{
    /**
     * @param Workorder $workorder
     * @param string $file
     * @return Invoices
     */
    public static function made(Workorder $workorder, string $file)
    {
        $invoice = new Invoices();
        $invoice->setFile($file);
        $invoice->setWorkorder($workorder);

        return $invoice;
    }
}
