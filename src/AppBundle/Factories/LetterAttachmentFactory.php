<?php

namespace AppBundle\Factories;

use AppBundle\Entity\File;
use AppBundle\Entity\LetterAttachment;

class LetterAttachmentFactory
{
    /**
     * @param File $file
     *
     * @return LetterAttachment
     */
    public static function make(File $file)
    {
        $newAttach = new LetterAttachment();
        $newAttach->setFile($file);

        return $newAttach;
    }
}
