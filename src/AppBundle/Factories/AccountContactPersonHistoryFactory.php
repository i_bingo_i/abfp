<?php

namespace AppBundle\Factories;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\AccountContactPersonHistory;
use AppBundle\Entity\AddressHistory;
use AppBundle\Entity\ContactPersonHistory;

class AccountContactPersonHistoryFactory
{
    /**
     * @param AccountContactPerson $accountContactPerson
     * @param ContactPersonHistory $contactPersonHistory
     * @param AddressHistory $sendingAddressHistory
     * @param null $user
     * @return AccountContactPersonHistory
     */
    public static function make(
        AccountContactPerson $accountContactPerson,
        ContactPersonHistory $contactPersonHistory,
        ?AddressHistory $sendingAddressHistory = null,
        $user = null
    ) {
        $newHistoryItem = new AccountContactPersonHistory();
        $newHistoryItem->setAuthor($user);
        $newHistoryItem->setOwnerEntity($accountContactPerson);
        $newHistoryItem->setContactPerson($contactPersonHistory);
        $newHistoryItem->setAccount($accountContactPerson->getAccount());
        $newHistoryItem->setAccess($accountContactPerson->getAccess());
        $newHistoryItem->setAccessPrimary($accountContactPerson->getAccessPrimary());
        $newHistoryItem->setAuthorizer($accountContactPerson->getAuthorizer());
        $newHistoryItem->setPayment($accountContactPerson->getPayment());
        $newHistoryItem->setPaymentPrimary($accountContactPerson->getPaymentPrimary());
        $newHistoryItem->setSendingAddress($sendingAddressHistory);
        $newHistoryItem->setCustomBillingAddress($accountContactPerson->getCustomBillingAddress());

        $newHistoryItem->setDeleted($accountContactPerson->getDeleted());
        $newHistoryItem->setDateCreate($accountContactPerson->getDateCreate());
        $newHistoryItem->setDateUpdate($accountContactPerson->getDateUpdate());
        $newHistoryItem->setNotes($accountContactPerson->getNotes());
        $newHistoryItem->setCustomer($accountContactPerson->getCustomer());

        return $newHistoryItem;
    }
}
