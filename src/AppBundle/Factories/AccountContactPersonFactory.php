<?php

namespace AppBundle\Factories;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\ContactPerson;

class AccountContactPersonFactory
{
    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @return AccountContactPerson
     */
    public static function make(Account $account, ContactPerson $contactPerson)
    {
        $acp = new AccountContactPerson();
        $acp->setAccount($account);
        $acp->setContactPerson($contactPerson);
        $acp->setAuthorizer(true);
        $acp->setPaymentPrimary(false);

        return $acp;
    }
}
