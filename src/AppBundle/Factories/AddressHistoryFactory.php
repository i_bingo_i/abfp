<?php

namespace AppBundle\Factories;

use AppBundle\Entity\Address;
use AppBundle\Entity\AddressHistory;

class AddressHistoryFactory
{
    /**
     * @param Address $address
     *
     * @return AddressHistory
     */
    public static function make(Address $address)
    {
        /** @var AddressHistory $addressHistory */
        $addressHistory = new AddressHistory();

        $addressHistory->setState($address->getState());
        $addressHistory->setAddressType($address->getAddressType());
        $addressHistory->setOwnerEntity($address);
        $addressHistory->setAddress($address->getAddress());
        $addressHistory->setCity($address->getCity());
        $addressHistory->setZip($address->getZip());
        $addressHistory->setDateCreate($address->getDateCreate());
        $addressHistory->setDateUpdate($address->getDateUpdate());

        return $addressHistory;
    }
}
