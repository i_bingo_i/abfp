<?php

namespace AppBundle\Factories;

use AppBundle\Entity\AccountHistory;
use AppBundle\Entity\DeviceHistory;
use AppBundle\Entity\ServiceHistory;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderFrozenContent;

class WorkorderFrozenContentFactory
{
    /**
     * @param Workorder $workorder
     * @param AccountHistory $accountHistory
     * @param DeviceHistory $deviceHistory
     * @param ServiceHistory $serviceHistory
     * @return WorkorderFrozenContent
     */
    public static function make(
        Workorder $workorder,
        AccountHistory $accountHistory,
        DeviceHistory $deviceHistory,
        ServiceHistory $serviceHistory
    ) {
        /** @var WorkorderFrozenContent $workorderFrozenContent */
        $workorderFrozenContent = new WorkorderFrozenContent();
        $workorderFrozenContent->setWorkorder($workorder);
        $workorderFrozenContent->setAccountHistory($accountHistory);
        $workorderFrozenContent->setDeviceHistory($deviceHistory);
        $workorderFrozenContent->setServiceHistory($serviceHistory);

        return $workorderFrozenContent;
    }
}
