<?php

namespace AppBundle\Factories;

use AppBundle\Entity\File;

class FileFactory
{
    /**
     * @param $filePath
     * @param int $fileSize
     *
     * @return File
     */
    public static function make($filePath, $fileSize = 0)
    {
        /** @var File $newFile */
        $newFile = new File();
        $newFile->setFile($filePath);
        $newFile->setSize($fileSize);

        return $newFile;
    }
}
