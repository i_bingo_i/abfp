<?php

namespace AppBundle\Parsers\Company\Form;

use AppBundle\DTO\Requests\CreateCompanyAndLinkedToAccountDTO;
use AppBundle\Entity\Account;
use AppBundle\Entity\Address;
use Symfony\Component\Form\Form;

class CreateAndLinkedToAccountParser
{
    /**
     * @param Form $form
     * @param Account $account
     *
     * @return CreateCompanyAndLinkedToAccountDTO
     */
    public function parse(Form $form, Account $account)
    {
        $createAndLinkedToAccountRequest = new CreateCompanyAndLinkedToAccountDTO();
        $createAndLinkedToAccountRequest->setCompany($form->get('company')->getData());
        $createAndLinkedToAccountRequest->setAccount($account);

        /** @var Address $address */
        $address = $form->get('billingAddress')->getData();
        $createAndLinkedToAccountRequest->setBillingAddress($address);

        return $createAndLinkedToAccountRequest;
    }
}
