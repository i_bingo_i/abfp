<?php

namespace AppBundle\Parsers\Company\Form;

use AppBundle\DTO\Requests\CreateCompanyAndLinkedToCPDTO;
use AppBundle\Entity\Address;
use AppBundle\Entity\ContactPerson;
use Symfony\Component\Form\Form;

class CreateAndLinkedToCpParser
{
    /**
     * @param Form $form
     * @param ContactPerson $contactPerson
     *
     * @return CreateCompanyAndLinkedToCPDTO
     */
    public function parse(Form $form, ContactPerson $contactPerson)
    {
        $createAndLinkedToCpRequest = new CreateCompanyAndLinkedToCPDTO();
        $createAndLinkedToCpRequest->setCompany($form->get('company')->getData());
        $createAndLinkedToCpRequest->setContactPerson($contactPerson);

        /** @var Address $address */
        $address = $form->get('billingAddress')->getData();
        $createAndLinkedToCpRequest->setBillingAddress($address);

        return $createAndLinkedToCpRequest;
    }
}
