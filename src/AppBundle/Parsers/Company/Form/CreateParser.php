<?php

namespace AppBundle\Parsers\Company\Form;

use AppBundle\DTO\Requests\SaveCompanyRequest;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use Symfony\Component\Form\Form;

class CreateParser
{
    /**
     * @param Form $form
     *
     * @return SaveCompanyRequest
     */
    public function parse(Form $form)
    {
        $saveCompanyRequest = new SaveCompanyRequest();
        /** @var Company $company */
        $company = $form->get('company')->getData();
        $saveCompanyRequest->setCompany($company);

        /** @var Address $address */
        $address = $form->get('billingAddress')->getData();
        $saveCompanyRequest->setBillingAddress($address);

        return $saveCompanyRequest;
    }
}
