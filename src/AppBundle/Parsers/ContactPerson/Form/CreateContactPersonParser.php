<?php

namespace AppBundle\Parsers\ContactPerson\Form;

use AppBundle\DTO\Requests\SaveContactPersonRequest;
use AppBundle\Entity\Address;
use AppBundle\Entity\Repository\AddressTypeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Form;

class CreateContactPersonParser
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->addressTypeRepository = $this->objectManager->getRepository('AppBundle:AddressType');
    }

    /**
     * @param Form $form
     *
     * @return SaveContactPersonRequest
     */
    public function parse(Form $form)
    {
        /** @var SaveContactPersonRequest $createContactPersonDTO */
        $createContactPersonDTO = new SaveContactPersonRequest();
        $createContactPersonDTO->setContactPerson($form->get('contactPerson')->getData());

        /** @var Address $address */
        $address = $form->get('billingAddress')->getData();
        $createContactPersonDTO->setBillingAddress($address);

        return $createContactPersonDTO;
    }
}
