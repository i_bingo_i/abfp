<?php

namespace AppBundle\Parsers;

use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use AppBundle\Entity\EntityManager\FileManager;
use AppBundle\Entity\File;
use AppBundle\Entity\Workorder;
use AppBundle\Factories\FileFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\LetterAttachment;
use AppBundle\Factories\LetterAttachmentFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\Files;

class FormSendViaEmail
{
    /** @var FileManager */
    private $fileManager;
    /** @var ContainerInterface */
    private $container;
    /** @var  EntityManager */
    private $entityManager;
    /** @var Files */
    private $fileService;

    /**
     * FormSendInvoiceViaEmail constructor.
     * @param ContainerInterface $container
     * @param EntityManager $entityManager
     */
    public function __construct(ContainerInterface $container, EntityManager $entityManager)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->fileManager = $this->container->get("app.file_wo_attacment.manager");
        $this->fileService = $this->container->get("app.files");
    }

    /**
     * @param $data
     * @return SendFormViaEmailDTO
     */
    public function parse($data)
    {
        $sendInvoiceViaEmailDTO = new SendFormViaEmailDTO();

        if (!isset($data["workorder"]))
        {
            // parse error
        }

        /** @var Workorder $workorder */
        $workorder = $data["workorder"];
        $sendInvoiceViaEmailDTO->setWorkorder($workorder);
        if (isset($data["from"])) {
            $sendInvoiceViaEmailDTO->setFrom($data["from"]);
        }

        if (isset($data["to"])) {
            $sendInvoiceViaEmailDTO->setTo($data["to"]);
        }

        if (isset($data["emailSubject"])) {
            $sendInvoiceViaEmailDTO->setEmailSubject($data["emailSubject"]);
        }

        if (isset($data["emailBody"])) {
            $sendInvoiceViaEmailDTO->setEmailBody($data["emailBody"]);
        }

        if (isset($data["file"]) and !empty($data["file"])) {

            $this->parseFile($data["file"], $sendInvoiceViaEmailDTO);
        }

        if (isset($data["attachedFiles"]) and !empty($data["attachedFiles"])) {
            $this->parseAttachedFiles($data["attachedFiles"], $sendInvoiceViaEmailDTO);
        }

        return $sendInvoiceViaEmailDTO;
    }

    /**
     * @param $uploadedFiles
     * @param SendFormViaEmailDTO $sendInvoiceViaEmailDTO
     */
    private function parseFile($uploadedFiles, SendFormViaEmailDTO $sendInvoiceViaEmailDTO)
    {
        /** @var UploadedFile $uploadedFile */
        foreach ($uploadedFiles as $uploadedFile) {
            if ($uploadedFile) {
                $newName = $this->createFileName($uploadedFile);
                /** @var File $file */
                $file = $this->fileManager->createByUpload($uploadedFile, '', $newName);
                $sendInvoiceViaEmailDTO->addAttachment($this->createAttachment($file));
            }
        }
    }

    /**
     * @param $attachedFiles
     * @param SendFormViaEmailDTO $sendInvoiceViaEmailDTO
     */
    private function parseAttachedFiles($attachedFiles, SendFormViaEmailDTO $sendInvoiceViaEmailDTO)
    {
        foreach ($attachedFiles as $filePath) {
            if ($filePath) {
                /** @var File $file */
                $file = FileFactory::make($filePath, $this->fileService->getExistFileSize($filePath));
                $sendInvoiceViaEmailDTO->addAttachment($this->createAttachment($file));
            }
        }
    }

    /**
     * @param File $file
     * @return LetterAttachment
     */
    private function createAttachment(File $file)
    {
        /** @var LetterAttachment $attachment */
        $attachment = LetterAttachmentFactory::make($file);
        $this->entityManager->persist($file);
        $this->entityManager->persist($attachment);

        return $attachment;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return string
     */
    private function createFileName(UploadedFile $uploadedFile)
    {
        $originalExtention = $uploadedFile->getClientOriginalExtension();
        $originalFullName = $uploadedFile->getClientOriginalName();
        $originalName = str_replace(".".$originalExtention, "", $originalFullName);

        $newName = $originalName."_".(new \DateTime())->format("YmdHis").".".$originalExtention;

        return $newName;
    }
}
