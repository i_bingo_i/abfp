<?php

namespace AppBundle\Security;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use AppBundle\Services\Account\Authorizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomProposalVoter extends Voter
{
    const CREATE = 'create_custom_proposal';

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CREATE,
        ])) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $proposalFormData = $subject;

        switch ($attribute) {
            case self::CREATE:
                return $this->canCreateCustomProposal($proposalFormData);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $proposalFormData
     * @return bool
     */
    private function canCreateCustomProposal($proposalFormData)
    {
        /** @var DeviceCategory $selectedProposalDivision */
        $selectedProposalDivision = $proposalFormData["division"];
        $accountDivisions = $proposalFormData["accountDivisions"];
        /** @var Account $account */
        $account = $proposalFormData["account"];

        //check division
        $accountDivisionsNames = array();
        foreach ($accountDivisions as $key => $value) {
            $accountDivisionsNames[] = $key;
        }

        // if selected division for custom proposal is not in account divisions
        if (!in_array($selectedProposalDivision->getAlias(), $accountDivisionsNames)) {
            return false;
        }

        //check by error authorizer address
        /** @var Authorizer $proposalAddressService */
        $accountAuthorizerService = $this->container->get('app.account_authorizer.service');

        if ($accountAuthorizerService->checkForErrorAddress($account)) {
            return false;
        }

        return true;
    }
}
