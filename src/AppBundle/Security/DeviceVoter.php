<?php

namespace AppBundle\Security;

use AppBundle\Entity\Device;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\RepairDeviceInfo as RepairDeviceInfoRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Doctrine\Common\Persistence\ObjectManager;

class DeviceVoter extends Voter
{
    /** @var ContainerInterface */
    private $container;
    /** @var ObjectManager */
    private $objectManager;
    /** @var object|\Symfony\Component\HttpFoundation\Session\Session */
    private $session;
    /** @var RepairDeviceInfoRepository */
    private $repairDeviceInfoRepository;

    const CAN_NOT_DELETE_DEVICE_FROM_PROPOSAL = 'can_not_delete_device_from_proposal';

    /**
     * DeviceVoter constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->repairDeviceInfoRepository = $objectManager->getRepository("AppBundle:RepairDeviceInfo");
        $this->session = $this->container->get('session');
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @return bool
     */
    protected function supports($attribute, $params)
    {
        if (!in_array($attribute, [
            self::CAN_NOT_DELETE_DEVICE_FROM_PROPOSAL,
        ])) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $params, TokenInterface $token)
    {
        switch ($attribute) {
            case self::CAN_NOT_DELETE_DEVICE_FROM_PROPOSAL:
                return $this->canNotDeleteDeviceFromProposal($params);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $params
     * @return bool
     */
    private function canNotDeleteDeviceFromProposal($params)
    {
        //check if user tries to delete the device a second time from any Proposal

        if (!isset($params['device']) and !isset($params['proposal'])) {
            $this->setSessionError("Some params were missed");
            return false;
        }

        if ($this->checkOnNotCreatedDevice($params['device'])) {
            return false;
        }

        if ($this->checkOnNotCreatedProposal($params['proposal'])) {
            return false;
        }

        $repairDeviceInfo = $this->repairDeviceInfoRepository->findOneBy(
            [
                "device" => $params['device'],
                "proposal" => $params['proposal']
            ]
        );

        if (!$repairDeviceInfo instanceof RepairDeviceInfo) {
            $this->setSessionError("Device has been already removed from this document");
            return false;
        }

        return true;
    }

    /**
     * @param $device
     * @return bool
     */
    private function checkOnNotCreatedDevice($device)
    {
        if (!$device instanceof Device) {
            $this->setSessionError("This Device has not created yet.");

            return true;
        }

        return false;
    }

    /**
     * @param $proposal
     * @return bool
     */
    private function checkOnNotCreatedProposal($proposal)
    {
        if (!$proposal instanceof Proposal) {
            $this->setSessionError("This Proposal has not created yet.");

            return true;
        }

        return false;
    }

    /**
     * @param $message
     */
    private function setSessionError($message)
    {
        $this->session->getFlashBag()->add("failed", $message);
    }
}
