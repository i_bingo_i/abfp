<?php

namespace AppBundle\Security;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalType;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProposalVoter extends Voter
{
    const CUSTOM_TYPE = 'custom';
    const REPAIR_TYPE = 'repair';
    const RETEST_TYPE = 'retest';

    const MAIL_EMAIL_ACTION = 'mail_email_action';
    const CHECK_AUTHORIZER_FOR_PROPOSAL = "check_authorizer_for_proposal";

    /** @var ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var ContainerInterface */
    private $container;
    /** @var object|\Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    /**
     * ProposalVoter constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->session = $this->container->get('session');
        $this->accountContactPersonRepository = $this->objectManager->getRepository("AppBundle:AccountContactPerson");
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
           self::CUSTOM_TYPE,
           self::REPAIR_TYPE,
           self::RETEST_TYPE,
           self::MAIL_EMAIL_ACTION,
           self::CHECK_AUTHORIZER_FOR_PROPOSAL,
        ])) {
            return false;
        }

        if (!$subject instanceof Proposal) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var Proposal $proposal */
        $proposal = $subject;
        /** @var ProposalType $proposalTypeAlias */
        $proposalTypeAlias = $proposal->getType()->getAlias();

        switch ($attribute) {
            case self::CUSTOM_TYPE:
                return $this->canViewCustom($proposalTypeAlias);
            case self::REPAIR_TYPE:
                return $this->canViewRepair($proposalTypeAlias);
            case self::RETEST_TYPE:
                return $this->canViewRetest($proposalTypeAlias);
            case self::MAIL_EMAIL_ACTION:
                return $this->canSendMailAndEmailRetestProposal($proposal);
            case self::CHECK_AUTHORIZER_FOR_PROPOSAL:
                return $this->checkAuthorizerForProposal($proposal);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $proposalTypeAlias
     * @return bool
     */
    private function canViewCustom($proposalTypeAlias)
    {
        if ($proposalTypeAlias == self::CUSTOM_TYPE) {
            return true;
        }

        return false;
    }

    /**
     * @param $proposalTypeAlias
     * @return bool
     */
    private function canViewRepair($proposalTypeAlias)
    {
        if ($proposalTypeAlias == self::REPAIR_TYPE) {
            return true;
        }

        return false;
    }

    /**
     * @param $proposalTypeAlias
     * @return bool
     */
    private function canViewRetest($proposalTypeAlias)
    {
        if ($proposalTypeAlias == self::RETEST_TYPE) {
            return true;
        }

        return false;
    }


    /**
     * @param Proposal $proposal
     * @return bool
     */
    private function canSendMailAndEmailRetestProposal(Proposal $proposal)
    {
        /** @var ProposalType $proposalTypeAlias */
        $proposalTypeAlias = $proposal->getType()->getAlias();

        $countProposalServices = $proposal->getServicesCount();

        if ($countProposalServices > 0) {
            return true;
        }

        // in the custom proposal can not be services
        if ($proposalTypeAlias == "custom") {
            return true;
        }

        return false;
    }

    /**
     * @param Proposal $proposal
     * @return bool
     */
    private function checkAuthorizerForProposal(Proposal $proposal)
    {
        /** @var Account $account */
        $account = $proposal->getAccount();
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $proposal->getDivision();
        /** @var AccountContactPerson $accountContactPerson */
        $accountContactPerson =
            $this->accountContactPersonRepository->findAuthorizerByDivision($account, $deviceCategory);

        if ($accountContactPerson instanceof AccountContactPerson) {
            return true;
        }

        $this->setSessionError('You can not do this action without Account Authorizer');
        return false;
    }

    /**
     * @param $message
     */
    private function setSessionError($message)
    {
        $this->session->getFlashBag()->add("failed", $message);
    }
}
