<?php

namespace AppBundle\Security;

use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CompanyVoter extends Voter
{
    const CREATE = 'create';
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const ADD_ACCOUNT = 'add_account';
    const ADD_CONTACT = 'add_contact';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CREATE,
            self::VIEW,
            self::EDIT,
            self::DELETE,
            self::ADD_ACCOUNT,
            self::ADD_CONTACT,
        ])) {
            return false;
        }

        if (!$subject instanceof Company) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Company $company */
        $company = $subject;

        switch ($attribute) {
            case self::DELETE:
                return $this->canDelete($company, $user);
            case self::EDIT:
                return $this->canEdit($company, $user);
            case self::ADD_ACCOUNT:
                return $this->canAddAccount($company, $user);
            case self::ADD_CONTACT:
                return $this->canAddContact($company, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param Company $company
     * @param User $user
     * @return bool
     */
    private function canDelete(Company $company, User $user)
    {
        if (!$company->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Company $company
     * @param User $user
     * @return bool
     */
    private function canEdit(Company $company, User $user)
    {
        if (!$company->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Company $company
     * @param User $user
     * @return bool
     */
    private function canAddAccount(Company $company, User $user)
    {
        if (!$company->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Company $company
     * @param User $user
     * @return bool
     */
    private function canAddContact(Company $company, User $user)
    {
        if (!$company->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }
}
