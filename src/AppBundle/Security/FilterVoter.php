<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FilterVoter extends Voter
{
    const CHECK_ON_CORRECT_FILTER_FOR_CREATING_RN = 'check_on_correct_filter_for_creating_rn';

    /** @var ContainerInterface */
    private $container;
    /** @var object|\Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    /**
     * FilterVoter constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->session = $this->container->get('session');
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @return bool
     */
    protected function supports($attribute, $params)
    {
        if (!in_array($attribute, [
            self::CHECK_ON_CORRECT_FILTER_FOR_CREATING_RN,
        ])) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $filters, TokenInterface $token)
    {
        switch ($attribute) {
            case self::CHECK_ON_CORRECT_FILTER_FOR_CREATING_RN:
                return $this->checkOnCorrectFilterForCreatingRN($filters);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $filters
     * @return bool
     */
    private function checkOnCorrectFilterForCreatingRN($filters)
    {
        if (!array_key_exists('date', $filters)
            ||(empty($filters['date']['from']) || empty($filters['date']['to']))
        ) {
            $this->session->getFlashBag()->add('error', '');

            return false;
        }

        return true;
    }
}
