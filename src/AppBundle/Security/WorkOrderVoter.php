<?php

namespace AppBundle\Security;

use AppBundle\Entity\Proposal;
use AppBundle\Entity\Repository\WorkorderTypeRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderStatus;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\WorkorderRepository;

class WorkOrderVoter extends Voter
{
    const CAN_SCHEDULING_WO = 'can_scheduling_wo';
    const CAN_RESCHEDULING_WO = 'can_rescheduling_wo';
    const CAN_NOT_ADD_PROPOSAL_TO_WO = 'can_not_add_proposal_to_wo';
    const CAN_NOT_ADD_PROPOSAL_TO_FROZEN_WO = 'can_not_add_proposal_to_frozen_wo';
    const CAN_NOT_ADD_SERVICES_TO_FROZEN_WO = 'can_not_add_services_to_frozen_wo';
    const CAN_NOT_ADD_INSPECTION_TO_FROZEN_WO = 'can_not_add_inspection_to_frozen_wo';

    private const MAX_PRIORITY_WO_STATUS_BY_SCHEDULING = 5;
    private const CAN_RESCHEDULING_WO_STATUSES_ALIAS = ['to_be_done_today'];
    private const PROPOSAL_STATUSES_YOU_CAN_NOT_ADD_TO_WO =
        ['deleted', 'workorder_created', 'replaced', 'services_discarded'];

    /** @var ContainerInterface */
    private $container;
    /** @var object|\Symfony\Component\HttpFoundation\Session\Session */
    private $session;
    /** @var ObjectManager */
    private $objectManager;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var WorkorderTypeRepository */
    private $workorderTypeRepository;

    /**
     * WorkOrderVoter constructor.
     * @param ContainerInterface $container
     * @param ObjectManager $objectManager
     */
    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->session = $this->container->get('session');
        $this->workorderRepository = $this->objectManager->getRepository("AppBundle:Workorder");
        $this->workorderTypeRepository = $this->objectManager->getRepository("AppBundle:WorkorderType");
    }


    /**
     * @param string $attribute
     * @param mixed $params
     * @return bool
     */
    protected function supports($attribute, $params)
    {
        if (!in_array($attribute, [
            self::CAN_SCHEDULING_WO,
            self::CAN_RESCHEDULING_WO,
            self::CAN_NOT_ADD_PROPOSAL_TO_WO,
            self::CAN_NOT_ADD_SERVICES_TO_FROZEN_WO,
            self::CAN_NOT_ADD_INSPECTION_TO_FROZEN_WO,
            self::CAN_NOT_ADD_PROPOSAL_TO_FROZEN_WO,
        ])) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $params, TokenInterface $token)
    {
        switch ($attribute) {
            case self::CAN_SCHEDULING_WO:
                return $this->canSchedulingWo($params);
            case self::CAN_RESCHEDULING_WO:
                return $this->canReschedulingWo($params);
            case self::CAN_NOT_ADD_PROPOSAL_TO_WO:
                return $this->canNotAddProposalToWo($params);
            case self::CAN_NOT_ADD_SERVICES_TO_FROZEN_WO:
                return $this->canNotAddServicesToFrozenWo($params);
            case self::CAN_NOT_ADD_INSPECTION_TO_FROZEN_WO:
                return $this->canNotAddInspectionToFrozenWo($params);
            case self::CAN_NOT_ADD_PROPOSAL_TO_FROZEN_WO:
                return $this->canNotAddProposalToFrozenWo($params);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param array $params
     * @return bool
     */
    private function canSchedulingWo(array $params)
    {
        /** @var Workorder $workorder */
        $workorder = $params['workorder'];

        if (!isset($params['workorder']) and !$params['workorder'] instanceof Workorder) {
            return false;
        }

        /** @var WorkorderStatus $currentWoStatus */
        $currentWoStatus = $workorder->getStatus();
        $currentStatusPriority = $currentWoStatus->getPriority();

        if ($currentStatusPriority > self::MAX_PRIORITY_WO_STATUS_BY_SCHEDULING) {
            $this->setSessionError("You can not do this action");
            return false;
        }

        return true;
    }

    /**
     * @param array $params
     * @return bool
     */
    private function canReschedulingWo(array $params)
    {
        /** @var Workorder $workorder */
        $workorder = $params['workorder'];

        if (!isset($params['workorder']) and !$params['workorder'] instanceof Workorder) {
            return false;
        }

        /** @var WorkorderStatus $currentWoStatus */
        $currentWoStatus = $workorder->getStatus();
        $currentWoStatusAlias = $currentWoStatus->getAlias();

        if (!in_array($currentWoStatusAlias, self::CAN_RESCHEDULING_WO_STATUSES_ALIAS)) {
            $this->setSessionError("You can only do this with status TO BE DONE TODAY.");
            return false;
        }

        return true;
    }

    /**
     * @param $params
     * @return bool
     */
    private function canNotAddProposalToFrozenWo($params)
    {
        if (!isset($params['workorder'], $params['proposal'])) {
            $this->setSessionError('No relevant parameters');
            return false;
        }
        /** @var Workorder $workorder */
        $workorder = $params['workorder'];

        if ($this->checkOnNotCreatedWO($workorder)) {
            return false;
        }

        /** @var Proposal $proposal */
        $proposal = $params['proposal'];
        $partErrorMessageByName = $this->getProposalTypeName($proposal);

        if ($this->checkOnFrozenWO($workorder, $partErrorMessageByName)) {
            return false;
        }

        return true;
    }

    /**
     * @param $params
     * @return bool
     */
    private function canNotAddProposalToWo($params)
    {
        if (!isset($params['proposal'])) {
            $this->setSessionError('No relevant parameters');
            return false;
        }

        /** @var Proposal $proposal */
        $proposal = $params['proposal'];
        $partErrorMessageByName = $this->getProposalTypeName($proposal);
        $proposalStatusAlias = $proposal->getStatus()->getAlias();

        if ($this->checkOnAddEmptyProposalToWo($proposal, $partErrorMessageByName)) {
            return false;
        }

        if ($this->checkOnIncorrectProposalStatusForAddToWO($proposalStatusAlias, $partErrorMessageByName)) {
            return false;
        }

        return true;
    }

    /**
     * @param $workorder
     * @return bool
     */
    private function canNotAddServicesToFrozenWo($workorder)
    {
        if (!$workorder instanceof Workorder) {
            $message = 'This Workorder has not created yet.';
            $this->setSessionError($message);

            return false;
        }

        if ($workorder->getIsFrozen()) {
            $this->setSessionError('Services were not added to Workorder as Workorded was frozen by some user.');

            return false;
        }

        return true;
    }

    /**
     * @param $workorder
     * @return bool
     */
    private function canNotAddInspectionToFrozenWo($workorder)
    {
        if ($this->checkOnNotCreatedWO($workorder)) {
            return false;
        }

        if ($workorder->getIsFrozen()) {
            $this->setSessionError('Inspection was not added to Workorder as Workorded was frozen by some user.');

            return false;
        }

        return true;
    }

    /**
     * @param $workorder
     * @return bool
     */
    private function checkOnNotCreatedWO($workorder)
    {
        if (!$workorder instanceof Workorder) {
            $this->setSessionError('This Workorder has not created yet.');
            return true;
        }

        return false;
    }

    /**
     * @param Workorder $workorder
     * @param $partErrorMessageByProposalType
     * @return bool
     */
    private function checkOnFrozenWO(Workorder $workorder, $partErrorMessageByProposalType)
    {
        if ($workorder->getIsFrozen()) {
            $message =
                $partErrorMessageByProposalType . " " .
                "was not added to Workorder as Workorded was frozen by some user.";

            $this->setSessionError($message);
            return true;
        }

        return false;
    }

    /**
     * @param $proposalStatusAlias
     * @param $partErrorMessageByProposalType
     * @return bool
     */
    private function checkOnIncorrectProposalStatusForAddToWO($proposalStatusAlias, $partErrorMessageByProposalType)
    {
        if (in_array($proposalStatusAlias, self::PROPOSAL_STATUSES_YOU_CAN_NOT_ADD_TO_WO)) {
            $message =
                $partErrorMessageByProposalType . " " . "was not added to Workorder as Proposal have incorrect status.";
            $this->setSessionError($message);
            return true;
        }

        return false;
    }

    /**
     * @param Proposal $proposal
     * @return bool
     */
    private function checkOnAddEmptyProposalToWo(Proposal $proposal, $partErrorMessageByProposalType)
    {
        $services = $proposal->getServices();

        if (count($services) < 1) {
            $message =
                $partErrorMessageByProposalType . " " . "was not added to Workorder as Proposal have not services.";
            $this->setSessionError($message);
            return true;
        }

        return false;
    }

    /**
     * @param Proposal $proposal
     * @return string
     */
    private function getProposalTypeName(Proposal $proposal)
    {
        $proposalTypeAlias = $proposal->getType()->getAlias();

        if ($proposalTypeAlias == 'retest') {
            $partErrorMessageByName = "Retest Notice";
        }
        if ($proposalTypeAlias == 'repair') {
            $partErrorMessageByName = "Proposal";
        }

        return $partErrorMessageByName;
    }

    /**
     * @param $message
     */
    private function setSessionError($message)
    {
        $this->session->getFlashBag()->add("failed", $message);
    }
}
