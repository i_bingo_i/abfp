<?php

namespace AppBundle\Security;

use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ContactPersonVoter extends Voter
{
    const CREATE = 'create';
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const SELECT_COMPANY = 'select_company';
    const SEARCH_COMPANY = 'search_company';
    const SET_ROLES = 'set_roles';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CREATE,
            self::VIEW,
            self::EDIT,
            self::DELETE,
            self::SELECT_COMPANY,
            self::SEARCH_COMPANY,
            self::SET_ROLES,
        ])) {
            return false;
        }

        if (!$subject instanceof ContactPerson) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var ContactPerson $contactPerson */
        $contactPerson = $subject;

        switch ($attribute) {
            case self::DELETE:
                return $this->canDelete($contactPerson, $user);
            case self::EDIT:
                return $this->canEdit($contactPerson, $user);
            case self::SELECT_COMPANY:
                return $this->canSelectCompany($contactPerson, $user);
            case self::SEARCH_COMPANY:
                return $this->canSearchCompany($contactPerson, $user);
            case self::SET_ROLES:
                return $this->canSetRoles($contactPerson, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param ContactPerson $contactPerson
     * @param User $user
     * @return bool
     */
    private function canDelete(ContactPerson $contactPerson, User $user)
    {
        if (!$contactPerson->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param ContactPerson $contactPerson
     * @param User $user
     * @return bool
     */
    private function canEdit(ContactPerson $contactPerson, User $user)
    {
        if (!$contactPerson->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param ContactPerson $contactPerson
     * @param User $user
     * @return bool
     */
    private function canSelectCompany(ContactPerson $contactPerson, User $user)
    {
        if (!$contactPerson->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param ContactPerson $contactPerson
     * @param User $user
     * @return bool
     */
    private function canSearchCompany(ContactPerson $contactPerson, User $user)
    {
        if (!$contactPerson->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param ContactPerson $contactPerson
     * @param User $user
     * @return bool
     */
    private function canSetRoles(ContactPerson $contactPerson, User $user)
    {
        if (!$contactPerson->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }
}
