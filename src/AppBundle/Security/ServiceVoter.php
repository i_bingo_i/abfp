<?php

namespace AppBundle\Security;

use AppBundle\Entity\Proposal;
use AppBundle\Entity\Service;
use AppBundle\Entity\Workorder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ServiceVoter extends Voter
{
    /** @var ContainerInterface */
    private $container;
    /** @var object|\Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    const CAN_NOT_ADD_INSPECTION_TO_WO = 'can_not_add_inspection_to_wo';
    const CAN_NOT_ADD_INSPECTION_TO_PROPOSAL = 'can_not_add_inspection_to_proposal';
    const CAN_NOT_REMOVE_SERVICE_FROM_PROPOSALS = 'can_not_remove_service_from_proposals';
    const OPPORTUNITY_TO_CHANGE_REPAIR_SERVICE_COMMENT = 'opportunity_to_change_repair_service_comment';
    const OPPORTUNITY_TO_DISCARD_REPAIR_SERVICE = 'opportunity_to_discard_repair_service';

    /**
     * ServiceVoter constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->session = $this->container->get('session');
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @return bool
     */
    protected function supports($attribute, $params)
    {
        if (!in_array($attribute, [
            self::CAN_NOT_ADD_INSPECTION_TO_WO,
            self::CAN_NOT_ADD_INSPECTION_TO_PROPOSAL,
            self::CAN_NOT_REMOVE_SERVICE_FROM_PROPOSALS,
            self::OPPORTUNITY_TO_CHANGE_REPAIR_SERVICE_COMMENT,
            self::OPPORTUNITY_TO_DISCARD_REPAIR_SERVICE
        ])) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $params, TokenInterface $token)
    {
        switch ($attribute) {
            case self::CAN_NOT_ADD_INSPECTION_TO_WO:
                return $this->canNotAddInspectionToWo($params);
            case self::CAN_NOT_ADD_INSPECTION_TO_PROPOSAL:
                return $this->canNotAddInspectionToProposal($params);
            case self::CAN_NOT_REMOVE_SERVICE_FROM_PROPOSALS:
                return $this->canNotRemoveServiceFromProposals($params);
            case self::OPPORTUNITY_TO_CHANGE_REPAIR_SERVICE_COMMENT:
                return $this->opportunityToEditRepairServiceComment($params);
            case self::OPPORTUNITY_TO_DISCARD_REPAIR_SERVICE:
                return $this->opportunityToDiscardRepairService($params);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $service
     * @return bool
     */
    private function canNotAddInspectionToWo($service)
    {
        if ($this->checkOnNotCreatedService($service)) {
            return false;
        }

        $serviceStatusAlias = $service->getStatus()->getAlias();

        if ($serviceStatusAlias != 'under_retest_opportunity') {
            $this->setSessionError(
                "Service can not be added to Workorder as service has incorrect status 
                (most likely has been added to another document)"
            );

            return false;
        }

        return true;
    }

    /**
     * @param $service
     * @return bool
     */
    private function canNotAddInspectionToProposal($service)
    {
        if ($this->checkOnNotCreatedService($service)) {
            return false;
        }

        $serviceStatusAlias = $service->getStatus()->getAlias();

        if ($serviceStatusAlias != 'under_retest_opportunity') {
            $this->setSessionError(
                "Service can not be added to Proposal as service has incorrect status 
                (most likely has been added to another document)"
            );

            return false;
        }

        return true;
    }

    /**
     * @param $service
     * @return bool
     */
    private function canNotRemoveServiceFromProposals($service)
    {
        //check if user tries to delete the service a second time from any Proposal

        if ($this->checkOnNotCreatedService($service)) {
            return false;
        }

        $proposal = $service->getProposal();

        if (!$proposal instanceof Proposal) {
            $this->setSessionError("Service has been already removed from this document");
            return false;
        }

        return true;
    }

    /**
     * @param $service
     * @return bool
     */
    private function opportunityToEditRepairServiceComment($service)
    {
        if ($this->checkOnNotCreatedService($service)) {
            return false;
        }

        /** @var Proposal $proposal */
        $proposal = $service->getProposal();
        /** @var Workorder $workorder */
        $workorder = $service->getWorkorder();
        $serviceStatusAlias = $service->getStatus()->getAlias();
        $denyStatuses = ['deleted', 'discarded', 'resolved'];
        $errorMessage = 'You can not edit this Deficiency description';

        if (in_array($serviceStatusAlias, $denyStatuses)) {
            $this->setSessionError($errorMessage);
            return false;
        }

        if (($proposal instanceof Proposal and $proposal->getIsFrozen()) and !$workorder instanceof Workorder) {
            $this->setSessionError($errorMessage);
            return false;
        }

        if ($workorder instanceof Workorder and $workorder->getIsFrozen()) {
            $this->setSessionError($errorMessage);
            return false;
        }

        return true;
    }

    /**
     * @param $service
     * @return bool
     */
    private function opportunityToDiscardRepairService($service)
    {
        if ($this->checkOnNotCreatedService($service)) {
            return false;
        }
        $serviceStatusAlias = $service->getStatus()->getAlias();

        if ($serviceStatusAlias != 'repair_opportunity') {
            $this->setSessionError("Non-frequency service can not be discarded");
            return false;
        }

        return true;
    }

    /**
     * @param $service
     * @return bool
     */
    private function checkOnNotCreatedService($service)
    {
        if (!$service instanceof Service) {
            $this->setSessionError("This Service has not created yet.");

            return true;
        }

        return false;
    }

    /**
     * @param $message
     */
    private function setSessionError($message)
    {
        $this->session->getFlashBag()->add("failed", $message);
    }
}
