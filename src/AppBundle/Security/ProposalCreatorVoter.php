<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProposalCreatorVoter extends Voter
{
    const CHECK_OPPORTUNITY_LIMIT_FOR_CREATING_RN = 'check_opportunity_limit_for_creating_rn';

    /** @var ContainerInterface */
    private $container;
    /** @var object|\Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    /**
     * ProposalCreatorVoter constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->session = $this->container->get('session');
    }

    /**
     * @param $attribute
     * @param $params
     * @return bool
     */
    protected function supports($attribute, $params)
    {
        if (!in_array($attribute, [
            self::CHECK_OPPORTUNITY_LIMIT_FOR_CREATING_RN,
        ])) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $params, TokenInterface $token)
    {
        switch ($attribute) {
            case self::CHECK_OPPORTUNITY_LIMIT_FOR_CREATING_RN:
                return $this->checkOpportunityLimitForCreatingRN($params);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $count
     * @return bool
     */
    private function checkOpportunityLimitForCreatingRN($count)
    {
        if ($count > 501) {
            $this->setSessionError("You selected more than 500 Retest Opportunities for batch Retest Notices creation.
                Please reduce amount of filtered opportunities to be less than 500 and try again.");

            return false;
        }

        return true;
    }

    /**
     * @param $message
     */
    private function setSessionError($message)
    {
        $this->session->getFlashBag()->add("failed", $message);
    }
}
