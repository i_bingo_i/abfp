<?php

namespace AppBundle\Security;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AccountContactPersonVoter extends Voter
{
    const CREATE = 'create';
    const VIEW = 'view';
    const DELETE = 'delete';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE, self::VIEW, self::DELETE))) {
            return false;
        }

        if (!$subject instanceof AccountContactPerson) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var AccountContactPerson $accountContactPerson */
        $accountContactPerson = $subject;

        switch ($attribute) {
            case self::DELETE:
                return $this->canDelete($accountContactPerson, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @param User $user
     * @return bool
     */
    private function canDelete(AccountContactPerson $accountContactPerson, User $user)
    {
        if (!$accountContactPerson->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }
}
