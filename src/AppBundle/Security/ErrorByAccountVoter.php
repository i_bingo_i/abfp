<?php

namespace AppBundle\Security;

use AppBundle\Entity\Account;
use AppBundle\Entity\Message;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Service;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ErrorByAccountVoter extends Voter
{
    const ADD_INSPECTION_TO_WO = 'add_inspection_to_wo';
    const CREATE_REPAIR_PROPOSAL = 'create_repair_proposal';
    const ADD_REPAIR_TO_WO = 'add_repair_to_wo';
    const ADD_INSPECTION_TO_PROPOSAL = 'add_inspection_to_proposal';

    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var MessageRepository */
    private $messageRepository;
    /** @var object|\Symfony\Component\HttpFoundation\Session\Session */
    private $session;

    /**
     * ErrorByAccountVoter constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->messageRepository = $this->objectManager->getRepository('AppBundle:Message');
        $this->session = $this->container->get('session');
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @return bool
     */
    protected function supports($attribute, $params)
    {
        if (!in_array($attribute, [
            self::ADD_INSPECTION_TO_WO,
            self::CREATE_REPAIR_PROPOSAL,
            self::ADD_REPAIR_TO_WO,
            self::ADD_INSPECTION_TO_PROPOSAL
        ])) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $params
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $params, TokenInterface $token)
    {
        /** @var Account $ownerAccount */
        $ownerAccount = $params['ownerAccount'];
        $messages = $this->messageRepository->findErrorByAccount($ownerAccount);

        switch ($attribute) {
            case self::ADD_INSPECTION_TO_WO:
                return $this->canAddServiceToWo($params, $messages);
            case self::CREATE_REPAIR_PROPOSAL:
                return $this->canCreateRepairProposal($messages);
            case self::ADD_REPAIR_TO_WO:
                return $this->canAddRepairToWo($messages);
            case self::ADD_INSPECTION_TO_PROPOSAL:
                return $this->canAddInspectionToProposal($params, $messages);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $params
     * @param $messages
     * @return bool
     */
    private function canAddServiceToWo($params, $messages)
    {
        /** @var Service $service */
        $service = $params['service'];
        $errorMessage = 'Inspection was not added to Workorder due to the issues with related Accounts.';

        if (!empty($messages)) {
            return $this->checkFromInspectionCreate($messages, $errorMessage, $service);
        }

        return true;
    }

    /**
     * @param $params
     * @param $messages
     * @return bool
     */
    private function canAddInspectionToProposal($params, $messages)
    {
        /** @var Service $service */
        $service = $params['service'];
        $errorMessage = 'Inspection was not added to Proposal due to the issues with related Accounts.';

        if (!empty($messages)) {
            return $this->checkFromInspectionCreate($messages, $errorMessage, $service);
        }

        return true;
    }

    /**
     * @param $messages
     * @return bool
     */
    private function canCreateRepairProposal($messages)
    {
        $errorMessage = 'Proposal was not created due to the issues with related Accounts';

        if (!empty($messages)) {
            return $this->checkFromRepairCreate($messages, $errorMessage);
        }

        return true;
    }

    private function canAddRepairToWo($messages)
    {
        $errorMessage = 'Repairs were not added to Workorder due to the issues with related Accounts.';

        if (!empty($messages)) {
            return $this->checkFromRepairCreate($messages, $errorMessage);
        }

        return true;
    }

    /**
     * @param $messages
     * @param $errorMessage
     * @return bool
     */
    private function checkFromRepairCreate($messages, $errorMessage)
    {
        /** @var Message $message */
        foreach ($messages as $message) {
            if (!($message->getAlias() == 'service_no_fee')) {
                $this->setSessionError($errorMessage);
                return false;
            }
        }

        return true;
    }

    /**
     * @param $messages
     * @param $errorMessage
     * @param Service $service
     * @return bool
     */
    private function checkFromInspectionCreate($messages, $errorMessage, Service $service)
    {
        /** @var Message $message */
        foreach ($messages as $message) {
            if (!($message->getAlias() == 'service_no_fee') or !$service->getFixedPrice()) {
                $this->setSessionError($errorMessage);
                return false;
            }
        }

        return true;
    }

    /**
     * @param $message
     * @return bool
     */
    private function setSessionError($message)
    {
        $this->session->getFlashBag()->add("failed", $message);

        return false;
    }
}
