<?php

namespace AppBundle\Security;

use AppBundle\Entity\Account;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AccountVoter extends Voter
{
    const CREATE = 'create';
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const SELECT_COMPANY = 'select_company';
    const SELECT_PARENT = 'select_parent';
    const ADD_CHILD = 'add_child';
    const SEARCH_COMPANY = 'search_company';
    const SEARCH_CHILD = 'search_child';
    const SEARCH_CONTACT = 'search_contact';
    const CREATE_DEVICE = 'create_device';
    const SET_ROLES = 'set_roles';

    /**
     * @var ObjectManager $entityManager
     */
    private $entityManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->entityManager = $objectManager;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array(
            $attribute,
            [
                self::CREATE,
                self::VIEW,
                self::EDIT,
                self::DELETE,
                self::SELECT_COMPANY,
                self::SELECT_PARENT,
                self::ADD_CHILD,
                self::SEARCH_COMPANY,
                self::SEARCH_CHILD,
                self::SEARCH_CONTACT,
                self::CREATE_DEVICE,
                self::SET_ROLES,
            ]
        )) {
            return false;
        }

        if (!$subject instanceof Account) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Account $account */
        $account = $subject;

        switch ($attribute) {
            case self::DELETE:
                return $this->canDelete($account, $user);
            case self::EDIT:
                return $this->canEdit($account, $user);
            case self::SELECT_PARENT:
                return $this->canSelectParent($account, $user);
            case self::SELECT_COMPANY:
                return $this->canSelectCompany($account, $user);
            case self::ADD_CHILD:
                return $this->canAddChild($account, $user);
            case self::SEARCH_COMPANY:
                return $this->canSearchCompany($account, $user);
            case self::SEARCH_CHILD:
                return $this->canSearchChild($account, $user);
            case self::SEARCH_CONTACT:
                return $this->canSearchContact($account, $user);
            case self::CREATE_DEVICE:
                return $this->canCreateDevice($account, $user);
            case self::SET_ROLES:
                return $this->canSetRoles($account, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canDelete(Account $account, User $user)
    {
        if (!$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canEdit(Account $account, User $user)
    {
        if (!$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canSelectCompany(Account $account, User $user)
    {
        if (!$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canSelectParent(Account $account, User $user)
    {
        if ($account->getType()->getAlias() == 'account'
            and !$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canAddChild(Account $account, User $user)
    {
        if ($account->getType()->getAlias() == 'group account'
            and !$account->getDeleted()
            and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canSearchCompany(Account $account, User $user)
    {
        if (!$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canSearchChild(Account $account, User $user)
    {
        if ($account->getType()->getAlias() == 'group account' and !$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canSearchContact(Account $account, User $user)
    {
        if (!$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canCreateDevice(Account $account, User $user)
    {
        if (!$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param User $user
     * @return bool
     */
    private function canSetRoles(Account $account, User $user)
    {
        if (!$account->getDeleted() and $user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return false;
    }
}
