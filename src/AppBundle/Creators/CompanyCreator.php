<?php

namespace AppBundle\Creators;

use AppBundle\Entity\Address;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\Company;

class CompanyCreator
{
    /** @var AddressCreator */
    private $addressCreator;

    public function __construct(AddressCreator $addressCreator)
    {
        $this->addressCreator = $addressCreator;
    }

    /**
     * @return Company
     */
    public function make()
    {
        /** @var Company $company */
        $company = new Company();
        /** @var Address $address */
        $address = $this->addressCreator->make(AddressType::COMPANY_ADDRESS_TYPE);
        $company->setAddress($address);

        return $company;
    }
}
