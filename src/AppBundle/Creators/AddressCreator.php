<?php

namespace AppBundle\Creators;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountType;
use AppBundle\Entity\Address;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\Repository\AddressTypeRepository;
use Doctrine\ORM\EntityManager;

class AddressCreator
{
    /** @var EntityManager */
    private $entityManager;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->addressTypeRepository = $entityManager->getRepository('AppBundle:AddressType');
    }

    /**
     * @param string $typeAlias
     *
     * @return Address
     */
    public function make($typeAlias = '')
    {
        /** @var Address $address */
        $address = new Address();

        if (!empty($typeAlias)) {
            /** @var AddressType $addressType */
            $addressType = $this->addressTypeRepository->findOneBy(['alias' => $typeAlias]);
            $address->setAddressType($addressType);
        }

        return $address;
    }

    /**
     * @param Account $account
     *
     * @return Address
     */
    public function makeShipToByAccount(Account $account)
    {
        /** @var Address $shipToAddress */
        $shipToAddress = $this->make(AddressType::SHIP_TO_ADDRESS);

        $address = $account->getName() . PHP_EOL
            . $account->getAddress()->getAddress();

        $shipToAddress->setAddress($address);
        $shipToAddress->setZip($account->getAddress()->getZip());
        $shipToAddress->setState($account->getAddress()->getState());
        $shipToAddress->setCity($account->getAddress()->getCity());

        return $shipToAddress;
    }
}
