<?php

namespace AppBundle\Creators;

use AdminBundle\Form\AccountContactPersonType;
use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Services\AccountContactPerson\Creator;
use AppBundle\Services\Address\AddressProvider;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Services\Customer\Provider\CustomerACPProvider;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;

class AccountContactPersonFormCreator
{
    /** @var AccountContactPerson */
    public $accountContactPerson;
    /** @var array */
    public $customers;
    /** @var Company */
    public $otherCompany;
    /** @var FormFactory */
    private $formFactory;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var Creator */
    private $creator;
    /** @var CustomerACPProvider */
    private $customerACPProvider;
    /** @var AddressProvider */
    private $addressProvider;
    /** @var CompanyRepository */
    private $companyRepository;

    /**
     * AccountContactPersonFormCreator constructor.
     * @param FormFactory $formFactory
     * @param AccountContactPersonRepository $accountContactPersonRepository
     * @param Creator $creator
     * @param CustomerACPProvider $customerACPProvider
     * @param AddressProvider $addressProvider
     * @param CompanyRepository $companyRepository
     */
    public function __construct(
        FormFactory $formFactory,
        AccountContactPersonRepository $accountContactPersonRepository,
        Creator $creator,
        CustomerACPProvider $customerACPProvider,
        AddressProvider $addressProvider,
        CompanyRepository $companyRepository
    ) {
        $this->formFactory = $formFactory;
        $this->accountContactPersonRepository = $accountContactPersonRepository;
        $this->creator = $creator;
        $this->customerACPProvider = $customerACPProvider;
        $this->addressProvider = $addressProvider;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function makeFormSetRoles(Request $request)
    {
        /** @var Account $account */
        $account = $request->get('account');
        /** @var ContactPerson $contactPerson */
        $contactPerson = $request->get('contactPerson');

        if ($request->get('company')) {
            $this->otherCompany = $this->companyRepository->find($request->get('company'));
        }

        /** @var AccountContactPerson $accountContactPerson */
        $this->accountContactPerson = $this->accountContactPersonRepository->findOneBy([
            'account' => $account,
            'contactPerson' => $contactPerson
        ]);

        if ($this->accountContactPerson and $this->accountContactPerson->getDeleted()) {
            $this->accountContactPerson->setDeleted(false);
        }

        if (!$this->accountContactPerson) {
            $this->accountContactPerson = $this->creator->create($account, $contactPerson);
        }

        if (!$this->accountContactPerson->getCustomBillingAddress()) {
            $this->creator->createCustomBillingAddress($this->accountContactPerson);
        }

        $this->customers = $this->customerACPProvider->getCustomers($this->accountContactPerson, $this->otherCompany);

        if ($this->customers['Other Company'] instanceof Customer) {
            $this->accountContactPerson->setCustomer($this->customers['Other Company']);
            $this->otherCompany = $this->companyRepository->find($this->customers['Other Company']->getEntityId());
        }

        $form = $this->formFactory->create(
            AccountContactPersonType::class,
            $this->accountContactPerson,
            [
                'validation_groups' => ['create'],
                'customers' => $this->customers,
            ]
        );

        return $form;
    }

    /**
     * @return array
     */
    public function getExistCompaniesForACP()
    {
        $existCompany = [
            'Account Company' => null,
            "Person Company" => null,
            'Other Company' => null
        ];
        /** @var Customer $customer */
        foreach ($this->customers as $key => $customer) {
            if ($customer instanceof Customer && $customer->getEntityType()->getAlias() == 'company') {
                $existCompany[$key] = $customer->getId();
            }
        }

        return $existCompany;
    }
}