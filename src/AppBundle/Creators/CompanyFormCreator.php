<?php

namespace AppBundle\Creators;

use AdminBundle\Form\CompanyFormType;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\Company;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Repository\CustomerRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CompanyFormCreator
{
    /** @var ContainerInterface */
    private $container;
    /** @var FormFactoryInterface */
    private $formFactory;
    /** @var CompanyCreator */
    private $companyCreator;
    /** @var AddressCreator */
    private $addressCreator;
    /** @var ObjectManager */
    private $objectManager;

    /**
     * CompanyFormCreator constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->formFactory = $this->container->get('form.factory');
        $this->companyCreator = $this->container->get('company.creator');
        $this->addressCreator = $this->container->get('address.creator');
        $this->objectManager = $this->container->get('app.entity_manager');
    }

    /**
     * @return Form $form
     */
    public function makeFormCreate()
    {
        $initData = [
            'company' => $this->companyCreator->make(),
            'billingAddress' => $this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE)
        ];

        /** @var Form $form */
        $form = $this->formFactory->create(
            CompanyFormType::class,
            $initData,
            ['validation_groups' => ['create_company']]
        );

        return $form;
    }

    /**
     * @param Company $company
     *
     * @return Form
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function makeFormUpdate(Company $company)
    {
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $this->objectManager->getRepository('InvoiceBundle:Customer');
        /** @var Customer $customer */
        $customer = $customerRepository->findByCompany($company);

        $initData = [
            'company' => $company,
            'billingAddress' => $customer->getBillingAddress()
        ];

        /** @var Form $form */
        $form = $this->formFactory->create(
            CompanyFormType::class,
            $initData,
            ['validation_groups' => ['create_company']]
        );

        return $form;
    }
}
