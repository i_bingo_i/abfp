<?php

namespace AppBundle\Creators;

use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\AddressType;

class ContactPersonCreator
{
    /** @var AddressCreator */
    private $addressCreator;

    public function __construct(AddressCreator $addressCreator)
    {
        $this->addressCreator = $addressCreator;
    }

    public function make()
    {
        /** @var ContactPerson $contactPerson */
        $contactPerson = new ContactPerson();

        /** @var Address $address */
        $address = $this->addressCreator->make(AddressType::PERSONAL_ADDRESS_TYPE);
        $contactPerson->setPersonalAddress($address);

        return $contactPerson;
    }
}
