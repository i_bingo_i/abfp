<?php

namespace AppBundle\Creators;

use AdminBundle\Form\ContactPersonFormType;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\ContactPerson;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Repository\CustomerRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ContactPersonFormCreator
{
    /** @var ContainerInterface */
    private $container;
    /** @var FormFactoryInterface */
    private $formFactory;
    /** @var ContactPersonCreator */
    private $contactPersonCreator;
    /** @var AddressCreator */
    private $addressCreator;
    /** @var ObjectManager */
    private $objectManager;

    /**
     * CompanyFormCreator constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->formFactory = $this->container->get('form.factory');
        $this->contactPersonCreator = $this->container->get('contact_person.creator');
        $this->addressCreator = $this->container->get('address.creator');
        $this->objectManager = $this->container->get('app.entity_manager');
    }

    /**
     * @return Form $form
     */
    public function makeFormCreate()
    {
        $initData = [
            'contactPerson' => $this->contactPersonCreator->make(),
            'billingAddress' => $this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE)
        ];

        /** @var Form $form */
        $form = $this->formFactory->create(
            ContactPersonFormType::class,
            $initData,
            ['validation_groups' => ['create_contact_person']]
        );

        return $form;
    }

    /**
     * @param ContactPerson $contactPerson
     * @return Form
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function makeFormUpdate(ContactPerson $contactPerson)
    {
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $this->objectManager->getRepository('InvoiceBundle:Customer');
        /** @var Customer $customer */
        $customer = $customerRepository->findByContactPerson($contactPerson);

        $initData = [
            'contactPerson' => $contactPerson,
            'billingAddress' => $customer->getBillingAddress()
        ];

        /** @var Form $form */
        $form = $this->formFactory->create(
            ContactPersonFormType::class,
            $initData,
            ['validation_groups' => ['create_contact_person']]
        );

        return $form;
    }
}
