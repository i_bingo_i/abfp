<?php

namespace AppBundle\Handlers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Session\Session;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /** @var Router $router */
    private $router;
    /** @var Session $session */
    private $session;

    /**
     * AccessDeniedHandler constructor.
     * @param Router $router
     * @param Session $session
     */
    public function __construct(Router $router, Session $session)
    {
        $this->router = $router;
        $this->session = $session;
    }

    /**
     * @param Request $request
     * @param AccessDeniedException $accessDeniedException
     * @return string
     */
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $errorMessage = "Error: ";
        $errorMessage .= $accessDeniedException->getMessage() . ' You are not allowed to access the Admin Panel.';
        $this->session->getFlashBag()->add('error', $errorMessage);

        return new RedirectResponse($this->router->generate('fos_user_security_login'), 302);
    }
}
