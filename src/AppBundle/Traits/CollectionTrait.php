<?php

namespace AppBundle\Traits;

trait CollectionTrait
{
    /**
     * @param $collection
     * @return array
     */
    private function getIdsArray($collection)
    {
        $ids = [];
        foreach ($collection as $item) {
            $ids[] = $item->getId();
        }

        return array_values($ids);
    }
}
