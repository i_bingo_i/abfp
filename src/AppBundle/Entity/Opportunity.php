<?php

namespace AppBundle\Entity;

/**
 * Opportunity
 */
class Opportunity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $servicesCount;

    /**
     * @var integer
     */
    private $devicesCount;

    /**
     * @var integer
     */
    private $sitesCount;

    /**
     * @var float
     */
    private $price;

    /**
     * @var \DateTime
     */
    private $executionServicesDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set servicesCount
     *
     * @param integer $servicesCount
     *
     * @return Opportunity
     */
    public function setServicesCount($servicesCount)
    {
        $this->servicesCount = $servicesCount;

        return $this;
    }

    /**
     * Get servicesCount
     *
     * @return integer
     */
    public function getServicesCount()
    {
        return $this->servicesCount;
    }

    /**
     * Set devicesCount
     *
     * @param integer $devicesCount
     *
     * @return Opportunity
     */
    public function setDevicesCount($devicesCount)
    {
        $this->devicesCount = $devicesCount;

        return $this;
    }

    /**
     * Get devicesCount
     *
     * @return integer
     */
    public function getDevicesCount()
    {
        return $this->devicesCount;
    }

    /**
     * Set sitesCount
     *
     * @param integer $sitesCount
     *
     * @return Opportunity
     */
    public function setSitesCount($sitesCount)
    {
        $this->sitesCount = $sitesCount;

        return $this;
    }

    /**
     * Get sitesCount
     *
     * @return integer
     */
    public function getSitesCount()
    {
        return $this->sitesCount;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Opportunity
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set executionServicesDate
     *
     * @param \DateTime $executionServicesDate
     *
     * @return Opportunity
     */
    public function setExecutionServicesDate($executionServicesDate)
    {
        $this->executionServicesDate = $executionServicesDate;

        return $this;
    }

    /**
     * Get executionServicesDate
     *
     * @return \DateTime
     */
    public function getExecutionServicesDate()
    {
        return $this->executionServicesDate;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $services;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $division;

    /**
     * @var \AppBundle\Entity\OpportunityType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\OpportunityStatus
     */
    private $status;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return Opportunity
     */
    public function addService(\AppBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \AppBundle\Entity\Service $service
     */
    public function removeService(\AppBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set division
     *
     * @param \AppBundle\Entity\DeviceCategory $division
     *
     * @return Opportunity
     */
    public function setDivision(\AppBundle\Entity\DeviceCategory $division = null)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * Get division
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\OpportunityType $type
     *
     * @return Opportunity
     */
    public function setType(\AppBundle\Entity\OpportunityType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\OpportunityType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\OpportunityStatus $status
     *
     * @return Opportunity
     */
    public function setStatus(\AppBundle\Entity\OpportunityStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\OpportunityStatus
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;


    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return Opportunity
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }
    /**
     * @var \AppBundle\Entity\Account
     */
    private $parentAccount;


    /**
     * Set parentAccount
     *
     * @param \AppBundle\Entity\Account $parentAccount
     *
     * @return Opportunity
     */
    public function setParentAccount(\AppBundle\Entity\Account $parentAccount = null)
    {
        $this->parentAccount = $parentAccount;

        return $this;
    }

    /**
     * Get parentAccount
     *
     * @return \AppBundle\Entity\Account
     */
    public function getParentAccount()
    {
        return $this->parentAccount;
    }
}
