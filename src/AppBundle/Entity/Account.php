<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Account
 *
 * (don't auto generate this entity)
 * - was added code hasMessage
 */
class Account
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $address;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $devices;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \AppBundle\Entity\AccountType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $parent;

    /**
     * @var \AppBundle\Entity\ClientType
     */
    private $clientType;

    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;

    /**
     * @var \DateTime
     */
    private $oldestOpportunityDate;

    /**
     * @var \DateTime
     */
    private $oldestOpportunityBackflowDate;

    /**
     * @var \DateTime
     */
    private $oldestOpportunityFireDate;

    /**
     * @var \DateTime
     */
    private $oldestOpportunityPlumbingDate;

    /**
     * @var \DateTime
     */
    private $oldestOpportunityAlarmDate;

    /**
     * @var \AppBundle\Entity\Municipality
     */
    private $municipality;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $opportunity;

    /**
     * @var \DateTime
     */
    private $oldestProposalDate;

    /**
     * @var \DateTime
     */
    private $oldestProposalBackflowDate;

    /**
     * @var \DateTime
     */
    private $oldestProposalFireDate;

    /**
     * @var \DateTime
     */
    private $oldestProposalPlumbingDate;

    /**
     * @var \DateTime
     */
    private $oldestProposalAlarmDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $contactPerson;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messages;

    /**
     * @var string
     */
    private $website;

    /**
     * @var boolean
     */
    private $specialDiscount = false;

    /**
     * @var boolean
     */
    private $isHistoryProcessed = false;

    /**
     * @var boolean
     */
    private $isMessagesProcessed = false;

    /**
     * @var string
     */
    private $oldAccountId;

    /**
     * @var string
     */
    private $oldLinkId;

    /**
     * @var \AppBundle\Entity\AccountBuildingType
     */
    private $buildingType;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $oldSystemMasLevelAddress;

    /**
     * @var string
     */
    private $siteSendTo;

    /**
     * @var \AppBundle\Entity\Coordinate
     */
    private $coordinate;

    /**
     * @var \AppBundle\Entity\PaymentTerm
     */
    private $paymentTerm;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->devices = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->opportunity = new ArrayCollection();
        $this->contactPerson = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Account
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Account
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Account
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Account
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return Account
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return Account
     */
    public function addDevice(\AppBundle\Entity\Device $device)
    {
        $this->devices[] = $device;

        return $this;
    }

    /**
     * Remove device
     *
     * @param \AppBundle\Entity\Device $device
     */
    public function removeDevice(\AppBundle\Entity\Device $device)
    {
        $this->devices->removeElement($device);
    }

    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevices()
    {
        return $this->devices;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Account $child
     *
     * @return Account
     */
    public function addChild(\AppBundle\Entity\Account $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Account $child
     */
    public function removeChild(\AppBundle\Entity\Account $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\AccountType $type
     *
     * @return Account
     */
    public function setType(\AppBundle\Entity\AccountType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\AccountType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Account $parent
     *
     * @return Account
     */
    public function setParent(\AppBundle\Entity\Account $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Account
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set clientType
     *
     * @param \AppBundle\Entity\ClientType $clientType
     *
     * @return Account
     */
    public function setClientType(\AppBundle\Entity\ClientType $clientType = null)
    {
        $this->clientType = $clientType;

        return $this;
    }

    /**
     * Get clientType
     *
     * @return \AppBundle\Entity\ClientType
     */
    public function getClientType()
    {
        return $this->clientType;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return Account
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set municipality
     *
     * @param \AppBundle\Entity\Municipality $municipality
     *
     * @return Account
     */
    public function setMunicipality(\AppBundle\Entity\Municipality $municipality = null)
    {
        $this->municipality = $municipality;

        return $this;
    }

    /**
     * Get municipality
     *
     * @return \AppBundle\Entity\Municipality
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }

    /**
     * Add opportunity
     *
     * @param \AppBundle\Entity\Opportunity $opportunity
     *
     * @return Account
     */
    public function addOpportunity(\AppBundle\Entity\Opportunity $opportunity)
    {
        $this->opportunity[] = $opportunity;

        return $this;
    }

    /**
     * Remove opportunity
     *
     * @param \AppBundle\Entity\Opportunity $opportunity
     */
    public function removeOpportunity(\AppBundle\Entity\Opportunity $opportunity)
    {
        $this->opportunity->removeElement($opportunity);
    }

    /**
     * Get opportunity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOpportunity()
    {
        return $this->opportunity;
    }

    /**
     * Set oldestOpportunityDate
     *
     * @param \DateTime $oldestOpportunityDate
     *
     * @return Account
     */
    public function setOldestOpportunityDate($oldestOpportunityDate = null)
    {
        $this->oldestOpportunityDate = $oldestOpportunityDate;

        return $this;
    }

    /**
     * Get oldestOpportunityDate
     *
     * @return \DateTime
     */
    public function getOldestOpportunityDate()
    {
        return $this->oldestOpportunityDate;
    }

    /**
     * Set oldestOpportunityBackflowDate
     *
     * @param \DateTime $oldestOpportunityBackflowDate
     *
     * @return Account
     */
    public function setOldestOpportunityBackflowDate($oldestOpportunityBackflowDate = null)
    {
        $this->oldestOpportunityBackflowDate = $oldestOpportunityBackflowDate;

        return $this;
    }

    /**
     * Get oldestOpportunityBackflowDate
     *
     * @return \DateTime
     */
    public function getOldestOpportunityBackflowDate()
    {
        return $this->oldestOpportunityBackflowDate;
    }

    /**
     * Set oldestOpportunityFireDate
     *
     * @param \DateTime $oldestOpportunityFireDate
     *
     * @return Account
     */
    public function setOldestOpportunityFireDate($oldestOpportunityFireDate = null)
    {
        $this->oldestOpportunityFireDate = $oldestOpportunityFireDate;

        return $this;
    }

    /**
     * Get oldestOpportunityFireDate
     *
     * @return \DateTime
     */
    public function getOldestOpportunityFireDate()
    {
        return $this->oldestOpportunityFireDate;
    }

    /**
     * Set oldestOpportunityPlumbingDate
     *
     * @param \DateTime $oldestOpportunityPlumbingDate
     *
     * @return Account
     */
    public function setOldestOpportunityPlumbingDate($oldestOpportunityPlumbingDate = null)
    {
        $this->oldestOpportunityPlumbingDate = $oldestOpportunityPlumbingDate;

        return $this;
    }

    /**
     * Get oldestOpportunityPlumbingDate
     *
     * @return \DateTime
     */
    public function getOldestOpportunityPlumbingDate()
    {
        return $this->oldestOpportunityPlumbingDate;
    }

    /**
     * Set oldestOpportunityAlarmDate
     *
     * @param \DateTime $oldestOpportunityAlarmDate
     *
     * @return Account
     */
    public function setOldestOpportunityAlarmDate($oldestOpportunityAlarmDate = null)
    {
        $this->oldestOpportunityAlarmDate = $oldestOpportunityAlarmDate;

        return $this;
    }

    /**
     * Get oldestOpportunityAlarmDate
     *
     * @return \DateTime
     */
    public function getOldestOpportunityAlarmDate()
    {
        return $this->oldestOpportunityAlarmDate;
    }

    /**
     * Set oldestProposalDate
     *
     * @param \DateTime $oldestProposalDate
     *
     * @return Account
     */
    public function setOldestProposalDate($oldestProposalDate)
    {
        $this->oldestProposalDate = $oldestProposalDate;

        return $this;
    }

    /**
     * Get oldestProposalDate
     *
     * @return \DateTime
     */
    public function getOldestProposalDate()
    {
        return $this->oldestProposalDate;
    }

    /**
     * Set oldestProposalBackflowDate
     *
     * @param \DateTime $oldestProposalBackflowDate
     *
     * @return Account
     */
    public function setOldestProposalBackflowDate($oldestProposalBackflowDate)
    {
        $this->oldestProposalBackflowDate = $oldestProposalBackflowDate;

        return $this;
    }

    /**
     * Get oldestProposalBackflowDate
     *
     * @return \DateTime
     */
    public function getOldestProposalBackflowDate()
    {
        return $this->oldestProposalBackflowDate;
    }

    /**
     * Set oldestProposalFireDate
     *
     * @param \DateTime $oldestProposalFireDate
     *
     * @return Account
     */
    public function setOldestProposalFireDate($oldestProposalFireDate)
    {
        $this->oldestProposalFireDate = $oldestProposalFireDate;

        return $this;
    }

    /**
     * Get oldestProposalFireDate
     *
     * @return \DateTime
     */
    public function getOldestProposalFireDate()
    {
        return $this->oldestProposalFireDate;
    }

    /**
     * Set oldestProposalPlumbingDate
     *
     * @param \DateTime $oldestProposalPlumbingDate
     *
     * @return Account
     */
    public function setOldestProposalPlumbingDate($oldestProposalPlumbingDate)
    {
        $this->oldestProposalPlumbingDate = $oldestProposalPlumbingDate;

        return $this;
    }

    /**
     * Get oldestProposalPlumbingDate
     *
     * @return \DateTime
     */
    public function getOldestProposalPlumbingDate()
    {
        return $this->oldestProposalPlumbingDate;
    }

    /**
     * Set oldestProposalAlarmDate
     *
     * @param \DateTime $oldestProposalAlarmDate
     *
     * @return Account
     */
    public function setOldestProposalAlarmDate($oldestProposalAlarmDate)
    {
        $this->oldestProposalAlarmDate = $oldestProposalAlarmDate;

        return $this;
    }

    /**
     * Get oldestProposalAlarmDate
     *
     * @return \DateTime
     */
    public function getOldestProposalAlarmDate()
    {
        return $this->oldestProposalAlarmDate;
    }

    /**
     * Add contactPerson
     *
     * @param \AppBundle\Entity\AccountContactPerson $contactPerson
     *
     * @return Account
     */
    public function addContactPerson(\AppBundle\Entity\AccountContactPerson $contactPerson)
    {
        $this->contactPerson[] = $contactPerson;

        return $this;
    }

    /**
     * Remove contactPerson
     *
     * @param \AppBundle\Entity\AccountContactPerson $contactPerson
     */
    public function removeContactPerson(\AppBundle\Entity\AccountContactPerson $contactPerson)
    {
        $this->contactPerson->removeElement($contactPerson);
    }

    /**
     * Get contactPerson
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Message $message
     *
     * @return Account
     */
    public function addMessage(\AppBundle\Entity\Message $message)
    {
        if (!$this->hasMessage($message)) {
            $this->messages[] = $message;
        }

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Message $message)
    {
        if ($this->hasMessage($message)) {
            $this->messages->removeElement($message);
        }
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param Message $message
     * @return bool
     */
    public function hasMessage(Message $message)
    {
        return $this->getMessages()->contains($message);
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Account
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set specialDiscount
     *
     * @param boolean $specialDiscount
     *
     * @return Account
     */
    public function setSpecialDiscount($specialDiscount)
    {
        $this->specialDiscount = $specialDiscount;

        return $this;
    }

    /**
     * Get specialDiscount
     *
     * @return boolean
     */
    public function getSpecialDiscount()
    {
        return $this->specialDiscount;
    }

    /**
     * Set oldAccountId
     *
     * @param string $oldAccountId
     *
     * @return Account
     */
    public function setOldAccountId($oldAccountId)
    {
        $this->oldAccountId = $oldAccountId;

        return $this;
    }

    /**
     * Get oldAccountId
     *
     * @return string
     */
    public function getOldAccountId()
    {
        return $this->oldAccountId;
    }

    /**
     * Set isHistoryProcessed
     *
     * @param boolean $isHistoryProcessed
     *
     * @return Account
     */
    public function setIsHistoryProcessed($isHistoryProcessed)
    {
        $this->isHistoryProcessed = $isHistoryProcessed;

        return $this;
    }

    /**
     * Get isHistoryProcessed
     *
     * @return boolean
     */
    public function getIsHistoryProcessed()
    {
        return $this->isHistoryProcessed;
    }

    /**
     * Set isMessagesProcessed
     *
     * @param boolean $isMessagesProcessed
     *
     * @return Account
     */
    public function setIsMessagesProcessed($isMessagesProcessed)
    {
        $this->isMessagesProcessed = $isMessagesProcessed;

        return $this;
    }

    /**
     * Get isMessagesProcessed
     *
     * @return boolean
     */
    public function getIsMessagesProcessed()
    {
        return $this->isMessagesProcessed;
    }

    /**
     * Set buildingType
     *
     * @param \AppBundle\Entity\AccountBuildingType $buildingType
     *
     * @return Account
     */
    public function setBuildingType(\AppBundle\Entity\AccountBuildingType $buildingType = null)
    {
        $this->buildingType = $buildingType;

        return $this;
    }

    /**
     * Get buildingType
     *
     * @return \AppBundle\Entity\AccountBuildingType
     */
    public function getBuildingType()
    {
        return $this->buildingType;
    }

    /**
     * Set oldLinkId
     *
     * @param string $oldLinkId
     *
     * @return Account
     */
    public function setOldLinkId($oldLinkId)
    {
        $this->oldLinkId = $oldLinkId;

        return $this;
    }

    /**
     * Get oldLinkId
     *
     * @return string
     */
    public function getOldLinkId()
    {
        return $this->oldLinkId;
    }

    /**
     * Set siteSendTo
     *
     * @param string $siteSendTo
     *
     * @return Account
     */
    public function setSiteSendTo($siteSendTo)
    {
        $this->siteSendTo = $siteSendTo;

        return $this;
    }

    /**
     * Get siteSendTo
     *
     * @return string
     */
    public function getSiteSendTo()
    {
        return $this->siteSendTo;
    }

    /**
     * Set oldSystemMasLevelAddress
     *
     * @param \AppBundle\Entity\Address $oldSystemMasLevelAddress
     *
     * @return Account
     */
    public function setOldSystemMasLevelAddress(\AppBundle\Entity\Address $oldSystemMasLevelAddress = null)
    {
        $this->oldSystemMasLevelAddress = $oldSystemMasLevelAddress;

        return $this;
    }

    /**
     * Get oldSystemMasLevelAddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getOldSystemMasLevelAddress()
    {
        return $this->oldSystemMasLevelAddress;
    }

    /**
     * Set coordinate
     *
     * @param \AppBundle\Entity\Coordinate $coordinate
     *
     * @return Account
     */
    public function setCoordinate(\AppBundle\Entity\Coordinate $coordinate = null)
    {
        $this->coordinate = $coordinate;

        return $this;
    }

    /**
     * Get coordinate
     *
     * @return \AppBundle\Entity\Coordinate
     */
    public function getCoordinate()
    {
        return $this->coordinate;
    }

    /**
     * Set paymentTerm
     *
     * @param \AppBundle\Entity\PaymentTerm $paymentTerm
     *
     * @return Account
     */
    public function setPaymentTerm(\AppBundle\Entity\PaymentTerm $paymentTerm = null)
    {
        $this->paymentTerm = $paymentTerm;

        return $this;
    }

    /**
     * Get paymentTerm
     *
     * @return \AppBundle\Entity\PaymentTerm
     */
    public function getPaymentTerm()
    {
        return $this->paymentTerm;
    }
}
