<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\PriceFilterTrait;
use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * Service
 *
 * (don't auto generate this entity)
 * - was added code filtering for fixedPrice and hourlyPrice field
 * - was added code setTestDateValue
 */
class Service
{
    use PriceFilterTrait;
    use DateTimeControlTrait;
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $fixedPrice;

    /**
     * @var float
     */
    private $hourlyPrice;

    /**
     * @var \DateTime
     */
    private $lastTested;

    /**
     * @var \DateTime
     */
    private $inspectionDue;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \AppBundle\Entity\ServiceNamed
     */
    private $named;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $device;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var \AppBundle\Entity\Contractor
     */
    private $companyLastTested;

    /**
     * @var \AppBundle\Entity\Opportunity
     */
    private $opportunity;

    /**
     * @var \DateTime
     */
    private $testDate;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\DepartmentChannel
     */
    private $departmentChannel;

    /**
     * @var \AppBundle\Entity\ServiceStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $proposal;

    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;

    /**
     * @var \AppBundle\Entity\Report
     */
    private $lastReport;

    /**
     * @var \AppBundle\Entity\ContractorService
     */
    private $contractorService;

    /**
     * @var boolean
     */
    private $directlyWO = false;

    /**
     * @var boolean
     */
    private $addedFromIos = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fixedPrice
     *
     * @param float $fixedPrice
     *
     * @return Service
     */
    public function setFixedPrice($fixedPrice)
    {
        $this->fixedPrice = $this->filterPrice($fixedPrice);

        return $this;
    }

    /**
     * Get fixedPrice
     *
     * @return float
     */
    public function getFixedPrice()
    {
        return $this->fixedPrice;
    }

    /**
     * Set hourlyPrice
     *
     * @param float $hourlyPrice
     *
     * @return Service
     */
    public function setHourlyPrice($hourlyPrice)
    {
        $this->hourlyPrice = $this->filterPrice($hourlyPrice);
    }

    /**
     * Get hourlyPrice
     *
     * @return float
     */
    public function getHourlyPrice()
    {
        return $this->hourlyPrice;
    }

    /**
     * Set lastTested
     *
     * @param \DateTime $lastTested
     *
     * @return Service
     */
    public function setLastTested($lastTested)
    {
        $this->lastTested = $lastTested;

        return $this;
    }

    /**
     * Get lastTested
     *
     * @return \DateTime
     */
    public function getLastTested()
    {
        return $this->lastTested;
    }

    /**
     * Set inspectionDue
     *
     * @param \DateTime $inspectionDue
     *
     * @return Service
     */
    public function setInspectionDue($inspectionDue)
    {
        $this->inspectionDue = $inspectionDue;

        return $this;
    }

    /**
     * Get inspectionDue
     *
     * @return string
     */
    public function getInspectionDue()
    {
        return $this->inspectionDue;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Service
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Service
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\ServiceNamed $named
     *
     * @return Service
     */
    public function setNamed(\AppBundle\Entity\ServiceNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\ServiceNamed
     */
    public function getNamed()
    {
        return $this->named;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return Service
     */
    public function setDevice(\AppBundle\Entity\Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return Service
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set companyLastTested
     *
     * @param \AppBundle\Entity\Contractor $companyLastTested
     *
     * @return Service
     */
    public function setCompanyLastTested(\AppBundle\Entity\Contractor $companyLastTested = null)
    {
        $this->companyLastTested = $companyLastTested;

        return $this;
    }

    /**
     * Get companyLastTested
     *
     * @return \AppBundle\Entity\Contractor
     */
    public function getCompanyLastTested()
    {
        return $this->companyLastTested;
    }

    /**
     * Set opportunity
     *
     * @param \AppBundle\Entity\Opportunity $opportunity
     *
     * @return Service
     */
    public function setOpportunity(\AppBundle\Entity\Opportunity $opportunity = null)
    {
        $this->opportunity = $opportunity;

        return $this;
    }

    /**
     * Get opportunity
     *
     * @return \AppBundle\Entity\Opportunity
     */
    public function getOpportunity()
    {
        return $this->opportunity;
    }

    /**
     * Set testDate
     *
     * @param \DateTime $testDate
     *
     * @return Service
     */
    public function setTestDate($testDate)
    {
        $this->testDate = $testDate;

        return $this;
    }

    /**
     * Get testDate
     *
     * @return \DateTime
     */
    public function getTestDate()
    {
        return $this->testDate;
    }

    /**
     * @return $this
     */
    public function setTestDateValue()
    {
        $this->testDate = $this->inspectionDue;

        return $this;
    }

    /**
     * Set departmentChannel
     *
     * @param \AppBundle\Entity\DepartmentChannel $departmentChannel
     *
     * @return Service
     */
    public function setDepartmentChannel(\AppBundle\Entity\DepartmentChannel $departmentChannel = null)
    {
        $this->departmentChannel = $departmentChannel;

        return $this;
    }

    /**
     * Get departmentChannel
     *
     * @return \AppBundle\Entity\DepartmentChannel
     */
    public function getDepartmentChannel()
    {
        return $this->departmentChannel;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\ServiceStatus $status
     *
     * @return Service
     */
    public function setStatus(\AppBundle\Entity\ServiceStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\ServiceStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set proposal
     *
     * @param \AppBundle\Entity\Proposal $proposal
     *
     * @return Service
     */
    public function setProposal(\AppBundle\Entity\Proposal $proposal = null)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return Service
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * Set lastReport
     *
     * @param \AppBundle\Entity\Report $lastReport
     *
     * @return Service
     */
    public function setLastReport(\AppBundle\Entity\Report $lastReport = null)
    {
        $this->lastReport = $lastReport;

        return $this;
    }

    /**
     * Get lastReport
     *
     * @return \AppBundle\Entity\Report
     */
    public function getLastReport()
    {
        return $this->lastReport;
    }

    /**
     * Set contractorService
     *
     * @param \AppBundle\Entity\ContractorService $contractorService
     *
     * @return Service
     */
    public function setContractorService(\AppBundle\Entity\ContractorService $contractorService = null)
    {
        $this->contractorService = $contractorService;

        return $this;
    }

    /**
     * Get contractorService
     *
     * @return \AppBundle\Entity\ContractorService
     */
    public function getContractorService()
    {
        return $this->contractorService;
    }
    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $lastTester;


    /**
     * Set lastTester
     *
     * @param \AppBundle\Entity\ContractorUser $lastTester
     *
     * @return Service
     */
    public function setLastTester(\AppBundle\Entity\ContractorUser $lastTester = null)
    {
        $this->lastTester = $lastTester;

        return $this;
    }

    /**
     * Get lastTester
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getLastTester()
    {
        return $this->lastTester;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Service
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Service
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set directlyWO
     *
     * @param boolean $directlyWO
     *
     * @return Service
     */
    public function setDirectlyWO($directlyWO)
    {
        $this->directlyWO = $directlyWO;

        return $this;
    }

    /**
     * Get directlyWO
     *
     * @return boolean
     */
    public function getDirectlyWO()
    {
        return $this->directlyWO;
    }

    /**
     * Set addedFromIos
     *
     * @param boolean $addedFromIos
     *
     * @return Service
     */
    public function setAddedFromIos($addedFromIos)
    {
        $this->addedFromIos = $addedFromIos;

        return $this;
    }

    /**
     * Get addedFromIos
     *
     * @return boolean
     */
    public function getAddedFromIos()
    {
        return $this->addedFromIos;
    }
}
