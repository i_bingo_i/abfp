<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Proposal;
use Doctrine\ORM\EntityRepository;

/**
 * ProposalLogRepository
 */
class ProposalLogRepository extends EntityRepository
{
    public function getProposalLogs(Proposal $proposal)
    {
        $query = $this->createQueryBuilder("pl");
        $query
            ->where("pl.proposal = :proposal")
            ->setParameter("proposal", $proposal)
            ->orderBy("pl.date", "DESC")
        ;

        return $query->getQuery()->getResult();
    }
}
