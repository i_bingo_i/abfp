<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ContractorUser;
use Doctrine\ORM\EntityRepository;

/**
 * ContractorUserHistoryRepository
 */
class ContractorUserHistoryRepository extends EntityRepository
{
    public function getLastRecord(ContractorUser $contractorUser)
    {
        $query = $this->createQueryBuilder("cuh");
        $query
            ->select()
            ->where('cuh.ownerEntity = :contractorUser')
            ->orderBy('cuh.dateSave', 'DESC')
            ->setMaxResults(1)
            ->setParameter('contractorUser', $contractorUser);

        $result = $query->getQuery()->getResult();

        return $result ? $result[0] : null;
    }
}
