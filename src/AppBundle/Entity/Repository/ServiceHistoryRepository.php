<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Device;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Service;
use Doctrine\ORM\EntityRepository;

/**
 * ServiceHistoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ServiceHistoryRepository extends EntityRepository
{
    // TODO: Need Refactoring
    /**
     * @param $services
     * @return array
     */
    public function getLastByService($services)
    {
        $query = $this->getEntityManager()->createQuery('
              SELECT sh 
              FROM AppBundle\Entity\ServiceHistory sh
              WHERE sh.ownerEntity IN (:services) 
                AND (SELECT MAX(sh_1.id)
                      FROM AppBundle\Entity\ServiceHistory sh_1
                      WHERE sh_1.ownerEntity = sh.ownerEntity) = sh.id
              GROUP BY sh.ownerEntity
              ORDER BY sh.id DESC');
        $query->setParameter('services', array_values($services));
        return $query->getResult();
    }

    /**
     * @param Service $service
     * @return mixed
     */
    public function getLastRecord(Service $service)
    {
        $query = $this->createQueryBuilder("sh");
        $query
            ->where('sh.ownerEntity = :service')
            ->orderBy('sh.id', 'DESC')
            ->setMaxResults(1)
            ->setParameter('service', $service);

        $serviceHistory = $query->getQuery()->getOneOrNullResult();

        return $serviceHistory;
    }

    /**
     * @param array $serviceHistoryIds
     * @return \DateTime
     */
    public function getEarliestDueDate(array $serviceHistoryIds)
    {
        $query = $this->createQueryBuilder("sh");
        $query
            ->select('MIN(sh.inspectionDue) as date')
            ->where('sh.id IN (:serviceHistoryIds)')
            ->setParameters([
                "serviceHistoryIds" => array_values($serviceHistoryIds)
            ]);

        return new \DateTime($query->getQuery()->getSingleResult()['date']);
    }

    /**
     * @param Device $device
     * @return array
     */
    public function getServiceHistoryWithWorkorderByDevice(Device $device)
    {
        $query = $this->createQueryBuilder('sh')
            ->where('sh.device = :device')
            ->setParameter('device', $device)
            ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param Device $device
     * @return array
     */
    public function getDeviceServicesHistories(Device $device)
    {

        $query = $this->createQueryBuilder('sh');
        $query
            ->leftJoin("sh.status", "shs")
            ->leftJoin("sh.lastReport", "shlr")
            ->where("sh.device = :device")
            ->andWhere("shs.alias = 'under_workorder'")
            ->andWhere("sh.deleted = false")
            ->andWhere("sh.workorder <>''")
            ->andWhere("sh.lastReport <>''")
            ->groupBy('shlr')
            ->orderBy('sh.id', 'DESC')
            ->setParameter('device', $device)
        ;

        return $resultId = $query->getQuery()->getResult();
    }

    /**
     * @param Device $device
     * @param Proposal $proposal
     * @return array
     */
    public function getRepairsByDeviceAndProposal(Device $device, Proposal $proposal)
    {
        return $this->createQueryBuilder('sh')
            ->leftJoin('sh.named', 'sn')
            ->leftJoin('sh.device', 'd')
            ->where('sn.frequency IS NULL')
            ->andWhere('sh.device = :device')
            ->andWhere('sh.proposal = :proposal')
            ->andWhere('sh.deleted = 0')
            ->setParameter('device', $device)
            ->setParameter('proposal', $proposal)
            ->getQuery()->getResult();
    }
}

