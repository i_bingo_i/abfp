<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ContractorCalendar;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * ContractorCalendarRepository
 */
class ContractorCalendarRepository extends EntityRepository
{
    /**
     * @return ContractorCalendar|bool
     */
    public function findForMyCompany()
    {
        /** @var QueryBuilder $query */
        $query = $this->createQueryBuilder("cc")
            ->leftJoin('cc.contractor', 'c')
            ->leftJoin('c.type', 't')
            ->where("t.alias = 'my_company'");

        $result = $query->getQuery()->getResult();

        $myCompanyCalendar = false;
        if (isset($result[0])) {
            /** @var ContractorCalendar $myCompanyCalendar */
            $myCompanyCalendar = $result[0];
        }

        return $myCompanyCalendar;
    }
}
