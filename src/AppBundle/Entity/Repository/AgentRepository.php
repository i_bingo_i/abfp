<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;

/**
 * AgentRepository
 */
class AgentRepository extends EntityRepository
{
    use PaginateQueryTrait;

    /**
     * @param int $page
     * @param int $maxItems
     * @return array
     */
    public function findAllWithPagination(? int $page, int $maxItems = 15)
    {
        $page = is_null($page) ? 1 : $page;
        $query = $this->createQueryBuilder('ag')
            ->orderBy('ag.id', 'ASC');
        return $this->paginate($query->getQuery(), $page, $maxItems);
    }
}
