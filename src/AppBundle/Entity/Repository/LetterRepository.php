<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Workorder;
use Doctrine\ORM\EntityRepository;

/**
 * LetterRepository
 */
class LetterRepository extends EntityRepository
{
    /**
     * @param Workorder $workorder
     * @param $pieceOfPath
     * @return bool
     */
    public function checkIfNeedleTypeWasSent(Workorder $workorder, $pieceOfPath)
    {
        $query = $this->createQueryBuilder("l");
        $query
            ->leftJoin("l.attachments", "at")
            ->leftJoin("at.file", "f")
            ->where('f.file LIKE :pieceOfPath')
            ->andWhere('l.workorder = :workorder')
            ->orderBy('l.dateCreate', 'DESC')
            ->setMaxResults(1)
            ->setParameters([
                "workorder" => $workorder,
                "pieceOfPath" => "%".$pieceOfPath."%"
            ])
        ;

        return $query->getQuery()->getResult() ? $query->getQuery()->getResult()[0] : false;
    }
}
