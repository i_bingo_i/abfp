<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Account;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\WorkorderStatus;
use AppBundle\Entity\WorkorderType;
use DateTime;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;
use Doctrine\ORM\QueryBuilder;

/**
 * WorkorderRepository
 */
class WorkorderRepository extends EntityRepository
{
    use PaginateQueryTrait;

    private const DEFAULT_LINES_QUANTITY = 100;
    private const ALL_RECORDS = 'all';

    /**
     * @param array $filters
     * @return array
     */
    public function findAllWithPagination(array $filters) : array
    {
        $query = $this->createQueryBuilder('wo')
            ->select('wo')
            ->leftJoin('wo.division', 'd')
            ->leftJoin('wo.status', 's')
            ->leftJoin('wo.account', 'a');

        $this->someOrders($query);
        $this->someFilters($query, $filters);

        $maxItems = !empty($filters['pagerCount']) ? $filters['pagerCount'] : self::DEFAULT_LINES_QUANTITY;
        $page = $filters['page'] ?? 1;
        $quantityPages = 1;

        if (isset($filters['pagerCount']) and ($filters['pagerCount'] !== self::ALL_RECORDS)) {
            return $this->paginate($query->getQuery(), $page, $filters['pagerCount']);
        }

        $query2 = $this->createQueryBuilder("o")
            ->select("count(o.id)")
            ->getQuery()->getResult()[0][1];

        if (is_numeric($maxItems)) {
            $query->setMaxResults($maxItems);
            $quantityPages = ceil($query2 / $maxItems);
        }

        $results = $query->getQuery()->getResult();

        return [
            'currentPage' => $page,
            'pagesCount' => $quantityPages,
            'count' => count($results),
            'results' => $results
        ];
    }

    /**
     * @param QueryBuilder $query
     * @param array $filters
     */
    public function someFilters(QueryBuilder $query, array $filters) : void
    {
        if (!empty($filters)) {
            if (!empty($filters['division'])) {
                $divisionsArray = explode(',', $filters['division']);

                $query->andWhere('wo.division IN (:division)')
                    ->setParameter('division', $divisionsArray);
            }
            if (!empty($filters['status'])) {
                $query->andWhere('wo.status = :status')
                    ->setParameter('status', $filters['status']);
            }
            if (!empty($filters['account'])) {
                $query->andWhere('wo.account = :account');
                $query->setParameter('account', $filters['account']->getId());
            }
            if (!empty($filters['date'])) {
                if (!empty($filters['date']['from']) && !empty($filters['date']['to'])) {
                    $query->andWhere(
                        $query->expr()->orX(
                            $query->expr()->orX(
                                $query->expr()->orX(
                                    $query->expr()->between('DATE(wo.scheduledFrom)', ':from', ':to'),
                                    $query->expr()->between('DATE(wo.scheduledTo)', ':from', ':to')
                                ),
                                $query->expr()->andX(
                                    'DATE(wo.scheduledFrom) < :from',
                                    'DATE(wo.scheduledTo) > :to'
                                )
                            ),
                            $query->expr()->andX(
                                $query->expr()->isNull('wo.scheduledFrom'),
                                $query->expr()->isNull('wo.scheduledTo')
                            )
                        )
                    );
                    $query->setParameter('from', (new \DateTime($filters['date']['from']))->format('Y-m-d'));
                    $query->setParameter('to', (new \DateTime($filters['date']['to']))->format('Y-m-d'));

                } elseif (!empty($filters['date']['from'])) {
                    $query->andWhere(
                        $query->expr()->orX(
                           'DATE(wo.scheduledFrom) >= :from',
                            $query->expr()->andX(
                                $query->expr()->isNull('wo.scheduledFrom'),
                                $query->expr()->isNull('wo.scheduledTo')
                            )
                        )
                    );
                    $query->setParameter('from', (new \DateTime($filters['date']['from']))->format('Y-m-d'));
                } elseif (!empty($filters['date']['to'])) {
                    $query->andWhere(
                        $query->expr()->orX(
                            'DATE(wo.scheduledTo) <= :to',
                            $query->expr()->andX(
                                $query->expr()->isNull('wo.scheduledFrom'),
                                $query->expr()->isNull('wo.scheduledTo')
                            )
                        )
                    );
                    $query->setParameter('to', (new \DateTime($filters['date']['to']))->format('Y-m-d'));
                }
            }
        }
    }

    //TODO Mysql function DATE() have been add
    /**
     * @param WorkorderStatus $status
     * @param DateTime $today
     * @param null $notInStatusesAliases
     * @return array
     */
    public function getWhichDoneToday(WorkorderStatus $status, DateTime $today, $notInStatusesAliases = null)
    {
        $query = $this->createQueryBuilder('wo');
        $query->leftJoin('wo.status', 's')
            ->where('DATE(wo.scheduledFrom) <= :today')
            ->andWhere('DATE(wo.scheduledTo) >= :today')
            ->andWhere('s.priority < :priority')
            ->setParameters(
                [
                    'today' => $today->format('Y-m-d'),
                    'priority' => $status->getPriority()
                ]
            );

        if (!empty($notInStatusesAliases)) {
            $query
                ->andWhere('s.alias NOT IN (:notInStatusesAliases)')
                ->setParameter('notInStatusesAliases', $notInStatusesAliases);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @param WorkorderStatus $status
     * @param DateTime $today
     * @param null $notInStatusesAliases
     * @return array
     */
    public function getScheduledDateWhichPassed(WorkorderStatus $status, DateTime $today, $notInStatusesAliases = null)
    {
        $query = $this->createQueryBuilder('wo');
        $query->leftJoin('wo.status', 's')
            ->where('DATE(wo.scheduledTo) < :today')
            ->andWhere('s.priority < :priority')
            ->setParameters(
                [
                    'today' => $today->format('Y-m-d'),
                    'priority' => $status->getPriority()
                ]
            );

        if (!empty($notInStatusesAliases)) {
            $query
                ->andWhere('s.alias NOT IN (:notInStatusesAliases)')
                ->setParameter('notInStatusesAliases', $notInStatusesAliases);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    private function someOrders(QueryBuilder $query) : QueryBuilder
    {
        $query->addOrderBy('wo.scheduledFrom');
        $query->addOrderBy('wo.division');

        return $query;
    }

    /**
     * @param Account $account
     * @return mixed
     */
    public function getCountWorkOrderInAccount(Account $account)
    {
        $query = $this->createQueryBuilder("wo")
            ->select("count(wo.id)")
            ->where('wo.account = :account');

        $query->setParameter('account', $account);
        $result = $query->getQuery()->getResult();

        return $result['0']['1'];
    }

    /**
     * @param ContractorUser $technician
     * @param $currentTimeStamp
     * @return array
     */
    public function getWorkordersByTechnician(ContractorUser $technician, $currentTimeStamp)
    {
        $date = new DateTime();
        $date->setTimestamp($currentTimeStamp);
        $today = $date->format('Y-m-d');

        $query = $this->createQueryBuilder("wo");
        $query->select('wo')
            ->leftJoin("wo.events", "ev")
            ->leftJoin("wo.account", "ac")
            ->leftJoin("wo.services", "se")
            ->where("ev.user = :technician")
            ->andWhere('DATE(ev.fromDate) = :today')
            ->addOrderBy("ev.fromDate", "ASC")
            ->addOrderBy("ev.toDate", "ASC")
            ->setParameters([
                "technician" => $technician,
                "today" => $today
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getScheduledFromCurrentDate()
    {
        $currentDate = new \DateTime();
        $woStatusesAliases = ['to_be_scheduled', 'scheduled', 'to_be_done_today'];

        $query = $this->createQueryBuilder('wo')
            ->leftJoin('wo.status', 'wos')
            ->where('DATE(wo.scheduledFrom) >= :currentDate')
            ->andWhere('wos.alias IN(:statusesAliases)')
            ->setParameter('currentDate', $currentDate->format('Y-m-d'))
            ->setParameter('statusesAliases', $woStatusesAliases)
        ;

        return $query->getQuery()->getResult();
    }


    /**
     * @param DeviceCategory $division
     * @return array
     */
    public function getWorkordersForMap(DeviceCategory $division, $woId = 0)
    {
        $currentDate = new \DateTime();
        $woStatusesAliases = ['scheduled', 'to_be_done_today'];

        $query = $this->createQueryBuilder('wo')
            ->leftJoin('wo.status', 'wos')
            ->where('DATE(wo.scheduledTo) >= :currentDate')
            ->andWhere('wos.alias IN(:statusesAliases)')
            ->andWhere('wo.division = :division')
            ->orWhere('wo.id = :woId')
            ->setParameters([
                "currentDate" => $currentDate->format('Y-m-d'),
                "statusesAliases" => $woStatusesAliases,
                "division" => $division,
                "woId" => $woId
            ]);
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @param string $woTypeAlias
     * @return array
     */
    public function findSuitableWOsForCreating(
        Account $account,
        DeviceCategory $division,
        $woTypeAlias = 'simple_workorder'
    ) {
        $query = $this->createQueryBuilder('wo')
            ->leftJoin('wo.status', 'wos')
            ->leftJoin('wo.type', 'wot')
            ->where('wo.account = :account')
            ->andWhere('wo.division = :division')
            ->andWhere('wot.alias = :woTypeAlias')
            ->andWhere('wo.isFrozen = 0')
            ->orderBy("wos.priority", "ASC")
            ->setParameters([
                "account" => $account,
                "division" => $division,
                "woTypeAlias" => $woTypeAlias
            ]);

        $workOrders = $query->getQuery()->getResult();

        return $workOrders;
    }

    /**
     * @param Account $account
     * @param string $woTypeAlias
     * @return array
     */
    public function findSuitableWOsForCreatingByAccount(Account $account, $woTypeAlias = 'simple_workorder')
    {
        $query = $this->createQueryBuilder('wo')
            ->leftJoin('wo.status', 'wos')
            ->leftJoin('wo.type', 'wot')
            ->where('wo.account = :account')
            ->andWhere('wot.alias = :woTypeAlias')
            ->andWhere('wo.isFrozen = 0')
            ->orderBy("wos.priority", "ASC")
            ->setParameters([
                "account" => $account,
                "woTypeAlias" => $woTypeAlias
            ]);

        $workOrders = $query->getQuery()->getResult();

        return $workOrders;
    }
}
