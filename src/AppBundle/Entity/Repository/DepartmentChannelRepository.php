<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Department;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Municipality;
use Doctrine\ORM\EntityRepository;

/**
 * DepartmentChannelRepository
 */
class DepartmentChannelRepository extends EntityRepository
{
    /**
     * @param Municipality $municipality
     * @param $divisions
     * @return mixed
     */
    public function getCountDefaultByDivisionsForMunicipality(Municipality $municipality, $divisions)
    {
        $qb = $this->createQueryBuilder("dc");
        $qb
            ->select('COUNT(dc.id)')
            ->leftJoin('dc.department', 'd')
            ->where('dc.isDefault = true')
            ->andWhere('dc.active = true')
            ->andWhere('d.municipality = :municipality')
            ->setParameters([
                'municipality' => $municipality
            ])
        ;

        if (!empty($divisions)) {
            $qb->andWhere($qb->expr()->in('d.division', $divisions));
        }

        $result = $qb->getQuery()->getSingleResult();

        return $result[1];
    }


    /**
     * @param Department $department
     * @return DepartmentChannel[]
     */
    public function getDepartmentChannels(Department $department)
    {
        $qb = $this->createQueryBuilder("dc");
        $qb
            ->where('dc.department = :department')
            ->andWhere('dc.deleted = false')
            ->setParameters([
                'department' => $department
            ])
        ;

        /** @var DepartmentChannel[] $channels */
        $channels = $qb->getQuery()->getResult();

        return $channels;
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $deviceCategory
     * @return mixed
     */
    public function getDefaultChannelWithoutAgent(Municipality $municipality, DeviceCategory $deviceCategory)
    {
        $qb = $this->createQueryBuilder("dc");
        $qb
            ->leftJoin('dc.department', 'd')
            ->leftJoin('dc.owner', 'ow')
            ->where('d.division = :division')
            ->andWhere('d.municipality =:municipality')
            ->andWhere('dc.active = true')
            ->andWhere('dc.isDefault = true')
            ->andWhere('dc.deleted = false')
            ->andWhere('ow.id IS NULL')
            ->setParameters(
                [
                    'division' => $deviceCategory,
                    'municipality' => $municipality
                ]
            );

        $channel = $qb->getQuery()->getOneOrNullResult();

        return $channel;
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $deviceCategory
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getDefaultChannelWithAgent(Municipality $municipality, DeviceCategory $deviceCategory)
    {
        $qb = $this->createQueryBuilder("dc");
        $qb
            ->leftJoin('dc.department', 'd')
            ->leftJoin('dc.owner', 'ow')
            ->where('d.division = :division')
            ->andWhere('d.municipality =:municipality')
            ->andWhere('dc.isDefault = true')
            ->andWhere('dc.active = true')
            ->andWhere('dc.deleted = false')
            ->andWhere('ow.id IS NOT NULL')
            ->setParameters(
                [
                    'division' => $deviceCategory,
                    'municipality' => $municipality
                ]
            );

        $channel = $qb->getQuery()->getOneOrNullResult();

        return $channel;
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $division
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getEmailDepartmentChannelByMunAndDiv(
        Municipality $municipality,
        DeviceCategory $division
    ) {
        $qb = $this->createQueryBuilder("dc");
        $qb
            ->leftJoin('dc.department', 'd')
            ->innerJoin("dc.fields", "dv")
            ->leftJoin("dc.named", "dcn")
            ->where('d.division = :division')
            ->andWhere('d.municipality =:municipality')
            ->andWhere("dcn.alias = 'email'")
            ->setParameters(
                [
                    'division' => $division,
                    'municipality' => $municipality
                ]
            );

        $channel = $qb->getQuery()->getOneOrNullResult();

        return $channel;
    }
}
