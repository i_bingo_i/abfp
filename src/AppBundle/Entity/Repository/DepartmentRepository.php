<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Municipality;
use Doctrine\ORM\EntityRepository;

/**
 * DepartmentRepository
 */
class DepartmentRepository extends EntityRepository
{

    /**
     * @param Municipality $municipality
     * @param $alias
     * @return mixed
     */
    public function getMunicipalityDepartmentByDevisionAlias(Municipality $municipality, $alias)
    {
        $query = $this->createQueryBuilder("d");
        $query
            ->leftJoin('d.division', 'dd')
            ->where('d.municipality = :municipality')
            ->andWhere('dd.alias = :alias')
            ->setParameters(["municipality" => $municipality, "alias" => $alias])
        ;

        $result = $query->getQuery()->getOneOrNullResult();

        return $result;
    }
}
