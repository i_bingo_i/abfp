<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Event;
use AppBundle\Entity\Workorder;
use Doctrine\ORM\EntityRepository;

/**
 * EventRepository
 */
class EventRepository extends EntityRepository
{

    /**
     * @param Workorder $workorder
     * @return array
     */
    public function getEventsByWorkorder(Workorder $workorder)
    {
        $query = $this->createQueryBuilder("e")
            ->select()
            ->leftJoin("e.user", "cu")
            ->leftJoin("cu.user", "u")
            ->where("e.workorder = :workorder")
            ->addOrderBy("e.fromDate", "ASC")
            ->addOrderBy("e.toDate", "ASC")
            ->addOrderBy("u.firstName", "ASC")
            ->setParameter("workorder", $workorder)
        ;
        $results = $query->getQuery()->getResult();

        return $results;
    }

    /**
     * @param Workorder $workorder
     * @param Event $event
     * @return mixed
     */
    public function getTheEarliestFromEvent(Workorder $workorder, Event $event)
    {
        $query = $this->createQueryBuilder("e");
        $minDate = $this->createQueryBuilder("event")
            ->select('MIN(event.fromDate)')
            ->where('event.workorder = :workorder')
            ->andWhere('event.id <> :event')
            ->setParameters(['workorder' => $workorder, 'event' => $event->getId()])
            ->getQuery()->getResult()[0][1];
        ;

        $query
            ->where('e.workorder = :workorder')
            ->andWhere('e.fromDate = :minDate')
            ->setParameters(['minDate' => $minDate, 'workorder' => $workorder]);

        return $query->getQuery()->getResult()[0];
    }

    /**
     * @param Workorder $workorder
     * @param Event $event
     * @return mixed
     */
    public function getTheOldestToDateEvent(Workorder $workorder, Event $event)
    {
        $query = $this->createQueryBuilder("e");
        $maxDate = $this->getMaxToDate($workorder, $event);
        $maxDateCreate = $this->getMaxDateCreateByToDate($workorder, $maxDate);
        $res = null;

        if (!empty($maxDate) || !empty($maxDateCreate)) {
            $query
                ->where('e.workorder = :workorder')
                ->andWhere('e.toDate = :maxDate')
                ->andWhere('e.dateCreate = :maxDateCreate')
                ->setParameters(
                    ['maxDate' => $maxDate, 'workorder' => $workorder, 'maxDateCreate' => $maxDateCreate]
                );

            $res = $query->getQuery()->getResult() ? $query->getQuery()->getResult()[0] : null;
        }

        return $res;
    }

    /**
     * @param Workorder $workorder
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCanFinishWo(Workorder $workorder)
    {
        $query = $this->createQueryBuilder("e");
        $query
            ->where('e.workorder = :workorder')
            ->andWhere('e.canFinishWO = 1')
            ->setParameters(
                ['workorder' => $workorder]
            );

        $result = $query->getQuery()->getOneOrNullResult();

        return $result;
    }

    /**
     * @param Workorder $workorder
     * @param Event|null $event
     * @return mixed
     */
    public function getMaxToDate(Workorder $workorder, Event $event = null)
    {
        $query = $this->createQueryBuilder("event");
        $query
            ->select('MAX(event.toDate)')
            ->where('event.workorder = :workorder')
            ->setParameter('workorder', $workorder);

        if ($event and !empty($event->getId())) {
            $query->andWhere('event.id <> :event')
                ->setParameter('event', $event);
        }

        return $query->getQuery()->getResult() ? $query->getQuery()->getResult()[0][1] : null;
    }

    /**
     * @param Workorder $workorder
     * @param $maxToDate
     * @return mixed
     */
    public function getMaxDateCreateByToDate(Workorder $workorder, $maxToDate)
    {
        $query = $this->createQueryBuilder("event");
        $query
            ->select('MAX(event.dateCreate)')
            ->where('event.toDate = :maxToDate')
            ->andWhere('event.workorder = :workorder')
            ->setParameters(['maxToDate' => $maxToDate, 'workorder' => $workorder]);

        return $query->getQuery()->getResult() ? $query->getQuery()->getResult()[0][1] : null;
    }

    /**
     * @param Workorder $workorder
     * @return array
     */
    public function getEventByToday(Workorder $workorder)
    {
        $query = $this->createQueryBuilder("e");
        $today = (new \DateTime('now'))->format('Y-m-d');

        $query
            ->where('e.workorder = :workorder')
            ->andWhere('DATE(e.fromDate) = :today')
            ->setParameters(['today' => $today, 'workorder' => $workorder]);

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $date
     * @return array
     */
    public function getEventsOnDate(\DateTime $date)
    {
        $query = $this->createQueryBuilder('e');
        $query
            ->where('e.fromDate LIKE :fromDate')
            ->setParameter('fromDate', '%'.$date->format('Y-m-d').'%');

        return $query->getQuery()->getResult();
    }
}
