<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\OpportunityStatus;
use AppBundle\Entity\OpportunityType;
use \Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use AppBundle\Entity\Account;
use AppBundle\Entity\Opportunity;
use Doctrine\ORM\Query\Expr\Select;
use Doctrine\ORM\Query\Expr\From;

use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * OpportunityRepository
 */
class OpportunityRepository extends EntityRepository
{
    use PaginateQueryTrait;

    private const DEFAULT_LINES_QUANTITY = 50;
    private const ALL_RECORDS = 'All';

    /**
     * @param array $filters
     * @param int|null $page
     * @return array
     */
    public function findByFilterWithPagination(array $filters = [], $page = null)
    {
        $query = $this->createQueryBuilder("o");
        $query
            ->select('o')
            ->leftJoin('o.parentAccount', 'a')
            ->leftJoin('o.division', 'dv');

        self::someFilters($query, $filters);
        $this->someOrders($query, $filters);

        $maxItems = !empty($filters['pagerCount']) ? $filters['pagerCount'] : self::DEFAULT_LINES_QUANTITY;
        $page = $page ?? 1;
        $quantityPages = 1;

        if (isset($filters['pagerCount']) and $filters['pagerCount'] !== self::ALL_RECORDS) {
            return $this->paginate($query->getQuery(), $page, $maxItems);
        }

        /** TODO Fix this code because it is strange */
        $query2 = $this->createQueryBuilder("o")
            ->select("count(o.id)");
        self::someFilters($query2, $filters);
        $result2 = $query2->getQuery()->getResult()[0][1];

        if (is_numeric($maxItems)) {
            $query->setMaxResults($maxItems);
            $quantityPages = ceil($result2 / $maxItems);
        }

        $results = $query->getQuery()->getResult();

        return [
            'currentPage' => $page,
            'pagesCount' => $quantityPages,
            'count' => count($results),
            'results' => $results
        ];
    }

    /**
     * @param Account $account
     * @param array $filters
     * @return array
     */
    public function findByFilterWithPaginationForAccount(Account $account, array $filters = [])
    {
        $query = $this->createQueryBuilder("o");
        $query
            ->select('o')
            ->leftJoin('o.parentAccount', 'a')
            ->leftJoin('o.division', 'dv');

        if ($account->getType()->getAlias() == 'account') {
            $query->andWhere("o.account = :account");
        } else {
            $query->andWhere("o.parentAccount = :account");
        }
        $query->setParameter('account', $account);

        self::someFilters($query, $filters);
        $this->someOrders($query, $filters);

        $results = $query->getQuery()->getResult();

        return [
            'currentPage' => 1,
            'pagesCount' => 1,
            'count' => count($results),
            'results' => $results
        ];
    }

    /**
     * @param $accounts
     * @param $filters
     * @return array
     */
    public function getFilteredAccountsOpportunities($accounts, $filters)
    {
        $query = $this->createQueryBuilder("o")
            ->select('o')
            ->where("o.account IN (:accounts)")
            ->leftJoin('o.division', 'dv')
            ->orderBy('dv.id', 'ASC')
            ->addOrderBy('o.executionServicesDate', 'ASC')
            ->setParameter("accounts", array_values($accounts));

        self::someFilters($query, $filters);

        return $query->getQuery()->getResult();
    }

    /**
     * @param Account $account
     * @param int|null $division
     * @return mixed
     */
    public function getOldestExecServicesDate(Account $account, $division = null)
    {
        $query = $this->createQueryBuilder("o")
            ->select('MIN(o.executionServicesDate)')
            ->where('o.account = :account')
            ->orWhere('o.parentAccount = :account')
            ->setParameter('account', $account);

        if (!empty($division)) {
            $query->andWhere('o.division = :division')
                ->setParameter('division', $division);
        }

        return $query->getQuery()->getOneOrNullResult()[1];
    }

    /**
     * Attention:
     * - not view Alarm opportunity (by Taras)
     *
     * @param QueryBuilder $query
     * @param $filters
     * @return QueryBuilder
     */
    public static function someFilters(QueryBuilder $query, $filters)
    {
        if (!empty($filters['division']) && $filters['division'] != 'all') {
            $divisionsArray = explode(',', $filters['division']);

            $query->andWhere('o.division IN (:division)')
                ->setParameter('division', $divisionsArray);
        }

        if (!empty($filters['type']) && is_string($filters['type'])) {
            $query
                ->leftJoin('o.type', 't')
                ->andWhere('t.alisa=:type')
                ->setParameter('type', $filters['type']);
        } elseif (!empty($filters['type'])) {
            $query->andWhere('o.type=:type')
                ->setParameter('type', $filters['type']);
        }

        if (!empty($filters['status'])) {
            $query->andWhere('o.status=:status')
                ->setParameter('status', $filters['status']);
        }

        if (!empty($filters['clientType'])) {
            $query->andWhere('a.clientType=:clientType')
                ->setParameter('clientType', $filters['clientType']);
        }

        if (!empty($filters['date'])) {
            if (!empty($filters['date']['from'])) {
                $query->andWhere('o.executionServicesDate>=:from')
                    ->setParameter('from', new \DateTime($filters['date']['from']));
            }
            if (!empty($filters['date']['to'])) {
                $query->andWhere('o.executionServicesDate<=:to')
                    ->setParameter('to', new \DateTime($filters['date']['to']));
            }
        }

        return $query;
    }

    /**
     * @param OpportunityStatus $status
     * @return array
     */
    public function getOverDue(OpportunityStatus $status)
    {
        $query = $this->createQueryBuilder("o")
            ->select()
            ->where('o.executionServicesDate < CURRENT_DATE()')
            ->andWhere("o.status <> :status")
            ->setParameter('status', $status->getId());

        return $query->getQuery()->getResult();
    }
    
    /**
     * @param QueryBuilder $query
     * @param $filters
     * @return QueryBuilder
     */
    private function someOrders(QueryBuilder $query, $filters)
    {
        if (!empty($filters['division'])) {
            switch ($filters['division']) {
                case '1': $query->orderBy('a.oldestOpportunityBackflowDate', 'ASC'); break;
                case '2': $query->orderBy('a.oldestOpportunityFireDate', 'ASC'); break;
                case '3': $query->orderBy('a.oldestOpportunityPlumbingDate', 'ASC'); break;
                case '4': $query->orderBy('a.oldestOpportunityAlarmDate', 'ASC'); break;
                case 'all': $query->orderBy('a.oldestOpportunityDate', 'ASC'); break;
            }
        } else {
            $query->orderBy('a.oldestOpportunityDate', 'ASC');
        }

        $query->addOrderBy('dv.id', 'ASC');
        $query->addOrderBy('o.executionServicesDate', 'ASC');
        $query->addOrderBy('o.id');

        return $query;
    }
}
