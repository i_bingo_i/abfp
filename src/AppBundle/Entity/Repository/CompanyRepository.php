<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Company;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;
use \Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * CompanyRepository
 */
class CompanyRepository extends EntityRepository
{
    use PaginateQueryTrait;

    public function getCount()
    {
        $query = $this->createQueryBuilder("c");
        $query
            ->select('COUNT(c.id)')
            ->where('c.deleted=0');
        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    /**
     * @param string $sourceEntity
     * @return array
     */
    public function getImportedCompanies($sourceEntity = '')
    {
        $query = $this->createQueryBuilder("c");
        $query
            ->select('c.oldCompanyId')
            ->where("c.oldCompanyId <> '' ")
        ;

        if (!empty($sourceEntity)) {
            $query->andWhere('c.sourceEntity = :sourceEntity')
                ->setParameter('sourceEntity', $sourceEntity);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @param Company $company
     * @param int $page
     * @param int $maxItems
     * @return array
     */
    public function findByParent(Company $company, $page = 1, $maxItems = 15)
    {
        $query = $this->createQueryBuilder("c");
        $query
            ->where('c.parent = :parent')
            ->setParameter('parent', $company);

        return $this->paginate($query->getQuery(), $page, $maxItems);
    }

    /**
     * @return Company[]
     */
    public function findAllWithoutCustomer()
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(Company::class, 'c');

        $query = $this->_em->createNativeQuery('SELECT * FROM company AS c WHERE (SELECT COUNT(*) FROM customer AS cm LEFT JOIN entity_type et ON cm.entity_type_id = et.id WHERE et.alias="company" and cm.entity_id=c.id) < 1', $rsm);
        /** @var Company[] $companies */
        $companies = $query->getResult();

        return $companies;
    }
}
