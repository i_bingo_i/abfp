<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Account;
use Doctrine\ORM\EntityRepository;

/**
 * DeviceCategoryRepository
 */
class DeviceCategoryRepository extends EntityRepository
{
    /**
     * @param Account $account
     * @return array
     */
    public function getDivision(Account $account)
    {
        $query = $this->createQueryBuilder("dc");
        $query
            ->select('dc')
            ->leftJoin('dc.deviceName', 'dn')
            ->innerJoin('dn.devices', 'd')
            ->innerJoin('d.account', 'a')
            ->where('a.id = :account')
            ->groupBy('dc.id')
            ->setParameter('account', $account);
        return $query->getQuery()->getResult();
    }

    /**
     * @param $statusesAlias
     * @return array
     */
    public function getByStatusesAlias($statusesAlias)
    {
        $query = $this->createQueryBuilder("dc");
        $query
            ->where("dc.alias IN (:statusesAliases)")
            ->setParameters([
                'statusesAliases' => $statusesAlias
            ]);

        $divisions = $query->getQuery()->getResult();
        return $divisions;
    }
}
