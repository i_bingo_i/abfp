<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Account;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalStatus;
use AppBundle\Entity\Workorder;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;
use Doctrine\ORM\QueryBuilder;

/**
 * ProposalRepository
 */
class ProposalRepository extends EntityRepository
{
    use PaginateQueryTrait;

    private const DEFAULT_LINES_QUANTITY = 50;
    private const ALL_RECORDS = 'All';

    /**
     * @param array $filters
     * @param null $typesAlias
     * @param null $page
     * @param null $contactPerson
     * @return array
     */
    public function findAllWithPagination(
        array $filters,
        $typesAlias = null,
        $page = null,
        $contactPerson = null
    ) : array {
        $query = $this->createQueryBuilder('pr')
            ->select('pr')
            ->leftJoin('pr.division', 'd')
            ->leftJoin('pr.account', 'a')
            ->leftJoin('pr.type', 't')
            ->leftJoin('pr.creator', 'c')
            ->leftJoin('pr.status', 's');

        if ($typesAlias) {
            $query
                ->andWhere('t.alias IN (:types)')
                ->setParameters([
                    'types' => $typesAlias
                ]);
        }

        $this->someOrders($query, $filters);
        $this->someFilters($query, $filters);

        // TODO: потребує рефакторингу
        $maxItems = !empty($filters['pagerCount']) ? $filters['pagerCount'] : self::DEFAULT_LINES_QUANTITY;
        $page = $page ?? 1;
        $quantityPages = 1;

        if (isset($filters['pagerCount']) and $filters['pagerCount'] !== self::ALL_RECORDS) {
            return $this->paginate($query->getQuery(), $page, $maxItems);
        }

        $countsWithPagination = $this->helperQueryForPaginationCount($typesAlias, $contactPerson);

        if (is_numeric($maxItems)) {
            $query->setMaxResults($maxItems);
            $quantityPages = ceil($countsWithPagination / $maxItems);
        }

        $results = $query->getQuery()->getResult();

        return [
            'currentPage' => $page,
            'pagesCount' => $quantityPages,
            'count' => count($results),
            'results' => $results
        ];
    }

    /**
     * @param null $typesAlias
     * @param null $contactPerson
     * @return mixed
     */
    private function helperQueryForPaginationCount($typesAlias = null, $contactPerson = null)
    {
        $query2 = $this->createQueryBuilder("pr")
            ->select("count(pr.id)")
            ->leftJoin('pr.type', 't');

        if ($typesAlias) {
            $query2
                ->andWhere('t.alias IN (:types)')
                ->setParameters([
                    'types' => $typesAlias
                ]);
        }

        if ($contactPerson and $contactPerson instanceof ContactPerson) {
            $query2
                ->leftJoin('pr.frozen', 'fz')
                ->leftJoin("pr.recipient", "acp")
                ->leftJoin("fz.recipient", "fzacp")
                ->leftJoin("fzacp.contactPerson", "fzcpHistory")
                ->andWhere("acp.contactPerson = :contactPerson OR fzcpHistory.ownerEntity = :contactPerson")
                ->setParameter('contactPerson', $contactPerson);
        }

        $count = $query2->getQuery()->getResult()[0][1];

        return $count;
    }

    /**
     * @param array $filters
     * @param null $typesAlias
     * @return array
     */
    public function findAllWithFilters(array $filters, $typesAlias = null) : array
    {
        $query = $this->createQueryBuilder('pr')
            ->select('pr')
            ->leftJoin('pr.division', 'd')
            ->leftJoin('pr.account', 'a')
            ->leftJoin('pr.type', 't')
            ->leftJoin('pr.creator', 'c')
            ->leftJoin('pr.status', 's');

        if ($typesAlias) {
            $query
                ->andWhere('t.alias IN (:types)')
                ->setParameters([
                    'types' => $typesAlias
                ]);
        }

        $this->someOrders($query, $filters);
        $this->someFilters($query, $filters);

        $results = $query->getQuery()->getResult();

        return [
            'count' => count($results),
            'results' => $results
        ];
    }

    /**
     * @param $filters
     * @param null $typesAlias
     * @return mixed
     */
    public function getQuantitiesWithFilters($filters, $typesAlias = null)
    {
        $query = $this->createQueryBuilder("pr");
        $query
            ->select(
                'COUNT(DISTINCT a.id) as account',
                'COUNT(DISTINCT pr.id) as notices',
                'COUNT(s.id) as services',
                'COUNT(DISTINCT d.id) as device'
            )
            ->leftJoin('pr.account', 'a')
            ->leftJoin('pr.services', 's', 's.id = pr.services')
            ->leftJoin('s.device', 'd', 's.device = d.id')
            ->where('d.deleted = 0')
            ->andWhere('s.deleted = 0');

        if ($typesAlias) {
            $query
                ->leftJoin('pr.type', 't')
                ->andWhere('t.alias IN (:types)')
                ->setParameters([
                    'types' => $typesAlias
                ]);
        }

        $this->someFilters($query, $filters);

        $result = $query->getQuery()->getSingleResult();

        return $result;
    }

    public function someFilters(QueryBuilder $query, array $filters)
    {
        if (!empty($filters)) {
            if (isset($filters['refId']) and !empty($filters['refId'])) {
                $query->andWhere(
                    $query->expr()->like('pr.referenceId', ':refId')
                )
                    ->setParameter('refId', '%'.trim($filters['refId']).'%');
            }
            if (!empty($filters['division'])) {
                $divisionsArray = explode(',', $filters['division']);

                $query->andWhere('pr.division IN (:division)')
                    ->setParameter('division', $divisionsArray);
            }
            if (!empty($filters['status'])) {
                $query->andWhere('pr.status = :status')
                    ->setParameter('status', $filters['status']);
            }
            if (!empty($filters['clientType'])) {
                $query->andWhere('a.clientType = :clientType')
                    ->setParameter('clientType', $filters['clientType']);
            }
            // TODO: use AdminBundle/Controller/ProposalController.php:339
            if (!empty($filters['contactPerson'])) {
                $query->leftJoin('pr.frozen', 'fz');
                $query->leftJoin("pr.recipient", "acp");
                $query->leftJoin("fz.recipient", "fzacp");
                $query->leftJoin("fzacp.contactPerson", "fzcpHistory");
                $query->andWhere("acp.contactPerson = :contactPerson OR fzcpHistory.ownerEntity = :contactPerson")
                    ->setParameter('contactPerson', $filters['contactPerson']);
            }
            if (!empty($filters['account'])) {
                $query->andWhere('pr.account = :account')
                    ->setParameter('account', $filters['account']);
            }
            if (!empty($filters['date'])) {
                if (!empty($filters['date']['from'])) {
                    $query->andWhere('pr.earliestDueDate >= :from')
                        ->setParameter('from', new \DateTime($filters['date']['from']));
                }
                if (!empty($filters['date']['to'])) {
                    $query->andWhere('pr.earliestDueDate <= :to')
                        ->setParameter('to', new \DateTime($filters['date']['to']));
                }
                if (!empty($filters['date']['createdFrom'])) {
                    $query->andWhere('date(pr.dateCreate) >= :createdFrom')
                        ->setParameter('createdFrom', new \DateTime($filters['date']['createdFrom']));
                }
                if (!empty($filters['date']['createdTo'])) {
                    $query->andWhere('date(pr.dateCreate) <= :createdTo')
                        ->setParameter('createdTo', new \DateTime($filters['date']['createdTo']));
                }
            }
        }
    }

    /**
     * Returns single result of current month proposals count query in array with 'currentMonthProposalsCount' key
     *
     * @return array
     */
    public function getCurrentMonthProposalsCount() : array
    {
        $query = $this->createQueryBuilder('pr')
            ->select('COUNT(pr) AS currentMonthProposalsCount')
            ->where('pr.dateCreate >= :from')
            ->andWhere('pr.dateCreate <= :to')
            ->setParameter('from', new \DateTime('first day of this month 00:00:00'))
            ->setParameter('to', new \DateTime('last day of this month 23:59:59'))
        ;

        $result = $query->getQuery()->getSingleResult();

        return $result;
    }

    /**
     * @param Account $account
     * @param null $division
     * @return mixed
     */
    public function getOldestEarliestDate(Account $account, $division = null)
    {
        $query = $this->createQueryBuilder("pr")
            ->select('MIN(pr.earliestDueDate)')
            ->where('pr.account = :account')
            ->setParameter('account', $account);

        if (!empty($division)) {
            $query->andWhere('pr.division = :division')
                ->setParameter('division', $division);
        }

        return $query->getQuery()->getOneOrNullResult()[1];
    }

    /**
     * @param \DateTime $futureDate
     * @param ProposalStatus $status
     * @return array
     */
    public function getTenDaysBeforeRetestNoticeEarliestDueDate(\DateTime $futureDate, ProposalStatus $status)
    {
        $query = $this->createQueryBuilder("pr")
            ->select()
            ->innerJoin('pr.status', 'prs')
            ->leftJoin("pr.type", 't')
            ->where('pr.earliestDueDate <= :futureDate')
            ->andWhere('pr.earliestDueDate >= CURRENT_DATE()')
            ->andWhere("prs.priority < :priority")
            ->andWhere("prs.priority >= 2")
            ->andWhere("prs.alias <> 'rejected_by_client'")
            ->andWhere("t.alias='retest'")
            ->setParameters(
                [
                    'futureDate' => $futureDate,
                    'priority' => $status->getPriority()
                ]
            );

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $limitDate
     * @param $exceptStatus
     * @return array
     */
    public function getNextSomeDaysAfterRetestNoticeEarliestDueDate(\DateTime $limitDate, ProposalStatus $exceptStatus)
    {
        $query = $this->createQueryBuilder("pr")
            ->select()
            ->leftJoin('pr.status', 'prs')
            ->leftJoin("pr.type", 't');

        if ($exceptStatus->getAlias() === 'past_due') {
            $query->where('pr.earliestDueDate < CURRENT_DATE()');
            $query->andWhere('pr.earliestDueDate >= :limitDate');

        } elseif ($exceptStatus->getAlias() === 'no_response') {
            $query->where("pr.earliestDueDate <= :limitDate");

        } elseif ($exceptStatus->getAlias() === 'send_past_due_email') {
            $query->where("pr.earliestDueDate = :limitDate");
        }

        $query
            ->andWhere("prs.alias <> 'done'")
            ->andWhere("prs.alias <> 'rejected_by_client'")
            ->andWhere("prs.alias <> :exceptStatus")
            ->andWhere("prs.priority < :statsPriority")
            ->andWhere("prs.priority >= 2")
            ->andWhere("t.alias='retest'")
            ->setParameters(
                [
                    'limitDate' => $limitDate->format('Y-m-d'),
                    'exceptStatus' => $exceptStatus->getAlias(),
                    'statsPriority' => $exceptStatus->getPriority()
                ]
            );

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $nowDate
     * @return array
     */
    public function getRetestProposalsWhichNeedCall(\DateTime $nowDate)
    {
        $forEarliestDueDate = clone($nowDate);
        $forEarliestDueDate = $forEarliestDueDate->modify("+20 days")->format("Y-m-d");
        $forFirstSendingDate = $nowDate->modify("-10 days")->format("Y-m-d H:i:s");

        $query = $this->createQueryBuilder('pr');
        $query
            ->leftJoin("pr.status", 'st')
            ->leftJoin("pr.type", 't')
            ->where("pr.earliestDueDate <= '".$forEarliestDueDate."'")
            ->orWhere("pr.firstSendingDate <= '". $forFirstSendingDate."' AND pr.firstSendingDate <> ''")
            ->andWhere("st.priority < 4")
            ->andWhere("st.priority >= 2")
            ->andWhere("st.alias <> 'rejected_by_client'")
            ->andWhere("t.alias='retest'")
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param \DateTime $nowDate
     * @param array $typesAlias
     * @return array
     */
    public function getProposalsWhichNeedCall(\DateTime $nowDate, array $typesAlias)
    {
        $forFirstSendingDate = $nowDate->modify("-5 days")->format("Y-m-d H:i:s");

        $query = $this->createQueryBuilder('pr');
        $query
            ->leftJoin("pr.status", 'st')
            ->leftJoin("pr.type", 't')
            ->where("pr.firstSendingDate <= '". $forFirstSendingDate."' AND pr.firstSendingDate <> ''")
            ->andWhere("st.priority < 4")
            ->andWhere("st.priority >= 2")
            ->andWhere("st.alias <> 'rejected_by_client'")
            ->andWhere('t.alias IN (:types)')
            ->setParameters([
                'types' => $typesAlias
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param array $filters
     * @return array
     */
    public function findRetestNoticesForMergedPdfByFilters(array $filters)
    {
        $query = $this->createQueryBuilder('pr')
            ->select('pr')
            ->leftJoin('pr.division', 'd')
            ->leftJoin('pr.account', 'a')
            ->leftJoin('pr.type', 't')
            ->leftJoin('pr.creator', 'c')
            ->leftJoin('pr.status', 's')
            ->leftJoin("pr.sendingAddress", "sa")
            ->leftJoin("pr.recipient", "acp")
            ->leftJoin("acp.contactPerson", "cp")
            ->where("s.alias <> 'reset_services_to_next_time'")
            ->andWhere("s.alias <> 'draft'")
            ->andWhere("s.alias <> 'deleted'")
            ->andWhere("s.alias <> 'workorder_created'")
            ->andWhere('t.alias = \'retest\'')
            ->addOrderBy("sa.zip", "ASC")
            ->addOrderBy("sa.address", "ASC")
            ->addOrderBy("cp.lastName", "ASC")
            ->addOrderBy("cp.firstName", "ASC")
            ->addOrderBy("cp.company", "ASC")
        ;

        $this->someFilters($query, $filters);

        return $query->getQuery()->getResult();
    }

    /**
     * @param Account $account
     * @param DeviceCategory $deviceCategory
     * @return array
     */
    public function getProposalByAccountAndDivision(Account $account, DeviceCategory $deviceCategory)
    {
        return $this->createQueryBuilder('pr')
            ->leftJoin('pr.account', 'a')
            //->leftJoin('ah.ownerEntity', 'a')
            ->leftJoin('pr.division', 'div')
            ->leftJoin('pr.status', 'prs')
            ->leftJoin('pr.type', 'prt')
            ->where('a.id = :account')
            ->andWhere('div.id = :division')
            ->andWhere('prs.alias = \'to_be_sent\'')
            ->andWhere('prt.alias = \'repair\'')
            ->setParameters(['account' => $account->getId(), 'division' => $deviceCategory->getId()])
            ->getQuery()->getResult();
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     *
     * @return Proposal
     */
    public function findDraftRetestNotice(Account $account, DeviceCategory $division)
    {
        $query = $this->getQueryFindDraft($account, $division);
        $query->andWhere('pt.alias = \'retest\'');
        $result = $query->getQuery()->getResult();

        /** @var Proposal $repairProposal */
        $repairProposal = $result ? $result[0] : null;

        return $repairProposal;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     *
     * @return Proposal
     */
    public function findDraftRepairProposal(Account $account, DeviceCategory $division)
    {
        $query = $this->getQueryFindDraft($account, $division);
        $query->andWhere('pt.alias = \'repair\'');
        $result = $query->getQuery()->getResult();

        /** @var Proposal $repairProposal */
        $repairProposal = $result ? $result[0] : null;

        return $repairProposal;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     *
     * @return QueryBuilder
     */
    private function getQueryFindDraft(Account $account, DeviceCategory $division)
    {
        /** @var QueryBuilder $query */
        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.account', 'a')
            ->leftJoin('p.division', 'd')
            ->leftJoin('p.status', 'ps')
            ->leftJoin('p.type', 'pt')
            ->where('a.id = :account')
            ->andWhere('d.id = :division')
            ->andWhere('ps.alias = \'draft\'')
            ->setParameters([
                'account' => $account->getId(),
                'division' => $division->getId()
            ]);

        return $query;
    }

    /**
     * @param QueryBuilder $query
     * @param $filters
     * @return QueryBuilder
     */
    private function someOrders(QueryBuilder $query, $filters)
    {
        if (!empty($filters['division'])) {
            switch ($filters['division']) {
                case '1': $query->orderBy('a.oldestProposalBackflowDate', 'ASC'); break;
                case '2': $query->orderBy('a.oldestProposalFireDate', 'ASC'); break;
                case '3': $query->orderBy('a.oldestProposalPlumbingDate', 'ASC'); break;
                case '4': $query->orderBy('a.oldestProposalAlarmDate', 'ASC'); break;
                case 'all': $query->orderBy('a.oldestProposalDate', 'ASC'); break;
            }
        } else {
            $query->orderBy('a.oldestProposalDate', 'ASC');
        }

        $query->addOrderBy('pr.division');
        $query->addOrderBy('pr.earliestDueDate');

        return $query;
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTheLatestCloned()
    {
        $query = $this->createQueryBuilder('pr')
            ->select('MAX(pr.clone)')
        ;

        $latestCloneId = $query->getQuery()->getSingleResult();

        $query = $this->createQueryBuilder('pr')
            ->where('pr.clone = :latestCloneId')
            ->setParameter('latestCloneId', $latestCloneId)
        ;

        return $query->getQuery()->getSingleResult();
    }

    /**
     * @param $account
     * @param null $typesAlias
     * @return mixed
     */
    public function getCountProposals($account, $typesAlias = null)
    {
        $query = $this->createQueryBuilder("pr")
                ->select("count(pr.id)")
                ->leftJoin('pr.account', 'a')// I done it because one that moment was old logic "create proposal"
            // TODO: FROZEN
//                ->leftJoin('ah.ownerEntity', 'a') // I done it because one that moment was old logic "create proposal"
                ->where('a.id = :account');

        if ($typesAlias) {
            $query
                ->leftJoin('pr.type', 't')
                ->andWhere('t.alias IN (:types)')
                ->setParameters([
                    'types' => $typesAlias
                ]);
        };

        $query->setParameter('account', $account);
        $result = $query->getQuery()->getResult();

        return $result['0']['1'];
    }
}
