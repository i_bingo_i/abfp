<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Municipality;

/**
 * DepartmentChannelDynamicFieldValueRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DepartmentChannelDynamicFieldValueRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param Municipality $municipality
     * @param DeviceCategory $division
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getEmailDepartmentChannelValByMunAndDiv(
        Municipality $municipality,
        DeviceCategory $division
    ) {
        $qb = $this->createQueryBuilder("dcv");
        $qb
            ->leftJoin("dcv.departmentChanel", "dc")
            ->leftJoin("dc.named", "dcn")
            ->leftJoin('dc.department', 'd')
            ->where('d.division = :division')
            ->andWhere('d.municipality =:municipality')
            ->andWhere('dc.deleted = false')
            ->addOrderBy("dc.isDefault", "DESC")
            ->addOrderBy("dc.dateCreate", "DESC")
            ->andWhere("dcn.alias = 'email'")
            ->setParameters(
                [
                    'division' => $division,
                    'municipality' => $municipality
                ]
            );
        
        $channels = $qb->getQuery()->getResult();

        return $channels ? $channels[0] : false;
    }
}
