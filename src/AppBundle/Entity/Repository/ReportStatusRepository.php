<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ReportStatusRepository
 */
class ReportStatusRepository extends EntityRepository
{
    /**
     * @param $aliases
     * @return array
     */
    public function findAllExceptAliases($aliases)
    {
        $query = $this->createQueryBuilder('s');
        $query->andWhere($query->expr()->notIn('s.alias', $aliases));

        return $query->getQuery()->getResult();
    }
}
