<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\Message;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Opportunity;
use \Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Company;
use Doctrine\ORM\QueryBuilder;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;
use AppBundle\Entity\Account;
use Doctrine\ORM\Query\Expr\Select;
use Doctrine\ORM\Query\Expr\From;
use Doctrine\ORM\Query\Expr\Join;

/**
 * AccountRepository
 */
class AccountRepository extends EntityRepository
{
    use PaginateQueryTrait;

    private const NOTHING = 0;

    public function getCount()
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->select('COUNT(a.id)')
            ->where('a.deleted=0');
        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    /**
     * @param Company $company
     * @param int $page
     * @param int $maxItems
     * @return array
     */
    public function findByCompany(Company $company, $page = 1, $maxItems = 15)
    {
        /** @var QueryBuilder $query */
        $query = $this->createQueryBuilder("c");

        $query
            ->where('c.company = :company')
            ->setParameter('company', $company);

        return $this->paginate($query->getQuery(), $page, $maxItems);
    }

    /**
     * @param Company $company
     * @return Account[]
     */
    public function findByCompanyWithoutPagination(Company $company)
    {
        /** @var QueryBuilder $query */
        $query = $this->createQueryBuilder("c");

        $query
            ->where('c.company = :company')
            ->setParameter('company', $company);

        /** @var Account[] $accounts */
        $accounts = $query->getQuery()->getResult();

        return $accounts;
    }

    /**
     * @param Account $account
     * @return mixed
     */
    public function getChildrenCount(Account $account)
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->select('COUNT(a.id)')
            ->where('a.parent = :account')
            ->setParameter('account', $account);
        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    /**
     * @param Account $account
     * @return int|mixed
     */
    public function getSiteCount(Account $account)
    {
        if (!empty($account->getParent())) {
            /** @var Account $parent */
            $parent = $this->find($account->getParent()->getId());
            return $this->getChildrenCount($parent);
        }
        return self::NOTHING;
    }

    /**
     * Attention:
     * - not view Alarm opportunity (by Taras)
     *
     * @param $filters
     * @return mixed
     */
    public function getQuantitiesWithFilters($filters)
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->select(
                'COUNT(DISTINCT a.id) as account',
                'COUNT(DISTINCT o.id) as opportunity',
                'COUNT(s.id) as services',
                'COUNT(DISTINCT d.id) as device'
            )
            ->innerJoin('a.opportunity', 'o', 'a.id=o.account')
            ->innerJoin('o.services', 's', 'o.id=s.opportunity')
            ->innerJoin('s.device', 'd', 's.device=d.id')
            ->leftJoin('o.division', 'dv') // add this code for not view Alarm opportunity (by Taras)
            ->where('d.deleted = 0')
            ->andWhere('s.deleted = 0');

        OpportunityRepository::someFilters($query, $filters);

        $result = $query->getQuery()->getSingleResult();

        return $result;
    }

    /**
     * Get devices and services for each division by Account and Division
     * @param $filters
     * @param $account
     * @param $division
     * @return mixed
     */
    public function getDevicesAndServices($filters, $account, $division)
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->select(
                'COUNT(DISTINCT d.id) as devices',
                'COUNT(DISTINCT s.id) as services'
            )
            ->innerJoin('a.opportunity', 'o', 'a.id=o.account')
            ->innerJoin('o.services', 's', 'o.id=s.opportunity')
            ->innerJoin('s.device', 'd', 's.device=d.id')
            ->innerJoin('d.named', 'dn', 'dn.id=d.named')
            ->innerJoin('dn.category', 'dc', 'dc.id=dn.category')
            ->where('dc.id=:division AND a.id=:account')
            ->andWhere('d.deleted = 0')
            ->andWhere('s.deleted = 0')
            ->setParameters(['division'=>$division, 'account'=>$account]);

        OpportunityRepository::someFilters($query, $filters);

        $result = $query->getQuery()->getSingleResult();

        return $result;
    }

    /**
     * @param Account $groupAccount
     * @return array
     */
    public function findChildrenWithOwnAuthorizerForGroupAccount(Account $groupAccount)
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->leftJoin('a.parent', 'p')
            ->leftJoin('a.messages', 'm')
            ->where('a.parent = :groupAccount')
            ->andWhere('m.alias = "site_has_own_authorizer"')
            ->setParameter('groupAccount', $groupAccount);

        /** @var Account[] $chirdren */
        $children = $query->getQuery()->getResult();

        return $children;
    }

    /**
     * @param Account $groupAccount
     * @param Message $message
     * @return array
     */
    public function findChildrenWithMessageForGroupAccount(Account $groupAccount, Message $message)
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->leftJoin('a.parent', 'p')
            ->leftJoin('a.messages', 'm')
            ->where('a.parent = :groupAccount')
            ->andWhere('m.alias = :messageAlias')
            ->setParameters([
                'groupAccount' => $groupAccount,
                'messageAlias' => $message->getAlias()
            ]);

        /** @var Account[] $children */
        $children = $query->getQuery()->getResult();

        return $children;
    }

    /**
     * @param ContactPerson $contact
     * @return Account[]
     */
    public function findByContactPerson(ContactPerson $contact)
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->leftJoin('a.contactPerson', 'acp')
            ->where('acp.contactPerson = :contact')
            ->andWhere('a.deleted = false')
            ->setParameter('contact', $contact);

        /** @var Account[] $accounts */
        $accounts = $query->getQuery()->getResult();

        return $accounts;
    }

    /**
     * @param Company $company
     * @return Account[]
     */
    public function findByContactPersonCompany(Company $company)
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->leftJoin('a.contactPerson', 'acp')
            ->leftJoin('acp.contactPerson', 'cp')
            ->where('cp.company = :company')
            ->andWhere('a.deleted = false')
            ->setParameter('company', $company);

        /** @var Account[] $accounts */
        $accounts = $query->getQuery()->getResult();

        return $accounts;
    }

    /**
     * @param Municipality $municipality
     * @param int $page
     * @param int $maxItems
     * @return array
     */
    public function getAccountsByMunicipalityWithPagination(Municipality $municipality, $page = 1, $maxItems = 50)
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->where('a.municipality = :municipality')
            ->setParameter('municipality', $municipality)
        ;

        return $this->paginate($query->getQuery(), $page, $maxItems);
    }

    public function getImportedAccount()
    {
        $query = $this->createQueryBuilder("a");
        $query
            ->select('a.oldAccountId')
            ->where("a.oldAccountId <> '' ")
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getAccountsWithPreEngineeredFireSuppressionSystemDevices()
    {
        $query = $this->createQueryBuilder("a");
        return $query
            ->leftJoin('a.devices', 'd')
            ->leftJoin('d.named', 'dn')
            ->where('dn.alias = \'pre_engineered_fire_suppression_system\'')
            ->getQuery()->getResult();
    }
}

