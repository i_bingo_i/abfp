<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;

/**
 * MunicipalityRepository
 */
class MunicipalityRepository extends EntityRepository
{
    use PaginateQueryTrait;

    /**
     * @param int $page
     * @param int $maxItems
     * @return array
     */
    public function findAllWithPagination(int $page = 1, int $maxItems = 15) : array
    {
        $query = $this->createQueryBuilder('m')
            ->orderBy('m.id', 'ASC');
        return $this->paginate($query->getQuery(), $page, $maxItems);
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        $query = $this->createQueryBuilder("m");
        $query->select('COUNT(m.id)');
        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    public function getImportedMunicipality()
    {
        $query = $this->createQueryBuilder("m");
        $query
            ->select('m.oldMunicipalityId')
            ->where("m.oldMunicipalityId <> '' ")
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param Account $account
     * @param DeviceCategory $deviceCategory
     * @return mixed
     */
    public function getMunicipalityFee(Account $account, DeviceCategory $deviceCategory)
    {
        $query = $this->createQueryBuilder('m');
        $query->select('(dch.uploadFee + dch.processingFee)')
            ->leftJoin('m.account', 'a')
            ->leftJoin('m.department', 'd')
            ->leftJoin('d.channels', 'dch')
            ->where('a.id = :account')
            ->andWhere('dch.isDefault = 1')
            ->andWhere('dch.deleted = false')
            ->andWhere('dch.active = true')
            ->andWhere('dch.feesWillVary = false')
            ->andWhere('dch.devision = :division')
            ->setParameters([
                'account' => $account,
                'division' => $deviceCategory
            ]);

        return !empty($query->getQuery()->getResult()) ? $query->getQuery()->getResult()[0][1] : 0;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $deviceCategory
     * @return mixed
     */
    public function getMunicipalityFeeAndProcessingFee(Account $account, DeviceCategory $deviceCategory)
    {
        $query = $this->createQueryBuilder('m');
        $query->select('dch.uploadFee, dch.processingFee, dch.feesWillVary')
            ->leftJoin('m.account', 'a')
            ->leftJoin('m.department', 'd')
            ->leftJoin('d.channels', 'dch')
            ->where('a.id = :account')
            ->andWhere('dch.isDefault = 1')
            ->andWhere('dch.devision = :division')
            ->andWhere('dch.deleted = false')
            ->andWhere('dch.active = true')
            ->setParameters([
                'account' => $account,
                'division' => $deviceCategory
            ]);
        $result = $query->getQuery()->getResult();

        return !empty($result) ? $result[0] : null;
    }
}
