<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;

/**
 * ProcessingStatsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProcessingStatsRepository extends \Doctrine\ORM\EntityRepository
{
    use PaginateQueryTrait;

    public function findAllWithPagination($filters)
    {
        $query = $this->createQueryBuilder('ps');
        $query
            ->select('ps')
            ->leftJoin('ps.type', "pst")
            ->orderBy("ps.dateTime", "DESC")
            ->orderBy("ps.id", "DESC")
        ;

        $page = isset($filters['page']) ? $filters['page'] : 1;
        $maxItems = !empty($filters['pagerCount']) ? $filters['pagerCount'] : 50;

        return $this->paginate($query->getQuery(), $page, $maxItems);
    }
}
