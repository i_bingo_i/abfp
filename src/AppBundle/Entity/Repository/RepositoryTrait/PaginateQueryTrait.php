<?php

namespace AppBundle\Entity\Repository\RepositoryTrait;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

trait PaginateQueryTrait
{
    /**
     * @param Query $query
     * @param int $page
     * @param int $maxItems
     *
     * @return array
     */
    private function paginate(Query $query, $page = 1, $maxItems = 10)
    {
        /** @var Paginator $paginator */
        $paginator = new Paginator($query);
        $paginator
            ->getQuery()
            ->setFirstResult(($page - 1) * $maxItems)
            ->setMaxResults($maxItems);

        $count = $paginator->count();
        $pages = ceil($count/$maxItems);

        return [
            'currentPage' => (int)$page,
            'pagesCount' => (int)$pages,
            'count' => $count,
            'results' => $paginator->getQuery()->getResult()
        ];
    }
}
