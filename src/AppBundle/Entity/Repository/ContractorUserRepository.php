<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ContractorUserRepository
 */
class ContractorUserRepository extends EntityRepository
{
    public function getImportedContractorUser()
    {
        $qb = $this->createQueryBuilder('cu');
        return $qb->select()
            ->leftJoin('cu.contractor', 'c')
            ->where($qb->expr()->isNotNull('c.oldSistemId'))
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $users
     * @return array
     */
    public function getContractorUsersByUsers($users)
    {
        $qb = $this->createQueryBuilder('cu');
        return $qb
            ->leftJoin('cu.user', 'u')
            ->where($qb->expr()->in('u.id', $users))
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }
}
