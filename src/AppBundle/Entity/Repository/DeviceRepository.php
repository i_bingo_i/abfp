<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Device;
use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Opportunity;
use AppBundle\Entity\Service;
use Doctrine\ORM\EntityRepository;

/**
 * DeviceRepository
 */
class DeviceRepository extends EntityRepository
{
    /**
     * @param Account $account
     * @param DeviceCategory $deviceCategory
     * @return array
     */
    public function findByAccountAndCategory(Account $account, DeviceCategory $deviceCategory)
    {

        $query = $this->createQueryBuilder("d")
            ->leftJoin('d.named', 'dnm')
            ->leftJoin('dnm.category', 'cat')
            ->where('d.account = :account')
            ->andWhere('dnm.category = :category')
            ->setParameters([
                'account' => $account,
                'category' => $deviceCategory
            ]);

        return $query->getQuery()->getResult();
    }

    /**
     * @param Account $account
     * @return mixed
     */
    public function getDeviceCount(Account $account)
    {
        $query = $this->createQueryBuilder("d");
        $query
            ->select('COUNT(d.id)')
            ->where('d.deleted=0')
            ->andWhere('d.account = :account')
            ->setParameter("account", $account)
        ;
        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    /**
     * @param Account $account
     * @return mixed
     */
    public function getDeviceCountIncludedDeleted(Account $account)
    {
        $query = $this->createQueryBuilder("d");
        $query
            ->select('COUNT(d.id)')
            ->where('d.account = :account')
            ->setParameter("account", $account)
        ;
        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    /**
     * @param $accounts
     * @return array
     */
    public function getDeviceByGroupAccountChildren($accounts)
    {
        $query = $this->createQueryBuilder("d");
        $query
            ->where("d.account  IN (:accounts)")
            ->Andwhere("d.deleted=0")
            ->setParameter("accounts", array_values($accounts))
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param Device $device
     * @param array $parents
     * @return array
     */
    public function getParents(Device $device, &$parents = [])
    {
        if ($parent = $device->getParent()) {
            array_push($parents, $parent);
            $this->getParents($parent, $parents);
        }

        return $parents;
    }

    /**
     * @param DeviceCategory $division
     * @param Account $account
     * @return mixed
     */
    public function getCountByDivisionForAccount(DeviceCategory $division, Account $account)
    {
        $query = $this->createQueryBuilder("d");
        $query
            ->select('COUNT(d.id)')
            ->leftJoin('d.named', 'dn')
            ->where('d.account = :account')
            ->andWhere("dn.category = :division")
            ->andWhere("d.deleted = false")
            ->setParameters([
                "account" => $account,
                "division" => $division
            ]);

        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    /**
     * @param DeviceCategory $division
     * @param Account[] $accounts
     * @return mixed
     */
    public function getCountByDivisionForGroupAccount(DeviceCategory $division, $accounts)
    {
        $query = $this->createQueryBuilder("d");
        $query
            ->select('COUNT(d.id)')
            ->leftJoin('d.named', 'dn')
            ->where('d.account IN (:accounts)')
            ->andWhere("dn.category = :division")
            ->andWhere("d.deleted = false")
            ->setParameters([
                "accounts" => array_values($accounts),
                "division" => $division
            ]);

        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    /**
     * @return array
     */
    public function getImportedDevices()
    {
        $query = $this->createQueryBuilder("d");
        $query
            ->select('d.oldDeviceId')
            ->where("d.oldDeviceId <> '' ")
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param Account $account
     * @return array
     */
    public function getAlarmPanels(Account $account)
    {
        $query = $this->createQueryBuilder("d");
        $query
            ->leftJoin('d.named', 'dn')
            ->where('d.account=:account')
            ->andWhere('d.deleted <> 1')
            ->andWhere(
                $query->expr()->orX(
                    'dn.alias=\'fire_alarm_control_panel\''
                )
            )
            ->setParameter('account', $account);

        return $query->getQuery()->getResult();
    }


    /**
     * @return array
     */
    public function getDevicesWithEmptyTitle()
    {
        $query = $this->createQueryBuilder("d");
        $query
//            ->where($query->expr()->isNull('d.title'));
            ->where("d.title IS NULL");

        return $query->getQuery()->getResult();
    }
}
