<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use \Doctrine\ORM\QueryBuilder;

/**
 * UserRepository
 */
class UserRepository extends EntityRepository
{
    /**
     * @return QueryBuilder
     */
    public function findAllQuery()
    {
        $query = $this->createQueryBuilder("u");

        return $query;
    }

    /**
     * @param User $user
     * @return array
     */
    public function findUniqueByEmail(User $user)
    {
        $query = $this->createQueryBuilder('us');
        $query
            ->select('')
            ->where('us.email = :email')
            ->setParameter("email", $user->getEmail());

        if ($user->getId()) {
            $query
                ->andWhere('us.id != :identifier')
                ->setParameter("identifier", $user->getId());
        }

        $users = $query->getQuery()->getResult();

        return $users;
    }
}
