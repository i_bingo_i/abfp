<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ContactPerson;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use AppBundle\Entity\Company;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * ContactPersonRepository
 */
class ContactPersonRepository extends EntityRepository
{
    use PaginateQueryTrait;

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCount()
    {
        $query = $this->createQueryBuilder("c");
        $query
            ->select('COUNT(c.id)')
            ->where('c.deleted=0');
        $result = $query->getQuery()->getSingleResult();

        return $result[1];
    }

    /**
     * @param Company $company
     * @param int $page
     * @param int $maxItems
     * @return array
     */
    public function findByCompany(Company $company, $page = 1, $maxItems = 15)
    {
        /** @var QueryBuilder $query */
        $query = $this->createQueryBuilder("c");

        $query
            ->where('c.company = :company')
            ->setParameter('company', $company);

        return $this->paginate($query->getQuery(), $page, $maxItems);
    }

    /**
     * @param Company $company
     * @return array
     */
    public function getByCompany(Company $company)
    {
        /** @var QueryBuilder $query */
        $query = $this->createQueryBuilder("c");
        $query
            ->where('c.company = :company')
            ->andWhere('c.deleted = 0')
            ->andWhere($query->expr()->orX('c.sourceEntity = \'Org\'', 'c.sourceEntity = \'Master\''))
            ->setParameter('company', $company);

        return $query->getQuery()->getResult();
    }

    /**
     * @param integer $sourceId
     * @return array
     */
    public function getBySourceIdCompany($sourceId)
    {
        /** @var QueryBuilder $query */
        $query = $this->createQueryBuilder("c");
        $query
            ->where('c.sourceId = :sourceId')
            ->andWhere('c.deleted = 0')
            ->andWhere('c.sourceEntity = \'Master\'')
            ->setParameter('sourceId', $sourceId);

        return $query->getQuery()->getResult();
    }

    /**
     * @return ContactPerson[]
     */
    public function findAllWithoutCustomer()
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(ContactPerson::class, 'cp');

        $query = $this->_em->createNativeQuery('SELECT * FROM contact_person AS cp WHERE (SELECT COUNT(*) FROM customer AS cm LEFT JOIN entity_type et ON cm.entity_type_id = et.id WHERE et.alias="contact_person" and cm.entity_id=cp.id) < 1', $rsm);
        /** @var ContactPerson[] $contactPersons */
        $contactPersons = $query->getResult();

        return $contactPersons;
    }
}
