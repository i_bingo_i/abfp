<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ProposalStatusRepository
 */
class ProposalStatusRepository extends EntityRepository
{
    /**
     * @param $aliases
     * @return array
     */
    public function findByAliases($aliases)
    {
        $query = $this->createQueryBuilder('s')
            ->andWhere('s.alias IN (:aliases)')
            ->setParameters([
                'aliases' => $aliases
            ]);

        $statuses = $query->getQuery()->getResult();
        return $statuses;
    }

    /**
     * @param $aliases
     * @return array
     */
    public function findAllExceptAliases($aliases)
    {
        $query = $this->createQueryBuilder('s');
        $query->andWhere($query->expr()->notIn('s.alias', $aliases));

        return $query->getQuery()->getResult();
    }
}
