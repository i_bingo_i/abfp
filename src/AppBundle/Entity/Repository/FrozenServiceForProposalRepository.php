<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Device;
use AppBundle\Entity\FrozenProposal;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Service;
use Doctrine\ORM\EntityRepository;

/**
 * FrozenServiceForProposalRepository
 */
class FrozenServiceForProposalRepository extends EntityRepository
{
    /**
     * @param Device $device
     * @param FrozenProposal $frozenProposal
     * @return array
     */
    public function findRepairByDeviceForProposal(Device $device, FrozenProposal $frozenProposal)
    {
        $query = $this->createQueryBuilder('fsfp');

        $query->leftJoin('fsfp.serviceHistory', 'sh')
            ->leftJoin('sh.named', 'sn')
            ->leftJoin('sh.device', 'd')
            ->where('sn.frequency IS NULL')
            ->andWhere('sh.device = :device')
            ->andWhere('fsfp.frozenProposal = :frozenProposal')
            ->andWhere('sh.deleted = 0')
            ->setParameter('device', $device)
            ->setParameter('frozenProposal', $frozenProposal);

        return $query->getQuery()->getResult();
    }


    /**
     * @param Service $service
     * @return mixed
     */
    public function findRepairByServiceForProposal(Service $service)
    {
        $query = $this->createQueryBuilder('fs');

        $query->leftJoin('fs.serviceHistory', 'sh')
            ->where('sh.ownerEntity = :service')
            ->setParameter('service', $service);

        return $query->getQuery()->getSingleResult();
    }
}
