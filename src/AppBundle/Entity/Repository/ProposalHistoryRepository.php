<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalHistory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

/**
 * ProposalHistoryRepository
 */
class ProposalHistoryRepository extends EntityRepository
{
    /**
     * @param Proposal $proposal
     * @return ProposalHistory|null
     */
    public function findLastByProposal(Proposal $proposal)
    {
        $query = $this->createQueryBuilder("ph");
        $query->where('ph.ownerEntity = :proposal')
            ->setParameter('proposal', $proposal)
            ->orderBy('ph.dateCreate');

        try {
            /** @var ProposalHistory $proposalHistory */
            $proposalHistory = $query->getQuery()->getSingleResult();
        } catch (NoResultException $noResultException) {
            return null;
        } catch (NonUniqueResultException $nonUniqueResultException) {
            return null;
        }

        return $proposalHistory;
    }
}
