<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\DeviceCategory;
use Doctrine\ORM\EntityRepository;


/**
 * AccountContactPersonRepository
 */
class AccountContactPersonRepository extends EntityRepository
{
    /**
     * @param Account $account
     * @return int
     */
    public function getCount(Account $account)
    {
        $query = $this->createQueryBuilder("acp");
        $query
            ->select('COUNT(acp.id)')
            ->where('acp.account = :account')
            ->andWhere('acp.deleted=false')
            ->setParameter('account', $account)
            ->groupBy('acp.contactPerson');

        $result = $query->getQuery()->getResult();

        return count($result);
    }

    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @return int
     */
    public function getCountForAccountAndContact(Account $account, ContactPerson $contactPerson)
    {
        $query = $this->createQueryBuilder("acp");
        $query
            ->select('COUNT(acp.id)')
            ->where('acp.account = :account')
            ->andWhere('acp.contactPerson = :contactPerson')
            ->andWhere('acp.deleted=false')
            ->setParameters([
                'account' => $account,
                'contactPerson' => $contactPerson
            ])
            ->groupBy('acp.contactPerson');

        $result = $query->getQuery()->getResult();

        return count($result);
    }

    /**
     * @param $account
     * @return array
     */
    public function getAccountContactPersonsForAccount($account)
    {
        $query = $this->createQueryBuilder("acp")
            ->where("acp.account = :account")
            ->andWhere("acp.deleted=false")
            ->setParameter('account', $account);

        $result =  $query->getQuery()->getResult();

        return $result;
    }

    /**
     * @param Account $account
     * @return AccountContactPerson[]
     */
    public function findPrimaryAuthorizersByAccount(Account $account)
    {
        $query = $this->createQueryBuilder("acp")
            ->innerJoin('acp.responsibilities', 'r')
            ->where("acp.account = :account")
            ->andWhere("acp.deleted=false")
            ->andWhere("r.id > 0")
            ->setParameter('account', $account);

        /** @var AccountContactPerson[] $authorizers */
        $authorizers =  $query->getQuery()->getResult();

        return $authorizers;
    }

    /**
     * @param Account $account
     * @param $divisionIds
     * @return AccountContactPerson[]
     */
    public function findPrimaryAuthorizersByAccountForDivisions(Account $account, $divisionIds)
    {
        $query = $this->createQueryBuilder("acp")
            ->innerJoin('acp.responsibilities', 'r')
            ->where("acp.account = :account")
            ->andWhere("acp.deleted=false")
            ->andWhere("r.id IN (:divisions)")
            ->setParameters([
                'account' => $account,
                'divisions' => array_values($divisionIds)
            ]);

        /** @var AccountContactPerson[] $authorizers */
        $authorizers =  $query->getQuery()->getResult();

        return $authorizers;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     *
     * @return AccountContactPerson
     */
    public function findAuthorizerByDivision(Account $account, DeviceCategory $division)
    {
        $query = $this->createQueryBuilder("acp")
            ->innerJoin('acp.responsibilities', 'r')
            ->where("acp.account = :account")
            ->andWhere("acp.deleted=false")
            ->andWhere("r.id = :division")
            ->setParameters([
                'account' => $account,
                'division' => $division
            ]);

        /** @var AccountContactPerson $authorizer */
        $authorizer = $query->getQuery()->getResult() ? $query->getQuery()->getResult()[0] : null;

        return $authorizer;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @return null
     */
    public function findPrimaryPaymentByDivisionForAccount(Account $account, DeviceCategory $division)
    {
        $query = $this->createQueryBuilder("acp")
            ->innerJoin("acp.paymentResponsibilities", "pr")
            ->where("acp.account = :account")
            ->andWhere("acp.deleted=false")
            ->andWhere("acp.payment = true")
            ->andWhere("pr.id = :division")
            ->setParameters([
                "account" => $account,
                "division" => $division
            ]);

        $paymentByDivision = $query->getQuery()->getResult() ? $query->getQuery()->getResult()[0] : null;

        return $paymentByDivision;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @return null
     */
    public function ifPaymentHasAuthorizerResponsibilityByDivisionForAccount(Account $account, DeviceCategory $division)
    {
        $query = $this->createQueryBuilder("acp")
            ->innerJoin("acp.paymentResponsibilities", "pr")
            ->innerJoin("acp.responsibilities", "ar")
            ->where("acp.account = :account")
            ->andWhere("acp.deleted=false")
            ->andWhere("acp.payment = true")
            ->andWhere("acp.authorizer = true")
            ->andWhere("pr.id = :division")
            ->andWhere("ar.id = :division")
            ->setParameters([
                "account" => $account,
                "division" => $division
            ]);

        return $query->getQuery()->getResult() ? $query->getQuery()->getResult()[0] : null;
    }
}
