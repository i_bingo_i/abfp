<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Message;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Account;

/**
 * MessageRepository
 */
class MessageRepository extends EntityRepository
{
    /**
     * @param Account $account
     * @return Message[]
     */
    public function findByAccount(Account $account)
    {
        $query = $this->createQueryBuilder("m");
        $query
            ->innerJoin('m.accounts', 'a')
            ->where('a.deleted=false')
            ->andWhere('a = :account')
            ->setParameter('account', $account)
        ;
        /** @var Message[] $messages */
        $messages = $query->getQuery()->getResult();

        return $messages;
    }

    /**
     * @param Account $account
     * @return Message[]
     */
    public function findErrorByAccount(Account $account)
    {
        $query = $this->createQueryBuilder("m");
        $query
            ->innerJoin('m.accounts', 'a')
            ->leftJoin('m.type', 't')
            ->where('a.deleted=false')
            ->andWhere('a = :account')
            ->andWhere('t.alias = :type')
            ->setParameters([
                'account' => $account,
                'type' => 'error'
            ])
        ;
        /** @var Message[] $messages */
        $messages = $query->getQuery()->getResult();

        return $messages;
    }

    /**
     * @param $messageAlias
     * @param Account $account
     * @return Message|bool
     */
    public function isByAliasForAccount($messageAlias, Account $account)
    {
        $query = $this->createQueryBuilder("m");
        $query
            ->innerJoin('m.accounts', 'a')
            ->where('a.deleted=false')
            ->andWhere('a = :account')
            ->andWhere('m.alias = :messageAlias')
            ->setParameters([
                'account' => $account,
                'messageAlias' => $messageAlias
            ])
        ;

        /** @var Message[] $messages */
        $messages = $query->getQuery()->getResult();
        if ($messages) {
            return $messages[0];
        }

        return false;
    }

    /**
     * @param $messageAlias
     * @param Account $account
     * @return Message|bool
     */
    public function isByAliasForChildrenAccount($messageAlias, Account $account)
    {
        $query = $this->createQueryBuilder("m");
        $query
            ->innerJoin('m.accounts', 'a')
            ->where('a.deleted=false')
            ->andWhere('a.parent = :account')
            ->andWhere('m.alias = :messageAlias')
            ->setParameters([
                'account' => $account,
                'messageAlias' => $messageAlias
            ])
        ;

        /** @var Message[] $messages */
        $messages = $query->getQuery()->getResult();
        if ($messages) {
            return $messages[0];
        }

        return false;
    }
}
