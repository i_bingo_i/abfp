<?php

namespace AppBundle\Entity;

/**
 * OpportunityStatus
 */
class OpportunityStatus
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alisa;

    /**
     * @var integer
     */
    private $sort;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OpportunityStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alisa
     *
     * @param string $alisa
     *
     * @return OpportunityStatus
     */
    public function setAlisa($alisa)
    {
        $this->alisa = $alisa;

        return $this;
    }

    /**
     * Get alisa
     *
     * @return string
     */
    public function getAlisa()
    {
        return $this->alisa;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return OpportunityStatus
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }
}
