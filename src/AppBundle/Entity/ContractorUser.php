<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\PhoneFilterTrait;

/**
 * ContractorUser
 *
 * DO NOT REGENERATE THIS ENTITY!! Serializer is here!!
 *
 * Added code filter for personal phone, work phone and fax  (don't auto generate this entity)
 */
class ContractorUser
{
    use PhoneFilterTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var string
     */
    private $signature;

    /**
     * @var boolean
     */
    private $active = true;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $personalEmail;

    /**
     * @var string
     */
    private $personalPhone;

    /**
     * @var string
     */
    private $workPhone;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $address;

    /**
     * @var \AppBundle\Entity\Licenses
     */
    private $license;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Contractor
     */
    private $contractor;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $equipment;

    /**
     * @var boolean
     */
    private $isHistoryProcessed = false;

    /**
     * @var string
     */
    private $msCalendarId;

    /**
     * @var string
     */
    private $msRefreshToken;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * Add equipment
     *
     * @param \AppBundle\Entity\Equipment $equipment
     *
     * @return ContractorUser
     */
    public function addEquipment(\AppBundle\Entity\Equipment $equipment)
    {
        $this->equipment[] = $equipment;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return ContractorUser
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set signature
     *
     * @param string $signature
     *
     * @return ContractorUser
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ContractorUser
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ContractorUser
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set personalEmail
     *
     * @param string $personalEmail
     *
     * @return ContractorUser
     */
    public function setPersonalEmail($personalEmail)
    {
        $this->personalEmail = $personalEmail;

        return $this;
    }

    /**
     * Get personalEmail
     *
     * @return string
     */
    public function getPersonalEmail()
    {
        return $this->personalEmail;
    }

    /**
     * Set personalPhone
     *
     * @param string $personalPhone
     *
     * @return ContractorUser
     */
    public function setPersonalPhone($personalPhone)
    {
        $this->personalPhone = $this->filterPhone($personalPhone);

        return $this;
    }

    /**
     * Get personalPhone
     *
     * @return string
     */
    public function getPersonalPhone()
    {
        return $this->personalPhone;
    }

    /**
     * Set workPhone
     *
     * @param string $workPhone
     *
     * @return ContractorUser
     */
    public function setWorkPhone($workPhone)
    {
        $this->workPhone = $this->filterPhone($workPhone);

        return $this;
    }

    /**
     * Get workPhone
     *
     * @return string
     */
    public function getWorkPhone()
    {
        return $this->workPhone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return ContractorUser
     */
    public function setFax($fax)
    {
        $this->fax =  $this->filterPhone($fax);

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return ContractorUser
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set license
     *
     * @param \AppBundle\Entity\Licenses $license
     *
     * @return ContractorUser
     */
    public function setLicense(\AppBundle\Entity\Licenses $license = null)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license
     *
     * @return \AppBundle\Entity\Licenses
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return ContractorUser
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set contractor
     *
     * @param \AppBundle\Entity\Contractor $contractor
     *
     * @return ContractorUser
     */
    public function setContractor(\AppBundle\Entity\Contractor $contractor = null)
    {
        $this->contractor = $contractor;

        return $this;
    }

    /**
     * Get contractor
     *
     * @return \AppBundle\Entity\Contractor
     */
    public function getContractor()
    {
        return $this->contractor;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->equipment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Remove equipment
     *
     * @param \AppBundle\Entity\Equipment $equipment
     */
    public function removeEquipment(\AppBundle\Entity\Equipment $equipment)
    {
        $this->equipment->removeElement($equipment);
    }

    /**
     * Get equipment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipment()
    {
        return $this->equipment;
    }

    /**
     * Set isHistoryProcessed
     *
     * @param boolean $isHistoryProcessed
     *
     * @return ContractorUser
     */
    public function setIsHistoryProcessed($isHistoryProcessed)
    {
        $this->isHistoryProcessed = $isHistoryProcessed;

        return $this;
    }

    /**
     * Get isHistoryProcessed
     *
     * @return boolean
     */
    public function getIsHistoryProcessed()
    {
        return $this->isHistoryProcessed;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->getUser()->getFirstName();
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->getUser()->getLastName();
    }


    public function getUserEmail()
    {
        return $this->getUser()->getEmail();
    }

    /**
     * Set msRefreshToken
     *
     * @param string $msRefreshToken
     *
     * @return ContractorUser
     */
    public function setMsRefreshToken($msRefreshToken)
    {
        $this->msRefreshToken = $msRefreshToken;

        return $this;
    }

    /**
     * Get msRefreshToken
     *
     * @return string
     */
    public function getMsRefreshToken()
    {
        return $this->msRefreshToken;
    }

    /**
     * Set msCalendarId
     *
     * @param string $msCalendarId
     *
     * @return ContractorUser
     */
    public function setMsCalendarId($msCalendarId)
    {
        $this->msCalendarId = $msCalendarId;

        return $this;
    }

    /**
     * Get msCalendarId
     *
     * @return string
     */
    public function getMsCalendarId()
    {
        return $this->msCalendarId;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ContractorUser
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
