<?php

namespace AppBundle\Entity;

/**
 * MunicipalityHistory
 */
class MunicipalityHistory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $website;

    /**
     * @var string
     */
    private $municipalityPhone;

    /**
     * @var string
     */
    private $municipalityFax;

    /**
     * @var \DateTime
     */
    private $dateSave;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $account;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $department;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $address;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * @var \AppBundle\Entity\Municipality
     */
    private $ownerEntity;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->account = new \Doctrine\Common\Collections\ArrayCollection();
        $this->department = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MunicipalityHistory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return MunicipalityHistory
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set municipalityPhone
     *
     * @param string $municipalityPhone
     *
     * @return MunicipalityHistory
     */
    public function setMunicipalityPhone($municipalityPhone)
    {
        $this->municipalityPhone = $municipalityPhone;

        return $this;
    }

    /**
     * Get municipalityPhone
     *
     * @return string
     */
    public function getMunicipalityPhone()
    {
        return $this->municipalityPhone;
    }

    /**
     * Set municipalityFax
     *
     * @param string $municipalityFax
     *
     * @return MunicipalityHistory
     */
    public function setMunicipalityFax($municipalityFax)
    {
        $this->municipalityFax = $municipalityFax;

        return $this;
    }

    /**
     * Get municipalityFax
     *
     * @return string
     */
    public function getMunicipalityFax()
    {
        return $this->municipalityFax;
    }

    /**
     * Set dateSave
     *
     * @param \DateTime $dateSave
     *
     * @return MunicipalityHistory
     */
    public function setDateSave($dateSave)
    {
        $this->dateSave = $dateSave;

        return $this;
    }

    /**
     * Get dateSave
     *
     * @return \DateTime
     */
    public function getDateSave()
    {
        return $this->dateSave;
    }

    /**
     * Add account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return MunicipalityHistory
     */
    public function addAccount(\AppBundle\Entity\Account $account)
    {
        $this->account[] = $account;

        return $this;
    }

    /**
     * Remove account
     *
     * @param \AppBundle\Entity\Account $account
     */
    public function removeAccount(\AppBundle\Entity\Account $account)
    {
        $this->account->removeElement($account);
    }

    /**
     * Get account
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return MunicipalityHistory
     */
    public function addDepartment(\AppBundle\Entity\Department $department)
    {
        $this->department[] = $department;

        return $this;
    }

    /**
     * Remove department
     *
     * @param \AppBundle\Entity\Department $department
     */
    public function removeDepartment(\AppBundle\Entity\Department $department)
    {
        $this->department->removeElement($department);
    }

    /**
     * Get department
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return MunicipalityHistory
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return MunicipalityHistory
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set ownerEntity
     *
     * @param \AppBundle\Entity\Municipality $ownerEntity
     *
     * @return MunicipalityHistory
     */
    public function setOwnerEntity(\AppBundle\Entity\Municipality $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \AppBundle\Entity\Municipality
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }
}
