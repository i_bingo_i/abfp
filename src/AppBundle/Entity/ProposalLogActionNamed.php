<?php

namespace AppBundle\Entity;

/**
 * ProposalLogActionNamed
 */
class ProposalLogActionNamed
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $route;

    /**
     * @var boolean
     */
    private $displayForActive = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProposalLogActionNamed
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return ProposalLogActionNamed
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set route
     *
     * @param string $route
     *
     * @return ProposalLogActionNamed
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set displayForActive
     *
     * @param boolean $displayForActive
     *
     * @return ProposalLogActionNamed
     */
    public function setDisplayForActive($displayForActive)
    {
        $this->displayForActive = $displayForActive;

        return $this;
    }

    /**
     * Get displayForActive
     *
     * @return boolean
     */
    public function getDisplayForActive()
    {
        return $this->displayForActive;
    }
}
