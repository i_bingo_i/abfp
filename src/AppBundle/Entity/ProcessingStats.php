<?php

namespace AppBundle\Entity;

/**
 * ProcessingStats
 */
class ProcessingStats
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateTime;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \AppBundle\Entity\ProcessingStatsType
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateTime
     *
     * @param \DateTime $dateTime
     *
     * @return ProcessingStats
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * Get dateTime
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return ProcessingStats
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\ProcessingStatsType $type
     *
     * @return ProcessingStats
     */
    public function setType(\AppBundle\Entity\ProcessingStatsType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\ProcessingStatsType
     */
    public function getType()
    {
        return $this->type;
    }
}
