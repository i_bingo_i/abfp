<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\PriceFilterTrait;

/**
 * ContractorService
 */
class ContractorService
{
    use PriceFilterTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $fixedPrice;

    /**
     * @var float
     */
    private $hoursPrice;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $code;

    /**
     * @var \AppBundle\Entity\State
     */
    private $state;

    /**
     * @var \AppBundle\Entity\ServiceNamed
     */
    private $named;

    /**
     * @var \AppBundle\Entity\Contractor
     */
    private $contractor;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $deviceCategory;

    /**
     * @var float
     */
    private $estimationTime;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fixedPrice
     *
     * @param float $fixedPrice
     *
     * @return ContractorService
     */
    public function setFixedPrice($fixedPrice)
    {
        $this->fixedPrice = $this->filterPrice($fixedPrice);

        return $this;
    }

    /**
     * Get fixedPrice
     *
     * @return float
     */
    public function getFixedPrice()
    {
        return $this->fixedPrice;
    }

    /**
     * Set hoursPrice
     *
     * @param float $hoursPrice
     *
     * @return ContractorService
     */
    public function setHoursPrice($hoursPrice)
    {
        $this->hoursPrice = $hoursPrice;

        return $this;
    }

    /**
     * Get hoursPrice
     *
     * @return float
     */
    public function getHoursPrice()
    {
        return $this->hoursPrice;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return ContractorService
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ContractorService
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\ServiceNamed $named
     *
     * @return ContractorService
     */
    public function setNamed(\AppBundle\Entity\ServiceNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\ServiceNamed
     */
    public function getNamed()
    {
        return $this->named;
    }

    /**
     * Set contractor
     *
     * @param \AppBundle\Entity\Contractor $contractor
     *
     * @return ContractorService
     */
    public function setContractor(\AppBundle\Entity\Contractor $contractor = null)
    {
        $this->contractor = $contractor;

        return $this;
    }

    /**
     * Get contractor
     *
     * @return \AppBundle\Entity\Contractor
     */
    public function getContractor()
    {
        return $this->contractor;
    }

    /**
     * Set deviceCategory
     *
     * @param \AppBundle\Entity\DeviceCategory $deviceCategory
     *
     * @return ContractorService
     */
    public function setDeviceCategory(\AppBundle\Entity\DeviceCategory $deviceCategory = null)
    {
        $this->deviceCategory = $deviceCategory;

        return $this;
    }

    /**
     * Get deviceCategory
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getDeviceCategory()
    {
        return $this->deviceCategory;
    }

    /**
     * Set state
     *
     * @param \AppBundle\Entity\State $state
     *
     * @return ContractorService
     */
    public function setState(\AppBundle\Entity\State $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \AppBundle\Entity\State
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set estimationTime
     *
     * @param float $estimationTime
     *
     * @return ContractorService
     */
    public function setEstimationTime($estimationTime)
    {
        $this->estimationTime = $estimationTime;

        return $this;
    }

    /**
     * Get estimationTime
     *
     * @return float
     */
    public function getEstimationTime()
    {
        return $this->estimationTime;
    }
}
