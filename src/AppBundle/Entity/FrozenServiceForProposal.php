<?php

namespace AppBundle\Entity;

/**
 * FrozenServiceForProposal
 */
class FrozenServiceForProposal
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \AppBundle\Entity\ServiceHistory
     */
    private $serviceHistory;

    /**
     * @var \AppBundle\Entity\FrozenProposal
     */
    private $frozenProposal;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceHistory
     *
     * @param \AppBundle\Entity\ServiceHistory $serviceHistory
     *
     * @return FrozenServiceForProposal
     */
    public function setServiceHistory(\AppBundle\Entity\ServiceHistory $serviceHistory = null)
    {
        $this->serviceHistory = $serviceHistory;

        return $this;
    }

    /**
     * Get serviceHistory
     *
     * @return \AppBundle\Entity\ServiceHistory
     */
    public function getServiceHistory()
    {
        return $this->serviceHistory;
    }

    /**
     * Set frozenProposal
     *
     * @param \AppBundle\Entity\FrozenProposal $frozenProposal
     *
     * @return FrozenServiceForProposal
     */
    public function setFrozenProposal(\AppBundle\Entity\FrozenProposal $frozenProposal = null)
    {
        $this->frozenProposal = $frozenProposal;

        return $this;
    }

    /**
     * Get frozenProposal
     *
     * @return \AppBundle\Entity\FrozenProposal
     */
    public function getFrozenProposal()
    {
        return $this->frozenProposal;
    }
}
