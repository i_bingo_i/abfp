<?php

namespace AppBundle\Entity;

/**
 * ServiceHistory
 */
class ServiceHistory
{
 
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $fixedPrice;

    /**
     * @var float
     */
    private $hourlyPrice;

    /**
     * @var \DateTime
     */
    private $lastTested;

    /**
     * @var \DateTime
     */
    private $inspectionDue;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $testDate;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \AppBundle\Entity\ServiceNamed
     */
    private $named;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $device;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var \AppBundle\Entity\Contractor
     */
    private $companyLastTested;

    /**
     * @var \AppBundle\Entity\Opportunity
     */
    private $opportunity;

    /**
     * @var \AppBundle\Entity\DepartmentChannel
     */
    private $departmentChannel;

    /**
     * @var \AppBundle\Entity\ProposalHistory
     */
    private $proposal;

    /**
     * @var \AppBundle\Entity\ServiceStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\Report
     */
    private $lastReport;

    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;

    /**
     * @var \AppBundle\Entity\Service
     */
    private $ownerEntity;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * @var \AppBundle\Entity\ContractorService
     */
    private $contractorService;

    /**
     * @var boolean
     */
    private $directlyWO = false;

    /**
     * @var float
     */
    private $municipalityFee;

    /**
     * @var boolean
     */
    private $departmentChannelWithFeesWillVary = false;

    /**
     * @var boolean
     */
    private $addedFromIos = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fixedPrice
     *
     * @param float $fixedPrice
     *
     * @return ServiceHistory
     */
    public function setFixedPrice($fixedPrice)
    {
        $this->fixedPrice = $fixedPrice;

        return $this;
    }

    /**
     * Get fixedPrice
     *
     * @return float
     */
    public function getFixedPrice()
    {
        return $this->fixedPrice;
    }

    /**
     * Set hourlyPrice
     *
     * @param float $hourlyPrice
     *
     * @return ServiceHistory
     */
    public function setHourlyPrice($hourlyPrice)
    {
        $this->hourlyPrice = $hourlyPrice;

        return $this;
    }

    /**
     * Get hourlyPrice
     *
     * @return float
     */
    public function getHourlyPrice()
    {
        return $this->hourlyPrice;
    }

    /**
     * Set lastTested
     *
     * @param \DateTime $lastTested
     *
     * @return ServiceHistory
     */
    public function setLastTested($lastTested)
    {
        $this->lastTested = $lastTested;

        return $this;
    }

    /**
     * Get lastTested
     *
     * @return \DateTime
     */
    public function getLastTested()
    {
        return $this->lastTested;
    }

    /**
     * Set inspectionDue
     *
     * @param \DateTime $inspectionDue
     *
     * @return ServiceHistory
     */
    public function setInspectionDue($inspectionDue)
    {
        $this->inspectionDue = $inspectionDue;

        return $this;
    }

    /**
     * Get inspectionDue
     *
     * @return \DateTime
     */
    public function getInspectionDue()
    {
        return $this->inspectionDue;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ServiceHistory
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ServiceHistory
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set testDate
     *
     * @param \DateTime $testDate
     *
     * @return ServiceHistory
     */
    public function setTestDate($testDate)
    {
        $this->testDate = $testDate;

        return $this;
    }

    /**
     * Get testDate
     *
     * @return \DateTime
     */
    public function getTestDate()
    {
        return $this->testDate;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ServiceHistory
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\ServiceNamed $named
     *
     * @return ServiceHistory
     */
    public function setNamed(\AppBundle\Entity\ServiceNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\ServiceNamed
     */
    public function getNamed()
    {
        return $this->named;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return ServiceHistory
     */
    public function setDevice(\AppBundle\Entity\Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return ServiceHistory
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set companyLastTested
     *
     * @param \AppBundle\Entity\Contractor $companyLastTested
     *
     * @return ServiceHistory
     */
    public function setCompanyLastTested(\AppBundle\Entity\Contractor $companyLastTested = null)
    {
        $this->companyLastTested = $companyLastTested;

        return $this;
    }

    /**
     * Get companyLastTested
     *
     * @return \AppBundle\Entity\Contractor
     */
    public function getCompanyLastTested()
    {
        return $this->companyLastTested;
    }

    /**
     * Set opportunity
     *
     * @param \AppBundle\Entity\Opportunity $opportunity
     *
     * @return ServiceHistory
     */
    public function setOpportunity(\AppBundle\Entity\Opportunity $opportunity = null)
    {
        $this->opportunity = $opportunity;

        return $this;
    }

    /**
     * Get opportunity
     *
     * @return \AppBundle\Entity\Opportunity
     */
    public function getOpportunity()
    {
        return $this->opportunity;
    }

    /**
     * Set departmentChannel
     *
     * @param \AppBundle\Entity\DepartmentChannel $departmentChannel
     *
     * @return ServiceHistory
     */
    public function setDepartmentChannel(\AppBundle\Entity\DepartmentChannel $departmentChannel = null)
    {
        $this->departmentChannel = $departmentChannel;

        return $this;
    }

    /**
     * Get departmentChannel
     *
     * @return \AppBundle\Entity\DepartmentChannel
     */
    public function getDepartmentChannel()
    {
        return $this->departmentChannel;
    }


    /**
     * Set status
     *
     * @param \AppBundle\Entity\ServiceStatus $status
     *
     * @return ServiceHistory
     */
    public function setStatus(\AppBundle\Entity\ServiceStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Set proposal
     *
     * @param \AppBundle\Entity\ProposalHistory $proposal
     *
     * @return ServiceHistory
     */
    public function setProposal(\AppBundle\Entity\ProposalHistory $proposal = null)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal
     *
     * @return \AppBundle\Entity\ProposalHistory
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\ServiceStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set lastReport
     *
     * @param \AppBundle\Entity\Report $lastReport
     *
     * @return ServiceHistory
     */
    public function setLastReport(\AppBundle\Entity\Report $lastReport = null)
    {
        $this->lastReport = $lastReport;

        return $this;
    }

    /**
     * Get lastReport
     *
     * @return \AppBundle\Entity\Report
     */
    public function getLastReport()
    {
        return $this->lastReport;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return ServiceHistory
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * Set ownerEntity
     *
     * @param \AppBundle\Entity\Service $ownerEntity
     *
     * @return ServiceHistory
     */
    public function setOwnerEntity(\AppBundle\Entity\Service $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \AppBundle\Entity\Service
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return ServiceHistory
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set contractorService
     *
     * @param \AppBundle\Entity\ContractorService $contractorService
     *
     * @return ServiceHistory
     */
    public function setContractorService(\AppBundle\Entity\ContractorService $contractorService = null)
    {
        $this->contractorService = $contractorService;

        return $this;
    }

    /**
     * Get contractorService
     *
     * @return \AppBundle\Entity\ContractorService
     */
    public function getContractorService()
    {
        return $this->contractorService;
    }
    /**
     * @var \DateTime
     */
    private $dateUpdate;


    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ServiceHistory
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set directlyWO
     *
     * @param boolean $directlyWO
     *
     * @return ServiceHistory
     */
    public function setDirectlyWO($directlyWO)
    {
        $this->directlyWO = $directlyWO;

        return $this;
    }

    /**
     * Get directlyWO
     *
     * @return boolean
     */
    public function getDirectlyWO()
    {
        return $this->directlyWO;
    }

    /**
     * Set municipalityFee
     *
     * @param float $municipalityFee
     *
     * @return ServiceHistory
     */
    public function setMunicipalityFee($municipalityFee)
    {
        $this->municipalityFee = $municipalityFee;

        return $this;
    }

    /**
     * Get municipalityFee
     *
     * @return float
     */
    public function getMunicipalityFee()
    {
        return $this->municipalityFee;
    }

    /**
     * Set departmentChannelWithFeesWillVary
     *
     * @param boolean $departmentChannelWithFeesWillVary
     *
     * @return ServiceHistory
     */
    public function setDepartmentChannelWithFeesWillVary($departmentChannelWithFeesWillVary)
    {
        $this->departmentChannelWithFeesWillVary = $departmentChannelWithFeesWillVary;

        return $this;
    }

    /**
     * Get departmentChannelWithFeesWillVary
     *
     * @return boolean
     */
    public function getDepartmentChannelWithFeesWillVary()
    {
        return $this->departmentChannelWithFeesWillVary;
    }

    /**
     * Set addedFromIos
     *
     * @param boolean $addedFromIos
     *
     * @return ServiceHistory
     */
    public function setAddedFromIos($addedFromIos)
    {
        $this->addedFromIos = $addedFromIos;

        return $this;
    }

    /**
     * Get addedFromIos
     *
     * @return boolean
     */
    public function getAddedFromIos()
    {
        return $this->addedFromIos;
    }
}
