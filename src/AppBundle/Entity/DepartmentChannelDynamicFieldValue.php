<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * DepartmentChannelDynamicFieldValue
 */
class DepartmentChannelDynamicFieldValue
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\ChannelDynamicField
     */
    private $field;

    /**
     * @var \AppBundle\Entity\DepartmentChannel
     */
    private $departmentChanel;

    /**
     * @var \AppBundle\Entity\ChannelDropboxChoices
     */
    private $optionValue;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $entityValue;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return DepartmentChannelDynamicFieldValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return DepartmentChannelDynamicFieldValue
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return DepartmentChannelDynamicFieldValue
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set field
     *
     * @param \AppBundle\Entity\ChannelDynamicField $field
     *
     * @return DepartmentChannelDynamicFieldValue
     */
    public function setField(\AppBundle\Entity\ChannelDynamicField $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \AppBundle\Entity\ChannelDynamicField
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set departmentChanel
     *
     * @param \AppBundle\Entity\DepartmentChannel $departmentChanel
     *
     * @return DepartmentChannelDynamicFieldValue
     */
    public function setDepartmentChanel(\AppBundle\Entity\DepartmentChannel $departmentChanel = null)
    {
        $this->departmentChanel = $departmentChanel;

        return $this;
    }

    /**
     * Get departmentChanel
     *
     * @return \AppBundle\Entity\DepartmentChannel
     */
    public function getDepartmentChanel()
    {
        return $this->departmentChanel;
    }

    /**
     * Set optionValue
     *
     * @param \AppBundle\Entity\ChannelDropboxChoices $optionValue
     *
     * @return DepartmentChannelDynamicFieldValue
     */
    public function setOptionValue(\AppBundle\Entity\ChannelDropboxChoices $optionValue = null)
    {
        $this->optionValue = $optionValue;

        return $this;
    }

    /**
     * Get optionValue
     *
     * @return \AppBundle\Entity\ChannelDropboxChoices
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }

    /**
     * Set entityValue
     *
     * @param \AppBundle\Entity\Address $entityValue
     *
     * @return DepartmentChannelDynamicFieldValue
     */
    public function setEntityValue(\AppBundle\Entity\Address $entityValue = null)
    {
        $this->entityValue = $entityValue;

        return $this;
    }

    /**
     * Get entityValue
     *
     * @return \AppBundle\Entity\Address
     */
    public function getEntityValue()
    {
        return $this->entityValue;
    }
}
