<?php

namespace AppBundle\Entity\ElasticaRepository;

use AppBundle\Entity\ElasticaRepository\ElasticaRepositoryTrait\FormatPaginationResultTrait;
use Elastica\Query\Match;
use Elastica\Query\Terms;
use FOS\ElasticaBundle\Repository;
use Elastica\Query\BoolQuery;
use Elastica\Query;
use Elastica\Query\QueryString;
use Elastica\Util;

class MunicipalityRepository extends Repository
{
    use FormatPaginationResultTrait;

    /**
     * @param $searchOption
     * @param int $maxItems
     * @param null $maxResultsCount
     * @return array
     */
    public function findMunicipality($searchOption, $maxItems = 10, $maxResultsCount = null)
    {
        /** @var BoolQuery $boolQuery */
        $boolQuery = new BoolQuery();

        $searchPhraseArray = [];
        if (isset($searchOption['searchPhrase'])) {
            $searchPhraseArray = explode(' ', $searchOption['searchPhrase']);
        }

        $wordQuery = new QueryString();
        $searchFields = [
            'id',
            'name',
            'website'
        ];

        foreach ($searchPhraseArray as $word) {
            $wordQuery->setFields($searchFields);
            $wordQuery->setQuery("*" . Util::escapeTerm($word) . "*");
            $boolQuery->addMust($wordQuery);
        }

        /** @var Query $query */
        $query = new Query($boolQuery);
        if ($maxResultsCount) {
            $query->setSize($maxResultsCount);
        }

        $query->addSort([
            'type.sort' => ['order' => 'asc']
        ]);

        $results = $this->findPaginated($query);

        return $this->formatPaginationResult($results, $searchOption['page'] ?? 1, $maxItems);
    }

    /**
     * @param $searchOption
     * @param $numberOfRecords
     * @return array
     */
    public function findWithCustomQuery($searchOption, $numberOfRecords = 30)
    {
        /** @var BoolQuery $boolQuery */
        $boolQuery = new BoolQuery();
        $searchPhraseArray = explode(' ', $searchOption);

        foreach ($searchPhraseArray as $word) {
            $wordQuery = new QueryString();
            $searchFields = ['name'];

            $wordQuery->setFields($searchFields);
            $wordQuery->setQuery("*" . Util::escapeTerm($word) . "*");
            $boolQuery->addMust($wordQuery);
        }

        /** @var Query $query */
        $query = new Query($boolQuery);

        $query->addSort([
            'deleted' => ['order' => 'asc']
        ]);

        return $this->find($query, $numberOfRecords);
    }
}