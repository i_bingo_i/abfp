<?php

namespace AppBundle\Entity\ElasticaRepository;

use AppBundle\Entity\ElasticaRepository\ElasticaRepositoryTrait\FormatPaginationResultTrait;
use Elastica\Query\Match;
use Elastica\Query\Terms;
use FOS\ElasticaBundle\Repository;
use Elastica\Query\BoolQuery;
use Elastica\Query;
use Elastica\Query\QueryString;
use Elastica\Util;

class ContractorRepository extends Repository
{
    use FormatPaginationResultTrait;

     /**
     * @param $searchOption
     * @return array
     */
    public function findWithCustomQuery($searchOption)
    {
        /** @var BoolQuery $boolQuery */
        $boolQuery = new BoolQuery();
        $searchPhraseArray = explode(' ', $searchOption);

        foreach ($searchPhraseArray as $word) {
            $wordQuery = new QueryString();
            $searchFields = ['name'];

            $wordQuery->setFields($searchFields);
            $wordQuery->setQuery("*" . Util::escapeTerm($word) . "*");
            $boolQuery->addMust($wordQuery);
        }

        /** @var Query $query */
        $query = new Query($boolQuery);

        $query->addSort([
            'deleted' => ['order' => 'asc']
        ]);

        return $this->find($query);
    }

    /**
     * @param $searchOption
     * @param int $maxItems
     * @param int|null $maxResultsCount
     * @return array
     */
    public function findContractor($searchOption, $maxItems = 10, $maxResultsCount = null)
    {
        /** @var BoolQuery $boolQuery */
        $boolQuery = new BoolQuery();

        $searchPhraseArray = [];
        if (isset($searchOption['searchPhrase'])) {
            $searchPhraseArray = explode(' ', $searchOption['searchPhrase']);
        }

        foreach ($searchPhraseArray as $word) {
            $wordQuery = new QueryString();
            $searchFields = [
                'id',
                'name',
                'website',
                'type.name',
                'address.address',
                'address.city',
                'address.zip',
                'address.state.name',
                'address.state.code'
            ];

            $wordQuery->setFields($searchFields);
            $wordQuery->setQuery("*" . Util::escapeTerm($word) . "*");
            $boolQuery->addMust($wordQuery);
        }

        if (isset($searchOption['type'])) {
            $fieldQuery = new Terms();
            $fieldQuery->setTerms('type.id', $searchOption['type']);
            $boolQuery->addMust($fieldQuery);
        }

        /** @var Query $query */
        $query = new Query($boolQuery);
        if ($maxResultsCount) {
            $query->setSize($maxResultsCount);
        }

        $query->addSort([
            'type.sort' => ['order' => 'asc']
        ]);

        $results = $this->findPaginated($query);

        return $this->formatPaginationResult($results, $searchOption['page'] ?? 1, $maxItems);
    }
}