<?php

namespace AppBundle\Entity\ElasticaRepository;

use FOS\ElasticaBundle\Repository;
use Elastica\Query\BoolQuery;
use Elastica\Query;
use Elastica\Query\QueryString;
use Elastica\Util;
use AppBundle\Entity\ElasticaRepository\ElasticaRepositoryTrait\FormatPaginationResultTrait;

class CompanyRepository extends Repository implements ISearchRepository
{
    use FormatPaginationResultTrait;

    /**
     * @param $searchOption
     * @param int $maxItems
     * @param null $maxResultsCount
     * @return array
     */
    public function findWithCustomQuery($searchOption, $maxItems = 10, $maxResultsCount = null)
    {
        /** @var BoolQuery $boolQuery */
        $boolQuery = new BoolQuery();
        $searchPhraseArray = explode(' ', $searchOption['searchPhrase']);

        foreach ($searchPhraseArray as $word) {
            $wordQuery = new QueryString();
            $searchFields = [
                'id',
                'name',
                'address.address',
                'address.zip',
                'address.city',
                'address.state.name',
                'address.state.code',
                'website'
            ];

            if (isset($searchOption['cn_s_in_notes']) and $searchOption['cn_s_in_notes'] == 1) {
                $searchFields[] = 'notes';
            }

            $wordQuery->setFields($searchFields);
            $wordQuery->setQuery("*" . Util::escapeTerm($word) . "*");
            $boolQuery->addMust($wordQuery);
        }

        /** @var Query $query */
        $query = new Query($boolQuery);
        if ($maxResultsCount) {
            $query->setSize($maxResultsCount);
        }

        $query->addSort([
            'deleted' => ['order' => 'asc']
        ]);

        $results = $this->findPaginated($query);

        return $this->formatPaginationResult($results, $searchOption['c_page'], $maxItems);
    }
}
