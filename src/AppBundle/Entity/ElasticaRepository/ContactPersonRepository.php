<?php

namespace AppBundle\Entity\ElasticaRepository;

use FOS\ElasticaBundle\Repository;
use Elastica\Query\BoolQuery;
use Elastica\Query;
use Elastica\Query\QueryString;
use Elastica\Util;
use Elastica\Query\Match;
use AppBundle\Entity\ElasticaRepository\ElasticaRepositoryTrait\FormatPaginationResultTrait;

class ContactPersonRepository extends Repository implements ISearchRepository
{
    use FormatPaginationResultTrait;

    /**
     * @param $searchOption
     * @param int $maxItems
     * @param null $maxResultsCount
     * @return array
     */
    public function findWithCustomQuery($searchOption, $maxItems = 10, $maxResultsCount = null)
    {
        /** @var BoolQuery $boolQuery */
        $boolQuery = new BoolQuery();
        $searchPhraseArray = explode(' ', $searchOption['searchPhrase']);

        foreach ($searchPhraseArray as $word) {
            $wordQuery = new QueryString();
            $searchFields = ['id', 'firstName', 'lastName', 'title', 'email', 'phone', 'cell', 'fax'];

            if (isset($searchOption['c_s_in_notes']) and $searchOption['c_s_in_notes'] == 1) {
                $searchFields[] = 'notes';
            }

            $wordQuery->setFields($searchFields);
            $wordQuery->setQuery("*" . Util::escapeTerm($word) . "*");
            $boolQuery->addMust($wordQuery);
        }

        if (isset($searchOption['c_s_cod'])) {
            $fieldQuery = new Match();
            $fieldQuery->setFieldQuery('cod', $searchOption['c_s_cod']);
            $boolQuery->addMust($fieldQuery);
        }

        /** @var Query $query */
        $query = new Query($boolQuery);
        if ($maxResultsCount) {
            $query->setSize($maxResultsCount);
        }

        $query->addSort([
            'deleted' => ['order' => 'asc']
        ]);

        $results = $this->findPaginated($query);

        return $this->formatPaginationResult($results, $searchOption['cn_page'], $maxItems);
    }
}
