<?php

namespace AppBundle\Entity\ElasticaRepository;

use AppBundle\Entity\AccountType;
use FOS\ElasticaBundle\Repository;
use Elastica\Query;
use AppBundle\Entity\ElasticaRepository\ElasticaRepositoryTrait\FormatPaginationResultTrait;
use AppBundle\Entity\ElasticaRepository\Decorators\AccountDefaultBoolQuery;
use AppBundle\Entity\ElasticaRepository\Decorators\AccountSearchPhraseBoolQuery;
use AppBundle\Entity\ElasticaRepository\Decorators\AccountClientTypeBoolQuery;
use AppBundle\Entity\ElasticaRepository\Decorators\AccountTypeBoolQuery;
use AppBundle\Entity\ElasticaRepository\Decorators\IAccountSearchBoolQueryDecorator;

class AccountRepository extends Repository implements ISearchRepository
{
    use FormatPaginationResultTrait;

    /**
     * @param $searchOption
     * @param int $maxItems
     * @param null $maxResultsCount
     * @return array
     */
    public function findWithCustomQuery($searchOption, $maxItems = 10, $maxResultsCount = null)
    {
        /** @var IAccountSearchBoolQueryDecorator $accountSearchBoolQuery */
        $accountSearchBoolQuery = $this->getSearchPhraseAndClientTypeBoolQuery($searchOption);

        return $this->getResult($accountSearchBoolQuery, $searchOption['a_page'], $maxItems, $maxResultsCount);
    }

    /**
     * @param $searchOption
     * @param AccountType $accountType
     * @param int $maxItems
     * @param null $maxResultsCount
     * @return array
     */
    public function findAccountByType($searchOption, AccountType $accountType, $maxItems = 10, $maxResultsCount = null)
    {
        /** @var IAccountSearchBoolQueryDecorator $accountSearchBoolQuery */
        $accountSearchBoolQuery = $this->getSearchPhraseAndClientTypeBoolQuery($searchOption);
        $accountSearchBoolQuery = new AccountTypeBoolQuery($accountSearchBoolQuery, $accountType);

        return $this->getResult($accountSearchBoolQuery, $searchOption['a_page'], $maxItems, $maxResultsCount);
    }

    /**
     * @param $searchOption
     * @return AccountClientTypeBoolQuery|AccountDefaultBoolQuery|AccountSearchPhraseBoolQuery
     */
    private function getSearchPhraseAndClientTypeBoolQuery($searchOption)
    {
        /** @var IAccountSearchBoolQueryDecorator */
        $accountSearchBoolQuery = new AccountDefaultBoolQuery();
        /** @var IAccountSearchBoolQueryDecorator */
        $accountSearchBoolQuery = new AccountSearchPhraseBoolQuery($accountSearchBoolQuery, $searchOption);
        /** @var IAccountSearchBoolQueryDecorator */
        $accountSearchBoolQuery = new AccountClientTypeBoolQuery($accountSearchBoolQuery, $searchOption);

        return $accountSearchBoolQuery;
    }

    /**
     * @param IAccountSearchBoolQueryDecorator $accountSearchBoolQuery
     * @param $page
     * @param $maxItems
     * @param $maxResultsCount
     * @return array
     */
    private function getResult(
        IAccountSearchBoolQueryDecorator $accountSearchBoolQuery,
        $page,
        $maxItems,
        $maxResultsCount
    ) {
        /** @var Query $query */
        $query = new Query($accountSearchBoolQuery->getBoolQuery());
        if ($maxResultsCount) {
            $query->setSize($maxResultsCount);
        }

        $query->addSort([
            'deleted' => ['order' => 'asc']
        ]);

        $results = $this->findPaginated($query);

        return $this->formatPaginationResult($results, $page, $maxItems);
    }
}
