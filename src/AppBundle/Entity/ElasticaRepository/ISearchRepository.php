<?php

namespace AppBundle\Entity\ElasticaRepository;

interface ISearchRepository
{
    public function findWithCustomQuery($searchOption, $maxItems, $maxResultsCount);
}
