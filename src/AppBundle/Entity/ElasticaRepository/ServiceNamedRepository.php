<?php

namespace AppBundle\Entity\ElasticaRepository;

use Elastica\Aggregation\Filter;
use Elastica\Query\Exists;
use Elastica\Query\Match;
use FOS\ElasticaBundle\Repository;
use Elastica\Query\BoolQuery;
use Elastica\Query;
use Elastica\Query\QueryString;
use Elastica\Util;
use AppBundle\Entity\ElasticaRepository\ElasticaRepositoryTrait\FormatPaginationResultTrait;

class ServiceNamedRepository extends Repository implements ISearchRepository
{
    use FormatPaginationResultTrait;

    /**
     * @param $searchOption
     * @param int $maxItems
     * @param null $maxResultsCount
     * @return array
     */
    public function findWithCustomQuery($searchOption, $maxItems = 10, $maxResultsCount = null)
    {
        /** @var BoolQuery $boolQuery */
        $boolQuery = new BoolQuery();
        $searchPhraseArray = explode(' ', $searchOption['phrase']);
        $searchFields = ['name'];

        foreach ($searchPhraseArray as $word) {
            $wordQuery = new QueryString();

            $wordQuery->setFields($searchFields);
            $wordQuery->setQuery("*" . Util::escapeTerm($word) . "*");
            $boolQuery->addMust($wordQuery);
        }

        $existQuery = new Exists('frequency');
        $boolQuery->addMustNot($existQuery);

        $deviceName = new Match();
        $deviceName->setFieldQuery('deviceNamed.alias', $searchOption['deviceAlias']);
        $boolQuery->addShould($deviceName);

        /** @var Query $query */
        $query = new Query($boolQuery);

        return $this->find($query);
    }
}