<?php

namespace AppBundle\Entity\ElasticaRepository\ElasticaRepositoryTrait;

use \Pagerfanta\Pagerfanta;
use Pagerfanta\Exception\NotValidCurrentPageException;

trait FormatPaginationResultTrait
{
    /**
     * @param Pagerfanta $paginationResult
     * @param $page
     * @param $maxItems
     * @return array
     */
    protected function formatPaginationResult($paginationResult, $page, $maxItems)
    {
        $paginationResult->setMaxPerPage($maxItems);
        try {
            $paginationResult->setCurrentPage($page);
        } catch (NotValidCurrentPageException $e) {
            $paginationResult->setCurrentPage($paginationResult->getNbPages());
        }

        return [
            'currentPage' => $paginationResult->getCurrentPage(),
            'pagesCount' => $paginationResult->getNbPages(),
            'count' => $paginationResult->count(),
            'results' => $paginationResult->getCurrentPageResults()
        ];
    }
}
