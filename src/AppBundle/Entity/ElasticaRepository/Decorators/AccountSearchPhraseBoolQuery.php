<?php

namespace AppBundle\Entity\ElasticaRepository\Decorators;

use Elastica\Query\BoolQuery;
use Elastica\Query\QueryString;
use Elastica\Util;

class AccountSearchPhraseBoolQuery implements IAccountSearchBoolQueryDecorator
{
    /** @var BoolQuery $boolQuery */
    protected $boolQuery;
    protected $searchOption;

    /**
     * AccountSearchPhraseBoolQuery constructor.
     * @param IAccountSearchBoolQueryDecorator $boolQueryDecorator
     * @param $searchOption
     */
    public function __construct(IAccountSearchBoolQueryDecorator $boolQueryDecorator, $searchOption)
    {
        $this->boolQuery = $boolQueryDecorator->getBoolQuery();
        $this->searchOption = $searchOption;
    }

    /**
     * @return BoolQuery
     */
    public function getBoolQuery()
    {
        $searchPhraseArray = explode(' ', $this->searchOption['searchPhrase']);

        foreach ($searchPhraseArray as $word) {
            $wordQuery = new QueryString();
            $searchFields = [
                'id',
                'name',
                'address.address',
                'address.zip',
                'address.city',
                'address.state.name',
                'address.state.code'
            ];

            if (isset($this->searchOption['a_s_in_notes']) and $this->searchOption['a_s_in_notes'] == 1) {
                $searchFields[] = 'notes';
            }

            $wordQuery->setFields($searchFields);
            $wordQuery->setQuery("*" . Util::escapeTerm($word) . "*");
            $this->boolQuery->addMust($wordQuery);
        }

        return $this->boolQuery;
    }
}
