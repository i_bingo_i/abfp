<?php

namespace AppBundle\Entity\ElasticaRepository\Decorators;

use AppBundle\Entity\AccountType;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;

class AccountTypeBoolQuery implements IAccountSearchBoolQueryDecorator
{
    /** @var BoolQuery $boolQuery */
    protected $boolQuery;
    /** @var AccountType $accountType */
    protected $accountType;

    /**
     * AccountTypeBoolQuery constructor.
     * @param IAccountSearchBoolQueryDecorator $boolQueryDecorator
     * @param $accountType
     */
    public function __construct(IAccountSearchBoolQueryDecorator $boolQueryDecorator, AccountType $accountType)
    {
        $this->boolQuery = $boolQueryDecorator->getBoolQuery();
        $this->accountType = $accountType;
    }

    /**
     * @return BoolQuery
     */
    public function getBoolQuery()
    {
        /** @var Match $fieldTypeQuery */
        $fieldTypeQuery = new Match();
        $fieldTypeQuery->setFieldQuery('type.id', $this->accountType->getId());
        $this->boolQuery->addMust($fieldTypeQuery);

        return $this->boolQuery;
    }
}
