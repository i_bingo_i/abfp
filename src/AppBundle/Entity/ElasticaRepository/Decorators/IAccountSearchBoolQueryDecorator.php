<?php

namespace AppBundle\Entity\ElasticaRepository\Decorators;

interface IAccountSearchBoolQueryDecorator
{
    public function getBoolQuery();
}
