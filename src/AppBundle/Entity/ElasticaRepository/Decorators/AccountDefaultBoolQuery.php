<?php

namespace AppBundle\Entity\ElasticaRepository\Decorators;

use Elastica\Query\BoolQuery;

class AccountDefaultBoolQuery implements IAccountSearchBoolQueryDecorator
{
    public function getBoolQuery()
    {
        /** @var BoolQuery $boolQuery */
        return new BoolQuery();
    }
}
