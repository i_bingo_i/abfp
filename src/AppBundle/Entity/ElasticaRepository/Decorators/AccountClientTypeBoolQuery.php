<?php

namespace AppBundle\Entity\ElasticaRepository\Decorators;

use Elastica\Query\BoolQuery;
use Elastica\Query\Match;

class AccountClientTypeBoolQuery implements IAccountSearchBoolQueryDecorator
{
    /** @var BoolQuery $boolQuery */
    protected $boolQuery;
    protected $searchOption;

    /**
     * AccountClientTypeBoolQuery constructor.
     * @param IAccountSearchBoolQueryDecorator $boolQueryDecorator
     * @param $searchOption
     */
    public function __construct(IAccountSearchBoolQueryDecorator $boolQueryDecorator, $searchOption)
    {
        $this->boolQuery = $boolQueryDecorator->getBoolQuery();
        $this->searchOption = $searchOption;
    }

    /**
     * @return BoolQuery
     */
    public function getBoolQuery()
    {
        if (isset($this->searchOption['clientType'])) {
            /** @var Match $fieldClientTypeQuery */
            $fieldClientTypeQuery = new Match();
            $fieldClientTypeQuery->setFieldQuery('clientType.alias', implode(' ', $this->searchOption['clientType']));
            $this->boolQuery->addMust($fieldClientTypeQuery);
        }

        return $this->boolQuery;
    }
}
