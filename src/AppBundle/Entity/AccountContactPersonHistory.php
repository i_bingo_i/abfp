<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use InvoiceBundle\Entity\Customer;

/**
 * AccountContactPersonHistory
 */
class AccountContactPersonHistory
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\AccountContactPerson
     */
    private $ownerEntity;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * @var boolean
     */
    private $authorizer = false;

    /**
     * @var boolean
     */
    private $access = false;

    /**
     * @var boolean
     */
    private $accessPrimary = false;

    /**
     * @var boolean
     */
    private $payment = false;

    /**
     * @var boolean
     */
    private $paymentPrimary = false;

    /**
     * @var \AppBundle\Entity\ContactPersonHistory
     */
    private $contactPerson;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var string
     */
    private $sendingAddressDescription;

    /**
     * @var \AppBundle\Entity\AddressHistory
     */
    private $sendingAddress;

    /**
     * @var string
     */
    private $notes;

    /** @var Address */
    private $customBillingAddress;

    /** @var Customer */
    private $customer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return AccountContactPersonHistory
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return AccountContactPersonHistory
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return AccountContactPersonHistory
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set ownerEntity
     *
     * @param \AppBundle\Entity\AccountContactPerson $ownerEntity
     *
     * @return AccountContactPersonHistory
     */
    public function setOwnerEntity(\AppBundle\Entity\AccountContactPerson $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \AppBundle\Entity\AccountContactPerson
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return AccountContactPersonHistory
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set contactPerson
     *
     * @param \AppBundle\Entity\ContactPersonHistory $contactPerson
     *
     * @return AccountContactPersonHistory
     */
    public function setContactPerson(\AppBundle\Entity\ContactPersonHistory $contactPerson = null)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return \AppBundle\Entity\ContactPersonHistory
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return AccountContactPersonHistory
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set authorizer
     *
     * @param boolean $authorizer
     *
     * @return AccountContactPersonHistory
     */
    public function setAuthorizer($authorizer)
    {
        $this->authorizer = $authorizer;

        return $this;
    }

    /**
     * Get authorizer
     *
     * @return boolean
     */
    public function getAuthorizer()
    {
        return $this->authorizer;
    }

    /**
     * Set access
     *
     * @param boolean $access
     *
     * @return AccountContactPersonHistory
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return boolean
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set accessPrimary
     *
     * @param boolean $accessPrimary
     *
     * @return AccountContactPersonHistory
     */
    public function setAccessPrimary($accessPrimary)
    {
        $this->accessPrimary = $accessPrimary;

        return $this;
    }

    /**
     * Get accessPrimary
     *
     * @return boolean
     */
    public function getAccessPrimary()
    {
        return $this->accessPrimary;
    }

    /**
     * Set payment
     *
     * @param boolean $payment
     *
     * @return AccountContactPersonHistory
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return boolean
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set paymentPrimary
     *
     * @param boolean $paymentPrimary
     *
     * @return AccountContactPersonHistory
     */
    public function setPaymentPrimary($paymentPrimary)
    {
        $this->paymentPrimary = $paymentPrimary;

        return $this;
    }

    /**
     * Get paymentPrimary
     *
     * @return boolean
     */
    public function getPaymentPrimary()
    {
        return $this->paymentPrimary;
    }

    /**
     * Set sendingAddressDescription
     *
     * @param string $sendingAddressDescription
     *
     * @return AccountContactPersonHistory
     */
    public function setSendingAddressDescription($sendingAddressDescription)
    {
        $this->sendingAddressDescription = $sendingAddressDescription;

        return $this;
    }

    /**
     * Get sendingAddressDescription
     *
     * @return string
     */
    public function getSendingAddressDescription()
    {
        return $this->sendingAddressDescription;
    }

    /**
     * Set sendingAddress
     *
     * @param \AppBundle\Entity\AddressHistory $sendingAddress
     *
     * @return AccountContactPersonHistory
     */
    public function setSendingAddress(\AppBundle\Entity\AddressHistory $sendingAddress = null)
    {
        $this->sendingAddress = $sendingAddress;

        return $this;
    }

    /**
     * Get sendingAddress
     *
     * @return \AppBundle\Entity\AddressHistory
     */
    public function getSendingAddress()
    {
        return $this->sendingAddress;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return AccountContactPersonHistory
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @return Address
     */
    public function getCustomBillingAddress()
    {
        return $this->customBillingAddress;
    }

    /**
     * @param Address $customBillingAddress
     */
    public function setCustomBillingAddress($customBillingAddress = null)
    {
        $this->customBillingAddress = $customBillingAddress;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }
}
