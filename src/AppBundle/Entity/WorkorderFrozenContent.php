<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
/**
 * WorkorderFrozenContent
 */
class WorkorderFrozenContent
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $municipalityFee;

    /**
     * @var string
     */
    private $processingFee;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \AppBundle\Entity\AccountHistory
     */
    private $accountHistory;

    /**
     * @var \AppBundle\Entity\DeviceHistory
     */
    private $deviceHistory;

    /**
     * @var \AppBundle\Entity\ServiceHistory
     */
    private $serviceHistory;

    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set municipalityFee
     *
     * @param string $municipalityFee
     *
     * @return WorkorderFrozenContent
     */
    public function setMunicipalityFee($municipalityFee)
    {
        $this->municipalityFee = $municipalityFee;

        return $this;
    }

    /**
     * Get municipalityFee
     *
     * @return string
     */
    public function getMunicipalityFee()
    {
        return $this->municipalityFee;
    }

    /**
     * Set processingFee
     *
     * @param string $processingFee
     *
     * @return WorkorderFrozenContent
     */
    public function setProcessingFee($processingFee)
    {
        $this->processingFee = $processingFee;

        return $this;
    }

    /**
     * Get processingFee
     *
     * @return string
     */
    public function getProcessingFee()
    {
        return $this->processingFee;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return WorkorderFrozenContent
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set accountHistory
     *
     * @param \AppBundle\Entity\AccountHistory $accountHistory
     *
     * @return WorkorderFrozenContent
     */
    public function setAccountHistory(\AppBundle\Entity\AccountHistory $accountHistory = null)
    {
        $this->accountHistory = $accountHistory;

        return $this;
    }

    /**
     * Get accountHistory
     *
     * @return \AppBundle\Entity\AccountHistory
     */
    public function getAccountHistory()
    {
        return $this->accountHistory;
    }

    /**
     * Set deviceHistory
     *
     * @param \AppBundle\Entity\DeviceHistory $deviceHistory
     *
     * @return WorkorderFrozenContent
     */
    public function setDeviceHistory(\AppBundle\Entity\DeviceHistory $deviceHistory = null)
    {
        $this->deviceHistory = $deviceHistory;

        return $this;
    }

    /**
     * Get deviceHistory
     *
     * @return \AppBundle\Entity\DeviceHistory
     */
    public function getDeviceHistory()
    {
        return $this->deviceHistory;
    }

    /**
     * Set serviceHistory
     *
     * @param \AppBundle\Entity\ServiceHistory $serviceHistory
     *
     * @return WorkorderFrozenContent
     */
    public function setServiceHistory(\AppBundle\Entity\ServiceHistory $serviceHistory = null)
    {
        $this->serviceHistory = $serviceHistory;

        return $this;
    }

    /**
     * Get serviceHistory
     *
     * @return \AppBundle\Entity\ServiceHistory
     */
    public function getServiceHistory()
    {
        return $this->serviceHistory;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return WorkorderFrozenContent
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }
}
