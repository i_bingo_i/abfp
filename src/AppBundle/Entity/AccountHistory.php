<?php

namespace AppBundle\Entity;

/**
 * AccountHistory
 */
class AccountHistory
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     */
    private $dateSave;

    /**
     * @var string
     */
    private $website;

    /**
     * @var boolean
     */
    private $specialDiscount = false;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $devices;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $address;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $billingAddress;

    /**
     * @var \AppBundle\Entity\AccountType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $parent;

    /**
     * @var \AppBundle\Entity\ClientType
     */
    private $clientType;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $ownerEntity;

    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * @var \AppBundle\Entity\PaymentTerm
     */
    private $paymentTerm;

    /**
     * @var \AppBundle\Entity\Municipality
     */
    private $municipality;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->devices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AccountHistory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return AccountHistory
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return AccountHistory
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return AccountHistory
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateSave
     *
     * @param \DateTime $dateSave
     *
     * @return AccountHistory
     */
    public function setDateSave($dateSave)
    {
        $this->dateSave = $dateSave;

        return $this;
    }

    /**
     * Get dateSave
     *
     * @return \DateTime
     */
    public function getDateSave()
    {
        return $this->dateSave;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return AccountHistory
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set specialDiscount
     *
     * @param boolean $specialDiscount
     *
     * @return AccountHistory
     */
    public function setSpecialDiscount($specialDiscount)
    {
        $this->specialDiscount = $specialDiscount;

        return $this;
    }

    /**
     * Get specialDiscount
     *
     * @return boolean
     */
    public function getSpecialDiscount()
    {
        return $this->specialDiscount;
    }

    /**
     * Add device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return AccountHistory
     */
    public function addDevice(\AppBundle\Entity\Device $device)
    {
        $this->devices[] = $device;

        return $this;
    }

    /**
     * Remove device
     *
     * @param \AppBundle\Entity\Device $device
     */
    public function removeDevice(\AppBundle\Entity\Device $device)
    {
        $this->devices->removeElement($device);
    }

    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevices()
    {
        return $this->devices;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Account $child
     *
     * @return AccountHistory
     */
    public function addChild(\AppBundle\Entity\Account $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Account $child
     */
    public function removeChild(\AppBundle\Entity\Account $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return AccountHistory
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set billingAddress
     *
     * @param \AppBundle\Entity\Address $billingAddress
     *
     * @return AccountHistory
     */
    public function setBillingAddress(\AppBundle\Entity\Address $billingAddress = null)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\AccountType $type
     *
     * @return AccountHistory
     */
    public function setType(\AppBundle\Entity\AccountType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\AccountType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Account $parent
     *
     * @return AccountHistory
     */
    public function setParent(\AppBundle\Entity\Account $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Account
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set clientType
     *
     * @param \AppBundle\Entity\ClientType $clientType
     *
     * @return AccountHistory
     */
    public function setClientType(\AppBundle\Entity\ClientType $clientType = null)
    {
        $this->clientType = $clientType;

        return $this;
    }

    /**
     * Get clientType
     *
     * @return \AppBundle\Entity\ClientType
     */
    public function getClientType()
    {
        return $this->clientType;
    }

    /**
     * Set ownerEntity
     *
     * @param \AppBundle\Entity\Account $ownerEntity
     *
     * @return AccountHistory
     */
    public function setOwnerEntity(\AppBundle\Entity\Account $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \AppBundle\Entity\Account
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return AccountHistory
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return AccountHistory
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set paymentTerm
     *
     * @param \AppBundle\Entity\PaymentTerm $paymentTerm
     *
     * @return AccountHistory
     */
    public function setPaymentTerm(\AppBundle\Entity\PaymentTerm $paymentTerm = null)
    {
        $this->paymentTerm = $paymentTerm;

        return $this;
    }

    /**
     * Get paymentTerm
     *
     * @return \AppBundle\Entity\PaymentTerm
     */
    public function getPaymentTerm()
    {
        return $this->paymentTerm;
    }

    /**
     * Set municipality
     *
     * @param \AppBundle\Entity\Municipality $municipality
     *
     * @return AccountHistory
     */
    public function setMunicipality(\AppBundle\Entity\Municipality $municipality = null)
    {
        $this->municipality = $municipality;

        return $this;
    }

    /**
     * Get municipality
     *
     * @return \AppBundle\Entity\Municipality
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }
}
