<?php

namespace AppBundle\Entity;

/**
 * PaymentTerm
 */
class PaymentTerm
{
    public const  NET_FIFTEEN_DAYS = 'net_fifteen_days';
    public const  NET_THIRTY_DAYS = 'net_thirty_days';
    public const  COD = 'cod';
    public const  FULL_PAID_BEFORE_STARTED = 'full_paid_before_started';
    public const  FIFTY_PERCENT_DEPOSIT = 'fifty_percent_deposit';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $accountingSystemId;

    /**
     * @var int
     */
    private $editSequence;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PaymentTerm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return PaymentTerm
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }
    /**
     * @var string
     */
    private $description;


    /**
     * Set description
     *
     * @param string $description
     *
     * @return PaymentTerm
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getAccountingSystemId()
    {
        return $this->accountingSystemId;
    }

    /**
     * @param string $accountingSystemId
     */
    public function setAccountingSystemId($accountingSystemId)
    {
        $this->accountingSystemId = $accountingSystemId;
    }

    /**
     * @return int
     */
    public function getEditSequence()
    {
        return $this->editSequence;
    }

    /**
     * @param int $editSequence
     */
    public function setEditSequence($editSequence)
    {
        $this->editSequence = $editSequence;
    }

}
