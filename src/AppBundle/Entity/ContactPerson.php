<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use AppBundle\Entity\EntityTrait\PhoneFilterTrait;
use Interfaces\IdInterface;

/**
 * ContactPerson
 *
 * Added code filter for phone number (don't auto generate this entity)
 */
class ContactPerson implements IdInterface
{
    use DateTimeControlTrait;
    use PhoneFilterTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $ext;

    /**
     * @var string
     */
    private $cell;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var boolean
     */
    private $cod = false;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $proposal;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $personalAddress;

    /**
     * @var integer
     */
    private $numContact;

    /**
     * @var string
     */
    private $sourceEntity;

    /**
     * @var string
     */
    private $sourceId;

    /**
     * @var boolean
     */
    private $isHistoryProcessed = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proposal = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return ContactPerson
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return ContactPerson
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ContactPerson
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ContactPerson
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return ContactPerson
     */
    public function setPhone($phone)
    {
        $this->phone = $this->filterPhone($phone);

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set ext
     *
     * @param string $ext
     *
     * @return ContactPerson
     */
    public function setExt($ext)
    {
        $this->ext = $ext;

        return $this;
    }

    /**
     * Get ext
     *
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * Set cell
     *
     * @param string $cell
     *
     * @return ContactPerson
     */
    public function setCell($cell)
    {
        $this->cell = $this->filterPhone($cell);

        return $this;
    }

    /**
     * Get cell
     *
     * @return string
     */
    public function getCell()
    {
        return $this->cell;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return ContactPerson
     */
    public function setFax($fax)
    {
        $this->fax = $this->filterPhone($fax);

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set cod
     *
     * @param boolean $cod
     *
     * @return ContactPerson
     */
    public function setCod($cod)
    {
        $this->cod = $cod;

        return $this;
    }

    /**
     * Get cod
     *
     * @return boolean
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return ContactPerson
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ContactPerson
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ContactPerson
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ContactPerson
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return ContactPerson
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add proposal
     *
     * @param \AppBundle\Entity\Proposal $proposal
     *
     * @return ContactPerson
     */
    public function addProposal(\AppBundle\Entity\Proposal $proposal)
    {
        $this->proposal[] = $proposal;

        return $this;
    }

    /**
     * Remove proposal
     *
     * @param \AppBundle\Entity\Proposal $proposal
     */
    public function removeProposal(\AppBundle\Entity\Proposal $proposal)
    {
        $this->proposal->removeElement($proposal);
    }

    /**
     * Get proposal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * Set personalAddress
     *
     * @param \AppBundle\Entity\Address $personalAddress
     *
     * @return ContactPerson
     */
    public function setPersonalAddress(\AppBundle\Entity\Address $personalAddress = null)
    {
        $this->personalAddress = $personalAddress;

        return $this;
    }

    /**
     * Get personalAddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getPersonalAddress()
    {
        return $this->personalAddress;
    }

    /**
     * Set numContact
     *
     * @param integer $numContact
     *
     * @return ContactPerson
     */
    public function setNumContact($numContact)
    {
        $this->numContact = $numContact;

        return $this;
    }

    /**
     * Get numContact
     *
     * @return integer
     */
    public function getNumContact()
    {
        return $this->numContact;
    }

    /**
     * Set sourceEntity
     *
     * @param string $sourceEntity
     *
     * @return ContactPerson
     */
    public function setSourceEntity($sourceEntity)
    {
        $this->sourceEntity = $sourceEntity;

        return $this;
    }

    /**
     * Get sourceEntity
     *
     * @return string
     */
    public function getSourceEntity()
    {
        return $this->sourceEntity;
    }

    /**
     * Set sourceId
     *
     * @param string $sourceId
     *
     * @return ContactPerson
     */
    public function setSourceId($sourceId)
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    /**
     * Get sourceId
     *
     * @return string
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * Set isHistoryProcessed
     *
     * @param boolean $isHistoryProcessed
     *
     * @return ContactPerson
     */
    public function setIsHistoryProcessed($isHistoryProcessed)
    {
        $this->isHistoryProcessed = $isHistoryProcessed;

        return $this;
    }

    /**
     * Get isHistoryProcessed
     *
     * @return boolean
     */
    public function getIsHistoryProcessed()
    {
        return $this->isHistoryProcessed;
    }
}
