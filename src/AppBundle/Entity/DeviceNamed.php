<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use Doctrine\Common\Collections\Collection;

/**
 * DeviceNamed
 */
class DeviceNamed
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fields;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $category;

    /**
     * @var \AppBundle\Entity\DeviceNamed
     */
    private $parent;

    /**
     * @var integer
     */
    private $sort = 0;

    /**
     * @var integer
     */
    private $level = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DeviceNamed
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return DeviceNamed
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return DeviceNamed
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return DeviceNamed
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Add field
     *
     * @param \AppBundle\Entity\DynamicField $field
     *
     * @return DeviceNamed
     */
    public function addField(\AppBundle\Entity\DynamicField $field)
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * Remove field
     *
     * @param \AppBundle\Entity\DynamicField $field
     */
    public function removeField(\AppBundle\Entity\DynamicField $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\DeviceNamed $child
     *
     * @return DeviceNamed
     */
    public function addChild(\AppBundle\Entity\DeviceNamed $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\DeviceNamed $child
     */
    public function removeChild(\AppBundle\Entity\DeviceNamed $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\DeviceCategory $category
     *
     * @return DeviceNamed
     */
    public function setCategory(\AppBundle\Entity\DeviceCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\DeviceNamed $parent
     *
     * @return DeviceNamed
     */
    public function setParent(\AppBundle\Entity\DeviceNamed $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\DeviceNamed
     */
    public function getParent()
    {
        return $this->parent;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $serviceNamed;


    /**
     * Add serviceNamed
     *
     * @param \AppBundle\Entity\ServiceNamed $serviceNamed
     *
     * @return DeviceNamed
     */
    public function addServiceNamed(\AppBundle\Entity\ServiceNamed $serviceNamed)
    {
        $this->serviceNamed[] = $serviceNamed;

        return $this;
    }

    /**
     * Remove serviceNamed
     *
     * @param \AppBundle\Entity\ServiceNamed $serviceNamed
     */
    public function removeServiceNamed(\AppBundle\Entity\ServiceNamed $serviceNamed)
    {
        $this->serviceNamed->removeElement($serviceNamed);
    }

    /**
     * Get serviceNamed
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceNamed()
    {
        return $this->serviceNamed;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $devices;


    /**
     * Add device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return DeviceNamed
     */
    public function addDevice(\AppBundle\Entity\Device $device)
    {
        $this->devices[] = $device;

        return $this;
    }

    /**
     * Remove device
     *
     * @param \AppBundle\Entity\Device $device
     */
    public function removeDevice(\AppBundle\Entity\Device $device)
    {
        $this->devices->removeElement($device);
    }

    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevices()
    {
        return $this->devices;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return DeviceNamed
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return mixed|null
     */
    public function getInspections()
    {
        $services = $this->separateInspectionsAndRepair($this->getServiceNamed());

        return array_key_exists("inspections", $services) ? $services["inspections"] : null;

    }

    /**
     * @return mixed|null
     */
    public function getRepairs()
    {
        $services = $this->separateInspectionsAndRepair($this->getServiceNamed());

        return array_key_exists("repairs", $services) ? $services["repairs"] : null;
    }

    /**
     * @param Collection $servicesNameds
     * @return mixed
     */
    private function separateInspectionsAndRepair(Collection $servicesNameds)
    {
        $separateServicesNamed = [];
        /** @var ServiceNamed $serviceNamed */
        foreach ($servicesNameds as $serviceNamed) {
            if (empty($serviceNamed->getFrequency())) {
                $separateServicesNamed["repairs"][] = $serviceNamed;
            } else {
                $separateServicesNamed["inspections"][] = $serviceNamed;
            }
        }

        return $separateServicesNamed;
    }

    /**
     * @return bool
     */
    public function getIsAlarm()
    {
        if ($this->getAlias() === "fire_alarm_control_panel") {
            return true;
        }

        return false;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return DeviceNamed
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }
}
