<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * DynamicFieldValue
 */
class DynamicFieldValue
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\DynamicField
     */
    private $field;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $device;

    /**
     * @var \AppBundle\Entity\DynamicFieldDropboxChoices
     */
    private $optionValue;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return DynamicFieldValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return DynamicFieldValue
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return DynamicFieldValue
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set field
     *
     * @param \AppBundle\Entity\DynamicField $field
     *
     * @return DynamicFieldValue
     */
    public function setField(\AppBundle\Entity\DynamicField $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \AppBundle\Entity\DynamicField
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return DynamicFieldValue
     */
    public function setDevice(\AppBundle\Entity\Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set optionValue
     *
     * @param \AppBundle\Entity\DynamicFieldDropboxChoices $optionValue
     *
     * @return DynamicFieldValue
     */
    public function setOptionValue(\AppBundle\Entity\DynamicFieldDropboxChoices $optionValue = null)
    {
        $this->optionValue = $optionValue;

        return $this;
    }

    /**
     * Get optionValue
     *
     * @return \AppBundle\Entity\DynamicFieldDropboxChoices
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }
}
