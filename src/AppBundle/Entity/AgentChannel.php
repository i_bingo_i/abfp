<?php

namespace AppBundle\Entity;

/**
 * AgentChannel
 */
class AgentChannel
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $isDefault = false;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dynamicFields;

    /**
     * @var \AppBundle\Entity\ChannelNamed
     */
    private $named;

    /**
     * @var \AppBundle\Entity\Agent
     */
    private $agent;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $deviceCategory;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dynamicFields = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     *
     * @return AgentChannel
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Add dynamicField
     *
     * @param \AppBundle\Entity\AgentChannelDynamicFieldValue $dynamicField
     *
     * @return AgentChannel
     */
    public function addDynamicField(\AppBundle\Entity\AgentChannelDynamicFieldValue $dynamicField)
    {
        $this->dynamicFields[] = $dynamicField;

        return $this;
    }

    /**
     * Remove dynamicField
     *
     * @param \AppBundle\Entity\AgentChannelDynamicFieldValue $dynamicField
     */
    public function removeDynamicField(\AppBundle\Entity\AgentChannelDynamicFieldValue $dynamicField)
    {
        $this->dynamicFields->removeElement($dynamicField);
    }

    /**
     * Get dynamicFields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDynamicFields()
    {
        return $this->dynamicFields;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\ChannelNamed $named
     *
     * @return AgentChannel
     */
    public function setNamed(\AppBundle\Entity\ChannelNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\ChannelNamed
     */
    public function getNamed()
    {
        return $this->named;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\Agent $agent
     *
     * @return AgentChannel
     */
    public function setAgent(\AppBundle\Entity\Agent $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set deviceCategory
     *
     * @param \AppBundle\Entity\DeviceCategory $deviceCategory
     *
     * @return AgentChannel
     */
    public function setDeviceCategory(\AppBundle\Entity\DeviceCategory $deviceCategory = null)
    {
        $this->deviceCategory = $deviceCategory;

        return $this;
    }

    /**
     * Get deviceCategory
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getDeviceCategory()
    {
        return $this->deviceCategory;
    }
}
