<?php

namespace AppBundle\Entity;

/**
 * AgentChannelDynamicFieldValue
 */
class AgentChannelDynamicFieldValue
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var \AppBundle\Entity\AgentChannel
     */
    private $agentChannel;

    /**
     * @var \AppBundle\Entity\ChannelDynamicField
     */
    private $field;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $entityValue;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return AgentChannelDynamicFieldValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set agentChannel
     *
     * @param \AppBundle\Entity\AgentChannel $agentChannel
     *
     * @return AgentChannelDynamicFieldValue
     */
    public function setAgentChannel(\AppBundle\Entity\AgentChannel $agentChannel = null)
    {
        $this->agentChannel = $agentChannel;

        return $this;
    }

    /**
     * Get agentChannel
     *
     * @return \AppBundle\Entity\AgentChannel
     */
    public function getAgentChannel()
    {
        return $this->agentChannel;
    }

    /**
     * Set field
     *
     * @param \AppBundle\Entity\ChannelDynamicField $field
     *
     * @return AgentChannelDynamicFieldValue
     */
    public function setField(\AppBundle\Entity\ChannelDynamicField $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \AppBundle\Entity\ChannelDynamicField
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set entityValue
     *
     * @param \AppBundle\Entity\Address $entityValue
     *
     * @return AgentChannelDynamicFieldValue
     */
    public function setEntityValue(\AppBundle\Entity\Address $entityValue = null)
    {
        $this->entityValue = $entityValue;

        return $this;
    }

    /**
     * Get entityValue
     *
     * @return \AppBundle\Entity\Address
     */
    public function getEntityValue()
    {
        return $this->entityValue;
    }
}
