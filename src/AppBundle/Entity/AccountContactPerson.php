<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use InvoiceBundle\Entity\Customer;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\Inline;

/**
 * DO NOT REGENERATE THIS ENTITY!! Serializer is here!!
 *
 * AccountContactPerson
 */
class AccountContactPerson
{
    use DateTimeControlTrait;

    /**
     * @var integer
     * @Groups({"workorder", "account", "invoice", "full"})
     * @Type("string")
     * @SerializedName("id")
     */
    private $id;

    /**
     * @var boolean
     * @Groups({"workorder", "account", "full"})
     * @Type("boolean")
     * @SerializedName("deleted")
     */
    private $deleted = false;

    /**
     * @var boolean
     * @Groups({"workorder", "account", "invoice", "full"})
     * @Type("boolean")
     * @SerializedName("authorizer")
     */
    private $authorizer = false;

    /**
     * @var boolean
     * @Groups({"workorder", "account", "full"})
     * @Type("boolean")
     * @SerializedName("access")
     */
    private $access = false;

    /**
     * @var boolean
     * @Groups({"workorder", "account", "full"})
     * @Type("boolean")
     * @SerializedName("accessPrimary")
     */
    private $accessPrimary = false;

    /**
     * @var boolean
     * @Groups({"workorder", "account", "full"})
     * @Type("boolean")
     * @SerializedName("payment")
     */
    private $payment = false;

    /**
     * @var boolean
     * @Groups({"workorder", "account", "full"})
     * @Type("boolean")
     * @SerializedName("paymentPrimary")
     */
    private $paymentPrimary = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     * @Groups({"workorder", "account", "full"})
     * @Type("DateTime<'U'>")
     * @SerializedName("timestamp")
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\ContactPerson
     * @Groups({"invoice"})
     * @Type("AppBundle\Entity\ContactPerson")
     * @SerializedName("contactPerson")
     */
    private $contactPerson;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $responsibilities;

    /**
     * @var \AppBundle\Entity\Address
     * @Groups({"invoice"})
     * @Type("AppBundle\Entity\Address")
     * @SerializedName("sendingAddress")
     */
    private $sendingAddress;

    /**
     * @var boolean
     */
    private $isHistoryProcessed = false;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $paymentResponsibilities;

    /**
     * @var Customer
     * @Groups({"invoice"})
     * @Type("InvoiceBundle\Entity\Customer")
     * @SerializedName("customer")
     */
    private $customer;

    /** @var Address */
    private $customBillingAddress;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->responsibilities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->paymentResponsibilities = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return AccountContactPerson
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return AccountContactPerson
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return AccountContactPerson
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set contactPerson
     *
     * @param \AppBundle\Entity\ContactPerson $contactPerson
     *
     * @return AccountContactPerson
     */
    public function setContactPerson(\AppBundle\Entity\ContactPerson $contactPerson = null)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return \AppBundle\Entity\ContactPerson
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return AccountContactPerson
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set authorizer
     *
     * @param boolean $authorizer
     *
     * @return AccountContactPerson
     */
    public function setAuthorizer($authorizer)
    {
        $this->authorizer = $authorizer;

        return $this;
    }

    /**
     * Get authorizer
     *
     * @return boolean
     */
    public function getAuthorizer()
    {
        return $this->authorizer;
    }

    /**
     * Set access
     *
     * @param boolean $access
     *
     * @return AccountContactPerson
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return boolean
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set accessPrimary
     *
     * @param boolean $accessPrimary
     *
     * @return AccountContactPerson
     */
    public function setAccessPrimary($accessPrimary)
    {
        $this->accessPrimary = $accessPrimary;

        return $this;
    }

    /**
     * Get accessPrimary
     *
     * @return boolean
     */
    public function getAccessPrimary()
    {
        return $this->accessPrimary;
    }

    /**
     * Set payment
     *
     * @param boolean $payment
     *
     * @return AccountContactPerson
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return boolean
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set paymentPrimary
     *
     * @param boolean $paymentPrimary
     *
     * @return AccountContactPerson
     */
    public function setPaymentPrimary($paymentPrimary)
    {
        $this->paymentPrimary = $paymentPrimary;

        return $this;
    }

    /**
     * Get paymentPrimary
     *
     * @return boolean
     */
    public function getPaymentPrimary()
    {
        return $this->paymentPrimary;
    }

    /**
     * Add responsibility
     *
     * @param \AppBundle\Entity\DeviceCategory $responsibility
     *
     * @return AccountContactPerson
     */
    public function addResponsibility(\AppBundle\Entity\DeviceCategory $responsibility)
    {
        $this->responsibilities[] = $responsibility;

        return $this;
    }

    /**
     * Remove responsibility
     *
     * @param \AppBundle\Entity\DeviceCategory $responsibility
     */
    public function removeResponsibility(\AppBundle\Entity\DeviceCategory $responsibility)
    {
        $this->responsibilities->removeElement($responsibility);
    }

    /**
     * Get responsibilities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponsibilities()
    {
        return $this->responsibilities;
    }

    /**
     * Set sendingAddress
     *
     * @param \AppBundle\Entity\Address $sendingAddress
     *
     * @return AccountContactPerson
     */
    public function setSendingAddress(\AppBundle\Entity\Address $sendingAddress = null)
    {
        $this->sendingAddress = $sendingAddress;

        return $this;
    }

    /**
     * Get sendingAddress
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("sendingAddress")
     * @return \AppBundle\Entity\Address
     */
    public function getSendingAddress()
    {
        return $this->sendingAddress;
    }

    /**
     * Set isHistoryProcessed
     *
     * @param boolean $isHistoryProcessed
     *
     * @return AccountContactPerson
     */
    public function setIsHistoryProcessed($isHistoryProcessed)
    {
        $this->isHistoryProcessed = $isHistoryProcessed;

        return $this;
    }

    /**
     * Get isHistoryProcessed
     *
     * @return boolean
     */
    public function getIsHistoryProcessed()
    {
        return $this->isHistoryProcessed;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return AccountContactPerson
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("firstName")
     */
    public function getFirstName()
    {
        return $this->getContactPerson()->getFirstName();
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("firstLast")
     */
    public function getLastName()
    {
        return $this->getContactPerson()->getLastName();
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("phone")
     */
    public function getPhone()
    {
        return $this->getContactPerson()->getPhone();
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("cell")
     */
    public function getCell()
    {
        return $this->getContactPerson()->getCell();
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("ext")
     */
    public function getExt()
    {
        return $this->getContactPerson()->getExt();
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("email")
     */
    public function getEmail()
    {
        return $this->getContactPerson()->getEmail();
    }

    /**
     * @return \AppBundle\Entity\Company|null
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @Type("AppBundle\Entity\Company")
     * @MaxDepth(2)
     * @Inline
     * @SerializedName("company")
     */
    public function getCompany()
    {
        return $this->getContactPerson()->getCompany();
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("position")
     */
    public function getTitle()
    {
        return $this->getContactPerson()->getTitle();
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("addressType")
     */
    public function getAddressType()
    {
        return $this->getSendingAddress() ? $this->getSendingAddress()->getAddressType()->getAlias() : "";
    }


    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("isAccess")
     */
    public function getIsAccess()
    {
        $isAccess = false;

        if($this->getAccessPrimary() or $this->getAccess()) {
            $isAccess = true;
        }

        return $isAccess;
    }

    /**
     * @return array
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("divisions")
     */
    public function getDevisionsForACP()
    {
        $devicisons = $this->getResponsibilities();
        $serialiseDevisions = [];
        /** @var DeviceCategory $responibility */
        foreach ($devicisons as $responibility) {
            $serialiseDevisions[] = $responibility->getAlias();
        }

        return $serialiseDevisions;
    }

    /**
     * @return string
     * @VirtualProperty
     * @Groups({"workorder", "account", "full"})
     * @SerializedName("address")
     */
    public function getAddressForACP()
    {
        $address = $this->getSendingAddress();
        $formatedAddress = '';
        if ($address) {
            if (!is_null($address->getAddress())) {
                $formatedAddress .= $address->getAddress();
                $formatedAddress .= ($address->getCity() or $address->getState() or $address->getZip()) ? ', ' : ' ';
            }

            if (!is_null($address->getCity())) {
                $formatedAddress .= $address->getCity();
                $formatedAddress .= ($address->getState() or $address->getZip()) ? ', ' : ' ';
            }

            $formatedAddress .= $address->getState() ? $address->getState()->getCode() . ' ' : '';
            $formatedAddress .= $address->getZip() ? $address->getZip() . ' ' : '';
        }
        return trim($formatedAddress);
    }

    /**
     * Add paymentResponsibility
     *
     * @param \AppBundle\Entity\DeviceCategory $paymentResponsibility
     *
     * @return AccountContactPerson
     */
    public function addPaymentResponsibility(\AppBundle\Entity\DeviceCategory $paymentResponsibility)
    {
        $this->paymentResponsibilities[] = $paymentResponsibility;

        return $this;
    }

    /**
     * Remove paymentResponsibility
     *
     * @param \AppBundle\Entity\DeviceCategory $paymentResponsibility
     */
    public function removePaymentResponsibility(\AppBundle\Entity\DeviceCategory $paymentResponsibility)
    {
        $this->paymentResponsibilities->removeElement($paymentResponsibility);
    }

    /**
     * Get paymentResponsibilities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentResponsibilities()
    {
        return $this->paymentResponsibilities;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer = null)
    {
        $this->customer = $customer;
    }

    /**
     * @return Address
     */
    public function getCustomBillingAddress()
    {
        return $this->customBillingAddress;
    }

    /**
     * @param Address $customBillingAddress
     */
    public function setCustomBillingAddress($customBillingAddress = null)
    {
        $this->customBillingAddress = $customBillingAddress;
    }
}
