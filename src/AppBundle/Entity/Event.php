<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * Event
 */
class Event
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fromDate;

    /**
     * @var \DateTime
     */
    private $toDate;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var boolean
     */
    private $isFinal = false;

    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Coordinate
     */
    private $coordinate;

    /**
     * @var \AppBundle\Entity\EventType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;

    /**
     * @var boolean
     */
    private $canFinishWO = false;

    /**
     * @var boolean
     */
    private $isTimeCritical = false;

    /**
     * @var string
     */
    private $msOutlookCalendarEventId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     *
     * @return Event
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     *
     * @return Event
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Event
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Event
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set isFinal
     *
     * @param boolean $isFinal
     *
     * @return Event
     */
    public function setIsFinal($isFinal)
    {
        $this->isFinal = $isFinal;

        return $this;
    }

    /**
     * Get isFinal
     *
     * @return boolean
     */
    public function getIsFinal()
    {
        return $this->isFinal;
    }

    /**
     * Set canFinishWO
     *
     * @param boolean $canFinishWO
     *
     * @return Event
     */
    public function setCanFinishWO($canFinishWO)
    {
        $this->canFinishWO = $canFinishWO;

        return $this;
    }

    /**
     * Get canFinishWO
     *
     * @return boolean
     */
    public function getCanFinishWO()
    {
        return $this->canFinishWO;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\ContractorUser $user
     *
     * @return Event
     */
    public function setUser(\AppBundle\Entity\ContractorUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set coordinate
     *
     * @param \AppBundle\Entity\Coordinate $coordinate
     *
     * @return Event
     */
    public function setCoordinate(\AppBundle\Entity\Coordinate $coordinate = null)
    {
        $this->coordinate = $coordinate;

        return $this;
    }

    /**
     * Get coordinate
     *
     * @return \AppBundle\Entity\Coordinate
     */
    public function getCoordinate()
    {
        return $this->coordinate;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\EventType $type
     *
     * @return Event
     */
    public function setType(\AppBundle\Entity\EventType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\EventType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return Event
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @return $this
     */
    public function setDateCreateValue()
    {
        $this->dateCreate = new \DateTime();

        return $this;
    }

    /**
     * @return $this
     */
    public function setDateUpdateValue()
    {
        $this->dateUpdate = new \DateTime();

        return $this;
    }

    /**
     * Set isTimeCritical
     *
     * @param boolean $isTimeCritical
     *
     * @return Event
     */
    public function setIsTimeCritical($isTimeCritical)
    {
        $this->isTimeCritical = $isTimeCritical;

        return $this;
    }

    /**
     * Get isTimeCritical
     *
     * @return boolean
     */
    public function getIsTimeCritical()
    {
        return $this->isTimeCritical;
    }

    /**
     * Set msOutlookCalendarEventId
     *
     * @param string $msOutlookCalendarEventId
     *
     * @return Event
     */
    public function setMsOutlookCalendarEventId($msOutlookCalendarEventId)
    {
        $this->msOutlookCalendarEventId = $msOutlookCalendarEventId;

        return $this;
    }

    /**
     * Get msOutlookCalendarEventId
     *
     * @return string
     */
    public function getMsOutlookCalendarEventId()
    {
        return $this->msOutlookCalendarEventId;
    }
}
