<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * RangeQuestion
 */
class RangeQuestion
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $subTitle;

    /**
     * @var integer
     */
    private $minValue = 0;

    /**
     * @var integer
     */
    private $maxValue = 0;

    /**
     * @var integer
     */
    private $lowerBound = 0;

    /**
     * @var integer
     */
    private $upperBound = 0;

    /**
     * @var integer
     */
    private $sort = 0;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\RangeQuestionStatus
     */
    private $positiveStatus;

    /**
     * @var \AppBundle\Entity\RangeQuestionStatus
     */
    private $lowerNegativeStatus;

    /**
     * @var \AppBundle\Entity\RangeQuestionStatus
     */
    private $upperNegativeStatus;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return RangeQuestion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subTitle
     *
     * @param string $subTitle
     *
     * @return RangeQuestion
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * Get subTitle
     *
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set minValue
     *
     * @param integer $minValue
     *
     * @return RangeQuestion
     */
    public function setMinValue($minValue)
    {
        $this->minValue = $minValue;

        return $this;
    }

    /**
     * Get minValue
     *
     * @return integer
     */
    public function getMinValue()
    {
        return $this->minValue;
    }

    /**
     * Set maxValue
     *
     * @param integer $maxValue
     *
     * @return RangeQuestion
     */
    public function setMaxValue($maxValue)
    {
        $this->maxValue = $maxValue;

        return $this;
    }

    /**
     * Get maxValue
     *
     * @return integer
     */
    public function getMaxValue()
    {
        return $this->maxValue;
    }

    /**
     * Set lowerBound
     *
     * @param integer $lowerBound
     *
     * @return RangeQuestion
     */
    public function setLowerBound($lowerBound)
    {
        $this->lowerBound = $lowerBound;

        return $this;
    }

    /**
     * Get lowerBound
     *
     * @return integer
     */
    public function getLowerBound()
    {
        return $this->lowerBound;
    }

    /**
     * Set upperBound
     *
     * @param integer $upperBound
     *
     * @return RangeQuestion
     */
    public function setUpperBound($upperBound)
    {
        $this->upperBound = $upperBound;

        return $this;
    }

    /**
     * Get upperBound
     *
     * @return integer
     */
    public function getUpperBound()
    {
        return $this->upperBound;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return RangeQuestion
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return RangeQuestion
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return RangeQuestion
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set positiveStatus
     *
     * @param \AppBundle\Entity\RangeQuestionStatus $positiveStatus
     *
     * @return RangeQuestion
     */
    public function setPositiveStatus(\AppBundle\Entity\RangeQuestionStatus $positiveStatus = null)
    {
        $this->positiveStatus = $positiveStatus;

        return $this;
    }

    /**
     * Get positiveStatus
     *
     * @return \AppBundle\Entity\RangeQuestionStatus
     */
    public function getPositiveStatus()
    {
        return $this->positiveStatus;
    }

    /**
     * Set lowerNegativeStatus
     *
     * @param \AppBundle\Entity\RangeQuestionStatus $lowerNegativeStatus
     *
     * @return RangeQuestion
     */
    public function setLowerNegativeStatus(\AppBundle\Entity\RangeQuestionStatus $lowerNegativeStatus = null)
    {
        $this->lowerNegativeStatus = $lowerNegativeStatus;

        return $this;
    }

    /**
     * Get lowerNegativeStatus
     *
     * @return \AppBundle\Entity\RangeQuestionStatus
     */
    public function getLowerNegativeStatus()
    {
        return $this->lowerNegativeStatus;
    }

    /**
     * Set upperNegativeStatus
     *
     * @param \AppBundle\Entity\RangeQuestionStatus $upperNegativeStatus
     *
     * @return RangeQuestion
     */
    public function setUpperNegativeStatus(\AppBundle\Entity\RangeQuestionStatus $upperNegativeStatus = null)
    {
        $this->upperNegativeStatus = $upperNegativeStatus;

        return $this;
    }

    /**
     * Get upperNegativeStatus
     *
     * @return \AppBundle\Entity\RangeQuestionStatus
     */
    public function getUpperNegativeStatus()
    {
        return $this->upperNegativeStatus;
    }
    /**
     * @var \AppBundle\Entity\RangeQuestionStatus
     */
    private $failAnywayStatus;


    /**
     * Set failAnywayStatus
     *
     * @param \AppBundle\Entity\RangeQuestionStatus $failAnywayStatus
     *
     * @return RangeQuestion
     */
    public function setFailAnywayStatus(\AppBundle\Entity\RangeQuestionStatus $failAnywayStatus = null)
    {
        $this->failAnywayStatus = $failAnywayStatus;

        return $this;
    }

    /**
     * Get failAnywayStatus
     *
     * @return \AppBundle\Entity\RangeQuestionStatus
     */
    public function getFailAnywayStatus()
    {
        return $this->failAnywayStatus;
    }
    /**
     * @var \AppBundle\Entity\ServicePatern
     */
    private $servicePatern;


    /**
     * Set servicePatern
     *
     * @param \AppBundle\Entity\ServicePatern $servicePatern
     *
     * @return RangeQuestion
     */
    public function setServicePatern(\AppBundle\Entity\ServicePatern $servicePatern = null)
    {
        $this->servicePatern = $servicePatern;

        return $this;
    }

    /**
     * Get servicePatern
     *
     * @return \AppBundle\Entity\ServicePatern
     */
    public function getServicePatern()
    {
        return $this->servicePatern;
    }
}
