<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * RangeResult
 */
class RangeResult
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $resultValue = 0;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resultValue
     *
     * @param integer $resultValue
     *
     * @return RangeResult
     */
    public function setResultValue($resultValue)
    {
        $this->resultValue = $resultValue;

        return $this;
    }

    /**
     * Get resultValue
     *
     * @return integer
     */
    public function getResultValue()
    {
        return $this->resultValue;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return RangeResult
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return RangeResult
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }
    /**
     * @var \AppBundle\Entity\RangeQuestion
     */
    private $rangeQuestion;


    /**
     * Set rangeQuestion
     *
     * @param \AppBundle\Entity\RangeQuestion $rangeQuestion
     *
     * @return RangeResult
     */
    public function setRangeQuestion(\AppBundle\Entity\RangeQuestion $rangeQuestion = null)
    {
        $this->rangeQuestion = $rangeQuestion;

        return $this;
    }

    /**
     * Get rangeQuestion
     *
     * @return \AppBundle\Entity\RangeQuestion
     */
    public function getRangeQuestion()
    {
        return $this->rangeQuestion;
    }
    /**
     * @var \AppBundle\Entity\RangeQuestionStatus
     */
    private $rangeQuestionStatus;


    /**
     * Set rangeQuestionStatus
     *
     * @param \AppBundle\Entity\RangeQuestionStatus $rangeQuestionStatus
     *
     * @return RangeResult
     */
    public function setRangeQuestionStatus(\AppBundle\Entity\RangeQuestionStatus $rangeQuestionStatus = null)
    {
        $this->rangeQuestionStatus = $rangeQuestionStatus;

        return $this;
    }

    /**
     * Get rangeQuestionStatus
     *
     * @return \AppBundle\Entity\RangeQuestionStatus
     */
    public function getRangeQuestionStatus()
    {
        return $this->rangeQuestionStatus;
    }
    /**
     * @var \AppBundle\Entity\Report
     */
    private $report;


    /**
     * Set report
     *
     * @param \AppBundle\Entity\Report $report
     *
     * @return RangeResult
     */
    public function setReport(\AppBundle\Entity\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \AppBundle\Entity\Report
     */
    public function getReport()
    {
        return $this->report;
    }
}
