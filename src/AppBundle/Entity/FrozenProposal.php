<?php

namespace AppBundle\Entity;

/**
 * FrozenProposal
 */
class FrozenProposal
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $frozenServices;

    /**
     * @var \AppBundle\Entity\AccountHistory
     */
    private $account;

    /**
     * @var \AppBundle\Entity\AccountContactPersonHistory
     */
    private $recipient;

    /**
     * @var \AppBundle\Entity\ContractorUserHistory
     */
    private $creator;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->frozenServices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add frozenService
     *
     * @param \AppBundle\Entity\FrozenServiceForProposal $frozenService
     *
     * @return FrozenProposal
     */
    public function addFrozenService(\AppBundle\Entity\FrozenServiceForProposal $frozenService)
    {
        $this->frozenServices[] = $frozenService;

        return $this;
    }

    /**
     * Remove frozenService
     *
     * @param \AppBundle\Entity\FrozenServiceForProposal $frozenService
     */
    public function removeFrozenService(\AppBundle\Entity\FrozenServiceForProposal $frozenService)
    {
        $this->frozenServices->removeElement($frozenService);
    }

    /**
     * Get frozenServices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFrozenServices()
    {
        return $this->frozenServices;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\AccountHistory $account
     *
     * @return FrozenProposal
     */
    public function setAccount(\AppBundle\Entity\AccountHistory $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\AccountHistory
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set recipient
     *
     * @param \AppBundle\Entity\AccountContactPersonHistory $recipient
     *
     * @return FrozenProposal
     */
    public function setRecipient(\AppBundle\Entity\AccountContactPersonHistory $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \AppBundle\Entity\AccountContactPersonHistory
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set creator
     *
     * @param \AppBundle\Entity\ContractorUserHistory $creator
     *
     * @return FrozenProposal
     */
    public function setCreator(\AppBundle\Entity\ContractorUserHistory $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \AppBundle\Entity\ContractorUserHistory
     */
    public function getCreator()
    {
        return $this->creator;
    }
}
