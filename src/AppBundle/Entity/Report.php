<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * Report
 */
class Report
{
    use DateTimeControlTrait;
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $fileReport;

    /**
     * @var boolean
     */
    private $isNeedRepair = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\ReportStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\Service
     */
    private $service;

    /**
     * @var \AppBundle\Entity\Contractor
     */
    private $contractor;

    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $tester;

    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;

    /**
     * @var string
     */
    private $printTemplate;

    /**
     * @var string
     */
    private $textReport;

    /**
     * @var string
     */
    private $htmlReport;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Report
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set fileReport
     *
     * @param string $fileReport
     *
     * @return Report
     */
    public function setFileReport($fileReport)
    {
        $this->fileReport = $fileReport;

        return $this;
    }

    /**
     * Get fileReport
     *
     * @return string
     */
    public function getFileReport()
    {
        return $this->fileReport;
    }

    /**
     * Set isNeedRepair
     *
     * @param boolean $isNeedRepair
     *
     * @return Report
     */
    public function setIsNeedRepair($isNeedRepair)
    {
        $this->isNeedRepair = $isNeedRepair;

        return $this;
    }

    /**
     * Get isNeedRepair
     *
     * @return boolean
     */
    public function getIsNeedRepair()
    {
        return $this->isNeedRepair;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Report
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Report
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\ReportStatus $status
     *
     * @return Report
     */
    public function setStatus(\AppBundle\Entity\ReportStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\ReportStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return Report
     */
    public function setService(\AppBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \AppBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set contractor
     *
     * @param \AppBundle\Entity\Contractor $contractor
     *
     * @return Report
     */
    public function setContractor(\AppBundle\Entity\Contractor $contractor = null)
    {
        $this->contractor = $contractor;

        return $this;
    }

    /**
     * Get contractor
     *
     * @return \AppBundle\Entity\Contractor
     */
    public function getContractor()
    {
        return $this->contractor;
    }

    /**
     * Set tester
     *
     * @param \AppBundle\Entity\ContractorUser $tester
     *
     * @return Report
     */
    public function setTester(\AppBundle\Entity\ContractorUser $tester = null)
    {
        $this->tester = $tester;

        return $this;
    }

    /**
     * Get tester
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getTester()
    {
        return $this->tester;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return Report
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }
    /**
     * @var \DateTime
     */
    private $finishTime;


    /**
     * Set finishTime
     *
     * @param \DateTime $finishTime
     *
     * @return Report
     */
    public function setFinishTime($finishTime)
    {
        $this->finishTime = $finishTime;

        return $this;
    }

    /**
     * Get finishTime
     *
     * @return \DateTime
     */
    public function getFinishTime()
    {
        return $this->finishTime;
    }

    /**
     * @return int|null
     */
    public function getMyWorkorderId()
    {
        if ($this->getWorkorder()) {
            return $this->getWorkorder()->getId();
        }

        return null;
    }

    /**
     * Set printTemplate
     *
     * @param string $printTemplate
     *
     * @return Report
     */
    public function setPrintTemplate($printTemplate)
    {
        $this->printTemplate = $printTemplate;

        return $this;
    }

    /**
     * Get printTemplate
     *
     * @return string
     */
    public function getPrintTemplate()
    {
        return $this->printTemplate;
    }

    /**
     * Set textReport
     *
     * @param string $textReport
     *
     * @return Report
     */
    public function setTextReport($textReport)
    {
        $this->textReport = $textReport;

        return $this;
    }

    /**
     * Get textReport
     *
     * @return string
     */
    public function getTextReport()
    {
        return $this->textReport;
    }

    /**
     * Set htmlReport
     *
     * @param string $htmlReport
     *
     * @return Report
     */
    public function setHtmlReport($htmlReport)
    {
        $this->htmlReport = $htmlReport;

        return $this;
    }

    /**
     * Get htmlReport
     *
     * @return string
     */
    public function getHtmlReport()
    {
        return $this->htmlReport;
    }
}
