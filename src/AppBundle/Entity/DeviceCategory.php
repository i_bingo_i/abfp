<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * DeviceCategory
 */
class DeviceCategory
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var integer
     */
    private $weight = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DeviceCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return DeviceCategory
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return DeviceCategory
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return DeviceCategory
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return DeviceCategory
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return DeviceCategory
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $deviceName;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->deviceName = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add deviceName
     *
     * @param \AppBundle\Entity\DeviceNamed $deviceName
     *
     * @return DeviceCategory
     */
    public function addDeviceName(\AppBundle\Entity\DeviceNamed $deviceName)
    {
        $this->deviceName[] = $deviceName;

        return $this;
    }

    /**
     * Remove deviceName
     *
     * @param \AppBundle\Entity\DeviceNamed $deviceName
     */
    public function removeDeviceName(\AppBundle\Entity\DeviceNamed $deviceName)
    {
        $this->deviceName->removeElement($deviceName);
    }

    /**
     * Get deviceName
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeviceName()
    {
        return $this->deviceName;
    }
}
