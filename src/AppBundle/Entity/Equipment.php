<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
/**
 * Equipment
 *
 * Added auto set Next Calibration Date  (don't auto generate this entity)
 */
class Equipment
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $make;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $serialNumber;

    /**
     * @var \DateTime
     */
    private $lastCalibratedDate;

    /**
     * @var \DateTime
     */
    private $nextCalibratedDate;

    /**
     * @var boolean
     */
    private $companySuppliedGauge = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\EquipmentType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $contractorUser;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set make
     *
     * @param string $make
     *
     * @return Equipment
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get make
     *
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Equipment
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     *
     * @return Equipment
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set lastCalibratedDate
     *
     * @param \DateTime $lastCalibratedDate
     *
     * @return Equipment
     */
    public function setLastCalibratedDate($lastCalibratedDate)
    {
        $lastDateTime = clone $lastCalibratedDate;
        $this->setNextCalibratedDate($lastDateTime->modify("+1 year"));
        unset($lastDateTime);
        $this->lastCalibratedDate = $lastCalibratedDate;

        return $this;
    }

    /**
     * Get lastCalibratedDate
     *
     * @return \DateTime
     */
    public function getLastCalibratedDate()
    {
        return $this->lastCalibratedDate;
    }

    /**
     * Set nextCalibratedDate
     *
     * @param \DateTime $nextCalibratedDate
     *
     * @return Equipment
     */
    public function setNextCalibratedDate($nextCalibratedDate)
    {
        $this->nextCalibratedDate = $nextCalibratedDate;

        return $this;
    }

    /**
     * Get nextCalibratedDate
     *
     * @return \DateTime
     */
    public function getNextCalibratedDate()
    {
        return $this->nextCalibratedDate;
    }

    /**
     * Set companySuppliedGauge
     *
     * @param boolean $companySuppliedGauge
     *
     * @return Equipment
     */
    public function setCompanySuppliedGauge($companySuppliedGauge)
    {
        $this->companySuppliedGauge = $companySuppliedGauge;

        return $this;
    }

    /**
     * Get companySuppliedGauge
     *
     * @return boolean
     */
    public function getCompanySuppliedGauge()
    {
        return $this->companySuppliedGauge;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Equipment
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Equipment
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\EquipmentType $type
     *
     * @return Equipment
     */
    public function setType(\AppBundle\Entity\EquipmentType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\EquipmentType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set contractorUser
     *
     * @param \AppBundle\Entity\ContractorUser $contractorUser
     *
     * @return Equipment
     */
    public function setContractorUser(\AppBundle\Entity\ContractorUser $contractorUser = null)
    {
        $this->contractorUser = $contractorUser;

        return $this;
    }

    /**
     * Get contractorUser
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getContractorUser()
    {
        return $this->contractorUser;
    }
}
