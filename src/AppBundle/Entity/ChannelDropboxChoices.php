<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * ChannelDropboxChoices
 */
class ChannelDropboxChoices
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $optionValue;

    /**
     * @var boolean
     */
    private $selectDefault = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\ChannelDynamicField
     */
    private $field;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optionValue
     *
     * @param string $optionValue
     *
     * @return ChannelDropboxChoices
     */
    public function setOptionValue($optionValue)
    {
        $this->optionValue = $optionValue;

        return $this;
    }

    /**
     * Get optionValue
     *
     * @return string
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }

    /**
     * Set selectDefault
     *
     * @param boolean $selectDefault
     *
     * @return ChannelDropboxChoices
     */
    public function setSelectDefault($selectDefault)
    {
        $this->selectDefault = $selectDefault;

        return $this;
    }

    /**
     * Get selectDefault
     *
     * @return boolean
     */
    public function getSelectDefault()
    {
        return $this->selectDefault;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ChannelDropboxChoices
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ChannelDropboxChoices
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set field
     *
     * @param \AppBundle\Entity\ChannelDynamicField $field
     *
     * @return ChannelDropboxChoices
     */
    public function setField(\AppBundle\Entity\ChannelDynamicField $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \AppBundle\Entity\ChannelDynamicField
     */
    public function getField()
    {
        return $this->field;
    }
}
