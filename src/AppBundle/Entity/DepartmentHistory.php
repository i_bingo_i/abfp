<?php

namespace AppBundle\Entity;

/**
 * DepartmentHistory
 */
class DepartmentHistory
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $dateSave;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $channels;

    /**
     * @var \AppBundle\Entity\Municipality
     */
    private $municipality;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $division;

    /**
     * @var \AppBundle\Entity\Agent
     */
    private $agent;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * @var \AppBundle\Entity\Department
     */
    private $ownerEntity;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->channels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DepartmentHistory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateSave
     *
     * @param \DateTime $dateSave
     *
     * @return DepartmentHistory
     */
    public function setDateSave($dateSave)
    {
        $this->dateSave = $dateSave;

        return $this;
    }

    /**
     * Get dateSave
     *
     * @return \DateTime
     */
    public function getDateSave()
    {
        return $this->dateSave;
    }

    /**
     * Add channel
     *
     * @param \AppBundle\Entity\DepartmentChannel $channel
     *
     * @return DepartmentHistory
     */
    public function addChannel(\AppBundle\Entity\DepartmentChannel $channel)
    {
        $this->channels[] = $channel;

        return $this;
    }

    /**
     * Remove channel
     *
     * @param \AppBundle\Entity\DepartmentChannel $channel
     */
    public function removeChannel(\AppBundle\Entity\DepartmentChannel $channel)
    {
        $this->channels->removeElement($channel);
    }

    /**
     * Get channels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * Set municipality
     *
     * @param \AppBundle\Entity\Municipality $municipality
     *
     * @return DepartmentHistory
     */
    public function setMunicipality(\AppBundle\Entity\Municipality $municipality = null)
    {
        $this->municipality = $municipality;

        return $this;
    }

    /**
     * Get municipality
     *
     * @return \AppBundle\Entity\Municipality
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }

    /**
     * Set division
     *
     * @param \AppBundle\Entity\DeviceCategory $division
     *
     * @return DepartmentHistory
     */
    public function setDivision(\AppBundle\Entity\DeviceCategory $division = null)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * Get division
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\Agent $agent
     *
     * @return DepartmentHistory
     */
    public function setAgent(\AppBundle\Entity\Agent $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return DepartmentHistory
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set ownerEntity
     *
     * @param \AppBundle\Entity\Department $ownerEntity
     *
     * @return DepartmentHistory
     */
    public function setOwnerEntity(\AppBundle\Entity\Department $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \AppBundle\Entity\Department
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }
}
