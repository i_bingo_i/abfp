<?php

namespace AppBundle\Entity;

/**
 * ContractorCalendar
 */
class ContractorCalendar
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $refreshToken;

    /**
     * @var \AppBundle\Entity\Contractor
     */
    private $contractor;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     *
     * @return ContractorCalendar
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set refreshToken
     *
     * @param string $refreshToken
     *
     * @return ContractorCalendar
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    /**
     * Get refreshToken
     *
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * Set contractor
     *
     * @param \AppBundle\Entity\Contractor $contractor
     *
     * @return ContractorCalendar
     */
    public function setContractor(\AppBundle\Entity\Contractor $contractor = null)
    {
        $this->contractor = $contractor;

        return $this;
    }

    /**
     * Get contractor
     *
     * @return \AppBundle\Entity\Contractor
     */
    public function getContractor()
    {
        return $this->contractor;
    }
}
