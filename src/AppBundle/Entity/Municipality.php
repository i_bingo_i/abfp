<?php

namespace AppBundle\Entity;

/**
 * Municipality
 */
class Municipality
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $website;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $contacts;

    /**
     * @var string
     */
    private $oldMunicipalityId;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $address;

    /**
     * @var string
     */
    private $municipalityPhone;

    /**
     * @var string
     */
    private $municipalityFax;

    /**
     * @var string
     */
    private $oldBackflowDepartmentId;

    /**
     * @var string
     */
    private $oldFireDepartmentId;

    /**
     * @var boolean
     */
    private $isMixedMunicipality = false;

    /**
     * @var boolean
     */
    private $isHistoryProcessed = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Municipality
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Municipality
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Add contact
     *
     * @param \AppBundle\Entity\Contact $contact
     *
     * @return Municipality
     */
    public function addContact(\AppBundle\Entity\Contact $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \AppBundle\Entity\Contact $contact
     */
    public function removeContact(\AppBundle\Entity\Contact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $account;


    /**
     * Add account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return Municipality
     */
    public function addAccount(\AppBundle\Entity\Account $account)
    {
        $this->account[] = $account;

        return $this;
    }

    /**
     * Remove account
     *
     * @param \AppBundle\Entity\Account $account
     */
    public function removeAccount(\AppBundle\Entity\Account $account)
    {
        $this->account->removeElement($account);
    }

    /**
     * Get account
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccount()
    {
        return $this->account;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $department;


    /**
     * Add department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return Municipality
     */
    public function addDepartment(\AppBundle\Entity\Department $department)
    {
        $this->department[] = $department;

        return $this;
    }

    /**
     * Remove department
     *
     * @param \AppBundle\Entity\Department $department
     */
    public function removeDepartment(\AppBundle\Entity\Department $department)
    {
        $this->department->removeElement($department);
    }

    /**
     * Get department
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set oldMunicipalityId
     *
     * @param string $oldMunicipalityId
     *
     * @return Municipality
     */
    public function setOldMunicipalityId($oldMunicipalityId)
    {
        $this->oldMunicipalityId = $oldMunicipalityId;

        return $this;
    }

    /**
     * Get oldMunicipalityId
     *
     * @return string
     */
    public function getOldMunicipalityId()
    {
        return $this->oldMunicipalityId;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return Municipality
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set municipalityPhone
     *
     * @param string $municipalityPhone
     *
     * @return Municipality
     */
    public function setMunicipalityPhone($municipalityPhone)
    {
        $this->municipalityPhone = $municipalityPhone;

        return $this;
    }

    /**
     * Get municipalityPhone
     *
     * @return string
     */
    public function getMunicipalityPhone()
    {
        return $this->municipalityPhone;
    }

    /**
     * Set municipalityFax
     *
     * @param string $municipalityFax
     *
     * @return Municipality
     */
    public function setMunicipalityFax($municipalityFax)
    {
        $this->municipalityFax = $municipalityFax;

        return $this;
    }

    /**
     * Get municipalityFax
     *
     * @return string
     */
    public function getMunicipalityFax()
    {
        return $this->municipalityFax;
    }

    /**
     * Set isHistoryProcessed
     *
     * @param boolean $isHistoryProcessed
     *
     * @return Municipality
     */
    public function setIsHistoryProcessed($isHistoryProcessed)
    {
        $this->isHistoryProcessed = $isHistoryProcessed;

        return $this;
    }

    /**
     * Get isHistoryProcessed
     *
     * @return boolean
     */
    public function getIsHistoryProcessed()
    {
        return $this->isHistoryProcessed;
    }

    /**
     * Set oldBackflowDepartmentId
     *
     * @param string $oldBackflowDepartmentId
     *
     * @return Municipality
     */
    public function setOldBackflowDepartmentId($oldBackflowDepartmentId)
    {
        $this->oldBackflowDepartmentId = $oldBackflowDepartmentId;

        return $this;
    }

    /**
     * Get oldBackflowDepartmentId
     *
     * @return string
     */
    public function getOldBackflowDepartmentId()
    {
        return $this->oldBackflowDepartmentId;
    }

    /**
     * Set oldFireDepartmentId
     *
     * @param string $oldFireDepartmentId
     *
     * @return Municipality
     */
    public function setOldFireDepartmentId($oldFireDepartmentId)
    {
        $this->oldFireDepartmentId = $oldFireDepartmentId;

        return $this;
    }

    /**
     * Get oldFireDepartmentId
     *
     * @return string
     */
    public function getOldFireDepartmentId()
    {
        return $this->oldFireDepartmentId;
    }

    /**
     * Set isMixedMunicipality
     *
     * @param boolean $isMixedMunicipality
     *
     * @return Municipality
     */
    public function setIsMixedMunicipality($isMixedMunicipality)
    {
        $this->isMixedMunicipality = $isMixedMunicipality;

        return $this;
    }

    /**
     * Get isMixedMunicipality
     *
     * @return boolean
     */
    public function getIsMixedMunicipality()
    {
        return $this->isMixedMunicipality;
    }
}
