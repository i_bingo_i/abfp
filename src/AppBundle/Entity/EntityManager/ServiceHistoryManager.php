<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\ServiceHistory;
use Doctrine\Common\Persistence\ObjectManager;

class ServiceHistoryManager
{
    /** @var  ObjectManager */
    private $objectManager;

    /**
     * ServiceHistoryManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param ServiceHistory $serviceHistoryItem
     */
    public function save(ServiceHistory $serviceHistoryItem)
    {
        $this->objectManager->persist($serviceHistoryItem);
        $this->objectManager->flush();
    }
}
