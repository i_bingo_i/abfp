<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\ServiceNamedRepository;

class ServiceNamedManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var DeviceNamedManager */
    private $deviceNamedManager;

    public function __construct(ObjectManager $objectManager, DeviceNamedManager $deviceNamedManager)
    {
        $this->objectManager = $objectManager;

        $this->serviceNamedRepository = $this->objectManager->getRepository('AppBundle:ServiceNamed');
        $this->deviceNamedManager = $deviceNamedManager;
    }

    /**
     * @param ServiceNamed $serviceNamed
     */
    public function save(ServiceNamed $serviceNamed)
    {
        $this->objectManager->persist($serviceNamed);
        $this->objectManager->flush();
    }

    /**
     * @param ServiceNamed $serviceNamed
     * @param $data
     * @return ServiceNamed|bool
     */
    public function createRepairService(ServiceNamed $serviceNamed, $data)
    {
        /** @var DeviceNamed $deviceNamed */
        $deviceNamed = $data['deviceNamed'];
        $name = $data['name'];
        $deficiency = $data['deficiency'];
        $isNeedSendMunicipalityReport = $data['isNeedSendMunicipalityReport'];
        $isNeedMunicipalityFee = $data['isNeedMunicipalityFee'];

        $existServiceNamed = $this->serviceNamedRepository->getExistRepairServiceNamed(
            $name,
            $deviceNamed,
            $deficiency
        );

        if (!$existServiceNamed instanceof ServiceNamed) {
            $serviceNamed->setName($name);
            $serviceNamed->setDeficiency($deficiency);
            $serviceNamed->setIsNeedSendMunicipalityReport($isNeedSendMunicipalityReport);
            $serviceNamed->setIsNeedMunicipalityFee($isNeedMunicipalityFee);
            $serviceNamed->addDeviceNamed($deviceNamed);

            $this->save($serviceNamed);
        } else {
            return false;
        }

        return $serviceNamed;
    }

    /**
     * @param ServiceNamed $serviceNamed
     * @param DeviceNamed $deviceNamed
     * @return ServiceNamed|bool
     */
    public function createInspectionService(ServiceNamed $serviceNamed, DeviceNamed $deviceNamed)
    {
        $name = $serviceNamed->getName();
        $frequency = $serviceNamed->getFrequency();

        $existServiceNamed = $this->serviceNamedRepository->getExistInspectionServiceNamed(
            $name,
            $deviceNamed,
            $frequency
        );

        if ($existServiceNamed instanceof ServiceNamed) {
            return false;
        }

        $treeOfDeviceNamed = $this->deviceNamedManager->getDevicesNamedTree($deviceNamed);

        /** @var DeviceNamed $deviceNamed */
        foreach ($treeOfDeviceNamed as $deviceNamed) {
            $serviceNamed->addDeviceNamed($deviceNamed);
        }
        $this->save($serviceNamed);

        return $serviceNamed;
    }
}
