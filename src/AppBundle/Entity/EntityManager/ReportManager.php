<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Report;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Router;

class ReportManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $container;
    private $dispatcher;

    /**
     * ServiceManager constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->dispatcher = $dispatcher;
        $this->objectManager = $objectManager;
        $this->container = $container;
    }

    /**
     * @param Report $report
     */
    public function save(Report $report)
    {
        $this->objectManager->persist($report);
        $this->objectManager->flush();
    }

    /**
     * @param Report $report
     */
    public function create(Report $report)
    {
        $this->save($report);
    }

    /**
     * @param UploadedFile $file
     * @return bool|string
     */
    public function saveFile(UploadedFile $file)
    {
        $saveImagePath = "/uploads/reports";
//        /** @var Router $routerService */
//        $routerService = $this->container->get('router');
//
//        $host = $routerService->getContext()->getHost();
//        $scheme = $routerService->getContext()->getScheme();

//        return $scheme."://".$host.$saveImagePath."/".$this->container->get('app.images')->saveUploadFile($file, $saveImagePath);
        return $saveImagePath."/".$this->container->get('app.images')->saveUploadFile($file, $saveImagePath);
    }
}
