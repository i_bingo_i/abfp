<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\TrashRequests;
use Doctrine\Common\Persistence\ObjectManager;

class TrashRequestsManager
{
    /** @var ObjectManager  */
    private $objectManager;

    /**
     * TrashRequestsManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param TrashRequests $trashRequests
     */
    public function save(TrashRequests $trashRequests)
    {
        $this->objectManager->persist($trashRequests);
        $this->objectManager->flush();
    }
}
