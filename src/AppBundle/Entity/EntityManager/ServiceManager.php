<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Account;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\Report;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ServiceHistoryRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\ServiceStatusRepository;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\Contractor;
use AppBundle\Entity\ServiceFrequency;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;
use AppBundle\Events\ServiceCreatedEvent;
use AppBundle\Events\ServiceDeletedEvent;
use AppBundle\Events\ServiceUpdatedEvent;
use AppBundle\Services\ServiceHistory\Creator;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Opportunity;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ServiceManager
{
    public const STATUS_UNDER_RETEST_NOTICE = 'under_retest_notice';
    public const STATUS_UNDER_WORKORDER = 'under_workorder';
    public const STATUS_RETEST_OPPORTUNITY = 'under_retest_opportunity';
    public const STATUS_REPAIR_OPPORTUNITY = 'repair_opportunity';
    public const STATUS_UNDER_PROPOSAL = 'under_proposal';
    public const STATUS_DELETED = 'deleted';
    public const STATUS_OMITTED = 'omitted';
    public const STATUS_DISCARDED = 'discarded';
    public const STATUS_RESOLVED = 'resolved';

    public const TYPE_REPAIR_SERVICE = 'repair';
    public const TYPE_RETEST_SERVICE = 'retest';

    /** @var  ObjectManager */
    private $objectManager;
    /** @var  ServiceRepository */
    private $serviceRepository;
    /** @var  ContractorServiceRepository */
    private $contractorServiceRepository;
    /** @var OpportunityManager */
    private $opportunityManager;
    /** @var ContainerInterface  */
    private $container;
    private $dispatcher;
    /** @var ServiceStatusRepository */
    private $serviceStatusRepository;
    /** @var ContractorRepository */
    private $contractorRepository;
    /** @var AccountRepository */
    private $accountRepository;
    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;
    /** @var Creator */
    private $serviceHistoryCreator;
    /** @var array */
    private $opportunityAndServiceStatuses = [
        'retest' => self::STATUS_RETEST_OPPORTUNITY,
        'repair' => self::STATUS_REPAIR_OPPORTUNITY
    ];

    /**
     * ServiceManager constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param ObjectManager $objectManager
     * @param OpportunityManager $opportunityManager
     * @param ContainerInterface $container
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        ObjectManager $objectManager,
        OpportunityManager $opportunityManager,
        ContainerInterface $container
    ) {
        $this->dispatcher = $dispatcher;
        $this->objectManager = $objectManager;

        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->contractorServiceRepository = $this->objectManager->getRepository('AppBundle:ContractorService');
        $this->opportunityManager = $opportunityManager;
        $this->container = $container;
        $this->serviceStatusRepository = $this->objectManager->getRepository('AppBundle:ServiceStatus');
        $this->contractorRepository = $this->objectManager->getRepository('AppBundle:Contractor');
        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
        $this->serviceHistoryRepository = $this->objectManager->getRepository('AppBundle:ServiceHistory');
        $this->serviceHistoryCreator = $this->container->get('app.service_history_creator.service');
    }

    /**
     * @param Service $service
     */
    public function save(Service $service)
    {
        $this->objectManager->persist($service);
        $this->objectManager->flush();
    }

    /**
     * @param Service $service
     */
    public function delete(Service $service)
    {
        $this->resetOpportunity($service);
        $service->setDeleted(true);
        $this->setStatus($service, self::STATUS_DELETED);
        $this->save($service);
        /** @var ServiceDeletedEvent $event */
        $event = new ServiceDeletedEvent($service);
        $this->dispatcher->dispatch('service.deleted', $event);
    }

    /**
     * @param Service $service
     */
    public function create(Service $service)
    {
        $this->save($service);
        /** @var ServiceCreatedEvent $event */
        $event = new ServiceCreatedEvent($service);
        $this->dispatcher->dispatch('service.created', $event);
    }

    /**
     * @param Service $service
     */
    public function update(Service $service)
    {
        $this->save($service);
        /** @var ServiceUpdatedEvent $event */
        $event = new ServiceUpdatedEvent($service);
        $this->dispatcher->dispatch('service.updated', $event);
    }

    /**
     * @param Device $device
     */
    public function deleteFromDevice(Device $device)
    {
        $services = $this->serviceRepository->findBy(['device' => $device->getId()]);

        foreach ($services as $service) {
            if (!$service->getDeleted()) {
                $this->delete($service);
            }
        }
    }

    /**
     * @param Service $service
     */
    public function checkAndCreateContractorService(Service $service)
    {
        $contractor = $service->getCompanyLastTested() ? $service->getCompanyLastTested() : false;
        /** @var ServiceNamed $serviceNamed */
        $serviceNamed = $service->getNamed() ? $service->getNamed() : false;

        /** @var ContractorService $contractorService */
        $contractorService = $this->contractorServiceRepository->findBy([
            'contractor' => $contractor,
            'named' => $serviceNamed
        ]);

        if (!$contractorService and $contractor) {
            /** @var ContractorService $newContractorService */
            $newContractorService = new ContractorService();

            $newContractorService->setNamed($serviceNamed);
            $newContractorService->setContractor($contractor);
            /** @var DeviceNamed $deviceNamed */
            $deviceNamed = $serviceNamed->getDeviceNamed()->current();
            $newContractorService->setDeviceCategory($deviceNamed->getCategory());
            $newContractorService->setState($contractor->getAddress()->getState());

            $this->objectManager->persist($newContractorService);
            $this->objectManager->flush();
        }
    }

    /**
     * @param Service $service
     * @return Service
     */
    public function serialize(Service $service)
    {
        /** @var ContractorService $contractorService */
        $contractorService = $this->contractorServiceRepository->findOneBy([
            'named' => $service->getNamed(),
            'contractor' => $service->getCompanyLastTested()
        ]);

        $probableFee = '';
        if ($contractorService) {
            $probableFee = $contractorService->getFixedPrice();
        }
        $service->probableFee = $probableFee;

        return $service;
    }

    /**
     * Attach opportunity to service or
     * Create new Opportunity and attach new opportunity to service
     * @param Service $service
     * @param string $opportunityType
     */
    public function attachOpportunity(Service $service, $opportunityType = 'retest')
    {
        /** @var Opportunity $opportunity */
        $opportunity = $this->opportunityManager->getNowExistingOpportunity($service, $opportunityType);

        if (empty($opportunity)) { // Create new Opportunity and attach new opportunity to service
            $opportunity = $this->opportunityManager->creating($service, $opportunityType);
            $this->opportunityManager->create($opportunity);
        }

        if ($this->opportunityManager->isPastDue($opportunity)) {
            $this->opportunityManager->setStatus($opportunity, $this->opportunityManager::STATUS_PAST_DUE);
        }

        $service->setOpportunity($opportunity);
        $opportunity->addService($service);
        $opportunity = $this->opportunityManager->updateSomeFields($opportunity);
        $this->opportunityManager->update($opportunity);

        /** @var AccountManager $accountManager */
        $accountManager = $this->container->get('app.account.manager');
        $accountManager->refreshOldestOpportunityDate($opportunity->getAccount());

        $service->setOpportunity($opportunity);
        $service = $this->setStatus($service, $this->opportunityAndServiceStatuses[$opportunityType]);
        $this->update($service);
    }

    /**
     * @param Service $service
     * @return bool
     */
    public function resetOpportunity(Service $service)
    {
        $opportunity = $service->getOpportunity();
        if (!$opportunity) {
            return false;
        }

        $opportunity->removeService($service);
        $this->objectManager->persist($opportunity);

        $service->setOpportunity();
        $this->objectManager->persist($service);

        if (count($opportunity->getServices()) == 0) {
            $this->objectManager->remove($opportunity);
        } else {
            $this->opportunityManager->updateSomeFields($opportunity);
            $this->opportunityManager->update($opportunity);
        }
        $this->objectManager->flush();
        $this->container->get('app.account.manager')->refreshOldestOpportunityDate($opportunity->getAccount());

        return true;
    }

    /**
     * @param DepartmentChannel $DepartmentChannel
     */
    public function setDepartmentChannel(DepartmentChannel $DepartmentChannel)
    {
        $services = $this->getServicesByDepartmentChannel($DepartmentChannel);

        if (!empty($services)) {
            /** @var Service $service */
            foreach ($services as $service) {
                $service->setDepartmentChannel($DepartmentChannel);
                $this->objectManager->persist($service);
            }
            $this->objectManager->flush();
        }
    }

    /**
     * @param DepartmentChannel $DepartmentChannel
     * @return array
     */
    public function getServicesByDepartmentChannel(DepartmentChannel $DepartmentChannel)
    {
        $accountsIds = [];
        /** @var DeviceCategory $division */
        $division = $DepartmentChannel->getDepartment()->getDivision();

        /** @var Account $account */
        foreach ($DepartmentChannel->getDepartment()->getMunicipality()->getAccount() as $account) {
            $accountsIds[] = $account->getId();
        }

        return $this->serviceRepository->getByAccountDivision($division, $accountsIds);
    }

    /**
     * @param Service $service
     * @param string $status
     * @return Service
     */
    public function setStatus(Service $service, string $status)
    {
        $serviceStatus = $this->serviceStatusRepository->findOneBy(['alias' => $status]);
        $service->setStatus($serviceStatus);

        return $service;
    }

    /**
     * @param Service $service
     * @return bool
     */
    public function isDone(Service $service)
    {
        return $service->getInspectionDue() > new \DateTime('+ 75 days');
    }

    /**
     * @param Service[] $services
     * @return mixed
     */
    public function detachProposalForCollection($services)
    {
        /** @var Service $service */
        foreach ($services as $key => $service) {
            $service->setProposal(null);
            $services[$key] = $service;
        }

        return $services;
    }

    /**
     * @param $servicesCollection
     */
    public function createOpportunityByService($servicesCollection)
    {
        /** @var Service $service */
        foreach ($servicesCollection as $service) {
            /** @var ServiceNamed $serviceNamed */
            $serviceNamed = $service->getNamed();

            if ($serviceNamed->getFrequency()) {
                $serviceTypeAlias = 'retest';
            } else {
                $serviceTypeAlias = 'repair';
            }

            $this->attachOpportunity($service, $serviceTypeAlias);
        }
    }

    /**
     * @param Service $service
     * @return Service
     */
    public function resetProposal(Service $service)
    {
        $service->setProposal(null);

        return $service;
    }

    /**
     * @param Service $service
     * @return Service
     */
    public function unlinkServiceFromProposalAndResetStatus(Service $service)
    {
        $service = $this->resetProposal($service);
        $service = $this->setStatus(
            $service,
            self::STATUS_RETEST_OPPORTUNITY
        );

        return $service;
    }

    /**
     * @param Service $service
     * @return Service
     */
    public function resetServiceToNextTime(Service $service)
    {
        /** @var ServiceFrequency $serviceFrequency */
        $serviceFrequency = $service->getNamed()->getFrequency();
        if (!empty($serviceFrequency) and $serviceFrequency->getAlias() != "non_repeatable") {
            /** @var \DateTime $dueDate */
            $dueDate = $service->getInspectionDue();
            $toDay = new \DateTime('now');

            if (strtotime($dueDate->format('Y-m-d')) > strtotime($toDay->format('Y-m-d'))) {
                $newDueDate = $toDay->modify("+".$serviceFrequency->getName());
            } else {
                $newDueDate = clone($dueDate);
                $newDueDate = $newDueDate->modify("+".$serviceFrequency->getName());
            }

            $service->setInspectionDue($newDueDate);
            $service->setTestDate($newDueDate);
        }

        return $service;
    }

    /**
     * @param Device $device
     * @return array
     */
    public function getServiceHistoryWithWorkorderByDevice(Device $device)
    {
        return $this->serviceHistoryRepository->getServiceHistoryWithWorkorderByDevice($device);
    }

    /**
     * @param Service $service
     */
    public function omit(Service $service)
    {
        $this->resetOpportunity($service);
        $this->setStatus($service, self::STATUS_OMITTED);
        $this->save($service);
    }

    /**
     * @param Service $service
     */
    public function discard(Service $service)
    {
        $service = $service->setComment(null);
        $this->resetOpportunity($service);
        $this->resetProposal($service);
        $this->setStatus($service, self::STATUS_DISCARDED);
        $this->save($service);

        /** @var ServiceNamed $serviceNamed */
        $serviceNamed = $service->getNamed();

        if ($serviceNamed->getFrequency()) {
            $serviceTypeAlias = 'retest';
            $this->attachOpportunity($service, $serviceTypeAlias);
        }
    }

    /**
     * @param Service $service
     */
    public function restore(Service $service)
    {
        $frequency = $service->getNamed()->getFrequency();

        if (!empty($frequency)) {
            $this->attachOpportunity($service, self::TYPE_RETEST_SERVICE);
        } else {
            $service->setInspectionDue(new \DateTime());
            $this->attachOpportunity($service, self::TYPE_REPAIR_SERVICE);
        }
    }

    /**
     * @param Device $device
     * @return array
     */
    public function getNonFrequencyServiceNames(Device $device)
    {
        $serviceNameRepository = $this->objectManager->getRepository('AppBundle:ServiceNamed');
        $result = [];

        /** @var Service $service */
        foreach ($device->getServices() as $service) {
            if (in_array($service->getStatus()->getAlias(), ['discarded', 'resolved'])) {
                continue;
            }

            $result[] = $service->getNamed()->getId();
        }

        return $serviceNameRepository->getNamesByDeviceName($device, $result);
    }

    /**
     * @param Device $device
     * @param ServiceNamed $serviceNamed
     * @return Service|null|object
     */
    public function getDiscardAndResolvedService(Device $device, ServiceNamed $serviceNamed)
    {
        $serviceStatusDiscarded = $this->objectManager->getRepository('AppBundle:ServiceStatus')
            ->findOneBy(['alias' => self::STATUS_DISCARDED]);
        $serviceStatusResolved = $this->objectManager->getRepository('AppBundle:ServiceStatus')
            ->findOneBy(['alias' => self::STATUS_RESOLVED]);

        return $this->objectManager->getRepository('AppBundle:Service')->findOneBy(
            [
                'device' => $device,
                'named' => $serviceNamed,
                'status' => [$serviceStatusDiscarded, $serviceStatusResolved]
            ]
        );
    }

    /**
     * @param Device $device
     * @param ServiceNamed $serviceNamed
     * @return Service|null|object
     */
    public function getOmitedService(Device $device, ServiceNamed $serviceNamed)
    {
        $serviceStatusOmited = $this->objectManager->getRepository('AppBundle:ServiceStatus')
            ->findOneBy(['alias' => self::STATUS_OMITTED]);

        return $this->objectManager->getRepository('AppBundle:Service')->findOneBy(
            [
                'device' => $device,
                'named' => $serviceNamed,
                'status' => [$serviceStatusOmited]
            ]
        );
    }


    /**
     * @param Service $service
     * @param Report $report
     * @param bool $needsResetServices
     */
    public function finishServiceActions(Service $service, Report $report, $needsResetServices = true)
    {
        /** @var Contractor $myCompanyContractor */
        $myCompanyContractor = $this->contractorRepository->getMyCompanyContractor();

        $service->setCompanyLastTested($myCompanyContractor);
        $service->setLastReport($report);

        if ($needsResetServices) {
            $this->resetServiceToNextTime($service);
        }

        $serviceFrequency = $service->getNamed()->getFrequency();
        if (!empty($serviceFrequency)) {
            $service->setLastTested(new \DateTime);
        }

        $this->update($service);
    }

    /**
     * @param Service $service
     */
    public function setResolvedStatus(Service $service)
    {
        $service->setComment(null);
        $this->setStatus($service, self::STATUS_RESOLVED);
        $this->save($service);
    }

    /**
     * @param Account $account
     * @return array
     */
    public function getAccountInspectionsNamed(Account $account)
    {
        $named = array();

        if ($account->getType()->getAlias() == "group account") {
            $inspections = $this->getInspectionsByGroupAccount($account);
        } else {
            $inspections = $this->getInspectionsByAccount($account);
        }

        /** @var Service $inspection */
        foreach ($inspections as $inspection) {
            $named[] = $inspection->getNamed();
        }

        return $named;
    }

    /**
     * @param Account $account
     * @param array $data
     */
    public function changeInspectionsInformation(Account $account, array $data)
    {
        /** @var ServiceNamed $name */
        $name = $data['named'];
        $fixedPrice = $data['fixedPrice'];
        $inspectionDue = $data['inspectionDue'];
        $lastTested = $data['lastTested'];
        $companyLastTested = $data['companyLastTested'];

        if ($account->getType()->getAlias() == "group account") {
            $inspections = $this->getInspectionsByGroupAccountAndName($account, $name);
        } else {
            $inspections = $this->getInspectionsByAccountAndName($account, $name);
        }

        /** @var Service $inspection */
        foreach ($inspections as $inspection) {
            if ($fixedPrice) {
                $inspection->setFixedPrice($fixedPrice);
            }

            if ($inspectionDue) {
                $inspection->setInspectionDue($inspectionDue);
                $this->resetOpportunity($inspection);
                $this->attachOpportunity($inspection);
            }

            if ($lastTested) {
                $inspection->setLastTested($lastTested);
            }

            if ($companyLastTested) {
                $inspection->setCompanyLastTested($companyLastTested);
            }

            $this->update($inspection);
        }
    }

    /**
     * @param Account $account
     * @return array
     */
    public function getInspectionsByAccount(Account $account)
    {
        $inspections = $this->serviceRepository->getInspectionsByAccount(
            $account,
            self::STATUS_RETEST_OPPORTUNITY
        );

        return $inspections;
    }

    /**
     * @param Account $account
     * @return array
     */
    public function getInspectionsByGroupAccount(Account $account)
    {
        $accounts = $this->accountRepository->findBy([
            'deleted' => false,
            'parent' => $account
        ]);

        $inspections = $this->serviceRepository->getInspectionsByGroupAccountChildren(
            $accounts,
            self::STATUS_RETEST_OPPORTUNITY
        );

        return $inspections;
    }

    /**
     * @param Account $account
     * @param ServiceNamed $name
     * @return array
     */
    public function getInspectionsByAccountAndName(Account $account, ServiceNamed $name)
    {
        $inspections = $this->serviceRepository->getInspectionsByAccountAndName(
            $account,
            $name,
            self::STATUS_RETEST_OPPORTUNITY
        );

        return $inspections;
    }

    /**
     * @param Account $account
     * @param ServiceNamed $name
     * @return array
     */
    public function getInspectionsByGroupAccountAndName(Account $account, ServiceNamed $name)
    {
        $accounts = $this->accountRepository->findBy([
            'deleted' => false,
            'parent' => $account
        ]);

        $inspections = $this->serviceRepository->getInspectionsByGroupAccountAndName(
            $accounts,
            $name,
            self::STATUS_RETEST_OPPORTUNITY
        );

        return $inspections;
    }

    /**
     * @param Account $account
     */
    public function setNewDepartmentChannelForAllServicesByAccount(Account $account)
    {
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->container->get('app.department_channel.manager');
        $services = $this->serviceRepository->getServicesByAccount($account);

        /** @var Service $service */
        foreach ($services as $service) {
            $service->setDepartmentChannel($departmentChannelManager->getChannelByDivision($service));
            $this->serviceHistoryCreator->create($service);
        }
    }
}
