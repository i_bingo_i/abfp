<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Department;
use AppBundle\Entity\Agent;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Events\DepartmentCreatedEvent;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DepartmentManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AgentManager */
    private $agentManager;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var  EventDispatcherInterface */
    private $dispatcher;

    /**
     * DepartmentManager constructor.
     * @param ObjectManager $objectManager
     * @param AgentManager $agentManager
     */
    public function __construct(
        ObjectManager $objectManager,
        AgentManager $agentManager,
        EventDispatcherInterface $dispatcher
    ) {
        $this->objectManager = $objectManager;
        $this->agentManager = $agentManager;
        $this->dispatcher = $dispatcher;

        $this->deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
    }

    /**
     * @param Department $department
     */
    public function save(Department $department)
    {
        $this->objectManager->persist($department);
        $this->objectManager->flush();
    }

    /**
     * @param Department $department
     * @return Department
     */
    public function serialize(Department $department)
    {
        /** @var Agent $agent */
        $agent = $department->getAgent();

        $agentContacts = [];
        if ($agent) {
            $agentContacts = $this->agentManager->getContactsByDivision($agent, $department->getDivision());
        }
        $department->agentContacts = $agentContacts;

        return $department;
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $deviceCategory
     */
    public function create(Municipality $municipality, DeviceCategory $deviceCategory)
    {
        $department = new Department();
        $department->setMunicipality($municipality);
        $department->setDivision($deviceCategory);
        $department->setName($this->createNameByMunicipality($municipality, $deviceCategory));
        $this->save($department);

        /** @var DepartmentCreatedEvent $event */
        $event = new DepartmentCreatedEvent($department);
        $this->dispatcher->dispatch('department.created', $event);
    }

    /**
     * @param Municipality $municipality
     */
    public function createForNewMunicipality(Municipality $municipality)
    {
        $divisionStatusesAlias = ["backflow", "fire", "plumbing"];
        $divisions = $this->deviceCategoryRepository->getByStatusesAlias($divisionStatusesAlias);

        /** @var DeviceCategory $division */
        foreach ($divisions as $division) {
            $this->create($municipality, $division);
        }
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $deviceCategory
     * @return string
     */
    private function createNameByMunicipality(Municipality $municipality, DeviceCategory $deviceCategory)
    {
        $deviceCategoryName = $deviceCategory->getName();
        $municipalityName = $municipality->getName();

        return $municipalityName . " " . $deviceCategoryName . " " . "Department";
    }
}
