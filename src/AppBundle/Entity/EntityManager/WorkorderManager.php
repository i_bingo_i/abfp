<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountHistory;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceHistory;
use AppBundle\Entity\Event;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\AccountHistoryRepository;
use AppBundle\Entity\Repository\DeviceHistoryRepository;
use AppBundle\Entity\Repository\EventRepository;
use AppBundle\Entity\Repository\PaymentMethodRepository;
use AppBundle\Entity\Repository\ProposalRepository;
use AppBundle\Entity\Repository\ReportRepository;
use AppBundle\Entity\Repository\ServiceHistoryRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use AppBundle\Entity\ServiceStatus;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderFrozenContent;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Entity\WorkorderStatus;
use AppBundle\Events\WorkorderCreatedEvent;
use AppBundle\Services\Account\AccountHistoryService;
use AppBundle\Services\AccountContactPersonHistory\Creator;
use WorkorderBundle\Services\DeviceInfoService;
use WorkorderBundle\Services\Freeze;
use WorkorderBundle\Services\WorkorderService;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Entity\Repository\ServiceStatusRepository;
use AppBundle\Entity\Repository\WorkorderFrozenContentRepository;
use AppBundle\Services\Workorder\PDFManager;
use WorkorderBundle\Services\LogCreator;
use WorkorderBundle\Services\LogMessage;

class WorkorderManager
{
    public const STATUS_SEND_BILL_CREATE_INVOICE = 'send_bill_create_invoice';
    public const STATUS_PENDING_PAYMENT = 'pending_payment';
    public const STATUS_SEND_REPORTS = 'send_reports';
    public const STATUS_TO_BE_SCHEDULED = 'to_be_scheduled';
    public const STATUS_SCHEDULED = 'scheduled';
    public const STATUS_TO_BE_DONE_TODAY = 'to_be_done_today';
    public const STATUS_DONE = 'done';
    public const STATUS_NOT_COMPLETED = 'not_completed';
    public const STATUS_DELETE = 'delete';
    public const STATUSES_FOR_FINDING_EXISTING_WO = [
        'to_be_scheduled', 'scheduled', 'to_be_done_today', 'reschedule', 'not_completed', 'send_bill_create_invoice'
    ];
    public const STATUS_RESCHEDULE = 'reschedule';
    public const STATUS_SEND_INVOICE = 'send_invoice';
    public const PAYMENT_METHOD_BILL = 'bill';
    public const TYPE_SIMPLE = 'simple_workorder';
    public const TYPE_GENERIC = 'generic_workorder';

    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $container;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var  WorkorderStatusRepository */
    private $workorderStatusRepository;
    /** @var  ServiceHistoryRepository */
    private $serviceHistoryRepository;
    /** @var WorkorderLogManager|object  */
    private $workorderLogManager;
    /** @var AccountHistoryRepository  */
    private $accountHistoryRepository;
    /** @var  DeviceHistoryRepository */
    private $deviceHistoryRepository;
    /** @var  AccountHistoryManager */
    private $accountHistoryManager;
    /** @var  DeviceHistoryManager */
    private $deviceHistoryManager;
    /** @var  DeviceManager */
    private $deviceManager;
    /** @var  AccountHistoryRepository */
    private $accountContactPersonHistoryRepository;
    /** @var  AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var EventRepository */
    private $eventRepository;
    /** @var WorkorderService */
    private $workorderService;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var PaymentMethodRepository */
    private $paymentMethodRepository;
    /** @var ProposalRepository */
    private $proposalRepository;
    /** @var DeviceInfoService */
    private $workorderDeviceInfoService;
    /** @var ServiceStatusRepository */
    private $serviceStatusRepository;
    /** @var WorkorderFrozenContentRepository */
    private $workorderFrozenContentRepository;
    /** @var PDFManager */
    private $woPdfManager;
    /** @var ReportRepository */
    private $reportsRepository;
    /** @var Creator */
    private $accountContactPersonHistoryCreatorService;
    /** @var Freeze */
    private $woFreeze;
    /** @var AccountHistoryService */
    private $accountHistoryService;

    /**
     * WorkorderManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher
    ) {
        $this->objectManager = $objectManager;
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->serviceManager = $this->container->get('app.service.manager');
        $this->proposalManager = $this->container->get('app.proposal.manager');
        $this->proposalLogManager = $this->container->get('app.proposal_log.manager');
        $this->workorderLogManager = $this->container->get('app.workorder_log.manager');
        $this->accountHistoryManager = $this->container->get('app.account_history.manager');
        $this->deviceManager = $this->container->get('app.device.manager');
        $this->deviceHistoryManager = $this->container->get('app.device_history.manager');
        $this->accountContactPersonHistoryCreatorService =
            $this->container->get('app.account_contact_person_history_creator.service');
        $this->workorderStatusRepository = $this->objectManager->getRepository("AppBundle:WorkorderStatus");
        $this->serviceHistoryRepository = $this->objectManager->getRepository("AppBundle:ServiceHistory");
        $this->accountHistoryRepository = $this->objectManager->getRepository("AppBundle:AccountHistory");
        $this->deviceHistoryRepository = $this->objectManager->getRepository("AppBundle:DeviceHistory");
        $this->accountContactPersonRepository = $this->objectManager->getRepository("AppBundle:AccountContactPerson");
        $this->accountContactPersonHistoryRepository = $this->objectManager->getRepository(
            "AppBundle:AccountContactPersonHistory"
        );
        $this->reportsRepository = $this->objectManager->getRepository("AppBundle:Report");
        $this->eventRepository = $this->objectManager->getRepository("AppBundle:Event");
        $this->serviceStatusRepository = $this->objectManager->getRepository("AppBundle:ServiceStatus");
        $this->workorderService = $this->container->get('workorder.workorder_service.service');
        $this->workorderRepository = $this->objectManager->getRepository("AppBundle:Workorder");
        $this->paymentMethodRepository = $this->objectManager->getRepository("AppBundle:PaymentMethod");
        $this->proposalRepository = $this->objectManager->getRepository("AppBundle:Proposal");
        $this->workorderFrozenContentRepository =
            $this->objectManager->getRepository("AppBundle:WorkorderFrozenContent");
        $this->workorderDeviceInfoService = $this->container->get('workorder.device_info.service');
        $this->woPdfManager = $this->container->get('workorder.pdf.manager');
        $this->woFreeze = $this->container->get('workorder.freeze.service');
        $this->accountHistoryService = $this->container->get('app.account_history.service');
    }

    /**
     * @param Workorder $workorder
     */
    public function save(Workorder $workorder)
    {
        $this->objectManager->persist($workorder);
        $this->objectManager->flush();
    }

    /**
     * @param Workorder $workorder
     */
    public function create(Workorder $workorder)
    {
        $this->save($workorder);
        /** @var WorkorderCreatedEvent $event */
        $event = new WorkorderCreatedEvent($workorder);
        $this->dispatcher->dispatch('workorder.create', $event);
    }

    /**
     * @param Workorder $workorder
     */
    public function update(Workorder $workorder)
    {
        $this->save($workorder);
    }

    /**
     * @param Proposal $proposal
     * @return Workorder
     */
    public function setStartWorkorderProperties(Proposal $proposal)
    {
        $workorderNewStatus = $this->workorderStatusRepository->findOneBy(['alias' => 'to_be_scheduled']);
        $paymentMethod = $this->paymentMethodRepository->findOneBy(['alias' => 'bill']);

        $workOrder = new Workorder();
        $workOrder->setAccount($proposal->getAccount());
        $workOrder->setDivision($proposal->getDivision());
        $workOrder->setPaymentMethod($paymentMethod);
        $workOrder->addProposal($proposal);
        $workOrder->setStatus($workorderNewStatus);

        $this->create($workOrder);

        return $workOrder;
    }

    /**
     * @param $services
     * @param Workorder $workorder
     * @return Workorder
     */
    public function addServices($services, Workorder $workorder)
    {
        /** @var Service $service */
        foreach ($services as $service) {
            $statusAlias = $service->getStatus()->getAlias();
            if ($statusAlias == $this->serviceManager::STATUS_UNDER_RETEST_NOTICE
                || $statusAlias == $this->serviceManager::STATUS_UNDER_PROPOSAL
            ) {
                $this->addBasicService($service, $workorder);
                $this->serviceManager->update($service);
            }
        }
        $this->objectManager->persist($workorder);

        return $workorder;
    }

    /**
     * $fromAPI need for check if we use this method from API
     *
     * @param $services
     * @param Workorder $workorder
     * @param bool $fromAPI
     * @return Workorder
     */
    public function addUnderOpportunityServices($services, Workorder $workorder, $fromAPI = false)
    {
        /** @var Service $service */
        foreach ($services as $service) {
            $statusAlias = $service->getStatus()->getAlias();

            if ((($statusAlias == $this->serviceManager::STATUS_RETEST_OPPORTUNITY
                OR $statusAlias == $this->serviceManager::STATUS_REPAIR_OPPORTUNITY) AND !$fromAPI) OR
                (($statusAlias == $this->serviceManager::STATUS_RETEST_OPPORTUNITY
                 OR $statusAlias == $this->serviceManager::STATUS_REPAIR_OPPORTUNITY
                 OR $statusAlias == $this->serviceManager::STATUS_OMITTED
                ) AND $fromAPI
                )
            ) {
                $this->addBasicService($service, $workorder);
                $service = $service->setDirectlyWO(true);
                $service = $service->setAddedFromIos($fromAPI);
                $this->serviceManager->resetOpportunity($service);
                $this->serviceManager->update($service);
            }
        }
        $this->objectManager->persist($workorder);

        return $workorder;
    }

    /**
     * @param Service $service
     * @param Workorder $workorder
     */
    private function addBasicService(Service $service, Workorder $workorder)
    {
        /** @var ServiceStatus $underWorkorderStatus */
        $underWorkorderStatus = $this->serviceStatusRepository->findOneBy([
            'alias' => $this->serviceManager::STATUS_UNDER_WORKORDER
        ]);
        $workorder->addService($service);
        $service->setWorkorder($workorder);
        $service->setStatus($underWorkorderStatus);
    }

    /**
     * @param $services
     */
    public function setServicesStatus($services)
    {
        /** @var Service $service */
        foreach ($services as $service) {
            $this->serviceManager->setStatus($service, "under_workorder");
            $this->serviceManager->update($service);
        }
    }

    /**
     * @deprecated This method will be removed in the following releases.
     * @param Workorder $workorder
     * @param $statusAlias
     * @return bool
     */
    public function setStatus(Workorder $workorder, $statusAlias)
    {
        /** @var WorkorderStatus $workorderNewStatus */
        $workorderNewStatus = $this->workorderStatusRepository->findOneBy(['alias' => $statusAlias]);
        /** @var WorkorderStatus $workorderCurrentStatus */
        $workorderCurrentStatus = $workorder->getStatus();
        $isChangingStatus = false;

        // TODO: Дуже складно читати і розуміти даний кусок коду
        if (!empty($workorderNewStatus)
            &&
            (
                ($workorderNewStatus->getPriority() > $workorderCurrentStatus->getPriority())
                ||
                (
                    $workorderCurrentStatus->getAlias() === self::STATUS_NOT_COMPLETED
                    &&
                    (
                        $workorderNewStatus->getAlias() === self::STATUS_TO_BE_DONE_TODAY
                        ||
                        $workorderNewStatus->getAlias() === self::STATUS_SCHEDULED
                    )
                )
            )
        ) {
            $workorder->setStatus($workorderNewStatus);
            $isChangingStatus = true;
        }

        // TODO: В зв'язку з тим що умову з визначення статусу складно читати, нам щоб змінити логіку довелося створити ще одну умову котра буде виконуватися після першої. Потрібно це відрефакторити
        $workorderCurrentStatusAlias = $workorderCurrentStatus->getAlias();
        if ($workorderCurrentStatusAlias == "reschedule" and $statusAlias == "scheduled") {
            $workorder->setStatus($workorderNewStatus);
            $isChangingStatus = true;
        }

        return $isChangingStatus;
    }

    /**
     * @param Workorder $workorder
     * @param $statusAlias
     */
    public function rescheduleFromAPI(Workorder $workorder, $statusAlias)
    {
        $workorderNewStatus = $this->workorderStatusRepository->findOneBy(['alias' => $statusAlias]);
        /** @var WorkorderStatus $workorderCurrentStatus */
        $workorderCurrentStatus = $workorder->getStatus();

        if ($workorderNewStatus->getAlias() === self::STATUS_RESCHEDULE and
            !in_array($workorderCurrentStatus->getAlias(), [
                self::STATUS_PENDING_PAYMENT,
                self::STATUS_SEND_REPORTS,
                self::STATUS_DELETE,
                self::STATUS_DONE
            ])) {
            $workorder->setStatus($workorderNewStatus);
        }
    }

    /**
     * @param Workorder $workorder
     * @param $statusAlias
     * @return Workorder
     */
    public function setStatusWithoutVerification(Workorder $workorder, $statusAlias)
    {
        /** @var WorkorderStatus $workorderNewStatus */
        $workorderNewStatus = $this->workorderStatusRepository->findOneBy(['alias' => $statusAlias]);

        $workorder->setStatus($workorderNewStatus);
        return $workorder;
    }

    /**
     * @param Workorder $workorder
     * @return array
     */
    public function getGroupingServices(Workorder $workorder)
    {
        $groupingServices = [];
        $frozenItems = $workorder->getFrozenContent();
        $services = $workorder->getServices();
        if ($workorder->getIsFrozen()) {
            $groupingServices = $this->getGroupingServicesForFrozenContent($frozenItems);
        } elseif (!empty($services)) {
            $groupingServices = $this->getGroupingServicesForRealServices($services);
        }

        return $groupingServices;
    }

    /**
     * @param Collection $services
     * @return mixed
     */
    public function getGroupingServicesForRealServices(Collection $services)
    {
        $groupingServices = [];
        /** @var Service $service */
        foreach ($services as $service) {
            /** @var Account $account */
            $account = $service->getAccount();
            if (empty($groupingServices[$account->getId()])) {
                $groupingServices[$account->getId()]['account'] = $account;
            }

            /** @var Device $device */
            $device = $service->getDevice();
            /** @var DeviceManager $deviceManager */

            if (empty($groupingServices[$account->getId()]['devices'][$device->getId()])) {
                $groupingServices[$account->getId()]['devices'][$device->getId()]['device'] = $this->deviceManager->serialize($device);
            }
            $groupingServices[$account->getId()]['devices'][$device->getId()]['services'][] = $service;
            $groupingServices[$account->getId()]['devices'][$device->getId()][$service->getProposal()->getType()->getAlias()] = true;
        }

        return $groupingServices;
    }

    /**
     * @param Collection $frozenItems
     * @return array
     */
    public function getGroupingServicesForFrozenContent(Collection $frozenItems)
    {
        $groupingServices = [];
        /** @var WorkorderFrozenContent $frozenItem */
        foreach ($frozenItems as $frozenItem) {
            /** @var AccountHistory $accountHistory */
            $accountHistory = $frozenItem->getAccountHistory();
            $ownerAccount = $accountHistory->getOwnerEntity();
            $ownerAccountId = $ownerAccount->getId();
            if (empty($groupingServices[$ownerAccountId])) {
                $groupingServices[$ownerAccountId]['account'] = $accountHistory;
            }
            /** @var DeviceHistory $deviceHistory */
            $deviceHistory = $frozenItem->getDeviceHistory();
            $ownerDevice = $deviceHistory->getOwnerEntity();
            $ownerDeviceId = $ownerDevice->getId();
            /** @var ServiceHistory $serviceHistory */
            $serviceHistory = $frozenItem->getServiceHistory();
            $serviceHistory->setMunicipalityFee($frozenItem->getMunicipalityFee());
            $serviceHistory->processingFee = $frozenItem->getProcessingFee();

            /** @var DeviceManager $deviceManager */
            if (empty($groupingServices[$ownerAccountId]['devices'][$ownerDeviceId])) {
                $groupingServices[$ownerAccountId]['devices'][$ownerDeviceId]['device'] =
                    $this->deviceHistoryManager->serialize($deviceHistory);
            }
            $groupingServices[$ownerAccountId]['devices'][$ownerDeviceId]['services'][] = $serviceHistory;

            $proposalTypeAlias = 'retest';
            if (!$serviceHistory->getNamed()->getFrequency()) {
                $proposalTypeAlias = 'repair';
            }
            $groupingServices[$ownerAccountId]['devices'][$ownerDeviceId][$proposalTypeAlias] = true;
        }
        return $groupingServices;
    }

    /**
     * @param Workorder $workorder
     * @return Account|AccountHistory
     */
    public function getAccountForWorkorder(Workorder $workorder)
    {
        $account = $workorder->getAccount();
        if ($workorder->getIsFrozen()) {
            return $this->accountHistoryService->checkAndGetAccountHistoryForFrozeWo($account, $workorder);
        }

        return $account;
    }

    /**
     * @param $workorders
     * @return mixed
     */
    public function processWorkorderFindFrozen($workorders)
    {
        /** @var Workorder $workorder */
        foreach ($workorders as &$workorder) {
            if ($workorder->getIsFrozen()) {
                $workorder = $this->addAccountHistoryItemForFrozenWorkorder($workorder);
            }
        }

        return $workorders;
    }

    /**
     * @param Workorder $workorder
     * @return Workorder
     */
    public function addAccountHistoryItemForFrozenWorkorder(Workorder $workorder)
    {
        $accountHistory = $this->getAccountForWorkorder($workorder);
        $workorder->accountHistory = $accountHistory;

        return $workorder;
    }

    /**
     * @param UploadedFile $image
     * @return bool|string
     */
    public function saveImage(UploadedFile $image)
    {
        $saveImagePath = "/uploads/invoices/";
        return $this->container->get('app.images')->saveUploadImage($image, $saveImagePath);
    }

    /**
     * @param $workorders
     * @return mixed
     */
    public function groupingByScheduleDate($workorders)
    {
        $groupedList = [];
        /** @var Workorder $workorder */
        foreach ($workorders as $workorder) {
            foreach ($this->iterateScheduleByDays($workorder) as $day) {
                $scheduledFromTimestamp = 0;
                if ($day) {
                    $scheduledFromTimestamp = $day->getTimestamp();
                }

                if (!array_key_exists($scheduledFromTimestamp, $groupedList)) {
                    $groupedItem['from'] = $day ? clone $day : $day;
                    $groupedItem['list'] = [];
                    $groupedList[$scheduledFromTimestamp] = $groupedItem;
                }

                $groupedList[$scheduledFromTimestamp]['list'][] = $workorder;
            }
        }

        return $groupedList;
    }

    /**
     * @param Workorder $workorder
     * @return \Generator
     */
    public function iterateScheduleByDays(Workorder $workorder)
    {
        $current = $workorder->getScheduledFrom();
        $to = $workorder->getScheduledTo();

        if ($current) {
            $current->setTime(0, 0, 0, 0);
            $to->setTime(0, 0, 0, 0);
        } else {
            yield $current;
        }

        for (; $current and $current <= $to; $current->modify("+1 day")) {
            yield $current;
        }
    }

    /**
     * @param Workorder $workorder
     * @param array $data
     * @param bool $setStatusWithoutVerification
     * @return bool
     * @throws \Exception
     */
    public function setScheduledDate(Workorder $workorder, array $data, $setStatusWithoutVerification = false)
    {
        $scheduledFrom = new \DateTime($data['startDate'] . ' ' . $data['startTime']);
        $scheduledTo = new \DateTime($data['startDate'] . ' ' . $data['endTime']);
        $status = null;
        $toDayDate = new \DateTime();

        if (empty($workorder->getScheduledFrom())) {
            $workorder->setScheduledFrom($scheduledFrom);
        }

        if (empty($workorder->getScheduledTo())) {
            $workorder->setScheduledTo($scheduledTo);
        }

        if ($scheduledFrom->getTimestamp() < $workorder->getScheduledFrom()->getTimestamp()) {
            $workorder->setScheduledFrom($scheduledFrom);
        }

        if ($scheduledTo->getTimestamp() > $workorder->getScheduledTo()->getTimestamp()) {
            $workorder->setScheduledTo($scheduledTo);
        }

        if (strtotime($toDayDate->format('d-m-Y')) >= strtotime($workorder->getScheduledFrom()->format('d-m-Y'))
            && strtotime($toDayDate->format('d-m-Y')) <= strtotime($workorder->getScheduledTo()->format('d-m-Y'))
        ) {
            $status = self::STATUS_TO_BE_DONE_TODAY;
        }

        // $setStatusWithoutVerification - костыль, с помощью которого мы можем засетить
        // статус без проверки (используется только для админки)
        if ($setStatusWithoutVerification) {
            $this->setStatusWithoutVerification($workorder, $status ?? self::STATUS_SCHEDULED);
            $isChangingStatus = true;
        } else {
            $isChangingStatus = $this->setStatus($workorder, $status ?? self::STATUS_SCHEDULED);
        }

        if ($status == self::STATUS_TO_BE_DONE_TODAY) {
            // Create log record in WO log
            /** @var LogMessage $logMessage */
            $logMessage = $this->container->get('workorder.log_message');
            /** @var LogCreator $woLogCreator */
            $woLogCreator = $this->container->get('workorder.log_creator');
            $woLogCreator->createRecord($workorder, [
                'type' => WorkorderLogType::TYPE_SCHEDULED_DATE_HAS_COME,
                'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_SCHEDULED_DATE_HAS_COME),
                'changingStatus' => $isChangingStatus,
                'author' => 'system'
            ]);

            $isChangingStatus = false;
        }

        return $isChangingStatus;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function isDateValid(array $data)
    {
        $session = $this->container->get('session');
        $scheduledFrom = new \DateTime($data['startDate'] . ' ' . $data['startTime']);
        $scheduledTo = new \DateTime($data['startDate'] . ' ' . $data['endTime']);
        $currentDate = new \DateTime();

        if ($scheduledFrom->getTimestamp() >= $scheduledTo->getTimestamp()) {
            $session->getFlashBag()->add('error', 'Start date can\'t be bigger then finish date');
            return true;
        }

        if (strtotime($scheduledFrom->format('d-m-Y')) < strtotime($currentDate->format('d-m-Y'))) {
            $session->getFlashBag()->add('error', 'Start date can\'t be less than current date');
            return true;
        }

        return false;
    }

    /**
     * @param Workorder $workorder
     * @param Event $event
     * @return bool
     */
    public function checkScheduleDate(Workorder $workorder, Event $event)
    {
        $theEarliestEvent = null;
        $theOldestEvent = null;
        $isExistEventInWorkorder = null;
        $workorderStatusAlias = $workorder->getStatus()->getAlias();
        $isChangedStatusOnToBeScheduled = false;
        $isChangedStatusOnScheduled = false;

        /**
         * integer|null $isExistEventInWorkorder
         * Event|null $theEarliestEvent
         * Event|null $theOldestEvent
         */
        extract($this->workorderService->setScheduledDate($workorder, $event), EXTR_OVERWRITE);

        if ($workorderStatusAlias != 'reschedule') {
            $isChangedStatusOnToBeScheduled = $this->workorderService->setStatusIfEventNotExist(
                $workorder,
                self::STATUS_TO_BE_SCHEDULED,
                $isExistEventInWorkorder
            );

            $isChangedStatusOnScheduled = $this->workorderService->setStatusIfEventByTodayNotExist(
                $workorder,
                self::STATUS_SCHEDULED,
                $isExistEventInWorkorder
            );
        }

        if (empty($isExistEventInWorkorder) || isset($theEarliestEvent) || isset($theOldestEvent)) {
            $this->update($workorder);
        }

        return $isChangedStatusOnToBeScheduled || $isChangedStatusOnScheduled;
    }

    /**
     * @param Workorder $workorder
     * @param Proposal $proposal
     * @return Workorder
     */
    public function setNotIncludedItemsFromProposal(Workorder $workorder, Proposal $proposal)
    {
        $proposalNotIncludedItems = $proposal->getNotIncludedItems();
        $workOrderNotIncludedItems = $workorder->getNotIncludedItems()->toArray();

        foreach ($proposalNotIncludedItems as $item) {
            if (!in_array($item, $workOrderNotIncludedItems)) {
                $workorder->addNotIncludedItem($item);
            }
        }

        return $workorder;
    }

    /**
     * @param Workorder $workorder
     * @return float
     */
    public function calculatePricesForGrandTotal(Workorder $workorder)
    {
        $grandTotalPrice = $workorder->getGrandTotal();
        $additionalServicesCost = $workorder->getAdditionalServicesCost();
        $afterHoursWorkCost = $workorder->getAfterHoursWorkCost();
        $discount = $workorder->getDiscount();

        $result = ($grandTotalPrice + $additionalServicesCost + $afterHoursWorkCost) - $discount;

        return $result;
    }

    /**
     * @param Workorder $workorder
     * @return array
     */
    public function getServicesHistoryWithReportByFrozenWorkorder(Workorder $workorder)
    {
        $servicesHistory = array();

        $frozenContentsWithReports = $this->workorderFrozenContentRepository->getContentWithServicesHistoryReport(
            $workorder
        );

        /** @var WorkorderFrozenContent $content */
        foreach ($frozenContentsWithReports as $content) {
            $servicesHistory[] = $content->getServiceHistory();
        }

        return $servicesHistory;
    }

    /**
     * @param Workorder $workorder
     * @param $comment
     * @throws \Exception
     */
    public function deleteWorkorder(Workorder $workorder, $comment)
    {
        $deletedStatus = $this->workorderStatusRepository->findOneBy([
            'alias' => self::STATUS_DELETE
        ]);

        $workorder->setStatus($deletedStatus);
        $workorder->setScheduledFrom(new \DateTime());
        $workorder->setScheduledTo(new \DateTime());

        if (!$workorder->getIsFrozen()) {
            $this->woFreeze->freezing($workorder);
        }

        $this->update($workorder);

        // Create log record in WO log
        /** @var LogMessage $logMessage */
        $logMessage = $this->container->get('workorder.log_message');
        /** @var LogCreator $woLogCreator */
        $woLogCreator = $this->container->get('workorder.log_creator');
        $woLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_DELETE,
            'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_DELETE),
            'changingStatus' => true,
            'comment' => $comment
        ]);
    }

    /**
     * @param Workorder $workorder
     * @return int
     */
    public function getCountsWOReports(Workorder $workorder)
    {
        $counts = count($this->workorderFrozenContentRepository->getContentWithServicesHistoryReport($workorder));

        if (!$workorder->getIsFrozen()) {
            $counts = count($this->reportsRepository->getWorkOrderReports($workorder));
        }

        return $counts;
    }

    /**
     * @param Workorder $workorder
     * @param $comment
     * @param bool $fromAPI
     * @throws \Exception
     */
    public function reschedule(Workorder $workorder, $comment, $fromAPI = false)
    {
        if ($fromAPI) {
            $this->rescheduleFromAPI($workorder, self::STATUS_RESCHEDULE);
        } else {
            $rescheduleStatus = $this->workorderStatusRepository->findOneBy([
                'alias' => self::STATUS_RESCHEDULE
            ]);

            $workorder->setStatus($rescheduleStatus);
        }

        // Create log record in WO log
        /** @var LogMessage $logMessage */
        $logMessage = $this->container->get('workorder.log_message');
        /** @var LogCreator $woLogCreator */
        $woLogCreator = $this->container->get('workorder.log_creator');
        $woLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_RESCHEDULE,
            'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_RESCHEDULE),
            'comment' => $comment,
            'changingStatus' => true
        ]);
    }
}
