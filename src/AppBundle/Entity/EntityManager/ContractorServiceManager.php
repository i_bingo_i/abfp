<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorService;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\State;
use Doctrine\Common\Persistence\ObjectManager;

class ContractorServiceManager
{
    private const DEFAULT_ILLINOIS_STATE_ALIAS = 'illinois';

    /** @var  ObjectManager */
    private $objectManager;

    /** @var StateRepository */
    private $stateRepository;
    /** @var ContractorRepository */
    private $contractorRepository;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        $this->stateRepository = $this->objectManager->getRepository('AppBundle:State');
        $this->contractorRepository = $this->objectManager->getRepository('AppBundle:Contractor');
    }

    /**
     * @param ContractorService $contractorService
     */
    public function save(ContractorService $contractorService)
    {
        $this->objectManager->persist($contractorService);
        $this->objectManager->flush();
    }

    /**
     * @param ContractorService $contractorService
     * @param ServiceNamed $serviceNamed
     * @param $data
     */
    public function createForRepairService(ContractorService $contractorService, ServiceNamed $serviceNamed, $data)
    {
        $contractorId = $data['contractor'];
        /** @var DeviceNamed $deviceNamed */
        $deviceNamed = $data['deviceNamed'];
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $deviceNamed->getCategory();
        /** @var Contractor $contractor */
        $contractor = $this->contractorRepository->find($contractorId);
        /** @var State $defaultIllinoisState */
        $defaultIllinoisState = $this->stateRepository->findOneBy(['alias' => self::DEFAULT_ILLINOIS_STATE_ALIAS]);

        $contractorService->setContractor($contractor);
        $contractorService->setState($defaultIllinoisState);
        $contractorService->setNamed($serviceNamed);
        $contractorService->setDeviceCategory($deviceCategory);
        $contractorService->setEstimationTime(0);

        $this->save($contractorService);
    }

    /**
     * @param ContractorService $contractorService
     * @param ServiceNamed $serviceNamed
     * @param DeviceNamed $deviceNamed
     */
    public function createForInspectionService(
        ContractorService $contractorService,
        ServiceNamed $serviceNamed,
        DeviceNamed $deviceNamed
    ) {
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $deviceNamed->getCategory();

        $contractorService->setNamed($serviceNamed);
        $contractorService->setDeviceCategory($deviceCategory);
        $this->save($contractorService);
    }

    /**
     * @return ContractorService
     */
    public function prepareCreateForInspectionService()
    {
        /** @var State $defaultState */
        $defaultState = $this->stateRepository->findOneBy(['alias' => 'illinois']);
        /** @var Contractor $myCompanyContractor */
        $myCompanyContractor = $this->contractorRepository->getMyCompanyContractor();

        /** @var ContractorService $contractorService */
        $contractorService = new ContractorService();
        $contractorService->setState($defaultState);
        $contractorService->setContractor($myCompanyContractor);

        return $contractorService;
    }
}
