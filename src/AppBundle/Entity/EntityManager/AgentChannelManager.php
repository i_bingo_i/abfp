<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Agent;
use AppBundle\Entity\Repository\AgentChannelRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\DeviceCategory;
use Doctrine\Common\Persistence\ObjectManager;

class AgentChannelManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AgentChannelRepository */
    private $agentChannelRepository;

    /**
     * AgentChannelManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->agentChannelRepository = $this->objectManager->getRepository('AppBundle:AgentChannel');
    }

    /**
     * @param Agent $agent
     * @return array
     */
    public function getByDeviceCategory(Agent $agent)
    {
        /** @var DeviceCategoryRepository $deviceCategoryRepository */
        $deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');

        $allDeviceCategories = $deviceCategoryRepository->findAll();
        $result = [];

        foreach ($allDeviceCategories as $category) {
            $result[$category->getAlias()]['deviceCategory'] = $category;
            $result[$category->getAlias()]['result'] = $this->agentChannelRepository->findBy(
                [
                    'agent' => $agent->getId(),
                    'deviceCategory' => $category->getId()
                ]
            );
        }
        return $result;
    }
}