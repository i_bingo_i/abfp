<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Proposal;
use AppBundle\Entity\ServiceHistory;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\FrozenServiceForProposal;

class FrozenServiceForProposalManager
{
    /** @var ObjectManager */
    private $objectManager;

    /**
     * FrozenServiceForProposalManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param FrozenServiceForProposal $frozenService
     */
    public function save(FrozenServiceForProposal $frozenService)
    {
        $this->objectManager->persist($frozenService);
        $this->objectManager->flush();
    }

    /**
     * @param ServiceHistory $serviceHistory
     * @param Proposal $proposal
     * @return FrozenServiceForProposal
     */
    public function create(ServiceHistory $serviceHistory, Proposal $proposal)
    {
        /** @var FrozenServiceForProposal $frozenService */
        $frozenService = new FrozenServiceForProposal();
        $frozenService->setServiceHistory($serviceHistory);
        $frozenService->setProposal($proposal);
        $this->save($frozenService);

        return $frozenService;
    }
}
