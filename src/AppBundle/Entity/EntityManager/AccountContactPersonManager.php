<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\ActionManagers\AccountContactPersonActionManger;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\Repository\AccountContactPersonHistoryRepository;
use AppBundle\Entity\Repository\ContactPersonRepository;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Account;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Events\AccountContactPersonSetRolesEvent;
use AppBundle\Events\AccountContactPersonDeletedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use AppBundle\Entity\AccountContactPersonHistory;

class AccountContactPersonManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var  AccountContactPersonHistoryRepository */
    private $accountContactPersonHistoryRepository;
    /** @var  TokenStorage*/
    private $tokenStorage;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var AccountContactPersonActionManger */
    private $accountContactPersonActionManger;

    /**
     * AccountContactPersonManager constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param ObjectManager $objectManager
     * @param TokenStorage $tokenStorage
     * @param AccountContactPersonActionManger $accountContactPersonActionManger
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        ObjectManager $objectManager,
        TokenStorage $tokenStorage,
        AccountContactPersonActionManger $accountContactPersonActionManger
    ) {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;

        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
        $this->accountContactPersonHistoryRepository = $this->objectManager->getRepository(
            'AppBundle:AccountContactPersonHistory'
        );
        $this->tokenStorage = $tokenStorage;
        $this->accountContactPersonActionManger = $accountContactPersonActionManger;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     */
    public function save(AccountContactPerson $accountContactPerson)
    {
        $this->objectManager->persist($accountContactPerson);
        $this->objectManager->flush();
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     */
    public function update(AccountContactPerson $accountContactPerson)
    {
        $this->save($accountContactPerson);
        /** @var AccountContactPersonSetRolesEvent $event */
        $event = new AccountContactPersonSetRolesEvent($accountContactPerson);
        $this->dispatcher->dispatch('contact.set_roles', $event);
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @return AccountContactPerson|ContactPerson|null
     */
    public function isAuthorize(Account $account, DeviceCategory $division)
    {
        /** @var AccountContactPerson $accountContactPerson */
        foreach ($account->getContactPerson() as $accountContactPerson) {
            $responsibilities = $accountContactPerson->getResponsibilities();
            if (!$accountContactPerson->getDeleted() && $responsibilities) {
                /** @var DeviceCategory $responsibility */
                foreach ($responsibilities as $responsibility) {
                    if ($responsibility === $division) {
                        return $accountContactPerson;
                    }
                }
            }
        }

        return null;
    }


    /**
     * @param AccountContactPerson $accountContactPerson
     * @return AccountContactPersonHistory|null|object
     */
    private function getAccountContactPersonHistoryForACP(AccountContactPerson $accountContactPerson)
    {
        return $this->accountContactPersonHistoryRepository->getLastRecord($accountContactPerson);
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @return AccountContactPersonHistory|bool|null|object
     */
    public function getHistoryAuthorizer(Account $account, DeviceCategory $division)
    {
        $accountContactPerson = $this->isAuthorize($account, $division);
        if ($accountContactPerson) {
            return $this->getAccountContactPersonHistoryForACP($accountContactPerson);
        }

        return null;
    }

    /**
     * @param ContactPerson $contactPerson
     * @return bool
     */
    public function deleteAndUnlinkAccountContactPersons(ContactPerson $contactPerson)
    {
        $accountContactPersons = $this->accountContactPersonRepository->findBy([
            "contactPerson" => $contactPerson,
            "deleted" => false
        ]);

        if (!$accountContactPersons) {
            return false;
        }
        /** @var AccountContactPerson $accountContactPerson */
        foreach ($accountContactPersons as $accountContactPerson) {
            $this->accountContactPersonActionManger->resetCustomerToContactPerson($accountContactPerson);
            $this->delete($accountContactPerson);
            $this->unlinkAccountContactPersonFromAccount($accountContactPerson);
        }

        return true;
    }


    /**
     * @param AccountContactPerson $contact
     */
    public function delete(AccountContactPerson $contact)
    {
        $contact->setDeleted(true);
        $this->save($contact);
        /** @var AccountContactPersonDeletedEvent $event */
        $event = new AccountContactPersonDeletedEvent($contact);
        $this->dispatcher->dispatch('contact.deleted', $event);
    }

    /**
     * @param ContactPerson $contactPerson
     */
    public function deleteByContactPerson(ContactPerson $contactPerson)
    {
        $accountContactPerson = $this->accountContactPersonRepository->findBy(['contactPerson' => $contactPerson]);
        if (!empty($accountContactPerson) && is_array($accountContactPerson)) {
            /** @var AccountContactPerson $contact */
            foreach ($accountContactPerson as $contact) {
                $this->delete($contact);
            }
        }
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     */
    public function unlinkAccountContactPersonFromAccount(AccountContactPerson $accountContactPerson)
    {
        $accountContactPerson->setAccount(null);
        $this->save($accountContactPerson);
    }

    /**
     * @param Account $account
     * @return array
     */
    public function accountContactPersonsForWOCreate(Account $account)
    {
        $acp = [];

        $accountContactPersons = $this->accountContactPersonRepository->findBy(
            ['account'=>$account, 'deleted'=>false]
        );

        /** @var AccountContactPerson $accountContactPerson */
        foreach ($accountContactPersons as $key => $accountContactPerson) {
            $name = $accountContactPerson->getContactPerson()->getFirstName() . ' ' .
                $accountContactPerson->getContactPerson()->getLastName();
            $acp[$key]['id'] = $accountContactPerson->getId();
            $acp[$key]['name'] = $name;
        }

        return $acp;
    }
}
