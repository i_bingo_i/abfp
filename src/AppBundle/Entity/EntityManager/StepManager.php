<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Step;
use Doctrine\Common\Persistence\ObjectManager;

class StepManager
{
    /** @var ObjectManager */
    private $objectManager;

    /**
     * StepManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param Step $step
     */
    public function save(Step $step)
    {
        $this->objectManager->persist($step);
        $this->objectManager->flush();
    }
}
