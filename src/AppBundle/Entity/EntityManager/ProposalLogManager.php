<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Letter;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalLog;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProposalLogManager
{
    /** @var ObjectManager  */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;

    /**
     * ProposalLogManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');
    }

    /**
     * @param Proposal $proposal
     * @param $message
     * @param null|string $comment
     * @param Letter|null $letter
     * @param null $author
     */
    public function create(Proposal $proposal, $message, ?string $comment = null, Letter $letter = null, $author = null)
    {
        $contractorUser = null;
        $token = $this->serviceContainer->get('security.token_storage')->getToken();

        if ($token and $author != 'system') {
            $contractorUser = $this->contractorUserRepository->findOneBy(['user' => $token->getUser()]);
        }

        $newProposalLog = new ProposalLog();
        $newProposalLog->setDate(new \DateTime());
        $newProposalLog->setProposal($proposal);
        $newProposalLog->setAuthor($contractorUser);
        $newProposalLog->setComment($comment);
        $newProposalLog->setLetter($letter);
        $newProposalLog->setMessage($message);


        $this->objectManager->persist($newProposalLog);
        $this->objectManager->flush();
    }

    /**
     * @param Proposal $proposal
     * @param Service $service
     */
    public function addInspectionToProposal(Proposal $proposal, Service $service)
    {
        /** @var ServiceNamed $serviceNamed */
        $serviceNamed = $service->getNamed();
        $serviceName = $serviceNamed->getName();
        $message = $serviceName . " " . "was added to Proposal";

        $this->create($proposal, $message);
    }
}
