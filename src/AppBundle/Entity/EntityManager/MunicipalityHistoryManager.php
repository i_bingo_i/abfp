<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Account;
use AppBundle\Entity\Department;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\MunicipalityHistory;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class MunicipalityHistoryManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var TokenStorage */
    private $tokenStorage;

    /**
     * MunicipalityHistoryManager constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorage $tokenStorage
     */
    public function __construct(ObjectManager $objectManager, TokenStorage $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Municipality $municipality
     * @param int $flushFlag
     */
    public function createAccountHistoryItem(Municipality $municipality, $flushFlag = 1)
    {
        $token = $this->tokenStorage->getToken();
        $user = null;
        if (!empty($token) and gettype($token->getUser()) != "string") {
            /** @var User $user */
            $user = $token->getUser();
        }

        /** @var MunicipalityHistory $newMunicipalityHistory */
        $newMunicipalityHistory = new MunicipalityHistory();
        $newMunicipalityHistory->setOwnerEntity($municipality);
        $newMunicipalityHistory->setAuthor($user);

        $newMunicipalityHistory->setName($municipality->getName());
        $newMunicipalityHistory->setAddress($municipality->getAddress());
        $newMunicipalityHistory->setWebsite($municipality->getWebsite());
        $newMunicipalityHistory->setMunicipalityFax($municipality->getMunicipalityFax());
        $newMunicipalityHistory->setMunicipalityPhone($municipality->getMunicipalityPhone());
        $newMunicipalityHistory->setDateSave(new \DateTime());

        $accounts = $municipality->getAccount();
        if ($accounts and !empty($accounts)) {
            /** @var Account $account */
            foreach ($accounts as $account) {
                $newMunicipalityHistory->addAccount($account);
            }
        }
        $departments = $municipality->getDepartment();
        if ($departments and !empty($departments)) {
            /** @var Department $department */
            foreach ($departments as $department) {
                $newMunicipalityHistory->addDepartment($department);
            }
        }

        $this->objectManager->persist($newMunicipalityHistory);
        if ($flushFlag) {
            $this->objectManager->flush();
        }
    }
}
