<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Entity\Letter;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalType;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\ProposalStatusRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use AppBundle\Events\ProposalCloneEvent;
use AppBundle\Events\ProposalCreateEvent;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\DeviceRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use AppBundle\Events\ProposalUpdateEvent;
use AppBundle\Entity\FrozenServiceForProposal;
use AppBundle\Services\Service\EstimationTime;

use AppBundle\Traits\CollectionTrait;

class ProposalManager
{
    use CollectionTrait;

    /** @var ObjectManager */
    private $objectManager;
    /** @var  ServiceRepository */
    private $serviceRepository;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var ProposalStatusRepository */
    private $proposalStatusRepository;
    /** @var $proposalRepository */
    private $proposalRepository;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var AccountRepository */
    private $accountRepository;

    /** @var array */
    public $proposalAndServiceStatuses = [
        'retest' => 'under_retest_notice',
        'repair' => 'under_proposal'
    ];

    /**
     * ProposalManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $serviceContainer;
        $this->serviceManager = $this->serviceContainer->get('app.service.manager');
        $this->dispatcher = $this->serviceContainer->get('event_dispatcher');

        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->proposalRepository = $this->objectManager->getRepository('AppBundle:Proposal');
        $this->deviceRepository = $this->objectManager->getRepository('AppBundle:Device');
        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
        $this->proposalStatusRepository = $this->objectManager->getRepository('AppBundle:ProposalStatus');
    }


    /**
     * @param Proposal $proposal
     */
    public function save(Proposal $proposal)
    {
        $this->objectManager->persist($proposal);
        $this->objectManager->flush();
    }

    /**
     * @param Proposal $proposal
     */
    public function update(Proposal $proposal)
    {
        $this->save($proposal);
        /** @var ProposalUpdateEvent $event */
        $event = new ProposalUpdateEvent($proposal);
        $this->dispatcher->dispatch('proposal.update', $event);
    }

    /**
     * Returns next id of Proposal
     *
     * @return string
     */
    public function generateReferenceId() : string
    {
        $currentDate = strtoupper(date('My'));
        $currentMonthProposalsCountData = $this->proposalRepository->getCurrentMonthProposalsCount();
        $currentMonthProposalsCount = $currentMonthProposalsCountData['currentMonthProposalsCount'];
        $nextMonthProposalId = $currentMonthProposalsCount + 1;
        $referenceId = $currentDate.str_pad($nextMonthProposalId, 4, '0', STR_PAD_LEFT);

        return $referenceId;
    }

    // TODO: NEED REFACTORING
    /**
     * @param Proposal[] $proposals
     * @return array
     */
    public function groupByAccount($proposals)
    {
        $result = [];
        /** @var Proposal $proposal */
        foreach ($proposals as $proposal) {
            $account = $proposal->getAccount();
            $proposal = $this->setCounters($proposal);

            $resultId = $account->getId();
            $result[$resultId]['account'] = $account;
            $result[$resultId]['proposal'][] = $proposal;
        }

        return $result;
    }

    /**
     * @param $proposals
     * @return mixed
     */
    public function setCountersForProposalsArray($proposals)
    {
        foreach ($proposals as $key => $proposal) {
            $proposals[$key] = $this->setCounters($proposal);
        }

        return $proposals;
    }

    // TODO: NEED REFACTORING
    /**
     * @param Proposal $proposal
     * @return Proposal
     */
    private function setCounters(Proposal $proposal)
    {
        $account = $proposal->getAccount();
        if ($account->getType()->getAlias() == 'account') {
            $proposal->countAllServices = $this->serviceRepository->getCountByDivisionForAccount(
                $proposal->getDivision(),
                $account
            );

            $proposal->countAllDevices = $this->deviceRepository->getCountByDivisionForAccount(
                $proposal->getDivision(),
                $account
            );

            $proposal->countAllSites = 1;
        } else {
            $accounts = $this->accountRepository->findBy([
                'deleted' => false,
                'parent' => $account
            ]);

            $proposal->countAllServices = $this->serviceRepository->getCountByDivisionForGroupAccount(
                $proposal->getDivision(),
                $accounts
            );

            $proposal->countAllDevices = $this->deviceRepository->getCountByDivisionForGroupAccount(
                $proposal->getDivision(),
                $accounts
            );

            $proposal->countAllSites = count($accounts);
        }

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     * @return array
     */
    public function groupingServices(Proposal $proposal)
    {
        $groupingServices = [];

        if ($proposal->getIsFrozen()) {
            $services = $proposal->getFrozen()->getFrozenServices();
        } else {
            $services = $proposal->getServices();
        }

        foreach ($services as $service) {
            if ($service instanceof FrozenServiceForProposal) {
                $service = $service->getServiceHistory();
            }

            /** @var Account $account */
            $account = $service->getAccount();
            $accountId = $account->getId();

            if (empty($groupingServices[$accountId])) {
                $groupingServices[$accountId]['account'] = $account;
            }

            /** @var Device $device */
            $device = $service->getDevice();
            /** @var DeviceManager $deviceManager */
            $deviceManager = $currentUser = $this->serviceContainer->get('app.device.manager');
            if (empty($groupingServices[$account->getId()]['devices'][$device->getId()])) {
                $serializedDevice = $deviceManager->serialize($device);

                if (($proposal instanceof Proposal) && $proposal->getType()->getAlias() == 'repair') {
                    $serializedDevice->repairDeviceInfoForThisProp = $device->getRepairDeviceInfo()
                        ->filter(function (RepairDeviceInfo $info) use ($proposal) {
                            $infoProposalId = $info->getProposal() ? $info->getProposal()->getId() : null;

                            return $infoProposalId == $proposal->getId();
                        })->first();
                }

                $groupingServices[$account->getId()]['devices'][$device->getId()]['device'] = $serializedDevice;
            }

            $groupingServices[$account->getId()]['devices'][$device->getId()]['services'][] = $service;
        }

        return $groupingServices;
    }

    /**
     * @param Proposal $proposal
     * @param $proposalStatusAlias
     * @return Proposal
     */
    public function setStatus(Proposal $proposal, $proposalStatusAlias)
    {
        $proposalStatus = $this->proposalStatusRepository->findOneBy(['alias' => $proposalStatusAlias]);
        if ($proposalStatus->getPriority() > $proposal->getStatus()->getPriority()) {
            $proposal->setStatus($proposalStatus);
            //TODO: выпилить отсюда update и перенести его в то место, где вызывается setStatus
            $this->update($proposal);
        }

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     * @return Proposal
     */
    public function setSendingDate(Proposal $proposal)
    {
        if (!$proposal->getFirstSendingDate()) {
            $proposal->setFirstSendingDate(new \DateTime());
        }

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     * @param Letter $letter
     */
    public function sendViaEmail(Proposal $proposal, Letter $letter)
    {
        $status = 'sent_via_email';
        if (in_array($proposal->getStatus()->getAlias(), ['sent_via_mail', 'sent_via_mail_and_email'])) {
            $status = 'sent_via_mail_and_email';
        }

        /** @var ProposalType $proposalTypeAlias */
        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalLogMailMessages = [
            "retest" => "Letter was sent",
            "repair" => "Proposal was sent via email.",
            "custom" => "Proposal was sent via email."
        ];

        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->serviceContainer->get('app.proposal_log.manager');
        $proposalLogManager->create($proposal, $proposalLogMailMessages["$proposalTypeAlias"], '', $letter);

        $this->setStatus($proposal, $status);
        $this->setSendingDate($proposal);
        $this->update($proposal);

        if ($proposal->getLifeCycle()->getAlias() == "retest_notice_life_cycle"
            and $proposal->getStatus()->getPriority() <= 3) {
            $this->serviceContainer->get('app.proposals_processing.service')->checkAutomaticRetestNoticeAction($proposal);
        }
    }

    /**
     * @param Proposal $proposal
     * @param Letter $letter
     */
    public function sendEmailReminder(Proposal $proposal, Letter $letter)
    {
        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->serviceContainer->get('app.proposal_log.manager');
        $proposalLogManager->create($proposal, 'Email reminder was sent', '', $letter);

        $this->setStatus($proposal, 'email_reminder_sent');
        $this->setSendingDate($proposal);
        $this->update($proposal);
    }

    /**
     * @param Proposal $proposal
     */
    public function sendViaMail(Proposal $proposal)
    {
        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->serviceContainer->get('app.proposal_log.manager');

        $status = 'sent_via_mail';
        if (in_array($proposal->getStatus()->getAlias(), ['sent_via_email', 'sent_via_mail_and_email'])) {
            $status = 'sent_via_mail_and_email';
        }

        /** @var ProposalType $proposalTypeAlias */
        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalLogMailMessages = [
            "retest" => "Retest Notice was sent via mail",
            "repair" => "Proposal was sent via mail.",
            "custom" => "Proposal was sent via mail."
        ];

        $proposalLogManager->create($proposal, $proposalLogMailMessages["$proposalTypeAlias"]);
        $this->setSendingDate($proposal);
        $this->setStatus($proposal, $status);
        $this->update($proposal);

        if ($proposal->getLifeCycle()->getAlias() == "retest_notice_life_cycle"
            and $proposal->getStatus()->getPriority() <= 3) {
            $this->serviceContainer->get('app.proposals_processing.service')->checkAutomaticRetestNoticeAction($proposal);
        }
    }

    // TODO: NEED REFACTORING
    /**
     * @param Proposal $proposal
     * @return Proposal
     */
    public function addDepartmentChannel(Proposal $proposal)
    {
        $departmentChannelRepository = $this->objectManager->getRepository('AppBundle:DepartmentChannel');
        $departmentRepository = $this->objectManager->getRepository('AppBundle:Department');
        $department = $departmentRepository->findOneBy(
            [
                'municipality' => $proposal->getAccount()->getMunicipality(),
                'division' => $proposal->getDivision()
            ]
        );
        $departmentChannel = $departmentChannelRepository->findOneBy(['department' => $department, 'isDefault' => 1]);
        $proposal->departmentChannel = $departmentChannel;

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     */
    public function resetServicesToNextTime(Proposal $proposal)
    {
        $services = $proposal->getServices();
        /** @var Service $service */
        foreach ($services as $service) {
            $service = $this->serviceManager->unlinkServiceFromProposalAndResetStatus($service);
            $service = $this->serviceManager->resetServiceToNextTime($service);
            $this->serviceManager->update($service);
            $this->serviceManager->attachOpportunity($service);
        }
    }

    /**
     * @param array $filters
     * @return bool
     */
    public function isShowButtonForMergePDF(array $filters)
    {
        $proposalsByFilter = $this->proposalRepository->findRetestNoticesForMergedPdfByFilters($filters);

        $proposalsForMerge = true;
        if ((
                (array_key_exists('division', $filters) and !empty($filters['division'])) ||
                (array_key_exists('clientType', $filters) and !empty($filters['clientType'])) ||
                (array_key_exists('status', $filters) and !empty($filters['status'])) ||
                (array_key_exists('date', $filters) and (!empty($filters['date']['from']) or !empty($filters['date']['to'])))
            ) && empty($proposalsByFilter)) {
            $proposalsForMerge = false;
        }

        return $proposalsForMerge;
    }

    /**
     * @param Proposal $proposal
     * @return bool
     */
    public function discardAllServices(Proposal $proposal)
    {
        /** @var Service $service */
        foreach ($proposal->getServices() as $service) {
            $this->serviceManager->discard($service);
        }

        return true;
    }
}
