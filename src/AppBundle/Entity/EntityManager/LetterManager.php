<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\AccountContactPersonHistory;
use AppBundle\Entity\Letter;
use AppBundle\Entity\LetterAttachment;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Repository\AccountContactPersonHistoryRepository;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\WorkorderAttachment;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Services\Helper;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Validator\ValidatorInterface as ValidatorInterface;

class LetterManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var object|\Swift_Mailer */
    private $mailer;
    /** @var object|\Symfony\Bundle\TwigBundle\TwigEngine */
    private $templating;
    /** @var Helper */
    private $helperService;
    /** @var object|ValidatorInterface */
    private $validator;

    /**
     * LetterManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;
        $this->mailer = $this->serviceContainer->get('swiftmailer.mailer');
        $this->templating = $this->serviceContainer->get('templating');
        $this->helperService = $this->serviceContainer->get('app.helper');
        $this->validator = $this->serviceContainer->get('validator');
    }

    /**
     * @param Letter $letter
     * @param string $templateName
     * @param $status
     * @return Letter|null
     */
    public function sendEmail(Letter $letter, string $templateName, $status)
    {
        if ($this->send($letter, $templateName, $status) != false) {
            return $this->saveMessage($letter);
        }
        return null;
    }

    /**
     * @param Letter $letter
     * @param string $template
     * @return bool
     */
    public function sendLetter(
        Letter $letter,
        string $template = "@Admin/Letter/content/workorder/send_compose_form_to_authorizer.html.twig"
    ) {
        try {
            $message = \Swift_Message::newInstance();
            $messageSubject = $letter->getSubject();
            $response = false;

            $from = 'mail@backflowandfire.com';
            if ($letter->getFrom()) {
                $from = $letter->getFrom()->getUser()->getEmail();
            }

            $to = $this->getRecipient($letter);

            if ($to) {
                $renderedTemplate = $this->templating->render($template, [
                    'body' => $letter->getBody(),
                    'workorder' => $letter->getWorkorder()
                ]);

                $message->setSubject($messageSubject)
                    ->setFrom($from)
                    ->setTo($to)
                    ->setBody($renderedTemplate, 'text/html');

                $attachments = $letter->getAttachments();

                if($attachments) {
                    /** @var LetterAttachment $attachment */
                    foreach ($attachments as $attachment) {
                        $attachmentPath = __DIR__ . "/../../../../web" . $attachment->getFile()->getFile();
                        $message->attach(\Swift_Attachment::fromPath($attachmentPath));

                    }
                }
                $this->mailer->send($message);
                $response = true;
            }

            return $response;

        } catch (\Exception $exception) {
            $this->serviceContainer->get('session')->getFlashBag()->add('failed', 'All emails are not valid');
        }
    }

    /**
     * @param Letter $letter
     * @param string $templateName
     * @param $status
     * @param string $path
     * @return bool
     */
    public function send(Letter $letter, string $templateName, $status, $path = '@Admin/Letter/content/')
    {
        try {
            $message = \Swift_Message::newInstance();
            $messageSubject = $letter->getSubject();
            $response = false;

            $from = 'mail@backflowandfire.com';
            if ($letter->getFrom()) {
                $from = $letter->getFrom()->getUser()->getEmail();
            }

            $to = $this->getRecipient($letter);

            if ($to) {
                $template = $path . $templateName;
                $renderedTemplate = $this->templating->render($template, [
                    'body' => $letter->getBody(),
                    'proposal' => $letter->getProposal()
                ]);

                $message->setSubject($messageSubject)
                    ->setFrom($from)
                    ->setTo($to)
                    ->setBody($renderedTemplate, 'text/html');

                if ($status !== 'send_past_due_email') {
                    $attachmentPath = __DIR__ . "/../../../../web/uploads/proposals/" . $letter->getAttachment();
                    $message->attach(\Swift_Attachment::fromPath($attachmentPath));
                }

                $this->mailer->send($message);
                $response = true;
            }

            return $response;
        } catch (\Exception $exception) {
            $this->serviceContainer->get('session')->getFlashBag()->add('failed', 'All emails are not valid');
        }
    }

    /**
     * @param string $string
     * @return array
     */
    private function convertingStringToEmailArray(string $string)
    {
        $delimiters = array(",", "|", ":", ";", " ");

        $exploded = $this->helperService->multiExplode($delimiters, $string);

        return $exploded;
    }

    /**
     * @param string $email
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function validationEmailErrorCheck(string $email)
    {
        $emailConstraint = new EmailConstraint();
        $emailConstraint->message = 'Invalid email address';

        $errors = $this->validator->validate(
            $email,
            $emailConstraint
        );

        return $errors;
    }

    /**
     * @param array $emails
     * @return array
     */
    private function prepareSendingEmails(array $emails)
    {
        $result = array();
        foreach ($emails as $key => $value) {
            $trimValue = trim($value);

            $emailErrors = $this->validationEmailErrorCheck($trimValue);

            if ($trimValue and 0 === count($emailErrors)) {
                $result["$trimValue"] = $trimValue;
            }
        }

        return $result;
    }

    /**
     * @param Letter $letter
     * @param array $emails
     */
    private function reSaveEmailsString(Letter $letter, array $emails)
    {
        $newEmailsValue = implode(", ", $emails);

        $letter->setEmails($newEmailsValue);

        $this->objectManager->flush();
    }

    /**
     * @param Letter $letter
     * @return Letter
     */
    public function saveMessage(Letter $letter)
    {
        $this->objectManager->persist($letter);
        $this->objectManager->flush();

        return $letter;
    }

    /**
     * @param Proposal $proposal
     * @param string $proposalStatus
     * @return Letter
     */
    public function prepareLetter(Proposal $proposal, $proposalStatus = '')
    {
        if ($proposal->getIsFrozen()) {
            /** @var AccountContactPerson $recipient */
            $recipient = $proposal->getFrozen()->getRecipient()->getOwnerEntity();
        } else {
            /** @var AccountContactPerson $recipient */
            $recipient = $proposal->getRecipient();
        }

        /** @var AccountContactPersonHistoryRepository $acphRepository */
        $acphRepository = $this->objectManager->getRepository('AppBundle:AccountContactPersonHistory');
        $recipient = $acphRepository->getLastRecord($recipient);

        $contactPersonEmail = $recipient->getContactPerson()->getEmail();

        $newSendMail = new Letter();
        $newSendMail->setTo($recipient);
        $newSendMail->setAttachment($proposalStatus == 'send_past_due_email' ? null : $proposal->getReport());
        $newSendMail->setProposal($proposal);
        $newSendMail->setEmails($contactPersonEmail);

        return $newSendMail;
    }

    /**
     * @param Letter $letter
     * @return array|null
     */
    private function getRecipient(Letter $letter)
    {
        $to = null;

        if ($letter->getEmails()) {
            $emailsArray = $this->convertingStringToEmailArray($letter->getEmails());
            $result = $this->prepareSendingEmails($emailsArray);

            if ($result) {
                $this->reSaveEmailsString($letter, $result);
                $to = $result;
            }
        }

        return $to;
    }
}
