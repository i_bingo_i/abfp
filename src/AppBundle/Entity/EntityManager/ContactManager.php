<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Contact;
use AppBundle\Entity\DeviceCategory;

class ContactManager
{
    /**
     * @param Contact $contact
     * @param DeviceCategory $division
     * @return bool
     */
    public function isDivision(Contact $contact, DeviceCategory $division)
    {
        /** @var DeviceCategory $contactDivision */
        foreach ($contact->getDivisions() as $contactDivision) {
            if ($contactDivision === $division) {
                return true;
            }
        }

        return false;
    }
}
