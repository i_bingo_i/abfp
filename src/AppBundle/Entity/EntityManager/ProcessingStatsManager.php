<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\ProcessingStats;
use AppBundle\Entity\Repository\ProcessingStatsTypeRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProcessingStatsManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var  ProcessingStatsTypeRepository */
    private $processingStatsTypeRepository;

    /**
     * ProcessingStatsManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;
        $this->processingStatsTypeRepository = $this->objectManager->getRepository("AppBundle:ProcessingStatsType");

    }

    /**
     * @param ProcessingStats $processingStats
     */
    public function save(ProcessingStats $processingStats)
    {
        $processingStats->setDateTime(new \DateTime());
        $this->objectManager->persist($processingStats);
        $this->objectManager->flush();
    }

    /**
     * @param ProcessingStats $processingStats
     */
    public function create(ProcessingStats $processingStats)
    {
        $this->save($processingStats);
    }

    /**
     * @param $message
     * @param string $alias
     */
    public function createMessageByAlias($message, $alias = "info")
    {
        $newProcessingStat = new ProcessingStats();
        $newProcessingStatType = $this->processingStatsTypeRepository->findOneBy(["alias" => $alias]);
        $newProcessingStat->setType($newProcessingStatType);
        $newProcessingStat->setMessage($message);
        $this->create($newProcessingStat);
    }
}
