<?php

namespace AppBundle\Entity\EntityManager;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Device;
use AppBundle\Entity\User;
use AppBundle\Entity\DeviceHistory;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\DynamicFieldValueHistory;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class DeviceHistoryManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var TokenStorage */
    private $tokenStorage;

    /**
     * DeviceManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager, TokenStorage $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;

    }

    public function createDeviceHistoryItem(Device $device)
    {

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $newDeviceHistoryItem = new DeviceHistory();
        $newDeviceHistoryItem->setAccount($device->getAccount());
        $newDeviceHistoryItem->setAuthor($user);
        $newDeviceHistoryItem->setOwnerEntity($device);
        $newDeviceHistoryItem->setPhoto($device->getPhoto());
        $newDeviceHistoryItem->setLocation($device->getLocation());
        $newDeviceHistoryItem->setComments($device->getComments());
        $newDeviceHistoryItem->setNoteToTester($device->getNoteToTester());
        $newDeviceHistoryItem->setNamed($device->getNamed());
        $newDeviceHistoryItem->setDateCreate($device->getDateCreate());
        $newDeviceHistoryItem->setDateUpdate(new \DateTime());
        $newDeviceHistoryItem->setDeleted($device->getDeleted());
        $newDeviceHistoryItem->setStatus($device->getStatus());
        $this->objectManager->persist($newDeviceHistoryItem);

        /** @var DynamicFieldValue $field */
        foreach ($device->getFields() as $field) {
            $newFieldValue = new DynamicFieldValueHistory($newDeviceHistoryItem);
            $newFieldValue->setDevice($newDeviceHistoryItem);
            $newFieldValue->setOwnerEntity($field);
            $newFieldValue->setField($field->getField());
            $newFieldValue->setValue($field->getValue());
            $newFieldValue->setOptionValue($field->getOptionValue());
            $newFieldValue->setDateCreate($field->getDateCreate());
            $newFieldValue->setDateUpdate($field->getDateUpdate());
            $this->objectManager->persist($newFieldValue);
            $newDeviceHistoryItem->addField($newFieldValue);
        }
        $this->objectManager->persist($newDeviceHistoryItem);
        $this->objectManager->flush();
    }

    public function serialize(DeviceHistory $deviceHistory)
    {
        $deviceHistory->title = $this->getTitle($deviceHistory);
//
        return $deviceHistory;
    }

    /**
     * @param DeviceHistory $deviceHistory
     * @return string
     */
    public function getTitle(DeviceHistory $deviceHistory)
    {
        $title = $deviceHistory->getNamed()->getName();
        $deviceCategoryAlias = $deviceHistory->getNamed()->getCategory()->getAlias();

        if ($deviceCategoryAlias == 'backflow') {
            /** @var DynamicFieldValueHistory $dynamicFieldValue */
            foreach ($deviceHistory->getFields() as $dynamicFieldValue) {
                if ($dynamicFieldValue->getField()->getAlias() == 'type' and $dynamicFieldValue->getOptionValue()) {
                    $title = $dynamicFieldValue->getOptionValue()->getOptionValue();
                    break;
                }
            }
        }

        return $title;
    }
}
