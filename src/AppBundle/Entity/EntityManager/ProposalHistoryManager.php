<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalHistory;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ProposalHistoryManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var TokenStorage */
    private $tokenStorage;

    /**
     * ProposalHistoryManager constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorage $tokenStorage
     */
    public function __construct(ObjectManager $objectManager, TokenStorage $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Proposal $proposal
     */
    public function createProposalHistoryItem(Proposal $proposal) : void
    {
        $token = $this->tokenStorage->getToken();
        $user = null;
        if (!empty($token)) {
            /** @var User $user */
            $user = $token->getUser();
        }

        /** @var ProposalHistory $newHistoryItem */
        $newHistoryItem = new ProposalHistory();
        $newHistoryItem->setStatus($proposal->getStatus());
        $newHistoryItem->setAccount($proposal->getAccount());
        $newHistoryItem->setType($proposal->getType());
        $newHistoryItem->setDivision($proposal->getDivision());
        $newHistoryItem->setAuthor($user);
        $newHistoryItem->setCreator($proposal->getCreator());
        $newHistoryItem->setDateCreate($proposal->getDateCreate());
        $newHistoryItem->setDateOfProposal($proposal->getDateOfProposal());
        $newHistoryItem->setDateUpdate($proposal->getDateUpdate());
        $newHistoryItem->setDevicesCount($proposal->getDevicesCount());
        $newHistoryItem->setEarliestDueDate($proposal->getEarliestDueDate());
        $newHistoryItem->setGrandTotal($proposal->getGrandTotal());
        $newHistoryItem->setOwnerEntity($proposal);
        $newHistoryItem->setRecipient($proposal->getRecipient());
        $newHistoryItem->setReferenceId($proposal->getReferenceId());
        $newHistoryItem->setReport($proposal->getReport());
        $newHistoryItem->setSendingAddress($proposal->getSendingAddress());
        $newHistoryItem->setClone($proposal->getClone());
        $newHistoryItem->setCloned($proposal->getCloned());
        $newHistoryItem->setSendingAddressDescription($proposal->getSendingAddressDescription());
        $newHistoryItem->setServicesCount($proposal->getServicesCount());
        $newHistoryItem->setServiceTotalFee($proposal->getServiceTotalFee());
        $newHistoryItem->setSitesCount($proposal->getSitesCount());
        $newHistoryItem->setTotalEstimationTime($proposal->getTotalEstimationTime());
        $newHistoryItem->setAfterHoursWorkCost($proposal->getAfterHoursWorkCost());
        $newHistoryItem->setDiscount($proposal->getDiscount());
        $newHistoryItem->setAdditionalCost($proposal->getAdditionalCost());
        $newHistoryItem->setIsMunicipalityFessWillVary($proposal->getIsMunicipalityFessWillVary());
        $newHistoryItem->setAdditionalCostComment($proposal->getAdditionalCostComment());

        $this->objectManager->persist($newHistoryItem);
        $this->objectManager->flush();
    }
}
