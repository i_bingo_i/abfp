<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\AccountHistory;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\Coordinate;
use AppBundle\Entity\Message;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Opportunity;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\AccountHistoryRepository;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\DepartmentChannelRepository;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\OpportunityRepository;
use AppBundle\Entity\Repository\OpportunityTypeRepository;
use AppBundle\Entity\Repository\ProposalRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\User;
use AppBundle\Events\AccountDeletedEvent;
use AppBundle\Events\AccountUpdateContactsEvent;
use AppBundle\Events\AccountUpdatedEvent;
use AppBundle\Services\Account\DivisionsUnderAccount;
use AppBundle\Services\Account\ErrorMessagesService;
use AppBundle\Services\Coordinates\Helper;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Entity\Service;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Services\AccountContactPersonCompose;
use AppBundle\Entity\AccountContactPerson;
use Geocoder\Model\Coordinates;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AppBundle\Events\AccountCreatedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use AppBundle\Entity\Repository\MessageRepository;
use Geocoder\Exception\NoResult;

class AccountManager
{
    public const SERVICE_STATUS_RETEST_OPPORTUNITY = 'under_retest_opportunity';

    /** @var  ObjectManager */
    private $objectManager;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var DeviceManager */
    private $deviceManager;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var DeviceNamedRepository */
    private $accountDeviceNamedRepository;
    /** @var ContractorServiceRepository $contractorServiceRepository  */
    private $contractorServiceRepository;
    /** @var AccountContactPersonCompose $accountContactPersonCompose*/
    private $accountContactPersonCompose;
    /** @var Service $serviceRepository */
    private $serviceRepository;
    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var  ProposalRepository */
    private $proposalRepository;
    /** @var  WorkorderRepository */
    private $workorderRepository;
    /** @var  OpportunityRepository */
    private $opportunityRepository;
    /** @var TokenStorageInterface */
    private $tokenStorage;
    /** @var AccountRepository */
    private $accountRepository;
    /** @var AccountHistoryManager */
    private $accountHistoryManager;
    /** @var MessageRepository */
    private $messageRepository;
    private $dispatcher;
    /** @var MessageManager */
    private $messageManager;
    /** @var DepartmentChannelRepository $departmentChannelRepository */
    private $departmentChannelRepository;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var AddressManager */
    private $addressManager;
    /** @var ContainerInterface */
    private $container;
    /** @var OpportunityTypeRepository $opportunityTypeRepository */
    private $opportunityTypeRepository;
    /** @var Helper */
    private $coordinateHelper;
    /** @var ErrorMessagesService */
    private $accountErrorMessagesService;
    /** @var DivisionsUnderAccount */
    private $divisionsUnderAccountService;

    /**
     * AccountManager constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param ObjectManager $objectManager
     * @param DeviceManager $deviceManager
     * @param AccountContactPersonCompose $accountContactPersonCompose
     * @param TokenStorageInterface $tokenStorage
     * @param AccountHistoryManager $accountHistoryManager
     * @param MessageManager $messageManager
     * @param AddressManager $addressManager
     * @param ContainerInterface $container
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        ObjectManager $objectManager,
        DeviceManager $deviceManager,
        AccountContactPersonCompose $accountContactPersonCompose,
        TokenStorageInterface $tokenStorage,
        AccountHistoryManager $accountHistoryManager,
        MessageManager $messageManager,
        AddressManager $addressManager,
        ContainerInterface $container
    ) {
        $this->container = $container;
        $this->objectManager = $objectManager;
        $this->deviceManager = $deviceManager;
        $this->accountContactPersonCompose = $accountContactPersonCompose;
        $this->tokenStorage = $tokenStorage;
        $this->accountHistoryManager = $accountHistoryManager;
        $this->dispatcher = $dispatcher;
        $this->messageManager = $messageManager;
        $this->addressManager = $addressManager;

        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
        $this->deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
        $this->accountDeviceNamedRepository = $this->objectManager->getRepository('AppBundle:DeviceNamed');
        $this->contractorServiceRepository = $this->objectManager->getRepository('AppBundle:ContractorService');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->deviceRepository = $this->objectManager->getRepository('AppBundle:Device');
        $this->opportunityRepository = $this->objectManager->getRepository('AppBundle:Opportunity');
        $this->messageRepository = $this->objectManager->getRepository('AppBundle:Message');
        $this->departmentChannelRepository = $objectManager->getRepository('AppBundle:DepartmentChannel');
        $this->addressTypeRepository = $objectManager->getRepository('AppBundle:AddressType');
        $this->proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        $this->workorderRepository = $objectManager->getRepository('AppBundle:Workorder');
        $this->opportunityTypeRepository = $objectManager->getRepository('AppBundle:OpportunityType');
        $this->coordinateHelper = $container->get('app.coordinates_helper.service');
        $this->accountErrorMessagesService = $container->get('app.account_error_messages.service');
        $this->divisionsUnderAccountService = $container->get('app.account_divions_under_account.service');
    }

    /**
     * @param Account $account
     */
    public function save(Account $account)
    {
        $this->objectManager->persist($account);
        $this->objectManager->flush();
    }

    /**
     * @param Account $account
     */
    public function delete(Account $account)
    {
        /** @var User $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();

        $this->deleteDevicesByAccount($account);
        if ($account->getType()->getAlias() === 'group account') {
            $this->deleteChildren($account);
        }

        $account->setDeleted(true);
        $this->save($account);
        $this->accountHistoryManager->createAccountHistoryItem($account);

        /** @var AccountDeletedEvent $event */
        $event = new AccountDeletedEvent($account);
        $this->dispatcher->dispatch('account.deleted', $event);
    }

    /**
     * @param $account
     */
    public function create($account)
    {
        $this->coordinateHelper->refreshByAccount($account);
        $this->save($account);
        /** @var AccountCreatedEvent $event */
        $event = new AccountCreatedEvent($account);
        $this->dispatcher->dispatch('account.created', $event);
    }

    /**
     * @param $account
     */
    public function update($account)
    {
        $this->coordinateHelper->refreshByAccount($account);
        $this->save($account);
        /** @var AccountUpdatedEvent $event */
        $event = new AccountUpdatedEvent($account);
        $this->dispatcher->dispatch('account.updated', $event);
    }

    /**
     * @param $account
     */
    public function updateContactPerson($account)
    {
        $this->save($account);
        /** @var AccountUpdateContactsEvent $event */
        $event = new AccountUpdateContactsEvent($account);
        $this->dispatcher->dispatch('account.update.contacts', $event);
    }

    /**
     * @param Account $account
     */
    public function deleteDevicesByAccount(Account $account)
    {
        /** @var User $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();

        /** @var Device[] $devices */
        $devices = $this->deviceRepository->findBy([
            'account' => $account->getId(),
            'parent' => null
        ]);

        if (isset($devices)) {
            /** @var Device $device */
            foreach ($devices as $device) {
                $this->deviceManager->delete($device, $currentUser);
            }
        }
    }

    /**
     * @param Account $account
     */
    public function deleteChildren(Account $account)
    {
        /** @var Account[] $children */
        $children = $this->accountRepository->findBy([
            'parent' => $account,
            'deleted' => false
        ]);

        /** @var Account $child */
        foreach ($children as $child) {
            $this->delete($child);
        }
    }

    /**
     * @param Account $account
     * @return Account
     */
    public function serializeByDeviceCategories(Account $account)
    {
        $devicesByCategories = $this->initDeviceCategoriesArrayForDevices();
        $countsDevices = $this->initCountsDevices();
        $countsServices = $this->initCountsServices();
        $countsDevices['all'] = $this->deviceRepository->getDeviceCount($account);
        $countsDevices['allAndDeleted'] = $this->deviceRepository->getDeviceCountIncludedDeleted($account);

        /** @var Device $device */
        foreach ($account->getDevices() as $device) {
            $categoryAlias = $device->getNamed()->getCategory()->getAlias();
            if (!$device->getParent()) {
                $devicesByCategories[$categoryAlias][] = $this->deviceManager->serialize($device);
            }

            if (empty($device->getDeleted())) {
                $countsDevices[$categoryAlias]++;
            }
            $countsDevices[$categoryAlias . 'AndDeleted']++;

            $countServices = $this->serviceRepository->getServicesByDevice($device);
            $countUnderRetestOpportunityServices = $this->serviceRepository->getServicesByDeviceAndStatus(
                $device,
                self::SERVICE_STATUS_RETEST_OPPORTUNITY
            );
            $countServicesAndDeleted = $this->serviceRepository->getServicesByDeviceIncludedDeleted($device);
            $countsServices['all'] += $countServices;
            $countsServices['countUnderRetestOpportunityServices'] += $countUnderRetestOpportunityServices;
            $countsServices['allAndDeleted'] += $countServicesAndDeleted;
            $countsServices[$categoryAlias] += $countServices;
            $countsServices[$categoryAlias . 'AndDeleted'] += $countServicesAndDeleted;
        }
        $account->devicesByCategories = $devicesByCategories;
        $account->countsDevices = $countsDevices;
        $account->countsServices = $countsServices;

        return $account;
    }

    /**
     * @param Account $account
     * @return Account
     */
    public function serializeGroupAccountByDeviceCategories(Account $account)
    {
        /** @var Account[] $children */
        $children = $account->getChildren();
        $countsDevices = $this->initCountsDevices();
        $countsServices = $this->initCountsServices();

        /**
         * @var int $key
         * @var Account $child
         */
        foreach ($children as $key => $child) {
            $serializedChild = $this->serializeByDeviceCategories($child);
            foreach ($serializedChild->countsDevices as $countName => $count) {
                $countsDevices[$countName] += $count;
            }
            $children[$key] = $serializedChild;

            foreach ($countsServices as $serviceKey => $count) {
                $countsServices[$serviceKey] += $serializedChild->countsServices[$serviceKey];
            }
        }
        $account->countsDevices = $countsDevices;
        $account->countsServices = $countsServices;

        return $account;
    }

    /**
     * @param Account $account
     */
    public function refreshOldestOpportunityDate(Account $account)
    {
        /** @var OpportunityRepository $opportunityRepository */
        $opportunityRepository = $this->objectManager->getRepository('AppBundle:Opportunity');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $this->objectManager->getRepository('AppBundle:DeviceCategory');

        $oldestDate = $opportunityRepository->getOldestExecServicesDate($account);
        $oldestDate = !empty($oldestDate) ? new \DateTime($oldestDate) : null;

        /** @var DeviceCategory $division */
        foreach ($deviceCategory->findAll() as $division) {
            $oldestDivisionDate = $opportunityRepository->getOldestExecServicesDate($account, $division);
            $field = 'setOldestOpportunity' . $division->getName() . 'Date';
            $oldestDivisionDate = !empty($oldestDivisionDate) ? new \DateTime($oldestDivisionDate) : null;

            $account->$field($oldestDivisionDate);
        }

        $account->setOldestOpportunityDate($oldestDate);
        $this->objectManager->persist($account);
        $this->objectManager->flush();

        if ($account->getParent()) {
            $this->refreshOldestOpportunityDate($account->getParent());
        }
    }

    /**
     * @param Account $account
     */
    public function refreshOldestProposalDate(Account $account)
    {
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $this->objectManager->getRepository('AppBundle:Proposal');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $this->objectManager->getRepository('AppBundle:DeviceCategory');

        $oldestDate = $proposalRepository->getOldestEarliestDate($account);
        $oldestDate = !empty($oldestDate) ? new \DateTime($oldestDate) : null;

        /** @var DeviceCategory $division */
        foreach ($deviceCategory->findAll() as $division) {
            $oldestDivisionDate = $proposalRepository->getOldestEarliestDate($account, $division);
            $field = 'setOldestProposal' . $division->getName() . 'Date';
            $oldestDivisionDate = !empty($oldestDivisionDate) ? new \DateTime($oldestDivisionDate) : null;

            $account->$field($oldestDivisionDate);
        }

        $account->setOldestProposalDate($oldestDate);
        $this->objectManager->persist($account);
        $this->objectManager->flush();
    }

    // TODO: винести це в окремий сервіс роботи з меседжами
    /**
     * Attention:
     * - for Alarms of the devices we return the department Fire (by Taras)
     *
     * @param Account $account
     * @param bool $flushFlag
     */
    public function checkDefaultDepartmentChanel(Account $account, $flushFlag = true)
    {
        $isMessage = false;
        if ($account->getType()->getAlias() === 'account') {
            /** @var DeviceCategory[] $divisions */
            $divisions = $this->divisionsUnderAccountService->getDivisions($account);

            /** Костиль котрий підміняє Alarm департамент на Fire **/
            /** @var DeviceCategory $fireCategory */
            $fireCategory = $this->deviceCategoryRepository->findOneBy(['alias' => 'fire']);
            /** @var DeviceCategory $division */
            foreach ($divisions as $division) {
                if ($division->getAlias() == 'alarm') {
                    unset($divisions[$division->getId()]);
                    $divisions[$fireCategory->getId()] = $fireCategory;
                    break;
                }
            }
            /** Кінець костиля **/

            $divisionsIdsArray = $this->getIdsArray($divisions);
            $countDefaultDepartmentChanel =
                $this->departmentChannelRepository->getCountDefaultByDivisionsForMunicipality(
                    $account->getMunicipality(),
                    $divisionsIdsArray
                );

            if (!empty($divisions) and $countDefaultDepartmentChanel < count($divisions)) {
                $isMessage = true;
            }
        } else {
            $isChildrenMessage = $this->messageRepository->isByAliasForChildrenAccount(
                'no_municipality_compliance_channel_backflow',
                $account
            );

            $isMessage = $isChildrenMessage ? true : false;
        }

        /** @var Message $noDefaultDepartmentChanelMessage */
        $noDefaultDepartmentChanelMessage = $this->messageRepository->findOneBy(['alias' => 'no_municipality_compliance_channel_backflow']);
        $this->checkMessage($isMessage, $account, $noDefaultDepartmentChanelMessage, $flushFlag);

        /** @var Account $parentAccount */
        $parentAccount = $account->getParent();
        if ($parentAccount) {
            $this->checkDefaultDepartmentChanel($parentAccount, $flushFlag);
        }
    }

    // TODO: винести це в окремий сервіс роботи з меседжами
    /**
     * @param Account $account
     * @param bool $flushFlag
     */
    public function checkServiceFee(Account $account, $flushFlag = true)
    {
        if ($account->getType()->getAlias() === 'account') {
            $countNoPriceServices = $this->serviceRepository->getCountNoPriceBySiteAccount($account);
        } else {
            $countNoPriceServices = $this->serviceRepository->getCountNoPriceByGroupAccount($account);
        }

        /** @var Message $noServiceFeeMessage */
        $noServiceFeeMessage = $this->messageRepository->findOneBy(['alias' => 'service_no_fee']);
        $this->checkMessage($countNoPriceServices, $account, $noServiceFeeMessage, $flushFlag);

        /** @var Account $parentAccount */
        $parentAccount = $account->getParent();
        if ($parentAccount) {
            $this->checkServiceFee($parentAccount, $flushFlag);
        }
    }

    // TODO: винести це в окремий сервіс роботи з меседжами
    /**
     * @param Account $account
     * @param bool $flushFlag
     */
    public function checkAuthorizer(Account $account, $flushFlag = true)
    {
        /** @var DeviceCategory[] $divisions */
        $divisions = [];
        if (!$account->getParent()) {
            $divisions = $this->divisionsUnderAccountService->getDivisions($account);
        }

        if (!empty($divisions) and !$account->getParent()) {
            /** @var AccountContactPerson $contacts */
            $contacts = $this->accountContactPersonRepository->findBy([
                'account' => $account,
                'authorizer' => true,
                'deleted' => false
            ]);

            /** @var AccountContactPerson $contact */
            foreach ($contacts as $contact) {
                if (empty($divisions)) break;

                /** @var DeviceCategory $responsibility */
                foreach ($contact->getResponsibilities() as $responsibility) {
                    $divisionId = $this->findDivisionId($responsibility, $divisions);
                    if ($divisionId) {
                        unset($divisions[$responsibility->getId()]);
                    }
                }
            }
        }

        /** @var Message $noAuthorizerMessage */
        $noAuthorizerMessage = $this->messageRepository->findOneBy(['alias' => 'no_autorizer']);
        $this->checkMessage($divisions, $account, $noAuthorizerMessage, $flushFlag);

        if ($account->getParent()) {
            $this->checkAuthorizer($account->getParent());
        }
    }

    // TODO: Переробити під витягування з репозиторію
    /**
     * @param Account $account
     * @return array
     */
    public function getAccountDivisions(Account $account)
    {
        $divisions = [];

        $contacts = $this->accountContactPersonRepository->findBy([
            'account' => $account,
            'authorizer' => true,
            'deleted' => false
        ]);

        foreach ($contacts as $contact) {
            $contactResponsibilities = $contact->getResponsibilities();

            foreach ($contactResponsibilities as $responsibilities) {
                $divisions[$responsibilities->getAlias()] = ['id' => $responsibilities->getId(), 'name' => $responsibilities->getName()];
            }
        }

        return $divisions;
    }

    // TODO: винести це в окремий сервіс роботи з меседжами
    /**
     * @param Account $account
     * @param bool $flushFlag
     */
    public function checkMessageAuthorizerAddress(Account $account, $flushFlag = true)
    {
        /** @var DeviceCategory[] $divisions */
        $divisions = $this->divisionsUnderAccountService->getDivisions($account);

        $isInvalidAuthorizersSandingAddress = false;
        if (!empty($divisions)) {
            $isInvalidAuthorizersSandingAddress = $this->isInvalidAuthorizersSandingAddressByDivisions(
                $account,
                $divisions
            );
        }

        /** @var Message $invalidAuthorizerAddressMessage */
        $invalidAuthorizerAddressMessage = $this->messageRepository->findOneBy([
            'alias' => 'authorizer_invalid_address'
        ]);
        $this->checkMessage(
            $isInvalidAuthorizersSandingAddress,
            $account,
            $invalidAuthorizerAddressMessage,
            $flushFlag
        );

        /** @var Account $parent */
        $parent = $account->getParent();
        if ($parent) {
            $this->checkMessageAuthorizerAddressForParent($parent, $flushFlag);
        }
    }

    // TODO: винести це в окремий сервіс роботи з меседжами
    /**
     * @param Account $parent
     * @param bool $flushFlag
     */
    private function checkMessageAuthorizerAddressForParent(Account $parent, $flushFlag = true)
    {
        /** @var DeviceCategory[] $divisions */
        $divisions = $this->divisionsUnderAccountService->getDivisions($parent);
        /** @var Message $invalidAuthorizerAddressMessage */
        $invalidAuthorizerAddressMessage = $this->messageRepository->findOneBy([
            'alias' => 'authorizer_invalid_address'
        ]);

        $isInvalidAuthorizersSandingAddress = false;
        $isInvalidAuthorizersSandingAddressForChildren = false;
        if (!empty($divisions)) {
            $isInvalidAuthorizersSandingAddress = $this->isInvalidAuthorizersSandingAddressByDivisions(
                $parent,
                $divisions
            );

            /** @var Account[] $childrenWithAuthorizerInvalidAddress */
            $childrenWithAuthorizerInvalidAddress = $this->accountRepository
                ->findChildrenWithMessageForGroupAccount($parent, $invalidAuthorizerAddressMessage);
            if ($childrenWithAuthorizerInvalidAddress) {
                $isInvalidAuthorizersSandingAddressForChildren = true;
            }
        }

        if ($isInvalidAuthorizersSandingAddress or $isInvalidAuthorizersSandingAddressForChildren) {
            $this->checkMessage(true, $parent, $invalidAuthorizerAddressMessage, $flushFlag);
        } else {
            $this->checkMessage(false, $parent, $invalidAuthorizerAddressMessage, $flushFlag);
        }
    }

    // TODO: винести це в окремий сервіс роботи з меседжами
    /**
     * @param Account $account
     * @param $divisions
     * @return bool
     */
    private function isInvalidAuthorizersSandingAddressByDivisions(Account $account, $divisions)
    {
        $divisionIds = $this->getIdsArray($divisions);
        /** @var AccountContactPerson[] $primaryAuthorizersForDivisions */
        $primaryAuthorizersForDivisions = $this->accountContactPersonRepository->findPrimaryAuthorizersByAccountForDivisions($account, $divisionIds);

        /** @var AccountContactPerson $authorizer */
        foreach ($primaryAuthorizersForDivisions as $authorizer) {
            /** @var Address $authorizerSandingAddress */
            $authorizerSandingAddress = $authorizer->getSendingAddress();
            $isInvalidAddress = $this->addressManager->isInvalid($authorizerSandingAddress);

            if ($isInvalidAddress) {
                return true;
            }
        }

        return false;
    }

    // TODO: винести це в окремий сервіс роботи з меседжами

    /**
     * @param $isSet
     * @param Account $account
     * @param Message $message
     * @param bool $flushFlag
     */
    private function checkMessage($isSet, Account $account, Message $message, $flushFlag = true)
    {
        if ($isSet) {
            $account->addMessage($message);
        } else {
            $account->removeMessage($message);
        }
        if ($flushFlag) {
            $this->save($account);
        } else {
            $this->objectManager->persist($account);
        }
    }

    /**
     * @param DeviceCategory $responsibility
     * @param $divisions
     * @return bool|int|string
     */
    private function findDivisionId(DeviceCategory $responsibility, $divisions)
    {
        /** @var DeviceCategory $division */
        foreach ($divisions as $key => $division) {
            if ($responsibility === $division) {
                return $key;
            }
        }

        return false;
    }


    /**
     * @return array
     */
    private function initCountsServices()
    {
        /** @var DeviceCategory[] $deviceCategories */
        $deviceCategories = $this->deviceCategoryRepository->findAll();
        $initCountsServices = [
            'all' => 0,
            'allAndDeleted' => 0,
            'countUnderRetestOpportunityServices' => 0
        ];

        /** @var DeviceCategory $deviceCategory */
        foreach ($deviceCategories as $deviceCategory) {
            $initCountsServices[$deviceCategory->getAlias()] = 0;
            $initCountsServices[$deviceCategory->getAlias() . 'AndDeleted'] = 0;
        }

        return $initCountsServices;
    }

    /**
     * @return array
     */
    private function initCountsDevices()
    {
        /** @var DeviceCategory[] $deviceCategories */
        $deviceCategories = $this->deviceCategoryRepository->findAll();
        $initCountsDevices = [
            'all' => 0,
            'allAndDeleted' => 0
        ];

        /** @var DeviceCategory $deviceCategory */
        foreach ($deviceCategories as $deviceCategory) {
            $initCountsDevices[$deviceCategory->getAlias()] = 0;
            $initCountsDevices[$deviceCategory->getAlias() . 'AndDeleted'] = 0;
        }

        return $initCountsDevices;
    }

    /**
     * @return array
     */
    private function initDeviceCategoriesArrayForDevices()
    {
        /** @var DeviceCategory[] $deviceCategories */
        $deviceCategories = $this->deviceCategoryRepository->findAll();
        $initDevices = [];
        /** @var DeviceCategory $deviceCategory */
        foreach ($deviceCategories as $deviceCategory) {
            $initDevices[$deviceCategory->getAlias()] = [];
        }

        return $initDevices;
    }

    /**
     * @param Account $account
     * @param array $filterParams
     * @return array
     */
    public function getAccountViewParameters(Account $account, $filterParams = [])
    {
        /** @var AccountContactPerson[] $accountContactPersons */
        $accountContactPersons = $this->accountContactPersonRepository->findBy([
            'account' => $account,
            'deleted' => false
        ]);
        $deviceNamedArray = $this->accountDeviceNamedRepository->findBy([
            'parent' => null
        ]);

        /** @var OpportunityTypeRepository $opportunityTypeRetest */
        $opportunityTypeRetest = $this->opportunityTypeRepository->findBy(['alisa' => 'retest']);
        $filterParams['type'] = $opportunityTypeRetest;

        $proposalsTypeRetest = ["retest"];
        $proposalsAll = ["repair", "custom"];
        $opportunityRepair = ["repair"];

        $viewParams = [];
        $viewParams['deviceNamed'] = (isset($deviceNamedArray) and !empty($deviceNamedArray)) ? $deviceNamedArray : '';
        $viewParams['contacts'] = $this->accountContactPersonCompose->compose($accountContactPersons);
        $viewParams['allContactsCount'] = $this->accountContactPersonRepository->getCount($account);
        $viewParams['contractorServices'] = $this->contractorServiceRepository->findAll();
        $viewParams['account'] = $account;
        $viewParams['allDevicesCount'] = $this->getAllDeviceCount($account);
        $viewParams['allServicesCount'] = $this->getAllServicesCount($account);
        $viewParams['opportunities'] = $this->getOpportunities($account, $filterParams);
        $viewParams['errorMessages'] = $this->accountErrorMessagesService->getErrorMessages($account);
        $viewParams['countRetestProposals'] =
            $this->proposalRepository->getCountProposals($account, $proposalsTypeRetest);
        $viewParams['countProposals'] =
            $this->proposalRepository->getCountProposals($account, $proposalsAll);
        $viewParams['countWorkorder'] = $this->workorderRepository->getCountWorkOrderInAccount($account);
        $viewParams['countRepairOpportunity'] =
            $this->serviceRepository->getCountAccountService($account, $opportunityRepair);

        return $viewParams;
    }

    /**
     * @param Account $account
     * @param $filterParams
     * @return array
     */
    private function getOpportunities(Account $account, $filterParams)
    {
        if ($account->getType()->getAlias() == "account") {
            $opportunitiesArray = $this->getSiteAccountOpportunities($account, $filterParams);
        } else {
            $opportunitiesArray = $this->getGroupAccountOpportunities($account, $filterParams);
        }

        return $opportunitiesArray;
    }

    /**
     * @param Account $account
     * @param array $filterParams
     * @return array
     */
    private function getSiteAccountOpportunities(Account $account, $filterParams)
    {
        $opportunitiesArray = $this->opportunityRepository->getFilteredAccountsOpportunities(
            [$account->getId()],
            $filterParams
        );

        return $opportunitiesArray;
    }

    /**
     * @param Account $account
     * @param array $filterParams
     * @return array
     */
    private function getGroupAccountOpportunities(Account $account, $filterParams)
    {
        $idsArray = $this->getChildrenIds($account);
        $opportunitiesArray = $this->opportunityRepository->getFilteredAccountsOpportunities(
            $idsArray,
            $filterParams
        );

        return $opportunitiesArray;
    }

    /**
     * @param Account $account
     * @return int|mixed
     */
    private function getAllDeviceCount(Account $account)
    {
        if ($account->getType()->getAlias() == "account") {
            $allDeviceCount = $this->deviceRepository->getDeviceCount($account);
        } else {
            $idsArray = $this->getChildrenIds($account);
            $allDeviceCount = count(
                $this->deviceRepository->getDeviceByGroupAccountChildren($idsArray)
            );
        }
        return $allDeviceCount;
    }

    /**
     * @param Account $account
     * @return int
     */
    private function getAllServicesCount(Account $account)
    {
        if ($account->getType()->getAlias() == "account") {
            $allServicesCount = count($this->serviceRepository->getServicesByAccount($account));
        } else {
            $idsArray = $this->getChildrenIds($account);
            $allServicesCount = count(
                $this->serviceRepository->getServicesByGroupAccountChildren($idsArray)
            );
        }
        return $allServicesCount;
    }

    /**
     * @param Account $account
     * @return array
     */
    private function getChildrenIds(Account $account)
    {
        $idsArray = [];
        /** @var Account $child */
        foreach ($account->getChildren() as $child) {
            $idsArray[] = $child->getId();
        }

        return $idsArray;
    }

    /**
     * @param Account $account
     */
    public function checkParentAndAuthorizer(Account $account)
    {
        $this->messageManager->isChildAccountHaveOwnAuthorizer($account);
    }

    /**
     * @param $collection
     * @return array
     */
    private function getIdsArray($collection)
    {
        $res = [];
        foreach ($collection as $item) {
            $res[] = $item->getId();
        }
        return array_values($res);
    }

    /**
     * @param Account $account
     * @return Account
     */
    public function setAddressForAccount(Account $account)
    {
        $address = $account->getAddress();

        if (!$account->getAddress()) {
            $address = new Address();
        }

        if ($account->getType()->getAlias() == "account") {
            $accountTypeSite = $this->addressTypeRepository->findOneBy(["alias" => "site address"]);
            $address->setAddressType($accountTypeSite);
            $account->setAddress($address);

            return $account;
        }

        $accountTypeSite = $this->addressTypeRepository->findOneBy(["alias" => "group account address"]);
        $address->setAddressType($accountTypeSite);
        $account->setAddress($address);

        return $account;
    }



    /**
     * @param Company $company
     * @return bool
     */
    public function unsetAllAccountsFromCompany(Company $company)
    {
        $accounts = $this->accountRepository->findBy(["company" => $company]);

        if (!$accounts) {
            return false;
        }
        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->unsetAccountFromCompany($account);
        }

        return true;
    }

    /**
     * @param Account $account
     */
    public function unsetAccountFromCompany(Account $account)
    {

        $account->setCompany(null);
        $this->save($account);
    }

    /**
     * @param Account $account
     * @return bool
     */
    public function checkOnChangeMunicipality(Account $account)
    {
        /** @var AccountHistoryRepository $accountHistoryRepository */
        $accountHistoryRepository = $this->objectManager->getRepository('AppBundle:AccountHistory');
        /** @var AccountHistory $lastAccountHistory */
        $lastAccountHistory = $accountHistoryRepository->getLastRecord($account);
        /** @var Municipality $accountHistoryMunicipality */
        $accountHistoryMunicipality = $lastAccountHistory->getMunicipality();
        $accountHistoryMunicipalityId = null;

        if ($accountHistoryMunicipality instanceof Municipality) {
            $accountHistoryMunicipalityId = $lastAccountHistory->getMunicipality()->getId();
        }

        $accountMunicipalityId = $account->getMunicipality()->getId();

        if ($accountHistoryMunicipalityId == $accountMunicipalityId) {
            return false;
        }

        return true;
    }
}
