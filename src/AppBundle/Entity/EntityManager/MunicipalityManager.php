<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Account;
use AppBundle\Entity\Address;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\Agent;
use AppBundle\Entity\AgentChannel;
use AppBundle\Entity\AgentChannelDynamicFieldValue;
use AppBundle\Entity\Department;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DepartmentChannelDynamicFieldValue;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\AgentChannelRepository;
use AppBundle\Entity\Repository\DepartmentChannelRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\User;
use AppBundle\Events\MunicipalityCreatedEvent;
use AppBundle\Services\DepartmentChannelDynamicFieldsService;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use AppBundle\Events\DepartmentSetAgentEvent;
use AppBundle\Events\DepartmentUnsetAgentEvent;

class MunicipalityManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var DepartmentHistoryManager  */
    private $departmentHistoryManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var  EventDispatcherInterface */
    private $dispatcher;
    /** @var MunicipalityRepository */
    private $municipalityRepository;
    /** @var \AppBundle\Services\Address\Creator */
    private $addressCreatorService;
    /** @var DepartmentChannelDynamicFieldsService */
    private $departmentChannelDynamicFieldsService;

    /**
     * MunicipalityManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher
    ) {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;
        $this->dispatcher = $dispatcher;

        $this->departmentHistoryManager = $this->serviceContainer->get('app.department_history.manager');
        $this->municipalityRepository = $this->objectManager->getRepository('AppBundle:Municipality');
        $this->addressCreatorService = $container->get('app.address_creator.service');
        $this->departmentChannelDynamicFieldsService = $container->get('app.department_channel_dynamic_fields.service');
    }

    /**
     * @param $collection
     */
    public function save($collection)
    {
        $this->objectManager->persist($collection);
        $this->objectManager->flush();
    }

    /**
     * @param Municipality $municipality
     * @return Municipality
     */
    public function create(Municipality $municipality)
    {
        $this->save($municipality);
        /** @var MunicipalityCreatedEvent $event */
        $event = new MunicipalityCreatedEvent($municipality);
        $this->dispatcher->dispatch('municipality.created', $event);

        return $municipality;
    }

    //TODO: потрібно відрефакторити тому що не зрозуміло як працює цей метод
    /**
     * @param Department $department
     * @param Agent $agent
     * @param User $user
     * @return bool
     */
    public function attachAgentToDepartment(Department $department, Agent $agent, User $user)
    {
        /** @var AgentChannelRepository $AgentChannelRepository */
        $agentChannelRepository = $this->objectManager->getRepository('AppBundle:AgentChannel');
        /** @var DepartmentChannelRepository $DepartmentChannelRepository */
        $departmentChannelRepository = $this->objectManager->getRepository('AppBundle:DepartmentChannel');

        if ($department->getAgent() === $agent) {
            return true;
        }

        $agentChannels = $agentChannelRepository->findBy(['agent' => $agent->getId()]);

        $department->setAgent($agent);
        $this->save($department);
        $this->departmentHistoryManager->createDepartmentHistory($department, $user);

        /** @var DepartmentChannelManager $departmentChanelManager */
        $departmentChanelManager = $this->serviceContainer->get('app.department_channel.manager');
        $departmentChanelManager->allUnActiveByDepartment($department);

        if (count($agentChannels)) {
            foreach ($agentChannels as $agentChannel) {
                if ($agentChannel->getDeviceCategory() === $department->getDivision()) {
                    $attachDepartmentChannel = $departmentChannelRepository->findOneBy([
                        'department' => $department,
                        'devision' => $department->getDivision(),
                        'owner' => $agentChannel,
                        'ownerAgent' => $agentChannel->getAgent(),
                        'deleted' => true
                    ]);

                    if ($attachDepartmentChannel instanceof DepartmentChannel) {
                        //remove old department channel dynamic fields
                        $this->departmentChannelDynamicFieldsService
                            ->removeAllFromDepartmentChannel($attachDepartmentChannel);
                        $this->resurrectDepartmentChannel($attachDepartmentChannel, $agentChannel);
                    } else {
                        $this->copyAgentToDepartment($agentChannel, $department);
                    }
                }
            }
        }

        /** @var DepartmentSetAgentEvent $event */
        $event = new DepartmentSetAgentEvent($agent, $department);
        $this->dispatcher->dispatch('department.set_agent', $event);
    }

    //TODO: потрібно відрефакторити тому що не зрозуміло як працює цей метод
    /**
     * @param Department $department
     * @param User $user
     */
    public function deleteAgent(Department $department, User $user)
    {
        /** @var DepartmentChannelRepository $DepartmentChannelRepository */
        $departmentChannelRepository = $this->objectManager->getRepository('AppBundle:DepartmentChannel');
        /** @var DepartmentChannel[] $departmentChannels */
        $departmentChannels = $departmentChannelRepository->findBy([
            'ownerAgent' => $department->getAgent(),
            'department' => $department
        ]);

        /** @var Agent $agent */
        $agent = $department->getAgent();
        $department->setAgent();
        $this->save($department);
        $this->departmentHistoryManager->createDepartmentHistory($department, $user);

        /** @var DepartmentChannelManager $departmentChanelManager */
        $departmentChanelManager = $this->serviceContainer->get('app.department_channel.manager');
        $departmentChanelManager->allActiveByDepartment($department);

        /** @var DepartmentChannel $departmentChannel */
        foreach ($departmentChannels as $departmentChannel) {
            if ($departmentChannel->getIsDefault()) {
                $attachDepartmentChannel = $departmentChannelRepository->findOneBy([
                    'department' => $departmentChannel->getDepartment(),
                    'devision' => $departmentChannel->getDevision(),
                    'isDefault' => true
                ]);
                $this->serviceContainer->get('app.service.manager')->setDepartmentChannel($attachDepartmentChannel);
            }
            /** @var Service $service */
            foreach ($departmentChannel->getServices() as $service) {
                $service->setDepartmentChannel(null);
                $this->objectManager->persist($service);
            }

            $departmentChannel->setDeleted(true);
            $departmentChannel->setActive(false);
        }

        $this->objectManager->flush();

        /** @var DepartmentUnsetAgentEvent $event */
        $event = new DepartmentUnsetAgentEvent($agent, $department);
        $this->dispatcher->dispatch('department.unset_agent', $event);
    }

    /**
     * @param Account $account
     * @param array $result
     * @return array
     */
    public function prepareMunicipalityAndProcessingFeeByDivision(Account $account, $result = [])
    {
        /** @var DeviceCategoryRepository $deviceCategoryRepository */
        $deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
        /** @var Account|array $account */
        $account = $account->getChildren()->isEmpty() ? $account : $account->getChildren()->toArray();

        if (is_array($account)) {
            foreach ($account as $oneAccount) {
                $result = $this->prepareMunicipalityAndProcessingFeeByDivision($oneAccount, $result);
            }
        }

        if ($account instanceof Account) {
            /** @var array $divicions */
            $divisions = $deviceCategoryRepository->getDivision($account);

            /** @var DeviceCategory $division */
            foreach ($divisions as $division) {
                $result[$account->getId()][$division->getAlias()] = $this->municipalityRepository
                    ->getMunicipalityFeeAndProcessingFee(
                        $account,
                        $division
                    );
            }
        }

        return $result;
    }

    /**
     * @return Municipality
     */
    public function prepareCreate()
    {
        /** @var Municipality $newMunicipality */
        $newMunicipality = new Municipality();
        /** @var Address $address */
        $address = $this->addressCreatorService->createWithMunicipalityType();

        $this->objectManager->persist($address);
        $newMunicipality->setAddress($address);

        return $newMunicipality;
    }

    /**
     * Copy AgentChannel to DepartmentChannel
     *
     * @param AgentChannel $agentChannel
     * @param Department $department
     */
    private function copyAgentToDepartment(AgentChannel $agentChannel, Department $department)
    {
        $departmentChannel = new DepartmentChannel();
        $departmentChannel->setNamed($agentChannel->getNamed());
        $departmentChannel->setIsDefault($agentChannel->getIsDefault());
        $departmentChannel->setDevision($agentChannel->getDeviceCategory());
        $departmentChannel->setDepartment($department);
        $departmentChannel->setOwnerAgent($agentChannel->getAgent());
        $departmentChannel->setOwner($agentChannel);
        $departmentChannel->setActive(true);

        $this->save($departmentChannel);

        if ($departmentChannel->getIsDefault()) {
            $this->serviceContainer->get('app.service.manager')->setDepartmentChannel($departmentChannel);
        }

        $this->departmentChannelDynamicFieldsService->createByAgentChannel($agentChannel, $departmentChannel);
    }

    /**
     * @param DepartmentChannel $departmentChannel
     * @param AgentChannel $agentChannel
     */
    private function resurrectDepartmentChannel(DepartmentChannel $departmentChannel, AgentChannel $agentChannel)
    {
        $departmentChannel->setDeleted(false);
        $departmentChannel->setActive(true);
        $departmentChannel->setIsDefault($agentChannel->getIsDefault());
        $this->save($departmentChannel);

        if ($departmentChannel->getIsDefault()) {
            $this->serviceContainer->get('app.service.manager')->setDepartmentChannel($departmentChannel);
        }

        $this->departmentChannelDynamicFieldsService->createByAgentChannel($agentChannel, $departmentChannel);
    }
}
