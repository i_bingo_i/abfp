<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Repository\ContactPersonRepository;
use AppBundle\Events\ContactPersonUnsetCompanyEvent;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\ContactPersonHistory;
use AppBundle\Entity\User;
use AppBundle\Entity\Company;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use AppBundle\Events\ContactPersonCreatedEvent;
use AppBundle\Events\ContactPersonUpdatedEvent;
use AppBundle\Events\ContactPersonDeletedEvent;

class ContactPersonManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var  ContactPersonRepository */
    private $contactPersonRepository;

    /**
     * ContactPersonManager constructor.
     * @param ObjectManager $objectManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(ObjectManager $objectManager, EventDispatcherInterface $dispatcher) {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;
        $this->contactPersonRepository = $this->objectManager->getRepository("AppBundle:ContactPerson");
    }

    /**
     * @param ContactPerson $contactPerson
     */
    public function save(ContactPerson $contactPerson)
    {
        $this->objectManager->persist($contactPerson);
        $this->objectManager->flush();
    }

    /**
     * @deprecated This method is deprecated. Please use ContactPersonActionManager method create.
     * @param ContactPerson $contactPerson
     */
    public function create(ContactPerson $contactPerson)
    {
        $this->save($contactPerson);
        /** @var ContactPersonCreatedEvent $event */
        $event = new ContactPersonCreatedEvent($contactPerson);
        $this->dispatcher->dispatch('contact_person.created', $event);
    }

    /**
     * @param ContactPerson $contactPerson
     */
    public function update(ContactPerson $contactPerson)
    {
        $this->save($contactPerson);
        /** @var ContactPersonUpdatedEvent $event */
        $event = new ContactPersonUpdatedEvent($contactPerson);
        $this->dispatcher->dispatch('contact_person.updated', $event);
    }

    /**
     * @param Company $company
     * @return bool
     */
    public function deleteAndUnsetAllContactPersonsFromCompany(Company $company)
    {
        $contactPersons = $this->contactPersonRepository->findBy(["company" => $company, "deleted"=>false]);

        if (!$contactPersons) {
            return false;
        }
        /** @var ContactPerson $contactPerson */
        foreach ($contactPersons as $contactPerson) {
            $this->delete($contactPerson);
            $this->unsetContactPersonFromCompany($contactPerson);
        }

        return true;
    }

    /**
     * @param ContactPerson $contactPerson
     */
    public function delete(ContactPerson $contactPerson)
    {
        $contactPerson->setDeleted(true);
        $this->save($contactPerson);
        /** @var ContactPersonDeletedEvent $event */
        $event = new ContactPersonDeletedEvent($contactPerson);
        $this->dispatcher->dispatch('contact_person.deleted', $event);
    }

    /**
     * @param ContactPerson $contactPerson
     */
    public function unsetContactPersonFromCompany(ContactPerson $contactPerson)
    {
        $contactPerson->setCompany(null);
        $this->save($contactPerson);
        /** @var ContactPersonUnsetCompanyEvent $event */
        $event = new ContactPersonUnsetCompanyEvent($contactPerson);
        $this->dispatcher->dispatch('contact_person.unset_company', $event);
    }
}