<?php

namespace AppBundle\Entity\EntityManager;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\AccountContactPersonHistory;

class AccountContactPersonHistoryManager
{
    /** @var  ObjectManager */
    private $objectManager;

    /**
     * AccountContactPersonHistoryManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function save(AccountContactPersonHistory $acpHistory)
    {
        $this->objectManager->persist($acpHistory);
        $this->objectManager->flush();
    }
}
