<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Repository\ProposalStatusRepository;
use Doctrine\Common\Persistence\ObjectManager;

class ProposalStatusManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var  ProposalStatusRepository */
    private $proposalStatusRepository;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        $this->proposalStatusRepository = $this->objectManager->getRepository('AppBundle:ProposalStatus');
    }

    /**
     * @return array
     */
    public function getForRepairProposal()
    {
        $aliases = [
            "draft", "replacing", "sent_via_mail", "sent_via_email", "sent_via_mail_and_email", "call_needed",
            "client_was_called", "rejected_by_client",
            "deleted", "workorder_created", "services_discarded", "replaced"
        ];

        $statuses = $this->proposalStatusRepository->findByAliases($aliases);

        return $statuses;
    }

    /**
     * @return array
     */
    public function getForRetestNotices()
    {
        $aliases = [
            "draft", "to_be_sent", "replacing", "sent_via_mail", "sent_via_email", "sent_via_mail_and_email",
            "call_needed", "client_was_called", "send_email_reminder", "email_reminder_sent", "past_due",
            "send_past_due_email", "rejected_by_client", "no_response", "deleted", "reset_services_to_next_time",
            "workorder_created", "replaced"
        ];

        $statuses = $this->proposalStatusRepository->findByAliases($aliases);

        return $statuses;
    }
}
