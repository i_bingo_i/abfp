<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\ProposalLifeCycle;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Proposal;

class ProposalLifeCycleManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var $lifeCycleRepository */
    private $proposalLifeCycleRepository;

    /**
     * ProposalLifeCycle constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->proposalLifeCycleRepository = $this->objectManager->getRepository('AppBundle:ProposalLifeCycle');
    }

    /**
     * @param Proposal $proposal
     * @return ProposalLifeCycle|null|object
     */
    public function getProposalLifeCycle(Proposal $proposal)
    {
        $proposalTypeAlias = $proposal->getType()->getAlias();

        if ($proposalTypeAlias == 'retest') {
            /** @var ProposalLifeCycle $lifeCycle */
            $proposalLifeCycle = $this->proposalLifeCycleRepository->findOneBy(['alias' => 'retest_notice_life_cycle']);
        } else {
            /** @var ProposalLifeCycle $lifeCycle */
            $proposalLifeCycle = $this->proposalLifeCycleRepository->findOneBy(['alias' => 'proposal_life_cycle']);
        }

        return $proposalLifeCycle;
    }

}