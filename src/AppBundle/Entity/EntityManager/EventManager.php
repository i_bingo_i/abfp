<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Event;
use Doctrine\Common\Persistence\ObjectManager;

class EventManager
{
    public const TYPE_WORKORDER = 'workorder';
    public const TYPE_TIME_OFF = 'time_off';

    /** @var ObjectManager */
    private $objectManager;

    /**
     * EventManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param Event $event
     */
    public function save(Event $event)
    {
        $this->objectManager->persist($event);
        $this->objectManager->flush();
    }

    /**
     * @param $event
     */
    public function delete($event)
    {
        $this->objectManager->remove($event);
        $this->objectManager->flush();
    }
}
