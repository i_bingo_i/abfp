<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Agent;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Contact;

class AgentManager
{
    private $contactManager;

    /**
     * AgentManager constructor.
     *
     * @param ContactManager $contactManager
     */
    public function __construct(ContactManager $contactManager)
    {
        $this->contactManager = $contactManager;
    }

    // TODO: потрібно зробити це через репозиторій ContactRepository
    /**
     * @param Agent $agent
     * @param DeviceCategory $division
     * @return array
     */
    public function getContactsByDivision(Agent $agent, DeviceCategory $division)
    {
        $contacts = [];
        /** @var Contact $contact */
        foreach ($agent->getContacts() as $contact) {
            if ($this->contactManager->isDivision($contact, $division)) {
                $contacts[] = $contact;
             }
        }

        return $contacts;
    }
}
