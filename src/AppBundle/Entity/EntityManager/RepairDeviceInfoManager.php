<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Device;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\RepairDeviceInfo;
use Doctrine\Common\Persistence\ObjectManager;

class RepairDeviceInfoManager
{
    /** @var ObjectManager */
    private $objectManager;

    /**
     * RepairDeviceInfoManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param RepairDeviceInfo $deviceInfo
     */
    public function save(RepairDeviceInfo $deviceInfo)
    {
        $this->objectManager->persist($deviceInfo);
        $this->objectManager->flush();
    }

    /**
     * @param Proposal $proposal
     * @param Device $device
     */
    public function delete(Proposal $proposal, Device $device)
    {
        /** @var RepairDeviceInfo $repairDeviceInfo */
        $repairDeviceInfo = $device->getRepairDeviceInfo()->filter(function (RepairDeviceInfo $item) use ($proposal) {
            return  $item->getProposal()->getId() == $proposal->getId();
        })->first();

        $this->remove($repairDeviceInfo);
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     */
    public function remove(RepairDeviceInfo $repairDeviceInfo)
    {
        $this->objectManager->remove($repairDeviceInfo);
        $this->objectManager->flush();
    }
}
