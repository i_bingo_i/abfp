<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Repository\AddressTypeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Company;
use AppBundle\Entity\Address;
use AppBundle\Events\CompanyCreatedEvent;
use AppBundle\Events\CompanyUpdatedEvent;
use AppBundle\Events\CompanyDeletedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CompanyManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var  AddressTypeRepository */
    private $addressTypeRepository;
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /**
     * CompanyManager constructor.
     * @param ObjectManager $objectManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(ObjectManager $objectManager, EventDispatcherInterface $dispatcher)
    {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;

        $this->addressTypeRepository = $this->objectManager->getRepository("AppBundle:AddressType");
    }

    /**
     * @param Company $company
     */
    public function save(Company $company)
    {
        $this->objectManager->persist($company);
        $this->objectManager->flush();
    }

    /**
     * @param Company $company
     * @return Company
     */
    public function setAddressForCompany(Company $company)
    {
        /** @var Address $address */
        $address = $company->getAddress();
        if (!$company->getAddress()) {
            $address = new Address();
        }

        $companyType = $this->addressTypeRepository->findOneBy(["alias" => "company address"]);
        $address->setAddressType($companyType);
        $company->setAddress($address);

        return $company;
    }

    /**
     * @deprecated This method is deprecated. Please use CompanyActionManager method create.
     * @param Company $company
     */
    public function create(Company $company)
    {
        $this->save($company);
        /** @var CompanyUpdatedEvent $event */
        $event = new CompanyCreatedEvent($company);
        $this->dispatcher->dispatch('company.created', $event);
    }

    /**
     * @param Company $company
     */
    public function update(Company $company)
    {
        $this->save($company);
        /** @var CompanyUpdatedEvent $event */
        $event = new CompanyUpdatedEvent($company);
        $this->dispatcher->dispatch('company.updated', $event);
    }

    /**
     * @param Company $company
     */
    public function delete(Company $company)
    {
        $company->setDeleted(true);
        $this->objectManager->persist($company);
        $this->objectManager->flush();
        /** @var CompanyDeletedEvent $event */
        $event = new CompanyDeletedEvent($company);
        $this->dispatcher->dispatch('company.deleted', $event);
    }
}
