<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\WorkorderLog;
use Doctrine\Common\Persistence\ObjectManager;

class WorkorderLogManager
{
    /** @var ObjectManager  */
    private $objectManager;

    /**
     * WorkorderLogManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param WorkorderLog $workorderLogRecord
     */
    public function save(WorkorderLog $workorderLogRecord)
    {
        $this->objectManager->persist($workorderLogRecord);
        $this->objectManager->flush();
    }
}
