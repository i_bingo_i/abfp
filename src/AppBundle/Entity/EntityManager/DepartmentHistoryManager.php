<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Department;
use AppBundle\Entity\DepartmentHistory;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class DepartmentHistoryManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var TokenStorage */
    private $tokenStorage;

    /**
     * DepartmentHistoryManager constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorage $tokenStorage
     */
    public function __construct(ObjectManager $objectManager, TokenStorage $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;
    }

    public function createDepartmentHistory(Department $department, User $user = null)
    {
        if (!$user) {
            $token = $this->tokenStorage->getToken();
            if (!empty($token) and gettype($token->getUser()) != "string") {
                /** @var User $user */
                $user = $token->getUser();
            }
        }

        $departmentHistory = new DepartmentHistory();
        $departmentHistory->setMunicipality($department->getMunicipality());
        $departmentHistory->setDivision($department->getDivision());
        $departmentHistory->setAgent($department->getAgent());
        $departmentHistory->setAuthor($user);
        $departmentHistory->setOwnerEntity($department);
        $departmentHistory->setName($department->getName());
        $departmentHistory->setDateSave(new \DateTime());

        $this->objectManager->persist($departmentHistory);
        $this->objectManager->flush();
    }
}