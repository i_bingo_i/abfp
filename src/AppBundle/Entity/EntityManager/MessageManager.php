<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Department;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Message;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Repository\MessageTypeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MessageManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var MessageRepository */
    private $messageRepository;
    /** @var MessageTypeRepository */
    private $messageTypeRepository;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var DeviceCategoryRepository */
    private $deviceCategoriesRepository;
    /** @var \AppBundle\Entity\Repository\MunicipalityRepository|\Doctrine\Common\Persistence\ObjectRepository  */
    private $municipalityRepository;
    /** @var AccountContactPersonRepository  */
    private $accountContactPersonRepository;

    private const NO_AUTORIZER = 1;
    private const NO_MUNICIPALITY_COMPLIANCE_CHANNEL_BACKFLOW = 4;
    private const SITE_HAS_OWN_AUTORIZER = 'site_has_own_authorizer';


    /**
     * MessageManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;
        $this->messageRepository = $this->objectManager->getRepository('AppBundle:Message');
        $this->messageTypeRepository = $this->objectManager->getRepository('AppBundle:MessageType');
        $this->deviceCategoriesRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
        $this->municipalityRepository = $this->objectManager->getRepository('AppBundle:Municipality');
        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
    }

    /**
     * @param $messages
     * @param Message $checkMessage
     *
     * @return bool
     */
    public function isMessage($messages, Message $checkMessage)
    {
        /** @var Message $message */
        foreach ($messages as $key => $message) {
            if ($messages[$key] === $checkMessage) {

                return true;
            }
        }

        return false;
    }

//    /**
//     * @param Account $account
//     * @return bool
//     */
//    public function isMunicipalityComplianceChannelByAccount(Account $account)
//    {
//        $departments = $account->getMunicipality()->getDepartment();
//        $divisions = $this->deviceCategoriesRepository->getDivision($account);
//        $isDefaultChannel = [];
//
//        if (empty($divisions)) {
//            return true;
//        }
//
//        /** @var Department $department */
//        foreach ($departments as $department) {
//            if (in_array($department->getDivision()->getId(), $this->getIdsArray($divisions))) {
//                /** @var DepartmentChannel $channel */
//                foreach ($department->getChannels() as $channel) {
//                    if (!empty($channel->getIsDefault())) {
//                        $isDefaultChannel[] = $department->getDivision()->getId();
//                    }
//                }
//            }
//        }
//        $this->attachOrDetachMunicipalityComplianceChannelMessage($account, $isDefaultChannel, $divisions);
//    }

//    /**
//     * @param Department $department
//     */
//    public function isMunicipalityComplianceChannelByDepartment(Department $department)
//    {
//        $accounts = $department->getMunicipality()->getAccount();
//
//        /** @var Account $account */
//        foreach ($accounts as $account) {
//            $this->isMunicipalityComplianceChannelByAccount($account);
//        }
//    }

    /**
     * @param Account $account
     * @param Message $message
     * @param bool $flushFlag
     */
    private function attachToAccount(Account $account, Message $message, $flushFlag = true)
    {
        $account->addMessage($message);
        $this->objectManager->persist($account);
        if ($flushFlag) {
            $this->objectManager->flush();
        }
    }

    /**
     * @param Account $account
     * @param Message $message
     * @param bool $flushFlag
     */
    private function detachAccount(Account $account, Message $message, $flushFlag = true)
    {
        $account->removeMessage($message);
        $this->objectManager->persist($account);
        if ($flushFlag) {
            $this->objectManager->flush();
        }
    }


//    /**
//     * @param $account
//     * @param $isDefaultChannel
//     * @param $divisions
//     * @return bool
//     */
//    private function attachOrDetachMunicipalityComplianceChannelMessage(Account $account, $isDefaultChannel, $divisions)
//    {
//        $message = $this->messageRepository->find(self::NO_MUNICIPALITY_COMPLIANCE_CHANNEL_BACKFLOW);
//
//        $isDefaultChannel = array_unique($isDefaultChannel, SORT_NUMERIC);
//        $divisions = $this->getIdsArray($divisions);
//        sort($isDefaultChannel, SORT_NUMERIC);
//        sort($divisions, SORT_NUMERIC);
//
//        $this->attachOrDetachParentAccount($account, $message);
//
//        if (empty($isDefaultChannel)) {
//            $this->attachToAccount($account, $message);
//            return false;
//        }
//
//        if ($isDefaultChannel == $divisions) {
//            $this->detachAccount($account, $message);
//        } else {
//            $this->attachToAccount($account, $message);
//        }
//    }

//    /**
//     * @param Account $account
//     * @param Message $message
//     */
//    private function attachOrDetachParentAccount(Account $account, Message $message)
//    {
//        $parentAccount = $account->getParent();
//        if (!empty($parentAccount)) {
//            $notExistsMessage = 0;
//
//            /** @var Account $child */
//            foreach ($parentAccount->getChildren() as $child) {
//                if (in_array($message->getId(), $this->getIdsArray($child->getMessages()))) {
//                    $this->attachToAccount($parentAccount, $message);
//                    continue;
//                }
//                $notExistsMessage++;
//            }
//
//            if (empty($notExistsMessage)) {
//                $this->detachAccount($parentAccount, $message);
//            }
//        }
//    }

    /**
     * @param $collection
     * @return array
     */
    private function getIdsArray($collection)
    {
        $res = [];
        foreach ($collection as $item) {
            $res[] = $item->getId();
        }
        return array_values($res);
    }


    /**
     * @param Account $account
     * @param bool $flushFlag
     */
    public function isChildAccountHaveOwnAuthorizer(Account $account, $flushFlag = true)
    {
        /** @var AccountContactPerson $primaryAuthorizers */
        $primaryAuthorizers = $this->accountContactPersonRepository->findPrimaryAuthorizersByAccount($account);

        /** @var Message $hasOwnAuthorizerMessage */
        $siteHasOwnAuthorizerMessage = $this->messageRepository->findOneBy(['alias' => self::SITE_HAS_OWN_AUTORIZER]);
        $isSiteHasOwnAuthorizer = $this->messageRepository->isByAliasForAccount(
            self::SITE_HAS_OWN_AUTORIZER,
            $account
        );

        if ($account->getParent() and !empty($primaryAuthorizers) and !$isSiteHasOwnAuthorizer) {
            $this->attachToAccount($account, $siteHasOwnAuthorizerMessage, $flushFlag);
        }

        if (($account->getParent() and empty($primaryAuthorizers))
            or (!$account->getParent() and !empty($primaryAuthorizers))
            and $isSiteHasOwnAuthorizer
        ) {
            $this->detachAccount($account, $siteHasOwnAuthorizerMessage, $flushFlag);
        }
    }
}
