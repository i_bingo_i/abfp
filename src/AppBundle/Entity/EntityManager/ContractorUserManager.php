<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Event;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\LicensesRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Events\ContractorUserCreatedEvent;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorUser;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ContractorUserManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var  LicensesRepository */
    private $licensesRepository;
    /** @var TokenStorageInterface */
    private $tokenStorage;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /**
     * ContractorUserManager constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        ObjectManager $objectManager,
        TokenStorageInterface $tokenStorage,
        EventDispatcherInterface $dispatcher
    ) {
        $this->objectManager = $objectManager;
        $this->licensesRepository = $this->objectManager->getRepository('AppBundle:Licenses');
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');
        $this->tokenStorage = $tokenStorage;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ContractorUser $contractorUser
     */
    public function save(ContractorUser $contractorUser)
    {
        $this->objectManager->persist($contractorUser);
        $this->objectManager->flush();
    }

    /**
     * @param ContractorUser $contractorUser
     */
    public function create(ContractorUser $contractorUser)
    {
        $this->save($contractorUser);
        /** @var ContractorUserCreatedEvent $event */
        $event = new ContractorUserCreatedEvent($contractorUser);
        $this->dispatcher->dispatch('contractor_user.created', $event);
    }

    /**
     * @param ContractorUser $contractorUser
     */
    public function delete(ContractorUser $contractorUser)
    {
        $contractorUser->setDeleted(true);
        $contractorUser->setActive(false);
        $this->save($contractorUser);

        /** @var ContractorUserCreatedEvent $event */
        $event = new ContractorUserCreatedEvent($contractorUser);
        $this->dispatcher->dispatch('contractor_user.deleted', $event);
    }

    /**
     * @param ContractorUser $contractorUser
     * @return \AppBundle\Entity\Licenses[]|array
     */
    public function getContractorUserLicenses(ContractorUser $contractorUser)
    {
        return $this->licensesRepository->findBy(["contractorUser" => $contractorUser]);
    }

    /**
     * @return ContractorUser
     */
    public function getContractorUserByCurrentUser()
    {
        $userLoggedIn = $this->tokenStorage->getToken()->getUser();

        /** @var ContractorUser $contractorUser */
        $contractorUser = $this->contractorUserRepository->findOneBy(
            [
                'user' => $userLoggedIn,
            ]
        );

        return $contractorUser;
    }

    /**
     * @param string $msOutlookEmail
     * @return ContractorUser
     */
    public function getContractorUserByMicrosoftOutlookEmail(string $msOutlookEmail)
    {
        /** @var ContractorUser $contractorUser */
        $contractorUser = $this->contractorUserRepository->findOneBy(
            [
                'msOutlookEmail' => $msOutlookEmail,
            ]
        );

        return $contractorUser;
    }

    /**
     * @param array $workorders
     * @return array $result
     */
    public function getContractorUsersGroupedByWorkorder($workorders)
    {
        $result = [];
        /** @var Workorder $workorder */
        foreach ($workorders as $workorder) {
            /** @var Event $event */
            foreach ($workorder->getEvents() as $event) {
                $result[$workorder->getId()][$event->getId()] = $event;
            }
        }

        return $result;
    }
}
