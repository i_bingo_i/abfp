<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Service;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\Device;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Services\Images;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use AppBundle\Events\DeviceCreatedEvent;
use AppBundle\Events\DeviceDeletedEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class DeviceManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var  Images */
    private $imageService;
    /** @var DeviceHistoryManager */
    private $deviceHistoryManager;
    /** @var ServiceManager */
    private $serviceManager;
    private $dispatcher;
    /** @var TokenStorage */
    private $tokenStorage;

    /**
     * DeviceManager constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param ObjectManager $objectManager
     * @param Images $imageService
     * @param DeviceHistoryManager $deviceHistoryManager
     * @param ServiceManager $serviceManager
     * @param TokenStorage $tokenStorage
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        ObjectManager $objectManager,
        Images $imageService,
        DeviceHistoryManager $deviceHistoryManager,
        ServiceManager $serviceManager,
        TokenStorage $tokenStorage
    ) {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;

        $this->deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
        $this->deviceRepository = $this->objectManager->getRepository('AppBundle:Device');
        $this->imageService = $imageService;
        $this->deviceHistoryManager = $deviceHistoryManager;
        $this->serviceManager = $serviceManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Device $device
     */
    public function save(Device $device)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $device->setLastEditor($user);
        $device->setTitle($this->getTitle($device));

        $this->objectManager->persist($device);
        $this->objectManager->flush();
    }

    /**
     * @param Device $device
     */
    public function create(Device $device)
    {
        $this->save($device);
        /** @var DeviceCreatedEvent $event */
        $event = new DeviceCreatedEvent($device);
        $this->dispatcher->dispatch('device.created', $event);
    }

    /**
     * @param DeviceNamed $deviceNamed
     * @return Device
     */
    public function createByDeviceNamed(DeviceNamed $deviceNamed, Device $parent = null)
    {
        $newDevice = new Device();
        if ($parent) {
            $newDevice->setParent($parent);
        }
        $newDevice->setNamed($deviceNamed);
        $this->objectManager->persist($newDevice);
        /** @var DynamicField $fields */
        $fields = $deviceNamed->getFields();
        foreach ($fields as $field) {
            $newValue = new DynamicFieldValue();
            $newValue->setDevice($newDevice);
            $newValue->setField($field);
            $newDevice->addField($newValue);
            $this->objectManager->persist($newValue);
        }

        return $newDevice;
    }

    /**
     * @param Device $device
     * @param $devicePhoto
     * @return Device
     */
    public function saveImageFromFrom(Device $device, $devicePhoto)
    {
        $saveImagePath = "/uploads/devices/";
        $devicePhotoName = $this->imageService->saveUploadImage($devicePhoto, $saveImagePath);
        $device->setPhoto($saveImagePath . $devicePhotoName);

        return $device;
    }

    /**
     * @param Device $device
     * @param $rootDir
     * @return Device
     */
    public function removeDevicePhoto(Device $device, $rootDir)
    {
        $fullFilePath = $rootDir. '/../web' . $device->getPhoto();
        if (file_exists($fullFilePath)) {
            unlink($fullFilePath);
        }
        $device->setPhoto('');

        return $device;
    }

    /**
     * @param Device $device
     * * Current user **
     * @param User $user
     */
    public function delete(Device $device, User $user)
    {
        $children = $this->deviceRepository->findBy(['parent' => $device->getId()]);

        /** Delete service by DeviceId */
        $this->serviceManager->deleteFromDevice($device, $user);
        $device->setDeleted(true);
        $this->save($device);
        $this->deviceHistoryManager->createDeviceHistoryItem($device);

        if (isset($children)) {
            foreach ($children as $child) {
                $this->delete($child, $user);
            }
        }

        /** @var DeviceDeletedEvent $event */
        $event = new DeviceDeletedEvent($device);
        $this->dispatcher->dispatch('device.deleted', $event);
    }

    /**
     * @param Device $device
     * @return Device
     */
    public function serialize(Device $device)
    {
        $device->subDevicesCount = $this->getSubDevicesCount($device);
        $device->allServicesCount = $this->getAllServicesCount($device);
        $device->deviceLevel = $this->getDeviceLevel($device);

        // TODO: виправити бо це таки костиль, потрібно для імпорту змінити логіку і тоді ці строки будуть не потрібні
        $title = $this->getTitle($device);
        $device->setTitle($title);

        if (count($device->getChildren())) {
            $device = $this->serializeSubDevices($device);
        }

        /**
         * @var int $key
         * @var Service $service
         */
        foreach ($device->getServices() as $key => $service)
        {
            $device->getServices()[$key] = $this->serviceManager->serialize($service);
        }

        return $device;
    }

    /**
     * @param Device $device
     * @return Device[]
     */
    public function getTreeForDevice(Device $device)
    {
        /** @var Device[] $deviceTree */
        $deviceTree = [];
        /** @var Device $rootDevice */
        $rootDevice = $this->getRootDevice($device);
        $serializedRootDevice = $this->serialize($rootDevice);

        $deviceTree[] = $serializedRootDevice;
        foreach ($serializedRootDevice->getChildren() as $subDevice)
        {
            $deviceTree[] = $subDevice;
            foreach ($subDevice->getChildren() as $subSubDevice) {
                $deviceTree[] = $subSubDevice;
            }
        }

        return $deviceTree;
    }

    /**
     * @param Device $device
     * @return string
     */
    public function getTitle(Device $device)
    {
        $title = $device->getNamed()->getName();
        $deviceCategoryAlias = $device->getNamed()->getCategory()->getAlias();

        if ($deviceCategoryAlias == 'backflow') {
            /** @var DynamicFieldValue $dynamicFieldValue */
            foreach ($device->getFields() as $dynamicFieldValue) {
                if ($dynamicFieldValue->getField()->getAlias() == 'type' and $dynamicFieldValue->getOptionValue()) {
                    $title = $dynamicFieldValue->getOptionValue()->getOptionValue();
                    break;
                }
            }
        }

//        if ($deviceCategoryAlias == 'fire' && $title == 'Fire Extinguishers') {
//            /** @var DynamicFieldValue $dynamicFieldValue */
//            foreach ($device->getFields() as $dynamicFieldValue) {
//                if ($dynamicFieldValue->getField()->getAlias() == 'number_of_extinguishers'
//                    and $dynamicFieldValue->getValue()
//                ) {
//                    $title = $dynamicFieldValue->getValue() .' extinguisher(s)';
//                    break;
//                }
//            }
//        }

        return $title;
    }

    /**
     * @param Device $device
     * @return Device
     */
    private function serializeSubDevices(Device $device)
    {
        foreach ($device->getChildren() as $key => $child) {
            $device->getChildren()[$key] = $this->serialize($child);
        }

        return $device;
    }

    /**
     * @param Device $device
     * @return int
     */
    private function getSubDevicesCount(Device $device)
    {
        $subDevicesCount = 0;
        foreach ($device->getChildren() as $subDevice) {
            $subDevicesCount++;
            $subSumDeviceCount = count($subDevice->getChildren());
            $subDevicesCount += $subSumDeviceCount;
        }

        return $subDevicesCount;
    }

    /**
     * @param Device $device
     * @return int
     */
    private function getAllServicesCount(Device $device)
    {
        $servicesCount = count($device->getServices());
        foreach ($device->getChildren() as $subDevice) {
            $servicesCount += count($subDevice->getServices());
        }

        return $servicesCount;
    }

    /**
     * @param Device $device
     * @return int
     */
    public function getDeviceLevel(Device $device)
    {
        $deviceLevel = 0;
        $parent = $device->getParent();
        if ($parent) {
            $deviceLevel = $parent->getParent() ? 2 : 1 ;
        }

        return $deviceLevel;
    }

    /**
     * @param Device $device
     * @return Device
     */
    private function getRootDevice(Device $device)
    {
        /** @var Device $root */
        $root = $device;
        if ($device->getParent()) {
            $root = $device->getParent()->getParent() ? $device->getParent()->getParent() : $device->getParent() ;
        }

        return $root;
    }

    /**
     * @param Device $device
     */
    public function detachAlarmPanel(Device $device)
    {
        if ($device->getNamed()->getAlias() == 'fire_alarm_control_panel') {
            /** @var Device $deviceWithAlarm */
            foreach ($device->getDevicesWithAlarmPanel() as $deviceWithAlarm) {
                $deviceWithAlarm->setAlarmPanel();
                $this->save($deviceWithAlarm);
            }
        }
    }

    /**
     * @param Device $device
     * @return bool
     */
    public function isExistNotSelectedService(Device $device)
    {
        return !empty($device->getServices()->filter(
            function (Service $service) {
                return $service->getNamed()->getFrequency() == null
                    && empty($service->getDeleted())
                    && !in_array($service->getStatus()->getAlias(), ['resolved', 'discarded'])
                    && empty($service->getProposal())
                    && empty($service->getWorkorder());
            }
        )->count());
    }
}
