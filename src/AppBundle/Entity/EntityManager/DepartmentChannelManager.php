<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\FeesBasis;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\FeesBasisRepository;
use AppBundle\Entity\Service;
use AppBundle\Events\DepartmentChannelCreatedEvent;
use AppBundle\Services\Proposal\Division;
use AppBundle\Services\Proposal\ProposalService;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ChannelNamed;
use AppBundle\Entity\Agent;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\ChannelDynamicField;
use AppBundle\Entity\DepartmentChannelDynamicFieldValue;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\DepartmentChannelRepository;
use AppBundle\Entity\Department;
use AppBundle\Entity\EntityTrait\PhoneFilterTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DepartmentChannelManager
{
    use PhoneFilterTrait;

    /** @var  ObjectManager */
    private $objectManager;
    /** @var DeviceCategoryRepository */
    private $devisionRepository;
    /** @var DepartmentChannelRepository */
    private $departmentChannelRepository;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var Division */
    private $divisionService;
    /** @var FeesBasisRepository */
    private $feesBasicRepository;


    /**
     * DepartmentChannelManager constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param ObjectManager $objectManager
     * @param Division $divisionService
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        ObjectManager $objectManager,
        Division $divisionService
    ) {
        $this->dispatcher = $dispatcher;
        $this->objectManager = $objectManager;
        $this->devisionRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
        $this->departmentChannelRepository = $this->objectManager->getRepository('AppBundle:DepartmentChannel');
        $this->feesBasicRepository = $this->objectManager->getRepository('AppBundle:FeesBasis');
        $this->divisionService = $divisionService;
    }

    /**
     * @param DepartmentChannel $departmentChannel
     */
    public function save(DepartmentChannel $departmentChannel)
    {
        $this->objectManager->persist($departmentChannel);
        $this->objectManager->flush();
    }

    /**
     * @param DepartmentChannel $departmentChannel
     */
    public function delete(DepartmentChannel $departmentChannel)
    {
        $this->objectManager->remove($departmentChannel);
        $this->objectManager->flush();
    }

    /**
     * @param DepartmentChannel $departmentChannel
     */
    public function create(DepartmentChannel $departmentChannel)
    {
        $this->save($departmentChannel);
        /** @var DepartmentChannelCreatedEvent $event */
        $event = new DepartmentChannelCreatedEvent($departmentChannel);
        $this->dispatcher->dispatch('department_channel.created', $event);
    }

    /**
     * @param ChannelNamed $channelNamed
     * @param Department $department
     * @return DepartmentChannel
     */
    public function createByChannelNamedAndDepartment(ChannelNamed $channelNamed, Department $department)
    {
        $newDepartmentChannel = new DepartmentChannel();
        $newDepartmentChannel->setNamed($channelNamed);
        $newDepartmentChannel->setDevision($department->getDivision());
        $newDepartmentChannel->setDepartment($department);
        $this->objectManager->persist($newDepartmentChannel);
        $fields = $channelNamed->getFields();
        /** @var ChannelDynamicField $field */
        foreach ($fields as $field) {
            $newValue = new DepartmentChannelDynamicFieldValue();
            $newValue->setDepartmentChanel($newDepartmentChannel);
            $newValue->setField($field);
            $newDepartmentChannel->addField($newValue);
            $this->objectManager->persist($newValue);
        }
        return $newDepartmentChannel;
    }

    /**
     * @param Department $department
     * @param DepartmentChannel $newDepartmentChannel
     * @return DepartmentChannel
     */
    public function firstChannelIsDefault(
        Department $department,
        DepartmentChannel $newDepartmentChannel,
        Agent $agent = null
    ) {
        $channels = $this->departmentChannelRepository->findBy([
            "department" => $department,
            "ownerAgent" =>$agent,
            "deleted" => false
        ]);
        if (!$channels) {
            $newDepartmentChannel->setIsDefault(true);
        }

        return $newDepartmentChannel;
    }

    /**
     * @param ChannelNamed $channelNamed
     * @param Department $department
     * @param Agent $agent
     * @return DepartmentChannel
     */
    public function createByChannelNamedAndDepartmentForAgent(
        ChannelNamed $channelNamed,
        Department $department,
        Agent $agent
    ) {
        /** @var DepartmentChannel $newDepartmentAgentChannel */
        $newDepartmentAgentChannel = $this->createByChannelNamedAndDepartment($channelNamed, $department);
        $newDepartmentAgentChannel->setOwnerAgent($agent);
        $this->objectManager->persist($newDepartmentAgentChannel);

        return $newDepartmentAgentChannel;
    }

    /**
     * @param Department $department
     * @param Agent|null $agent
     */
    public function unDefaultDepartmentChannels(Department $department, Agent $agent = null)
    {
        /** @var []DepartmentChannel $defaultDepartmentChannels */
        $defaultDepartmentChannels = $this->departmentChannelRepository->findBy([
                "department" => $department,
                "ownerAgent" => $agent,
                "isDefault" => true,
            ]);
        /** @var DepartmentChannel $channel */
        foreach ($defaultDepartmentChannels as $channel) {
            $channel->setIsDefault(false);
            $this->objectManager->persist($channel);
        }
        $this->objectManager->flush();
    }

    /**
     * @param DepartmentChannel $departmentChannel
     * @return DepartmentChannel
     */
    public function filteringFax(DepartmentChannel $departmentChannel)
    {
        if ($departmentChannel->getNamed()->getAlias() == "fax") {
            /** @var DepartmentChannelDynamicFieldValue $field */
            foreach ($departmentChannel->getFields() as $field) {
                $field->setValue($this->filterPhone($field->getValue()));
            }
        }

        return $departmentChannel;
    }

    /**
     * @param Service $service
     * @return DepartmentChannel
     */
    public function getChannelByDivision(Service $service)
    {
        $departments = $service->getAccount()->getMunicipality()->getDepartment();
        $division = $this->divisionService->temporaryForAlarmAndPlumbingDivisions(
            $service->getDevice()->getNamed()->getCategory()
        );

        /** @var Department $department */
        foreach ($departments as $department) {
            if ($department->getDivision()->getId() == $division->getId()) {
                /** @var DepartmentChannel $channel */
                foreach ($department->getChannels() as $channel) {
                    if ($channel->getIsDefault()) {
                        return $channel;
                    }
                }
            }
        }
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $deviceCategory
     * @return mixed|null
     */
    public function getDefaultChannel(Municipality $municipality, DeviceCategory $deviceCategory)
    {
        $defaultChannelWithoutAgent =
            $this->departmentChannelRepository->getDefaultChannelWithoutAgent($municipality, $deviceCategory);

        $defaultChannelWithAgent =
            $this->departmentChannelRepository->getDefaultChannelWithAgent($municipality, $deviceCategory);

        if ($defaultChannelWithAgent instanceof DepartmentChannel) {
            return $defaultChannelWithAgent;
        }

        if ($defaultChannelWithoutAgent instanceof DepartmentChannel) {
            return $defaultChannelWithoutAgent;
        }

        return null;
    }

    /**
     * @param Department $department
     */
    public function allUnActiveByDepartment(Department $department)
    {
        $this->setFieldsActiveByDepartment(false, $department);
    }

    /**
     * @param Department $department
     */
    public function allActiveByDepartment(Department $department)
    {
        $this->setFieldsActiveByDepartment(true, $department);
    }

    /**
     * @param Department $department
     * @param ChannelNamed $channelNamed
     * @return DepartmentChannel
     */
    public function prepareToCreate(Department $department, ChannelNamed $channelNamed)
    {
        /** @var DepartmentChannel $departmentChannel */
        $departmentChannel = $this->createByChannelNamedAndDepartment(
            $channelNamed,
            $department
        );
        $departmentChannel = $this->firstChannelIsDefault(
            $department,
            $departmentChannel
        );

        $departmentChannel = $this->setDefaultFeesBasicForDepartment($departmentChannel);

        return $departmentChannel;
    }

    /**
     * @param Department $department
     * @param ChannelNamed $channelNamed
     * @param Agent $agent
     * @return DepartmentChannel
     */
    public function prepareToCreateForAgent(Department $department, ChannelNamed $channelNamed, Agent $agent)
    {
        /** @var DepartmentChannel $departmentChannel */
        $departmentChannel = $this->createByChannelNamedAndDepartmentForAgent(
            $channelNamed,
            $department,
            $agent
        );

        $departmentChannel = $this->firstChannelIsDefault(
            $department,
            $departmentChannel,
            $agent
        );

        $departmentChannel = $this->setDefaultFeesBasicForDepartment($departmentChannel);

        return $departmentChannel;
    }

    /**
     * @param DepartmentChannel $departmentChannel
     * @return DepartmentChannel
     */
    public function setDefaultFeesBasicForDepartment(DepartmentChannel $departmentChannel)
    {
        /** @var FeesBasis $defaultFeesBasic */
        $defaultFeesBasic = $this->feesBasicRepository->findOneBy(['alias' => 'per device']);
        /** @var DepartmentChannel $departmentChannel */
        $departmentChannel->setFeesBasis($defaultFeesBasic);

        return $departmentChannel;
    }

    /**
     * @param $departmentChannel
     * @return float|int
     */
    public function municipalityFee($departmentChannel)
    {
        $municipalityFee = 0;
        if (!$departmentChannel) {
            return $municipalityFee;
        }

        $processingFee = $departmentChannel->getProcessingFee();
        if ($processingFee) {
            $municipalityFee += (float)$processingFee;
        }

        $uploadFee = $departmentChannel->getUploadFee();
        if ($uploadFee) {
            $municipalityFee += (float)$uploadFee;
        }

        return $municipalityFee;
    }

    /**
     * @param Department $department
     * @return bool
     */
    public function checkIfDepartmentChannelsHaveFeesWillVary(Department $department)
    {
        $departmentChannels = $this->departmentChannelRepository->getDepartmentChannels($department);

        foreach ($departmentChannels as $channel) {
            if ($channel->getFeesWillVary()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $deviceCategory
     * @return DepartmentChannel|bool
     */
    public function getDefaultDepartmentChannelWithFeesWillVary(
        Municipality $municipality,
        DeviceCategory $deviceCategory
    ) {
        /** @var DepartmentChannel $channel */
        $channel = $this->getDefaultChannel($municipality, $deviceCategory);

        if ($channel->getFeesWillVary()) {
            return $channel;
        }

        return false;
    }

    /**
     * @param Municipality $municipality
     * @param DeviceCategory $deviceCategory
     * @return bool
     */
    public function checkIfDefaultDepartmentChannelHasFeesWillVary(
        Municipality $municipality,
        DeviceCategory $deviceCategory
    ) {
        if ($this->getDefaultDepartmentChannelWithFeesWillVary($municipality, $deviceCategory)) {
            return true;
        }

        return false;
    }

    /**
     * @param DepartmentChannel $departmentChannel
     */
    public function deleteChannel(DepartmentChannel $departmentChannel)
    {
        $departmentChannel->setDeleted(true);
        $this->save($departmentChannel);
    }

    /**
     * @param $value
     * @param Department $department
     */
    private function setFieldsActiveByDepartment($value, Department $department)
    {
        /** @var DepartmentChannel[] $channels */
        $channels = $department->getChannels();
        /** @var DepartmentChannel $channel */
        foreach ($channels as $channel) {
            $channel->setActive($value);
            $this->objectManager->persist($channel);
        }

        $this->objectManager->flush();
    }
}
