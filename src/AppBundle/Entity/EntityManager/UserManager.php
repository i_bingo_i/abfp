<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\UserAccessToken;

class UserManager
{
    public const ROLES = ['super_admin' => 'ROLE_SUPER_ADMIN'];

    /** @var ObjectManager  */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var UserAccessToken */
    private $accessTokenService;

    /**
     * UserManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;

        $this->accessTokenService = $this->serviceContainer->get('api.user_access_token');
    }

    /**
     * @param User $user
     */
    public function save(User $user)
    {
        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }

    /**
     * @param User $user
     */
    public function delete(User $user)
    {
        $token = $this->accessTokenService->createUserAccessToken($user);

        $user->setDeleted(true);
        $user->setEnabled(false);
        $user->setAccessToken($token);

        $this->save($user);
    }

    /**
     * @param User $user
     * @param $role
     */
    public function setRoles(User $user, $role = self::ROLES['super_admin'])
    {
        $user->addRole($role);
    }

    /**
     * @param User $user
     */
    public function setAccessToken(User $user)
    {
        $token = $this->accessTokenService->createUserAccessToken($user);
        $user->setAccessToken($token);
    }
}
