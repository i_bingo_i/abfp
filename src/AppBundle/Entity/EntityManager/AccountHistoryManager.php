<?php

namespace AppBundle\Entity\EntityManager;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountHistory;
use AppBundle\Entity\User;

class AccountHistoryManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var TokenStorage */
    private $tokenStorage;

    /**
     * AccountHistoryManager constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorage $tokenStorage
     */
    public function __construct(ObjectManager $objectManager, TokenStorage $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Account $account
     */
    public function createAccountHistoryItem(Account $account, $flushFlag = 1)
    {
        $token = $this->tokenStorage->getToken();
        $user = null;
        if (!empty($token) and gettype($token->getUser()) != "string") {
            /** @var User $user */
            $user = $token->getUser();
        }

        /** @var AccountHistory $newHistoryItem */
        $newHistoryItem = new AccountHistory();
        $newHistoryItem->setName($account->getName());
        $newHistoryItem->setAuthor($user);
        $newHistoryItem->setOwnerEntity($account);
        $newHistoryItem->setParent($account->getParent());
        $newHistoryItem->setType($account->getType());
        $newHistoryItem->setClientType($account->getClientType());
        $newHistoryItem->setCompany($account->getCompany());
        $newHistoryItem->setAddress($account->getAddress());
        $newHistoryItem->setDateSave($account->getDateUpdate());
        $newHistoryItem->setNotes($account->getNotes());
        $newHistoryItem->setCompany($account->getCompany());
        $newHistoryItem->setSpecialDiscount($account->getSpecialDiscount());
        $newHistoryItem->setDateCreate($account->getDateCreate());
        $newHistoryItem->setDateUpdate($account->getDateUpdate());
        $newHistoryItem->setPaymentTerm($account->getPaymentTerm());
        $newHistoryItem->setMunicipality($account->getMunicipality());

        $this->objectManager->persist($newHistoryItem);
        if ($flushFlag) {
            $this->objectManager->flush();
        }
    }
}
