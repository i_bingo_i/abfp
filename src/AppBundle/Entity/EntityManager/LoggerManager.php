<?php

namespace AppBundle\Entity\EntityManager;

class LoggerManager
{
    const ENTITY_NAMES = [
        'Workorder Scheduling' => 'workorder',
        'Failed Requests' => 'fail_request'
    ];
}
