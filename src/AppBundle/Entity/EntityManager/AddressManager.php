<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Address;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AddressManager
{
    public const TYPE_SPECIAL_BILLING_ADDRESS = "special billing address";

    /** @var  ObjectManager */
    private $objectManager;
    /** @var \AppBundle\Entity\Repository\AddressTypeRepository */
    private $addressTypeRepository;
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /**
     * AddressManager constructor.
     * @param ObjectManager $objectManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(ObjectManager $objectManager, EventDispatcherInterface $dispatcher)
    {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;

        $this->addressTypeRepository = $this->objectManager->getRepository('AppBundle:AddressType');
    }

    /**
     * @param Address $address
     * @return bool
     */
    public function isInvalid(Address $address)
    {
        if (empty($address->getAddress())
            or empty($address->getCity())
            or empty($address->getState())
            or empty($address->getZip())
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param Address $address
     * @param string $status
     */
    public function setType(Address $address, string $status)
    {
        $accountTypeSite = $this->addressTypeRepository->findOneBy(["alias" => $status]);
        $address->setAddressType($accountTypeSite);
    }

    /**
     * @param Address $address
     */
    public function save(Address $address)
    {
        $this->objectManager->persist($address);
        $this->objectManager->flush();
    }
}
