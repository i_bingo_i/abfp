<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\File;
use AppBundle\Factories\FileFactory;
use Doctrine\ORM\EntityManager;
use AppBundle\Services\Files;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManager
{
    private $uploadSavePath;
    /** @var  EntityManager */
    private $entityManager;
    /** @var Files */
    private $fileService;

    /**
     * FileManager constructor.
     * @param EntityManager $entityManager
     * @param Files $fileService
     * @param $uploadSavePath
     */
    public function __construct(EntityManager $entityManager, Files $fileService, $uploadSavePath)
    {
        $this->entityManager = $entityManager;
        $this->uploadSavePath = $uploadSavePath;
        $this->fileService = $fileService;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param string $prefix
     * @param string $newName
     * @return File
     */
    public function createByUpload(UploadedFile $uploadedFile, $prefix = '', $newName = '')
    {
        if ($newName) {
            $fileName = $newName;
        } else {
            $fileName = $this->fileService->getRandomFileName($uploadedFile, $prefix);
        }

        $newFileName = $this->fileService->saveUploadFile($uploadedFile, $this->uploadSavePath, $fileName);
        $filePath = $this->uploadSavePath.$newFileName;

        /** @var File $file */
        $file = FileFactory::make($filePath, $this->fileService->getExistFileSize($filePath));

        return $file;
    }
}
