<?php

namespace AppBundle\Entity\EntityManager;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\ContactPersonHistory;
use AppBundle\Entity\User;

class ContactPersonHistoryManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var TokenStorage */
    private $tokenStorage;

    /**
     * ContactPersonHistoryManager constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorage $tokenStorage
     */
    public function __construct(ObjectManager $objectManager, TokenStorage $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param ContactPerson $contactPerson
     */
    public function createContactPersonHistoryItem(ContactPerson $contactPerson, $flushFlag = 1)
    {
        $token = $this->tokenStorage->getToken();
        $user = null;
        if (!empty($token) and gettype($token->getUser()) != "string") {
            /** @var User $user */
            $user = $token->getUser();
        }

        /** @var ContactPersonHistory $newHistoryItem */
        $newHistoryItem = new ContactPersonHistory();
        $newHistoryItem->setAuthor($user);
        $newHistoryItem->setOwnerEntity($contactPerson);
        $newHistoryItem->setFirstName($contactPerson->getFirstName());
        $newHistoryItem->setLastName($contactPerson->getLastName());
        $newHistoryItem->setTitle($contactPerson->getTitle());
        $newHistoryItem->setEmail($contactPerson->getEmail());
        $newHistoryItem->setCell($contactPerson->getCell());
        $newHistoryItem->setCod($contactPerson->getCod());
        $newHistoryItem->setExt($contactPerson->getExt());
        $newHistoryItem->setPhone($contactPerson->getPhone());
        $newHistoryItem->setNotes($contactPerson->getNotes());
        $newHistoryItem->setFax($contactPerson->getFax());
        $newHistoryItem->setCompany($contactPerson->getCompany());
        $newHistoryItem->setDateCreate($contactPerson->getDateCreate());
        $newHistoryItem->setDateUpdate($contactPerson->getDateUpdate());

        $this->objectManager->persist($newHistoryItem);
        if ($flushFlag) {
            $this->objectManager->flush();
        }
    }
}
