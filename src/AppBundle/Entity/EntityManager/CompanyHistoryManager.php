<?php

namespace AppBundle\Entity\EntityManager;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyHistory;
use AppBundle\Entity\User;

class CompanyHistoryManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var  TokenStorage */
    private $tokenStorage;

    /**
     * CompanyHistoryManager constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorage $tokenStorage
     */
    public function __construct(ObjectManager $objectManager, TokenStorage $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Company $company
     * @param int $flushFlag
     */
    public function createCompanyHistoryItem(Company $company, $flushFlag = 1)
    {
        $token = $this->tokenStorage->getToken();
        $user = null;
        if (!empty($token) and gettype($token->getUser()) != "string") {
            /** @var User $user */
            $user = $token->getUser();
        }

        /** @var CompanyHistory $newHistoryItem */
        $newHistoryItem = new CompanyHistory();
        $newHistoryItem->setAuthor($user);
        $newHistoryItem->setOwnerEntity($company);
        $newHistoryItem->setName($company->getName());
        $newHistoryItem->setAddress($company->getAddress());
        $newHistoryItem->setWebsite($company->getWebsite());
        $newHistoryItem->setDeleted($company->getDeleted());
        $newHistoryItem->setNotes($company->getNotes());
        $newHistoryItem->setDateCreate($company->getDateCreate());
        $newHistoryItem->setDateUpdate($company->getDateUpdate());

        $this->objectManager->persist($newHistoryItem);
        if ($flushFlag) {
            $this->objectManager->flush();
        }
    }
}
