<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\ClientType;
use AppBundle\Entity\Opportunity;
use AppBundle\Entity\OpportunityType;
use AppBundle\Entity\OpportunityStatus;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Repository\OpportunityRepository;
use AppBundle\Entity\Repository\OpportunityStatusRepository;
use AppBundle\Entity\Repository\OpportunityTypeRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Events\OpportunityCreateEvent;
use AppBundle\Events\OpportunityUpdateEvent;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Service;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OpportunityManager
{
    public const STATUS_PAST_DUE = 'past_due';
    public const STATUS_RN_TO_BE_CREATED = 'rn_to_be_created';
    public const TYPE_RETEST = 'retest';
    public const TYPE_REPAIR = 'repair';

    /** @var ObjectManager  */
    private $objectManager;
    /** @var OpportunityRepository  */
    private $opportunityRepository;
    /** @var AccountRepository */
    private $accountRepository;
    /** @var ClientType $clientTypeRepository  */
    private $clientTypeRepository;
    /** @var OpportunityTypeRepository  */
    private $opportunityTypeRepository;
    /** @var OpportunityStatusRepository  */
    private $opportunityStatusRepository;
    /** @var DeviceCategoryRepository  */
    private $deviceCategoryRepository;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var EventDispatcherInterface  */
    private $dispatcher;
    /** @var AccountContactPersonManager */
    private $accountContactPersonManager;
    /** @var MessageRepository */
    private $messageRepository;

    /**
     * OpportunityManager constructor.
     * @param ObjectManager $objectManager
     * @param EventDispatcherInterface $dispatcher
     * @param AccountContactPersonManager $accountContactPersonManager
     */
    public function __construct(
        ObjectManager $objectManager,
        EventDispatcherInterface $dispatcher,
        AccountContactPersonManager $accountContactPersonManager
    ) {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;
        $this->accountContactPersonManager = $accountContactPersonManager;

        $this->opportunityRepository = $this->objectManager->getRepository('AppBundle:Opportunity');
        $this->opportunityTypeRepository = $this->objectManager->getRepository('AppBundle:OpportunityType');
        $this->opportunityStatusRepository = $this->objectManager->getRepository('AppBundle:OpportunityStatus');
        $this->clientTypeRepository = $this->objectManager->getRepository('AppBundle:ClientType');
        $this->deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->deviceRepository = $this->objectManager->getRepository('AppBundle:Device');
        $this->messageRepository = $this->objectManager->getRepository('AppBundle:Message');
    }

    /**
     * @param Opportunity $opportunity
     */
    public function create(Opportunity $opportunity)
    {
        $this->save($opportunity);
        /** @var OpportunityCreateEvent $event */
        $event = new OpportunityCreateEvent($opportunity);
        $this->dispatcher->dispatch('opportunity.create', $event);
    }

    /**
     * @param Opportunity $opportunity
     */
    public function update(Opportunity $opportunity)
    {
        $this->save($opportunity);
        /** @var OpportunityCreateEvent $event */
        $event = new OpportunityUpdateEvent($opportunity);
        $this->dispatcher->dispatch('opportunity.update', $event);
    }

    /**
     * @param $entity
     */
    public function save($entity)
    {
        $this->objectManager->persist($entity);
        $this->objectManager->flush();
    }

    // TODO: відрефакторити використавши метод setSomeFields для встановлення відповідних полів
    /**
     * @param Service $service
     * @param string $opportunityType
     * @return Opportunity $opportunity
     */
    public function creating(Service $service, $opportunityType = self::TYPE_RETEST)
    {
        /** @var OpportunityTypeRepository $opportunityTypeRepository */
        $opportunityTypeRepository = $this->objectManager->getRepository('AppBundle:OpportunityType');
        /** @var OpportunityStatusRepository $opportunityStatusRepository */
        $opportunityStatusRepository = $this->objectManager->getRepository('AppBundle:OpportunityStatus');
        /** @var OpportunityType $opportunityType */
        $opportunityType = $opportunityTypeRepository->findOneBy(['alisa' => $opportunityType]);
        /** @var \DateTime $serviceDue */
        $serviceDue = $service->getInspectionDue();

        /** @var OpportunityStatus $opportunityStatus */
        $opportunityStatus = $opportunityStatusRepository->findOneBy(['alisa' => self::STATUS_RN_TO_BE_CREATED]);
        /** @var Account $account */
        $account = $service->getAccount();
        $parentAccount =$account->getParent() ?? $account;

        /** @var Opportunity $opportunity */
        $opportunity = new Opportunity();
        $opportunity->setDivision($service->getDevice()->getNamed()->getCategory());
        $opportunity->setAccount($account);
        $opportunity->setParentAccount($parentAccount);
        $opportunity->setType($opportunityType);
        $opportunity->setStatus($opportunityStatus);
        $opportunity->setExecutionServicesDate($serviceDue);
        $deviceCount = $this->getCountDevicesForServices($opportunity);
        $opportunity->setDevicesCount($deviceCount);
        $opportunity->setServicesCount(1);
        $opportunity->setSitesCount($this->accountRepository->getSiteCount($account));
        $opportunity->setPrice($service->getFixedPrice());

        //$this->updateOldestDates($opportunity, $account);

        return $opportunity;
    }

    // TODO: відрефакторити метод і створити ще один метод лише для встановлення цих значень setSomeFields
    /**
     * @param Opportunity $opportunity
     * @return Opportunity
     */
    public function updateSomeFields(Opportunity $opportunity)
    {
        $quantityServices = count($opportunity->getServices());
        $price = $this->calculatePrice($opportunity);
        $deviceCount = $this->getCountDevicesForServices($opportunity);

        $opportunity->setDevicesCount($deviceCount);
        $opportunity->setServicesCount($quantityServices);
        $opportunity->setPrice($price);

        return $opportunity;
    }

    // TODO: перенести цей метод в ServiceManager
    /**
     * @param $opportunity
     * @return int|float
     */
    public function calculatePrice(Opportunity $opportunity)
    {
        $sumFee = 0;

        foreach ($opportunity->getServices() as $oneService) {
            $sumFee += $oneService->getFixedPrice();
        }
        return $sumFee;
    }

    // TODO: перенести цей метод в ServiceManager
    /**
     * @param Opportunity $opportunity
     * @return int
     */
    public function getCountDevicesForServices(Opportunity $opportunity)
    {
        $deviceCount = [];
        /** @var Service $service */
        foreach ($opportunity->getServices() as $service) {
            $deviceId = $service->getDevice()->getId();
            $deviceCount[$deviceId] = '';
        }

        return count($deviceCount);
    }

    /**
     * @param Opportunity $opportunity
     */
    public function delete(Opportunity $opportunity)
    {
        $this->objectManager->remove($opportunity);
        $this->objectManager->flush();
    }

    // TODO: зробити рефакторинг цього методу і взагалі створення опорюніті
    /**
     * @param Account $account
     */
    public function refreshForAccount(Account $account)
    {
        /** @var Opportunity[] $opportunities */
        $opportunities = $this->opportunityRepository->findBy(['account' => $account]);
        /** @var Opportunity $opportunity */
        foreach ($opportunities as $opportunity) {
            /** @var Account $parentAccount */
            $parentAccount = $opportunity->getAccount()->getParent();
            if ($parentAccount) {
                $opportunity->setParentAccount($parentAccount);
            } else {
                $opportunity->setParentAccount($opportunity->getAccount());
            }
            $this->objectManager->persist($opportunity);
        }

        $this->objectManager->flush();
    }

    /**
     * @return array
     */
    public function getInfoForFilters()
    {
        $contentForFilterFields= [];
        $contentForFilterFields["opportunityType"] = $this->opportunityTypeRepository->findAll();
        $contentForFilterFields["opportunityStatus"] = $this->opportunityStatusRepository->findAll();
        $contentForFilterFields["clientType"] = $this->clientTypeRepository->findAll();
        $contentForFilterFields["pagerCount"] = ["50", "100", "300", "500"];
        $contentForFilterFields["division"] = $this->deviceCategoryRepository->findAll();

        return $contentForFilterFields;
    }

    /**
     * @param Opportunity[] $opportunities
     * @return array
     */
    public function groupByAccount($opportunities)
    {
        $result = [];
        /** @var Opportunity $opportunity */
        foreach ($opportunities as $opportunity) {
            $ownerAccount = $opportunity->getAccount();
            if ($ownerAccount->getParent()) {
                $ownerAccount = $ownerAccount->getParent();
            }
            $ownerAccountId = $ownerAccount->getId();

            if (empty($result[$ownerAccountId]['account'])) {
                $result[$ownerAccountId]['account'] = $ownerAccount;
            }

            $opportunity->countAllServices = $this->serviceRepository->getCountByDivisionForAccount(
                $opportunity->getDivision(),
                $opportunity->getAccount()
            );

            $opportunity->countAllDevices = $this->deviceRepository->getCountByDivisionForAccount(
                $opportunity->getDivision(),
                $opportunity->getAccount()
            );

            $result[$ownerAccountId]['opportunity'][] = $opportunity;
        }

        return $result;
    }

    /**
     * Get services for Opportunities array
     * @param Opportunity[] $opportunities
     * @return Service[]
     */
    public function getServicesForArray($opportunities)
    {
        /** @var Service[] $services */
        $services = [];
        /** @var Opportunity $opportunity */
        foreach ($opportunities as $opportunity) {
            /** @var Service[] $opportunityServices */
            $opportunityServices = $opportunity->getServices()->toArray();
            $services = array_merge($services, $opportunityServices);
        }

        return $services;
    }

    /**
     * @param Opportunity $opportunity
     * @param string $status
     * @return Opportunity
     */
    public function setStatus(Opportunity $opportunity, string $status)
    {
        $status = $this->opportunityStatusRepository->findOneBy(['alisa' => $status]);
        $opportunity->setStatus($status);

        return $opportunity;
    }

    /**
     * @param Opportunity $opportunity
     * @param string $type
     * @return OpportunityType|null|object|string
     */
    public function setType(Opportunity $opportunity, string $type)
    {
        $type = $this->opportunityTypeRepository->findOneBy(['alisa' => $type]);
        $opportunity->setType($type);

        return $type;
    }

    /**
     * @param Opportunity $opportunity
     * @return bool
     */
    public function isPastDue(Opportunity $opportunity)
    {
        $executionDate = strtotime($opportunity->getExecutionServicesDate()->format('Y-m-d'));
        $today = strtotime((new \DateTime())->format('Y-m-d'));
        return $executionDate < $today;
    }

    /**
     * @param Service $service
     * @param $opportunityType
     * @return Opportunity
     */
    public function getNowExistingOpportunity(Service $service, $opportunityType)
    {
        $opportunity = null;

        if ($opportunityType == self::TYPE_RETEST) {
            $oportunityType = $this->opportunityTypeRepository->findOneBy(['alisa' => $opportunityType]);

            /** @var Opportunity $opportunity */
            $opportunity = $this->opportunityRepository->findOneBy([
                'account' => $service->getAccount(),
                'division' => $service->getDevice()->getNamed()->getCategory(),
                'executionServicesDate' => $service->getTestDate(),
                'type' => $oportunityType
            ]);
        } elseif ($opportunityType == self::TYPE_REPAIR) {
            /** @var array $services */
            $services = $this->serviceRepository->getWithNonFrequencyOpportunity($service);
            /** @var Opportunity $opportunity */
            $opportunity = count($services) ? $services[0]->getOpportunity() : $opportunity;
        }

        return $opportunity;
    }
}
