<?php

namespace AppBundle\Entity\EntityManager;

use AppBundle\Entity\DeviceNamed;
use Doctrine\Common\Persistence\ObjectManager;

class DeviceNamedManager
{
    /** @var  ObjectManager */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param DeviceNamed $deviceNamed
     */
    public function save(DeviceNamed $deviceNamed)
    {
        $this->objectManager->persist($deviceNamed);
        $this->objectManager->flush();
    }

    /**
     * @param DeviceNamed $deviceNamed
     */
    public function create(DeviceNamed $deviceNamed)
    {
        $deviceNamed->setLevel($this->getLevelByParent($deviceNamed));
        $this->save($deviceNamed);
    }

    /**
     * @param DeviceNamed $deviceNamed
     * @return array|null
     */
    public function getDevicesNamedTree(DeviceNamed $deviceNamed)
    {
        $deviceNamedLevel = $deviceNamed->getLevel();

        $treeOfDeviceNamed = null;

        if ($deviceNamedLevel == 2) {
            $deviceParent = $deviceNamed->getParent();
            $rootParentDevice = $deviceParent->getParent();

            $treeOfDeviceNamed = $this->getChildrenDevicesNamedTree($rootParentDevice);
        } elseif ($deviceNamedLevel == 1) {
            $rootParentDevice = $deviceNamed->getParent();

            $treeOfDeviceNamed = $this->getChildrenDevicesNamedTree($rootParentDevice);
        } else {
            $treeOfDeviceNamed = $this->getChildrenDevicesNamedTree($deviceNamed);
        }

        return $treeOfDeviceNamed;
    }

    /**
     * @param DeviceNamed $deviceNamed
     * @return array
     */
    public function getChildrenDevicesNamedTree(DeviceNamed $deviceNamed)
    {
        $treeOfDeviceNamed = array();
        $treeOfDeviceNamed[] = $deviceNamed;

        /** @var DeviceNamed $childFirstLevel */
        foreach ($deviceNamed->getChildren() as $childFirstLevel) {
            $treeOfDeviceNamed[] = $childFirstLevel;

            foreach ($childFirstLevel->getChildren() as $childSecondLevel) {
                $treeOfDeviceNamed[] = $childSecondLevel;
            }
        }

        return $treeOfDeviceNamed;
    }

    /**
     * @param DeviceNamed $deviceNamed
     * @return int
     */
    private function getLevelByParent(DeviceNamed $deviceNamed)
    {
        $parent = $deviceNamed->getParent();
        $currentLevel = 0;

        if ($parent) {
            $parentLevel = $parent->getLevel();
            $currentLevel = $parentLevel + 1;
        }

        return $currentLevel;
    }
}
