<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
/**
 * DynamicFieldDropboxChoices
 */
class DynamicFieldDropboxChoices
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $optionValue;

    /**
     * @var boolean
     */
    private $selectDefault = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\DynamicField
     */
    private $field;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optionValue
     *
     * @param string $optionValue
     *
     * @return DynamicFieldDropboxChoices
     */
    public function setOptionValue($optionValue)
    {
        $this->optionValue = $optionValue;

        return $this;
    }

    /**
     * Get optionValue
     *
     * @return string
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }

    /**
     * Set selectDefault
     *
     * @param boolean $selectDefault
     *
     * @return DynamicFieldDropboxChoices
     */
    public function setSelectDefault($selectDefault)
    {
        $this->selectDefault = $selectDefault;

        return $this;
    }

    /**
     * Get selectDefault
     *
     * @return boolean
     */
    public function getSelectDefault()
    {
        return $this->selectDefault;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return DynamicFieldDropboxChoices
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return DynamicFieldDropboxChoices
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set field
     *
     * @param \AppBundle\Entity\DynamicField $field
     *
     * @return DynamicFieldDropboxChoices
     */
    public function setField(\AppBundle\Entity\DynamicField $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \AppBundle\Entity\DynamicField
     */
    public function getField()
    {
        return $this->field;
    }
    /**
     * @var string
     */
    private $alias;


    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return DynamicFieldDropboxChoices
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }
}
