<?php

namespace AppBundle\Entity;

/**
 * ProposalLogAction
 */
class ProposalLogAction
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $link;

    /**
     * @var boolean
     */
    private $isActive = false;

    /**
     * @var \AppBundle\Entity\ProposalLogActionNamed
     */
    private $named;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProposalLogAction
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return ProposalLogAction
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return ProposalLogAction
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return ProposalLogAction
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\ProposalLogActionNamed $named
     *
     * @return ProposalLogAction
     */
    public function setNamed(\AppBundle\Entity\ProposalLogActionNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\ProposalLogActionNamed
     */
    public function getNamed()
    {
        return $this->named;
    }
}
