<?php

namespace AppBundle\Entity;

/**
 * DeviceHistory
 */
class DeviceHistory
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var string
     */
    private $noteToTester;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fields;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \AppBundle\Entity\DeviceNamed
     */
    private $named;

    /**
     * @var \AppBundle\Entity\DeviceStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $ownerEntity;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $parent;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return DeviceHistory
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return DeviceHistory
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return DeviceHistory
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set noteToTester
     *
     * @param string $noteToTester
     *
     * @return DeviceHistory
     */
    public function setNoteToTester($noteToTester)
    {
        $this->noteToTester = $noteToTester;

        return $this;
    }

    /**
     * Get noteToTester
     *
     * @return string
     */
    public function getNoteToTester()
    {
        return $this->noteToTester;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return DeviceHistory
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return DeviceHistory
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return DeviceHistory
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Add field
     *
     * @param \AppBundle\Entity\DynamicFieldValueHistory $field
     *
     * @return DeviceHistory
     */
    public function addField(\AppBundle\Entity\DynamicFieldValueHistory $field)
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * Remove field
     *
     * @param \AppBundle\Entity\DynamicFieldValueHistory $field
     */
    public function removeField(\AppBundle\Entity\DynamicFieldValueHistory $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Device $child
     *
     * @return DeviceHistory
     */
    public function addChild(\AppBundle\Entity\Device $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Device $child
     */
    public function removeChild(\AppBundle\Entity\Device $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\DeviceNamed $named
     *
     * @return DeviceHistory
     */
    public function setNamed(\AppBundle\Entity\DeviceNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\DeviceNamed
     */
    public function getNamed()
    {
        return $this->named;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\DeviceStatus $status
     *
     * @return DeviceHistory
     */
    public function setStatus(\AppBundle\Entity\DeviceStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\DeviceStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return DeviceHistory
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set ownerEntity
     *
     * @param \AppBundle\Entity\Device $ownerEntity
     *
     * @return DeviceHistory
     */
    public function setOwnerEntity(\AppBundle\Entity\Device $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \AppBundle\Entity\Device
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Device $parent
     *
     * @return DeviceHistory
     */
    public function setParent(\AppBundle\Entity\Device $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Device
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return DeviceHistory
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
