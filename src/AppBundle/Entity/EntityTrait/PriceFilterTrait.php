<?php

namespace AppBundle\Entity\EntityTrait;

trait PriceFilterTrait
{
    /**
     * @param $price
     * @return float
     */
    public function filterPrice($price)
    {
        return (float)str_replace(',', '.', str_replace('$', '', $price));
    }
}
