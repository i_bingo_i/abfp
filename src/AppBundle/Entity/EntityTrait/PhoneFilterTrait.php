<?php

namespace AppBundle\Entity\EntityTrait;

trait PhoneFilterTrait
{
    /**
     * @param string $phone
     * @return mixed
     */
    public function filterPhone($phone)
    {
        return str_replace(["(", ")", "-", " "], "", $phone);
    }
}
