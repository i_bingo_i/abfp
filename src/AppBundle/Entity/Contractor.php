<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\PhoneFilterTrait;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contractor
 *
 * Added code filter for phone number (don't auto generate this entity)
 */
class Contractor
{
    use PhoneFilterTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $website;

    /**
     * @var string
     */
    private $stateLicenseNumber;

    /**
     * @var \DateTime
     */
    private $stateLicenseExpirationDate;

    /**
     * @var \DateTime
     */
    private $coiExpiration;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $address;

    /**
     * @var \AppBundle\Entity\ContractorType
     */
    private $type;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $oldSistemId;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var string
     */
    private $email;

    /**
     * @var boolean
     */
    private $isHistoryProcessed = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contractor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Contractor
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set stateLicenseNumber
     *
     * @param string $stateLicenseNumber
     *
     * @return Contractor
     */
    public function setStateLicenseNumber($stateLicenseNumber)
    {
        $this->stateLicenseNumber = $stateLicenseNumber;

        return $this;
    }

    /**
     * Get stateLicenseNumber
     *
     * @return string
     */
    public function getStateLicenseNumber()
    {
        return $this->stateLicenseNumber;
    }

    /**
     * Set stateLicenseExpirationDate
     *
     * @param \DateTime $stateLicenseExpirationDate
     *
     * @return Contractor
     */
    public function setStateLicenseExpirationDate($stateLicenseExpirationDate)
    {
        $this->stateLicenseExpirationDate = $stateLicenseExpirationDate;

        return $this;
    }

    /**
     * Get stateLicenseExpirationDate
     *
     * @return \DateTime
     */
    public function getStateLicenseExpirationDate()
    {
        return $this->stateLicenseExpirationDate;
    }

    /**
     * Set coiExpiration
     *
     * @param \DateTime $coiExpiration
     *
     * @return Contractor
     */
    public function setCoiExpiration($coiExpiration)
    {
        $this->coiExpiration = $coiExpiration;

        return $this;
    }

    /**
     * Get coiExpiration
     *
     * @return \DateTime
     */
    public function getCoiExpiration()
    {
        return $this->coiExpiration;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Contractor
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Contractor
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Contractor
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return Contractor
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\ContractorType $type
     *
     * @return Contractor
     */
    public function setType(\AppBundle\Entity\ContractorType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\ContractorType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Contractor
     */
    public function setPhone($phone)
    {
        $this->phone = $this->filterPhone($phone);

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set oldSistemId
     *
     * @param string $oldSistemId
     *
     * @return Contractor
     */
    public function setOldSistemId($oldSistemId)
    {
        $this->oldSistemId = $oldSistemId;

        return $this;
    }

    /**
     * Get oldSistemId
     *
     * @return string
     */
    public function getOldSistemId()
    {
        return $this->oldSistemId;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Contractor
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Contractor
     */
    public function setFax($fax)
    {
        $this->fax = $this->filterPhone($fax);

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Contractor
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDateCreateValue()
    {
        $this->dateCreate = new \DateTime();

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDateUpdateValue()
    {
        $this->dateUpdate = new \DateTime();

        return $this;
    }


    /**
     * Set isHistoryProcessed
     *
     * @param boolean $isHistoryProcessed
     *
     * @return Contractor
     */
    public function setIsHistoryProcessed($isHistoryProcessed)
    {
        $this->isHistoryProcessed = $isHistoryProcessed;

        return $this;
    }

    /**
     * Get isHistoryProcessed
     *
     * @return boolean
     */
    public function getIsHistoryProcessed()
    {
        return $this->isHistoryProcessed;
    }
}
