<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * ProposalHistory
 */
class ProposalHistory
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $referenceId;

    /**
     * @var integer
     */
    private $servicesCount;

    /**
     * @var integer
     */
    private $devicesCount;

    /**
     * @var integer
     */
    private $sitesCount;

    /**
     * @var \DateTime
     */
    private $dateOfProposal;

    /**
     * @var \DateTime
     */
    private $earliestDueDate;

    /**
     * @var float
     */
    private $grandTotal;

    /**
     * @var float
     */
    private $serviceTotalFee;

    /**
     * @var string
     */
    private $sendingAddressDescription;

    /**
     * @var string
     */
    private $report;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var float
     */
    private $afterHoursWorkCost;

    /**
     * @var float
     */
    private $discount;

    /**
     * @var string
     */
    private $additionalComment;

    /**
     * @var float
     */
    private $totalEstimationTime;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $services;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $logs;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $letters;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $division;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var \AppBundle\Entity\ProposalType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $creator;

    /**
     * @var \AppBundle\Entity\ProposalStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\AccountContactPerson
     */
    private $recipient;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $sendingAddress;

    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $ownerEntity;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $clone;

    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $cloned;

    /**
     * @var float
     */
    private $additionalCost;

    /**
     * @var boolean
     */
    private $isMunicipalityFessWillVary = false;

    /**
     * @var string
     */
    private $additionalCostComment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
        $this->logs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->letters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referenceId
     *
     * @param string $referenceId
     *
     * @return ProposalHistory
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * Get referenceId
     *
     * @return string
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * Set servicesCount
     *
     * @param integer $servicesCount
     *
     * @return ProposalHistory
     */
    public function setServicesCount($servicesCount)
    {
        $this->servicesCount = $servicesCount;

        return $this;
    }

    /**
     * Get servicesCount
     *
     * @return integer
     */
    public function getServicesCount()
    {
        return $this->servicesCount;
    }

    /**
     * Set devicesCount
     *
     * @param integer $devicesCount
     *
     * @return ProposalHistory
     */
    public function setDevicesCount($devicesCount)
    {
        $this->devicesCount = $devicesCount;

        return $this;
    }

    /**
     * Get devicesCount
     *
     * @return integer
     */
    public function getDevicesCount()
    {
        return $this->devicesCount;
    }

    /**
     * Set sitesCount
     *
     * @param integer $sitesCount
     *
     * @return ProposalHistory
     */
    public function setSitesCount($sitesCount)
    {
        $this->sitesCount = $sitesCount;

        return $this;
    }

    /**
     * Get sitesCount
     *
     * @return integer
     */
    public function getSitesCount()
    {
        return $this->sitesCount;
    }

    /**
     * Set dateOfProposal
     *
     * @param \DateTime $dateOfProposal
     *
     * @return ProposalHistory
     */
    public function setDateOfProposal($dateOfProposal)
    {
        $this->dateOfProposal = $dateOfProposal;

        return $this;
    }

    /**
     * Get dateOfProposal
     *
     * @return \DateTime
     */
    public function getDateOfProposal()
    {
        return $this->dateOfProposal;
    }

    /**
     * Set earliestDueDate
     *
     * @param \DateTime $earliestDueDate
     *
     * @return ProposalHistory
     */
    public function setEarliestDueDate($earliestDueDate)
    {
        $this->earliestDueDate = $earliestDueDate;

        return $this;
    }

    /**
     * Get earliestDueDate
     *
     * @return \DateTime
     */
    public function getEarliestDueDate()
    {
        return $this->earliestDueDate;
    }

    /**
     * Set grandTotal
     *
     * @param float $grandTotal
     *
     * @return ProposalHistory
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grandTotal = $grandTotal;

        return $this;
    }

    /**
     * Get grandTotal
     *
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->grandTotal;
    }

    /**
     * Set serviceTotalFee
     *
     * @param float $serviceTotalFee
     *
     * @return ProposalHistory
     */
    public function setServiceTotalFee($serviceTotalFee)
    {
        $this->serviceTotalFee = $serviceTotalFee;

        return $this;
    }

    /**
     * Get serviceTotalFee
     *
     * @return float
     */
    public function getServiceTotalFee()
    {
        return $this->serviceTotalFee;
    }

    /**
     * Set sendingAddressDescription
     *
     * @param string $sendingAddressDescription
     *
     * @return ProposalHistory
     */
    public function setSendingAddressDescription($sendingAddressDescription)
    {
        $this->sendingAddressDescription = $sendingAddressDescription;

        return $this;
    }

    /**
     * Get sendingAddressDescription
     *
     * @return string
     */
    public function getSendingAddressDescription()
    {
        return $this->sendingAddressDescription;
    }

    /**
     * Set report
     *
     * @param string $report
     *
     * @return ProposalHistory
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return string
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ProposalHistory
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ProposalHistory
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set afterHoursWorkCost
     *
     * @param float $afterHoursWorkCost
     *
     * @return ProposalHistory
     */
    public function setAfterHoursWorkCost($afterHoursWorkCost)
    {
        $this->afterHoursWorkCost = $afterHoursWorkCost;

        return $this;
    }

    /**
     * Get afterHoursWorkCost
     *
     * @return float
     */
    public function getAfterHoursWorkCost()
    {
        return $this->afterHoursWorkCost;
    }

    /**
     * Set discount
     *
     * @param float $discount
     *
     * @return ProposalHistory
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set additionalComment
     *
     * @param string $additionalComment
     *
     * @return ProposalHistory
     */
    public function setAdditionalComment($additionalComment)
    {
        $this->additionalComment = $additionalComment;

        return $this;
    }

    /**
     * Get additionalComment
     *
     * @return string
     */
    public function getAdditionalComment()
    {
        return $this->additionalComment;
    }

    /**
     * Set totalEstimationTime
     *
     * @param float $totalEstimationTime
     *
     * @return ProposalHistory
     */
    public function setTotalEstimationTime($totalEstimationTime)
    {
        $this->totalEstimationTime = $totalEstimationTime;

        return $this;
    }

    /**
     * Get totalEstimationTime
     *
     * @return float
     */
    public function getTotalEstimationTime()
    {
        return $this->totalEstimationTime;
    }

    /**
     * Add service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return ProposalHistory
     */
    public function addService(\AppBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \AppBundle\Entity\Service $service
     */
    public function removeService(\AppBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add log
     *
     * @param \AppBundle\Entity\ProposalLog $log
     *
     * @return ProposalHistory
     */
    public function addLog(\AppBundle\Entity\ProposalLog $log)
    {
        $this->logs[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \AppBundle\Entity\ProposalLog $log
     */
    public function removeLog(\AppBundle\Entity\ProposalLog $log)
    {
        $this->logs->removeElement($log);
    }

    /**
     * Get logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Add letter
     *
     * @param \AppBundle\Entity\Letter $letter
     *
     * @return ProposalHistory
     */
    public function addLetter(\AppBundle\Entity\Letter $letter)
    {
        $this->letters[] = $letter;

        return $this;
    }

    /**
     * Remove letter
     *
     * @param \AppBundle\Entity\Letter $letter
     */
    public function removeLetter(\AppBundle\Entity\Letter $letter)
    {
        $this->letters->removeElement($letter);
    }

    /**
     * Get letters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLetters()
    {
        return $this->letters;
    }

    /**
     * Set division
     *
     * @param \AppBundle\Entity\DeviceCategory $division
     *
     * @return ProposalHistory
     */
    public function setDivision(\AppBundle\Entity\DeviceCategory $division = null)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * Get division
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return ProposalHistory
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\ProposalType $type
     *
     * @return ProposalHistory
     */
    public function setType(\AppBundle\Entity\ProposalType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\ProposalType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set creator
     *
     * @param \AppBundle\Entity\ContractorUser $creator
     *
     * @return ProposalHistory
     */
    public function setCreator(\AppBundle\Entity\ContractorUser $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\ProposalStatus $status
     *
     * @return ProposalHistory
     */
    public function setStatus(\AppBundle\Entity\ProposalStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\ProposalStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set recipient
     *
     * @param \AppBundle\Entity\AccountContactPerson $recipient
     *
     * @return ProposalHistory
     */
    public function setRecipient(\AppBundle\Entity\AccountContactPerson $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \AppBundle\Entity\AccountContactPerson
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set sendingAddress
     *
     * @param \AppBundle\Entity\Address $sendingAddress
     *
     * @return ProposalHistory
     */
    public function setSendingAddress(\AppBundle\Entity\Address $sendingAddress = null)
    {
        $this->sendingAddress = $sendingAddress;

        return $this;
    }

    /**
     * Get sendingAddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getSendingAddress()
    {
        return $this->sendingAddress;
    }

    /**
     * Set ownerEntity
     *
     * @param \AppBundle\Entity\Proposal $ownerEntity
     *
     * @return ProposalHistory
     */
    public function setOwnerEntity(\AppBundle\Entity\Proposal $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return ProposalHistory
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set clone
     *
     * @param \AppBundle\Entity\Proposal $clone
     *
     * @return ProposalHistory
     */
    public function setClone(\AppBundle\Entity\Proposal $clone = null)
    {
        $this->clone = $clone;

        return $this;
    }

    /**
     * Get clone
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getClone()
    {
        return $this->clone;
    }

    /**
     * Set cloned
     *
     * @param \AppBundle\Entity\Proposal $cloned
     *
     * @return ProposalHistory
     */
    public function setCloned(\AppBundle\Entity\Proposal $cloned = null)
    {
        $this->cloned = $cloned;

        return $this;
    }

    /**
     * Get cloned
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getCloned()
    {
        return $this->cloned;
    }

    /**
     * Set additionalCost
     *
     * @param float $additionalCost
     *
     * @return ProposalHistory
     */
    public function setAdditionalCost($additionalCost)
    {
        $this->additionalCost = $additionalCost;

        return $this;
    }

    /**
     * Get additionalCost
     *
     * @return float
     */
    public function getAdditionalCost()
    {
        return $this->additionalCost;
    }

    /**
     * Set isMunicipalityFessWillVary
     *
     * @param boolean $isMunicipalityFessWillVary
     *
     * @return ProposalHistory
     */
    public function setIsMunicipalityFessWillVary($isMunicipalityFessWillVary)
    {
        $this->isMunicipalityFessWillVary = $isMunicipalityFessWillVary;

        return $this;
    }

    /**
     * Get isMunicipalityFessWillVary
     *
     * @return boolean
     */
    public function getIsMunicipalityFessWillVary()
    {
        return $this->isMunicipalityFessWillVary;
    }

    /**
     * Set additionalCostComment
     *
     * @param string $additionalCostComment
     *
     * @return ProposalHistory
     */
    public function setAdditionalCostComment($additionalCostComment)
    {
        $this->additionalCostComment = $additionalCostComment;

        return $this;
    }

    /**
     * Get additionalCostComment
     *
     * @return string
     */
    public function getAdditionalCostComment()
    {
        return $this->additionalCostComment;
    }
}
