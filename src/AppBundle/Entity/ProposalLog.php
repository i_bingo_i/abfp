<?php

namespace AppBundle\Entity;

/**
 * ProposalLog
 */
class ProposalLog
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $proposal;

    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $author;

    /**
     * @var \AppBundle\Entity\Letter
     */
    private $letter;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $actions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ProposalLog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ProposalLog
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return ProposalLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set proposal
     *
     * @param \AppBundle\Entity\Proposal $proposal
     *
     * @return ProposalLog
     */
    public function setProposal(\AppBundle\Entity\Proposal $proposal = null)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\ContractorUser $author
     *
     * @return ProposalLog
     */
    public function setAuthor(\AppBundle\Entity\ContractorUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set letter
     *
     * @param \AppBundle\Entity\Letter $letter
     *
     * @return ProposalLog
     */
    public function setLetter(\AppBundle\Entity\Letter $letter = null)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Get letter
     *
     * @return \AppBundle\Entity\Letter
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Add action
     *
     * @param \AppBundle\Entity\ProposalLogAction $action
     *
     * @return ProposalLog
     */
    public function addAction(\AppBundle\Entity\ProposalLogAction $action)
    {
        $this->actions[] = $action;

        return $this;
    }

    /**
     * Remove action
     *
     * @param \AppBundle\Entity\ProposalLogAction $action
     */
    public function removeAction(\AppBundle\Entity\ProposalLogAction $action)
    {
        $this->actions->removeElement($action);
    }

    /**
     * Get actions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActions()
    {
        return $this->actions;
    }
}
