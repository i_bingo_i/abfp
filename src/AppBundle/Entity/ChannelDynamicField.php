<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * ChannelDynamicField
 */
class ChannelDynamicField
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $selectOptions;

    /**
     * @var \AppBundle\Entity\ChannelNamed
     */
    private $channel;

    /**
     * @var \AppBundle\Entity\DynamicFieldType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\DynamicFieldValidation
     */
    private $validation;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->selectOptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ChannelDynamicField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return ChannelDynamicField
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ChannelDynamicField
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ChannelDynamicField
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Add selectOption
     *
     * @param \AppBundle\Entity\ChannelDropboxChoices $selectOption
     *
     * @return ChannelDynamicField
     */
    public function addSelectOption(\AppBundle\Entity\ChannelDropboxChoices $selectOption)
    {
        $this->selectOptions[] = $selectOption;

        return $this;
    }

    /**
     * Remove selectOption
     *
     * @param \AppBundle\Entity\ChannelDropboxChoices $selectOption
     */
    public function removeSelectOption(\AppBundle\Entity\ChannelDropboxChoices $selectOption)
    {
        $this->selectOptions->removeElement($selectOption);
    }

    /**
     * Get selectOptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSelectOptions()
    {
        return $this->selectOptions;
    }

    /**
     * Set channel
     *
     * @param \AppBundle\Entity\ChannelNamed $channel
     *
     * @return ChannelDynamicField
     */
    public function setChannel(\AppBundle\Entity\ChannelNamed $channel = null)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return \AppBundle\Entity\ChannelNamed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\DynamicFieldType $type
     *
     * @return ChannelDynamicField
     */
    public function setType(\AppBundle\Entity\DynamicFieldType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\DynamicFieldType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set validation
     *
     * @param \AppBundle\Entity\DynamicFieldValidation $validation
     *
     * @return ChannelDynamicField
     */
    public function setValidation(\AppBundle\Entity\DynamicFieldValidation $validation = null)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * Get validation
     *
     * @return \AppBundle\Entity\DynamicFieldValidation
     */
    public function getValidation()
    {
        return $this->validation;
    }
}
