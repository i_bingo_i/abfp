<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * DepartmentChannelHistory
 */
class DepartmentChannelHistory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $uploadFee = '0';

    /**
     * @var string
     */
    private $processingFee = '0';

    /**
     * @var boolean
     */
    private $isDefault = false;

    /**
     * @var boolean
     */
    private $active = true;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fields;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $services;

    /**
     * @var \AppBundle\Entity\ChannelNamed
     */
    private $named;

    /**
     * @var \AppBundle\Entity\Agent
     */
    private $ownerAgent;

    /**
     * @var \AppBundle\Entity\FeesBasis
     */
    private $feesBasis;

    /**
     * @var \AppBundle\Entity\Department
     */
    private $department;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $devision;

    /**
     * @var \AppBundle\Entity\AgentChannel
     */
    private $owner;

    /**
     * @var \AppBundle\Entity\User
     */
    private $author;

    /**
     * @var \AppBundle\Entity\DepartmentChannel
     */
    private $ownerEntity;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fields = new ArrayCollection();
        $this->services = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uploadFee
     *
     * @param string $uploadFee
     *
     * @return DepartmentChannelHistory
     */
    public function setUploadFee($uploadFee)
    {
        $this->uploadFee = $uploadFee;

        return $this;
    }

    /**
     * Get uploadFee
     *
     * @return string
     */
    public function getUploadFee()
    {
        return $this->uploadFee;
    }

    /**
     * Set processingFee
     *
     * @param string $processingFee
     *
     * @return DepartmentChannelHistory
     */
    public function setProcessingFee($processingFee)
    {
        $this->processingFee = $processingFee;

        return $this;
    }

    /**
     * Get processingFee
     *
     * @return string
     */
    public function getProcessingFee()
    {
        return $this->processingFee;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     *
     * @return DepartmentChannelHistory
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return DepartmentChannelHistory
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return DepartmentChannelHistory
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return DepartmentChannelHistory
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return DepartmentChannelHistory
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Add field
     *
     * @param \AppBundle\Entity\DepartmentChannelDynamicFieldValue $field
     *
     * @return DepartmentChannelHistory
     */
    public function addField(\AppBundle\Entity\DepartmentChannelDynamicFieldValue $field)
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * Remove field
     *
     * @param \AppBundle\Entity\DepartmentChannelDynamicFieldValue $field
     */
    public function removeField(\AppBundle\Entity\DepartmentChannelDynamicFieldValue $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return DepartmentChannelHistory
     */
    public function addService(\AppBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \AppBundle\Entity\Service $service
     */
    public function removeService(\AppBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\ChannelNamed $named
     *
     * @return DepartmentChannelHistory
     */
    public function setNamed(\AppBundle\Entity\ChannelNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\ChannelNamed
     */
    public function getNamed()
    {
        return $this->named;
    }

    /**
     * Set ownerAgent
     *
     * @param \AppBundle\Entity\Agent $ownerAgent
     *
     * @return DepartmentChannelHistory
     */
    public function setOwnerAgent(\AppBundle\Entity\Agent $ownerAgent = null)
    {
        $this->ownerAgent = $ownerAgent;

        return $this;
    }

    /**
     * Get ownerAgent
     *
     * @return \AppBundle\Entity\Agent
     */
    public function getOwnerAgent()
    {
        return $this->ownerAgent;
    }

    /**
     * Set feesBasis
     *
     * @param \AppBundle\Entity\FeesBasis $feesBasis
     *
     * @return DepartmentChannelHistory
     */
    public function setFeesBasis(\AppBundle\Entity\FeesBasis $feesBasis = null)
    {
        $this->feesBasis = $feesBasis;

        return $this;
    }

    /**
     * Get feesBasis
     *
     * @return \AppBundle\Entity\FeesBasis
     */
    public function getFeesBasis()
    {
        return $this->feesBasis;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return DepartmentChannelHistory
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set devision
     *
     * @param \AppBundle\Entity\DeviceCategory $devision
     *
     * @return DepartmentChannelHistory
     */
    public function setDevision(\AppBundle\Entity\DeviceCategory $devision = null)
    {
        $this->devision = $devision;

        return $this;
    }

    /**
     * Get devision
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getDevision()
    {
        return $this->devision;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\AgentChannel $owner
     *
     * @return DepartmentChannelHistory
     */
    public function setOwner(\AppBundle\Entity\AgentChannel $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\AgentChannel
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return DepartmentChannelHistory
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set ownerEntity
     *
     * @param \AppBundle\Entity\DepartmentChannel $ownerEntity
     *
     * @return DepartmentChannelHistory
     */
    public function setOwnerEntity(\AppBundle\Entity\DepartmentChannel $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \AppBundle\Entity\DepartmentChannel
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }
}
