<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * WorkorderStatus
 *  (don't auto generate this entity)
 * - hasAvailableStatus code has been added
 */
class WorkorderStatus
{
    public const STATUS_SEND_INVOICE = 'send_invoice';
    public const SUBMITTED_INVOICE_STATUS = 'send_invoice';
    public const SEND_REPORTS_TO_CLIENT_STATUS = 'send_reports_to_client';
    public const SEND_REPORTS_TO_AHJ_STATUS = 'send_reports_to_ahj';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var integer
     */
    private $priority;

    /**
     * @var string
     */
    private $cssClass;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $availableStatuses;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $availableActions;


    /**
     * WorkorderStatus constructor.
     */
    public function __construct()
    {
        $this->availableStatuses = new ArrayCollection();
        $this->availableActions = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WorkorderStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return WorkorderStatus
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return WorkorderStatus
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set cssClass
     *
     * @param string $cssClass
     *
     * @return WorkorderStatus
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;

        return $this;
    }

    /**
     * Get cssClass
     *
     * @return string
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * Add availableStatus
     *
     * @param \AppBundle\Entity\WorkorderStatus $availableStatus
     *
     * @return WorkorderStatus
     */
    public function addAvailableStatus(\AppBundle\Entity\WorkorderStatus $availableStatus)
    {
        if (!$this->hasAvailableStatus($availableStatus)) {
            $this->availableStatuses[] = $availableStatus;
        }

        return $this;
    }

    /**
     * Remove availableStatus
     *
     * @param \AppBundle\Entity\WorkorderStatus $availableStatus
     */
    public function removeAvailableStatus(\AppBundle\Entity\WorkorderStatus $availableStatus)
    {
        if ($this->hasAvailableStatus($availableStatus)) {
            $this->availableStatuses->removeElement($availableStatus);
        }
    }

    /**
     * Get availableStatuses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvailableStatuses()
    {
        return $this->availableStatuses;
    }

    /**
     * @param WorkorderStatus $availableStatus
     * @return bool
     */
    public function hasAvailableStatus(WorkorderStatus $availableStatus)
    {
        return $this->getAvailableStatuses()->contains($availableStatus);
    }

    /**
     * Add availableAction
     *
     * @param \AppBundle\Entity\WorkorderStatus $availableAction
     *
     * @return WorkorderStatus
     */
    public function addAvailableAction(\AppBundle\Entity\WorkorderStatus $availableAction)
    {
        if (!$this->hasAvailableAction($availableAction)) {
            $this->availableActions[] = $availableAction;
        }

        return $this;
    }

    /**
     * Remove availableAction
     *
     * @param \AppBundle\Entity\WorkorderStatus $availableAction
     */
    public function removeAvailableAction(\AppBundle\Entity\WorkorderStatus $availableAction)
    {
        if ($this->hasAvailableAction($availableAction)) {
            $this->availableActions->removeElement($availableAction);
        }
    }

    /**
     * Get availableActions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvailableActions()
    {
        return $this->availableActions;
    }

    /**
     * @param WorkorderStatus $availableAction
     * @return bool
     */
    public function hasAvailableAction(WorkorderStatus $availableAction)
    {
        return $this->getAvailableActions()->contains($availableAction);
    }
}
