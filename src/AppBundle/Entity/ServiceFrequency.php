<?php

namespace AppBundle\Entity;

/**
 * ServiceFrequency
 */
class ServiceFrequency
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $mounthFrequency;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ServiceFrequency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return ServiceFrequency
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set mounthFrequency
     *
     * @param string $mounthFrequency
     *
     * @return ServiceFrequency
     */
    public function setMounthFrequency($mounthFrequency)
    {
        $this->mounthFrequency = $mounthFrequency;

        return $this;
    }

    /**
     * Get mounthFrequency
     *
     * @return string
     */
    public function getMounthFrequency()
    {
        return $this->mounthFrequency;
    }
}
