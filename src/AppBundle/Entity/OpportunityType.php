<?php

namespace AppBundle\Entity;

/**
 * OpportunityType
 */
class OpportunityType
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alisa;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OpportunityType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alisa
     *
     * @param string $alisa
     *
     * @return OpportunityType
     */
    public function setAlisa($alisa)
    {
        $this->alisa = $alisa;

        return $this;
    }

    /**
     * Get alisa
     *
     * @return string
     */
    public function getAlisa()
    {
        return $this->alisa;
    }
}
