<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use AppBundle\Entity\EntityTrait\PriceFilterTrait;

/**
 * Proposal
 *
 * (don't auto generate this entity)
 * - was added code filtering for afterHoursWorkCost and discount field
 */
class Proposal
{
    use DateTimeControlTrait;
    use PriceFilterTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $referenceId;

    /**
     * @var integer
     */
    private $servicesCount;

    /**
     * @var integer
     */
    private $devicesCount;

    /**
     * @var integer
     */
    private $sitesCount;

    /**
     * @var \DateTime
     */
    private $dateOfProposal;

    /**
     * @var \DateTime
     */
    private $earliestDueDate;

    /**
     * @var float
     */
    private $grandTotal;

    /**
     * @var float
     */
    private $serviceTotalFee;

    /**
     * @var string
     */
    private $sendingAddressDescription;

    /**
     * @var string
     */
    private $report;

    /**
     * @var \DateTime
     */
    private $firstSendingDate;

    /**
     * @var boolean
     */
    private $isFrozen = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var float
     */
    private $afterHoursWorkCost;

    /**
     * @var float
     */
    private $discount;

    /**
     * @var string
     */
    private $additionalComment;

    /**
     * @var float
     */
    private $allTotalMunicAndProcFee;

    /**
     * @var float
     */
    private $allTotalRepairsPrice;

    /**
     * @var float
     */
    private $allGrandTotalPrice;

    /**
     * @var float
     */
    private $totalEstimationTime;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $services;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $logs;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $letters;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $repairDeviceInfo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $workorderLogs;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $division;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var \AppBundle\Entity\ProposalType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $creator;

    /**
     * @var \AppBundle\Entity\ProposalStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\AccountContactPerson
     */
    private $recipient;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $sendingAddress;

    /**
     * @var \AppBundle\Entity\FrozenProposal
     */
    private $frozen;

    /**
     * @var \AppBundle\Entity\ProposalLifeCycle
     */
    private $lifeCycle;

    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $clone;

    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $cloned;

    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $notIncludedItems;

    /**
     * @var float
     */
    private $additionalCost;

    /**
     * @var boolean
     */
    private $isMunicipalityFessWillVary = false;

    /**
     * @var string
     */
    private $additionalCostComment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
        $this->logs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->letters = new \Doctrine\Common\Collections\ArrayCollection();
        $this->repairDeviceInfo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workorderLogs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notIncludedItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referenceId
     *
     * @param string $referenceId
     *
     * @return Proposal
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * Get referenceId
     *
     * @return string
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * Set servicesCount
     *
     * @param integer $servicesCount
     *
     * @return Proposal
     */
    public function setServicesCount($servicesCount)
    {
        $this->servicesCount = $servicesCount;

        return $this;
    }

    /**
     * Get servicesCount
     *
     * @return integer
     */
    public function getServicesCount()
    {
        return $this->servicesCount;
    }

    /**
     * Set devicesCount
     *
     * @param integer $devicesCount
     *
     * @return Proposal
     */
    public function setDevicesCount($devicesCount)
    {
        $this->devicesCount = $devicesCount;

        return $this;
    }

    /**
     * Get devicesCount
     *
     * @return integer
     */
    public function getDevicesCount()
    {
        return $this->devicesCount;
    }

    /**
     * Set sitesCount
     *
     * @param integer $sitesCount
     *
     * @return Proposal
     */
    public function setSitesCount($sitesCount)
    {
        $this->sitesCount = $sitesCount;

        return $this;
    }

    /**
     * Get sitesCount
     *
     * @return integer
     */
    public function getSitesCount()
    {
        return $this->sitesCount;
    }

    /**
     * Set dateOfProposal
     *
     * @param \DateTime $dateOfProposal
     *
     * @return Proposal
     */
    public function setDateOfProposal($dateOfProposal)
    {
        $this->dateOfProposal = $dateOfProposal;

        return $this;
    }

    /**
     * Get dateOfProposal
     *
     * @return \DateTime
     */
    public function getDateOfProposal()
    {
        return $this->dateOfProposal;
    }

    /**
     * Set earliestDueDate
     *
     * @param \DateTime $earliestDueDate
     *
     * @return Proposal
     */
    public function setEarliestDueDate($earliestDueDate)
    {
        $this->earliestDueDate = $earliestDueDate;

        return $this;
    }

    /**
     * Get earliestDueDate
     *
     * @return \DateTime
     */
    public function getEarliestDueDate()
    {
        return $this->earliestDueDate;
    }

    /**
     * Set grandTotal
     *
     * @param float $grandTotal
     *
     * @return Proposal
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grandTotal = $grandTotal;

        return $this;
    }

    /**
     * Get grandTotal
     *
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->grandTotal;
    }

    /**
     * Set serviceTotalFee
     *
     * @param float $serviceTotalFee
     *
     * @return Proposal
     */
    public function setServiceTotalFee($serviceTotalFee)
    {
        $this->serviceTotalFee = $serviceTotalFee;

        return $this;
    }

    /**
     * Get serviceTotalFee
     *
     * @return float
     */
    public function getServiceTotalFee()
    {
        return $this->serviceTotalFee;
    }

    /**
     * Set sendingAddressDescription
     *
     * @param string $sendingAddressDescription
     *
     * @return Proposal
     */
    public function setSendingAddressDescription($sendingAddressDescription)
    {
        $this->sendingAddressDescription = $sendingAddressDescription;

        return $this;
    }

    /**
     * Get sendingAddressDescription
     *
     * @return string
     */
    public function getSendingAddressDescription()
    {
        return $this->sendingAddressDescription;
    }

    /**
     * Set report
     *
     * @param string $report
     *
     * @return Proposal
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return string
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set firstSendingDate
     *
     * @param \DateTime $firstSendingDate
     *
     * @return Proposal
     */
    public function setFirstSendingDate($firstSendingDate)
    {
        $this->firstSendingDate = $firstSendingDate;

        return $this;
    }

    /**
     * Get firstSendingDate
     *
     * @return \DateTime
     */
    public function getFirstSendingDate()
    {
        return $this->firstSendingDate;
    }

    /**
     * Set isFrozen
     *
     * @param boolean $isFrozen
     *
     * @return Proposal
     */
    public function setIsFrozen($isFrozen)
    {
        $this->isFrozen = $isFrozen;

        return $this;
    }

    /**
     * Get isFrozen
     *
     * @return boolean
     */
    public function getIsFrozen()
    {
        return $this->isFrozen;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Proposal
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Proposal
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set afterHoursWorkCost
     *
     * @param float $afterHoursWorkCost
     *
     * @return Proposal
     */
    public function setAfterHoursWorkCost($afterHoursWorkCost)
    {
        $this->afterHoursWorkCost = $this->filterPrice($afterHoursWorkCost);

        return $this;
    }

    /**
     * Get afterHoursWorkCost
     *
     * @return float
     */
    public function getAfterHoursWorkCost()
    {
        return $this->afterHoursWorkCost;
    }

    /**
     * Set discount
     *
     * @param float $discount
     *
     * @return Proposal
     */
    public function setDiscount($discount)
    {
        $this->discount = $this->filterPrice($discount);

        return $this;
    }

    /**
     * Get discount
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set additionalComment
     *
     * @param string $additionalComment
     *
     * @return Proposal
     */
    public function setAdditionalComment($additionalComment)
    {
        $this->additionalComment = $additionalComment;

        return $this;
    }

    /**
     * Get additionalComment
     *
     * @return string
     */
    public function getAdditionalComment()
    {
        return $this->additionalComment;
    }

    /**
     * Set allTotalMunicAndProcFee
     *
     * @param float $allTotalMunicAndProcFee
     *
     * @return Proposal
     */
    public function setAllTotalMunicAndProcFee($allTotalMunicAndProcFee)
    {
        $this->allTotalMunicAndProcFee = $allTotalMunicAndProcFee;

        return $this;
    }

    /**
     * Get allTotalMunicAndProcFee
     *
     * @return float
     */
    public function getAllTotalMunicAndProcFee()
    {
        return $this->allTotalMunicAndProcFee;
    }

    /**
     * Set allTotalRepairsPrice
     *
     * @param float $allTotalRepairsPrice
     *
     * @return Proposal
     */
    public function setAllTotalRepairsPrice($allTotalRepairsPrice)
    {
        $this->allTotalRepairsPrice = $allTotalRepairsPrice;

        return $this;
    }

    /**
     * Get allTotalRepairsPrice
     *
     * @return float
     */
    public function getAllTotalRepairsPrice()
    {
        return $this->allTotalRepairsPrice;
    }

    /**
     * Set allGrandTotalPrice
     *
     * @param float $allGrandTotalPrice
     *
     * @return Proposal
     */
    public function setAllGrandTotalPrice($allGrandTotalPrice)
    {
        $this->allGrandTotalPrice = $allGrandTotalPrice;

        return $this;
    }

    /**
     * Get allGrandTotalPrice
     *
     * @return float
     */
    public function getAllGrandTotalPrice()
    {
        return $this->allGrandTotalPrice;
    }

    /**
     * Set totalEstimationTime
     *
     * @param float $totalEstimationTime
     *
     * @return Proposal
     */
    public function setTotalEstimationTime($totalEstimationTime)
    {
        $this->totalEstimationTime = $totalEstimationTime;

        return $this;
    }

    /**
     * Get totalEstimationTime
     *
     * @return float
     */
    public function getTotalEstimationTime()
    {
        return $this->totalEstimationTime;
    }

    /**
     * Add service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return Proposal
     */
    public function addService(\AppBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \AppBundle\Entity\Service $service
     */
    public function removeService(\AppBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add log
     *
     * @param \AppBundle\Entity\ProposalLog $log
     *
     * @return Proposal
     */
    public function addLog(\AppBundle\Entity\ProposalLog $log)
    {
        $this->logs[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \AppBundle\Entity\ProposalLog $log
     */
    public function removeLog(\AppBundle\Entity\ProposalLog $log)
    {
        $this->logs->removeElement($log);
    }

    /**
     * Get logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Add letter
     *
     * @param \AppBundle\Entity\Letter $letter
     *
     * @return Proposal
     */
    public function addLetter(\AppBundle\Entity\Letter $letter)
    {
        $this->letters[] = $letter;

        return $this;
    }

    /**
     * Remove letter
     *
     * @param \AppBundle\Entity\Letter $letter
     */
    public function removeLetter(\AppBundle\Entity\Letter $letter)
    {
        $this->letters->removeElement($letter);
    }

    /**
     * Get letters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLetters()
    {
        return $this->letters;
    }

    /**
     * Add repairDeviceInfo
     *
     * @param \AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo
     *
     * @return Proposal
     */
    public function addRepairDeviceInfo(\AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo)
    {
        $this->repairDeviceInfo[] = $repairDeviceInfo;

        return $this;
    }

    /**
     * Remove repairDeviceInfo
     *
     * @param \AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo
     */
    public function removeRepairDeviceInfo(\AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo)
    {
        $this->repairDeviceInfo->removeElement($repairDeviceInfo);
    }

    /**
     * Get repairDeviceInfo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepairDeviceInfo()
    {
        return $this->repairDeviceInfo;
    }

    /**
     * Add workorderLog
     *
     * @param \AppBundle\Entity\WorkorderLog $workorderLog
     *
     * @return Proposal
     */
    public function addWorkorderLog(\AppBundle\Entity\WorkorderLog $workorderLog)
    {
        $this->workorderLogs[] = $workorderLog;

        return $this;
    }

    /**
     * Remove workorderLog
     *
     * @param \AppBundle\Entity\WorkorderLog $workorderLog
     */
    public function removeWorkorderLog(\AppBundle\Entity\WorkorderLog $workorderLog)
    {
        $this->workorderLogs->removeElement($workorderLog);
    }

    /**
     * Get workorderLogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkorderLogs()
    {
        return $this->workorderLogs;
    }

    /**
     * Set division
     *
     * @param \AppBundle\Entity\DeviceCategory $division
     *
     * @return Proposal
     */
    public function setDivision(\AppBundle\Entity\DeviceCategory $division = null)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * Get division
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return Proposal
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\ProposalType $type
     *
     * @return Proposal
     */
    public function setType(\AppBundle\Entity\ProposalType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\ProposalType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set creator
     *
     * @param \AppBundle\Entity\ContractorUser $creator
     *
     * @return Proposal
     */
    public function setCreator(\AppBundle\Entity\ContractorUser $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\ProposalStatus $status
     *
     * @return Proposal
     */
    public function setStatus(\AppBundle\Entity\ProposalStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\ProposalStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set recipient
     *
     * @param \AppBundle\Entity\AccountContactPerson $recipient
     *
     * @return Proposal
     */
    public function setRecipient(\AppBundle\Entity\AccountContactPerson $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \AppBundle\Entity\AccountContactPerson
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set sendingAddress
     *
     * @param \AppBundle\Entity\Address $sendingAddress
     *
     * @return Proposal
     */
    public function setSendingAddress(\AppBundle\Entity\Address $sendingAddress = null)
    {
        $this->sendingAddress = $sendingAddress;

        return $this;
    }

    /**
     * Get sendingAddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getSendingAddress()
    {
        return $this->sendingAddress;
    }

    /**
     * Set frozen
     *
     * @param \AppBundle\Entity\FrozenProposal $frozen
     *
     * @return Proposal
     */
    public function setFrozen(\AppBundle\Entity\FrozenProposal $frozen = null)
    {
        $this->frozen = $frozen;

        return $this;
    }

    /**
     * Get frozen
     *
     * @return \AppBundle\Entity\FrozenProposal
     */
    public function getFrozen()
    {
        return $this->frozen;
    }

    /**
     * Set lifeCycle
     *
     * @param \AppBundle\Entity\ProposalLifeCycle $lifeCycle
     *
     * @return Proposal
     */
    public function setLifeCycle(\AppBundle\Entity\ProposalLifeCycle $lifeCycle = null)
    {
        $this->lifeCycle = $lifeCycle;

        return $this;
    }

    /**
     * Get lifeCycle
     *
     * @return \AppBundle\Entity\ProposalLifeCycle
     */
    public function getLifeCycle()
    {
        return $this->lifeCycle;
    }

    /**
     * Set clone
     *
     * @param \AppBundle\Entity\Proposal $clone
     *
     * @return Proposal
     */
    public function setClone(\AppBundle\Entity\Proposal $clone = null)
    {
        $this->clone = $clone;

        return $this;
    }

    /**
     * Get clone
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getClone()
    {
        return $this->clone;
    }

    /**
     * Set cloned
     *
     * @param \AppBundle\Entity\Proposal $cloned
     *
     * @return Proposal
     */
    public function setCloned(\AppBundle\Entity\Proposal $cloned = null)
    {
        $this->cloned = $cloned;

        return $this;
    }

    /**
     * Get cloned
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getCloned()
    {
        return $this->cloned;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return Proposal
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * Add notIncludedItem
     *
     * @param \AppBundle\Entity\NotIncluded $notIncludedItem
     *
     * @return Proposal
     */
    public function addNotIncludedItem(\AppBundle\Entity\NotIncluded $notIncludedItem)
    {
        $this->notIncludedItems[] = $notIncludedItem;

        return $this;
    }

    /**
     * Remove notIncludedItem
     *
     * @param \AppBundle\Entity\NotIncluded $notIncludedItem
     */
    public function removeNotIncludedItem(\AppBundle\Entity\NotIncluded $notIncludedItem)
    {
        $this->notIncludedItems->removeElement($notIncludedItem);
    }

    /**
     * Get notIncludedItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotIncludedItems()
    {
        return $this->notIncludedItems;
    }

    /**
     * Set additionalCost
     *
     * @param float $additionalCost
     *
     * @return Proposal
     */
    public function setAdditionalCost($additionalCost)
    {
        $this->additionalCost = $this->filterPrice($additionalCost);

        return $this;
    }

    /**
     * Get additionalCost
     *
     * @return float
     */
    public function getAdditionalCost()
    {
        return $this->additionalCost;
    }
    /**
     * @var string
     */
    private $internalComments;


    /**
     * Set internalComments
     *
     * @param string $internalComments
     *
     * @return Proposal
     */
    public function setInternalComments($internalComments)
    {
        $this->internalComments = $internalComments;

        return $this;
    }

    /**
     * Get internalComments
     *
     * @return string
     */
    public function getInternalComments()
    {
        return $this->internalComments;
    }

    /**
     * Set isMunicipalityFessWillVary
     *
     * @param boolean $isMunicipalityFessWillVary
     *
     * @return Proposal
     */
    public function setIsMunicipalityFessWillVary($isMunicipalityFessWillVary)
    {
        $this->isMunicipalityFessWillVary = $isMunicipalityFessWillVary;

        return $this;
    }

    /**
     * Get isMunicipalityFessWillVary
     *
     * @return boolean
     */
    public function getIsMunicipalityFessWillVary()
    {
        return $this->isMunicipalityFessWillVary;
    }

    /**
     * Set additionalCostComment
     *
     * @param string $additionalCostComment
     *
     * @return Proposal
     */
    public function setAdditionalCostComment($additionalCostComment)
    {
        $this->additionalCostComment = $additionalCostComment;

        return $this;
    }

    /**
     * Get additionalCostComment
     *
     * @return string
     */
    public function getAdditionalCostComment()
    {
        return $this->additionalCostComment;
    }
}
