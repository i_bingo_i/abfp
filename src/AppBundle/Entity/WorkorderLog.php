<?php

namespace AppBundle\Entity;

use InvoiceBundle\Entity\Invoices;

/**
 * WorkorderLog
 */
class WorkorderLog
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;

    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $author;

    /**
     * @var \AppBundle\Entity\WorkorderLogType
     */
    private $type;

    /**
     * @var \AppBundle\Entity\Letter
     */
    private $letter;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return WorkorderLog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return WorkorderLog
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return WorkorderLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return WorkorderLog
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\ContractorUser $author
     *
     * @return WorkorderLog
     */
    public function setAuthor(\AppBundle\Entity\ContractorUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\WorkorderLogType $type
     *
     * @return WorkorderLog
     */
    public function setType(\AppBundle\Entity\WorkorderLogType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\WorkorderLogType
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * @var Invoices
     */
    private $invoice;


    /**
     * Set invoice
     *
     * @param Invoices $invoice
     *
     * @return WorkorderLog
     */
    public function setInvoice(Invoices $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return Invoices
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $proposal;


    /**
     * Set proposal
     *
     * @param \AppBundle\Entity\Proposal $proposal
     *
     * @return WorkorderLog
     */
    public function setProposal(\AppBundle\Entity\Proposal $proposal = null)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }
    /**
     * @var boolean
     */
    private $changingStatus = false;


    /**
     * Set changingStatus
     *
     * @param boolean $changingStatus
     *
     * @return WorkorderLog
     */
    public function setChangingStatus($changingStatus)
    {
        $this->changingStatus = $changingStatus;

        return $this;
    }

    /**
     * Get changingStatus
     *
     * @return boolean
     */
    public function getChangingStatus()
    {
        return $this->changingStatus;
    }

    /**
     * Set letter
     *
     * @param \AppBundle\Entity\Letter $letter
     *
     * @return WorkorderLog
     */
    public function setLetter(\AppBundle\Entity\Letter $letter = null)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Get letter
     *
     * @return \AppBundle\Entity\Letter
     */
    public function getLetter()
    {
        return $this->letter;
    }
}
