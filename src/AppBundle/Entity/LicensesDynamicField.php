<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * LicensesDynamicField
 */
class LicensesDynamicField
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var boolean
     */
    private $isShow = true;

    /**
     * @var boolean
     */
    private $useLabel = false;

    /**
     * @var boolean
     */
    private $labelAfter = false;

    /**
     * @var string
     */
    private $labelDescription;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $selectOptions;

    /**
     * @var \AppBundle\Entity\LicensesNamed
     */
    private $license;

    /**
     * @var \AppBundle\Entity\DynamicFieldType
     */
    private $type;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->selectOptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LicensesDynamicField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return LicensesDynamicField
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set isShow
     *
     * @param boolean $isShow
     *
     * @return LicensesDynamicField
     */
    public function setIsShow($isShow)
    {
        $this->isShow = $isShow;

        return $this;
    }

    /**
     * Get isShow
     *
     * @return boolean
     */
    public function getIsShow()
    {
        return $this->isShow;
    }

    /**
     * Set useLabel
     *
     * @param boolean $useLabel
     *
     * @return LicensesDynamicField
     */
    public function setUseLabel($useLabel)
    {
        $this->useLabel = $useLabel;

        return $this;
    }

    /**
     * Get useLabel
     *
     * @return boolean
     */
    public function getUseLabel()
    {
        return $this->useLabel;
    }

    /**
     * Set labelAfter
     *
     * @param boolean $labelAfter
     *
     * @return LicensesDynamicField
     */
    public function setLabelAfter($labelAfter)
    {
        $this->labelAfter = $labelAfter;

        return $this;
    }

    /**
     * Get labelAfter
     *
     * @return boolean
     */
    public function getLabelAfter()
    {
        return $this->labelAfter;
    }

    /**
     * Set labelDescription
     *
     * @param string $labelDescription
     *
     * @return LicensesDynamicField
     */
    public function setLabelDescription($labelDescription)
    {
        $this->labelDescription = $labelDescription;

        return $this;
    }

    /**
     * Get labelDescription
     *
     * @return string
     */
    public function getLabelDescription()
    {
        return $this->labelDescription;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return LicensesDynamicField
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return LicensesDynamicField
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Add selectOption
     *
     * @param \AppBundle\Entity\LicensesDropboxChoices $selectOption
     *
     * @return LicensesDynamicField
     */
    public function addSelectOption(\AppBundle\Entity\LicensesDropboxChoices $selectOption)
    {
        $this->selectOptions[] = $selectOption;

        return $this;
    }

    /**
     * Remove selectOption
     *
     * @param \AppBundle\Entity\LicensesDropboxChoices $selectOption
     */
    public function removeSelectOption(\AppBundle\Entity\LicensesDropboxChoices $selectOption)
    {
        $this->selectOptions->removeElement($selectOption);
    }

    /**
     * Get selectOptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSelectOptions()
    {
        return $this->selectOptions;
    }

    /**
     * Set license
     *
     * @param \AppBundle\Entity\LicensesNamed $license
     *
     * @return LicensesDynamicField
     */
    public function setLicense(\AppBundle\Entity\LicensesNamed $license = null)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license
     *
     * @return \AppBundle\Entity\LicensesNamed
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\DynamicFieldType $type
     *
     * @return LicensesDynamicField
     */
    public function setType(\AppBundle\Entity\DynamicFieldType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\DynamicFieldType
     */
    public function getType()
    {
        return $this->type;
    }
}
