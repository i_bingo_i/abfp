<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\PriceFilterTrait;

/**
 * RepairDeviceInfo
 */
class RepairDeviceInfo
{
    use PriceFilterTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var float
     */
    private $estimationTime;

    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $inspectionPrice;

    /**
     * @var \AppBundle\Entity\SpecialNotification
     */
    private $specialNotification;

    /**
     * @var \AppBundle\Entity\Proposal
     */
    private $proposal;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $device;

    /**
     * @var float
     */
    private $inspectionEstimationTime;

    /**
     * @var boolean
     */
    private $isGlued = false;

    /**
     * @var boolean
     */
    private $departmentChannelWithFeesWillVary = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return RepairDeviceInfo
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set estimationTime
     *
     * @param float $estimationTime
     *
     * @return RepairDeviceInfo
     */
    public function setEstimationTime($estimationTime)
    {
        $this->estimationTime = $this->filterPrice($estimationTime);

        return $this;
    }

    /**
     * Get estimationTime
     *
     * @return float
     */
    public function getEstimationTime()
    {
        return $this->estimationTime;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return RepairDeviceInfo
     */
    public function setPrice($price)
    {
        $this->price = $this->filterPrice($price);

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set specialNotification
     *
     * @param \AppBundle\Entity\SpecialNotification $specialNotification
     *
     * @return RepairDeviceInfo
     */
    public function setSpecialNotification(\AppBundle\Entity\SpecialNotification $specialNotification = null)
    {
        $this->specialNotification = $specialNotification;

        return $this;
    }

    /**
     * Get specialNotification
     *
     * @return \AppBundle\Entity\SpecialNotification
     */
    public function getSpecialNotification()
    {
        return $this->specialNotification;
    }

    /**
     * Set proposal
     *
     * @param \AppBundle\Entity\Proposal $proposal
     *
     * @return RepairDeviceInfo
     */
    public function setProposal(\AppBundle\Entity\Proposal $proposal = null)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal
     *
     * @return \AppBundle\Entity\Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return RepairDeviceInfo
     */
    public function setDevice(\AppBundle\Entity\Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }
    /**
     * @var float
     */
    private $subtotalMunicAndProcFee;

    /**
     * @var float
     */
    private $deviceTotal;


    /**
     * Set subtotalMunicAndProcFee
     *
     * @param float $subtotalMunicAndProcFee
     *
     * @return RepairDeviceInfo
     */
    public function setSubtotalMunicAndProcFee($subtotalMunicAndProcFee)
    {
        $this->subtotalMunicAndProcFee = $subtotalMunicAndProcFee;

        return $this;
    }

    /**
     * Get subtotalMunicAndProcFee
     *
     * @return float
     */
    public function getSubtotalMunicAndProcFee()
    {
        return $this->subtotalMunicAndProcFee;
    }

    /**
     * Set deviceTotal
     *
     * @param float $deviceTotal
     *
     * @return RepairDeviceInfo
     */
    public function setDeviceTotal($deviceTotal)
    {
        $this->deviceTotal = $deviceTotal;

        return $this;
    }

    /**
     * Get deviceTotal
     *
     * @return float
     */
    public function getDeviceTotal()
    {
        return $this->deviceTotal;
    }

    /**
     * Set inspectionEstimationTime
     *
     * @param float $inspectionEstimationTime
     *
     * @return RepairDeviceInfo
     */
    public function setInspectionEstimationTime($inspectionEstimationTime)
    {
        $this->inspectionEstimationTime = $this->filterPrice($inspectionEstimationTime);

        return $this;
    }

    /**
     * Get inspectionEstimationTime
     *
     * @return float
     */
    public function getInspectionEstimationTime()
    {
        return $this->inspectionEstimationTime;
    }

    /**
     * Set inspectionPrice
     *
     * @param float $inspectionPrice
     *
     * @return RepairDeviceInfo
     */
    public function setInspectionPrice($inspectionPrice)
    {
        $this->inspectionPrice = $this->filterPrice($inspectionPrice);

        return $this;
    }

    /**
     * Get inspectionPrice
     *
     * @return float
     */
    public function getInspectionPrice()
    {
        return $this->inspectionPrice;
    }

    /**
     * Set isGlued
     *
     * @param boolean $isGlued
     *
     * @return RepairDeviceInfo
     */
    public function setIsGlued($isGlued)
    {
        $this->isGlued = $isGlued;

        return $this;
    }

    /**
     * Get isGlued
     *
     * @return boolean
     */
    public function getIsGlued()
    {
        return $this->isGlued;
    }

    /**
     * Set departmentChannelWithFeesWillVary
     *
     * @param boolean $departmentChannelWithFeesWillVary
     *
     * @return RepairDeviceInfo
     */
    public function setDepartmentChannelWithFeesWillVary($departmentChannelWithFeesWillVary)
    {
        $this->departmentChannelWithFeesWillVary = $departmentChannelWithFeesWillVary;

        return $this;
    }

    /**
     * Get departmentChannelWithFeesWillVary
     *
     * @return boolean
     */
    public function getDepartmentChannelWithFeesWillVary()
    {
        return $this->departmentChannelWithFeesWillVary;
    }
}
