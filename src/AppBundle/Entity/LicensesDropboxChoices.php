<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * LicensesDropboxChoices
 */
class LicensesDropboxChoices
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $optionValue;

    /**
     * @var boolean
     */
    private $selectDefault = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\LicensesDynamicField
     */
    private $field;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optionValue
     *
     * @param string $optionValue
     *
     * @return LicensesDropboxChoices
     */
    public function setOptionValue($optionValue)
    {
        $this->optionValue = $optionValue;

        return $this;
    }

    /**
     * Get optionValue
     *
     * @return string
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }

    /**
     * Set selectDefault
     *
     * @param boolean $selectDefault
     *
     * @return LicensesDropboxChoices
     */
    public function setSelectDefault($selectDefault)
    {
        $this->selectDefault = $selectDefault;

        return $this;
    }

    /**
     * Get selectDefault
     *
     * @return boolean
     */
    public function getSelectDefault()
    {
        return $this->selectDefault;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return LicensesDropboxChoices
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return LicensesDropboxChoices
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set field
     *
     * @param \AppBundle\Entity\LicensesDynamicField $field
     *
     * @return LicensesDropboxChoices
     */
    public function setField(\AppBundle\Entity\LicensesDynamicField $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \AppBundle\Entity\LicensesDynamicField
     */
    public function getField()
    {
        return $this->field;
    }
}
