<?php

namespace AppBundle\Entity;

/**
 * ServiceNamed
 */
class ServiceNamed
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $forAccount = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ServiceNamed
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set forAccount
     *
     * @param boolean $forAccount
     *
     * @return ServiceNamed
     */
    public function setForAccount($forAccount)
    {
        $this->forAccount = $forAccount;

        return $this;
    }

    /**
     * Get forAccount
     *
     * @return boolean
     */
    public function getForAccount()
    {
        return $this->forAccount;
    }
    /**
     * @var \AppBundle\Entity\ServiceFrequency
     */
    private $frequency;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $deviceNamed;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->deviceNamed = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set frequency
     *
     * @param \AppBundle\Entity\ServiceFrequency $frequency
     *
     * @return ServiceNamed
     */
    public function setFrequency(\AppBundle\Entity\ServiceFrequency $frequency = null)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return \AppBundle\Entity\ServiceFrequency
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Add deviceNamed
     *
     * @param \AppBundle\Entity\DeviceNamed $deviceNamed
     *
     * @return ServiceNamed
     */
    public function addDeviceNamed(\AppBundle\Entity\DeviceNamed $deviceNamed)
    {
        $this->deviceNamed[] = $deviceNamed;

        return $this;
    }

    /**
     * Remove deviceNamed
     *
     * @param \AppBundle\Entity\DeviceNamed $deviceNamed
     */
    public function removeDeviceNamed(\AppBundle\Entity\DeviceNamed $deviceNamed)
    {
        $this->deviceNamed->removeElement($deviceNamed);
    }

    /**
     * Get deviceNamed
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeviceNamed()
    {
        return $this->deviceNamed;
    }
    /**
     * @var string
     */
    private $deficiency;


    /**
     * Set deficiency
     *
     * @param string $deficiency
     *
     * @return ServiceNamed
     */
    public function setDeficiency($deficiency)
    {
        $this->deficiency = $deficiency;

        return $this;
    }

    /**
     * Get deficiency
     *
     * @return string
     */
    public function getDeficiency()
    {
        return $this->deficiency;
    }
    /**
     * @var boolean
     */
    private $isNeedSendMunicipalityReport = false;


    /**
     * Set isNeedSendMunicipalityReport
     *
     * @param boolean $isNeedSendMunicipalityReport
     *
     * @return ServiceNamed
     */
    public function setIsNeedSendMunicipalityReport($isNeedSendMunicipalityReport)
    {
        $this->isNeedSendMunicipalityReport = $isNeedSendMunicipalityReport;

        return $this;
    }

    /**
     * Get isNeedSendMunicipalityReport
     *
     * @return boolean
     */
    public function getIsNeedSendMunicipalityReport()
    {
        return $this->isNeedSendMunicipalityReport;
    }
    /**
     * @var boolean
     */
    private $isNeedMunicipalityFee = false;


    /**
     * Set isNeedMunicipalityFee
     *
     * @param boolean $isNeedMunicipalityFee
     *
     * @return ServiceNamed
     */
    public function setIsNeedMunicipalityFee($isNeedMunicipalityFee)
    {
        $this->isNeedMunicipalityFee = $isNeedMunicipalityFee;

        return $this;
    }

    /**
     * Get isNeedMunicipalityFee
     *
     * @return boolean
     */
    public function getIsNeedMunicipalityFee()
    {
        return $this->isNeedMunicipalityFee;
    }
}
