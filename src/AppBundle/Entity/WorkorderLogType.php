<?php

namespace AppBundle\Entity;

/**
 * WorkorderLogType
 *
 * (don't auto generate this entity)
 * - was added type constants
 */
class WorkorderLogType
{
    public const TYPE_WORKORDER_WAS_SCHEDULED = 'workorder_was_scheduled';
    public const TYPE_WORKORDER_TO_BE_SCHEDULED = 'workorder_to_be_scheduled';
    public const TYPE_BILL_SENT_INVOICE_CREATE = 'bill_sent_invoice_created';
    public const TYPE_PAYMEN_RECEIVED = 'payment_received';
    public const TYPE_REPORTS_SENT = 'reports_sent';
    public const TYPE_WORKORDER_WAS_CREATED = 'workorder_was_created';
    public const TYPE_SCHEDULED_DATE_HAS_COME = 'scheduled_date_has_come';
    public const TYPE_DONE = 'done';
    public const TYPE_SCHEDULED_DATE_HAS_PASSED = 'scheduled_date_has_passed';
    public const TYPE_DELETE = 'delete';
    public const TYPE_RESCHEDULE = 'reschedule';
    public const TYPE_SERVICE_WERE_ADDED = 'services_were_added';
    public const TYPE_VERIFY_JOB_RESULTS = 'verify_job_results';
    public const TYPE_SUBMIT_INVOICE = 'submit_invoice';
    public const TYPE_USE_CUSTOM_PDF = 'use_custom_pdf';
    public const TYPE_INVOICE_SENT_VIA_MAIL = 'invoice_sent_via_email';
    public const TYPE_INVOICE_SENT_VIA_MAIL_OR_FAX = 'invoice_sent_via_mail_fax';
    public const TYPE_SEND_REPORTS_TO_ACCOUNT_AUTHORIZER = 'send_reports_to_account_authorizer';
    public const TYPE_REPORTS_SENT_ALONG_WITH_INVOICE_VIA_MAIL_OR_FAX =
        'reports_sent_along_with_invoice_via_mail_fax';
    public const TYPE_REPORTS_SENT_TO_CLIENT_VIA_MAIL_OR_FAX = 'reports_sent_to_client_via_mail_fax';
    public const TYPE_REPORTS_SENT_TO_CLIENT_VIA_EMAIL = 'reports_sent_to_client_via_email';
    public const TYPE_REPORTS_SEND_TO_AHJ_VIA_MAIL_OR_FAX = 'reports_send_to_ahj_via_mail_fax';
    public const TYPE_REPORTS_SENT_TO_AHJ_VIA_MAIL_OR_FAX = 'reports_sent_to_ahj_via_mail_fax';
    public const TYPE_REPORTS_SENT_TO_AHJ_VIA_EMAIL = 'reports_sent_to_ahj_via_email';
    public const TYPE_PAYMENT_RECEIVED = 'pending_payment';
    public const TYPE_PAYMENTS_NOT_FOUND_IN_QUICKBOOKS = 'payments_not_found_in_quickbooks';
    public const TYPE_PAYMENT_WAS_RECEIVED = 'done';
    public const TYPE_INVOICE_DRAFT_WAS_SAVE = 'invoice_draft_was_save';
    public const TYPE_INVOICE_SUBMITTED = 'invoice_submitted';
    public const TYPE_INVOICE_PAYMENT_OVERRIDE = 'invoice_payments_override_was_edited';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WorkorderLogType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return WorkorderLogType
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }
}
