<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;

/**
 * Question
 */
class Question
{
    use DateTimeControlTrait;
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $statement;

    /**
     * @var boolean
     */
    private $isSelectByDefault = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\QuestionType
     */
    private $questionType;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statement
     *
     * @param string $statement
     *
     * @return Question
     */
    public function setStatement($statement)
    {
        $this->statement = $statement;

        return $this;
    }

    /**
     * Get statement
     *
     * @return string
     */
    public function getStatement()
    {
        return $this->statement;
    }

    /**
     * Set isSelectByDefault
     *
     * @param boolean $isSelectByDefault
     *
     * @return Question
     */
    public function setIsSelectByDefault($isSelectByDefault)
    {
        $this->isSelectByDefault = $isSelectByDefault;

        return $this;
    }

    /**
     * Get isSelectByDefault
     *
     * @return boolean
     */
    public function getIsSelectByDefault()
    {
        return $this->isSelectByDefault;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Question
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Question
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set questionType
     *
     * @param \AppBundle\Entity\QuestionType $questionType
     *
     * @return Question
     */
    public function setQuestionType(\AppBundle\Entity\QuestionType $questionType = null)
    {
        $this->questionType = $questionType;

        return $this;
    }

    /**
     * Get questionType
     *
     * @return \AppBundle\Entity\QuestionType
     */
    public function getQuestionType()
    {
        return $this->questionType;
    }
}
