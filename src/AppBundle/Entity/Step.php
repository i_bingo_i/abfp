<?php

namespace AppBundle\Entity;

/**
 * Step
 */
class Step
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \AppBundle\Entity\StepStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\StepNamed
     */
    private $named;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\StepStatus $status
     *
     * @return Step
     */
    public function setStatus(\AppBundle\Entity\StepStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\StepStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\StepNamed $named
     *
     * @return Step
     */
    public function setNamed(\AppBundle\Entity\StepNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\StepNamed
     */
    public function getNamed()
    {
        return $this->named;
    }
    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;


    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return Step
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }
}
