<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;


/**
 * LicensesDynamicFieldValue
 */
class LicensesDynamicFieldValue
{
    use DateTimeControlTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \AppBundle\Entity\LicensesDynamicField
     */
    private $field;

    /**
     * @var \AppBundle\Entity\Licenses
     */
    private $license;

    /**
     * @var \AppBundle\Entity\LicensesDropboxChoices
     */
    private $optionValue;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return LicensesDynamicFieldValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return LicensesDynamicFieldValue
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return LicensesDynamicFieldValue
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set field
     *
     * @param \AppBundle\Entity\LicensesDynamicField $field
     *
     * @return LicensesDynamicFieldValue
     */
    public function setField(\AppBundle\Entity\LicensesDynamicField $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \AppBundle\Entity\LicensesDynamicField
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set license
     *
     * @param \AppBundle\Entity\Licenses $license
     *
     * @return LicensesDynamicFieldValue
     */
    public function setLicense(\AppBundle\Entity\Licenses $license = null)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license
     *
     * @return \AppBundle\Entity\Licenses
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set optionValue
     *
     * @param \AppBundle\Entity\LicensesDropboxChoices $optionValue
     *
     * @return LicensesDynamicFieldValue
     */
    public function setOptionValue(\AppBundle\Entity\LicensesDropboxChoices $optionValue = null)
    {
        $this->optionValue = $optionValue;

        return $this;
    }

    /**
     * Get optionValue
     *
     * @return \AppBundle\Entity\LicensesDropboxChoices
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }
}
