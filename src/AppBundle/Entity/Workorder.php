<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use AppBundle\Entity\EntityTrait\PriceFilterTrait;
use InvoiceBundle\Entity\Invoices;

/**
 * Workorder
 *  (don't auto generate this entity)
 * - was added code filtering for grandTotal and $totalServiceFee field
 */
class Workorder
{
    use DateTimeControlTrait;
    use PriceFilterTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $grandTotal;

    /**
     * @var float
     */
    private $totalServiceFee;

    /**
     * @var float
     */
    private $afterHoursWorkCost;

    /**
     * @var float
     */
    private $additionalServicesCost;

    /**
     * @var float
     */
    private $discount;

    /**
     * @var string
     */
    private $directions;

    /**
     * @var string
     */
    private $internalComment;

    /**
     * @var string
     */
    private $additionalComments;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $services;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $logs;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var \AppBundle\Entity\WorkorderStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\DeviceCategory
     */
    private $division;

    /**
     * @var \AppBundle\Entity\AccountContactPerson
     */
    private $authorizer;

    /**
     * @var \AppBundle\Entity\PaymentMethod
     */
    private $paymentMethod;

    /**
     * @var \DateTime
     */
    private $scheduledFrom;

    /**
     * @var \DateTime
     */
    private $scheduledTo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $frozenContent;

    /**
     * @var boolean
     */
    private $isFrozen = false;

    /**
     * @var \AppBundle\Entity\AccountContactPerson
     */
    private $accountAuthorizer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $events;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $invoices;

    /**
     * @var \AppBundle\Entity\PaymentTerm
     */
    private $paymentTerm;

    /**
     * @var float
     */
    private $totalEstimatedTimeInspections;

    /**
     * @var float
     */
    private $totalEstimatedTimeRepairs;

    /**
     * @var float
     */
    private $totalRepairsPrice;

    /**
     * @var float
     */
    private $totalMunicAndProcFee;

    /**
     * @var float
     */
    private $totalPrice;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $notIncludedItems;

    /**
     * @var string
     */
    private $signature;

    /**
     * @var \DateTime
     */
    private $finishTime;

    /**
     * @var \AppBundle\Entity\Coordinate
     */
    private $location;

    /**
     * @var integer
     */
    private $totalInspectionsCount;

    /**
     * @var integer
     */
    private $totalRepairsCount;

    /**
     * @var \AppBundle\Entity\ContractorUser
     */
    private $finisher;

    /**
     * @var \AppBundle\Entity\AccountContactPerson
     */
    private $signatureOwner;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $proposals;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $workorderDeviceInfo;

    /**
     * @var \AppBundle\Entity\User
     */
    private $createdBy;

    /**
     * @var string
     */
    private $pdf;

    /**
     * @var \AppBundle\Entity\WorkorderType
     */
    private $type;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comments;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $steps;

    /**
     * @var boolean
     */
    private $isMunicipalityFessWillVary = false;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $letters;

    /** @var string */
    private $poNo;

    /**
     * @var string
     */
    private $additionalCostComment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
        $this->logs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->frozenContent = new \Doctrine\Common\Collections\ArrayCollection();
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        $this->invoices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notIncludedItems = new \Doctrine\Common\Collections\ArrayCollection();
        $this->steps = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set grandTotal
     *
     * @param float $grandTotal
     *
     * @return Workorder
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grandTotal = $this->filterPrice($grandTotal);

        return $this;
    }

    /**
     * Get grandTotal
     *
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->grandTotal;
    }

    /**
     * Set totalServiceFee
     *
     * @param float $totalServiceFee
     *
     * @return Workorder
     */
    public function setTotalServiceFee($totalServiceFee)
    {
        $this->totalServiceFee = $this->filterPrice($totalServiceFee);

        return $this;
    }

    /**
     * Get totalServiceFee
     *
     * @return float
     */
    public function getTotalServiceFee()
    {
        return $this->totalServiceFee;
    }

    /**
     * Set afterHoursWorkCost
     *
     * @param float $afterHoursWorkCost
     *
     * @return Workorder
     */
    public function setAfterHoursWorkCost($afterHoursWorkCost)
    {
        $this->afterHoursWorkCost = $this->filterPrice($afterHoursWorkCost);

        return $this;
    }

    /**
     * Get afterHoursWorkCost
     *
     * @return float
     */
    public function getAfterHoursWorkCost()
    {
        return $this->afterHoursWorkCost;
    }

    /**
     * Set additionalServicesCost
     *
     * @param float $additionalServicesCost
     *
     * @return Workorder
     */
    public function setAdditionalServicesCost($additionalServicesCost)
    {
        $this->additionalServicesCost = $this->filterPrice($additionalServicesCost);

        return $this;
    }

    /**
     * Get additionalServicesCost
     *
     * @return float
     */
    public function getAdditionalServicesCost()
    {
        return $this->additionalServicesCost;
    }

    /**
     * Set discount
     *
     * @param float $discount
     *
     * @return Workorder
     */
    public function setDiscount($discount)
    {
        $this->discount = $this->filterPrice($discount);

        return $this;
    }

    /**
     * Get discount
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set directions
     *
     * @param string $directions
     *
     * @return Workorder
     */
    public function setDirections($directions)
    {
        $this->directions = $directions;

        return $this;
    }

    /**
     * Get directions
     *
     * @return string
     */
    public function getDirections()
    {
        return $this->directions;
    }

    /**
     * Set internalComment
     *
     * @param string $internalComment
     *
     * @return Workorder
     */
    public function setInternalComment($internalComment)
    {
        $this->internalComment = $internalComment;

        return $this;
    }

    /**
     * Get internalComment
     *
     * @return string
     */
    public function getInternalComment()
    {
        return $this->internalComment;
    }

    /**
     * Set additionalComments
     *
     * @param string $additionalComments
     *
     * @return Workorder
     */
    public function setAdditionalComments($additionalComments)
    {
        $this->additionalComments = $additionalComments;

        return $this;
    }

    /**
     * Get additionalComments
     *
     * @return string
     */
    public function getAdditionalComments()
    {
        return $this->additionalComments;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Workorder
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Workorder
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Add service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return Workorder
     */
    public function addService(\AppBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \AppBundle\Entity\Service $service
     */
    public function removeService(\AppBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add log
     *
     * @param \AppBundle\Entity\WorkorderLog $log
     *
     * @return Workorder
     */
    public function addLog(\AppBundle\Entity\WorkorderLog $log)
    {
        $this->logs[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \AppBundle\Entity\WorkorderLog $log
     */
    public function removeLog(\AppBundle\Entity\WorkorderLog $log)
    {
        $this->logs->removeElement($log);
    }

    /**
     * Get logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return Workorder
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\WorkorderStatus $status
     *
     * @return Workorder
     */
    public function setStatus(\AppBundle\Entity\WorkorderStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\WorkorderStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set division
     *
     * @param \AppBundle\Entity\DeviceCategory $division
     *
     * @return Workorder
     */
    public function setDivision(\AppBundle\Entity\DeviceCategory $division = null)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * Get division
     *
     * @return \AppBundle\Entity\DeviceCategory
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Set authorizer
     *
     * @param \AppBundle\Entity\AccountContactPerson $authorizer
     *
     * @return Workorder
     */
    public function setAuthorizer(\AppBundle\Entity\AccountContactPerson $authorizer = null)
    {
        $this->authorizer = $authorizer;

        return $this;
    }

    /**
     * Get authorizer
     *
     * @return \AppBundle\Entity\AccountContactPerson
     */
    public function getAuthorizer()
    {
        return $this->authorizer;
    }

    /**
     * Set paymentMethod
     *
     * @param \AppBundle\Entity\PaymentMethod $paymentMethod
     *
     * @return Workorder
     */
    public function setPaymentMethod(\AppBundle\Entity\PaymentMethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \AppBundle\Entity\PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }
        
    /**
     * Set scheduledFrom
     *
     * @param \DateTime $scheduledFrom
     *
     * @return Workorder
     */
    public function setScheduledFrom($scheduledFrom)
    {
        $this->scheduledFrom = $scheduledFrom;

        return $this;
    }

    /**
     * Get scheduledFrom
     *
     * @return \DateTime
     */
    public function getScheduledFrom()
    {
        return $this->scheduledFrom;
    }

    /**
     * Set scheduledTo
     *
     * @param \DateTime $scheduledTo
     *
     * @return Workorder
     */
    public function setScheduledTo($scheduledTo)
    {
        $this->scheduledTo = $scheduledTo;

        return $this;
    }

    /**
     * Get scheduledTo
     *
     * @return \DateTime
     */
    public function getScheduledTo()
    {
        return $this->scheduledTo;
    }

    /**
     * Add frozenContent
     *
     * @param \AppBundle\Entity\WorkorderFrozenContent $frozenContent
     *
     * @return Workorder
     */
    public function addFrozenContent(\AppBundle\Entity\WorkorderFrozenContent $frozenContent)
    {
        $this->frozenContent[] = $frozenContent;

        return $this;
    }

    /**
     * Remove frozenContent
     *
     * @param \AppBundle\Entity\WorkorderFrozenContent $frozenContent
     */
    public function removeFrozenContent(\AppBundle\Entity\WorkorderFrozenContent $frozenContent)
    {
        $this->frozenContent->removeElement($frozenContent);
    }

    /**
     * Get frozenContent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFrozenContent()
    {
        return $this->frozenContent;
    }

    /**
     * Add invoice
     *
     * @param Invoices $invoice
     *
     * @return Workorder
     */
    public function addInvoice(Invoices $invoice)
    {
        $this->invoices[] = $invoice;

        return $this;
    }

    /**
     * Remove invoice
     *
     * @param Invoices $invoice
     */
    public function removeInvoice(Invoices $invoice)
    {
        $this->invoices->removeElement($invoice);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * Set isFrozen
     *
     * @param boolean $isFrozen
     *
     * @return Workorder
     */
    public function setIsFrozen($isFrozen)
    {
        $this->isFrozen = $isFrozen;

        return $this;
    }

    /**
     * Get isFrozen
     *
     * @return boolean
     */
    public function getIsFrozen()
    {
        return $this->isFrozen;
    }

    /**
     * Set accountAuthorizer
     *
     * @param \AppBundle\Entity\AccountContactPerson $accountAuthorizer
     *
     * @return Workorder
     */
    public function setAccountAuthorizer(\AppBundle\Entity\AccountContactPerson $accountAuthorizer = null)
    {
        $this->accountAuthorizer = $accountAuthorizer;

        return $this;
    }

    /**
     * Get accountAuthorizer
     *
     * @return \AppBundle\Entity\AccountContactPerson
     */
    public function getAccountAuthorizer()
    {
        return $this->accountAuthorizer;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return Workorder
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Set paymentTerm
     *
     * @param \AppBundle\Entity\PaymentTerm $paymentTerm
     *
     * @return Workorder
     */
    public function setPaymentTerm(\AppBundle\Entity\PaymentTerm $paymentTerm = null)
    {
        $this->paymentTerm = $paymentTerm;

        return $this;
    }

    /**
     * Get paymentTerm
     *
     * @return \AppBundle\Entity\PaymentTerm
     */
    public function getPaymentTerm()
    {
        return $this->paymentTerm;
    }

    /**
     * Add proposal
     *
     * @param \AppBundle\Entity\Proposal $proposal
     *
     * @return Workorder
     */
    public function addProposal(\AppBundle\Entity\Proposal $proposal)
    {
        $this->proposals[] = $proposal;

        return $this;
    }

    /**
     * Remove proposal
     *
     * @param \AppBundle\Entity\Proposal $proposal
     */
    public function removeProposal(\AppBundle\Entity\Proposal $proposal)
    {
        $this->proposals->removeElement($proposal);
    }

    /**
     * Get proposals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProposals()
    {
        return $this->proposals;
    }

    /**
     * Add workorderDeviceInfo
     *
     * @param \AppBundle\Entity\WorkorderDeviceInfo $workorderDeviceInfo
     *
     * @return Workorder
     */
    public function addWorkorderDeviceInfo(\AppBundle\Entity\WorkorderDeviceInfo $workorderDeviceInfo)
    {
        $this->workorderDeviceInfo[] = $workorderDeviceInfo;

        return $this;
    }

    /**
     * Remove workorderDeviceInfo
     *
     * @param \AppBundle\Entity\WorkorderDeviceInfo $workorderDeviceInfo
     */
    public function removeWorkorderDeviceInfo(\AppBundle\Entity\WorkorderDeviceInfo $workorderDeviceInfo)
    {
        $this->workorderDeviceInfo->removeElement($workorderDeviceInfo);
    }

    /**
     * Get workorderDeviceInfo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkorderDeviceInfo()
    {
        return $this->workorderDeviceInfo;
    }

    /**
     * Set totalEstimatedTimeInspections
     *
     * @param float $totalEstimatedTimeInspections
     *
     * @return Workorder
     */
    public function setTotalEstimatedTimeInspections($totalEstimatedTimeInspections)
    {
        $this->totalEstimatedTimeInspections = $totalEstimatedTimeInspections;

        return $this;
    }

    /**
     * Get totalEstimatedTimeInspections
     *
     * @return float
     */
    public function getTotalEstimatedTimeInspections()
    {
        return $this->totalEstimatedTimeInspections;
    }

    /**
     * Set totalEstimatedTimeRepairs
     *
     * @param float $totalEstimatedTimeRepairs
     *
     * @return Workorder
     */
    public function setTotalEstimatedTimeRepairs($totalEstimatedTimeRepairs)
    {
        $this->totalEstimatedTimeRepairs = $totalEstimatedTimeRepairs;

        return $this;
    }

    /**
     * Get totalEstimatedTimeRepairs
     *
     * @return float
     */
    public function getTotalEstimatedTimeRepairs()
    {
        return $this->totalEstimatedTimeRepairs;
    }

    /**
     * Set signature
     *
     * @param string $signature
     *
     * @return Workorder
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set finishTime
     *
     * @param \DateTime $finishTime
     *
     * @return Workorder
     */
    public function setFinishTime($finishTime)
    {
        $this->finishTime = $finishTime;

        return $this;
    }

    /**
     * Get finishTime
     *
     * @return \DateTime
     */
    public function getFinishTime()
    {
        return $this->finishTime;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\Coordinate $location
     *
     * @return Workorder
     */
    public function setLocation(\AppBundle\Entity\Coordinate $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\Coordinate
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set totalRepairsPrice
     *
     * @param float $totalRepairsPrice
     *
     * @return Workorder
     */
    public function setTotalRepairsPrice($totalRepairsPrice)
    {
        $this->totalRepairsPrice = $totalRepairsPrice;

        return $this;
    }

    /**
     * Get totalRepairsPrice
     *
     * @return float
     */
    public function getTotalRepairsPrice()
    {
        return $this->totalRepairsPrice;
    }

    /**
     * Set totalMunicAndProcFee
     *
     * @param float $totalMunicAndProcFee
     *
     * @return Workorder
     */
    public function setTotalMunicAndProcFee($totalMunicAndProcFee)
    {
        $this->totalMunicAndProcFee = $totalMunicAndProcFee;

        return $this;
    }

    /**
     * Get totalMunicAndProcFee
     *
     * @return float
     */
    public function getTotalMunicAndProcFee()
    {
        return $this->totalMunicAndProcFee;
    }

    /**
     * Set totalInspectionsCount
     *
     * @param float $totalInspectionsCount
     *
     * @return Workorder
     */
    public function setTotalInspectionsCount($totalInspectionsCount)
    {
        $this->totalInspectionsCount = $totalInspectionsCount;

        return $this;
    }

    /**
     * Get totalInspectionsCount
     *
     * @return float
     */
    public function getTotalInspectionsCount()
    {
        return $this->totalInspectionsCount;
    }

    /**
     * Set totalRepairsCount
     *
     * @param float $totalRepairsCount
     *
     * @return Workorder
     */
    public function setTotalRepairsCount($totalRepairsCount)
    {
        $this->totalRepairsCount = $totalRepairsCount;

        return $this;
    }

    /**
     * Get totalRepairsCount
     *
     * @return float
     */
    public function getTotalRepairsCount()
    {
        return $this->totalRepairsCount;
    }

    /**
     * Set totalPrice
     *
     * @param float $totalPrice
     *
     * @return Workorder
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Add notIncludedItem
     *
     * @param \AppBundle\Entity\NotIncluded $notIncludedItem
     *
     * @return Workorder
     */
    public function addNotIncludedItem(\AppBundle\Entity\NotIncluded $notIncludedItem)
    {
        $this->notIncludedItems[] = $notIncludedItem;

        return $this;
    }

    /**
     * Remove notIncludedItem
     *
     * @param \AppBundle\Entity\NotIncluded $notIncludedItem
     */
    public function removeNotIncludedItem(\AppBundle\Entity\NotIncluded $notIncludedItem)
    {
        $this->notIncludedItems->removeElement($notIncludedItem);
    }

    /**
     * Get notIncludedItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotIncludedItems()
    {
        return $this->notIncludedItems;
    }

    /**
     * Set finisher
     *
     * @param \AppBundle\Entity\ContractorUser $finisher
     *
     * @return Workorder
     */
    public function setFinisher(\AppBundle\Entity\ContractorUser $finisher = null)
    {
        $this->finisher = $finisher;

        return $this;
    }

    /**
     * Get finisher
     *
     * @return \AppBundle\Entity\ContractorUser
     */
    public function getFinisher()
    {
        return $this->finisher;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Workorder
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     *
     * @return Workorder
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\WorkorderType $type
     *
     * @return Workorder
     */
    public function setType(\AppBundle\Entity\WorkorderType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\WorkorderType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set signatureOwner
     *
     * @param \AppBundle\Entity\AccountContactPerson $signatureOwner
     *
     * @return Workorder
     */
    public function setSignatureOwner(\AppBundle\Entity\AccountContactPerson $signatureOwner = null)
    {
        $this->signatureOwner = $signatureOwner;

        return $this;
    }

    /**
     * Get signatureOwner
     *
     * @return \AppBundle\Entity\AccountContactPerson
     */
    public function getSignatureOwner()
    {
        return $this->signatureOwner;
    }

    /**
     * @return bool
     */
    public function checkIsFrozen()
    {
        if($this->getStatus()->getAlias() == "send_bill_create_invoice") {
          return true;
        }

        return $this->getIsFrozen();
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\WorkorderComment $comment
     *
     * @return Workorder
     */
    public function addComment(\AppBundle\Entity\WorkorderComment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\WorkorderComment $comment
     */
    public function removeComment(\AppBundle\Entity\WorkorderComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return bool
     */
    public function checkIsReScheduled()
    {
        if ($this->getStatus()->getAlias() == "reschedule") {
            return true;
        }

        return false;
    }

    /**
     * Set isMunicipalityFessWillVary
     *
     * @param boolean $isMunicipalityFessWillVary
     *
     * @return Workorder
     */
    public function setIsMunicipalityFessWillVary($isMunicipalityFessWillVary)
    {
        $this->isMunicipalityFessWillVary = $isMunicipalityFessWillVary;

        return $this;
    }

    /**
     * Get isMunicipalityFessWillVary
     *
     * @return boolean
     */
    public function getIsMunicipalityFessWillVary()
    {
        return $this->isMunicipalityFessWillVary;
    }

    /**
     * Add step
     *
     * @param \AppBundle\Entity\Step $step
     *
     * @return Workorder
     */
    public function addStep(\AppBundle\Entity\Step $step)
    {
        $this->steps[] = $step;

        return $this;
    }

    /**
     * Remove step
     *
     * @param \AppBundle\Entity\Step $step
     */
    public function removeStep(\AppBundle\Entity\Step $step)
    {
        $this->steps->removeElement($step);
    }

    /**
     * Get steps
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSteps()
    {
        return $this->steps;
    }


    /**
     * Add letter
     *
     * @param \AppBundle\Entity\Letter $letter
     *
     * @return Workorder
     */
    public function addLetter(\AppBundle\Entity\Letter $letter)
    {
        $this->letters[] = $letter;

        return $this;
    }

    /**
     * Remove letter
     *
     * @param \AppBundle\Entity\Letter $letter
     */
    public function removeLetter(\AppBundle\Entity\Letter $letter)
    {
        $this->letters->removeElement($letter);
    }

    /**
     * Get letters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLetters()
    {
        return $this->letters;
    }

    /**
     * @return string
     */
    public function getPoNo()
    {
        return $this->poNo;
    }

    /**
     * @param string $poNo
     */
    public function setPoNo($poNo)
    {
        $this->poNo = $poNo;
    }

    /**
     * Set additionalCostComment
     *
     * @param string $additionalCostComment
     *
     * @return Workorder
     */
    public function setAdditionalCostComment($additionalCostComment)
    {
        $this->additionalCostComment = $additionalCostComment;

        return $this;
    }

    /**
     * Get additionalCostComment
     *
     * @return string
     */
    public function getAdditionalCostComment()
    {
        return $this->additionalCostComment;
    }
}
