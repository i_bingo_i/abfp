<?php

namespace AppBundle\Entity;

/**
 * WorkorderDeviceInfo
 */
class WorkorderDeviceInfo
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $repairComments;

    /**
     * @var float
     */
    private $repairEstimationTime;

    /**
     * @var float
     */
    private $inspectionEstimationTime;

    /**
     * @var float
     */
    private $subtotalInspectionsFee;

    /**
     * @var float
     */
    private $repairPartsAndLabourPrice;

    /**
     * @var float
     */
    private $subtotalMunicAndProcFee;

    /**
     * @var float
     */
    private $deviceTotal;

    /**
     * @var \AppBundle\Entity\SpecialNotification
     */
    private $repairSpecialNotification;

    /**
     * @var \AppBundle\Entity\Workorder
     */
    private $workorder;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $device;

    /**
     * @var boolean
     */
    private $departmentChannelWithFeesWillVary = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set repairComments
     *
     * @param string $repairComments
     *
     * @return WorkorderDeviceInfo
     */
    public function setRepairComments($repairComments)
    {
        $this->repairComments = $repairComments;

        return $this;
    }

    /**
     * Get repairComments
     *
     * @return string
     */
    public function getRepairComments()
    {
        return $this->repairComments;
    }

    /**
     * Set repairEstimationTime
     *
     * @param float $repairEstimationTime
     *
     * @return WorkorderDeviceInfo
     */
    public function setRepairEstimationTime($repairEstimationTime)
    {
        $this->repairEstimationTime = $repairEstimationTime;

        return $this;
    }

    /**
     * Get repairEstimationTime
     *
     * @return float
     */
    public function getRepairEstimationTime()
    {
        return $this->repairEstimationTime;
    }

    /**
     * Set inspectionEstimationTime
     *
     * @param float $inspectionEstimationTime
     *
     * @return WorkorderDeviceInfo
     */
    public function setInspectionEstimationTime($inspectionEstimationTime)
    {
        $this->inspectionEstimationTime = $inspectionEstimationTime;

        return $this;
    }

    /**
     * Get inspectionEstimationTime
     *
     * @return float
     */
    public function getInspectionEstimationTime()
    {
        return $this->inspectionEstimationTime;
    }

    /**
     * Set subtotalInspectionsFee
     *
     * @param float $subtotalInspectionsFee
     *
     * @return WorkorderDeviceInfo
     */
    public function setSubtotalInspectionsFee($subtotalInspectionsFee)
    {
        $this->subtotalInspectionsFee = $subtotalInspectionsFee;

        return $this;
    }

    /**
     * Get subtotalInspectionsFee
     *
     * @return float
     */
    public function getSubtotalInspectionsFee()
    {
        return $this->subtotalInspectionsFee;
    }

    /**
     * Set repairPartsAndLabourPrice
     *
     * @param float $repairPartsAndLabourPrice
     *
     * @return WorkorderDeviceInfo
     */
    public function setRepairPartsAndLabourPrice($repairPartsAndLabourPrice)
    {
        $this->repairPartsAndLabourPrice = $repairPartsAndLabourPrice;

        return $this;
    }

    /**
     * Get repairPartsAndLabourPrice
     *
     * @return float
     */
    public function getRepairPartsAndLabourPrice()
    {
        return $this->repairPartsAndLabourPrice;
    }

    /**
     * Set subtotalMunicAndProcFee
     *
     * @param float $subtotalMunicAndProcFee
     *
     * @return WorkorderDeviceInfo
     */
    public function setSubtotalMunicAndProcFee($subtotalMunicAndProcFee)
    {
        $this->subtotalMunicAndProcFee = $subtotalMunicAndProcFee;

        return $this;
    }

    /**
     * Get subtotalMunicAndProcFee
     *
     * @return float
     */
    public function getSubtotalMunicAndProcFee()
    {
        return $this->subtotalMunicAndProcFee;
    }

    /**
     * Set deviceTotal
     *
     * @param float $deviceTotal
     *
     * @return WorkorderDeviceInfo
     */
    public function setDeviceTotal($deviceTotal)
    {
        $this->deviceTotal = $deviceTotal;

        return $this;
    }

    /**
     * Get deviceTotal
     *
     * @return float
     */
    public function getDeviceTotal()
    {
        return $this->deviceTotal;
    }

    /**
     * Set repairSpecialNotification
     *
     * @param \AppBundle\Entity\SpecialNotification $repairSpecialNotification
     *
     * @return WorkorderDeviceInfo
     */
    public function setRepairSpecialNotification(\AppBundle\Entity\SpecialNotification $repairSpecialNotification = null)
    {
        $this->repairSpecialNotification = $repairSpecialNotification;

        return $this;
    }

    /**
     * Get repairSpecialNotification
     *
     * @return \AppBundle\Entity\SpecialNotification
     */
    public function getRepairSpecialNotification()
    {
        return $this->repairSpecialNotification;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return WorkorderDeviceInfo
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return WorkorderDeviceInfo
     */
    public function setDevice(\AppBundle\Entity\Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    public function getDeviceId(){
        return $this->getDevice()->getId();
    }

    public function getWorkorderId(){
        return $this->getWorkorder()->getId();
    }

    public function getSpecialNotificationName(){
        if ($this->getRepairSpecialNotification()) {
            return $this->getRepairSpecialNotification()->getName();
        }

        return false;
    }

    /**
     * @return string
     */
    public function getAllRepairComments()
    {
        return str_replace('&nbsp;', ' ', htmlspecialchars_decode($this->getRepairComments()));
    }

    /**
     * Set departmentChannelWithFeesWillVary
     *
     * @param boolean $departmentChannelWithFeesWillVary
     *
     * @return WorkorderDeviceInfo
     */
    public function setDepartmentChannelWithFeesWillVary($departmentChannelWithFeesWillVary)
    {
        $this->departmentChannelWithFeesWillVary = $departmentChannelWithFeesWillVary;

        return $this;
    }

    /**
     * Get departmentChannelWithFeesWillVary
     *
     * @return boolean
     */
    public function getDepartmentChannelWithFeesWillVary()
    {
        return $this->departmentChannelWithFeesWillVary;
    }
}
