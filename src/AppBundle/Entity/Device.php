<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use Doctrine\Common\Collections\Collection;

/**
 * DO NOT REGENERATE THIS ENTITY!! Serializer is here!!
 *
 * Device
 */
class Device
{
    use DateTimeControlTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $oldDeviceId;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var string
     */
    private $noteToTester;

    /**
     * @var boolean
     */
    private $deleted = false;

    /**
     * @var \DateTime
     */
    private $dateCreate;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fields;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \AppBundle\Entity\DeviceNamed
     */
    private $named;

    /**
     * @var \AppBundle\Entity\DeviceStatus
     */
    private $status;

    /**
     * @var \AppBundle\Entity\Account
     */
    private $account;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $parent;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $services;

    /**
     * @var boolean
     */
    private $isHistoryProcessed = false;

    /**
     * @var \AppBundle\Entity\Device
     */
    private $alarmPanel;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \AppBundle\Entity\User
     */
    private $lastEditor;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Device
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Device
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return Device
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set noteToTester
     *
     * @param string $noteToTester
     *
     * @return Device
     */
    public function setNoteToTester($noteToTester)
    {
        $this->noteToTester = $noteToTester;

        return $this;
    }

    /**
     * Get noteToTester
     *
     * @return string
     */
    public function getNoteToTester()
    {
        return $this->noteToTester;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Device
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Device
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Device
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Add field
     *
     * @param \AppBundle\Entity\DynamicFieldValue $field
     *
     * @return Device
     */
    public function addField(\AppBundle\Entity\DynamicFieldValue $field)
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * Remove field
     *
     * @param \AppBundle\Entity\DynamicFieldValue $field
     */
    public function removeField(\AppBundle\Entity\DynamicFieldValue $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Device $child
     *
     * @return Device
     */
    public function addChild(\AppBundle\Entity\Device $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Device $child
     */
    public function removeChild(\AppBundle\Entity\Device $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set named
     *
     * @param \AppBundle\Entity\DeviceNamed $named
     *
     * @return Device
     */
    public function setNamed(\AppBundle\Entity\DeviceNamed $named = null)
    {
        $this->named = $named;

        return $this;
    }

    /**
     * Get named
     *
     * @return \AppBundle\Entity\DeviceNamed
     */
    public function getNamed()
    {
        return $this->named;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\DeviceStatus $status
     *
     * @return Device
     */
    public function setStatus(\AppBundle\Entity\DeviceStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\DeviceStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set account
     *
     * @param \AppBundle\Entity\Account $account
     *
     * @return Device
     */
    public function setAccount(\AppBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Device $parent
     *
     * @return Device
     */
    public function setParent(\AppBundle\Entity\Device $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Device
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return Device
     */
    public function addService(\AppBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \AppBundle\Entity\Service $service
     */
    public function removeService(\AppBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set oldDeviceId
     *
     * @param string $oldDeviceId
     *
     * @return Device
     */
    public function setOldDeviceId($oldDeviceId)
    {
        $this->oldDeviceId = $oldDeviceId;

        return $this;
    }

    /**
     * Get oldDeviceId
     *
     * @return string
     */
    public function getOldDeviceId()
    {
        return $this->oldDeviceId;
    }


    /**
     * Set isHistoryProcessed
     *
     * @param boolean $isHistoryProcessed
     *
     * @return Device
     */
    public function setIsHistoryProcessed($isHistoryProcessed)
    {
        $this->isHistoryProcessed = $isHistoryProcessed;

        return $this;
    }

    /**
     * Get isHistoryProcessed
     *
     * @return boolean
     */
    public function getIsHistoryProcessed()
    {
        return $this->isHistoryProcessed;
    }
    /**
     * @var string
     */
    private $sourceEntity;


    /**
     * Set sourceEntity
     *
     * @param string $sourceEntity
     *
     * @return Device
     */
    public function setSourceEntity($sourceEntity)
    {
        $this->sourceEntity = $sourceEntity;

        return $this;
    }

    /**
     * Get sourceEntity
     *
     * @return string
     */
    public function getSourceEntity()
    {
        return $this->sourceEntity;
    }

    /**
     * Set alarmPanel
     *
     * @param \AppBundle\Entity\Device $alarmPanel
     *
     * @return Device
     */
    public function setAlarmPanel(\AppBundle\Entity\Device $alarmPanel = null)
    {
        $this->alarmPanel = $alarmPanel;

        return $this;
    }

    /**
     * Get alarmPanel
     *
     * @return \AppBundle\Entity\Device
     */
    public function getAlarmPanel()
    {
        return $this->alarmPanel;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $devicesWithAlarmPanel;


    /**
     * Add devicesWithAlarmPanel
     *
     * @param \AppBundle\Entity\Device $devicesWithAlarmPanel
     *
     * @return Device
     */
    public function addDevicesWithAlarmPanel(\AppBundle\Entity\Device $devicesWithAlarmPanel)
    {
        $this->devicesWithAlarmPanel[] = $devicesWithAlarmPanel;

        return $this;
    }

    /**
     * Remove devicesWithAlarmPanel
     *
     * @param \AppBundle\Entity\Device $devicesWithAlarmPanel
     */
    public function removeDevicesWithAlarmPanel(\AppBundle\Entity\Device $devicesWithAlarmPanel)
    {
        $this->devicesWithAlarmPanel->removeElement($devicesWithAlarmPanel);
    }

    /**
     * Get devicesWithAlarmPanel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevicesWithAlarmPanel()
    {
        return $this->devicesWithAlarmPanel;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Device
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $repairDeviceInfo;


    /**
     * Add repairDeviceInfo
     *
     * @param \AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo
     *
     * @return Device
     */
    public function addRepairDeviceInfo(\AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo)
    {
        $this->repairDeviceInfo[] = $repairDeviceInfo;

        return $this;
    }

    /**
     * Remove repairDeviceInfo
     *
     * @param \AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo
     */
    public function removeRepairDeviceInfo(\AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo)
    {
        $this->repairDeviceInfo->removeElement($repairDeviceInfo);
    }

    /**
     * Get repairDeviceInfo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepairDeviceInfo()
    {
        return $this->repairDeviceInfo;
    }


    /**
     * @return mixed|null
     */
    public function getInspections()
    {
        $services = $this->separateInspectionsAndRepair($this->getServices());

        return array_key_exists("inspections", $services) ? $services["inspections"] : null;

    }

    /**
     * @return mixed|null
     */
    public function getRepairs()
    {
        $services = $this->separateInspectionsAndRepair($this->getServices());

        return array_key_exists("repairs", $services) ? $services["repairs"] : null;
    }

    /**
     * @return array
     */
    public function getAllFields()
    {

        $fieldsValues = $this->getFields();
        $composeFields = null;
        /** @var DynamicFieldValue $fieldValue */
        foreach ($fieldsValues as $fieldValue) {
            $newFieldValue = [];

            $newFieldValue["id"] = (string)$fieldValue->getId();
            $newFieldValue["description"] = $fieldValue->getField()->getName();
            $newFieldValue["description_id"] = (string)$fieldValue->getField()->getId();
            $newFieldValue["description_alias"] = $fieldValue->getField()->getAlias();
            $newFieldValue["value"] = "";

            if($fieldValue->getOptionValue()){
                $newFieldValue["value"] = $fieldValue->getOptionValue()->getOptionValue();
                $newFieldValue["isOptionValue"] = true;
                $newFieldValue["optionValueId"] = (string)$fieldValue->getOptionValue()->getId();
            } elseif(!is_null($fieldValue->getValue())) {
                $newFieldValue["value"] = (string)$fieldValue->getValue();
            }

            $composeFields[] = $newFieldValue;
        }

        return $composeFields;

    }

    /**
     * @param Collection $services
     * @return array
     */
    private function separateInspectionsAndRepair(Collection $services)
    {
        $separateServices = [];
        /** @var Service $service */
        foreach ($services as $service) {
            if (empty($service->getNamed()->getFrequency())) {
                $separateServices["repairs"][] = $service;
            } else {
                $separateServices["inspections"][] = $service;
            }
        }

        return $separateServices;
    }

    /**
     * Set lastEditor
     *
     * @param \AppBundle\Entity\User $lastEditor
     *
     * @return Device
     */
    public function setLastEditor(\AppBundle\Entity\User $lastEditor = null)
    {
        $this->lastEditor = $lastEditor;

        return $this;
    }

    /**
     * Get lastEditor
     *
     * @return \AppBundle\Entity\User
     */
    public function getLastEditor()
    {
        return $this->lastEditor;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $workorderDeviceInfo;


    /**
     * Add workorderDeviceInfo
     *
     * @param \AppBundle\Entity\WorkorderDeviceInfo $workorderDeviceInfo
     *
     * @return Device
     */
    public function addWorkorderDeviceInfo(\AppBundle\Entity\WorkorderDeviceInfo $workorderDeviceInfo)
    {
        $this->workorderDeviceInfo[] = $workorderDeviceInfo;

        return $this;
    }

    /**
     * Remove workorderDeviceInfo
     *
     * @param \AppBundle\Entity\WorkorderDeviceInfo $workorderDeviceInfo
     */
    public function removeWorkorderDeviceInfo(\AppBundle\Entity\WorkorderDeviceInfo $workorderDeviceInfo)
    {
        $this->workorderDeviceInfo->removeElement($workorderDeviceInfo);
    }

    /**
     * Get workorderDeviceInfo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkorderDeviceInfo()
    {
        return $this->workorderDeviceInfo;
    }
}
