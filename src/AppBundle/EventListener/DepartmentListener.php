<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Account;
use AppBundle\Entity\Department;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\DepartmentHistoryManager;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Events\DepartmentCreatedEvent;
use AppBundle\Events\DepartmentSetAgentEvent;
use AppBundle\Events\DepartmentUnsetAgentEvent;
use Doctrine\Common\Persistence\ObjectManager;

class DepartmentListener
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AccountManager */
    private $accountManager;
    /** @var DepartmentHistoryManager */
    private $departmentHistoryManager;
    /** @var AccountRepository */
    private $accountRepository;

    /**
     * DepartmentListener constructor.
     * @param ObjectManager $objectManager
     * @param AccountManager $accountManager
     * @param DepartmentHistoryManager $departmentHistoryManager
     */
    public function __construct(
        ObjectManager $objectManager,
        AccountManager $accountManager,
        DepartmentHistoryManager $departmentHistoryManager
    ) {
        $this->objectManager = $objectManager;
        $this->accountManager = $accountManager;
        $this->departmentHistoryManager = $departmentHistoryManager;

        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
    }

    /**
     * @param DepartmentSetAgentEvent $event
     */
    public function postSetAgent(DepartmentSetAgentEvent $event)
    {
        /** @var Department $department */
        $department = $event->getDepartment();
        /** @var Municipality $municipality */
        $municipality = $department->getMunicipality();
        /** @var Account[] $accounts */
        $accounts = $this->accountRepository->findBy(['municipality' => $municipality]);

        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->accountManager->checkDefaultDepartmentChanel($account, false);
        }
        $this->objectManager->flush();
    }

    /**
     * @param DepartmentUnsetAgentEvent $event
     */
    public function postUnsetAgent(DepartmentUnsetAgentEvent $event)
    {
        /** @var Department $department */
        $department = $event->getDepartment();
        /** @var Municipality $municipality */
        $municipality = $department->getMunicipality();
        /** @var Account[] $accounts */
        $accounts = $this->accountRepository->findBy(['municipality' => $municipality]);

        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->accountManager->checkDefaultDepartmentChanel($account, false);
        }
        $this->objectManager->flush();
    }

    /**
     * @param DepartmentCreatedEvent $event
     */
    public function postCreate(DepartmentCreatedEvent $event)
    {
        /** @var Department $department */
        $department = $event->getDepartment();
        $this->departmentHistoryManager->createDepartmentHistory($department);
    }
}
