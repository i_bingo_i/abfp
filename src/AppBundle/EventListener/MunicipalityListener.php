<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EntityManager\MunicipalityHistoryManager;
use AppBundle\Entity\Municipality;
use AppBundle\Events\MunicipalityCreatedEvent;

class MunicipalityListener
{
    /** @var MunicipalityHistoryManager */
    private $municipalityHistoryManager;

    /**
     * MunicipalityListener constructor.
     * @param MunicipalityHistoryManager $municipalityHistoryManager
     */
    public function __construct(MunicipalityHistoryManager $municipalityHistoryManager)
    {
        $this->municipalityHistoryManager = $municipalityHistoryManager;
    }

    /**
     * @param MunicipalityCreatedEvent $event
     */
    public function postCreate(MunicipalityCreatedEvent $event)
    {
        /** @var Municipality $municipality */
        $municipality = $event->getMunicipality();
        $this->municipalityHistoryManager->createAccountHistoryItem($municipality);
    }
}
