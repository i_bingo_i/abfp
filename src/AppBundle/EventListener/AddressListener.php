<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Address;
use AppBundle\Entity\AddressHistory;
use AppBundle\Factories\AddressHistoryFactory;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PostFlushEventArgs;

class AddressListener
{
    private $addressHistories = [];

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var Address $entity */
        $entity = $args->getObject();

        if ($entity instanceof Address) {
            /** @var AddressHistory $addressHistory */
            $addressHistory = AddressHistoryFactory::make($entity);
            $entityManager = $args->getObjectManager();
            $entityManager->persist($addressHistory);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        /** @var Address $entity */
        if ($entity instanceof Address) {
            $this->addressHistories[] = AddressHistoryFactory::make($entity);
        }
    }

    /**
     * @param PostFlushEventArgs $args
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        if (! empty($this->addressHistories)) {
            /** @var EntityManager $entityManager */
            $entityManager = $args->getEntityManager();

            /** @var AddressHistory $history */
            foreach ($this->addressHistories as $history) {
                $entityManager->persist($history);
            }

            $this->addressHistories = [];
            $entityManager->flush();
        }
    }
}
