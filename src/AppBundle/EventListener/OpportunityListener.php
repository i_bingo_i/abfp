<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EntityManager\OpportunityManager;
use AppBundle\Events\OpportunityCreateEvent;
use AppBundle\Events\OpportunityUpdateEvent;

class OpportunityListener
{
    /** @var OpportunityManager */
    private $opportunityManager;

    public function __construct(OpportunityManager $opportunityManager)
    {
        $this->opportunityManager = $opportunityManager;
    }

    /**
     * @param OpportunityCreateEvent $event
     */
    public function postCreate(OpportunityCreateEvent $event)
    {
    }

    /**
     * @param OpportunityUpdateEvent $event
     */
    public function postUpdate(OpportunityUpdateEvent $event)
    {
    }
}
