<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\EntityManager\ProposalLogManager;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Workorder;
use AppBundle\Events\WorkorderCreatedEvent;

class WorkorderListener
{

    /** @var ProposalManager */
    private $proposalManager;
    /** @var  ServiceManager */
    private $serviceManager;
    /** @var  ProposalLogManager */
    private $proposalLogManager;
    /** @var  WorkorderManager */
    private $workorderManager;


    /**
     * WorkorderListener constructor.
     * @param ProposalManager $proposalManager
     * @param ProposalLogManager $proposalLogManager
     * @param ServiceManager $serviceManager
     * @param WorkorderManager $workorderManager
     */
    public function __construct(
        ProposalManager $proposalManager,
        ProposalLogManager $proposalLogManager,
        ServiceManager $serviceManager,
        WorkorderManager $workorderManager
    ) {
        $this->proposalManager = $proposalManager;
        $this->proposalLogManager = $proposalLogManager;
        $this->serviceManager = $serviceManager;
        $this->workorderManager = $workorderManager;
    }

    /**
     * @param WorkorderCreatedEvent $event
     */
    public function postCreate(WorkorderCreatedEvent $event)
    {
        /** @var Workorder $workorder */
        $workorder = $event->getWorkorder();
        $this->workorderManager->setServicesStatus($workorder->getServices());
        $this->proposalManager->setStatus($workorder->getProposals()->first(), 'workorder_created');
        $this->proposalLogManager->create(
            $workorder->getProposals()->first(),
            'NOTICE ACCEPTED. WORKORDER WAS CREATED'
        );
    }
}
