<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Account;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Events\ContactPersonCreatedEvent;
use AppBundle\Events\ContactPersonUpdatedEvent;
use AppBundle\Events\ContactPersonDeletedEvent;
use AppBundle\Events\ContactPersonUnsetCompanyEvent;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;
use AppBundle\Entity\EntityManager\ContactPersonHistoryManager;
use AppBundle\Entity\Repository\AccountRepository;

class ContactPersonListener
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContactPersonManager */
    private $contactPersonManager;
    /** @var AccountManager */
    private $accountManager;
    /** @var  AccountContactPersonManager */
    private $accountContactPersonManager;
    /** @var ContactPersonHistoryManager */
    private $contactPersonHistoryManager;
    /** @var AccountRepository */
    private $accountRepository;

    /**
     * ContactPersonListener constructor.
     * @param ObjectManager $objectManager
     * @param ContactPersonManager $contactPersonManager
     * @param AccountManager $accountManager
     * @param AccountContactPersonManager $accountContactPersonManager
     * @param ContactPersonHistoryManager $contactPersonHistoryManager
     */
    public function __construct(
        ObjectManager $objectManager,
        ContactPersonManager $contactPersonManager,
        AccountManager $accountManager,
        AccountContactPersonManager $accountContactPersonManager,
        ContactPersonHistoryManager $contactPersonHistoryManager
    ) {
        $this->objectManager = $objectManager;
        $this->contactPersonManager = $contactPersonManager;
        $this->accountManager = $accountManager;
        $this->accountContactPersonManager = $accountContactPersonManager;
        $this->contactPersonHistoryManager = $contactPersonHistoryManager;

        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
    }

    /**
     * @param ContactPersonCreatedEvent $event
     */
    public function postCreate(ContactPersonCreatedEvent $event)
    {
        /** @var ContactPerson $contact */
        $contactPerson = $event->getContactPerson();
        $this->contactPersonHistoryManager->createContactPersonHistoryItem($contactPerson);
    }

    /**
     * @param ContactPersonUpdatedEvent $event
     */
    public function postUpdate(ContactPersonUpdatedEvent $event)
    {
        /** @var ContactPerson $contact */
        $contactPerson = $event->getContactPerson();
        $this->contactPersonHistoryManager->createContactPersonHistoryItem($contactPerson);
        // TODO: можна оптимізувати витягуючи лише ті акаунти контакти яких мають персонал адрес і прибрати дублювання
        /** @var Account[] $accounts */
        $accounts = $this->accountRepository->findByContactPerson($contactPerson);
        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->accountManager->checkMessageAuthorizerAddress($account);
        }
    }

    /**
     * @param ContactPersonDeletedEvent $event
     */
    public function postDelete(ContactPersonDeletedEvent $event)
    {
        /** @var ContactPerson $contact */
        $contactPerson = $event->getContactPerson();
        $this->accountContactPersonManager->deleteAndUnlinkAccountContactPersons($contactPerson);
        $this->checkAuthorizerForAccountsByContactPerson($contactPerson);
        $this->contactPersonHistoryManager->createContactPersonHistoryItem($contactPerson);
    }

    /**
     * @param ContactPersonUnsetCompanyEvent $event
     */
    public function postUnsetCompany(ContactPersonUnsetCompanyEvent $event)
    {
        /** @var ContactPerson $contact */
        $contactPerson = $event->getContactPerson();

        // TODO: переписати цей комент англійською
        // Ця строка має бути до відвязування акаунтів від компанії тому що ми не зможемо знайти жодного акаунта
        /** @var Account[] $accounts */
        $accounts = $this->accountRepository->findByContactPerson($contactPerson);
        $this->accountContactPersonManager->deleteAndUnlinkAccountContactPersons($contactPerson);
        $this->checkAuthorizerForAccountsByContactPerson($contactPerson);

        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->accountManager->checkMessageAuthorizerAddress($account);
        }
    }

    /**
     * @param ContactPerson $contactPerson
     */
    private function checkAuthorizerForAccountsByContactPerson(ContactPerson $contactPerson)
    {
        /** @var Account[] $accounts */
        $accounts = $this->accountRepository->findByContactPerson($contactPerson);
        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->accountManager->checkAuthorizer($account);
        }
    }
}
