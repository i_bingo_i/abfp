<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Events\DeviceCreatedEvent;
use AppBundle\Events\DeviceDeletedEvent;
use AppBundle\Entity\EntityManager\AccountManager;

class DeviceListener
{
    /** @var AccountManager */
    private $accountManager;

    /**
     * AccountListener constructor.
     * @param AccountManager $accountManager
     */
    public function __construct(AccountManager $accountManager)
    {
        $this->accountManager = $accountManager;
    }

    /**
     * @param DeviceCreatedEvent $event
     */
    public function postCreate(DeviceCreatedEvent $event)
    {
        /** @var Device $device */
        $device = $event->getDevice();
        /** @var Account $account */
        $account = $device->getAccount();
        $this->checkMessagesByAccount($account);
    }

    /**
     * @param DeviceDeletedEvent $event
     */
    public function postDelete(DeviceDeletedEvent $event)
    {
        /** @var Device $device */
        $device = $event->getDevice();
        /** @var Account $account */
        $account = $device->getAccount();
        $this->checkMessagesByAccount($account);
    }

    /**
     * @param Account $account
     */
    private function checkMessagesByAccount(Account $account)
    {
        $this->accountManager->checkAuthorizer($account);
        $this->accountManager->checkDefaultDepartmentChanel($account);
        $this->accountManager->checkMessageAuthorizerAddress($account);
    }
}
