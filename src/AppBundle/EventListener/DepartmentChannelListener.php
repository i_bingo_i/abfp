<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Account;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Events\DepartmentChannelCreatedEvent;
use AppBundle\Events\DepartmentChannelDeletedEvent;
use Doctrine\Common\Persistence\ObjectManager;

class DepartmentChannelListener
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AccountManager */
    private $accountManager;
    /** @var AccountRepository */
    private $accountRepository;

    /**
     * DepartmentChannelListener constructor.
     * @param ObjectManager $objectManager
     * @param AccountManager $accountManager
     */
    public function __construct(ObjectManager $objectManager, AccountManager $accountManager) {
        $this->objectManager = $objectManager;
        $this->accountManager = $accountManager;

        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
    }

    /**
     * @param DepartmentChannelCreatedEvent $event
     */
    public function postCreate(DepartmentChannelCreatedEvent $event)
    {
        /** @var DepartmentChannel $departmentChanel */
        $departmentChanel = $event->getDepartmentChannel();
        /** @var Municipality $municipality */
        $municipality = $departmentChanel->getDepartment()->getMunicipality();
        /** @var Account[] $accounts */
        $accounts = $this->accountRepository->findBy(['municipality' => $municipality]);

        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->accountManager->checkDefaultDepartmentChanel($account);
        }
    }

    /**
     * @param DepartmentChannelDeletedEvent $event
     */
    public function postDelete(DepartmentChannelDeletedEvent $event)
    {
        /** @var DepartmentChannel $departmentChanel */
        $departmentChanel = $event->getDepartmentChannel();
    }
}
