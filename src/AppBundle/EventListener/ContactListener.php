<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Events\AccountContactPersonSetRolesEvent;
use AppBundle\Events\AccountContactPersonDeletedEvent;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Services\AccountContactPersonHistory\Creator;

class ContactListener
{
    /** @var AccountManager */
    private $accountManager;
    /** @var Creator */
    private $accountContactPersonHistoryCreatorService;

    /**
     * ContactListener constructor.
     * @param AccountManager $accountManager
     * @param Creator $accountContactPersonHistoryCreatorService
     */
    public function __construct(AccountManager $accountManager, Creator $accountContactPersonHistoryCreatorService)
    {
        $this->accountManager = $accountManager;
        $this->accountContactPersonHistoryCreatorService = $accountContactPersonHistoryCreatorService;
    }

    /**
     * @param AccountContactPersonSetRolesEvent $event
     */
    public function postSetRoles(AccountContactPersonSetRolesEvent $event)
    {
        /** @var AccountContactPerson $contact */
        $contact = $event->getContact();
        /** @var Account $account */
        $account = $contact->getAccount();

        if ($account instanceof Account) {
            $this->accountManager->checkAuthorizer($account);
            $this->accountManager->checkParentAndAuthorizer($account);
            $this->accountManager->checkMessageAuthorizerAddress($account);
        }

        $this->accountContactPersonHistoryCreatorService->create($contact);
    }

    /**
     * @param AccountContactPersonDeletedEvent $event
     */
    public function postDelete(AccountContactPersonDeletedEvent $event)
    {
        /** @var AccountContactPerson $contact */
        $contact = $event->getContact();
        /** @var Account $account */
        $account = $contact->getAccount();

        if ($account instanceof Account) {
            $this->accountManager->checkAuthorizer($account);
            $this->accountManager->checkParentAndAuthorizer($account);
            $this->accountManager->checkMessageAuthorizerAddress($account);
        }

        $this->accountContactPersonHistoryCreatorService->create($contact);
    }
}
