<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Account;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\ProposalHistoryManager;
use AppBundle\Entity\EntityManager\ProposalLogManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalType;
use AppBundle\Entity\Service;
use AppBundle\Events\ProposalCloneEvent;
use AppBundle\Events\ProposalCreateEvent;
use AppBundle\Events\ProposalUpdateEvent;
use AppBundle\Services\ProposalsProcessing;

class ProposalListener
{
    /** @var ProposalManager */
    private $proposalManager;
    /** @var ProposalHistoryManager */
    private $proposalHistoryManager;
    /** @var ProposalsProcessing */
    private $proposalProcessingService;
    /** @var ProposalLogManager */
    private $proposalLogManager;
    /** @var AccountManager */
    private $accountManager;

    public function __construct(
        ProposalManager $proposalManager,
        ProposalHistoryManager $proposalHistoryManager,
        ProposalsProcessing $proposalProcessingService,
        ProposalLogManager $proposalLogManager,
        AccountManager $accountManager
    ) {
        $this->proposalManager = $proposalManager;
        $this->proposalHistoryManager = $proposalHistoryManager;
        $this->proposalProcessingService = $proposalProcessingService;
        $this->proposalLogManager = $proposalLogManager;
        $this->accountManager = $accountManager;
    }

    /**
     * @param ProposalCreateEvent $event
     */
    public function postCreate(ProposalCreateEvent $event)
    {
        /** @var Proposal $proposal */
        $proposal = $event->getProposal();
        $proposalTypeAlias = $proposal->getType()->getAlias();
        $this->proposalHistoryManager->createProposalHistoryItem($proposal);

        if ($proposal->getLifeCycle()->getAlias() == 'retest_notice_life_cycle') {
            //$this->proposalProcessingService->checkPostCreate($proposal);
            $this->proposalLogManager->create(
                $proposal,
                'Retest Notice was created'
            );

            /** @var Account $account */
            $account = $proposal->getAccount();
            $this->accountManager->refreshOldestProposalDate($account);
        } else {
            $this->proposalLogManager->create(
                $proposal,
                $proposalTypeAlias == "custom" ? "Custom Proposal was created" : "Proposal was created"
            );
        }
    }

    /**
     * @param ProposalUpdateEvent $event
     */
    public function postUpdate(ProposalUpdateEvent $event)
    {
        $proposal = $event->getProposal();
        $this->proposalHistoryManager->createProposalHistoryItem($proposal);
    }

    /**
     * @param ProposalCloneEvent $event
     */
    public function postClone(ProposalCloneEvent $event)
    {
        /** @var Proposal $clonedProposal */
        $clonedProposal = $event->getClonedProposal();
        $clonedProposalTypeAlias = $clonedProposal->getType()->getAlias();

        $userComment = $event->getUserComment();

        $this->proposalLogManager->create(
            $clonedProposal,
            $clonedProposalTypeAlias == "retest" ? "This Retest Notice was created to replace another one"
                : "This Proposal was created to replace another one",
            $userComment
        );
    }
}
