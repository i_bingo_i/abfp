<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Account;
use AppBundle\Entity\EntityManager\MessageManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Events\AccountCreatedEvent;
use AppBundle\Events\AccountUpdatedEvent;
use AppBundle\Events\AccountUpdateContactsEvent;
use AppBundle\Entity\EntityManager\AccountHistoryManager;
use AppBundle\Events\ParentAccountSetEvent;
use AppBundle\Events\ParentAccountUnsetEvent;
use AppBundle\Events\AccountDeletedEvent;
use AppBundle\Entity\EntityManager\AccountManager;
use InvoiceBundle\Manager\JobManager;

class AccountListener
{
    /** @var MessageManager */
    private $messageManager;
    /** @var AccountHistoryManager */
    private $accountHistoryManager;
    /** @var AccountManager */
    private $accountManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var JobManager */
    private $jobManager;

    /**
     * AccountListener constructor.
     *
     * @param MessageManager $messageManager
     * @param AccountHistoryManager $accountHistoryManager
     * @param AccountManager $accountManager
     * @param ServiceManager $serviceManager
     * @param JobManager $jobManager
     */
    public function __construct(
        MessageManager $messageManager,
        AccountHistoryManager $accountHistoryManager,
        AccountManager $accountManager,
        ServiceManager $serviceManager,
        JobManager $jobManager
    ) {
        $this->messageManager = $messageManager;
        $this->accountHistoryManager = $accountHistoryManager;
        $this->accountManager = $accountManager;
        $this->serviceManager = $serviceManager;
        $this->jobManager = $jobManager;
    }

    /**
     * @param AccountCreatedEvent $event
     */
    public function postCreate(AccountCreatedEvent $event)
    {
        /** @var Account $account */
        $account = $event->getAccount();
        $this->messageManager->isChildAccountHaveOwnAuthorizer($account);
        $this->accountHistoryManager->createAccountHistoryItem($account);
    }

    /**
     * @param AccountUpdatedEvent $event
     */
    public function postUpdate(AccountUpdatedEvent $event)
    {
        /** @var Account $account */
        $account = $event->getAccount();
        if ($this->accountManager->checkOnChangeMunicipality($account)) {
            $this->serviceManager->setNewDepartmentChannelForAllServicesByAccount($account);
        }
        $this->messageManager->isChildAccountHaveOwnAuthorizer($account);
        $this->accountManager->checkDefaultDepartmentChanel($account);
        $this->accountHistoryManager->createAccountHistoryItem($account);
        $this->jobManager->refreshForAccount($account);
    }

    /**
     * @param AccountDeletedEvent $event
     */
    public function postDeleted(AccountDeletedEvent $event)
    {
        /** @var Account $account */
        $account = $event->getAccount();
        $this->accountManager->checkDefaultDepartmentChanel($account);
    }

    /**
     * @param AccountUpdateContactsEvent $event
     */
    public function postUpdateContacts(AccountUpdateContactsEvent $event)
    {
        /** @var Account $account */
        $account = $event->getAccount();
        $this->messageManager->isChildAccountHaveOwnAuthorizer($account);
    }

    /**
     * @param ParentAccountSetEvent $event
     */
    public function postSetParent(ParentAccountSetEvent $event)
    {
        /** @var Account $parent */
        $parent = $event->getParent();
        /** @var Account $account */
        $account = $event->getAccount();

        $this->accountManager->checkServiceFee($parent);
        $this->accountManager->checkDefaultDepartmentChanel($account);
    }

    /**
     * @param ParentAccountUnsetEvent $event
     */
    public function postUnsetParent(ParentAccountUnsetEvent $event)
    {
        /** @var Account $parent */
        $parent = $event->getParent();
        /** @var Account $account */
        $account = $event->getAccount();

        $this->accountManager->checkServiceFee($parent);
        $this->accountManager->checkDefaultDepartmentChanel($account);
        $this->accountManager->checkAuthorizer($account);
    }
}
