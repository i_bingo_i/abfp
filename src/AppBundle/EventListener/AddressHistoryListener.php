<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\AddressHistory;
use AppBundle\Events\AddressHistoryItemCreateEvent;
use AppBundle\Services\CreateACPHistoryAfterAddressUpdateService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AddressHistoryListener
{
    /** @var ContainerInterface  */
    private $container;
    /** @var CreateACPHistoryAfterAddressUpdateService */
    private $createACPHistoryAfterAddressUpdate;




    /**
     * AddressListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->createACPHistoryAfterAddressUpdate = $this->container->get("app.create_acp_history_after_address_update.service");

    }

    /**
     * @param AddressHistoryItemCreateEvent $event
     */
    public function postCreate(AddressHistoryItemCreateEvent $event)
    {
        $addressHistory = $event->getAddressHistory();
        $this->createACPHistoryAfterAddressUpdate->checkAndFindACPForCreateHistory($addressHistory);

    }
}
