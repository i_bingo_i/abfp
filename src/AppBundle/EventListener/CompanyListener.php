<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Account;
use AppBundle\Entity\Company;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\CompanyHistoryManager;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Events\CompanyCreatedEvent;
use AppBundle\Events\CompanyDeletedEvent;
use AppBundle\Events\CompanyLinkedEvent;
use AppBundle\Events\CompanyUnlinkedEvent;
use AppBundle\Events\CompanyUpdatedEvent;
use AppBundle\Entity\Repository\AccountRepository;

use Doctrine\Common\Persistence\ObjectManager;

class CompanyListener
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var CompanyHistoryManager */
    private $companyHistoryManager;
    /** @var  ContactPersonManager */
    private $contactPersonManager;
    /** @var  AccountManager */
    private $accountManager;
    /** @var AccountRepository */
    private $accountRepository;

    /**
     * CompanyListener constructor.
     * @param ObjectManager $objectManager
     * @param CompanyHistoryManager $companyHistoryManager
     * @param ContactPersonManager $contactPersonManager
     * @param AccountManager $accountManager
     */
    public function __construct(
        ObjectManager $objectManager,
        CompanyHistoryManager $companyHistoryManager,
        ContactPersonManager $contactPersonManager,
        AccountManager $accountManager
    ) {
        $this->objectManager = $objectManager;
        $this->companyHistoryManager = $companyHistoryManager;
        $this->contactPersonManager = $contactPersonManager;
        $this->accountManager = $accountManager;

        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
    }

    /**
     * @param CompanyCreatedEvent $event
     */
    public function postCreate(CompanyCreatedEvent $event)
    {
        /** @var Company $company */
        $company = $event->getCompany();
        $this->companyHistoryManager->createCompanyHistoryItem($company);
    }

    /**
     * @param CompanyUpdatedEvent $event
     */
    public function postUpdate(CompanyUpdatedEvent $event)
    {
        /** @var Company $company */
        $company = $event->getCompany();
        $this->companyHistoryManager->createCompanyHistoryItem($company);

        // TODO: можна оптимізувати витягуючи лише ті акаунти контакти яких мають персонал адрес
        /** @var Account[] $accounts */
        $accounts = $this->accountRepository->findByContactPersonCompany($company);
        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->accountManager->checkMessageAuthorizerAddress($account);
        }
    }

    /**
     * @param CompanyDeletedEvent $event
     */
    public function postDelete(CompanyDeletedEvent $event)
    {
        /** @var Company $company */
        $company = $event->getCompany();
        $this->companyHistoryManager->createCompanyHistoryItem($company);

        // TODO: переписати цей комент англійською
        // Ця строка має бути до відвязування акаунтів від компанії тому що ми не зможемо знайти жодного акаунта
        /** @var Account[] $accounts */
        $accounts = $this->accountRepository->findByContactPersonCompany($company);
        $this->contactPersonManager->deleteAndUnsetAllContactPersonsFromCompany($company);
        $this->accountManager->unsetAllAccountsFromCompany($company);
        /** @var Account $account */
        foreach ($accounts as $account) {
            $this->accountManager->checkMessageAuthorizerAddress($account);
        }
    }

    /**
     * @param CompanyLinkedEvent $event
     */
    public function postLink(CompanyLinkedEvent $event)
    {
        /** @var Company $company */
        $company = $event->getCompany();
        $this->companyHistoryManager->createCompanyHistoryItem($company);
    }

    /**
     * @param CompanyUnlinkedEvent $event
     */
    public function postUnlink(CompanyUnlinkedEvent $event)
    {
        /** @var Company $company */
        $company = $event->getCompany();
        $this->companyHistoryManager->createCompanyHistoryItem($company);
    }
}
