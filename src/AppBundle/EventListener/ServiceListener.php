<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Events\ServiceCreatedEvent;
use AppBundle\Events\ServiceUpdatedEvent;
use AppBundle\Events\ServiceDeletedEvent;
use AppBundle\Entity\Service;
use AppBundle\Services\ServiceHistory\Creator;

class ServiceListener
{
    /** @var Creator */
    private $serviceHistoryCreator;

    /** @var AccountManager */
    private $accountManager;

    /**
     * ServiceListener constructor.
     * @param Creator $serviceHistoryCreator
     * @param AccountManager $accountManager
     */
    public function __construct(Creator $serviceHistoryCreator, AccountManager $accountManager)
    {
        $this->serviceHistoryCreator = $serviceHistoryCreator;
        $this->accountManager = $accountManager;
    }

    /**
     * @param ServiceCreatedEvent $event
     */
    public function postCreate(ServiceCreatedEvent $event)
    {
        /** @var Service $service */
        $service = $event->getService();
        $this->accountManager->checkServiceFee($service->getAccount());
        $this->serviceHistoryCreator->create($service);
    }

    /**
     * @param ServiceUpdatedEvent $event
     */
    public function postUpdate(ServiceUpdatedEvent $event)
    {
        /** @var Service $service */
        $service = $event->getService();
        $this->accountManager->checkServiceFee($service->getAccount());
        $this->serviceHistoryCreator->create($service);
    }

    /**
     * @param ServiceDeletedEvent $event
     */
    public function postDelete(ServiceDeletedEvent $event)
    {
        /** @var Service $service */
        $service = $event->getService();
        $this->accountManager->checkServiceFee($service->getAccount());
        $this->serviceHistoryCreator->create($service);
    }
}
