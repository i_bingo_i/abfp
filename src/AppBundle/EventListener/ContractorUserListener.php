<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ContractorUser;
use AppBundle\Events\ContractorUserCreatedEvent;
use AppBundle\Services\ContractorUser\HistoryCreator;

class ContractorUserListener
{
    /** @var HistoryCreator */
    private $historyCreator;

    /**
     * ContractorUserListener constructor.
     * @param HistoryCreator $historyCreator
     */
    public function __construct(HistoryCreator $historyCreator)
    {
        $this->historyCreator = $historyCreator;
    }

    /**
     * @param ContractorUserCreatedEvent $event
     */
    public function postCreate(ContractorUserCreatedEvent $event)
    {
        /** @var ContractorUser $contractorUser */
        $contractorUser = $event->getContractorUser();

        $this->historyCreator->create($contractorUser);
    }

    /**
     * @param ContractorUserCreatedEvent $event
     */
    public function postDelete(ContractorUserCreatedEvent $event)
    {
        /** @var ContractorUser $contractorUser */
        $contractorUser = $event->getContractorUser();

        $this->historyCreator->create($contractorUser);
    }
}
