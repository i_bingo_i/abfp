<?php
namespace AppBundle\DataFixtures\TEST;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Company;

class LoadCompanyForTestData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $companyQwerty1 = new Company();
        $companyQwerty1->setName("qwertyuio qwertyuio asdfghjkl zxcvbnm,./ 1234567890 12345678 13fifehwi fewbfb efswiewhi dskfbik sdfh");
        $companyQwerty1->setAddressLine1("qwertyuio qwertyuio asdfghjkl zxcvbnm,./ 1234567890 12345678 13fifehwi fewbfb efswiewhi dskfbik sdfh");
        $companyQwerty1->setCity("ferjncfhjverojfo veriv erov iuverjvoerjv eriov voe");
        $companyQwerty1->setZip("13513512351253512381236156312537125372366327272522");
        $companyQwerty1->setWebsite("https://www.cewdrcewjnouchewcojewjewjciocewcioewcioewcioewcioewioewewcioewciecwcewcewcewcewep.com.ua");
        $companyQwerty1->setNotes("");
        $companyQwerty1->setState($this->getReference('state-alaska'));

        $manager->persist($companyQwerty1);
        $manager->flush();
        $this->addReference('company-qwerty11', $companyQwerty1);


        $companyFewfhewiuh = new Company();
        $companyFewfhewiuh->setName("fewfhewiuhfehwfhewfewiufewiufheiuwfhwifeiuwfewhofeiuwhfhwiufheiuwfhowfheiuwfhewofhewiufewfeueuueuheu");
        $companyFewfhewiuh->setAddressLine1("vfbndfbniuvheiruhvhnrivhbnvdfnjknbjodfbodfjbdfbdfbdfbdfbdfbfdbdfbdbdfbdfbdfjibdfjbdbidibifififififif");
        $companyFewfhewiuh->setCity("deeideieieieuiehjrfhjiuehiughierhihigrvhurhruurirr");
        $companyFewfhewiuh->setZip("1910191019");
        $companyFewfhewiuh->setWebsite("https://www.cewdrcewjnouchewcojewjewjciocewcioewcioewcioewcioewioewewcioewciecwcewcewcewcewep.com.ua");
        $companyFewfhewiuh->setNotes("dfvjkuiuvdfhiuvhdfvhiudfhjuvdfuhviudfvdfviudf iudfviu dfhiuvdfhvhfhdfhivdfhivhdfiviudfhviudfhuvhfhiudfhvidfhivhdfibhnvidefhbvfhj iud fbvd vbjdfjboujdfbjdfjbdfbhjdfjbdfhjbiodvfdj iofjdjgviodfrjgiovjdfvgjdfbviodfbiovdfifiofififfifiodfiodfdfodfdfiodiofdiofffkkkmfb");
        $companyFewfhewiuh->setState($this->getReference('state-alabama'));

        $manager->persist($companyFewfhewiuh);
        $manager->flush();
        $this->addReference('company-fewfhewiuh', $companyFewfhewiuh);
    }

    public function getOrder()
    {
        return 5;
    }
}
