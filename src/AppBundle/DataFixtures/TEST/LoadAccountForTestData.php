<?php
namespace AppBundle\DataFixtures\TEST;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Account;

class LoadAccountForTestData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $accountLongName = new Account();
        $accountLongName->setName("defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ");
        $accountLongName->setAddress("qwertyuio qwertyuio asdfghjkl zxcvbnm,./ 1234567890 12345678 13fifehwi fewbfb efswiewhi dskfbik sdfh");
        $accountLongName->setCity('qwertyuio qwertyuio qwertyuio qwertyuio 1234567890');
        $accountLongName->setZip('234234');
        $accountLongName->setNotes('qwertyuio qwertyuio asdfghjkl zxcvbnm,./ 1234567890 12345678 13fifehwi fewbfb efswiewhi dskfbik sdfh						');
        $accountLongName->setType($this->getReference('account-type-account'));
        $accountLongName->setState($this->getReference('state-alaska'));
        $accountLongName->setClientType($this->getReference('client-type-potential'));

        $manager->persist($accountLongName);
        $manager->flush();
        $this->addReference('account-long-name', $accountLongName);
    }

    public function getOrder()
    {
        return 4;
    }
}
