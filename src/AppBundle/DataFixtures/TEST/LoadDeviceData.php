<?php

namespace AppBundle\DataFixtures\TEST;

use AppBundle\Entity\Device;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDeviceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        $device1 = new Device();
//        $device1->setNamed($this->getReference('wet-system-device'));
//        $device1->setStatus($this->getReference('device-status-active'));
//        $device1->setAccount($this->getReference('account-one'));
//        $manager->persist($device1);
//        $manager->flush();
//        $this->addReference('device-one', $device1);
//
//
//        $device2 = new Device();
//        $device2->setNamed($this->getReference('wet-system-device'));
//        $device2->setStatus($this->getReference('device-status-active'));
//        $device2->setAccount($this->getReference('account-two'));
//        $manager->persist($device2);
//        $manager->flush();
//        $this->addReference('device-two', $device2);
    }

    public function getOrder()
    {
        return 16;
    }
}
