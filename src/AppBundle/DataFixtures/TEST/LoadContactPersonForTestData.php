<?php
namespace AppBundle\DataFixtures\TEST;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContactPerson;

class LoadContactPersonForTestData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contactPersonTest6 = new ContactPerson();
        $contactPersonTest6->setFirstName("qwertyuio qwertyuio asdfghjkl zxcvbnm,./ 1234567890 12345678 13fifehwi fewbfb efswiewhi dskfbik sdfh");
        $contactPersonTest6->setLastName("qwertyuio qwertyuio asdfghjkl zxcvbnm,./ 1234567890 12345678 13fifehwi fewbfb efswiewhi dskfbik sdfh");
        $contactPersonTest6->setEmail("contactperson6@gmail.com");
        $contactPersonTest6->setCod(false);
        $contactPersonTest6->setTitle("qwertywes qwertyuio asdfghjkl zxcvbnm,./ 1234567890 12345678 13fifehwi fewbfb efswiewhi dskfbik sdfh");
        $contactPersonTest6->setCell("4562347890");
        $contactPersonTest6->setPhone("3432140345");
        $contactPersonTest6->setExt("123");
        $contactPersonTest6->setFax("5563455677");
        $contactPersonTest6->setNotes("dfvjkuiuvdfhiuvhdfvhiudfhjuvdfuhviudfvdfviudf iudfviu dfhiuvdfhvhfhdfhivdfhivhdfiviudfhviudfhuvhfhiudfhvidfhivhdfibhnvidefhbvfhj iud fbvd vbjdfjboujdfbjdfjbdfbhjdfjbdfhjbiodvfdj iofjdjgviodfrjgiovjdfvgjdfbviodfbiovdfifiofififfifiodfiodfdfodfdfiodiofdiofffkkkmfb");

        $manager->persist($contactPersonTest6);
        $manager->flush();
        $this->addReference('contact-person-test-six', $contactPersonTest6);


        $contactPersonTest7 = new ContactPerson();
        $contactPersonTest7->setFirstName("fewfhewiuhfehwfhewfewiufewiufheiuwfhwifeiuwfewhofeiuwhfhwiufheiuwfhowfheiuwfhewofhewiufewfeueuueuheu");
        $contactPersonTest7->setLastName("fewfhewiuhfehwfhewfewiufewiufheiuwfhwifeiuwfewhofeiuwhfhwiufheiuwfhowfheiuwfhewofhewiufewfeueuueuheu");
        $contactPersonTest7->setEmail("contactperson7@gmail.com");
        $contactPersonTest7->setCod(true);
        $contactPersonTest7->setTitle("fewfhewiuhfehwfhewfewiufewiufheiuwfhwifeiuwfewhofeiuwhfhwiufheiuwfhowfheiuwfhewofhewiufewfeueuueuheu");
        $contactPersonTest7->setCell("5795323456");
        $contactPersonTest7->setPhone("4456784567");
        $contactPersonTest7->setExt("437");
        $contactPersonTest7->setFax("4545173245");
        $contactPersonTest7->setNotes("dfvjkuiuvdfhiuvhdfvhiudfhjuvdfuhviudfvdfviudf iudfviu dfhiuvdfhvhfhdfhivdfhivhdfiviudfhviudfhuvhfhiudfhvidfhivhdfibhnvidefhbvfhj iud fbvd vbjdfjboujdfbjdfjbdfbhjdfjbdfhjbiodvfdj iofjdjgviodf iiiii iovjdfvgjdfbviodfbiovdfifiofififfifiodfiodfdfodfdfiodiofdiofffkkkmfb");

        $manager->persist($contactPersonTest7);
        $manager->flush();
        $this->addReference('contact-person-test-seven', $contactPersonTest7);
    }

    public function getOrder()
    {
        return 6;
    }
}
