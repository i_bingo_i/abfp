<?php

namespace AppBundle\DataFixtures\TEST;

use AppBundle\Entity\DynamicFieldValue;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDynamicFieldValueData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Dynamic Field Size for Wet System
//        $dynamicFieldSizeForWetSystemDeviceOne = new DynamicFieldValue();
//        $dynamicFieldSizeForWetSystemDeviceOne->setField($this->getReference('size-field-for-wet-system'));
//        $dynamicFieldSizeForWetSystemDeviceOne->setOptionValue($this->getReference('size-field-for-wet-system-choice-3'));
//        $dynamicFieldSizeForWetSystemDeviceOne->setDevice($this->getReference('device-one'));
//
//        $manager->persist($dynamicFieldSizeForWetSystemDeviceOne);
//        $manager->flush();
//
//        $this->addReference('dynamic-field-size-for-wet-system-device-one', $dynamicFieldSizeForWetSystemDeviceOne);
//
//        //Dynamic Field Number of Risers/Sections for Wet System
//        $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceOne = new DynamicFieldValue();
//        $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceOne->setField($this->getReference('number-of-risers-sections-for-wet-system'));
//        $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceOne->setDevice($this->getReference('device-one'));
//        $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceOne->setValue('12');
//        $manager->persist($dynamicFieldNumberOfRisersSectionsForWetSystemDeviceOne);
//        $manager->flush();
//
//        $this->addReference('dynamic-field-number-of-risers-sections-for-wet-system-system-device-one', $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceOne);
//
//        //Dynamic Field Water Supply Source for Wet System
//        $dynamicFieldWaterSupplySourceForWetSystemDeviceOne = new DynamicFieldValue();
//        $dynamicFieldWaterSupplySourceForWetSystemDeviceOne->setField($this->getReference('water-supply-source-for-wet-system'));
//        $dynamicFieldWaterSupplySourceForWetSystemDeviceOne->setOptionValue($this->getReference('water-supply-source-field-for-wet-system-choice-2'));
//        $dynamicFieldWaterSupplySourceForWetSystemDeviceOne->setDevice($this->getReference('device-one'));
//
//        $manager->persist($dynamicFieldWaterSupplySourceForWetSystemDeviceOne);
//        $manager->flush();
//
//        $this->addReference('dynamic-field-water-supply-source-for-wet-system-device-one', $dynamicFieldWaterSupplySourceForWetSystemDeviceOne);
//        /****************************************************************/
//
//        //Dynamic Field Size for Wet System
//        $dynamicFieldSizeForWetSystemDeviceTwo = new DynamicFieldValue();
//        $dynamicFieldSizeForWetSystemDeviceTwo->setField($this->getReference('size-field-for-wet-system'));
//        $dynamicFieldSizeForWetSystemDeviceTwo->setOptionValue($this->getReference('size-field-for-wet-system-choice-4'));
//        $dynamicFieldSizeForWetSystemDeviceTwo->setDevice($this->getReference('device-two'));
//
//        $manager->persist($dynamicFieldSizeForWetSystemDeviceTwo);
//        $manager->flush();
//
//        $this->addReference('dynamic-field-size-for-wet-systemdevice-two', $dynamicFieldSizeForWetSystemDeviceTwo);
//
//        //Dynamic Field Number of Risers/Sections for Wet System
//        $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceTwo = new DynamicFieldValue();
//        $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceTwo->setField($this->getReference('number-of-risers-sections-for-wet-system'));
//        $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceTwo->setDevice($this->getReference('device-two'));
//        $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceTwo->setValue('5');
//        $manager->persist($dynamicFieldNumberOfRisersSectionsForWetSystemDeviceTwo);
//        $manager->flush();
//
//        $this->addReference('dynamic-field-number-of-risers-sections-for-wet-systemdevice-two', $dynamicFieldNumberOfRisersSectionsForWetSystemDeviceTwo);
//
//        //Dynamic Field Water Supply Source for Wet System
//        $dynamicFieldWaterSupplySourceForWetSystemDeviceTwo = new DynamicFieldValue();
//        $dynamicFieldWaterSupplySourceForWetSystemDeviceTwo->setField($this->getReference('water-supply-source-for-wet-system'));
//        $dynamicFieldWaterSupplySourceForWetSystemDeviceTwo->setOptionValue($this->getReference('water-supply-source-field-for-wet-system-choice-1'));
//        $dynamicFieldWaterSupplySourceForWetSystemDeviceTwo->setDevice($this->getReference('device-two'));
//
//        $manager->persist($dynamicFieldWaterSupplySourceForWetSystemDeviceTwo);
//        $manager->flush();
//
//        $this->addReference('dynamic-field-water-supply-source-for-wet-system-device-two', $dynamicFieldWaterSupplySourceForWetSystemDeviceTwo);
//        /****************************************************************/

    }

    public function getOrder()
    {
        return 18;
    }

}
