<?php
namespace AppBundle\DataFixtures\TEST;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Address;

class LoadAddressesForTestData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $addressBillingForCP6 = new Address();
        $addressBillingForCP6->setAddress("qwertyuio qwertyuio asdfghjkl zxcvbnm,./ 1234567890 12345678 13fifehwi fewbfb efswiewhi dskfbik sdfh");
        $addressBillingForCP6->setCity("ferjncfhjverojfo veriv erov iuverjvoerjv eriov voe");
        $addressBillingForCP6->setZip("13513512351253512381236156312537125372366327272522");
        $addressBillingForCP6->setState($this->getReference('state-alaska'));
        $addressBillingForCP6->setAddressType($this->getReference('address-type-billing'));
        $addressBillingForCP6->setContactPerson($this->getReference('contact-person-test-six'));
        $manager->persist($addressBillingForCP6);
        $manager->flush();
        $this->addReference('address-billing-for-CP6', $addressBillingForCP6);

        $addressMailingForCP6Empty = new Address();
        $addressMailingForCP6Empty->setState($this->getReference('state-alaska'));
        $addressMailingForCP6Empty->setAddressType($this->getReference('address-type-mailing'));
        $addressMailingForCP6Empty->setContactPerson($this->getReference('contact-person-test-six'));
        $manager->persist($addressMailingForCP6Empty);
        $manager->flush();
        $this->addReference('address-mailing-for-cp6-empty', $addressMailingForCP6Empty);

        /******************************************************************/

        $addressBillingForCP7 = new Address();
        $addressBillingForCP7->setAddress("vfbndfbniuvheiruhvhnrivhbnvdfnjknbjodfbodfjbdfbdfbdfbdfbdfbfdbdfbdbdfbdfbdfjibdfjbdbidibifififififif");
        $addressBillingForCP7->setCity("deeideieieieuiehjrfhjiuehiughierhihigrvhurhruurirr");
        $addressBillingForCP7->setZip("1910191019");
        $addressBillingForCP7->setState($this->getReference('state-alaska'));
        $addressBillingForCP7->setAddressType($this->getReference('address-type-billing'));
        $addressBillingForCP7->setContactPerson($this->getReference('contact-person-test-seven'));
        $manager->persist($addressBillingForCP7);
        $manager->flush();
        $this->addReference('address-billing-for-CP7', $addressBillingForCP7);

        $addressMailingForCP7Empty = new Address();
        $addressMailingForCP7Empty->setState($this->getReference('state-alaska'));
        $addressMailingForCP7Empty->setAddressType($this->getReference('address-type-mailing'));
        $addressMailingForCP7Empty->setContactPerson($this->getReference('contact-person-test-seven'));
        $manager->persist($addressMailingForCP7Empty);
        $manager->flush();
        $this->addReference('address-mailing-for-cp7-empty', $addressMailingForCP7Empty);
    }

    public function getOrder()
    {
        return 7;
    }
}
