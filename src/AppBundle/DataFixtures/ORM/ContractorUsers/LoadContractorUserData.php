<?php

namespace AppBundle\DataFixtures\ORM\ContractorUsers;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorUser;

class LoadContractorUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Contractors from IDAP Group
        //Maksim Schur

        $contractorUserOne = new ContractorUser();
        $contractorUserOne->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserOne->setUser($this->getReference('schur-maksim-super-admin'));
        $manager->persist($contractorUserOne);
        $manager->flush();

        $this->addReference('contractor-user-maksim-schur', $contractorUserOne);

        //Oleksandr Korchak
        $contractorUserTwo = new ContractorUser();
        $contractorUserTwo->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserTwo->setUser($this->getReference('oleksandr-korchak-super-admin'));
        $manager->persist($contractorUserTwo);
        $manager->flush();

        $this->addReference('contractor-user-oleksandr-korchak', $contractorUserTwo);

        //Pavlo Pashchevskyi
        $contractorUserThree = new ContractorUser();
        $contractorUserThree->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserThree->setUser($this->getReference('pavlo-pashchevskyi-super-admin'));
        $manager->persist($contractorUserThree);
        $manager->flush();

        $this->addReference('contractor-user-pavlo-pashchevskyi', $contractorUserThree);

        //Igor Yaitskikh
        $contractorUserFour = new ContractorUser();
        $contractorUserFour->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserFour->setUser($this->getReference('igor-yaitskikh-super-admin'));
        $manager->persist($contractorUserFour);
        $manager->flush();

        $this->addReference('contractor-user-igor-yaitskikh', $contractorUserFour);

        //Daria Novikova
        $contractorUserFive = new ContractorUser();
        $contractorUserFive->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserFive->setUser($this->getReference('daria-novikova-super-admin'));
        $manager->persist($contractorUserFive);
        $manager->flush();

        $this->addReference('contractor-user-daria-novikova', $contractorUserFive);

        //Mykola Shevchuk
        $contractorUserSix = new ContractorUser();
        $contractorUserSix->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserSix->setUser($this->getReference('mykola-shevchuk-super-admin'));
        $manager->persist($contractorUserSix);
        $manager->flush();

        $this->addReference('contractor-user-mykola-shevchuk', $contractorUserSix);

        //Taras Turbovets
        $contractorUserSeven = new ContractorUser();
        $contractorUserSeven->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserSeven->setUser($this->getReference('taras-turbovets-super-admin'));
        $manager->persist($contractorUserSeven);
        $manager->flush();

        $this->addReference('contractor-user-taras-turbovets', $contractorUserSeven);

        //Aleksandr Babich
        $contractorUserEight = new ContractorUser();
        $contractorUserEight->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserEight->setUser($this->getReference('babich-alex-super-admin'));
        $manager->persist($contractorUserEight);
        $manager->flush();

        $this->addReference('contractor-user-aleksandr-babich', $contractorUserEight);

        //Svetlana Kovalchuk
        $contractorUserSvetlanaKovalchuk = new ContractorUser();
        $contractorUserSvetlanaKovalchuk->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserSvetlanaKovalchuk->setUser($this->getReference('svetlana-kovalchuk-super-admin'));
        $manager->persist($contractorUserSvetlanaKovalchuk);
        $manager->flush();

        $this->addReference('contractor-user-svetlana-kovalchuk', $contractorUserSvetlanaKovalchuk);

        //Yurii Trokhymchuk
        $contractorUserYuriiTrokhymchuk = new ContractorUser();
        $contractorUserYuriiTrokhymchuk->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserYuriiTrokhymchuk->setUser($this->getReference('yurii-trokhymchuk-super-admin'));
        $manager->persist($contractorUserYuriiTrokhymchuk);
        $manager->flush();

        $this->addReference('contractor-user-yurii-trokhymchuk', $contractorUserYuriiTrokhymchuk);


        //Vlad Emets
        $contractorUserVladEmets = new ContractorUser();
        $contractorUserVladEmets->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserVladEmets->setUser($this->getReference('vlad-emets-super-admin'));
        $manager->persist($contractorUserVladEmets);
        $manager->flush();

        $this->addReference('contractor-user-vlad-emets', $contractorUserVladEmets);

        //Dmytro Vdovychenko
        $contractorUserDmytroVdovychenko = new ContractorUser();
        $contractorUserDmytroVdovychenko->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserDmytroVdovychenko->setUser($this->getReference('vdovychenko-dmytro-super-admin'));
        $manager->persist($contractorUserDmytroVdovychenko);
        $manager->flush();

        $this->addReference('contractor-user-dmytro-vdovychenko', $contractorUserDmytroVdovychenko);


        //Maksym Makartsov
        $contractorUserMaksymMakartsov = new ContractorUser();
        $contractorUserMaksymMakartsov->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserMaksymMakartsov->setUser($this->getReference('maksym-makartsov-super-admin'));
        $manager->persist($contractorUserMaksymMakartsov);
        $manager->flush();

        $this->addReference('contractor-user-maksym-makartsov', $contractorUserMaksymMakartsov);


        //Contractors from A.B.F.P.
        //Dan Harbut
        $contractorUserTen = new ContractorUser();
        $contractorUserTen->setContractor($this->getReference('contractor-my-company'));
        $contractorUserTen->setUser($this->getReference('dan-harbut-super-admin'));
        $contractorUserTen->setTitle('CEO (Superadmin)');
        $manager->persist($contractorUserTen);
        $manager->flush();

        $this->addReference('contractor-user-dan-harbut', $contractorUserTen);


        //Super Admin
        $contractorUserNine = new ContractorUser();
        $contractorUserNine->setContractor($this->getReference('contractor-my-company'));
        $contractorUserNine->setUser($this->getReference('super-admin-super-admin'));
        $contractorUserNine->setTitle('Super Admin');
        $manager->persist($contractorUserNine);
        $manager->flush();

        $this->addReference('contractor-user-super-admin', $contractorUserNine);


        $contractorUserElonMask = new ContractorUser();
        $contractorUserElonMask->setContractor($this->getReference('contractor-my-company'));
        $contractorUserElonMask->setUser($this->getReference('elon-musk-super-admin'));
        $contractorUserElonMask->setTitle('Super Admin');
        $manager->persist($contractorUserElonMask);
        $manager->flush();

        $this->addReference('contractor-user-elon-mask', $contractorUserElonMask);


        $contractorUserBillGates = new ContractorUser();
        $contractorUserBillGates->setContractor($this->getReference('contractor-my-company'));
        $contractorUserBillGates->setUser($this->getReference('bill-gates-super-admin'));
        $contractorUserBillGates->setTitle('Big Boss');
        $manager->persist($contractorUserBillGates);
        $manager->flush();

        $this->addReference('contractor-user-bill-gates', $contractorUserBillGates);



        $contractorUserSergeyBrin = new ContractorUser();
        $contractorUserSergeyBrin->setContractor($this->getReference('contractor-my-company'));
        $contractorUserSergeyBrin->setUser($this->getReference('sergey-brin-super-admin'));
        $contractorUserSergeyBrin->setTitle('Super Super Admin');
        $manager->persist($contractorUserSergeyBrin);
        $manager->flush();

        $this->addReference('contractor-user-sergey-brin', $contractorUserSergeyBrin);



        $contractorUserMarkZuckerberg = new ContractorUser();
        $contractorUserMarkZuckerberg->setContractor($this->getReference('contractor-my-company'));
        $contractorUserMarkZuckerberg->setUser($this->getReference('mark-zuckerberg-super-admin'));
        $contractorUserMarkZuckerberg->setTitle('Super Admin');
        $manager->persist($contractorUserMarkZuckerberg);
        $manager->flush();

        $this->addReference('contractor-user-mark-zuckerberg', $contractorUserMarkZuckerberg);


        $contractorUserGabeNewell = new ContractorUser();
        $contractorUserGabeNewell->setContractor($this->getReference('contractor-my-company'));
        $contractorUserGabeNewell->setUser($this->getReference('gabe-newell-super-admin'));
        $contractorUserGabeNewell->setTitle('Tester');
        $manager->persist($contractorUserGabeNewell);
        $manager->flush();

        $this->addReference('contractor-user-gabe-newell', $contractorUserGabeNewell);


        $contractorUserJakFresko = new ContractorUser();
        $contractorUserJakFresko->setContractor($this->getReference('contractor-my-company'));
        $contractorUserJakFresko->setUser($this->getReference('jak-fresko-super-admin'));
        $contractorUserJakFresko->setTitle('Super Admin');
        $manager->persist($contractorUserJakFresko);
        $manager->flush();

        $this->addReference('contractor-user-jak-fresko', $contractorUserJakFresko);


        //Contractors of Monarch
        //John Doe of Monarch
        $contractorUserEleven = new ContractorUser();
        $contractorUserEleven->setContractor($this->getReference('contractor-partner-with-coi-expiration'));
        $contractorUserEleven->setUser($this->getReference('john-doe-admin'));
        $contractorUserEleven->setTitle('Senior Manager');
        $contractorUserEleven->setPersonalPhone('(111)222-2222');
        $contractorUserEleven->setWorkPhone('(111)222-2222');
        $contractorUserEleven->setFax('(111)222-2222');
        $contractorUserEleven->setAddress($this->getReference('address-fifteen'));
        $contractorUserEleven->setPersonalEmail('john_doe@email.com');
        $manager->persist($contractorUserEleven);
        $manager->flush();

        $this->addReference('contractor-user-john-doe-for-monarch', $contractorUserEleven);

        //John Bishop
        $contractorUserTwelve = new ContractorUser();
        $contractorUserTwelve->setContractor($this->getReference('contractor-partner-with-coi-expiration'));
        $contractorUserTwelve->setUser($this->getReference('john-bishop-admin'));
        $contractorUserTwelve->setTitle('Manager');
        $contractorUserTwelve->setPersonalPhone('(111)222-2222');
        $contractorUserTwelve->setWorkPhone('(111)222-2222');
        $contractorUserTwelve->setFax('(111)222-2222');
        $contractorUserTwelve->setAddress($this->getReference('address-fifteen-2'));
        $contractorUserTwelve->setPersonalEmail('john_bishop@email.com');
        $manager->persist($contractorUserTwelve);
        $manager->flush();

        $this->addReference('contractor-user-john-bishop-for-monarch', $contractorUserTwelve);

        //James Hatfield
        $contractorUserThirteen = new ContractorUser();
        $contractorUserThirteen->setContractor($this->getReference('contractor-partner-with-coi-expiration'));
        $contractorUserThirteen->setUser($this->getReference('james-hatfield-admin'));
        $contractorUserThirteen->setTitle('Tester');
        $contractorUserThirteen->setPersonalPhone('(111)222-2222');
        $contractorUserThirteen->setWorkPhone('(111)222-2222');
        $contractorUserThirteen->setFax('(111)222-2222');
        $contractorUserThirteen->setAddress($this->getReference('address-fifteen-3'));
        $contractorUserThirteen->setPersonalEmail('james_hatfield@email.com');
        $contractorUserThirteen->setSignature('1500386316.png');
        $contractorUserThirteen->setPhoto('back-flow-testing.jpg');
        $manager->persist($contractorUserThirteen);
        $manager->flush();

        $this->addReference('contractor-user-james-hatfield-for-monarch', $contractorUserThirteen);

        //Kirk Hammet
        $contractorUserFourteen = new ContractorUser();
        $contractorUserFourteen->setContractor($this->getReference('contractor-partner-with-coi-expiration'));
        $contractorUserFourteen->setUser($this->getReference('kirk-hammet-admin'));
        $contractorUserFourteen->setTitle('Tester');
        $contractorUserFourteen->setPersonalPhone('(111)222-2222');
        $contractorUserFourteen->setWorkPhone('(111)222-2222');
        $contractorUserFourteen->setFax('(111)222-2222');
        $contractorUserFourteen->setAddress($this->getReference('address-fifteen-4'));
        $contractorUserFourteen->setPersonalEmail('kirk_hammet@email.com');
        $contractorUserFourteen->setSignature('1500386336.png');
        $contractorUserFourteen->setPhoto('thmb_tester3.jpg');
        $manager->persist($contractorUserFourteen);
        $manager->flush();

        $this->addReference('contractor-user-kirk-hammet-for-monarch', $contractorUserFourteen);
    }

    public function getOrder()
    {
        return 9;
    }
}
