<?php

namespace AppBundle\DataFixtures\ORM\ContractorUsers;

use AppBundle\Entity\ContractorUserHistory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadContractorUserHistoryData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        //Contractors from IDAP Group
        //Maksim Schur

        $contractorUserOne = new ContractorUserHistory();
        $contractorUserOne->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserOne->setUser($this->getReference('schur-maksim-super-admin'));
        $contractorUserOne->setOwnerEntity($this->getReference('contractor-user-maksim-schur'));
        $contractorUserOne->setDateSave(new \DateTime());
        $manager->persist($contractorUserOne);
        $manager->flush();

        $this->addReference('contractor-user-maksim-schur-history', $contractorUserOne);

        //Oleksandr Korchak
        $contractorUserTwo = new ContractorUserHistory();
        $contractorUserTwo->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserTwo->setUser($this->getReference('oleksandr-korchak-super-admin'));
        $contractorUserTwo->setOwnerEntity($this->getReference('contractor-user-oleksandr-korchak'));
        $contractorUserTwo->setDateSave(new \DateTime());
        $manager->persist($contractorUserTwo);
        $manager->flush();

        $this->addReference('contractor-user-oleksandr-korchak-history', $contractorUserTwo);

        //Pavlo Pashchevskyi
        $contractorUserThree = new ContractorUserHistory();
        $contractorUserThree->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserThree->setUser($this->getReference('pavlo-pashchevskyi-super-admin'));
        $contractorUserThree->setOwnerEntity($this->getReference('contractor-user-pavlo-pashchevskyi'));
        $contractorUserThree->setDateSave(new \DateTime());

        $manager->persist($contractorUserThree);
        $manager->flush();

        $this->addReference('contractor-user-pavlo-pashchevskyi-history', $contractorUserThree);

        //Igor Yaitskikh
        $contractorUserFour = new ContractorUserHistory();
        $contractorUserFour->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserFour->setUser($this->getReference('igor-yaitskikh-super-admin'));
        $contractorUserFour->setOwnerEntity($this->getReference('contractor-user-igor-yaitskikh'));
        $contractorUserFour->setDateSave(new \DateTime());

        $manager->persist($contractorUserFour);
        $manager->flush();

        $this->addReference('contractor-user-igor-yaitskikh-history', $contractorUserFour);

        //Daria Novikova
        $contractorUserFive = new ContractorUserHistory();
        $contractorUserFive->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserFive->setUser($this->getReference('daria-novikova-super-admin'));
        $contractorUserFive->setOwnerEntity($this->getReference('contractor-user-daria-novikova'));
        $contractorUserFive->setDateSave(new \DateTime());

        $manager->persist($contractorUserFive);
        $manager->flush();

        $this->addReference('contractor-user-daria-novikova-history', $contractorUserFive);

        //Mykola Shevchuk
        $contractorUserSix = new ContractorUserHistory();
        $contractorUserSix->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserSix->setUser($this->getReference('mykola-shevchuk-super-admin'));
        $contractorUserSix->setOwnerEntity($this->getReference('contractor-user-mykola-shevchuk'));
        $contractorUserSix->setDateSave(new \DateTime());

        $manager->persist($contractorUserSix);
        $manager->flush();

        $this->addReference('contractor-user-mykola-shevchuk-history', $contractorUserSix);

        //Taras Turbovets
        $contractorUserSeven = new ContractorUserHistory();
        $contractorUserSeven->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserSeven->setUser($this->getReference('taras-turbovets-super-admin'));
        $contractorUserSeven->setOwnerEntity($this->getReference('contractor-user-taras-turbovets'));
        $contractorUserSeven->setDateSave(new \DateTime());

        $manager->persist($contractorUserSeven);
        $manager->flush();

        $this->addReference('contractor-user-taras-turbovets-history', $contractorUserSeven);

        //Aleksandr Babich
        $contractorUserEight = new ContractorUserHistory();
        $contractorUserEight->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserEight->setUser($this->getReference('babich-alex-super-admin'));
        $contractorUserEight->setOwnerEntity($this->getReference('contractor-user-aleksandr-babich'));
        $contractorUserEight->setDateSave(new \DateTime());

        $manager->persist($contractorUserEight);
        $manager->flush();

        $this->addReference('contractor-user-aleksandr-babich-history', $contractorUserEight);

        //Svetlana Kovalchuk
        $contractorUserSvetlanaKovalchuk = new ContractorUserHistory();
        $contractorUserSvetlanaKovalchuk->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserSvetlanaKovalchuk->setUser($this->getReference('svetlana-kovalchuk-super-admin'));
        $contractorUserSvetlanaKovalchuk->setOwnerEntity($this->getReference('contractor-user-svetlana-kovalchuk'));
        $contractorUserSvetlanaKovalchuk->setDateSave(new \DateTime());

        $manager->persist($contractorUserSvetlanaKovalchuk);
        $manager->flush();

        $this->addReference('contractor-user-svetlana-kovalchuk-history', $contractorUserSvetlanaKovalchuk);

        //Yurii Trokhymchuk
        $contractorUserYuriiTrokhymchuk = new ContractorUserHistory();
        $contractorUserYuriiTrokhymchuk->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserYuriiTrokhymchuk->setUser($this->getReference('yurii-trokhymchuk-super-admin'));
        $contractorUserYuriiTrokhymchuk->setOwnerEntity($this->getReference('contractor-user-yurii-trokhymchuk'));
        $contractorUserYuriiTrokhymchuk->setDateSave(new \DateTime());

        $manager->persist($contractorUserYuriiTrokhymchuk);
        $manager->flush();

        $this->addReference('contractor-user-yurii-trokhymchuk-history', $contractorUserYuriiTrokhymchuk);


        //Vlad Emets
        $contractorUserVladEmets = new ContractorUserHistory();
        $contractorUserVladEmets->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserVladEmets->setUser($this->getReference('vlad-emets-super-admin'));
        $contractorUserVladEmets->setOwnerEntity($this->getReference('contractor-user-vlad-emets'));
        $contractorUserVladEmets->setDateSave(new \DateTime());

        $manager->persist($contractorUserVladEmets);
        $manager->flush();

        $this->addReference('contractor-user-vlad-emets-history', $contractorUserVladEmets);


        //Dmytro Vdovychenko
        $contractorUserDmytroVdovychenko = new ContractorUserHistory();
        $contractorUserDmytroVdovychenko->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserDmytroVdovychenko->setUser($this->getReference('vdovychenko-dmytro-super-admin'));
        $contractorUserDmytroVdovychenko->setOwnerEntity($this->getReference('contractor-user-dmytro-vdovychenko'));
        $contractorUserDmytroVdovychenko->setDateSave(new \DateTime());
        $manager->persist($contractorUserDmytroVdovychenko);
        $manager->flush();

        $this->addReference('contractor-user-dmytro-vdovychenko-history', $contractorUserDmytroVdovychenko);


        //Maksym Makartsov
        $contractorUserMaksymMakartsov = new ContractorUserHistory();
        $contractorUserMaksymMakartsov->setContractor($this->getReference('contractor-partner-idap-group'));
        $contractorUserMaksymMakartsov->setUser($this->getReference('maksym-makartsov-super-admin'));
        $contractorUserMaksymMakartsov->setOwnerEntity($this->getReference('contractor-user-maksym-makartsov'));
        $contractorUserMaksymMakartsov->setDateSave(new \DateTime());
        $manager->persist($contractorUserMaksymMakartsov);
        $manager->flush();

        $this->addReference('contractor-user-maksym-makartsov-history', $contractorUserMaksymMakartsov);


        //Contractors from A.B.F.P.
        //Dan Harbut
        $contractorUserTen = new ContractorUserHistory();
        $contractorUserTen->setContractor($this->getReference('contractor-my-company'));
        $contractorUserTen->setUser($this->getReference('dan-harbut-super-admin'));
        $contractorUserTen->setTitle('CEO (Superadmin)');
        $contractorUserTen->setOwnerEntity($this->getReference('contractor-user-dan-harbut'));
        $contractorUserTen->setDateSave(new \DateTime());

        $manager->persist($contractorUserTen);
        $manager->flush();

        $this->addReference('contractor-user-dan-harbut-history', $contractorUserTen);


        //Super Admin
        $contractorUserNine = new ContractorUserHistory();
        $contractorUserNine->setContractor($this->getReference('contractor-my-company'));
        $contractorUserNine->setUser($this->getReference('super-admin-super-admin'));
        $contractorUserNine->setTitle('Super Admin');
        $contractorUserNine->setOwnerEntity($this->getReference('contractor-user-super-admin'));
        $contractorUserNine->setDateSave(new \DateTime());

        $manager->persist($contractorUserNine);
        $manager->flush();

        $this->addReference('contractor-user-super-admin-history', $contractorUserNine);


        //Contractors of Monarch
        //John Doe of Monarch
        $contractorUserEleven = new ContractorUserHistory();
        $contractorUserEleven->setContractor($this->getReference('contractor-partner-with-coi-expiration'));
        $contractorUserEleven->setUser($this->getReference('john-doe-admin'));
        $contractorUserEleven->setTitle('Senior Manager');
        $contractorUserEleven->setPersonalPhone('(111)222-2222');
        $contractorUserEleven->setWorkPhone('(111)222-2222');
        $contractorUserEleven->setFax('(111)222-2222');
        $contractorUserEleven->setAddress($this->getReference('address-fifteen'));
        $contractorUserEleven->setPersonalEmail('john_doe@email.com');
        $contractorUserEleven->setOwnerEntity($this->getReference('contractor-user-john-doe-for-monarch'));
        $contractorUserEleven->setDateSave(new \DateTime());

        $manager->persist($contractorUserEleven);
        $manager->flush();

        $this->addReference('contractor-user-john-doe-for-monarch-history', $contractorUserEleven);

        //John Bishop
        $contractorUserTwelve = new ContractorUserHistory();
        $contractorUserTwelve->setContractor($this->getReference('contractor-partner-with-coi-expiration'));
        $contractorUserTwelve->setUser($this->getReference('john-bishop-admin'));
        $contractorUserTwelve->setTitle('Manager');
        $contractorUserTwelve->setPersonalPhone('(111)222-2222');
        $contractorUserTwelve->setWorkPhone('(111)222-2222');
        $contractorUserTwelve->setFax('(111)222-2222');
        $contractorUserTwelve->setAddress($this->getReference('address-fifteen-2'));
        $contractorUserTwelve->setPersonalEmail('john_bishop@email.com');
        $contractorUserTwelve->setOwnerEntity($this->getReference('contractor-user-john-bishop-for-monarch'));
        $contractorUserTwelve->setDateSave(new \DateTime());

        $manager->persist($contractorUserTwelve);
        $manager->flush();

        $this->addReference('contractor-user-john-bishop-for-monarch-history', $contractorUserTwelve);

        //James Hatfield
        $contractorUserThirteen = new ContractorUserHistory();
        $contractorUserThirteen->setContractor($this->getReference('contractor-partner-with-coi-expiration'));
        $contractorUserThirteen->setUser($this->getReference('james-hatfield-admin'));
        $contractorUserThirteen->setTitle('Tester');
        $contractorUserThirteen->setPersonalPhone('(111)222-2222');
        $contractorUserThirteen->setWorkPhone('(111)222-2222');
        $contractorUserThirteen->setFax('(111)222-2222');
        $contractorUserThirteen->setAddress($this->getReference('address-fifteen-3'));
        $contractorUserThirteen->setPersonalEmail('james_hatfield@email.com');
        $contractorUserThirteen->setSignature('1500386316.png');
        $contractorUserThirteen->setPhoto('back-flow-testing.jpg');
        $contractorUserThirteen->setOwnerEntity($this->getReference('contractor-user-james-hatfield-for-monarch'));
        $contractorUserThirteen->setDateSave(new \DateTime());

        $manager->persist($contractorUserThirteen);
        $manager->flush();

        $this->addReference('contractor-user-james-hatfield-for-monarch-history', $contractorUserThirteen);

        //Kirk Hammet
        $contractorUserFourteen = new ContractorUserHistory();
        $contractorUserFourteen->setContractor($this->getReference('contractor-partner-with-coi-expiration'));
        $contractorUserFourteen->setUser($this->getReference('kirk-hammet-admin'));
        $contractorUserFourteen->setTitle('Tester');
        $contractorUserFourteen->setPersonalPhone('(111)222-2222');
        $contractorUserFourteen->setWorkPhone('(111)222-2222');
        $contractorUserFourteen->setFax('(111)222-2222');
        $contractorUserFourteen->setAddress($this->getReference('address-fifteen-4'));
        $contractorUserFourteen->setPersonalEmail('kirk_hammet@email.com');
        $contractorUserFourteen->setSignature('1500386336.png');
        $contractorUserFourteen->setPhoto('thmb_tester3.jpg');
        $contractorUserFourteen->setOwnerEntity($this->getReference('contractor-user-kirk-hammet-for-monarch'));
        $contractorUserFourteen->setDateSave(new \DateTime());

        $manager->persist($contractorUserFourteen);
        $manager->flush();

        $this->addReference('contractor-user-kirk-hammet-for-monarch-history', $contractorUserFourteen);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 10;
    }
}