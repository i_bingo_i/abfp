<?php

namespace AppBundle\DataFixtures\ORM\Workorders;

use AppBundle\Entity\ReportStatus;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadReportStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $passStatus = new ReportStatus();
        $passStatus->setName('Pass');
        $passStatus->setAlias('pass');
        $manager->persist($passStatus);
        $manager->flush();

        $this->addReference('report-status-pass', $passStatus);


        $failStatus = new ReportStatus();
        $failStatus->setName('Fail');
        $failStatus->setAlias('fail');
        $manager->persist($failStatus);
        $manager->flush();

        $this->addReference('report-status-fail', $failStatus);


        $notTestableStatus = new ReportStatus();
        $notTestableStatus->setName('Not Testable');
        $notTestableStatus->setAlias('not_testable');
        $manager->persist($notTestableStatus);
        $manager->flush();

        $this->addReference('report-status-not-testable', $notTestableStatus);

        $completedStatus = new ReportStatus();
        $completedStatus->setName('Completed');
        $completedStatus->setAlias('completed');
        $manager->persist($completedStatus);
        $manager->flush();

        $this->addReference('report-status-completed', $completedStatus);

    }

    public function getOrder()
    {
        return 1;
    }
}
