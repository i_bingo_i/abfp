<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ContractorCalendar;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadContractorCalendarData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contractorCalendar1 = new ContractorCalendar();
        $contractorCalendar1->setContractor($this->getReference('contractor-my-company'));
        $manager->persist($contractorCalendar1);
        $manager->flush();
        $this->addReference('contractor-calendar-my-company', $contractorCalendar1);
    }

    public function getOrder()
    {
        return 9;
    }
}
