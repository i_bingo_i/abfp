<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\ContractorUserHistory;
use AppBundle\Entity\Licenses;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadTestUserAndContractorData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
//
    public function load(ObjectManager $manager)
    {
//        $accessTokenService = $this->container->get('api.user_access_token');
//
//        // Mark Zuckerberg
//        $markZuckerberg = new User();
//        $markZuckerberg->setFirstName('Mark');
//        $markZuckerberg->setLastName('Zuckerberg');
//        $markZuckerberg->setEmail('markzuck@gmail.com');
//        $markZuckerberg->setPlainPassword('123456');
//        $markZuckerberg->addRole('ROLE_SUPER_ADMIN');
//        $markZuckerberg->addRole('ROLE_ADMIN');
//        $markZuckerberg->addRole('ROLE_CONTACT');
//        $markZuckerberg->setEnabled(1);
//        $markZuckerbergToken = $accessTokenService->createUserAccessToken($markZuckerberg);
//        $markZuckerberg->setAccessToken($markZuckerbergToken);
//
//        $manager->persist($markZuckerberg);
//        $manager->flush();
//        $this->addReference('mark-zuckerberg-super-admin', $markZuckerberg);
//
//        // Elon Musk
//        $elonMusk = new User();
//        $elonMusk->setFirstName('Mark');
//        $elonMusk->setLastName('Elon');
//        $elonMusk->setEmail('elonmusk@gmail.com');
//        $elonMusk->setPlainPassword('123456');
//        $elonMusk->addRole('ROLE_SUPER_ADMIN');
//        $elonMusk->addRole('ROLE_ADMIN');
//        $elonMusk->addRole('ROLE_CONTACT');
//        $elonMusk->setEnabled(1);
//        $elonMuskToken = $accessTokenService->createUserAccessToken($elonMusk);
//        $elonMusk->setAccessToken($elonMuskToken);
//
//        $manager->persist($elonMusk);
//        $manager->flush();
//        $this->addReference('elon-musk-super-admin', $elonMusk);
//
//        // Bill Gates
//        $billGates = new User();
//        $billGates->setFirstName('Bill');
//        $billGates->setLastName('Gates');
//        $billGates->setEmail('billgates@gmail.com');
//        $billGates->setPlainPassword('123456');
//        $billGates->addRole('ROLE_SUPER_ADMIN');
//        $billGates->addRole('ROLE_ADMIN');
//        $billGates->addRole('ROLE_CONTACT');
//        $billGates->setEnabled(1);
//        $billGatesToken = $accessTokenService->createUserAccessToken($billGates);
//        $billGates->setAccessToken($billGatesToken);
//
//        $manager->persist($billGates);
//        $manager->flush();
//        $this->addReference('bill-gates-super-admin', $billGates);
//
//
//        // Sergey Brin
//        $sergeyBrin = new User();
//        $sergeyBrin->setFirstName('Sergey');
//        $sergeyBrin->setLastName('Brin');
//        $sergeyBrin->setEmail('sergeybrin@gmail.com');
//        $sergeyBrin->setPlainPassword('123456');
//        $sergeyBrin->addRole('ROLE_SUPER_ADMIN');
//        $sergeyBrin->addRole('ROLE_ADMIN');
//        $sergeyBrin->addRole('ROLE_CONTACT');
//        $sergeyBrin->setEnabled(1);
//        $sergeyBrinToken = $accessTokenService->createUserAccessToken($sergeyBrin);
//        $sergeyBrin->setAccessToken($sergeyBrinToken);
//
//        $manager->persist($sergeyBrin);
//        $manager->flush();
//        $this->addReference('sergey-brin-super-admin', $sergeyBrin);
//
//        // Gabe Newell
//        $gabeNewell = new User();
//        $gabeNewell->setFirstName('Gabe');
//        $gabeNewell->setLastName('Newell');
//        $gabeNewell->setEmail('gabenewell@gmail.com');
//        $gabeNewell->setPlainPassword('111111');
//        $gabeNewell->addRole('ROLE_SUPER_ADMIN');
//        $gabeNewell->addRole('ROLE_ADMIN');
//        $gabeNewell->addRole('ROLE_CONTACT');
//        $gabeNewell->setEnabled(1);
//        $gabeNewellToken = $accessTokenService->createUserAccessToken($gabeNewell);
//        $gabeNewell->setAccessToken($gabeNewellToken);
//
//        $manager->persist($gabeNewell);
//        $manager->flush();
//        $this->addReference('gabe-newell-super-admin', $gabeNewell);
//
//        /*****************************************************/
//
//        $contractorUserElonMask = new ContractorUser();
//        $contractorUserElonMask->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserElonMask->setUser($this->getReference('elon-musk-super-admin'));
//        $contractorUserElonMask->setTitle('Super Admin');
//        $manager->persist($contractorUserElonMask);
//        $manager->flush();
//
//        $this->addReference('contractor-user-elon-mask', $contractorUserElonMask);
//
//
//        $contractorUserBillGates = new ContractorUser();
//        $contractorUserBillGates->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserBillGates->setUser($this->getReference('bill-gates-super-admin'));
//        $contractorUserBillGates->setTitle('Big Boss');
//        $manager->persist($contractorUserBillGates);
//        $manager->flush();
//
//        $this->addReference('contractor-user-bill-gates', $contractorUserBillGates);
//
//
//
//        $contractorUserSergeyBrin = new ContractorUser();
//        $contractorUserSergeyBrin->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserSergeyBrin->setUser($this->getReference('sergey-brin-super-admin'));
//        $contractorUserSergeyBrin->setTitle('Super Super Admin');
//        $manager->persist($contractorUserSergeyBrin);
//        $manager->flush();
//
//        $this->addReference('contractor-user-sergey-brin', $contractorUserSergeyBrin);
//
//
//
//        $contractorUserMarkZuckerberg = new ContractorUser();
//        $contractorUserMarkZuckerberg->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserMarkZuckerberg->setUser($this->getReference('mark-zuckerberg-super-admin'));
//        $contractorUserMarkZuckerberg->setTitle('Super Admin');
//        $manager->persist($contractorUserMarkZuckerberg);
//        $manager->flush();
//
//        $this->addReference('contractor-user-mark-zuckerberg', $contractorUserMarkZuckerberg);
//
//
//        $contractorUserGabeNewell = new ContractorUser();
//        $contractorUserGabeNewell->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserGabeNewell->setUser($this->getReference('gabe-newell-super-admin'));
//        $contractorUserGabeNewell->setTitle('Tester');
//        $manager->persist($contractorUserGabeNewell);
//        $manager->flush();
//
//        $this->addReference('contractor-user-gabe-newell', $contractorUserGabeNewell);
//
//
//
//        /**********************************************************************************/
//
//
//        $contractorUserElonMaskHistory = new ContractorUserHistory();
//        $contractorUserElonMaskHistory->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserElonMaskHistory->setUser($this->getReference('elon-musk-super-admin'));
//        $contractorUserElonMaskHistory->setOwnerEntity($this->getReference('contractor-user-elon-mask'));
//        $contractorUserElonMaskHistory->setTitle('Super Admin');
//        $contractorUserElonMaskHistory->setDateSave(new \DateTime());
//        $manager->persist($contractorUserElonMaskHistory);
//        $manager->flush();
//
//        $this->addReference('contractor-user-elon-mask-history', $contractorUserElonMaskHistory);
//
//
//        $contractorUserBillGatesHistory = new ContractorUserHistory();
//        $contractorUserBillGatesHistory->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserBillGatesHistory->setUser($this->getReference('bill-gates-super-admin'));
//        $contractorUserBillGatesHistory->setOwnerEntity($this->getReference('contractor-user-bill-gates'));
//        $contractorUserBillGatesHistory->setTitle('Big Boss');
//        $contractorUserBillGatesHistory->setDateSave(new \DateTime());
//        $manager->persist($contractorUserBillGatesHistory);
//        $manager->flush();
//
//        $this->addReference('contractor-user-bill-gates-history', $contractorUserBillGatesHistory);
//
//
//        $contractorUserSergeyBrinHistory = new ContractorUserHistory();
//        $contractorUserSergeyBrinHistory->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserSergeyBrinHistory->setUser($this->getReference('sergey-brin-super-admin'));
//        $contractorUserSergeyBrinHistory->setOwnerEntity($this->getReference('contractor-user-sergey-brin'));
//        $contractorUserSergeyBrinHistory->setTitle('Big Boss');
//        $contractorUserSergeyBrinHistory->setDateSave(new \DateTime());
//        $manager->persist($contractorUserSergeyBrinHistory);
//        $manager->flush();
//
//        $this->addReference('contractor-user-sergey-brin-history', $contractorUserSergeyBrinHistory);
//
//
//        $contractorUserMarkZuckerbergHistory = new ContractorUserHistory();
//        $contractorUserMarkZuckerbergHistory->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserMarkZuckerbergHistory->setUser($this->getReference('mark-zuckerberg-super-admin'));
//        $contractorUserMarkZuckerbergHistory->setOwnerEntity($this->getReference('contractor-user-mark-zuckerberg'));
//        $contractorUserMarkZuckerbergHistory->setTitle('Admin');
//        $contractorUserMarkZuckerbergHistory->setDateSave(new \DateTime());
//        $manager->persist($contractorUserMarkZuckerbergHistory);
//        $manager->flush();
//
//        $this->addReference('contractor-user-mark-zuckerberg-history', $contractorUserMarkZuckerbergHistory);
//
//
//        $contractorUserGabeNewellHistory = new ContractorUserHistory();
//        $contractorUserGabeNewellHistory->setContractor($manager->getRepository("AppBundle:Contractor")->findOneBy(["name" => "American Backflow & Fire Prevention"]));
//        $contractorUserGabeNewellHistory->setUser($this->getReference('gabe-newell-super-admin'));
//        $contractorUserGabeNewellHistory->setOwnerEntity($this->getReference('contractor-user-gabe-newell'));
//        $contractorUserGabeNewellHistory->setTitle('Tester');
//        $contractorUserGabeNewellHistory->setDateSave(new \DateTime());
//        $manager->persist($contractorUserGabeNewellHistory);
//        $manager->flush();
//
//        $this->addReference('contractor-user-gabe-newell-history', $contractorUserGabeNewellHistory);
//
//
//        /**********************************************************************************/
//
//        /** Bill Gates */
//        $backflowBillGates = new Licenses();
//        $backflowBillGates->setCode("2342344323112");
//        $backflowBillGates->setRenewalDate(new \DateTime("+4 month"));
//        $backflowBillGates->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "backflow license"]));
//        $backflowBillGates->setContractorUser($this->getReference('contractor-user-bill-gates'));
//        $manager->persist($backflowBillGates);
//        $manager->flush();
//
//        $this->addReference('backflow-license-bill-g', $backflowBillGates);
//
//
//        $fireBillGates = new Licenses();
//        $fireBillGates->setCode("612327767115");
//        $fireBillGates->setRenewalDate(new \DateTime("+5 month"));
//        $fireBillGates->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "nafed license"]));
//        $fireBillGates->setContractorUser($this->getReference('contractor-user-bill-gates'));
//        $manager->persist($fireBillGates);
//        $manager->flush();
//
//        $this->addReference('nafed-license-bill-g', $fireBillGates);
//
//
//        /** Sergey Brin */
//        $backflowSergeyBrin = new Licenses();
//        $backflowSergeyBrin->setCode("21212124356");
//        $backflowSergeyBrin->setRenewalDate(new \DateTime("+4 month"));
//        $backflowSergeyBrin->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "backflow license"]));
//        $backflowSergeyBrin->setContractorUser($this->getReference('contractor-user-sergey-brin'));
//        $manager->persist($backflowSergeyBrin);
//        $manager->flush();
//
//        $this->addReference('backflow-license-sergey-b', $backflowSergeyBrin);
//
//
//        $fireSergeyBrin= new Licenses();
//        $fireSergeyBrin->setCode("12544565990");
//        $fireSergeyBrin->setRenewalDate(new \DateTime("+5 month"));
//        $fireSergeyBrin->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "nafed license"]));
//        $fireSergeyBrin->setContractorUser($this->getReference('contractor-user-sergey-brin'));
//        $manager->persist($fireSergeyBrin);
//        $manager->flush();
//
//        $this->addReference('nafed-license-sergey-b', $fireSergeyBrin);
//
//
//        /** Mark Zuckerberg */
//        $backflowMarkZuckerberg = new Licenses();
//        $backflowMarkZuckerberg->setCode("99000876552");
//        $backflowMarkZuckerberg->setRenewalDate(new \DateTime("+4 month"));
//        $backflowMarkZuckerberg->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "backflow license"]));
//        $backflowMarkZuckerberg->setContractorUser($this->getReference('contractor-user-mark-zuckerberg'));
//        $manager->persist($backflowMarkZuckerberg);
//        $manager->flush();
//
//        $this->addReference('backflow-license-mark-z', $backflowMarkZuckerberg);
//
//        $NAFEDMarkZ = new Licenses();
//        $NAFEDMarkZ->setCode("764355232345");
//        $NAFEDMarkZ->setRenewalDate(new \DateTime("+1 month"));
//        $NAFEDMarkZ->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "nafed license"]));
//        $NAFEDMarkZ->setContractorUser($this->getReference('contractor-user-mark-zuckerberg'));
//        $manager->persist($NAFEDMarkZ);
//        $manager->flush();
//
//        $this->addReference('nafed-license-mark-z', $NAFEDMarkZ);
//
//        /** Gabe Newell */
//        $backflowGabeNewell = new Licenses();
//        $backflowGabeNewell->setCode("99000876552");
//        $backflowGabeNewell->setRenewalDate(new \DateTime("+4 month"));
//        $backflowGabeNewell->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "backflow license"]));
//        $backflowGabeNewell->setContractorUser($this->getReference('contractor-user-gabe-newell'));
//        $manager->persist($backflowGabeNewell);
//        $manager->flush();
//
//        $this->addReference('backflow-license-gabe-n', $backflowGabeNewell);
//
//
//        $fireGabeNewell = new Licenses();
//        $fireGabeNewell->setCode("345345343212");
//        $fireGabeNewell->setRenewalDate(new \DateTime("+5 month"));
//        $fireGabeNewell->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "nafed license"]));
//        $fireGabeNewell->setContractorUser($this->getReference('contractor-user-gabe-newell'));
//        $manager->persist($fireGabeNewell);
//        $manager->flush();
//
//        $this->addReference('nafed-license-gabe-n', $fireGabeNewell);
//
//
//        /** Elon Mask */
//        $backflowMask = new Licenses();
//        $backflowMask->setCode("784325537345");
//        $backflowMask->setRenewalDate(new \DateTime("+1 month"));
//        $backflowMask->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "backflow license"]));
//        $backflowMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
//        $manager->persist($backflowMask);
//        $manager->flush();
//
//        $this->addReference('backflow-license-elon-m', $backflowMask);
//
//
//        $fireMask = new Licenses();
//        $fireMask->setCode("345345343212");
//        $fireMask->setRenewalDate(new \DateTime("+5 month"));
//        $fireMask->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "nafed license"]));
//        $fireMask->setContractorUser($this->getReference('contractor-user-gabe-newell'));
//        $manager->persist($fireMask);
//        $manager->flush();
//
//        $this->addReference('nafed-license-elon-m', $fireMask);
//
//
//        $userJackFresko = $manager->getRepository("AppBundle:User")->findOneBy(["email" => "jakfresko@gmail.com"]);
//
//
//        $plumbersFireJakFresko = new Licenses();
//        $plumbersFireJakFresko->setCode("712325537115");
//        $plumbersFireJakFresko->setRenewalDate(new \DateTime("+1 month"));
//        $plumbersFireJakFresko->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "plumbers license"]));
//        $plumbersFireJakFresko->setContractorUser($manager->getRepository("AppBundle:ContractorUser")->findOneBy(["user" => $userJackFresko]));
//        $manager->persist($plumbersFireJakFresko);
//        $manager->flush();
//
//        $this->addReference('plumbers-license-jak-f', $plumbersFireJakFresko);
//
//
//        $nicentFireJakFresko = new Licenses();
//        $nicentFireJakFresko->setCode("712325537115");
//        $nicentFireJakFresko->setRenewalDate(new \DateTime("+1 month"));
//        $nicentFireJakFresko->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "nicet license"]));
//        $nicentFireJakFresko->setContractorUser($manager->getRepository("AppBundle:ContractorUser")->findOneBy(["user" => $userJackFresko]));
//        $manager->persist($nicentFireJakFresko);
//        $manager->flush();
//
//        $this->addReference('nicent-license-jak-f', $nicentFireJakFresko);
//
//
//        $plumbersMask = new Licenses();
//        $plumbersMask->setCode("784325537345");
//        $plumbersMask->setRenewalDate(new \DateTime("+1 month"));
//        $plumbersMask->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "plumbers license"]));
//        $plumbersMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
//        $manager->persist($plumbersMask);
//        $manager->flush();
//
//        $this->addReference('plumbers-license-elon-m', $plumbersMask);
//
//
//        $nicentMask = new Licenses();
//        $nicentMask->setCode("784325537345");
//        $nicentMask->setRenewalDate(new \DateTime("+1 month"));
//        $nicentMask->setNamed($manager->getRepository("AppBundle:LicensesNamed")->findOneBy(["alias" => "nicet license"]));
//        $nicentMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
//        $manager->persist($nicentMask);
//        $manager->flush();
//
//        $this->addReference('nicent-license-elon-m', $nicentMask);
    }

    public function getOrder()
    {
        return 1;
    }
}
