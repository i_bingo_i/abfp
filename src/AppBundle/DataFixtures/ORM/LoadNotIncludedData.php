<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\NotIncluded;

class LoadNotIncludedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //backflow start
        $correctingServiceValvesIssuesIfAny = new NotIncluded();
        $correctingServiceValvesIssuesIfAny->setName('Correcting service valves issues if any');
        $correctingServiceValvesIssuesIfAny->setAlias('correcting_service_valves_issues_if_any');
        $correctingServiceValvesIssuesIfAny->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($correctingServiceValvesIssuesIfAny);
        $manager->flush();
        $this->addReference('correcting-service-valves-issues-if-any-backflow-not-included', $correctingServiceValvesIssuesIfAny);

        $permitsPermitFeesOrDrawings = new NotIncluded();
        $permitsPermitFeesOrDrawings->setName('Permits, Permit Fees or Drawings');
        $permitsPermitFeesOrDrawings->setAlias('permits_permit_fees_or_drawings');
        $permitsPermitFeesOrDrawings->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($permitsPermitFeesOrDrawings);
        $manager->flush();
        $this->addReference('permits-permit-fees-or-drawings-backflow-not-included', $permitsPermitFeesOrDrawings);

        $hardsPartsIfNeeded = new NotIncluded();
        $hardsPartsIfNeeded->setName('Hards Parts (if needed)');
        $hardsPartsIfNeeded->setAlias('hards_parts_if_needed');
        $hardsPartsIfNeeded->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($hardsPartsIfNeeded);
        $manager->flush();
        $this->addReference('hards-parts-if-needed-backflow-not-included', $hardsPartsIfNeeded);

        $additionalWorkNotMentionedInThisProposal = new NotIncluded();
        $additionalWorkNotMentionedInThisProposal->setName('Additional work not mentioned in this proposal');
        $additionalWorkNotMentionedInThisProposal->setAlias('additional_work_not_mentioned_in_this_proposal');
        $additionalWorkNotMentionedInThisProposal->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($additionalWorkNotMentionedInThisProposal);
        $manager->flush();
        $this->addReference('additional-work-not-mentioned-in-this-proposal-backflow-not-included', $additionalWorkNotMentionedInThisProposal);

        $hydraulicCalulations = new NotIncluded();
        $hydraulicCalulations->setName('Hydraulic Calulations');
        $hydraulicCalulations->setAlias('hydraulic_calulations');
        $hydraulicCalulations->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($hydraulicCalulations);
        $manager->flush();
        $this->addReference('hydraulic-calulations-backflow-not-included', $hydraulicCalulations);

        $anyPatchingOrPaintingServices = new NotIncluded();
        $anyPatchingOrPaintingServices->setName('Any Patching or Painting services');
        $anyPatchingOrPaintingServices->setAlias('any_patching_or_painting_services');
        $anyPatchingOrPaintingServices->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($anyPatchingOrPaintingServices);
        $manager->flush();
        $this->addReference('any-patching-or-painting-services-backflow-not-included', $anyPatchingOrPaintingServices);

        $electricalOrAlarmWorkIfNeededIsExtra = new NotIncluded();
        $electricalOrAlarmWorkIfNeededIsExtra->setName('Electrical or alarm work if needed is extra');
        $electricalOrAlarmWorkIfNeededIsExtra->setAlias('electrical_or_alarm_work_if_needed_is_extra');
        $electricalOrAlarmWorkIfNeededIsExtra->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($electricalOrAlarmWorkIfNeededIsExtra);
        $manager->flush();
        $this->addReference('electrical-or-alarm-work-if-needed-is-extra-backflow-not-included', $electricalOrAlarmWorkIfNeededIsExtra);

        // backflow end

        //fire start
        $anyNotificationsToBuildingOccupantsFire = new NotIncluded();
        $anyNotificationsToBuildingOccupantsFire->setName('Any Notifications to Building Occupants');
        $anyNotificationsToBuildingOccupantsFire->setAlias('any_notifications_to_building_occupants');
        $anyNotificationsToBuildingOccupantsFire->setDivision($this->getReference('device-category-fire'));
        $manager->persist($anyNotificationsToBuildingOccupantsFire);
        $manager->flush();
        $this->addReference('any-notifications-to-building-occupants-fire', $anyNotificationsToBuildingOccupantsFire);

        $anyNotificationsToBuildingOccupantsFire = new NotIncluded();
        $anyNotificationsToBuildingOccupantsFire->setName('Permits, Permit Fees or Drawings');
        $anyNotificationsToBuildingOccupantsFire->setAlias('permits_permit_fees_or_drawings_fire');
        $anyNotificationsToBuildingOccupantsFire->setDivision($this->getReference('device-category-fire'));
        $manager->persist($anyNotificationsToBuildingOccupantsFire);
        $manager->flush();
        $this->addReference('permits-permit-fees-or-drawings-fire', $anyNotificationsToBuildingOccupantsFire);

        $additionalWorkNotMentionedInThisProposalFire = new NotIncluded();
        $additionalWorkNotMentionedInThisProposalFire->setName('Additional work not mentioned in this proposal');
        $additionalWorkNotMentionedInThisProposalFire->setAlias('additional_work_not_mentioned_in_this_proposal_fire');
        $additionalWorkNotMentionedInThisProposalFire->setDivision($this->getReference('device-category-fire'));
        $manager->persist($additionalWorkNotMentionedInThisProposalFire);
        $manager->flush();
        $this->addReference('additional-work-not-mentioned-in-this-proposal-fire', $additionalWorkNotMentionedInThisProposalFire);

        $hydraulicCalculations = new NotIncluded();
        $hydraulicCalculations->setName('Hydraulic Calculations');
        $hydraulicCalculations->setAlias('hydraulic_calculations');
        $hydraulicCalculations->setDivision($this->getReference('device-category-fire'));
        $manager->persist($hydraulicCalculations);
        $manager->flush();
        $this->addReference('hydraulic-calculations-fire', $hydraulicCalculations);

        $AnyPatchingOrPaintingServicesFire = new NotIncluded();
        $AnyPatchingOrPaintingServicesFire->setName('Any Patching or Painting services');
        $AnyPatchingOrPaintingServicesFire->setAlias('any_patching_or_painting_services_fire');
        $AnyPatchingOrPaintingServicesFire->setDivision($this->getReference('device-category-fire'));
        $manager->persist($AnyPatchingOrPaintingServicesFire);
        $manager->flush();
        $this->addReference('any-patching-or-painting-services-fire', $AnyPatchingOrPaintingServicesFire);

        $anyTamperSwitches = new NotIncluded();
        $anyTamperSwitches->setName('Any tamper switches');
        $anyTamperSwitches->setAlias('any_tamper_switches');
        $anyTamperSwitches->setDivision($this->getReference('device-category-fire'));
        $manager->persist($anyTamperSwitches);
        $manager->flush();
        $this->addReference('any_tamper_switches-fire', $anyTamperSwitches);

        $insulation = new NotIncluded();
        $insulation->setName('Insulation');
        $insulation->setAlias('insulation');
        $insulation->setDivision($this->getReference('device-category-fire'));
        $manager->persist($insulation);
        $manager->flush();
        $this->addReference('insulation-fire', $insulation);

        $liftEquipmentFire = new NotIncluded();
        $liftEquipmentFire->setName('Lift equipment');
        $liftEquipmentFire->setAlias('lift_equipment_fire');
        $liftEquipmentFire->setDivision($this->getReference('device-category-fire'));
        $manager->persist($liftEquipmentFire);
        $manager->flush();
        $this->addReference('lift-equipment-fire', $liftEquipmentFire);

        $liftEquipmentFire = new NotIncluded();
        $liftEquipmentFire->setName('Any additional programming to Alarm Panel');
        $liftEquipmentFire->setAlias('any_additional_programming_to_alarm_panel_fire');
        $liftEquipmentFire->setDivision($this->getReference('device-category-fire'));
        $manager->persist($liftEquipmentFire);
        $manager->flush();
        $this->addReference('any-additional-programming-to-alarm-panel-fire', $liftEquipmentFire);

        //fire end

        //plumbing start
        $anyPatchingPaintingOrTileServicesPlumbing = new NotIncluded();
        $anyPatchingPaintingOrTileServicesPlumbing->setName('Any Patching, Painting or Tile services');
        $anyPatchingPaintingOrTileServicesPlumbing->setAlias('any_patching_painting_or_tile_services_plumbing');
        $anyPatchingPaintingOrTileServicesPlumbing->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($anyPatchingPaintingOrTileServicesPlumbing);
        $manager->flush();
        $this->addReference('any-patching-painting-or-tile-services-plumbing', $anyPatchingPaintingOrTileServicesPlumbing);

        $permitsPermitFeesOrDrawingsPlumbing = new NotIncluded();
        $permitsPermitFeesOrDrawingsPlumbing->setName('Permits. Permit Fees, or Drawings');
        $permitsPermitFeesOrDrawingsPlumbing->setAlias('permits_permit_fees_or_drawings_plumbing');
        $permitsPermitFeesOrDrawingsPlumbing->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($permitsPermitFeesOrDrawingsPlumbing);
        $manager->flush();
        $this->addReference('permits-permit-fees-or-drawings-plumbing', $permitsPermitFeesOrDrawingsPlumbing);

        $notResponsibleForCuttingUtilitiesInTheFloorPlumbing = new NotIncluded();
        $notResponsibleForCuttingUtilitiesInTheFloorPlumbing->setName('Not responsible For Cutting Utilities in the Floor');
        $notResponsibleForCuttingUtilitiesInTheFloorPlumbing->setAlias('not_responsible_for_cutting_utilities_in_the_floor_plumbing');
        $notResponsibleForCuttingUtilitiesInTheFloorPlumbing->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($notResponsibleForCuttingUtilitiesInTheFloorPlumbing);
        $manager->flush();
        $this->addReference('not-responsible-for-cutting-utilities-in-the-floor-plumbing', $notResponsibleForCuttingUtilitiesInTheFloorPlumbing);

        $correctingServiceValvesIssuesIfAnyPlumbing = new NotIncluded();
        $correctingServiceValvesIssuesIfAnyPlumbing->setName('Correcting service valves issues if any');
        $correctingServiceValvesIssuesIfAnyPlumbing->setAlias('correcting_service_valves_issues_if_any_plumbing');
        $correctingServiceValvesIssuesIfAnyPlumbing->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($correctingServiceValvesIssuesIfAnyPlumbing);
        $manager->flush();
        $this->addReference('correcting-service-valves-issues-if-any-plumbing', $correctingServiceValvesIssuesIfAnyPlumbing);

        $insulationPlumbing = new NotIncluded();
        $insulationPlumbing->setName('Insulation');
        $insulationPlumbing->setAlias('insulation_plumbing');
        $insulationPlumbing->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($insulationPlumbing);
        $manager->flush();
        $this->addReference('insulation-plumbing', $insulationPlumbing);

        $liftEquipmentPlumbing = new NotIncluded();
        $liftEquipmentPlumbing->setName('Lift equipment');
        $liftEquipmentPlumbing->setAlias('lift_equipment_plumbing');
        $liftEquipmentPlumbing->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($liftEquipmentPlumbing);
        $manager->flush();
        $this->addReference('lift-equipment-plumbing', $liftEquipmentPlumbing);
        //plumbing end
    }

    public function getOrder()
    {
        return 2;
    }
}
