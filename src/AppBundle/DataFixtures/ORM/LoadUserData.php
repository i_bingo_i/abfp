<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $accessTokenService = $this->container->get('api.user_access_token');


        // Schur Maksim
        $schurMaksim = new User();
        $schurMaksim->setFirstName('Maksim');
        $schurMaksim->setLastName('Schur');
        $schurMaksim->setEmail('schur.maksim@gmail.com');
        $schurMaksim->setPlainPassword('111111');
        $schurMaksim->addRole('ROLE_SUPER_ADMIN');
        $schurMaksim->setEnabled(1);
        $schurMaksimToken = $accessTokenService->createUserAccessToken($schurMaksim);
        $schurMaksim->setAccessToken($schurMaksimToken);

        $manager->persist($schurMaksim);
        $manager->flush();
        $this->addReference('schur-maksim-super-admin', $schurMaksim);


        // Oleksandr Korchak
        $oleksandrKorchak = new User();
        $oleksandrKorchak->setFirstName('Oleksandr');
        $oleksandrKorchak->setLastName('Korchak');
        $oleksandrKorchak->setEmail('oleksandr.korchak@idapgroup.com');
        $oleksandrKorchak->setPlainPassword('1111aA');
        $oleksandrKorchak->addRole('ROLE_SUPER_ADMIN');
        $oleksandrKorchak->setEnabled(1);
        $oleksandrToken = $accessTokenService->createUserAccessToken($oleksandrKorchak);
        $oleksandrKorchak->setAccessToken($oleksandrToken);

        $manager->persist($oleksandrKorchak);
        $manager->flush();
        $this->addReference('oleksandr-korchak-super-admin', $oleksandrKorchak);


        // Daria Novikova
        $dariaNovikova = new User();
        $dariaNovikova->setFirstName('Daria');
        $dariaNovikova->setLastName('Novikova');
        $dariaNovikova->setEmail('daria.novikova@idapgroup.com');
        $dariaNovikova->setPlainPassword('111111');
        $dariaNovikova->addRole('ROLE_SUPER_ADMIN');
        $dariaNovikova->setEnabled(1);
        $dariaNovikovaToken = $accessTokenService->createUserAccessToken($dariaNovikova);
        $dariaNovikova->setAccessToken($dariaNovikovaToken);

        $manager->persist($dariaNovikova);
        $manager->flush();
        $this->addReference('daria-novikova-super-admin', $dariaNovikova);


        // Taras Turbovets
        $tarasTurbovets = new User();
        $tarasTurbovets->setFirstName('Taras');
        $tarasTurbovets->setLastName('Turbovets');
        $tarasTurbovets->setEmail('taras.turbovets@idapgroup.com');
        $tarasTurbovets->setPlainPassword('111111');
        $tarasTurbovets->addRole('ROLE_SUPER_ADMIN');
        $tarasTurbovets->setEnabled(1);
        $tarasToken = $accessTokenService->createUserAccessToken($tarasTurbovets);
        $tarasTurbovets->setAccessToken($tarasToken);

        $manager->persist($tarasTurbovets);
        $manager->flush();
        $this->addReference('taras-turbovets-super-admin', $tarasTurbovets);


        // Igor Yaitskikh
        $igoryaItskikh = new User();
        $igoryaItskikh->setFirstName('Igor');
        $igoryaItskikh->setLastName('Yaitskikh');
        $igoryaItskikh->setEmail('kocmohabty@yandex.ru');
        $igoryaItskikh->setPlainPassword('111111');
        $igoryaItskikh->addRole('ROLE_SUPER_ADMIN');
        $igoryaItskikh->setEnabled(1);
        $igoryaToken = $accessTokenService->createUserAccessToken($igoryaItskikh);
        $igoryaItskikh->setAccessToken($igoryaToken);

        $manager->persist($igoryaItskikh);
        $manager->flush();
        $this->addReference('igor-yaitskikh-super-admin', $igoryaItskikh);


        // Mykola Shevchuk
        $mykolaShevchuk = new User();
        $mykolaShevchuk->setFirstName('Mykola');
        $mykolaShevchuk->setLastName('Shevchuk');
        $mykolaShevchuk->setEmail('mykola.shevchuk@idapgroup.com');
        $mykolaShevchuk->setPlainPassword('111111');
        $mykolaShevchuk->addRole('ROLE_SUPER_ADMIN');
        $mykolaShevchuk->setEnabled(1);
        $mykolaShevchukToken = $accessTokenService->createUserAccessToken($mykolaShevchuk);
        $mykolaShevchuk->setAccessToken($mykolaShevchukToken);

        $manager->persist($mykolaShevchuk);
        $manager->flush();
        $this->addReference('mykola-shevchuk-super-admin', $mykolaShevchuk);


        // Pavlo Pashchevskyi
        $pavloPashchevskyi = new User();
        $pavloPashchevskyi->setFirstName('Pavlo');
        $pavloPashchevskyi->setLastName('Pashchevskyi');
        $pavloPashchevskyi->setEmail('pavlo.pashchevskyi@idapgroup.com');
        $pavloPashchevskyi->setPlainPassword('111111');
        $pavloPashchevskyi->addRole('ROLE_SUPER_ADMIN');
        $pavloPashchevskyi->setEnabled(1);
        $pavloToken = $accessTokenService->createUserAccessToken($pavloPashchevskyi);
        $pavloPashchevskyi->setAccessToken($pavloToken);

        $manager->persist($pavloPashchevskyi);
        $manager->flush();
        $this->addReference('pavlo-pashchevskyi-super-admin', $pavloPashchevskyi);


        // Anastasiya Mashoshyna
        $anastasiyaMashoshyna = new User();
        $anastasiyaMashoshyna->setFirstName('Anastasiya');
        $anastasiyaMashoshyna->setLastName('Mashoshyna');
        $anastasiyaMashoshyna->setEmail('anastasiya.mashoshyna@idapgroup.com');
        $anastasiyaMashoshyna->setPlainPassword('111111');
        $anastasiyaMashoshyna->addRole('ROLE_SUPER_ADMIN');
        $anastasiyaMashoshyna->setEnabled(1);
        $anastasiyaToken = $accessTokenService->createUserAccessToken($anastasiyaMashoshyna);
        $anastasiyaMashoshyna->setAccessToken($anastasiyaToken);

        $manager->persist($anastasiyaMashoshyna);
        $manager->flush();
        $this->addReference('anastasiya-mashoshyna-super-admin', $anastasiyaMashoshyna);


        // Dan Harbut
        $danHarbut = new User();
        $danHarbut->setFirstName('Dan');
        $danHarbut->setLastName('Harbut');
        $danHarbut->setEmail('dharbut@gmail.com');
        $danHarbut->setPlainPassword('Backflow');
        $danHarbut->addRole('ROLE_SUPER_ADMIN');
        $danHarbut->addRole('ROLE_ADMIN');
        $danHarbut->addRole('ROLE_CONTACT');
        $danHarbut->setEnabled(1);
        $danHarbutToken = $accessTokenService->createUserAccessToken($danHarbut);
        $danHarbut->setAccessToken($danHarbutToken);

        $manager->persist($danHarbut);
        $manager->flush();
        $this->addReference('dan-harbut-super-admin', $danHarbut);


        // ABFP SuperAdmin
        $superAdmin = new User();
        $superAdmin->setFirstName('ABFP');
        $superAdmin->setLastName('SuperAdmin');
        $superAdmin->setEmail('superadmin@americanbackflowprevention.com');
        $superAdmin->setPlainPassword('Superadmin#123');
        $superAdmin->addRole('ROLE_SUPER_ADMIN');
        $superAdmin->addRole('ROLE_ADMIN');
        $superAdmin->addRole('ROLE_CONTACT');
        $superAdmin->setEnabled(1);
        $superAdminToken = $accessTokenService->createUserAccessToken($superAdmin);
        $superAdmin->setAccessToken($superAdminToken);

        $manager->persist($superAdmin);
        $manager->flush();
        $this->addReference('super-admin-super-admin', $superAdmin);


        // API Maksim Schur
        $apiUser = new User();
        $apiUser->setFirstName('Maksim');
        $apiUser->setLastName('Schur');
        $apiUser->setEmail('api@test.com');
        $apiUser->setPlainPassword('111111');
        $apiUser->addRole('ROLE_API');
        $apiUser->setEnabled(1);
        $apiUserAccessToken = $accessTokenService->createUserAccessToken($apiUser);
        $apiUser->setAccessToken($apiUserAccessToken);

        $manager->persist($apiUser);
        $manager->flush();
        $this->addReference('api-user', $apiUser);


        // Test1
        $testApi1User = new User();
        $testApi1User->setFirstName('Test1');
        $testApi1User->setLastName('Test1');
        $testApi1User->setEmail('test_api1@test.com');
        $testApi1User->setPlainPassword('111111');
        $testApi1User->addRole('ROLE_API');
        $testApi1User->setEnabled(1);
        $apiUserAccessToken = $accessTokenService->createUserAccessToken($testApi1User);
        $testApi1User->setAccessToken($apiUserAccessToken);

        $manager->persist($testApi1User);
        $manager->flush();
        $this->addReference('test-api1-user', $testApi1User);


        // Test2
        $testApi2User = new User();
        $testApi2User->setFirstName('Test2');
        $testApi2User->setLastName('Test2');
        $testApi2User->setEmail('test_api2@test.com');
        $testApi2User->setPlainPassword('111111');
        $testApi2User->addRole('ROLE_API');
        $testApi2User->setEnabled(1);
        $apiUserAccessToken = $accessTokenService->createUserAccessToken($testApi2User);
        $testApi2User->setAccessToken($apiUserAccessToken);

        $manager->persist($testApi2User);
        $manager->flush();
        $this->addReference('test-api2-user', $testApi2User);


        //apiUser2 (Igor Yaitskikh)
        $apiUser2 = new User();
        $apiUser2->setFirstName('Igor');
        $apiUser2->setLastName('Yaitskikh');
        $apiUser2->setEmail('api2@test.com');
        $apiUser2->setPlainPassword('111111');
        $apiUser2->addRole('ROLE_API');
        $apiUser2->setEnabled(1);
        $apiUserAccessToken2 = $accessTokenService->createUserAccessToken($apiUser2);
        $apiUser2->setAccessToken($apiUserAccessToken2);

        $manager->persist($apiUser2);
        $manager->flush();
        $this->addReference('api-user-2', $apiUser2);


        //Alex Babich
        $babichAlex = new User();
        $babichAlex->setFirstName('Alex');
        $babichAlex->setLastName('Babich');
        $babichAlex->setEmail('ibabich88i@gmail.com');
        $babichAlex->setPlainPassword('111111');
        $babichAlex->addRole('ROLE_SUPER_ADMIN');
        $babichAlex->setEnabled(1);
        $babichAlexToken = $accessTokenService->createUserAccessToken($babichAlex);
        $babichAlex->setAccessToken($babichAlexToken);

        $manager->persist($babichAlex);
        $manager->flush();
        $this->addReference('babich-alex-super-admin', $babichAlex);


        //John Doe
        $johnDoe = new User();
        $johnDoe->setFirstName('John');
        $johnDoe->setLastName('Doe');
        $johnDoe->setEmail('john.doe@example.com');
        $johnDoe->setPlainPassword('111111');
        $johnDoe->addRole('ROLE_SUPER_ADMIN');
        $johnDoe->addRole('ROLE_ADMIN');
        $johnDoe->addRole('ROLE_CONTACT');
        $johnDoe->setEnabled(1);
        $johnDoeToken = $accessTokenService->createUserAccessToken($johnDoe);
        $johnDoe->setAccessToken($johnDoeToken);

        $manager->persist($johnDoe);
        $manager->flush();
        $this->addReference('john-doe-super-admin', $johnDoe);


        //John Doe admin
        $johnDoeAdmin = new User();
        $johnDoeAdmin->setFirstName('John');
        $johnDoeAdmin->setLastName('Doe');
        $johnDoeAdmin->setEmail('john_doe_admin@email.com');
        $johnDoeAdmin->setPlainPassword('111111');
        $johnDoeAdmin->addRole('ROLE_ADMIN');
        $johnDoeAdmin->addRole('ROLE_CONTACT');
        $johnDoeAdmin->setEnabled(1);
        $johnDoeAdminToken = $accessTokenService->createUserAccessToken($johnDoeAdmin);
        $johnDoeAdmin->setAccessToken($johnDoeAdminToken);

        $manager->persist($johnDoeAdmin);
        $manager->flush();
        $this->addReference('john-doe-admin', $johnDoeAdmin);


        //John Bishop
        $johnBishop = new User();
        $johnBishop->setFirstName('John');
        $johnBishop->setLastName('Bishop');
        $johnBishop->setEmail('john_bishop@email.com');
        $johnBishop->setPlainPassword('111111');
        $johnBishop->addRole('ROLE_CONTACT');
        $johnBishop->setEnabled(1);
        $johnBishopToken = $accessTokenService->createUserAccessToken($johnBishop);
        $johnBishop->setAccessToken($johnBishopToken);

        $manager->persist($johnBishop);
        $manager->flush();
        $this->addReference('john-bishop-admin', $johnBishop);


        //James Hatfield
        $jamesHatfield = new User();
        $jamesHatfield->setFirstName('James');
        $jamesHatfield->setLastName('Hatfield');
        $jamesHatfield->setEmail('james_hatfield@email.com');
        $jamesHatfield->setPlainPassword('111111');
        $jamesHatfield->addRole('ROLE_TESTER');
        $jamesHatfield->setEnabled(1);
        $jamesHatfieldToken = $accessTokenService->createUserAccessToken($jamesHatfield);
        $jamesHatfield->setAccessToken($jamesHatfieldToken);

        $manager->persist($jamesHatfield);
        $manager->flush();
        $this->addReference('james-hatfield-admin', $jamesHatfield);


        //Kirk Hammet
        $kirkHammet = new User();
        $kirkHammet->setFirstName('Kirk');
        $kirkHammet->setLastName('Hammet');
        $kirkHammet->setEmail('kirk_hammet@email.com');
        $kirkHammet->setPlainPassword('111111');
        $kirkHammet->addRole('ROLE_TESTER');
        $kirkHammet->setEnabled(1);
        $kirkHammetToken = $accessTokenService->createUserAccessToken($kirkHammet);
        $kirkHammet->setAccessToken($kirkHammetToken);

        $manager->persist($kirkHammet);
        $manager->flush();
        $this->addReference('kirk-hammet-admin', $kirkHammet);


        //David Loes
        $davidLoes = new User();
        $davidLoes->setFirstName('David');
        $davidLoes->setLastName('Loes');
        $davidLoes->setEmail('loes5307@comcast.net');
        $davidLoes->setPlainPassword('Skidoo!200');
        $davidLoes->addRole('ROLE_SUPER_ADMIN');
        $davidLoes->addRole('ROLE_ADMIN');
        $davidLoes->addRole('ROLE_CONTACT');
        $davidLoes->setEnabled(1);
        $davidLoesToken = $accessTokenService->createUserAccessToken($davidLoes);
        $davidLoes->setAccessToken($davidLoesToken);

        $manager->persist($davidLoes);
        $manager->flush();
        $this->addReference('david-loes-super-admin', $davidLoes);


        // Svetlana Kovalchuk
        $svetlanaKovalchuk = new User();
        $svetlanaKovalchuk->setFirstName('Svetlana');
        $svetlanaKovalchuk->setLastName('Kovalchuk');
        $svetlanaKovalchuk->setEmail('svetlana.kovalchuk@idapgroup.com');
        $svetlanaKovalchuk->setPlainPassword('111111');
        $svetlanaKovalchuk->addRole('ROLE_SUPER_ADMIN');
        $svetlanaKovalchuk->setEnabled(1);
        $svetlanaToken = $accessTokenService->createUserAccessToken($svetlanaKovalchuk);
        $svetlanaKovalchuk->setAccessToken($svetlanaToken);

        $manager->persist($svetlanaKovalchuk);
        $manager->flush();
        $this->addReference('svetlana-kovalchuk-super-admin', $svetlanaKovalchuk);


        //Yurii Trokhymchuk
        $yuriiTrokhymchuk = new User();
        $yuriiTrokhymchuk->setFirstName('Yurii');
        $yuriiTrokhymchuk->setLastName('Trokhymchuk');
        $yuriiTrokhymchuk->setEmail('yurii@test.com');
        $yuriiTrokhymchuk->setPlainPassword('qqqqqq');
        $yuriiTrokhymchuk->addRole('ROLE_SUPER_ADMIN');
        $yuriiTrokhymchuk->setEnabled(1);
        $yuriiToken = $accessTokenService->createUserAccessToken($yuriiTrokhymchuk);
        $yuriiTrokhymchuk->setAccessToken($yuriiToken);

        $manager->persist($yuriiTrokhymchuk);
        $manager->flush();
        $this->addReference('yurii-trokhymchuk-super-admin', $yuriiTrokhymchuk);


        //Vlad Emets
        $vladEmets = new User();
        $vladEmets->setFirstName('Vlad');
        $vladEmets->setLastName('Emets');
        $vladEmets->setEmail('vlad@test.com');
        $vladEmets->setPlainPassword('qqqqqq');
        $vladEmets->addRole('ROLE_SUPER_ADMIN');
        $vladEmets->setEnabled(1);
        $vladEmetsToken = $accessTokenService->createUserAccessToken($vladEmets);
        $vladEmets->setAccessToken($vladEmetsToken);

        $manager->persist($vladEmets);
        $manager->flush();
        $this->addReference('vlad-emets-super-admin', $vladEmets);

        //nosik
        $nosik = new User();
        $nosik->setUsername('nosik@test.com');
        $nosik->setEmail('nosik@test.com');
        $nosik->setPlainPassword('qqqqqq');
        $nosik->addRole('ROLE_SUPER_ADMIN');
        $nosik->setEnabled(1);
        $nosikToken = $accessTokenService->createUserAccessToken($nosik);
        $nosik->setAccessToken($nosikToken);

        $manager->persist($nosik);
        $manager->flush();
        $this->addReference('nosik-super-admin', $nosik);

        //nikityuk
        $nikityuk = new User();
        $nikityuk->setUsername('nikityuk@test.com');
        $nikityuk->setEmail('nikityuk@test.com');
        $nikityuk->setPlainPassword('qqqqqq');
        $nikityuk->addRole('ROLE_SUPER_ADMIN');
        $nikityuk->setEnabled(1);
        $nikityukToken = $accessTokenService->createUserAccessToken($nikityuk);
        $nikityuk->setAccessToken($nikityukToken);

        $manager->persist($nikityuk);
        $manager->flush();
        $this->addReference('nikityuk-super-admin', $nikityuk);

        //leesogonich
        $leesogonich = new User();
        $leesogonich->setUsername('leesogonich@test.com');
        $leesogonich->setEmail('leesogonich@test.com');
        $leesogonich->setPlainPassword('qqqqqq');
        $leesogonich->addRole('ROLE_SUPER_ADMIN');
        $leesogonich->setEnabled(1);
        $leesogonichToken = $accessTokenService->createUserAccessToken($leesogonich);
        $leesogonich->setAccessToken($leesogonichToken);

        $manager->persist($leesogonich);
        $manager->flush();
        $this->addReference('leesogonich-super-admin', $leesogonich);

        /*************   TESTERS  *****************************************************/
        // Mark Zuckerberg
        $markZuckerberg = new User();
        $markZuckerberg->setFirstName('Mark');
        $markZuckerberg->setLastName('Zuckerberg');
        $markZuckerberg->setEmail('markzuck@gmail.com');
        $markZuckerberg->setPlainPassword('123456');
        $markZuckerberg->addRole('ROLE_SUPER_ADMIN');
        $markZuckerberg->addRole('ROLE_ADMIN');
        $markZuckerberg->addRole('ROLE_CONTACT');
        $markZuckerberg->setEnabled(1);
        $markZuckerbergToken = $accessTokenService->createUserAccessToken($markZuckerberg);
        $markZuckerberg->setAccessToken($markZuckerbergToken);

        $manager->persist($markZuckerberg);
        $manager->flush();
        $this->addReference('mark-zuckerberg-super-admin', $markZuckerberg);

        // Elon Musk
        $elonMusk = new User();
        $elonMusk->setFirstName('Mark');
        $elonMusk->setLastName('Elon');
        $elonMusk->setEmail('elonmusk@gmail.com');
        $elonMusk->setPlainPassword('123456');
        $elonMusk->addRole('ROLE_SUPER_ADMIN');
        $elonMusk->addRole('ROLE_ADMIN');
        $elonMusk->addRole('ROLE_CONTACT');
        $elonMusk->setEnabled(1);
        $elonMuskToken = $accessTokenService->createUserAccessToken($elonMusk);
        $elonMusk->setAccessToken($elonMuskToken);

        $manager->persist($elonMusk);
        $manager->flush();
        $this->addReference('elon-musk-super-admin', $elonMusk);

        // Bill Gates
        $billGates = new User();
        $billGates->setFirstName('Bill');
        $billGates->setLastName('Gates');
        $billGates->setEmail('billgates@gmail.com');
        $billGates->setPlainPassword('123456');
        $billGates->addRole('ROLE_SUPER_ADMIN');
        $billGates->addRole('ROLE_ADMIN');
        $billGates->addRole('ROLE_CONTACT');
        $billGates->setEnabled(1);
        $billGatesToken = $accessTokenService->createUserAccessToken($billGates);
        $billGates->setAccessToken($billGatesToken);

        $manager->persist($billGates);
        $manager->flush();
        $this->addReference('bill-gates-super-admin', $billGates);


        // Sergey Brin
        $sergeyBrin = new User();
        $sergeyBrin->setFirstName('Sergey');
        $sergeyBrin->setLastName('Brin');
        $sergeyBrin->setEmail('sergeybrin@gmail.com');
        $sergeyBrin->setPlainPassword('123456');
        $sergeyBrin->addRole('ROLE_SUPER_ADMIN');
        $sergeyBrin->addRole('ROLE_ADMIN');
        $sergeyBrin->addRole('ROLE_CONTACT');
        $sergeyBrin->setEnabled(1);
        $sergeyBrinToken = $accessTokenService->createUserAccessToken($sergeyBrin);
        $sergeyBrin->setAccessToken($sergeyBrinToken);

        $manager->persist($sergeyBrin);
        $manager->flush();
        $this->addReference('sergey-brin-super-admin', $sergeyBrin);

        // Gabe Newell
        $gabeNewell = new User();
        $gabeNewell->setFirstName('Gabe');
        $gabeNewell->setLastName('Newell');
        $gabeNewell->setEmail('gabenewell@gmail.com');
        $gabeNewell->setPlainPassword('111111');
        $gabeNewell->addRole('ROLE_SUPER_ADMIN');
        $gabeNewell->addRole('ROLE_ADMIN');
        $gabeNewell->addRole('ROLE_CONTACT');
        $gabeNewell->setEnabled(1);
        $gabeNewellToken = $accessTokenService->createUserAccessToken($gabeNewell);
        $gabeNewell->setAccessToken($gabeNewellToken);

        $manager->persist($gabeNewell);
        $manager->flush();
        $this->addReference('gabe-newell-super-admin', $gabeNewell);


        // Jak Fresko
        $jakFresko = new User();
        $jakFresko->setFirstName('Jak');
        $jakFresko->setLastName('Fresko');
        $jakFresko->setEmail('jakfresko@gmail.com');
        $jakFresko->setPlainPassword('123456');
        $jakFresko->addRole('ROLE_SUPER_ADMIN');
        $jakFresko->addRole('ROLE_ADMIN');
        $jakFresko->addRole('ROLE_CONTACT');
        $jakFresko->setEnabled(1);
        $jakFreskoToken = $accessTokenService->createUserAccessToken($jakFresko);
        $jakFresko->setAccessToken($jakFreskoToken);

        $manager->persist($jakFresko);
        $manager->flush();
        $this->addReference('jak-fresko-super-admin', $jakFresko);


        /** ************************************************************ **/

        // Vdovychenko Dmytro
        $vdovychenkoDmytro = new User();
        $vdovychenkoDmytro->setFirstName('Dmytro');
        $vdovychenkoDmytro->setLastName('Vdovychenko');
        $vdovychenkoDmytro->setEmail('vdovychenko.dmytro@gmail.com');
        $vdovychenkoDmytro->setPlainPassword('111111');
        $vdovychenkoDmytro->addRole('ROLE_SUPER_ADMIN');
        $vdovychenkoDmytro->setEnabled(1);
        $vdovychenkoDmytroToken = $accessTokenService->createUserAccessToken($vdovychenkoDmytro);
        $vdovychenkoDmytro->setAccessToken($vdovychenkoDmytroToken);

        $manager->persist($vdovychenkoDmytro);
        $manager->flush();
        $this->addReference('vdovychenko-dmytro-super-admin', $vdovychenkoDmytro);


        // Maksym Makartsov
        $maksymMakartsov = new User();
        $maksymMakartsov->setFirstName('Maksym');
        $maksymMakartsov->setLastName('Makartsov');
        $maksymMakartsov->setEmail('maksym.makartsov@idapgroup.com');
        $maksymMakartsov->setPlainPassword('111111');
        $maksymMakartsov->addRole('ROLE_SUPER_ADMIN');
        $maksymMakartsov->setEnabled(1);
        $maksymMakartsovToken = $accessTokenService->createUserAccessToken($maksymMakartsov);
        $maksymMakartsov->setAccessToken($maksymMakartsovToken);

        $manager->persist($maksymMakartsov);
        $manager->flush();
        $this->addReference('maksym-makartsov-super-admin', $maksymMakartsov);
    }

    public function getOrder()
    {
        return 1;
    }
}
