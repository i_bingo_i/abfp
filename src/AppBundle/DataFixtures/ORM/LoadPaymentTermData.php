<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\PaymentTerm;

class LoadPaymentTermData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $fifteenDaysPaymentTerm = new PaymentTerm();
        $fifteenDaysPaymentTerm->setName("Net 15 days. Our price is firm for 30 days.");
        $fifteenDaysPaymentTerm->setAlias("net_fifteen_days");
        $manager->persist($fifteenDaysPaymentTerm);
        $manager->flush();
        $this->addReference('fifteen-days-payment-term', $fifteenDaysPaymentTerm);


        $thirtyDaysPaymentTerm = new PaymentTerm();
        $thirtyDaysPaymentTerm->setName("Net 30 days. Our price is firm for 30 days.");
        $thirtyDaysPaymentTerm->setAlias("net_thirty_days");
        $manager->persist($thirtyDaysPaymentTerm);
        $manager->flush();
        $this->addReference('thirty-days-payment-term', $thirtyDaysPaymentTerm);


        $codPaymentTerm = new PaymentTerm();
        $codPaymentTerm->setName("COD");
        $codPaymentTerm->setAlias("cod");
        $manager->persist($codPaymentTerm);
        $manager->flush();
        $this->addReference('cod-payment-term', $codPaymentTerm);


        $fullPaidBeforeStartedPaymentTerm = new PaymentTerm();
        $fullPaidBeforeStartedPaymentTerm->setName("Paid in full before work can be started.");
        $fullPaidBeforeStartedPaymentTerm->setAlias("full_paid_before_started");
        $manager->persist($fullPaidBeforeStartedPaymentTerm);
        $manager->flush();
        $this->addReference('full-paid-before-started-payment-term', $fullPaidBeforeStartedPaymentTerm);


        $fiftyPercentDepositPaymentTerm = new PaymentTerm();
        $fiftyPercentDepositPaymentTerm->setName("50% Deposit Die Before Work Can Start.");
        $fiftyPercentDepositPaymentTerm->setAlias("fifty_percent_deposit");
        $manager->persist($fiftyPercentDepositPaymentTerm);
        $manager->flush();
        $this->addReference('fifty-percent-deposit-payment-term', $fiftyPercentDepositPaymentTerm);

    }

    public function getOrder()
    {
        return 1;
    }
}
