<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Company;

class LoadCompanyData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $companyOne = new Company();
        $companyOne->setName("Company One");
        $companyOne->setAddress($this->getReference("address-for-company-one"));
        $companyOne->setWebsite("google.com");
        $companyOne->setNotes("Notes for Company one loren ipsum notes for Company one loren ipsum");

        $manager->persist($companyOne);
        $manager->flush();
        $this->addReference('company-one', $companyOne);


        $companyTwo = new Company();
        $companyTwo->setName("Another Company Two");
        $companyTwo->setAddress($this->getReference("address-for-company-two"));
        $companyTwo->setWebsite("yandex.com");
        $companyTwo->setNotes("Notes for Company two notes notes notes notes notes notes notes notes notes notes");

        $manager->persist($companyTwo);
        $manager->flush();
        $this->addReference('company-two', $companyTwo);


        $companyThree = new Company();
        $companyThree->setName("Very Good Company Three");
        $companyThree->setAddress($this->getReference("address-for-company-three"));
        $companyThree->setWebsite("mail.ru");
        $companyThree->setNotes("Company Three notes notes notes notes notes ");

        $manager->persist($companyThree);
        $manager->flush();
        $this->addReference('company-three', $companyThree);


        $companyFour = new Company();
        $companyFour->setName("Grate Company Four");
        $companyFour->setAddress($this->getReference("address-for-company-four"));
        $companyFour->setWebsite("yahoo.com");
        $companyFour->setNotes("notes notes notes notes notes notes notes notes notes notes notes notes notes notes");

        $manager->persist($companyFour);
        $manager->flush();
        $this->addReference('company-four', $companyFour);


        $companyMcDonalds = new Company();
        $companyMcDonalds->setName("McDonalds");
        $companyMcDonalds->setAddress($this->getReference("address-for-company-mc-donalds"));
        $companyMcDonalds->setWebsite("https://www.mcdonalds.ua/ua.html");
        $companyMcDonalds->setNotes("Cool company");

        $manager->persist($companyMcDonalds);
        $manager->flush();
        $this->addReference('company-mcdonalds', $companyMcDonalds);
    }

    public function getOrder()
    {
        return 5;
    }
}
