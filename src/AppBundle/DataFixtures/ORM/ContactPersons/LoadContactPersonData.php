<?php
namespace AppBundle\DataFixtures\ORM\ContactPersons;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContactPerson;

class LoadContactPersonData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contactPersonTest1 = new ContactPerson();
        $contactPersonTest1->setFirstName("Eddard");
        $contactPersonTest1->setLastName("Stark");
        $contactPersonTest1->setEmail("eddard.stark@gmail.com");
        $contactPersonTest1->setCod(true);
        $contactPersonTest1->setTitle("Title for Contact Person");
        $contactPersonTest1->setCell("1212121211");
        $contactPersonTest1->setPhone("8905671221");
        $contactPersonTest1->setExt("003");
        $contactPersonTest1->setFax("3434346767");
        $contactPersonTest1->setNotes("sdsd sdsdsee rerhrrhrtrt kldfkjjgdlfhgu fgf fgderew vccx");
        $contactPersonTest1->setPersonalAddress($this->getReference('address-one'));

        $manager->persist($contactPersonTest1);
        $manager->flush();
        $this->addReference('contact-person-test-one', $contactPersonTest1);


        $contactPersonTest2 = new ContactPerson();
        $contactPersonTest2->setFirstName("Robb");
        $contactPersonTest2->setLastName("Stark");
        $contactPersonTest2->setEmail("robb.stark@gmail.com");
        $contactPersonTest2->setCod(true);
        $contactPersonTest2->setTitle("Contact Persons title");
        $contactPersonTest2->setCell("3434346767");
        $contactPersonTest2->setPhone("4547765555");
        $contactPersonTest2->setExt("123");
        $contactPersonTest2->setFax("5643433423");
        $contactPersonTest2->setNotes("wwwwww wwwww www w ww w w wwww");
        $contactPersonTest2->setPersonalAddress($this->getReference('address-one-empty'));

        $manager->persist($contactPersonTest2);
        $manager->flush();
        $this->addReference('contact-person-test-two', $contactPersonTest2);


        $contactPersonTest3 = new ContactPerson();
        $contactPersonTest3->setFirstName("Tyrion");
        $contactPersonTest3->setLastName("Lannister");
        $contactPersonTest3->setEmail("tyrion.lannister@gmail.com");
        $contactPersonTest3->setCod(false);
        $contactPersonTest3->setTitle("Contact Persons title test");
        $contactPersonTest3->setCell("5672440987");
        $contactPersonTest3->setPhone("3435671234");
        $contactPersonTest3->setExt("333");
        $contactPersonTest3->setFax("3437774444");
        $contactPersonTest3->setNotes("sdsdsd rrt");
        $contactPersonTest3->setCompany($this->getReference("company-two"));
        $contactPersonTest3->setPersonalAddress($this->getReference('address-two-empty'));

        $manager->persist($contactPersonTest3);
        $manager->flush();
        $this->addReference('contact-person-test-three', $contactPersonTest3);


        $contactPersonTest4 = new ContactPerson();
        $contactPersonTest4->setFirstName("Petyr");
        $contactPersonTest4->setLastName("Bealish");
        $contactPersonTest4->setEmail("petyr.bealish@gmail.com");
        $contactPersonTest4->setCod(false);
        $contactPersonTest4->setTitle("Contact test Persons title");
        $contactPersonTest4->setCell("3432222323");
        $contactPersonTest4->setPhone("5671215657");
        $contactPersonTest4->setExt("123");
        $contactPersonTest4->setFax("2137779765");
        $contactPersonTest4->setNotes("wwwwww wwwww www w ww w w wwww");
        $contactPersonTest4->setPersonalAddress($this->getReference('address-two'));

        $manager->persist($contactPersonTest4);
        $manager->flush();
        $this->addReference('contact-person-test-four', $contactPersonTest4);


        $contactPersonTest5 = new ContactPerson();
        $contactPersonTest5->setFirstName("Tony");
        $contactPersonTest5->setLastName("Stark");
        $contactPersonTest5->setEmail("tony.stark@gmail.com");
        $contactPersonTest5->setCod(false);
        $contactPersonTest5->setTitle("Contact Persons title");
        $contactPersonTest5->setCell("4562347890");
        $contactPersonTest5->setPhone("3432140345");
        $contactPersonTest5->setExt("123");
        $contactPersonTest5->setFax("5563455677");
        $contactPersonTest5->setNotes("wwwwww 1212 sdsd test dsdsd");
        $contactPersonTest5->setPersonalAddress($this->getReference('address-three'));

        $manager->persist($contactPersonTest5);
        $manager->flush();
        $this->addReference('contact-person-test-five', $contactPersonTest5);


        $contactPersonDeleted1 = new ContactPerson();
        $contactPersonDeleted1->setFirstName("Valentin");
        $contactPersonDeleted1->setLastName("Strykhalo");
        $contactPersonDeleted1->setEmail("valentin.strykhalo@test.com");
        $contactPersonDeleted1->setCod(false);
        $contactPersonDeleted1->setTitle("Contact Persons deleted 1");
        $contactPersonDeleted1->setCell("4215670000");
        $contactPersonDeleted1->setPhone("4454324567");
        $contactPersonDeleted1->setExt("159");
        $contactPersonDeleted1->setFax("4547873245");
        $contactPersonDeleted1->setNotes("some notes for deleted contact person 1");
        $contactPersonDeleted1->setDeleted(true);
        $contactPersonDeleted1->setPersonalAddress($this->getReference('address-four'));

        $manager->persist($contactPersonDeleted1);
        $manager->flush();
        $this->addReference('contact-person-deleted-1', $contactPersonDeleted1);


        $contactPersonDeleted2 = new ContactPerson();
        $contactPersonDeleted2->setFirstName("Nikola");
        $contactPersonDeleted2->setLastName("Tesla");
        $contactPersonDeleted2->setEmail("nikola.tesla@test.com");
        $contactPersonDeleted2->setCod(true);
        $contactPersonDeleted2->setTitle("Contact Persons deleted 2");
        $contactPersonDeleted2->setCell("1126531278");
        $contactPersonDeleted2->setPhone("3330964321");
        $contactPersonDeleted2->setExt("741");
        $contactPersonDeleted2->setFax("6782346789");
        $contactPersonDeleted2->setNotes("some notes for deleted contact person 2 some notes for deleted contact person 2 some notes for deleted contact person 2");
        $contactPersonDeleted2->setDeleted(true);
        $contactPersonDeleted2->setPersonalAddress($this->getReference('address-five-empty'));

        $manager->persist($contactPersonDeleted2);
        $manager->flush();
        $this->addReference('contact-person-deleted-2', $contactPersonDeleted2);

        $conflictContactPerson = new ContactPerson();
        $conflictContactPerson->setFirstName("Mailing Address Conflict!");
        $conflictContactPerson->setNotes("Some of the sites in the old system under this Group Account (formerly a Master) were using Master Address as mailing address to send retest notices.");

        $manager->persist($conflictContactPerson);
        $manager->flush();
        $this->addReference('conflict-contact-person', $conflictContactPerson);
    }

    public function getOrder()
    {
        return 6;
    }
}
