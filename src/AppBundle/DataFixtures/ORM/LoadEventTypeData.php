<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\EventType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadEventTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $workorder = new EventType();
        $workorder->setName('Workorder');
        $workorder->setAlias('workorder');
        $manager->persist($workorder);
        $manager->flush();

        $this->addReference('event-type-workorder', $workorder);

        $timeOff = new EventType();
        $timeOff->setName('Time off');
        $timeOff->setAlias('time_off');
        $manager->persist($timeOff);
        $manager->flush();

        $this->addReference('event-type-time-off', $timeOff);
    }

    public function getOrder()
    {
        return 1;
    }
}
