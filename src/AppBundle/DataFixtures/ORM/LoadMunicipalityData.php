<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Municipality;

class LoadMunicipalityData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $municipalityNY = new Municipality();
        $municipalityNY->setName('City of New York');
        $municipalityNY->setWebsite('http://www1.nyc.gov/');
        $municipalityNY->addContact($this->getReference('contact-five'));
        $municipalityNY->addContact($this->getReference('contact-six'));

        $manager->persist($municipalityNY);
        $manager->flush();
        $this->addReference('municipality-new-york', $municipalityNY);


        $municipalityChicago = new Municipality();
        $municipalityChicago->setName('City of Kyiv');
        $municipalityChicago->setWebsite('https://kyivcity.gov.ua/');
        $municipalityChicago->addContact($this->getReference('contact-four'));

        $manager->persist($municipalityChicago);
        $manager->flush();
        $this->addReference('municipality-chicago', $municipalityChicago);


        $municipalityPortland = new Municipality();
        $municipalityPortland->setName('City of Portland');
        $municipalityPortland->setWebsite('http://www.cityofportland.org');

        $manager->persist($municipalityPortland);
        $manager->flush();
        $this->addReference('municipality-portland', $municipalityPortland);
    }

    public function getOrder()
    {
        return 4;
    }
}
