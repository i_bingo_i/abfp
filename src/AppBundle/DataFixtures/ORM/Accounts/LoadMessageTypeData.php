<?php

namespace AppBundle\DataFixtures\ORM\Accounts;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\MessageType;

class LoadMessageTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //error
        $errorMessageType = new MessageType();
        $errorMessageType->setName('Error');
        $errorMessageType->setAlias('error');
        $manager->persist($errorMessageType);
        $manager->flush();

        $this->addReference('message-type-error', $errorMessageType);

        //warning
        $warningMessageType = new MessageType();
        $warningMessageType->setName('Warning');
        $warningMessageType->setAlias('warning');
        $manager->persist($warningMessageType);
        $manager->flush();

        $this->addReference('message-type-warning', $warningMessageType);
    }

    public function getOrder()
    {
        return 1;
    }
}
