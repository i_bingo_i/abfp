<?php

namespace AppBundle\DataFixtures\ORM\Accounts;

use AppBundle\Entity\AccountBuildingType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAccountBuildingTypeData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $accountBuildingTypeResidential = new AccountBuildingType();
        $accountBuildingTypeResidential->setName('Residential');
        $accountBuildingTypeResidential->setAlias('residential');
        $manager->persist($accountBuildingTypeResidential);
        $manager->flush();


        $accountBuildingTypeCommercial = new AccountBuildingType();
        $accountBuildingTypeCommercial->setName('Commercial');
        $accountBuildingTypeCommercial->setAlias('commercial');
        $manager->persist($accountBuildingTypeCommercial);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}