<?php

namespace AppBundle\DataFixtures\ORM\Accounts;

use AppBundle\Entity\AccountHistory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAccountHistoryData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $accountOne = new AccountHistory();
        $accountOne->setName("Account one");
        $accountOne->setAddress($this->getReference('address-for-account-one'));
        $accountOne->setNotes('Note: test');
        $accountOne->setType($this->getReference('account-type-account'));
        $accountOne->setClientType($this->getReference('client-type-active'));
        $accountOne->setOwnerEntity($this->getReference('account-one'));
        $accountOne->setDateCreate(new \DateTime());
        $accountOne->setDateUpdate(new \DateTime());
        $accountOne->setDateSave(new \DateTime());

        $manager->persist($accountOne);
        $manager->flush();
        $this->addReference('account-one-history', $accountOne);


        $accountGroupOne = new AccountHistory();
        $accountGroupOne->setName("Group account one");
        $accountGroupOne->setAddress($this->getReference('address-for-group-account-one'));
        $accountGroupOne->setNotes('Note: test');
        $accountGroupOne->setType($this->getReference('account-type-group-account'));
        $accountGroupOne->setClientType($this->getReference('client-type-inactive'));
        $accountGroupOne->setOwnerEntity($this->getReference('group-account-one'));
        $accountGroupOne->setDateCreate(new \DateTime());
        $accountGroupOne->setDateUpdate(new \DateTime());
        $accountGroupOne->setDateSave(new \DateTime());

        $manager->persist($accountGroupOne);
        $manager->flush();
        $this->addReference('group-account-one-history', $accountGroupOne);


        $accountTwo = new AccountHistory();
        $accountTwo->setName("Account two");
        $accountTwo->setAddress($this->getReference('address-for-account-two'));
        $accountTwo->setNotes('Note: test');
        $accountTwo->setType($this->getReference('account-type-account'));
        $accountTwo->setParent($this->getReference('group-account-one'));
        $accountTwo->setClientType($this->getReference('client-type-inactive'));
        $accountTwo->setOwnerEntity($this->getReference('account-two'));
        $accountTwo->setDateCreate(new \DateTime());
        $accountTwo->setDateUpdate(new \DateTime());
        $accountTwo->setDateSave(new \DateTime());

        $manager->persist($accountTwo);
        $manager->flush();
        $this->addReference('account-two-history', $accountTwo);


        $accountThree = new AccountHistory();
        $accountThree->setName("Account three");
        $accountThree->setAddress($this->getReference('address-for-account-three'));
        $accountThree->setNotes('Note: test');
        $accountThree->setType($this->getReference('account-type-account'));
        $accountThree->setParent($this->getReference('group-account-one'));
        $accountThree->setClientType($this->getReference('client-type-inactive'));
        $accountThree->setOwnerEntity($this->getReference('account-three'));
        $accountThree->setDateCreate(new \DateTime());
        $accountThree->setDateUpdate(new \DateTime());
        $accountThree->setDateSave(new \DateTime());

        $manager->persist($accountThree);
        $manager->flush();
        $this->addReference('account-three-history', $accountThree);


        $accountFive = new AccountHistory();
        $accountFive->setName("Account five");
        $accountFive->setAddress($this->getReference('address-for-account-five'));
        $accountFive->setNotes('Some notes');
        $accountFive->setType($this->getReference('account-type-group-account'));
        $accountFive->setClientType($this->getReference('client-type-potential'));
        $accountFive->setOwnerEntity($this->getReference('account-five'));
        $accountFive->setDateCreate(new \DateTime());
        $accountFive->setDateUpdate(new \DateTime());
        $accountFive->setDateSave(new \DateTime());

        $manager->persist($accountFive);
        $manager->flush();
        $this->addReference('account-five-history', $accountFive);


        $accountMcDonalds = new AccountHistory();
        $accountMcDonalds->setName("McDonalds");
        $accountMcDonalds->setAddress($this->getReference('address-for-account-mc-donalds'));
        $accountMcDonalds->setNotes('');
        $accountMcDonalds->setType($this->getReference('account-type-group-account'));
        $accountMcDonalds->setClientType($this->getReference('client-type-inactive'));
        $accountMcDonalds->setOwnerEntity($this->getReference('account-mcdonalds'));
        $accountMcDonalds->setDateCreate(new \DateTime());
        $accountMcDonalds->setDateUpdate(new \DateTime());
        $accountMcDonalds->setDateSave(new \DateTime());
        $accountMcDonalds->setWebsite('http://corporate.mcdonalds.com/mcd.html');

        $manager->persist($accountMcDonalds);
        $manager->flush();
        $this->addReference('account-mcdonalds-history', $accountMcDonalds);


        $accountNoName = new AccountHistory();
        $accountNoName->setName("/\ccount##1");
        $accountNoName->setAddress($this->getReference('address-for-account-no-name'));
        $accountNoName->setNotes('');
        $accountNoName->setType($this->getReference('account-type-account'));
        $accountNoName->setClientType($this->getReference('client-type-active'));
        $accountNoName->setOwnerEntity($this->getReference('account-noname'));
        $accountNoName->setDateCreate(new \DateTime());
        $accountNoName->setDateUpdate(new \DateTime());
        $accountNoName->setDateSave(new \DateTime());

        $manager->persist($accountNoName);
        $manager->flush();
        $this->addReference('account-noname-history', $accountNoName);


        $accountSix = new AccountHistory();
        $accountSix->setName("Account six");
        $accountSix->setAddress($this->getReference('address-for-account-six'));
        $accountSix->setNotes('qwerty!@#$&*()_');
        $accountSix->setType($this->getReference('account-type-account'));
        $accountSix->setClientType($this->getReference('client-type-active'));
        $accountSix->setOwnerEntity($this->getReference('account-six'));
        $accountSix->setDateCreate(new \DateTime());
        $accountSix->setDateUpdate(new \DateTime());
        $accountSix->setDateSave(new \DateTime());

        $manager->persist($accountSix);
        $manager->flush();
        $this->addReference('account-six-history', $accountSix);


        $accountSeven = new AccountHistory();
        $accountSeven->setName("Account seven");
        $accountSeven->setAddress($this->getReference('address-for-account-seven'));
        $accountSeven->setNotes('Illinois');
        $accountSeven->setType($this->getReference('account-type-account'));
        $accountSeven->setClientType($this->getReference('client-type-active'));
        $accountSeven->setOwnerEntity($this->getReference('account-seven'));
        $accountSeven->setDateCreate(new \DateTime());
        $accountSeven->setDateUpdate(new \DateTime());
        $accountSeven->setDateSave(new \DateTime());

        $manager->persist($accountSeven);
        $manager->flush();
        $this->addReference('account-seven-history', $accountSeven);


        $accountTest = new AccountHistory();
        $accountTest->setName("Test");
        $accountTest->setAddress($this->getReference('address-for-account-test'));
        $accountTest->setNotes('2342342');
        $accountTest->setType($this->getReference('account-type-group-account'));
        $accountTest->setClientType($this->getReference('client-type-inactive'));
        $accountTest->setOwnerEntity($this->getReference('account-test'));
        $accountTest->setDateCreate(new \DateTime());
        $accountTest->setDateUpdate(new \DateTime());
        $accountTest->setDateSave(new \DateTime());

        $manager->persist($accountTest);
        $manager->flush();
        $this->addReference('account-test-history', $accountTest);


        $account10 = new AccountHistory();
        $account10->setName("Account 10");
        $account10->setAddress($this->getReference('address-for-account-10'));
        $account10->setNotes('Test Note _ ( ) ^ dfmsisugvj dewqjk ehferwh hfekr		');
        $account10->setType($this->getReference('account-type-group-account'));
        $account10->setClientType($this->getReference('client-type-inactive'));
        $account10->setOwnerEntity($this->getReference('account-10'));
        $account10->setDateCreate(new \DateTime());
        $account10->setDateUpdate(new \DateTime());
        $account10->setDateSave(new \DateTime());

        $manager->persist($account10);
        $manager->flush();
        $this->addReference('account-10-history', $account10);


        $account89 = new AccountHistory();
        $account89->setName("Account frue89");
        $account89->setAddress($this->getReference('address-for-account-89'));
        $account89->setNotes('qwerty!@#$&*()_');
        $account89->setType($this->getReference('account-type-group-account'));
        $account89->setClientType($this->getReference('client-type-active'));
        $account89->setOwnerEntity($this->getReference('account-89'));
        $account89->setDateCreate(new \DateTime());
        $account89->setDateUpdate(new \DateTime());
        $account89->setDateSave(new \DateTime());

        $manager->persist($account89);
        $manager->flush();
        $this->addReference('account-89-history', $account89);


        $accountNine = new AccountHistory();
        $accountNine->setName("Account nine");
        $accountNine->setAddress($this->getReference('address-for-account-nine'));
        $accountNine->setNotes('Illinois');
        $accountNine->setType($this->getReference('account-type-account'));
        $accountNine->setClientType($this->getReference('client-type-active'));
        $accountNine->setOwnerEntity($this->getReference('account-nine'));
        $accountNine->setDateCreate(new \DateTime());
        $accountNine->setDateUpdate(new \DateTime());
        $accountNine->setDateSave(new \DateTime());

        $manager->persist($accountNine);
        $manager->flush();
        $this->addReference('account-nine-history', $accountNine);


        $accountAcc = new AccountHistory();
        $accountAcc->setName("Acc");
        $accountAcc->setAddress($this->getReference('address-for-account-acc'));
        $accountAcc->setNotes('defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ');
        $accountAcc->setType($this->getReference('account-type-account'));
        $accountAcc->setClientType($this->getReference('client-type-active'));
        $accountAcc->setOwnerEntity($this->getReference('account-acc'));
        $accountAcc->setDateCreate(new \DateTime());
        $accountAcc->setDateUpdate(new \DateTime());
        $accountAcc->setDateSave(new \DateTime());

        $manager->persist($accountAcc);
        $manager->flush();
        $this->addReference('account-acc-history', $accountAcc);


        $accountBfh = new AccountHistory();
        $accountBfh->setName("BFH");
        $accountBfh->setAddress($this->getReference('address-for-account-bfh'));
        $accountBfh->setNotes('defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ');
        $accountBfh->setType($this->getReference('account-type-account'));
        $accountBfh->setClientType($this->getReference('client-type-active'));
        $accountBfh->setOwnerEntity($this->getReference('account-bfh'));
        $accountBfh->setDateCreate(new \DateTime());
        $accountBfh->setDateUpdate(new \DateTime());
        $accountBfh->setDateSave(new \DateTime());

        $manager->persist($accountBfh);
        $manager->flush();
        $this->addReference('account-bfh-history', $accountBfh);


        $accountDeletedOne = new AccountHistory();
        $accountDeletedOne->setName("Deleted Account");
        $accountDeletedOne->setAddress($this->getReference('address-for-account-deleted-one'));
        $accountDeletedOne->setNotes('Some notes for account some notes for account some notes for account some notes for account');
        $accountDeletedOne->setType($this->getReference('account-type-account'));
        $accountDeletedOne->setClientType($this->getReference('client-type-active'));
        $accountDeletedOne->setOwnerEntity($this->getReference('account-deleted-one'));
        $accountDeletedOne->setDateCreate(new \DateTime());
        $accountDeletedOne->setDateUpdate(new \DateTime());
        $accountDeletedOne->setDateSave(new \DateTime());

        $manager->persist($accountDeletedOne);
        $manager->flush();
        $this->addReference('account-deleted-one-history', $accountDeletedOne);


        $accountDeletedTwo = new AccountHistory();
        $accountDeletedTwo->setName("Another Deleted Account");
        $accountDeletedTwo->setAddress($this->getReference('address-for-account-deleted-two'));
        $accountDeletedTwo->setNotes('Some notes for account some notes for account some notes for account some notes for account');
        $accountDeletedTwo->setType($this->getReference('account-type-group-account'));
        $accountDeletedTwo->setClientType($this->getReference('client-type-active'));
        $accountDeletedTwo->setOwnerEntity($this->getReference('account-deleted-two'));
        $accountDeletedTwo->setDateCreate(new \DateTime());
        $accountDeletedTwo->setDateUpdate(new \DateTime());
        $accountDeletedTwo->setDateSave(new \DateTime());

        $manager->persist($accountDeletedTwo);
        $manager->flush();
        $this->addReference('account-deleted-two-history', $accountDeletedTwo);

        $accountForContactPersonRolesChangingTesting = new AccountHistory();
        $accountForContactPersonRolesChangingTesting->setName('Account for changing ContactPersonRoles testing');
        $accountForContactPersonRolesChangingTesting->setAddress($this->getReference('address-for-account-for-changing-contact-person-roles'));
        $accountForContactPersonRolesChangingTesting->setNotes('account for changing contact person roles testing');
        $accountForContactPersonRolesChangingTesting->setType($this->getReference('account-type-account'));
        $accountForContactPersonRolesChangingTesting->setClientType($this->getReference('client-type-active'));
        $accountForContactPersonRolesChangingTesting->setOwnerEntity($this->getReference('account-for-changing-contact-person-roles-testing'));
        $accountForContactPersonRolesChangingTesting->setDateCreate(new \DateTime());
        $accountForContactPersonRolesChangingTesting->setDateUpdate(new \DateTime());
        $accountForContactPersonRolesChangingTesting->setDateSave(new \DateTime());

        $manager->persist($accountForContactPersonRolesChangingTesting);
        $manager->flush();
        $this->addReference(
            'account-for-changing-contact-person-roles-testing-history',
            $accountForContactPersonRolesChangingTesting
        );
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 6;
    }
}