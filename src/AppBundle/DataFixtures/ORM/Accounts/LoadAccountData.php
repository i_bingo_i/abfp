<?php
namespace AppBundle\DataFixtures\ORM\Accounts;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Account;

class LoadAccountData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $accountOne = new Account();
        $accountOne->setName("Account one");
        $accountOne->setAddress($this->getReference('address-for-account-one'));
        $accountOne->setNotes('Note: test');
        $accountOne->setType($this->getReference('account-type-account'));
        $accountOne->setClientType($this->getReference('client-type-active'));
        $accountOne->setMunicipality($this->getReference('municipality-chicago'));

        $manager->persist($accountOne);
        $manager->flush();
        $this->addReference('account-one', $accountOne);


        $accountGroupOne = new Account();
        $accountGroupOne->setName("Group account one");
        $accountGroupOne->setAddress($this->getReference('address-for-group-account-one'));
        $accountGroupOne->setNotes('Note: test');
        $accountGroupOne->setType($this->getReference('account-type-group-account'));
        $accountGroupOne->setClientType($this->getReference('client-type-inactive'));
        $accountGroupOne->setMunicipality($this->getReference('municipality-chicago'));
        $accountGroupOne->setOldestOpportunityDate(new \DateTime('-1 month'));
        $accountGroupOne->setOldestOpportunityFireDate(new \DateTime('-1 month'));
        $accountGroupOne->setOldestOpportunityBackflowDate(new \DateTime('+2 months'));
        $accountGroupOne->setOldSystemMasLevelAddress($this->getReference('special-old-system-address-for-account'));
        $accountGroupOne->addMessage($this->getReference('message-no-autorizer'));

        $manager->persist($accountGroupOne);
        $manager->flush();
        $this->addReference('group-account-one', $accountGroupOne);


        $accountTwo = new Account();
        $accountTwo->setName("Account two");
        $accountTwo->setAddress($this->getReference('address-for-account-two'));
        $accountTwo->setNotes('Note: test');
        $accountTwo->setType($this->getReference('account-type-account'));
        $accountTwo->setParent($accountGroupOne);
        $accountTwo->setClientType($this->getReference('client-type-inactive'));
        $accountTwo->setMunicipality($this->getReference('municipality-chicago'));
        $accountTwo->setOldestOpportunityDate(new \DateTime('+1 month'));
        $accountTwo->setOldestOpportunityFireDate(new \DateTime('+1 month'));

        $manager->persist($accountTwo);
        $manager->flush();
        $this->addReference('account-two', $accountTwo);


        $accountThree = new Account();
        $accountThree->setName("Account three");
        $accountThree->setAddress($this->getReference('address-for-account-three'));
        $accountThree->setNotes('Note: test');
        $accountThree->setType($this->getReference('account-type-account'));
        $accountThree->setParent($accountGroupOne);
        $accountThree->setClientType($this->getReference('client-type-inactive'));
        $accountThree->setMunicipality($this->getReference('municipality-chicago'));
        $accountThree->setOldestOpportunityDate(new \DateTime('-1 month'));
        $accountThree->setOldestOpportunityFireDate(new \DateTime('-1 month'));
        $accountThree->setOldestOpportunityBackflowDate(new \DateTime('+2 months'));

        $manager->persist($accountThree);
        $manager->flush();
        $this->addReference('account-three', $accountThree);


        $accountFive = new Account();
        $accountFive->setName("Account five");
        $accountFive->setAddress($this->getReference('address-for-account-five'));
        $accountFive->setNotes('Some notes');
        $accountFive->setType($this->getReference('account-type-group-account'));
        $accountFive->setClientType($this->getReference('client-type-potential'));
        $accountFive->setMunicipality($this->getReference('municipality-chicago'));

        $manager->persist($accountFive);
        $manager->flush();
        $this->addReference('account-five', $accountFive);


        $accountMcDonalds = new Account();
        $accountMcDonalds->setName("McDonalds");
        $accountMcDonalds->setAddress($this->getReference('address-for-account-mc-donalds'));
        $accountMcDonalds->setNotes('');
        $accountMcDonalds->setType($this->getReference('account-type-group-account'));
        $accountMcDonalds->setClientType($this->getReference('client-type-inactive'));
        $accountMcDonalds->setMunicipality($this->getReference('municipality-chicago'));
        $accountMcDonalds->setWebsite('http://corporate.mcdonalds.com/mcd.html');

        $manager->persist($accountMcDonalds);
        $manager->flush();
        $this->addReference('account-mcdonalds', $accountMcDonalds);


        $accountNoName = new Account();
        $accountNoName->setName("/\ccount##1");
        $accountNoName->setAddress($this->getReference('address-for-account-no-name'));
        $accountNoName->setNotes('');
        $accountNoName->setType($this->getReference('account-type-account'));
        $accountNoName->setClientType($this->getReference('client-type-active'));
        $accountNoName->setMunicipality($this->getReference('municipality-chicago'));

        $manager->persist($accountNoName);
        $manager->flush();
        $this->addReference('account-noname', $accountNoName);


        $accountSix = new Account();
        $accountSix->setName("Account six");
        $accountSix->setAddress($this->getReference('address-for-account-six'));
        $accountSix->setNotes('qwerty!@#$&*()_');
        $accountSix->setType($this->getReference('account-type-account'));
        $accountSix->setClientType($this->getReference('client-type-active'));
        $accountSix->setMunicipality($this->getReference('municipality-chicago'));

        $manager->persist($accountSix);
        $manager->flush();
        $this->addReference('account-six', $accountSix);


        $accountSeven = new Account();
        $accountSeven->setName("Account seven");
        $accountSeven->setAddress($this->getReference('address-for-account-seven'));
        $accountSeven->setNotes('Illinois');
        $accountSeven->setType($this->getReference('account-type-account'));
        $accountSeven->setClientType($this->getReference('client-type-active'));
        $accountSeven->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($accountSeven);
        $manager->flush();
        $this->addReference('account-seven', $accountSeven);


        $accountTest = new Account();
        $accountTest->setName("Test");
        $accountTest->setAddress($this->getReference('address-for-account-test'));
        $accountTest->setNotes('2342342');
        $accountTest->setType($this->getReference('account-type-group-account'));
        $accountTest->setClientType($this->getReference('client-type-inactive'));
        $accountTest->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($accountTest);
        $manager->flush();
        $this->addReference('account-test', $accountTest);


        $account10 = new Account();
        $account10->setName("Account 10");
        $account10->setAddress($this->getReference('address-for-account-10'));
        $account10->setNotes('Test Note _ ( ) ^ dfmsisugvj dewqjk ehferwh hfekr		');
        $account10->setType($this->getReference('account-type-group-account'));
        $account10->setClientType($this->getReference('client-type-inactive'));
        $account10->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($account10);
        $manager->flush();
        $this->addReference('account-10', $account10);


        $account89 = new Account();
        $account89->setName("Account frue89");
        $account89->setAddress($this->getReference('address-for-account-89'));
        $account89->setNotes('qwerty!@#$&*()_');
        $account89->setType($this->getReference('account-type-group-account'));
        $account89->setClientType($this->getReference('client-type-active'));
        $account89->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($account89);
        $manager->flush();
        $this->addReference('account-89', $account89);


        $accountNine = new Account();
        $accountNine->setName("Account nine");
        $accountNine->setAddress($this->getReference('address-for-account-nine'));
        $accountNine->setNotes('Illinois');
        $accountNine->setType($this->getReference('account-type-account'));
        $accountNine->setClientType($this->getReference('client-type-active'));
        $accountNine->setMunicipality($this->getReference('municipality-chicago'));

        $manager->persist($accountNine);
        $manager->flush();
        $this->addReference('account-nine', $accountNine);


        $accountAcc = new Account();
        $accountAcc->setName("Acc");
        $accountAcc->setAddress($this->getReference('address-for-account-acc'));
        $accountAcc->setNotes('defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ');
        $accountAcc->setType($this->getReference('account-type-account'));
        $accountAcc->setClientType($this->getReference('client-type-active'));
        $accountAcc->setMunicipality($this->getReference('municipality-chicago'));
        $accountAcc->setOldestOpportunityDate(new \DateTime());
        $accountAcc->setOldestOpportunityBackflowDate(new \DateTime());
        $accountAcc->setOldestOpportunityFireDate(new \DateTime());
        $accountAcc->addMessage($this->getReference('message-no-autorizer'));

        $manager->persist($accountAcc);
        $manager->flush();
        $this->addReference('account-acc', $accountAcc);


        $accountBfh = new Account();
        $accountBfh->setName("BFH");
        $accountBfh->setAddress($this->getReference('address-for-account-bfh'));
        $accountBfh->setNotes('defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ');
        $accountBfh->setType($this->getReference('account-type-account'));
        $accountBfh->setClientType($this->getReference('client-type-active'));
        $accountBfh->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($accountBfh);
        $manager->flush();
        $this->addReference('account-bfh', $accountBfh);


        $accountDeletedOne = new Account();
        $accountDeletedOne->setName("Deleted Account");
        $accountDeletedOne->setAddress($this->getReference('address-for-account-deleted-one'));
        $accountDeletedOne->setNotes('Some notes for account some notes for account some notes for account some notes for account');
        $accountDeletedOne->setType($this->getReference('account-type-account'));
        $accountDeletedOne->setClientType($this->getReference('client-type-active'));
        $accountDeletedOne->setDeleted(true);
        $accountDeletedOne->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($accountDeletedOne);
        $manager->flush();
        $this->addReference('account-deleted-one', $accountDeletedOne);


        $accountDeletedTwo = new Account();
        $accountDeletedTwo->setName("Another Deleted Account");
        $accountDeletedTwo->setAddress($this->getReference('address-for-account-deleted-two'));
        $accountDeletedTwo->setNotes('Some notes for account some notes for account some notes for account some notes for account');
        $accountDeletedTwo->setType($this->getReference('account-type-group-account'));
        $accountDeletedTwo->setClientType($this->getReference('client-type-active'));
        $accountDeletedTwo->setDeleted(true);
        $accountDeletedTwo->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($accountDeletedTwo);
        $manager->flush();
        $this->addReference('account-deleted-two', $accountDeletedTwo);

        $accountForContactPersonRolesChangingTesting = new Account();
        $accountForContactPersonRolesChangingTesting->setName('Account for changing ContactPersonRoles testing');
        $accountForContactPersonRolesChangingTesting->setAddress($this->getReference('address-for-account-for-changing-contact-person-roles'));
        $accountForContactPersonRolesChangingTesting->setNotes('account for changing contact person roles testing');
        $accountForContactPersonRolesChangingTesting->setType($this->getReference('account-type-account'));
        $accountForContactPersonRolesChangingTesting->setClientType($this->getReference('client-type-active'));
        $accountForContactPersonRolesChangingTesting->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($accountForContactPersonRolesChangingTesting);
        $manager->flush();
        $this->addReference(
            'account-for-changing-contact-person-roles-testing',
            $accountForContactPersonRolesChangingTesting
        );

        $accountBankOfNewYork = new Account();
        $accountBankOfNewYork->setName('BNY Mellon');
        $accountBankOfNewYork->setAddress($this->getReference('address-bank-of-new-york'));
        $accountBankOfNewYork->setType($this->getReference('account-type-account'));
        $accountBankOfNewYork->setClientType($this->getReference('client-type-active'));
        $accountBankOfNewYork->setMunicipality($this->getReference('municipality-new-york'));

        $manager->persist($accountBankOfNewYork);
        $manager->flush();
        $this->addReference('account-bny-mellon', $accountBankOfNewYork);
    }

    public function getOrder()
    {
        return 5;
    }
}
