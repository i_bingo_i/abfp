<?php
namespace AppBundle\DataFixtures\ORM\Accounts;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\AccountType;

class LoadAccountTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $accountTypeAccount = new AccountType();
        $accountTypeAccount->setName("Account");
        $accountTypeAccount->setAlias('account');

        $manager->persist($accountTypeAccount);
        $manager->flush();
        $this->addReference('account-type-account', $accountTypeAccount);


        $accountTypeGroupAccount = new AccountType();
        $accountTypeGroupAccount->setName("Group account");
        $accountTypeGroupAccount->setAlias('group account');

        $manager->persist($accountTypeGroupAccount);
        $manager->flush();
        $this->addReference('account-type-group-account', $accountTypeGroupAccount);
    }

    public function getOrder()
    {
        return 2;
    }
}
