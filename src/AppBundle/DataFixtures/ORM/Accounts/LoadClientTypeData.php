<?php
namespace AppBundle\DataFixtures\ORM\Accounts;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ClientType;

class LoadClientTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $clientTypeActive = new ClientType();
        $clientTypeActive->setName("Active");
        $clientTypeActive->setAlias('active');
        $manager->persist($clientTypeActive);
        $manager->flush();
        $this->addReference('client-type-active', $clientTypeActive);


        $clientTypeInactive = new ClientType();
        $clientTypeInactive->setName("Inactive");
        $clientTypeInactive->setAlias('inactive');
        $manager->persist($clientTypeInactive);
        $manager->flush();
        $this->addReference('client-type-inactive', $clientTypeInactive);

        $clientTypePotential = new ClientType();
        $clientTypePotential->setName("Potential");
        $clientTypePotential->setAlias('potential');
        $manager->persist($clientTypePotential);
        $manager->flush();
        $this->addReference('client-type-potential', $clientTypePotential);
    }

    public function getOrder()
    {
        return 1;
    }
}
