<?php

namespace AppBundle\DataFixtures\ORM\Accounts;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Message;

class LoadMessageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //no authoriser
        $message1 = new Message();
        $message1->setIcon('warning.svg');
        $message1->setMessage('Authorizer for some divisions under this account is missing');
        $message1->setAlias('no_autorizer');
        $message1->setType($this->getReference('message-type-error'));
        $manager->persist($message1);
        $manager->flush();

        $this->addReference('message-no-autorizer', $message1);

        //authoriser invalid address
        $message3 = new Message();
        $message3->setIcon('warning.svg');
        $message3->setMessage('Some Authorizers under this Account have invalid mailing address.');
        $message3->setAlias('authorizer_invalid_address');
        $message3->setType($this->getReference('message-type-error'));
        $manager->persist($message3);
        $manager->flush();

        $this->addReference('message-autorizer-invalid-address', $message3);

        //no municipality compliance channel backflow
        $message4 = new Message();
        $message4->setIcon('warning.svg');
        $message4->setMessage(
            'Information about upload fees is missing for some Divisions under this Account (some Municipalities related to this Account have no Compliance Channels for some divisions under this account).'
        );
        $message4->setAlias('no_municipality_compliance_channel_backflow');
        $message4->setType($this->getReference('message-type-error'));
        $manager->persist($message4);
        $manager->flush();

        $this->addReference('message-no-municipality-compliance-channel-backflow', $message4);

        //this Site has its own authoriser
        $message5 = new Message();
        $message5->setIcon('warning.svg');
        $message5->setMessage('This Site Account has its own Authoriser for some Divisions even thought it is under a Group Account.');
        $message5->setAlias('site_has_own_authorizer');
        $message5->setType($this->getReference('message-type-warning'));
        $manager->persist($message5);
        $manager->flush();

        $this->addReference('message-site-has-own-authorizer', $message5);


        //this Site has its own authoriser
        $serviceNoFeeMessage = new Message();
        $serviceNoFeeMessage->setIcon('warning.svg');
        $serviceNoFeeMessage->setMessage('Service Fee information for some of the Services under this Account is missing.');
        $serviceNoFeeMessage->setAlias('service_no_fee');
        $serviceNoFeeMessage->setType($this->getReference('message-type-error'));
        $manager->persist($serviceNoFeeMessage);
        $manager->flush();

        $this->addReference('message-service-no-fee', $serviceNoFeeMessage);
    }

    public function getOrder()
    {
        return 2;
    }
}
