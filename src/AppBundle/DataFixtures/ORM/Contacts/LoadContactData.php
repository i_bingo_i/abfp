<?php

namespace AppBundle\DataFixtures\ORM\Contacts;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Contact;

class LoadContactData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Contacts for departments
        $contact1 = new Contact();
        $contact1->setFirstName("John");
        $contact1->setLastName("Johnson");
        $contact1->setTitle("Title for Contact 1");
        $contact1->setPhone("0905671221");
        $contact1->setCell("0312121211");
        $contact1->setExt("003");
        $contact1->setFax("0434346767");
        $contact1->setAddress($this->getReference('address-one'));
        $contact1->setEmail("contact1@gmail.com");
        $contact1->addDivision($this->getReference('device-category-backflow'));
        $contact1->setType($this->getReference('contact-type-department'));

        $manager->persist($contact1);
        $manager->flush();
        $this->addReference('contact-one', $contact1);

        $contact2 = new Contact();
        $contact2->setFirstName("Jam");
        $contact2->setLastName("Smith");
        $contact2->setTitle("Title for Contact 2");
        $contact2->setPhone("0905671222");
        $contact2->setCell("0312121212");
        $contact2->setExt("004");
        $contact2->setFax("0434346768");
        $contact2->setEmail("contact2@gmail.com");
        $contact2->addDivision($this->getReference('device-category-fire'));
        $contact2->setType($this->getReference('contact-type-department'));

        $manager->persist($contact2);
        $manager->flush();
        $this->addReference('contact-two', $contact2);

        $contact3 = new Contact();
        $contact3->setFirstName("William");
        $contact3->setLastName("Johnson");
        $contact3->setTitle("Title for Contact 3");
        $contact3->setPhone("0905671223");
        $contact3->setCell("0312121213");
        $contact3->setExt("005");
        $contact3->setFax("0434346769");
        $contact3->setAddress($this->getReference('address-two'));
        $contact3->setEmail("contact3@gmail.com");
        $contact3->addDivision($this->getReference('device-category-fire'));
        $contact3->setType($this->getReference('contact-type-department'));

        $manager->persist($contact3);
        $manager->flush();
        $this->addReference('contact-three', $contact3);


        $contactJonSnow = new Contact();
        $contactJonSnow->setFirstName("John");
        $contactJonSnow->setLastName("Snow");
        $contactJonSnow->addDivision($this->getReference('device-category-backflow'));
        $contactJonSnow->setType($this->getReference('contact-type-department'));

        $manager->persist($contactJonSnow);
        $manager->flush();
        $this->addReference('contact-jon-snow', $contactJonSnow);

        //Contacts for municipalities
        $contact4 = new Contact();
        $contact4->setFirstName("James");
        $contact4->setLastName("Bond");
        $contact4->setTitle("Title 1 for Contact for Municipality");
        $contact4->setPhone("0915671221");
        $contact4->setCell("0322121211");
        $contact4->setExt("007");
        $contact4->setFax("0514346760");
        $contact4->setAddress($this->getReference('address-twelve'));
        $contact4->setEmail("contact4@gmail.com");
        $contact4->addDivision($this->getReference('device-category-fire'));
        $contact4->setType($this->getReference('contact-type-municipality'));

        $manager->persist($contact4);
        $manager->flush();
        $this->addReference('contact-four', $contact4);

        $contact5 = new Contact();
        $contact5->setFirstName("William");
        $contact5->setLastName("Taylor");
        $contact5->setTitle("Title 2 for Contact for Municipality");
        $contact5->setPhone("0915671222");
        $contact5->setCell("0322121212");
        $contact5->setExt("008");
        $contact5->setFax("0514346761");
        $contact5->setAddress($this->getReference('address-for-account-one'));
        $contact5->setEmail("contact5@gmail.com");
        $contact5->addDivision($this->getReference('device-category-fire'));
        $contact5->setType($this->getReference('contact-type-municipality'));

        $manager->persist($contact5);
        $manager->flush();
        $this->addReference('contact-five', $contact5);

        $contact6 = new Contact();
        $contact6->setFirstName("Jack");
        $contact6->setLastName("Sparrow");
        $contact6->setTitle("Title 3 for Contact for Municipality");
        $contact6->setPhone("0915671223");
        $contact6->setCell("0322121213");
        $contact6->setExt("009");
        $contact6->setFax("0514346762");
        $contact6->setAddress($this->getReference('address-for-account-three'));
        $contact6->setEmail("contact6@gmail.com");
        $contact6->addDivision($this->getReference('device-category-backflow'));
        $contact6->setType($this->getReference('contact-type-municipality'));

        $manager->persist($contact6);
        $manager->flush();
        $this->addReference('contact-six', $contact6);

        $contact7 = new Contact();
        $contact7->setFirstName("Bill");
        $contact7->setLastName("Smith");
        $contact7->setTitle("Title 4 for Contact for Municipality");
        $contact7->setPhone("0915671224");
        $contact7->setCell("0322121214");
        $contact7->setExt("010");
        $contact7->setFax("0514346763");
        $contact7->setAddress($this->getReference('address-thirteen'));
        $contact7->setEmail("contact7@gmail.com");
        $contact7->addDivision($this->getReference('device-category-backflow'));
        $contact7->setType($this->getReference('contact-type-municipality'));

        $manager->persist($contact7);
        $manager->flush();
        $this->addReference('contact-seven', $contact7);

        //Contacts for agents
        $contact8 = new Contact();
        $contact8->setFirstName("Ralph");
        $contact8->setLastName("Gleason");
        $contact8->setTitle("Title 1 for Contact for Agent");
        $contact8->setPhone("0925671221");
        $contact8->setCell("0332121211");
        $contact8->setExt("011");
        $contact8->setFax("0714346761");
        $contact8->setAddress($this->getReference('address-fourteen'));
        $contact8->setEmail("contact8@gmail.com");
        $contact8->addDivision($this->getReference('device-category-backflow'));
        $contact8->setType($this->getReference('contact-type-agent'));

        $manager->persist($contact8);
        $manager->flush();
        $this->addReference('contact-eight', $contact8);

        $contact9 = new Contact();
        $contact9->setFirstName("Paul");
        $contact9->setLastName("Mccartney");
        $contact9->setTitle("Title 2 for Contact for Agent");
        $contact9->setPhone("0925671222");
        $contact9->setCell("0332121212");
        $contact9->setExt("012");
        $contact9->setFax("0714346762");
        $contact9->setAddress($this->getReference('address-fifteen'));
        $contact9->setEmail("contact9@gmail.com");
        $contact9->addDivision($this->getReference('device-category-backflow'));
        $contact9->addDivision($this->getReference('device-category-fire'));
        $contact9->setType($this->getReference('contact-type-agent'));

        $manager->persist($contact9);
        $manager->flush();
        $this->addReference('contact-nine', $contact9);

        $contact10 = new Contact();
        $contact10->setFirstName("Linda");
        $contact10->setLastName("Smith");
        $contact10->setTitle("Title 3 for Contact for Agent");
        $contact10->setPhone("0925671223");
        $contact10->setCell("0332121213");
        $contact10->setExt("013");
        $contact10->setFax("0714346763");
        $contact10->setAddress($this->getReference('address-sixteen'));
        $contact10->setEmail("contact10@gmail.com");
        $contact10->addDivision($this->getReference('device-category-backflow'));
        $contact10->setType($this->getReference('contact-type-agent'));

        $manager->persist($contact10);
        $manager->flush();
        $this->addReference('contact-ten', $contact10);

        $contact11 = new Contact();
        $contact11->setFirstName("Jann");
        $contact11->setLastName("Wenner");
        $contact11->setTitle("Title 4 for Contact for Agent");
        $contact11->setPhone("0925671224");
        $contact11->setCell("0332121214");
        $contact11->setExt("014");
        $contact11->setFax("0714346764");
        $contact11->setAddress($this->getReference('address-seventeen'));
        $contact11->setEmail("contact11@gmail.com");
        $contact11->addDivision($this->getReference('device-category-fire'));
        $contact11->setType($this->getReference('contact-type-agent'));

        $manager->persist($contact11);
        $manager->flush();
        $this->addReference('contact-eleven', $contact11);


        $contact12 = new Contact();
        $contact12->setFirstName("Jann");
        $contact12->setLastName("Winner");
        $contact12->setTitle("Title 5 for Contact for Department");
        $contact12->setPhone("0925671224");
        $contact12->setExt("014");
        $contact12->setFax("0514345554");
        $contact12->addDivision($this->getReference('device-category-fire'));
        $contact12->setType($this->getReference('contact-type-department'));

        $manager->persist($contact12);
        $manager->flush();
        $this->addReference('contact-twelve', $contact12);


        $contact13 = new Contact();
        $contact13->setFirstName("Jon");
        $contact13->setLastName("Doe");
        $contact13->setTitle("Some Department's title");
        $contact13->setPhone("0925671224");
        $contact13->setExt("014");
        $contact13->setFax("0514345554");
        $contact13->setAddress($this->getReference('address-seventeen'));
        $contact13->addDivision($this->getReference('device-category-fire'));
        $contact13->setType($this->getReference('contact-type-department'));

        $manager->persist($contact13);
        $manager->flush();
        $this->addReference('contact-thirteen', $contact13);

        $contact14 = new Contact();
        $contact14->setFirstName("Harry");
        $contact14->setLastName("Potter");
        $contact14->setTitle("Some Agents's title");
        $contact14->setPhone("0925671224");
        $contact14->setExt("014");
        $contact14->setFax("0514345554");
        $contact14->setEmail("contact14@gmail.com");
        $contact14->setAddress($this->getReference('address-seventeen'));
        $contact14->addDivision($this->getReference('device-category-fire'));
        $contact14->setType($this->getReference('contact-type-agent'));

        $manager->persist($contact14);
        $manager->flush();
        $this->addReference('contact-fourteen', $contact14);

        $contact15 = new Contact();
        $contact15->setFirstName("Bruce");
        $contact15->setLastName("Willis");
        $contact15->setTitle("Some department's title");
        $contact15->setPhone("0925671224");
        $contact15->setExt("014");
        $contact15->setFax("0514345554");
        $contact15->setEmail("contact14@gmail.com");
        $contact15->setAddress($this->getReference('address-seventeen'));
        $contact15->addDivision($this->getReference('device-category-fire'));
        $contact15->setType($this->getReference('contact-type-department'));

        $manager->persist($contact15);
        $manager->flush();
        $this->addReference('contact-15', $contact15);

        $contact16 = new Contact();
        $contact16->setFirstName("Keira");
        $contact16->setLastName("Knightley");
        $contact16->setTitle("some title");
        $contact16->setPhone("0234571224");
        $contact16->setExt("014");
        $contact16->setFax("0412345554");
        $contact16->setEmail("contact16@gmail.com");
        $contact16->setAddress($this->getReference('address-seventeen'));
        $contact16->addDivision($this->getReference('device-category-backflow'));
        $contact16->setType($this->getReference('contact-type-department'));

        $manager->persist($contact16);
        $manager->flush();
        $this->addReference('contact-16', $contact16);

        $contact17 = new Contact();
        $contact17->setFirstName("John");
        $contact17->setLastName("Depp");
        $contact17->setTitle("Some Agents's title");
        $contact17->setPhone("0925671224");
        $contact17->setExt("014");
        $contact17->setFax("0514345554");
        $contact17->setEmail("contact17@gmail.com");
        $contact17->setAddress($this->getReference('address-seventeen'));
        $contact17->addDivision($this->getReference('device-category-backflow'));
        $contact17->setType($this->getReference('contact-type-department'));

        $manager->persist($contact17);
        $manager->flush();
        $this->addReference('contact-17', $contact17);
    }

    public function getOrder()
    {
        return 3;
    }
}
