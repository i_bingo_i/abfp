<?php

namespace AppBundle\DataFixtures\ORM\Contacts;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContactType;

class LoadContactTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contactTypeMunicipality = new ContactType();
        $contactTypeMunicipality->setName('Municipality contact');
        $contactTypeMunicipality->setAlias('municipality_contact');
        $contactTypeMunicipality->setIcon('municipality_contact.svg');
        $manager->persist($contactTypeMunicipality);
        $manager->flush();

        $this->addReference('contact-type-municipality', $contactTypeMunicipality);

        $contactTypeAgent = new ContactType();
        $contactTypeAgent->setName('Agent contact');
        $contactTypeAgent->setAlias('agent_contact');
        $contactTypeAgent->setIcon('agent_contact.svg');
        $manager->persist($contactTypeAgent);
        $manager->flush();

        $this->addReference('contact-type-agent', $contactTypeAgent);

        $contactTypeDepartment = new ContactType();
        $contactTypeDepartment->setName('Department contact');
        $contactTypeDepartment->setAlias('department_contact');
        $contactTypeDepartment->setIcon('municipality_contact.svg');
        $manager->persist($contactTypeDepartment);
        $manager->flush();

        $this->addReference('contact-type-department', $contactTypeDepartment);
    }

    public function getOrder()
    {
        return 1;
    }
}
