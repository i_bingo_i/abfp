<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\DynamicFieldValidation;

class LoadDynamicFieldValidationData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $dynamicFieldValidationNumber1 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber1->setName('app-font--uppercase');
        $dynamicFieldValidationNumber1->setAlias('app-font--uppercase');
        $manager->persist($dynamicFieldValidationNumber1);
        $manager->flush();

        $this->addReference('validation-app-font--uppercase', $dynamicFieldValidationNumber1);


        $dynamicFieldValidationNumber2 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber2->setName('app-validation-digits');
        $dynamicFieldValidationNumber2->setAlias('app-validation-digits');
        $manager->persist($dynamicFieldValidationNumber2);
        $manager->flush();

        $this->addReference('validation-app-validation-digits', $dynamicFieldValidationNumber2);


        $dynamicFieldValidationNumber3 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber3->setName('app-limit-digits');
        $dynamicFieldValidationNumber3->setAlias('app-limit-digits');
        $manager->persist($dynamicFieldValidationNumber3);
        $manager->flush();

        $this->addReference('validation-app-limit-digits', $dynamicFieldValidationNumber3);


        $dynamicFieldValidationNumber4 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber4->setName('app-validation-phone');
        $dynamicFieldValidationNumber4->setAlias('app-validation-phone');
        $manager->persist($dynamicFieldValidationNumber4);
        $manager->flush();

        $this->addReference('validation-app-validation-phone', $dynamicFieldValidationNumber4);


        $dynamicFieldValidationNumber5 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber5->setName('app-validation-currency');
        $dynamicFieldValidationNumber5->setAlias('app-validation-currency');
        $manager->persist($dynamicFieldValidationNumber5);
        $manager->flush();

        $this->addReference('validation-app-validation-currency', $dynamicFieldValidationNumber5);


        $dynamicFieldValidationNumber6 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber6->setName('app-validation-email');
        $dynamicFieldValidationNumber6->setAlias('app-validation-email');
        $manager->persist($dynamicFieldValidationNumber6);
        $manager->flush();

        $this->addReference('validation-app-validation-email', $dynamicFieldValidationNumber6);


        $dynamicFieldValidationNumber7 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber7->setName('app-validation-maxLength100');
        $dynamicFieldValidationNumber7->setAlias('app-validation-maxlength100');
        $manager->persist($dynamicFieldValidationNumber7);
        $manager->flush();

        $this->addReference('validation-app-validation-max-length-100', $dynamicFieldValidationNumber7);


        $dynamicFieldValidationNumber8 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber8->setName('app-validation-dynamicZip');
        $dynamicFieldValidationNumber8->setAlias('app-validation-dynamiczip');
        $manager->persist($dynamicFieldValidationNumber8);
        $manager->flush();

        $this->addReference('validation-app-validation-dynamic-zip', $dynamicFieldValidationNumber8);


        $dynamicFieldValidationNumber9 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber9->setName('app-validation-maxLength100');
        $dynamicFieldValidationNumber9->setAlias('app-validation-maxlength100');
        $manager->persist($dynamicFieldValidationNumber9);
        $manager->flush();

        $this->addReference('validation-app-validation-maxLength50', $dynamicFieldValidationNumber9);


        $dynamicFieldValidationNumber10 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber10->setName('app-validation-maxLength100');
        $dynamicFieldValidationNumber10->setAlias('app-validation-maxlength100');
        $manager->persist($dynamicFieldValidationNumber10);
        $manager->flush();

        $this->addReference('validation-app-validation-required', $dynamicFieldValidationNumber10);


        $dynamicFieldValidationNumber11 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber11->setName('app-limit-digits4');
        $dynamicFieldValidationNumber11->setAlias('app-limit-digits4');
        $manager->persist($dynamicFieldValidationNumber11);
        $manager->flush();

        $this->addReference('validation-app-limit-digits4', $dynamicFieldValidationNumber11);


        $dynamicFieldValidationNumber12 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber12->setName('app-limit-digits3');
        $dynamicFieldValidationNumber12->setAlias('app-limit-digits3');
        $manager->persist($dynamicFieldValidationNumber12);
        $manager->flush();

        $this->addReference('validation-app-limit-digits3', $dynamicFieldValidationNumber12);


        $dynamicFieldValidationNumber13 = new DynamicFieldValidation();
        $dynamicFieldValidationNumber13->setName('app-limit-number');
        $dynamicFieldValidationNumber13->setAlias('app-limit-number');
        $manager->persist($dynamicFieldValidationNumber13);
        $manager->flush();

        $this->addReference('validation-app-limit-number', $dynamicFieldValidationNumber13);
    }

    public function getOrder()
    {
        return 1;
    }
}
