<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedFireExtinguishersData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Fire Extinguishers root level *******/
        $fireExtinguishers = new DeviceNamed();
        $fireExtinguishers->setName('Fire Extinguisher(s)');
        $fireExtinguishers->setAlias('fire_extinguishers');
        $fireExtinguishers->setCategory($this->getReference('device-category-fire'));
        $fireExtinguishers->setSort(1);
        $manager->persist($fireExtinguishers);

        $this->addReference('fire-root-extinguishers-device', $fireExtinguishers);
        /************ End device named ****************/

        /** @var DynamicField $numberOfExtinguishers */
        $numberOfExtinguishers = new DynamicField();
        $numberOfExtinguishers->setName('Number of Extinguishers');
        $numberOfExtinguishers->setAlias('number_of_extinguishers');
        $numberOfExtinguishers->setType($this->getReference('dynamic-field-type-text'));
        $numberOfExtinguishers->setDevice($fireExtinguishers);
        $numberOfExtinguishers->setValidation($this->getReference('validation-app-limit-digits4'));
        $numberOfExtinguishers->setUseLabel(true);
        $numberOfExtinguishers->setLabelAfter(true);
        $numberOfExtinguishers->setLabelDescription("extinguisher(s)");
        $manager->persist($numberOfExtinguishers);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 11;
    }
}