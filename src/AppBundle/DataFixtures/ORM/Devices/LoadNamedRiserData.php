<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedRiserData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Riser *******/
        /** @var DeviceNamed $riser */
        $riser = new DeviceNamed();
        $riser->setName('Riser');
        $riser->setAlias('riser');
        $riser->setParent($this->getReference('fire-sprinkler-system-device'));
        $riser->setCategory($this->getReference('device-category-fire'));
        $riser->setLevel(1);
        $manager->persist($riser);

        $this->addReference('riser-device', $riser);
        /************ End device named ****************/



        /************  Dynamic fields list *************/
        /** @var DynamicField $riserNumber */
        $riserNumber = new DynamicField();
        $riserNumber->setName('Riser Number');
        $riserNumber->setAlias('riser_number');
        $riserNumber->setType($this->getReference('dynamic-field-type-text'));
        $riserNumber->setDevice($riser);
        $manager->persist($riserNumber);


        /** @var DynamicField $size */
        $size = new DynamicField();
        $size->setName('Size');
        $size->setAlias('riser_size');
        $size->setType($this->getReference('dynamic-field-type-dropdown'));
        $size->setDevice($riser);
        $manager->persist($size);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1 */
        $choice1 = new DynamicFieldDropboxChoices();
        $choice1->setField($size);
        $choice1->setOptionValue('1"');
        $choice1->setAlias('riser_size_1');
        $manager->persist($choice1);

        /** @var DynamicFieldDropboxChoices $choice1Dot25 */
        $choice1Dot25 = new DynamicFieldDropboxChoices();
        $choice1Dot25->setField($size);
        $choice1Dot25->setOptionValue('1.25"');
        $choice1Dot25->setAlias('riser_size_1_dot_25');
        $manager->persist($choice1Dot25);

        /** @var DynamicFieldDropboxChoices $choice1Dot5 */
        $choice1Dot5 = new DynamicFieldDropboxChoices();
        $choice1Dot5->setField($size);
        $choice1Dot5->setOptionValue('1.5"');
        $choice1Dot5->setAlias('riser_size_1_dot_5');
        $manager->persist($choice1Dot5);

        /** @var DynamicFieldDropboxChoices $choice2 */
        $choice2 = new DynamicFieldDropboxChoices();
        $choice2->setField($size);
        $choice2->setOptionValue('2"');
        $choice2->setAlias('riser_size_2');
        $manager->persist($choice2);

        /** @var DynamicFieldDropboxChoices $choice2Dot5 */
        $choice2Dot5 = new DynamicFieldDropboxChoices();
        $choice2Dot5->setField($size);
        $choice2Dot5->setOptionValue('2.5"');
        $choice2Dot5->setAlias('riser_size_2_dot_5');
        $manager->persist($choice2Dot5);

        /** @var DynamicFieldDropboxChoices $choice3 */
        $choice3 = new DynamicFieldDropboxChoices();
        $choice3->setField($size);
        $choice3->setOptionValue('3"');
        $choice3->setAlias('riser_size_3');
        $manager->persist($choice3);

        /** @var DynamicFieldDropboxChoices $choice4 */
        $choice4 = new DynamicFieldDropboxChoices();
        $choice4->setField($size);
        $choice4->setOptionValue('4"');
        $choice4->setAlias('riser_size_4');
        $manager->persist($choice4);

        /** @var DynamicFieldDropboxChoices $choice6 */
        $choice6 = new DynamicFieldDropboxChoices();
        $choice6->setField($size);
        $choice6->setOptionValue('6"');
        $choice6->setAlias('riser_size_6');
        $manager->persist($choice6);

        /** @var DynamicFieldDropboxChoices $choice8 */
        $choice8 = new DynamicFieldDropboxChoices();
        $choice8->setField($size);
        $choice8->setOptionValue('8"');
        $choice8->setAlias('riser_size_8');
        $manager->persist($choice8);

        /** @var DynamicFieldDropboxChoices $choice10 */
        $choice10 = new DynamicFieldDropboxChoices();
        $choice10->setField($size);
        $choice10->setOptionValue('10"');
        $choice10->setAlias('riser_size_10');
        $manager->persist($choice10);

        /** @var DynamicFieldDropboxChoices $choice12 */
        $choice12 = new DynamicFieldDropboxChoices();
        $choice12->setField($size);
        $choice12->setOptionValue('12"');
        $choice12->setAlias('riser_size_12');
        $manager->persist($choice12);
        /**************  End choices list ***************/


        /** @var DynamicField $itvLocation */
        $itvLocation = new DynamicField();
        $itvLocation->setName('ITV Location');
        $itvLocation->setAlias('itv_location');
        $itvLocation->setType($this->getReference('dynamic-field-type-text'));
        $itvLocation->setDevice($riser);
        $manager->persist($itvLocation);


        /** @var DynamicField $gaugeSizeIPS */
        $gaugeSizeIPS = new DynamicField();
        $gaugeSizeIPS->setName('Gauge Size IPS');
        $gaugeSizeIPS->setAlias('gauge_size_ips');
        $gaugeSizeIPS->setType($this->getReference('dynamic-field-type-dropdown'));
        $gaugeSizeIPS->setDevice($riser);
        $manager->persist($gaugeSizeIPS);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1Slash8 */
        $choice1Slash8 = new DynamicFieldDropboxChoices();
        $choice1Slash8->setField($gaugeSizeIPS);
        $choice1Slash8->setOptionValue('1/8"');
        $choice1Slash8->setAlias('riser_gauge_size_ips_1_slash_8');
        $manager->persist($choice1Slash8);

        /** @var DynamicFieldDropboxChoices $choice1Slash4 */
        $choice1Slash4 = new DynamicFieldDropboxChoices();
        $choice1Slash4->setField($gaugeSizeIPS);
        $choice1Slash4->setOptionValue('1/4"');
        $choice1Slash4->setSelectDefault(true);
        $choice1Slash4->setAlias('riser_gauge_size_ips_1_slash_4');
        $manager->persist($choice1Slash4);
        /**************  End choices list ***************/
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
