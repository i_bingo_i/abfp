<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceStatus;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDeviceStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //1
        $deviceStatusOne = new DeviceStatus();
        $deviceStatusOne->setName('Active');
        $deviceStatusOne->setAlias('active');

        $manager->persist($deviceStatusOne);
        $manager->flush();

        $this->addReference('device-status-active', $deviceStatusOne);

        //2
        $deviceStatusTwo = new DeviceStatus();
        $deviceStatusTwo->setName('Inactive');
        $deviceStatusTwo->setAlias('inactive');

        $manager->persist($deviceStatusTwo);
        $manager->flush();

        $this->addReference('device-status-inactive', $deviceStatusTwo);
    }

    public function getOrder()
    {
        return 11;
    }
}
