<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedFDCCheckValveData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named FDC Check Valve *******/
        /** @var DeviceNamed $fdcCheckValve */
        $fdcCheckValve = new DeviceNamed();
        $fdcCheckValve->setName('FDC Check Valve');
        $fdcCheckValve->setAlias('fdc_check_valve');
        $fdcCheckValve->setParent($this->getReference('fire-sprinkler-system-device'));
        $fdcCheckValve->setCategory($this->getReference('device-category-fire'));
        $fdcCheckValve->setLevel(1);
        $manager->persist($fdcCheckValve);

        $this->addReference('fdc-check-valve-device', $fdcCheckValve);
        /************ End device named ****************/



        /************  Dynamic fields list *************/
        /** @var DynamicField $size */
        $size = new DynamicField();
        $size->setName('Size');
        $size->setAlias('fdc_check_valve_size');
        $size->setType($this->getReference('dynamic-field-type-dropdown'));
        $size->setDevice($fdcCheckValve);
        $manager->persist($size);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1Dot5 */
        $choice1Dot5 = new DynamicFieldDropboxChoices();
        $choice1Dot5->setField($size);
        $choice1Dot5->setOptionValue('1.5"');
        $choice1Dot5->setAlias('fdc_check_valve_size_1_dot_5');
        $manager->persist($choice1Dot5);

        /** @var DynamicFieldDropboxChoices $choice2 */
        $choice2 = new DynamicFieldDropboxChoices();
        $choice2->setField($size);
        $choice2->setOptionValue('2"');
        $choice2->setAlias('fdc_check_valve_size_2');
        $manager->persist($choice2);

        /** @var DynamicFieldDropboxChoices $choice2Dot5 */
        $choice2Dot5 = new DynamicFieldDropboxChoices();
        $choice2Dot5->setField($size);
        $choice2Dot5->setOptionValue('2.5"');
        $choice2Dot5->setAlias('fdc_check_valve_size_2_dot_5');
        $manager->persist($choice2Dot5);

        /** @var DynamicFieldDropboxChoices $choice3 */
        $choice3 = new DynamicFieldDropboxChoices();
        $choice3->setField($size);
        $choice3->setOptionValue('3"');
        $choice3->setAlias('fdc_check_valve_size_3');
        $manager->persist($choice3);

        /** @var DynamicFieldDropboxChoices $choice4 */
        $choice4 = new DynamicFieldDropboxChoices();
        $choice4->setField($size);
        $choice4->setOptionValue('4"');
        $choice4->setAlias('fdc_check_valve_size_4');
        $manager->persist($choice4);

        /** @var DynamicFieldDropboxChoices $choice6 */
        $choice6 = new DynamicFieldDropboxChoices();
        $choice6->setField($size);
        $choice6->setOptionValue('6"');
        $choice6->setAlias('fdc_check_valve_size_6');
        $manager->persist($choice6);
        /**************  End choices list ***************/


        /** @var DynamicField $make */
        $make = new DynamicField();
        $make->setName('Make');
        $make->setAlias('make');
        $make->setType($this->getReference('dynamic-field-type-text'));
        $make->setDevice($fdcCheckValve);
        $manager->persist($make);


        /** @var DynamicField $connection */
        $connection = new DynamicField();
        $connection->setName('Connection');
        $connection->setAlias('connection');
        $connection->setType($this->getReference('dynamic-field-type-dropdown'));
        $connection->setDevice($fdcCheckValve);
        $manager->persist($connection);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceFlanged */
        $choiceFlanged = new DynamicFieldDropboxChoices();
        $choiceFlanged->setField($connection);
        $choiceFlanged->setOptionValue('Flanged');
        $choiceFlanged->setAlias('fdc_check_valve_connection_flanged');
        $choiceFlanged->setSelectDefault(true);
        $manager->persist($choiceFlanged);

        /** @var DynamicFieldDropboxChoices $choiceThreaded */
        $choiceThreaded = new DynamicFieldDropboxChoices();
        $choiceThreaded->setField($connection);
        $choiceThreaded->setOptionValue('Threaded');
        $choiceThreaded->setAlias('fdc_check_valve_connection_threaded');
        $manager->persist($choiceThreaded);

        /** @var DynamicFieldDropboxChoices $choiceGroove */
        $choiceGroove = new DynamicFieldDropboxChoices();
        $choiceGroove->setField($connection);
        $choiceGroove->setOptionValue('Groove');
        $choiceGroove->setAlias('fdc_check_valve_connection_groove');
        $manager->persist($choiceGroove);

        /** @var DynamicFieldDropboxChoices $choiceOther */
        $choiceOther = new DynamicFieldDropboxChoices();
        $choiceOther->setField($connection);
        $choiceOther->setOptionValue('Other');
        $choiceOther->setAlias('fdc_check_valve_connection_other');
        $manager->persist($choiceOther);
        /**************  End choices list ***************/


        /** @var DynamicField $drainDown */
        $drainDown = new DynamicField();
        $drainDown->setName('Drain Down');
        $drainDown->setAlias('drain_down');
        $drainDown->setType($this->getReference('dynamic-field-type-dropdown'));
        $drainDown->setDevice($fdcCheckValve);
        $manager->persist($drainDown);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceBallDrip */
        $choiceBallDrip = new DynamicFieldDropboxChoices();
        $choiceBallDrip->setField($drainDown);
        $choiceBallDrip->setOptionValue('Ball Drip');
        $choiceBallDrip->setAlias('fdc_check_valve_drain_down_ball_drip');
        $manager->persist($choiceBallDrip);

        /** @var DynamicFieldDropboxChoices $choiceOutFDC */
        $choiceOutFDC = new DynamicFieldDropboxChoices();
        $choiceOutFDC->setField($drainDown);
        $choiceOutFDC->setOptionValue('Out FDC');
        $choiceOutFDC->setAlias('fdc_check_valve_drain_down_out_fdc');
        $manager->persist($choiceOutFDC);

        /** @var DynamicFieldDropboxChoices $choiceOther */
        $choiceOther = new DynamicFieldDropboxChoices();
        $choiceOther->setField($drainDown);
        $choiceOther->setOptionValue('Other');
        $choiceOther->setAlias('fdc_check_valve_drain_down_other');
        $manager->persist($choiceOther);
        /**************  End choices list ***************/
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
