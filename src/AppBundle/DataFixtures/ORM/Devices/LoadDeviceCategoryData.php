<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceCategory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDeviceCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //1
        $deviceCategoryOne = new DeviceCategory();
        $deviceCategoryOne->setName('Backflow');
        $deviceCategoryOne->setAlias('backflow');
        $deviceCategoryOne->setIcon('backflow.svg');
        $deviceCategoryOne->setWeight(4);

        $manager->persist($deviceCategoryOne);
        $manager->flush();

        $this->addReference('device-category-backflow', $deviceCategoryOne);

        //2
        $deviceCategoryTwo = new DeviceCategory();
        $deviceCategoryTwo->setName('Fire');
        $deviceCategoryTwo->setAlias('fire');
        $deviceCategoryTwo->setIcon('fire.svg');
        $deviceCategoryTwo->setWeight(3);

        $manager->persist($deviceCategoryTwo);
        $manager->flush();

        $this->addReference('device-category-fire', $deviceCategoryTwo);

        //3
        $deviceCategoryThree = new DeviceCategory();
        $deviceCategoryThree->setName('Plumbing');
        $deviceCategoryThree->setAlias('plumbing');
        $deviceCategoryThree->setIcon('plumbing.svg');
        $deviceCategoryThree->setWeight(2);

        $manager->persist($deviceCategoryThree);
        $manager->flush();

        $this->addReference('device-category-plumbing', $deviceCategoryThree);

        //4
        $deviceCategoryFour = new DeviceCategory();
        $deviceCategoryFour->setName('Alarm');
        $deviceCategoryFour->setAlias('alarm');
        $deviceCategoryFour->setIcon('alarm.svg');
        $deviceCategoryFour->setWeight(1);

        $manager->persist($deviceCategoryFour);
        $manager->flush();

        $this->addReference('device-category-alarm', $deviceCategoryFour);
    }

    public function getOrder()
    {
        return 1;
    }
}
