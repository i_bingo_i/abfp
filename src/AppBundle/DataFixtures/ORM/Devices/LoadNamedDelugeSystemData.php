<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedDelugeSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named DelugeSystem *******/
        /** @var DeviceNamed $delugeSystem */
        $delugeSystem = new DeviceNamed();
        $delugeSystem->setName('Deluge System');
        $delugeSystem->setAlias('deluge_system');
        $delugeSystem->setParent($this->getReference('fire-sprinkler-system-device'));
        $delugeSystem->setCategory($this->getReference('device-category-fire'));
        $delugeSystem->setLevel(1);
        $manager->persist($delugeSystem);

        $this->addReference('deluge-system-device', $delugeSystem);
        /************ End device named ****************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
