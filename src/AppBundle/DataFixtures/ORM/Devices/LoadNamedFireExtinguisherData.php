<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedFireExtinguisherData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Fire Extinguisher *******/
        /** @var DeviceNamed $fireExtinguisher */
        $fireExtinguisher = new DeviceNamed();
        $fireExtinguisher->setName('Fire Extinguisher');
        $fireExtinguisher->setAlias('fire_extinguisher');
        $fireExtinguisher->setSort(1);
        $fireExtinguisher->setCategory($this->getReference('device-category-fire'));
        $fireExtinguisher->setParent($this->getReference('fire-root-extinguishers-device'));
        $manager->persist($fireExtinguisher);

        $this->addReference('fire-extinguisher-device', $fireExtinguisher);
        /************ End device named ****************/



        /************  Dynamic fields list *************/
        /** @var DynamicField $fireExtinguisherSize */
        $fireExtinguisherSize = new DynamicField();
        $fireExtinguisherSize->setName('Fire Extinguisher Size');
        $fireExtinguisherSize->setAlias('fire_extinguisher_size');
        $fireExtinguisherSize->setType($this->getReference('dynamic-field-type-dropdown'));
        $fireExtinguisherSize->setDevice($fireExtinguisher);
        $manager->persist($fireExtinguisherSize);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1LB */
        $choice1LB = new DynamicFieldDropboxChoices();
        $choice1LB->setField($fireExtinguisherSize);
        $choice1LB->setOptionValue('1 LB');
        $choice1LB->setAlias('fire_extinguisher_fire_extinguisher_size_1_lb');
        $manager->persist($choice1LB);

        /** @var DynamicFieldDropboxChoices $choice2LB */
        $choice2LB = new DynamicFieldDropboxChoices();
        $choice2LB->setField($fireExtinguisherSize);
        $choice2LB->setOptionValue('2 LB');
        $choice2LB->setAlias('fire_extinguisher_fire_extinguisher_size_2_lb');
        $manager->persist($choice2LB);

        /** @var DynamicFieldDropboxChoices $choice2dot5LB */
        $choice2dot5LB = new DynamicFieldDropboxChoices();
        $choice2dot5LB->setField($fireExtinguisherSize);
        $choice2dot5LB->setOptionValue('2.5 LB');
        $choice2dot5LB->setAlias('fire_extinguisher_fire_extinguisher_size_2_dot_5_lb');
        $manager->persist($choice2dot5LB);

        /** @var DynamicFieldDropboxChoices $choice4LB */
        $choice4LB = new DynamicFieldDropboxChoices();
        $choice4LB->setField($fireExtinguisherSize);
        $choice4LB->setOptionValue('4 LB');
        $choice4LB->setAlias('fire_extinguisher_fire_extinguisher_size_4_lb');
        $manager->persist($choice4LB);

        /** @var DynamicFieldDropboxChoices $choice5LB */
        $choice5LB = new DynamicFieldDropboxChoices();
        $choice5LB->setField($fireExtinguisherSize);
        $choice5LB->setOptionValue('5 LB');
        $choice5LB->setAlias('fire_extinguisher_fire_extinguisher_size_5_lb');
        $manager->persist($choice5LB);

        /** @var DynamicFieldDropboxChoices $choice6LB */
        $choice6LB = new DynamicFieldDropboxChoices();
        $choice6LB->setField($fireExtinguisherSize);
        $choice6LB->setOptionValue('6 LB');
        $choice6LB->setAlias('fire_extinguisher_fire_extinguisher_size_6_lb');
        $manager->persist($choice6LB);

        /** @var DynamicFieldDropboxChoices $choice10LB */
        $choice10LB = new DynamicFieldDropboxChoices();
        $choice10LB->setField($fireExtinguisherSize);
        $choice10LB->setOptionValue('10 LB');
        $choice10LB->setAlias('fire_extinguisher_fire_extinguisher_size_10_lb');
        $manager->persist($choice10LB);

        /** @var DynamicFieldDropboxChoices $choice20LB */
        $choice20LB = new DynamicFieldDropboxChoices();
        $choice20LB->setField($fireExtinguisherSize);
        $choice20LB->setOptionValue('20 LB');
        $choice20LB->setAlias('fire_extinguisher_fire_extinguisher_size_20_lb');
        $manager->persist($choice20LB);

        /** @var DynamicFieldDropboxChoices $choice30LB */
        $choice30LB = new DynamicFieldDropboxChoices();
        $choice30LB->setField($fireExtinguisherSize);
        $choice30LB->setOptionValue('30 LB');
        $choice30LB->setAlias('fire_extinguisher_fire_extinguisher_size_30_lb');
        $manager->persist($choice30LB);

        /** @var DynamicFieldDropboxChoices $choice50LB */
        $choice50LB = new DynamicFieldDropboxChoices();
        $choice50LB->setField($fireExtinguisherSize);
        $choice50LB->setOptionValue('50 LB');
        $choice50LB->setAlias('fire_extinguisher_fire_extinguisher_size_50_lb');
        $manager->persist($choice50LB);
        /**************  End choices list ***************/


        /** @var DynamicField $fireExtinguisherType */
        $fireExtinguisherType = new DynamicField();
        $fireExtinguisherType->setName('Fire Extinguisher Type');
        $fireExtinguisherType->setAlias('fire_extinguisher_type');
        $fireExtinguisherType->setType($this->getReference('dynamic-field-type-dropdown'));
        $fireExtinguisherType->setDevice($fireExtinguisher);
        $manager->persist($fireExtinguisherType);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceClassA */
        $choiceClassA = new DynamicFieldDropboxChoices();
        $choiceClassA->setField($fireExtinguisherType);
        $choiceClassA->setOptionValue('Class A');
        $choiceClassA->setAlias('fire_extinguisher_fire_extinguisher_type_class_a');
        $manager->persist($choiceClassA);

        /** @var DynamicFieldDropboxChoices $choiceClassB */
        $choiceClassB = new DynamicFieldDropboxChoices();
        $choiceClassB->setField($fireExtinguisherType);
        $choiceClassB->setOptionValue('Class B');
        $choiceClassB->setAlias('fire_extinguisher_fire_extinguisher_type_class_b');
        $manager->persist($choiceClassB);

        /** @var DynamicFieldDropboxChoices $choiceClassC */
        $choiceClassC = new DynamicFieldDropboxChoices();
        $choiceClassC->setField($fireExtinguisherType);
        $choiceClassC->setOptionValue('Class C');
        $choiceClassC->setAlias('fire_extinguisher_fire_extinguisher_type_class_c');
        $manager->persist($choiceClassC);

        /** @var DynamicFieldDropboxChoices $choiceClassD */
        $choiceClassD = new DynamicFieldDropboxChoices();
        $choiceClassD->setField($fireExtinguisherType);
        $choiceClassD->setOptionValue('Class D');
        $choiceClassD->setAlias('fire_extinguisher_fire_extinguisher_type_class_d');
        $manager->persist($choiceClassD);

        /** @var DynamicFieldDropboxChoices $choiceClassK */
        $choiceClassK = new DynamicFieldDropboxChoices();
        $choiceClassK->setField($fireExtinguisherType);
        $choiceClassK->setOptionValue('Class K');
        $choiceClassK->setAlias('fire_extinguisher_fire_extinguisher_type_class_k');
        $manager->persist($choiceClassK);
        /**************  End choices list ***************/
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
