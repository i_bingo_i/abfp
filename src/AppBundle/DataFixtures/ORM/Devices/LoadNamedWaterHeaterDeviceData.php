<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedWaterHeaterDeviceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $waterHeater = new DeviceNamed();
        $waterHeater->setName('Water Heater');
        $waterHeater->setAlias('water_heater');
        $waterHeater->setCategory($this->getReference('device-category-plumbing'));
        $manager->persist($waterHeater);
        $this->addReference('water-heater-device', $waterHeater);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
