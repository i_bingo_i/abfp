<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\BadMethodCallException;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedPreEngineeredFireSuppressionSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * @throws BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Section *******/
        /** @var DeviceNamed $preEngineeredFireSuppressionSystem */
        $preEngineeredFireSuppressionSystem = new DeviceNamed();
        $preEngineeredFireSuppressionSystem->setName('Pre-Engineered Fire Suppression System');
        $preEngineeredFireSuppressionSystem->setAlias('pre_engineered_fire_suppression_system');
        $preEngineeredFireSuppressionSystem->setCategory($this->getReference('device-category-fire'));
        $manager->persist($preEngineeredFireSuppressionSystem);

        $this->addReference('pre_engineered_fire_suppression_system_device', $preEngineeredFireSuppressionSystem);
        /************ End device named ****************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
