<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedFireAlarmControlPanelData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Fire Alarm Control Panel (FACP) *******/
        /** @var DeviceNamed $fireAlarmPanel */
        $fireAlarmPanel = new DeviceNamed();
        $fireAlarmPanel->setName('Fire Alarm Control Panel (FACP)');
        $fireAlarmPanel->setAlias('fire_alarm_control_panel');
        $fireAlarmPanel->setCategory($this->getReference('device-category-fire'));
        $manager->persist($fireAlarmPanel);

        $this->addReference('fire-alarm-control-panel-device', $fireAlarmPanel);
        /************ End device named ****************/


        /************  Dynamic fields list *************/
        /** @var DynamicField $facpType */
        $facpType = new DynamicField();
        $facpType->setName('FACP Type');
        $facpType->setAlias('facp_type');
        $facpType->setType($this->getReference('dynamic-field-type-dropdown'));
        $facpType->setDevice($fireAlarmPanel);
        $manager->persist($facpType);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $addressable */
        $addressable = new DynamicFieldDropboxChoices();
        $addressable->setField($facpType);
        $addressable->setOptionValue('Addressable');
        $addressable->setAlias('addressable');
        $manager->persist($addressable);

        /** @var DynamicFieldDropboxChoices $conventional */
        $conventional = new DynamicFieldDropboxChoices();
        $conventional->setField($facpType);
        $conventional->setOptionValue('Conventional');
        $conventional->setAlias('conventional');
        $manager->persist($conventional);


        /** @var DynamicField $make */
        $make = new DynamicField();
        $make->setName('Make');
        $make->setAlias('fire_alarm_panel_make');
        $make->setType($this->getReference('dynamic-field-type-dropdown'));
        $make->setDevice($fireAlarmPanel);
        $manager->persist($make);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $silentKnight */
        $silentKnight = new DynamicFieldDropboxChoices();
        $silentKnight->setField($make);
        $silentKnight->setOptionValue('Silent Knight');
        $silentKnight->setAlias('make_silent_knight');
        $manager->persist($silentKnight);

        /** @var DynamicFieldDropboxChoices $fireLite */
        $fireLite = new DynamicFieldDropboxChoices();
        $fireLite->setField($make);
        $fireLite->setOptionValue('Fire-Lite');
        $fireLite->setAlias('make_fire_lite');
        $manager->persist($fireLite);

        /** @var DynamicFieldDropboxChoices $simplex */
        $simplex = new DynamicFieldDropboxChoices();
        $simplex->setField($make);
        $simplex->setOptionValue('Simplex');
        $simplex->setAlias('make_simplex');
        $manager->persist($simplex);

        /** @var DynamicFieldDropboxChoices $edwards */
        $edwards = new DynamicFieldDropboxChoices();
        $edwards->setField($make);
        $edwards->setOptionValue('Edwards');
        $edwards->setAlias('make_edwards');
        $manager->persist($edwards);

        /** @var DynamicFieldDropboxChoices $notifier */
        $notifier = new DynamicFieldDropboxChoices();
        $notifier->setField($make);
        $notifier->setOptionValue('Notifier');
        $notifier->setAlias('make_notifier');
        $manager->persist($notifier);

        /** @var DynamicFieldDropboxChoices $kiddie */
        $kiddie = new DynamicFieldDropboxChoices();
        $kiddie->setField($make);
        $kiddie->setOptionValue('Kiddie');
        $kiddie->setAlias('make_kiddie');
        $manager->persist($kiddie);

        /** @var DynamicFieldDropboxChoices $siemens */
        $siemens = new DynamicFieldDropboxChoices();
        $siemens->setField($make);
        $siemens->setOptionValue('Siemens');
        $siemens->setAlias('make_siemens');
        $manager->persist($siemens);

        /** @var DynamicFieldDropboxChoices $adt */
        $adt = new DynamicFieldDropboxChoices();
        $adt->setField($make);
        $adt->setOptionValue('ADT');
        $adt->setAlias('make_adt');
        $manager->persist($adt);

        /** @var DynamicFieldDropboxChoices $deelat */
        $deelat = new DynamicFieldDropboxChoices();
        $deelat->setField($make);
        $deelat->setOptionValue('Deelat');
        $deelat->setAlias('make_deelat');
        $manager->persist($deelat);

        /** @var DynamicFieldDropboxChoices $ademco */
        $ademco = new DynamicFieldDropboxChoices();
        $ademco->setField($make);
        $ademco->setOptionValue('Ademco');
        $ademco->setAlias('make_ademco');
        $manager->persist($ademco);

        /** @var DynamicFieldDropboxChoices $napco */
        $napco = new DynamicFieldDropboxChoices();
        $napco->setField($make);
        $napco->setOptionValue('Napco');
        $napco->setAlias('make_napco');
        $manager->persist($napco);

        /** @var DynamicFieldDropboxChoices $potter */
        $potter = new DynamicFieldDropboxChoices();
        $potter->setField($make);
        $potter->setOptionValue('Potter');
        $potter->setAlias('make_potter');
        $manager->persist($potter);

        /** @var DynamicFieldDropboxChoices $other */
        $other = new DynamicFieldDropboxChoices();
        $other->setField($make);
        $other->setOptionValue('Other');
        $other->setAlias('make_other');
        $manager->persist($other);


        /** @var DynamicField $model */
        $model = new DynamicField();
        $model->setName('Model');
        $model->setAlias('model');
        $model->setType($this->getReference('dynamic-field-type-text'));
        $model->setDevice($fireAlarmPanel);
        $manager->persist($model);


        /** @var DynamicField $alarmMonitoringCompany */
        $alarmMonitoringCompany = new DynamicField();
        $alarmMonitoringCompany->setName('Alarm Monitoring Company');
        $alarmMonitoringCompany->setAlias('alarm_monitoring_company');
        $alarmMonitoringCompany->setType($this->getReference('dynamic-field-type-text'));
        $alarmMonitoringCompany->setDevice($fireAlarmPanel);
        $manager->persist($alarmMonitoringCompany);


        /** @var DynamicField $passcode */
        $passcode = new DynamicField();
        $passcode->setName('Passcode');
        $passcode->setAlias('passcode');
        $passcode->setType($this->getReference('dynamic-field-type-text'));
        $passcode->setDevice($fireAlarmPanel);
        $manager->persist($passcode);


        /** @var DynamicField $positionNumber */
        $positionNumber = new DynamicField();
        $positionNumber->setName('Position Number');
        $positionNumber->setAlias('position_number');
        $positionNumber->setType($this->getReference('dynamic-field-type-text'));
        $positionNumber->setDevice($fireAlarmPanel);
        $manager->persist($positionNumber);


        /** @var DynamicField $phoneNumber */
        $phoneNumber = new DynamicField();
        $phoneNumber->setName('Phone Number');
        $phoneNumber->setAlias('phone_number');
        $phoneNumber->setType($this->getReference('dynamic-field-type-text'));
        $phoneNumber->setDevice($fireAlarmPanel);
        $manager->persist($phoneNumber);
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
