<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedBackflowDeviceData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Fire Extinguisher *******/
        /** @var DeviceNamed $backflowDevice */
        $backflowDevice = new DeviceNamed();
        $backflowDevice->setName('Backflow Device');
        $backflowDevice->setAlias('backflow_device');
        $backflowDevice->setCategory($this->getReference('device-category-backflow'));
        $manager->persist($backflowDevice);

        $this->addReference('backflow-device', $backflowDevice);
        /************ End device named ****************/



        /************  Dynamic fields list *************/
        /** @var DynamicField $size */
        $size = new DynamicField();
        $size->setName('Size');
        $size->setAlias('backflow_size');
        $size->setType($this->getReference('dynamic-field-type-dropdown'));
        $size->setDevice($backflowDevice);
        $manager->persist($size);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1Slash4 */
        $choice1Slash4 = new DynamicFieldDropboxChoices();
        $choice1Slash4->setField($size);
        $choice1Slash4->setOptionValue('1/4"');
        $choice1Slash4->setAlias('backflow_device_size_1_slash_4');
        $manager->persist($choice1Slash4);

        /** @var DynamicFieldDropboxChoices $choice1Slash2 */
        $choice1Slash2 = new DynamicFieldDropboxChoices();
        $choice1Slash2->setField($size);
        $choice1Slash2->setOptionValue('1/2"');
        $choice1Slash2->setAlias('backflow_device_size_1_slash_2');
        $manager->persist($choice1Slash2);

        /** @var DynamicFieldDropboxChoices $choice3Slash2 */
        $choice3Slash2 = new DynamicFieldDropboxChoices();
        $choice3Slash2->setField($size);
        $choice3Slash2->setOptionValue('3/4"');
        $choice3Slash2->setAlias('backflow_device_size_3_slash_4');
        $manager->persist($choice3Slash2);

        /** @var DynamicFieldDropboxChoices $choice1 */
        $choice1 = new DynamicFieldDropboxChoices();
        $choice1->setField($size);
        $choice1->setOptionValue('1"');
        $choice1->setAlias('backflow_device_size_1');
        $manager->persist($choice1);

        /** @var DynamicFieldDropboxChoices $choice1Dot25 */
        $choice1Dot25 = new DynamicFieldDropboxChoices();
        $choice1Dot25->setField($size);
        $choice1Dot25->setOptionValue('1.25"');
        $choice1Dot25->setAlias('backflow_device_size_1_dot_25');
        $manager->persist($choice1Dot25);

        /** @var DynamicFieldDropboxChoices $choice1Dot5 */
        $choice1Dot5 = new DynamicFieldDropboxChoices();
        $choice1Dot5->setField($size);
        $choice1Dot5->setOptionValue('1.5"');
        $choice1Dot5->setAlias('backflow_device_size_1_dot_5');
        $manager->persist($choice1Dot5);

        /** @var DynamicFieldDropboxChoices $choice2 */
        $choice2 = new DynamicFieldDropboxChoices();
        $choice2->setField($size);
        $choice2->setOptionValue('2"');
        $choice2->setAlias('backflow_device_size_2');
        $manager->persist($choice2);

        /** @var DynamicFieldDropboxChoices $choice2Dot5 */
        $choice2Dot5 = new DynamicFieldDropboxChoices();
        $choice2Dot5->setField($size);
        $choice2Dot5->setOptionValue('2.5"');
        $choice2Dot5->setAlias('backflow_device_size_2_dot_5');
        $manager->persist($choice2Dot5);

        /** @var DynamicFieldDropboxChoices $choice3 */
        $choice3 = new DynamicFieldDropboxChoices();
        $choice3->setField($size);
        $choice3->setOptionValue('3"');
        $choice3->setAlias('backflow_device_size_3');
        $manager->persist($choice3);

        /** @var DynamicFieldDropboxChoices $choice4 */
        $choice4 = new DynamicFieldDropboxChoices();
        $choice4->setField($size);
        $choice4->setOptionValue('4"');
        $choice4->setAlias('backflow_device_size_4');
        $manager->persist($choice4);

        /** @var DynamicFieldDropboxChoices $choice6 */
        $choice6 = new DynamicFieldDropboxChoices();
        $choice6->setField($size);
        $choice6->setOptionValue('6"');
        $choice6->setAlias('backflow_device_size_6');
        $manager->persist($choice6);

        /** @var DynamicFieldDropboxChoices $choice8 */
        $choice8 = new DynamicFieldDropboxChoices();
        $choice8->setField($size);
        $choice8->setOptionValue('8"');
        $choice8->setAlias('backflow_device_size_8');
        $manager->persist($choice8);

        /** @var DynamicFieldDropboxChoices $choice10 */
        $choice10 = new DynamicFieldDropboxChoices();
        $choice10->setField($size);
        $choice10->setOptionValue('10"');
        $choice10->setAlias('backflow_device_size_10');
        $manager->persist($choice10);

        /** @var DynamicFieldDropboxChoices $choice12 */
        $choice12 = new DynamicFieldDropboxChoices();
        $choice12->setField($size);
        $choice12->setOptionValue('12"');
        $choice12->setAlias('backflow_device_size_12');
        $manager->persist($choice12);
        /**************  End choices list ***************/


        /** @var DynamicField $make */
        $make = new DynamicField();
        $make->setName('Make');
        $make->setAlias('backflow_make');
        $make->setType($this->getReference('dynamic-field-type-dropdown'));
        $make->setDevice($backflowDevice);
        $manager->persist($make);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceAmes */
        $choiceAmes = new DynamicFieldDropboxChoices();
        $choiceAmes->setField($make);
        $choiceAmes->setOptionValue('Ames');
        $choiceAmes->setAlias('backflow_device_make_ames');
        $manager->persist($choiceAmes);

        /** @var DynamicFieldDropboxChoices $choiceFebco */
        $choiceFebco = new DynamicFieldDropboxChoices();
        $choiceFebco->setField($make);
        $choiceFebco->setOptionValue('Febco');
        $choiceFebco->setAlias('backflow_device_make_febco');
        $manager->persist($choiceFebco);

        /** @var DynamicFieldDropboxChoices $choiceWatts */
        $choiceWatts = new DynamicFieldDropboxChoices();
        $choiceWatts->setField($make);
        $choiceWatts->setOptionValue('Watts');
        $choiceWatts->setAlias('backflow_device_make_watts');
        $manager->persist($choiceWatts);

        /** @var DynamicFieldDropboxChoices $choiceWilkins */
        $choiceWilkins = new DynamicFieldDropboxChoices();
        $choiceWilkins->setField($make);
        $choiceWilkins->setOptionValue('Wilkins');
        $choiceWilkins->setAlias('backflow_device_make_wilkins');
        $manager->persist($choiceWilkins);

        /** @var DynamicFieldDropboxChoices $choiceRainbird */
        $choiceRainbird = new DynamicFieldDropboxChoices();
        $choiceRainbird->setField($make);
        $choiceRainbird->setOptionValue('Rainbird');
        $choiceRainbird->setAlias('backflow_device_make_rainbird');
        $manager->persist($choiceRainbird);

        /** @var DynamicFieldDropboxChoices $choiceConbraco */
        $choiceConbraco = new DynamicFieldDropboxChoices();
        $choiceConbraco->setField($make);
        $choiceConbraco->setOptionValue('Conbraco');
        $choiceConbraco->setAlias('backflow_device_make_conbraco');
        $manager->persist($choiceConbraco);

        /** @var DynamicFieldDropboxChoices $choiceApollo */
        $choiceApollo = new DynamicFieldDropboxChoices();
        $choiceApollo->setField($make);
        $choiceApollo->setOptionValue('Apollo');
        $choiceApollo->setAlias('backflow_device_make_apollo');
        $manager->persist($choiceApollo);

        /** @var DynamicFieldDropboxChoices $choiceColt */
        $choiceColt = new DynamicFieldDropboxChoices();
        $choiceColt->setField($make);
        $choiceColt->setOptionValue('Colt');
        $choiceColt->setAlias('backflow_device_make_colt');
        $manager->persist($choiceColt);

        /** @var DynamicFieldDropboxChoices $choiceMaxim */
        $choiceMaxim = new DynamicFieldDropboxChoices();
        $choiceMaxim->setField($make);
        $choiceMaxim->setOptionValue('Maxim');
        $choiceMaxim->setAlias('backflow_device_make_maxim');
        $manager->persist($choiceMaxim);

        /** @var DynamicFieldDropboxChoices $choiceBeeco */
        $choiceBeeco = new DynamicFieldDropboxChoices();
        $choiceBeeco->setField($make);
        $choiceBeeco->setOptionValue('Beeco');
        $choiceBeeco->setAlias('backflow_device_make_beeco');
        $manager->persist($choiceBeeco);

        /** @var DynamicFieldDropboxChoices $choiceBackflowDirect */
        $choiceBackflowDirect = new DynamicFieldDropboxChoices();
        $choiceBackflowDirect->setField($make);
        $choiceBackflowDirect->setOptionValue('Backflow Direct');
        $choiceBackflowDirect->setAlias('backflow_device_make_backflow_direct');
        $manager->persist($choiceBackflowDirect);

        /** @var DynamicFieldDropboxChoices $choiceFlomatic */
        $choiceFlomatic = new DynamicFieldDropboxChoices();
        $choiceFlomatic->setField($make);
        $choiceFlomatic->setOptionValue('Flomatic');
        $choiceFlomatic->setAlias('backflow_device_make_flomatic');
        $manager->persist($choiceFlomatic);

        /** @var DynamicFieldDropboxChoices $choiceSwingCHK */
        $choiceSwingCHK = new DynamicFieldDropboxChoices();
        $choiceSwingCHK->setField($make);
        $choiceSwingCHK->setOptionValue('Swing CHK');
        $choiceSwingCHK->setAlias('backflow_device_make_swing_chk');
        $manager->persist($choiceSwingCHK);

        /** @var DynamicFieldDropboxChoices $choiceZurn */
        $choiceZurn = new DynamicFieldDropboxChoices();
        $choiceZurn->setField($make);
        $choiceZurn->setOptionValue('Zurn');
        $choiceZurn->setAlias('backflow_device_make_zurn');
        $manager->persist($choiceZurn);

        /** @var DynamicFieldDropboxChoices $choiceVerify */
        $choiceVerify = new DynamicFieldDropboxChoices();
        $choiceVerify->setField($make);
        $choiceVerify->setOptionValue('Verify');
        $choiceVerify->setAlias('backflow_device_make_verify');
        $manager->persist($choiceVerify);

        /** @var DynamicFieldDropboxChoices $choiceCashAcme */
        $choiceCashAcme = new DynamicFieldDropboxChoices();
        $choiceCashAcme->setField($make);
        $choiceCashAcme->setOptionValue('Cash Acme');
        $choiceCashAcme->setAlias('backflow_device_make_cash_acme');
        $manager->persist($choiceCashAcme);

        /** @var DynamicFieldDropboxChoices $choiceGlobe */
        $choiceGlobe = new DynamicFieldDropboxChoices();
        $choiceGlobe->setField($make);
        $choiceGlobe->setOptionValue('Globe');
        $choiceGlobe->setAlias('backflow_device_make_globe');
        $manager->persist($choiceGlobe);

        /** @var DynamicFieldDropboxChoices $choiceClaVal */
        $choiceClaVal = new DynamicFieldDropboxChoices();
        $choiceClaVal->setField($make);
        $choiceClaVal->setOptionValue('Cla-Val');
        $choiceClaVal->setAlias('backflow_device_make_cla_val');
        $manager->persist($choiceClaVal);

        /** @var DynamicFieldDropboxChoices $choiceBuckner */
        $choiceBuckner = new DynamicFieldDropboxChoices();
        $choiceBuckner->setField($make);
        $choiceBuckner->setOptionValue('Buckner');
        $choiceBuckner->setAlias('backflow_device_make_buckner');
        $manager->persist($choiceBuckner);

        /** @var DynamicFieldDropboxChoices $choiceHersey */
        $choiceHersey = new DynamicFieldDropboxChoices();
        $choiceHersey->setField($make);
        $choiceHersey->setOptionValue('Hersey');
        $choiceHersey->setAlias('backflow_device_make_hersey');
        $manager->persist($choiceHersey);

        /** @var DynamicFieldDropboxChoices $choiceARI */
        $choiceARI = new DynamicFieldDropboxChoices();
        $choiceARI->setField($make);
        $choiceARI->setOptionValue('A.R.I');
        $choiceARI->setAlias('backflow_device_make_ari');
        $manager->persist($choiceARI);

        /** @var DynamicFieldDropboxChoices $choiceOther */
        $choiceOther = new DynamicFieldDropboxChoices();
        $choiceOther->setField($make);
        $choiceOther->setOptionValue('Other');
        $choiceOther->setAlias('backflow_device_make_other');
        $manager->persist($choiceOther);
        /**************  End choices list ***************/


        /** @var DynamicField $model */
        $model = new DynamicField();
        $model->setName('Model');
        $model->setAlias('model');
        $model->setType($this->getReference('dynamic-field-type-text'));
        $model->setDevice($backflowDevice);
        $manager->persist($model);


        /** @var DynamicField $type */
        $type = new DynamicField();
        $type->setName('Type');
        $type->setAlias('type');
        $type->setType($this->getReference('dynamic-field-type-dropdown'));
        $type->setIsShow(false);
        $type->setDevice($backflowDevice);
        $manager->persist($type);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceRPZ */
        $choiceRPZ = new DynamicFieldDropboxChoices();
        $choiceRPZ->setField($type);
        $choiceRPZ->setOptionValue('RPZ');
        $choiceRPZ->setAlias('backflow_device_type_rpz');
        $manager->persist($choiceRPZ);

        /** @var DynamicFieldDropboxChoices $choiceRPDA */
        $choiceRPDA = new DynamicFieldDropboxChoices();
        $choiceRPDA->setField($type);
        $choiceRPDA->setOptionValue('RPDA');
        $choiceRPDA->setAlias('backflow_device_type_rpda');
        $manager->persist($choiceRPDA);

        /** @var DynamicFieldDropboxChoices $choiceDCDA */
        $choiceDCDA = new DynamicFieldDropboxChoices();
        $choiceDCDA->setField($type);
        $choiceDCDA->setOptionValue('DCDA');
        $choiceDCDA->setAlias('backflow_device_type_dcda');
        $manager->persist($choiceDCDA);

        /** @var DynamicFieldDropboxChoices $choiceDCV */
        $choiceDCV = new DynamicFieldDropboxChoices();
        $choiceDCV->setField($type);
        $choiceDCV->setOptionValue('DCV');
        $choiceDCV->setAlias('backflow_device_type_dcv');
        $manager->persist($choiceDCV);

        /** @var DynamicFieldDropboxChoices $choiceSVB */
        $choiceSVB = new DynamicFieldDropboxChoices();
        $choiceSVB->setField($type);
        $choiceSVB->setOptionValue('SVB');
        $choiceSVB->setAlias('backflow_device_type_svb');
        $manager->persist($choiceSVB);

        /** @var DynamicFieldDropboxChoices $choicePVB */
        $choicePVB = new DynamicFieldDropboxChoices();
        $choicePVB->setField($type);
        $choicePVB->setOptionValue('PVB');
        $choicePVB->setAlias('backflow_device_type_pvb');
        $manager->persist($choicePVB);

        /** @var DynamicFieldDropboxChoices $choiceUnapprovedDevice */
        $choiceUnapprovedDevice = new DynamicFieldDropboxChoices();
        $choiceUnapprovedDevice->setField($type);
        $choiceUnapprovedDevice->setOptionValue('Unapproved Device');
        $choiceUnapprovedDevice->setAlias('backflow_device_type_unapproved_device');
        $manager->persist($choiceUnapprovedDevice);
        /**************  End choices list ***************/


        /** @var DynamicField $serialNumber */
        $serialNumber = new DynamicField();
        $serialNumber->setName('Serial Number');
        $serialNumber->setAlias('serial_number');
        $serialNumber->setType($this->getReference('dynamic-field-type-text'));
        $serialNumber->setDevice($backflowDevice);
        $serialNumber->setValidation($this->getReference('validation-app-font--uppercase'));
        $manager->persist($serialNumber);


        /** @var DynamicField $hazard */
        $hazard = new DynamicField();
        $hazard->setName('Hazard');
        $hazard->setAlias('hazard');
        $hazard->setType($this->getReference('dynamic-field-type-dropdown'));
        $hazard->setDevice($backflowDevice);
        $manager->persist($hazard);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceBackWashSupply */
        $choiceBackWashSupply = new DynamicFieldDropboxChoices();
        $choiceBackWashSupply->setField($hazard);
        $choiceBackWashSupply->setOptionValue('Back Wash Supply');
        $choiceBackWashSupply->setAlias('backflow_device_hazard_back_wash_supply');
        $manager->persist($choiceBackWashSupply);

        /** @var DynamicFieldDropboxChoices $choiceBaptismFountain */
        $choiceBaptismFountain = new DynamicFieldDropboxChoices();
        $choiceBaptismFountain->setField($hazard);
        $choiceBaptismFountain->setOptionValue('Baptism Fountain');
        $choiceBaptismFountain->setAlias('backflow_device_hazard_baptism_fountain');
        $manager->persist($choiceBaptismFountain);

        /** @var DynamicFieldDropboxChoices $choiceBoilerFeed */
        $choiceBoilerFeed = new DynamicFieldDropboxChoices();
        $choiceBoilerFeed->setField($hazard);
        $choiceBoilerFeed->setOptionValue('Boiler Feed');
        $choiceBoilerFeed->setAlias('backflow_device_hazard_boiler_feed');
        $manager->persist($choiceBoilerFeed);

        /** @var DynamicFieldDropboxChoices $choiceBrineTank */
        $choiceBrineTank = new DynamicFieldDropboxChoices();
        $choiceBrineTank->setField($hazard);
        $choiceBrineTank->setOptionValue('Brine Tank');
        $choiceBrineTank->setAlias('backflow_device_hazard_brine_tank');
        $manager->persist($choiceBrineTank);

        /** @var DynamicFieldDropboxChoices $choiceCarWash */
        $choiceCarWash = new DynamicFieldDropboxChoices();
        $choiceCarWash->setField($hazard);
        $choiceCarWash->setOptionValue('Car Wash');
        $choiceCarWash->setAlias('backflow_device_hazard_car_wash');
        $manager->persist($choiceCarWash);

        /** @var DynamicFieldDropboxChoices $choiceChemicalFeeder */
        $choiceChemicalFeeder = new DynamicFieldDropboxChoices();
        $choiceChemicalFeeder->setField($hazard);
        $choiceChemicalFeeder->setOptionValue('Chemical Feeder');
        $choiceChemicalFeeder->setAlias('backflow_device_hazard_chemical_feeder');
        $manager->persist($choiceChemicalFeeder);

        /** @var DynamicFieldDropboxChoices $choiceChillerMakeUp */
        $choiceChillerMakeUp = new DynamicFieldDropboxChoices();
        $choiceChillerMakeUp->setField($hazard);
        $choiceChillerMakeUp->setOptionValue('Chiller Make Up');
        $choiceChillerMakeUp->setAlias('backflow_device_hazard_chiller_make_up');
        $manager->persist($choiceChillerMakeUp);

        /** @var DynamicFieldDropboxChoices $choiceWokRange */
        $choiceWokRange = new DynamicFieldDropboxChoices();
        $choiceWokRange->setField($hazard);
        $choiceWokRange->setOptionValue('Wok Range');
        $choiceWokRange->setAlias('backflow_device_hazard_wok_range');
        $manager->persist($choiceWokRange);

        /** @var DynamicFieldDropboxChoices $choiceChlorineLine */
        $choiceChlorineLine = new DynamicFieldDropboxChoices();
        $choiceChlorineLine->setField($hazard);
        $choiceChlorineLine->setOptionValue('Chlorine Line');
        $choiceChlorineLine->setAlias('backflow_device_hazard_chlorine_line');
        $manager->persist($choiceChlorineLine);

        /** @var DynamicFieldDropboxChoices $choiceCoffeeMachine */
        $choiceCoffeeMachine = new DynamicFieldDropboxChoices();
        $choiceCoffeeMachine->setField($hazard);
        $choiceCoffeeMachine->setOptionValue('Coffee Machine');
        $choiceCoffeeMachine->setAlias('backflow_device_hazard_coffee_machine');
        $manager->persist($choiceCoffeeMachine);

        /** @var DynamicFieldDropboxChoices $choiceColdFeedPediChair */
        $choiceColdFeedPediChair = new DynamicFieldDropboxChoices();
        $choiceColdFeedPediChair->setField($hazard);
        $choiceColdFeedPediChair->setOptionValue('Cold Feed Pedi Chair');
        $choiceColdFeedPediChair->setAlias('backflow_device_hazard_cold_feed_pedi_chair');
        $manager->persist($choiceColdFeedPediChair);

        /** @var DynamicFieldDropboxChoices $choiceColdWaterSupply */
        $choiceColdWaterSupply = new DynamicFieldDropboxChoices();
        $choiceColdWaterSupply->setField($hazard);
        $choiceColdWaterSupply->setOptionValue('Cold Water Supply');
        $choiceColdWaterSupply->setAlias('backflow_device_hazard_cold_water_supply');
        $manager->persist($choiceColdWaterSupply);

        /** @var DynamicFieldDropboxChoices $choiceCommercialFeed */
        $choiceCommercialFeed = new DynamicFieldDropboxChoices();
        $choiceCommercialFeed->setField($hazard);
        $choiceCommercialFeed->setOptionValue('Commercial Feed');
        $choiceCommercialFeed->setAlias('backflow_device_hazard_commercial_feed');
        $manager->persist($choiceCommercialFeed);

        /** @var DynamicFieldDropboxChoices $choiceCompressor */
        $choiceCompressor = new DynamicFieldDropboxChoices();
        $choiceCompressor->setField($hazard);
        $choiceCompressor->setOptionValue('Compressor');
        $choiceCompressor->setAlias('backflow_device_hazard_compressor');
        $manager->persist($choiceCompressor);

        /** @var DynamicFieldDropboxChoices $choiceCoolingTower */
        $choiceCoolingTower = new DynamicFieldDropboxChoices();
        $choiceCoolingTower->setField($hazard);
        $choiceCoolingTower->setOptionValue('Cooling Tower');
        $choiceCoolingTower->setAlias('backflow_device_hazard_cooling_tower');
        $manager->persist($choiceCoolingTower);

        /** @var DynamicFieldDropboxChoices $choiceDentalEquipment */
        $choiceDentalEquipment = new DynamicFieldDropboxChoices();
        $choiceDentalEquipment->setField($hazard);
        $choiceDentalEquipment->setOptionValue('Dental Equipment');
        $choiceDentalEquipment->setAlias('backflow_device_hazard_dental_equipment');
        $manager->persist($choiceDentalEquipment);

        /** @var DynamicFieldDropboxChoices $choiceDialysis */
        $choiceDialysis = new DynamicFieldDropboxChoices();
        $choiceDialysis->setField($hazard);
        $choiceDialysis->setOptionValue('Dialysis');
        $choiceDialysis->setAlias('backflow_device_hazard_dialysis');
        $manager->persist($choiceDialysis);

        /** @var DynamicFieldDropboxChoices $choiceDishwasher */
        $choiceDishwasher = new DynamicFieldDropboxChoices();
        $choiceDishwasher->setField($hazard);
        $choiceDishwasher->setOptionValue('Dishwasher');
        $choiceDishwasher->setAlias('backflow_device_hazard_dishwasher');
        $manager->persist($choiceDishwasher);

        /** @var DynamicFieldDropboxChoices $choiceDomestic */
        $choiceDomestic = new DynamicFieldDropboxChoices();
        $choiceDomestic->setField($hazard);
        $choiceDomestic->setOptionValue('Domestic');
        $choiceDomestic->setAlias('backflow_device_hazard_domestic');
        $manager->persist($choiceDomestic);

        /** @var DynamicFieldDropboxChoices $choiceDomesticByPass */
        $choiceDomesticByPass = new DynamicFieldDropboxChoices();
        $choiceDomesticByPass->setField($hazard);
        $choiceDomesticByPass->setOptionValue('Domestic By-Pass');
        $choiceDomesticByPass->setAlias('backflow_device_hazard_domestic_by_pass');
        $manager->persist($choiceDomesticByPass);

        /** @var DynamicFieldDropboxChoices $choiceDrinkingFountain */
        $choiceDrinkingFountain = new DynamicFieldDropboxChoices();
        $choiceDrinkingFountain->setField($hazard);
        $choiceDrinkingFountain->setOptionValue('Drinking Fountain');
        $choiceDrinkingFountain->setAlias('backflow_device_hazard_drinking_fountain');
        $manager->persist($choiceDrinkingFountain);

        /** @var DynamicFieldDropboxChoices $choiceEquipmentMakeUp */
        $choiceEquipmentMakeUp = new DynamicFieldDropboxChoices();
        $choiceEquipmentMakeUp->setField($hazard);
        $choiceEquipmentMakeUp->setOptionValue('Equipment Make-Up');
        $choiceEquipmentMakeUp->setAlias('backflow_device_hazard_equipment_make_up');
        $manager->persist($choiceEquipmentMakeUp);

        /** @var DynamicFieldDropboxChoices $choiceFilmProcessor */
        $choiceFilmProcessor = new DynamicFieldDropboxChoices();
        $choiceFilmProcessor->setField($hazard);
        $choiceFilmProcessor->setOptionValue('Film Processor');
        $choiceFilmProcessor->setAlias('backflow_device_hazard_film_processor');
        $manager->persist($choiceFilmProcessor);

        /** @var DynamicFieldDropboxChoices $choiceFilterSystem */
        $choiceFilterSystem = new DynamicFieldDropboxChoices();
        $choiceFilterSystem->setField($hazard);
        $choiceFilterSystem->setOptionValue('Filter System');
        $choiceFilterSystem->setAlias('backflow_device_hazard_filter_system');
        $manager->persist($choiceFilterSystem);

        /** @var DynamicFieldDropboxChoices $choiceFilterWater */
        $choiceFilterWater = new DynamicFieldDropboxChoices();
        $choiceFilterWater->setField($hazard);
        $choiceFilterWater->setOptionValue('Filter Water');
        $choiceFilterWater->setAlias('backflow_device_hazard_filter_water');
        $manager->persist($choiceFilterWater);

        /** @var DynamicFieldDropboxChoices $choiceFireAntiFreeze */
        $choiceFireAntiFreeze = new DynamicFieldDropboxChoices();
        $choiceFireAntiFreeze->setField($hazard);
        $choiceFireAntiFreeze->setOptionValue('Fire Anti-Freeze');
        $choiceFireAntiFreeze->setAlias('backflow_device_hazard_fire_anti_freeze');
        $manager->persist($choiceFireAntiFreeze);

        /** @var DynamicFieldDropboxChoices $choiceFireByPass */
        $choiceFireByPass = new DynamicFieldDropboxChoices();
        $choiceFireByPass->setField($hazard);
        $choiceFireByPass->setOptionValue('Fire By-Pass');
        $choiceFireByPass->setAlias('backflow_device_hazard_fire_by_pass');
        $manager->persist($choiceFireByPass);

        /** @var DynamicFieldDropboxChoices $choiceFireProtection */
        $choiceFireProtection = new DynamicFieldDropboxChoices();
        $choiceFireProtection->setField($hazard);
        $choiceFireProtection->setOptionValue('Fire Protection');
        $choiceFireProtection->setAlias('backflow_device_hazard_fire_protection');
        $manager->persist($choiceFireProtection);

        /** @var DynamicFieldDropboxChoices $choiceRadiantHeat */
        $choiceRadiantHeat = new DynamicFieldDropboxChoices();
        $choiceRadiantHeat->setField($hazard);
        $choiceRadiantHeat->setOptionValue('Radiant Heat');
        $choiceRadiantHeat->setAlias('backflow_device_hazard_radiant_heat');
        $manager->persist($choiceRadiantHeat);

        /** @var DynamicFieldDropboxChoices $choiceFountain */
        $choiceFountain = new DynamicFieldDropboxChoices();
        $choiceFountain->setField($hazard);
        $choiceFountain->setOptionValue('Fountain');
        $choiceFountain->setAlias('backflow_device_hazard_fountain');
        $manager->persist($choiceFountain);

        /** @var DynamicFieldDropboxChoices $choiceGarbageDisposal */
        $choiceGarbageDisposal = new DynamicFieldDropboxChoices();
        $choiceGarbageDisposal->setField($hazard);
        $choiceGarbageDisposal->setOptionValue('Garbage Disposal');
        $choiceGarbageDisposal->setAlias('backflow_device_hazard_garbage_disposal');
        $manager->persist($choiceGarbageDisposal);

        /** @var DynamicFieldDropboxChoices $choiceGenerator */
        $choiceGenerator = new DynamicFieldDropboxChoices();
        $choiceGenerator->setField($hazard);
        $choiceGenerator->setOptionValue('Generator');
        $choiceGenerator->setAlias('backflow_device_hazard_second_generator');
        $manager->persist($choiceGenerator);

        /** @var DynamicFieldDropboxChoices $choiceHoodwash */
        $choiceHoodwash = new DynamicFieldDropboxChoices();
        $choiceHoodwash->setField($hazard);
        $choiceHoodwash->setOptionValue('Hoodwash');
        $choiceHoodwash->setAlias('backflow_device_hazard_hoodwash');
        $manager->persist($choiceHoodwash);

        /** @var DynamicFieldDropboxChoices $choiceHoseBibb */
        $choiceHoseBibb = new DynamicFieldDropboxChoices();
        $choiceHoseBibb->setField($hazard);
        $choiceHoseBibb->setOptionValue('Hose Bibb');
        $choiceHoseBibb->setAlias('backflow_device_hazard_hose_bibb');
        $manager->persist($choiceHoseBibb);

        /** @var DynamicFieldDropboxChoices $choiceHoseConnection */
        $choiceHoseConnection = new DynamicFieldDropboxChoices();
        $choiceHoseConnection->setField($hazard);
        $choiceHoseConnection->setOptionValue('Hose Connection');
        $choiceHoseConnection->setAlias('backflow_device_hazard_hose_connection');
        $manager->persist($choiceHoseConnection);

        /** @var DynamicFieldDropboxChoices $choiceHoseReel */
        $choiceHoseReel = new DynamicFieldDropboxChoices();
        $choiceHoseReel->setField($hazard);
        $choiceHoseReel->setOptionValue('Hose Reel');
        $choiceHoseReel->setAlias('backflow_device_hazard_hose_reel');
        $manager->persist($choiceHoseReel);

        /** @var DynamicFieldDropboxChoices $choiceHotFeedPediChair */
        $choiceHotFeedPediChair = new DynamicFieldDropboxChoices();
        $choiceHotFeedPediChair->setField($hazard);
        $choiceHotFeedPediChair->setOptionValue('Hot Feed Pedi Chair');
        $choiceHotFeedPediChair->setAlias('backflow_device_hazard_hot_feed_pedi_chair');
        $manager->persist($choiceHotFeedPediChair);

        /** @var DynamicFieldDropboxChoices $choiceHotTub */
        $choiceHotTub = new DynamicFieldDropboxChoices();
        $choiceHotTub->setField($hazard);
        $choiceHotTub->setOptionValue('Hot Tub');
        $choiceHotTub->setAlias('backflow_device_hazard_hot_tub');
        $manager->persist($choiceHotTub);

        /** @var DynamicFieldDropboxChoices $choiceHotWaterSupply */
        $choiceHotWaterSupply = new DynamicFieldDropboxChoices();
        $choiceHotWaterSupply->setField($hazard);
        $choiceHotWaterSupply->setOptionValue('Hot Water Supply');
        $choiceHotWaterSupply->setAlias('backflow_device_hazard_hot_water_supply');
        $manager->persist($choiceHotWaterSupply);

        /** @var DynamicFieldDropboxChoices $choiceHumidfier */
        $choiceHumidfier = new DynamicFieldDropboxChoices();
        $choiceHumidfier->setField($hazard);
        $choiceHumidfier->setOptionValue('Humidfier');
        $choiceHumidfier->setAlias('backflow_device_hazard_humidfier');
        $manager->persist($choiceHumidfier);

        /** @var DynamicFieldDropboxChoices $choiceHVAC */
        $choiceHVACEquip = new DynamicFieldDropboxChoices();
        $choiceHVACEquip->setField($hazard);
        $choiceHVACEquip->setOptionValue('HVAC Equip.');
        $choiceHVACEquip->setAlias('backflow_device_hazard_hvac_equip');
        $manager->persist($choiceHVACEquip);

        /** @var DynamicFieldDropboxChoices $choiceHydrant */
        $choiceHydrant = new DynamicFieldDropboxChoices();
        $choiceHydrant->setField($hazard);
        $choiceHydrant->setOptionValue('Hydrant');
        $choiceHydrant->setAlias('backflow_device_hazard_hydrant');
        $manager->persist($choiceHydrant);

        /** @var DynamicFieldDropboxChoices $choiceIceMachine */
        $choiceIceMachine = new DynamicFieldDropboxChoices();
        $choiceIceMachine->setField($hazard);
        $choiceIceMachine->setOptionValue('Ice Machine');
        $choiceIceMachine->setAlias('backflow_device_hazard_ice_machine');
        $manager->persist($choiceIceMachine);

        /** @var DynamicFieldDropboxChoices $choiceIrrigation */
        $choiceIrrigation = new DynamicFieldDropboxChoices();
        $choiceIrrigation->setField($hazard);
        $choiceIrrigation->setOptionValue('Irrigation');
        $choiceIrrigation->setAlias('backflow_device_hazard_irrigation');
        $manager->persist($choiceIrrigation);

        /** @var DynamicFieldDropboxChoices $choiceJockeyPump */
        $choiceJockeyPump = new DynamicFieldDropboxChoices();
        $choiceJockeyPump->setField($hazard);
        $choiceJockeyPump->setOptionValue('Jockey Pump');
        $choiceJockeyPump->setAlias('backflow_device_hazard_jockey_pump');
        $manager->persist($choiceJockeyPump);

        /** @var DynamicFieldDropboxChoices $choiceLaboratory */
        $choiceLaboratory = new DynamicFieldDropboxChoices();
        $choiceLaboratory->setField($hazard);
        $choiceLaboratory->setOptionValue('Laboratory');
        $choiceLaboratory->setAlias('backflow_device_hazard_laboratory');
        $manager->persist($choiceLaboratory);
        
        /** @var DynamicFieldDropboxChoices $choiceLaundryCold */
        $choiceLaundryCold = new DynamicFieldDropboxChoices();
        $choiceLaundryCold->setField($hazard);
        $choiceLaundryCold->setOptionValue('Laundry Cold');
        $choiceLaundryCold->setAlias('backflow_device_hazard_laundry_cold');
        $manager->persist($choiceLaundryCold);
        
        /** @var DynamicFieldDropboxChoices $choiceLaundryHot */
        $choiceLaundryHot = new DynamicFieldDropboxChoices();
        $choiceLaundryHot->setField($hazard);
        $choiceLaundryHot->setOptionValue('Laundry Hot');
        $choiceLaundryHot->setAlias('backflow_device_hazard_laundry_hot');
        $manager->persist($choiceLaundryHot);
        
        /** @var DynamicFieldDropboxChoices $choiceMakeUpWater */
        $choiceMakeUpWater = new DynamicFieldDropboxChoices();
        $choiceMakeUpWater->setField($hazard);
        $choiceMakeUpWater->setOptionValue('Make Up Water');
        $choiceMakeUpWater->setAlias('backflow_device_hazard_make_up_water');
        $manager->persist($choiceMakeUpWater);

        /** @var DynamicFieldDropboxChoices $choiceMeltTank */
        $choiceMeltTank = new DynamicFieldDropboxChoices();
        $choiceMeltTank->setField($hazard);
        $choiceMeltTank->setOptionValue('Melt Tank');
        $choiceMeltTank->setAlias('backflow_device_hazard_melt_tank');
        $manager->persist($choiceMeltTank);
        
        /** @var DynamicFieldDropboxChoices $choiceMisters */
        $choiceMisters = new DynamicFieldDropboxChoices();
        $choiceMisters->setField($hazard);
        $choiceMisters->setOptionValue('Misters');
        $choiceMisters->setAlias('backflow_device_hazard_misters');
        $manager->persist($choiceMisters);
        
        /** @var DynamicFieldDropboxChoices $choiceMRI */
        $choiceMRI = new DynamicFieldDropboxChoices();
        $choiceMRI->setField($hazard);
        $choiceMRI->setOptionValue('MRI');
        $choiceMRI->setAlias('backflow_device_hazard_mri');
        $manager->persist($choiceMRI);

        /** @var DynamicFieldDropboxChoices $choiceOutsidePond */
        $choiceOutsidePond = new DynamicFieldDropboxChoices();
        $choiceOutsidePond->setField($hazard);
        $choiceOutsidePond->setOptionValue('Outside Pond');
        $choiceOutsidePond->setAlias('backflow_device_hazard_outside_pond');
        $manager->persist($choiceOutsidePond);

        /** @var DynamicFieldDropboxChoices $choicePhotoDeveloper */
        $choicePhotoDeveloper = new DynamicFieldDropboxChoices();
        $choicePhotoDeveloper->setField($hazard);
        $choicePhotoDeveloper->setOptionValue('Photo Developer');
        $choicePhotoDeveloper->setAlias('backflow_device_hazard_photo_developer');
        $manager->persist($choicePhotoDeveloper);

        /** @var DynamicFieldDropboxChoices $choice2 */
        $choice2 = new DynamicFieldDropboxChoices();
        $choice2->setField($hazard);
        $choice2->setOptionValue('Plaster Trimmer');
        $choice2->setAlias('backflow_device_hazard_plaster_trimmer');
        $manager->persist($choice2);

        /** @var DynamicFieldDropboxChoices $choicePondFeed */
        $choicePondFeed = new DynamicFieldDropboxChoices();
        $choicePondFeed->setField($hazard);
        $choicePondFeed->setOptionValue('Pond Feed');
        $choicePondFeed->setAlias('backflow_device_hazard_pond_feed');
        $manager->persist($choicePondFeed);

        /** @var DynamicFieldDropboxChoices $choice4 */
        $choice4 = new DynamicFieldDropboxChoices();
        $choice4->setField($hazard);
        $choice4->setOptionValue('Pool Feed');
        $choice4->setAlias('backflow_device_hazard_pool_feed');
        $manager->persist($choice4);
        
        /** @var DynamicFieldDropboxChoices $choicePortableHydrant */
        $choicePortableHydrant = new DynamicFieldDropboxChoices();
        $choicePortableHydrant->setField($hazard);
        $choicePortableHydrant->setOptionValue('Portable Hydrant');
        $choicePortableHydrant->setAlias('backflow_device_hazard_portable_hydrant');
        $manager->persist($choicePortableHydrant);

        /** @var DynamicFieldDropboxChoices $choicePotableWater */
        $choicePotableWater = new DynamicFieldDropboxChoices();
        $choicePotableWater->setField($hazard);
        $choicePotableWater->setOptionValue('Potable Water');
        $choicePotableWater->setAlias('backflow_device_hazard_potable_water');
        $manager->persist($choicePotableWater);

        /** @var DynamicFieldDropboxChoices $choicePressureWater */
        $choicePressureWater = new DynamicFieldDropboxChoices();
        $choicePressureWater->setField($hazard);
        $choicePressureWater->setOptionValue('Pressure Water');
        $choicePressureWater->setAlias('backflow_device_hazard_pressure_water');
        $manager->persist($choicePressureWater);
        
        /** @var DynamicFieldDropboxChoices $choicePressureWater */
        $choicePressureWater = new DynamicFieldDropboxChoices();
        $choicePressureWater->setField($hazard);
        $choicePressureWater->setOptionValue('Pressure Washer');
        $choicePressureWater->setAlias('backflow_device_hazard_pressure_washer');
        $manager->persist($choicePressureWater);
        
        /** @var DynamicFieldDropboxChoices $choiceProcessWater */
        $choiceProcessWater = new DynamicFieldDropboxChoices();
        $choiceProcessWater->setField($hazard);
        $choiceProcessWater->setOptionValue('Process Water');
        $choiceProcessWater->setAlias('backflow_device_hazard_process_water');
        $manager->persist($choiceProcessWater);
        
        /** @var DynamicFieldDropboxChoices $choicePressureRectifier */
        $choicePressureRectifier = new DynamicFieldDropboxChoices();
        $choicePressureRectifier->setField($hazard);
        $choicePressureRectifier->setOptionValue('Pressure Rectifier');
        $choicePressureRectifier->setAlias('backflow_device_hazard_pressure_rectifier');
        $manager->persist($choicePressureRectifier);

        /** @var DynamicFieldDropboxChoices $choice1 */
        $choice1 = new DynamicFieldDropboxChoices();
        $choice1->setField($hazard);
        $choice1->setOptionValue('Pressure Reducer');
        $choice1->setAlias('backflow_device_hazard_pressure_reducer');
        $manager->persist($choice1);

        /** @var DynamicFieldDropboxChoices $choiceROSystem */
        $choiceROSystem = new DynamicFieldDropboxChoices();
        $choiceROSystem->setField($hazard);
        $choiceROSystem->setOptionValue('RO System');
        $choiceROSystem->setAlias('backflow_device_hazard_ro_system');
        $manager->persist($choiceROSystem);

        /** @var DynamicFieldDropboxChoices $choiceSAA */
        $choiceSAA = new DynamicFieldDropboxChoices();
        $choiceSAA->setField($hazard);
        $choiceSAA->setOptionValue('SAA');
        $choiceSAA->setAlias('backflow_device_hazard_saa');
        $manager->persist($choiceSAA);
        
        /** @var DynamicFieldDropboxChoices $choiceSanitationFlush */
        $choiceSanitationFlush = new DynamicFieldDropboxChoices();
        $choiceSanitationFlush->setField($hazard);
        $choiceSanitationFlush->setOptionValue('Sanitation Flush');
        $choiceSanitationFlush->setAlias('backflow_device_hazard_sanitation_flush');
        $manager->persist($choiceSanitationFlush);

        /** @var DynamicFieldDropboxChoices $choiceSillcock */
        $choiceSillcock = new DynamicFieldDropboxChoices();
        $choiceSillcock->setField($hazard);
        $choiceSillcock->setOptionValue('Sillcock');
        $choiceSillcock->setAlias('backflow_device_hazard_sillcock');
        $manager->persist($choiceSillcock);

        /** @var DynamicFieldDropboxChoices $choiceSoapDispenser */
        $choiceSoapDispenser = new DynamicFieldDropboxChoices();
        $choiceSoapDispenser->setField($hazard);
        $choiceSoapDispenser->setOptionValue('Soap Dispenser');
        $choiceSoapDispenser->setAlias('backflow_device_hazard_soap_dispenser');
        $manager->persist($choiceSoapDispenser);

        /** @var DynamicFieldDropboxChoices $choiceSodaEquip */
        $choiceSodaEquip = new DynamicFieldDropboxChoices();
        $choiceSodaEquip->setField($hazard);
        $choiceSodaEquip->setOptionValue('Soda Equip');
        $choiceSodaEquip->setAlias('backflow_device_hazard_soda_equip');
        $manager->persist($choiceSodaEquip);

        /** @var DynamicFieldDropboxChoices $choiceSteamer */
        $choiceSteamer = new DynamicFieldDropboxChoices();
        $choiceSteamer->setField($hazard);
        $choiceSteamer->setOptionValue('Steamer');
        $choiceSteamer->setAlias('backflow_device_hazard_steamer');
        $manager->persist($choiceSteamer);

        /** @var DynamicFieldDropboxChoices $choiceSterilizer */
        $choiceSterilizer = new DynamicFieldDropboxChoices();
        $choiceSterilizer->setField($hazard);
        $choiceSterilizer->setOptionValue('Sterilizer');
        $choiceSterilizer->setAlias('backflow_device_hazard_sterilizer');
        $manager->persist($choiceSterilizer);

        /** @var DynamicFieldDropboxChoices $choiceTankFill */
        $choiceTankFill = new DynamicFieldDropboxChoices();
        $choiceTankFill->setField($hazard);
        $choiceTankFill->setOptionValue('Tank Fill');
        $choiceTankFill->setAlias('backflow_device_hazard_tank_fill');
        $manager->persist($choiceTankFill);

        /** @var DynamicFieldDropboxChoices $choiceTankWasher */
        $choiceTankWasher = new DynamicFieldDropboxChoices();
        $choiceTankWasher->setField($hazard);
        $choiceTankWasher->setOptionValue('Tank Washer');
        $choiceTankWasher->setAlias('backflow_device_hazard_tank_washer');
        $manager->persist($choiceTankWasher);

        /** @var DynamicFieldDropboxChoices $choiceTrashCompactor */
        $choiceTrashCompactor = new DynamicFieldDropboxChoices();
        $choiceTrashCompactor->setField($hazard);
        $choiceTrashCompactor->setOptionValue('Trash Compactor');
        $choiceTrashCompactor->setAlias('backflow_device_hazard_trash_compactor');
        $manager->persist($choiceTrashCompactor);

        /** @var DynamicFieldDropboxChoices $choiceTrashShoot */
        $choiceTrashShoot = new DynamicFieldDropboxChoices();
        $choiceTrashShoot->setField($hazard);
        $choiceTrashShoot->setOptionValue('Trash Shoot');
        $choiceTrashShoot->setAlias('backflow_device_hazard_trash_shoot');
        $manager->persist($choiceTrashShoot);

        /** @var DynamicFieldDropboxChoices $choiceTruckFill */
        $choiceTruckFill = new DynamicFieldDropboxChoices();
        $choiceTruckFill->setField($hazard);
        $choiceTruckFill->setOptionValue('Truck Fill');
        $choiceTruckFill->setAlias('backflow_device_hazard_truck_fill');
        $manager->persist($choiceTruckFill);

        /** @var DynamicFieldDropboxChoices $choiceUtilitySink */
        $choiceUtilitySink = new DynamicFieldDropboxChoices();
        $choiceUtilitySink->setField($hazard);
        $choiceUtilitySink->setOptionValue('Utility Sink');
        $choiceUtilitySink->setAlias('backflow_device_hazard_utility_sink');
        $manager->persist($choiceUtilitySink);

        /** @var DynamicFieldDropboxChoices $choiceVacuumPump */
        $choiceVaccumPump = new DynamicFieldDropboxChoices();
        $choiceVaccumPump->setField($hazard);
        $choiceVaccumPump->setOptionValue('Vaccum Pump');
        $choiceVaccumPump->setAlias('backflow_device_hazard_vaccum_pump');
        $manager->persist($choiceVaccumPump);

        /** @var DynamicFieldDropboxChoices $choiceWallHydrant */
        $choiceWallHydrant = new DynamicFieldDropboxChoices();
        $choiceWallHydrant->setField($hazard);
        $choiceWallHydrant->setOptionValue('Wall Hydrant');
        $choiceWallHydrant->setAlias('backflow_device_hazard_wall_hydrant');
        $manager->persist($choiceWallHydrant);

        /** @var DynamicFieldDropboxChoices $choiceWashHose */
        $choiceWashHose = new DynamicFieldDropboxChoices();
        $choiceWashHose->setField($hazard);
        $choiceWashHose->setOptionValue('Wash Hose');
        $choiceWashHose->setAlias('backflow_device_hazard_wash_hose');
        $manager->persist($choiceWashHose);

        /** @var DynamicFieldDropboxChoices $choiceWashroom */
        $choiceWashroom = new DynamicFieldDropboxChoices();
        $choiceWashroom->setField($hazard);
        $choiceWashroom->setOptionValue('Washroom');
        $choiceWashroom->setAlias('backflow_device_hazard_washroom');
        $manager->persist($choiceWashroom);

        /** @var DynamicFieldDropboxChoices $choiceWaste */
        $choiceWaste = new DynamicFieldDropboxChoices();
        $choiceWaste->setField($hazard);
        $choiceWaste->setOptionValue('Waste');
        $choiceWaste->setAlias('backflow_device_hazard_waste');
        $manager->persist($choiceWaste);

        /** @var DynamicFieldDropboxChoices $choiceWaterSoftner */
        $choiceWaterSoftner = new DynamicFieldDropboxChoices();
        $choiceWaterSoftner->setField($hazard);
        $choiceWaterSoftner->setOptionValue('Water Softner');
        $choiceWaterSoftner->setAlias('backflow_device_hazard_water_softner');
        $manager->persist($choiceWaterSoftner);

        /** @var DynamicFieldDropboxChoices $choiceXRay */
        $choiceXRay = new DynamicFieldDropboxChoices();
        $choiceXRay->setField($hazard);
        $choiceXRay->setOptionValue('X-Ray');
        $choiceXRay->setAlias('backflow_device_hazard_x_ray');
        $manager->persist($choiceXRay);

        /** @var DynamicFieldDropboxChoices $choiceYardHydrant */
        $choiceYardHydrant = new DynamicFieldDropboxChoices();
        $choiceYardHydrant->setField($hazard);
        $choiceYardHydrant->setOptionValue('Yard Hydrant');
        $choiceYardHydrant->setAlias('backflow_device_hazard_yard_hydrant');
        $manager->persist($choiceYardHydrant);
        
        /** @var DynamicFieldDropboxChoices $choiceFloorHeat */
        $choiceFloorHeat = new DynamicFieldDropboxChoices();
        $choiceFloorHeat->setField($hazard);
        $choiceFloorHeat->setOptionValue('Floor Heat');
        $choiceFloorHeat->setAlias('backflow_device_hazard_floor_heat');
        $manager->persist($choiceFloorHeat);

        /** @var DynamicFieldDropboxChoices $choiceSwimLift */
        $choiceSwimLift = new DynamicFieldDropboxChoices();
        $choiceSwimLift->setField($hazard);
        $choiceSwimLift->setOptionValue('Swim Lift');
        $choiceSwimLift->setAlias('backflow_device_hazard_swim_lift');
        $manager->persist($choiceSwimLift);

        /** @var DynamicFieldDropboxChoices $choiceOther */
        $choiceOther = new DynamicFieldDropboxChoices();
        $choiceOther->setField($hazard);
        $choiceOther->setOptionValue('Other');
        $choiceOther->setAlias('backflow_device_hazard_other');
        $manager->persist($choiceOther);
        /**************  End choices list ***************/


        /** @var DynamicField $orientation */
        $orientation = new DynamicField();
        $orientation->setName('Orientation');
        $orientation->setAlias('orientation');
        $orientation->setType($this->getReference('dynamic-field-type-dropdown'));
        $orientation->setDevice($backflowDevice);
        $manager->persist($orientation);
        
        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceHorizontal */
        $choiceHorizontal = new DynamicFieldDropboxChoices();
        $choiceHorizontal->setField($orientation);
        $choiceHorizontal->setOptionValue('Horizontal');
        $choiceHorizontal->setAlias('backflow_device_orientation_horizontal');
        $manager->persist($choiceHorizontal);

        /** @var DynamicFieldDropboxChoices $choiceVertical */
        $choiceVertical = new DynamicFieldDropboxChoices();
        $choiceVertical->setField($orientation);
        $choiceVertical->setOptionValue('Vertical');
        $choiceVertical->setAlias('backflow_device_orientation_vertical');
        $manager->persist($choiceVertical);
        /**************  End choices list ***************/


        /** @var DynamicField $shutOffValve */
        $shutOffValve = new DynamicField();
        $shutOffValve->setName('Shut Off Valve(s)');
        $shutOffValve->setAlias('shut_off_valve');
        $shutOffValve->setType($this->getReference('dynamic-field-type-dropdown'));
        $shutOffValve->setDevice($backflowDevice);
        $manager->persist($shutOffValve);
        
        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceBall */
        $choiceBall = new DynamicFieldDropboxChoices();
        $choiceBall->setField($shutOffValve);
        $choiceBall->setOptionValue('Ball');
        $choiceBall->setAlias('backflow_device_shut_off_valve_ball');
        $manager->persist($choiceBall);

        /** @var DynamicFieldDropboxChoices $choiceGate */
        $choiceGate = new DynamicFieldDropboxChoices();
        $choiceGate->setField($shutOffValve);
        $choiceGate->setOptionValue('Gate');
        $choiceGate->setAlias('backflow_device_shut_off_valve_gate');
        $manager->persist($choiceGate);

        /** @var DynamicFieldDropboxChoices $choice3 */
        $choice3 = new DynamicFieldDropboxChoices();
        $choice3->setField($shutOffValve);
        $choice3->setOptionValue('OS&Y');
        $choice3->setAlias('backflow_device_shut_off_valve_os&y');
        $manager->persist($choice3);

        /** @var DynamicFieldDropboxChoices $choiceNRS */
        $choiceNRS = new DynamicFieldDropboxChoices();
        $choiceNRS->setField($shutOffValve);
        $choiceNRS->setOptionValue('NRS');
        $choiceNRS->setAlias('backflow_device_shut_off_valve_nrs');
        $manager->persist($choiceNRS);

        /** @var DynamicFieldDropboxChoices $choiceButterfly */
        $choiceButterfly = new DynamicFieldDropboxChoices();
        $choiceButterfly->setField($shutOffValve);
        $choiceButterfly->setOptionValue('Butterfly');
        $choiceButterfly->setAlias('backflow_device_shut_off_valve_butterfly');
        $manager->persist($choiceButterfly);

        /** @var DynamicFieldDropboxChoices $choice6 */
        $choice6 = new DynamicFieldDropboxChoices();
        $choice6->setField($shutOffValve);
        $choice6->setOptionValue('Waffer');
        $choice6->setAlias('backflow_device_shut_off_valve_waffer');
        $manager->persist($choice6);

        /** @var DynamicFieldDropboxChoices $choiceIntegrated */
        $choiceIntegrated = new DynamicFieldDropboxChoices();
        $choiceIntegrated->setField($shutOffValve);
        $choiceIntegrated->setOptionValue('Integrated');
        $choiceIntegrated->setAlias('backflow_device_shut_off_valve_integrated');
        $manager->persist($choiceIntegrated);
        /**************  End choices list ***************/


        /** @var DynamicField $approvedInstallation */
        $approvedInstallation = new DynamicField();
        $approvedInstallation->setName('Approved Installation');
        $approvedInstallation->setAlias('approved_installation');
        $approvedInstallation->setType($this->getReference('dynamic-field-type-dropdown'));
        $approvedInstallation->setDevice($backflowDevice);
        $manager->persist($approvedInstallation);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceYes */
        $choiceYes = new DynamicFieldDropboxChoices();
        $choiceYes->setField($approvedInstallation);
        $choiceYes->setOptionValue('Yes');
        $choiceYes->setAlias('backflow_device_approved_installation_yes');
        $manager->persist($choiceYes);

        /** @var DynamicFieldDropboxChoices $choiceNo */
        $choiceNo = new DynamicFieldDropboxChoices();
        $choiceNo->setField($approvedInstallation);
        $choiceNo->setOptionValue('No');
        $choiceNo->setAlias('backflow_device_approved_installation_no');
        $manager->persist($choiceNo);
        /**************  End choices list ***************/
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 11;
    }
}
