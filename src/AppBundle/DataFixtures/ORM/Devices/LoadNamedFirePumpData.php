<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedFirePumpData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Fire Pump *******/
        /** @var DeviceNamed $firePump */
        $firePump = new DeviceNamed();
        $firePump->setName('Fire Pump');
        $firePump->setAlias('fire_pump');
        $firePump->setSort(2);
        $firePump->setCategory($this->getReference('device-category-fire'));
        $manager->persist($firePump);

        $this->addReference('fire-pump-device', $firePump);
        /************ End device named ****************/



        /************  Dynamic fields list *************/
        /** @var DynamicField $shaft */
        $shaft = new DynamicField();
        $shaft->setName('Shaft');
        $shaft->setAlias('shaft');
        $shaft->setType($this->getReference('dynamic-field-type-dropdown'));
        $shaft->setDevice($firePump);
        $manager->persist($shaft);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceHorizontalSplitCase */
        $choiceHorizontalSplitCase = new DynamicFieldDropboxChoices();
        $choiceHorizontalSplitCase->setField($shaft);
        $choiceHorizontalSplitCase->setOptionValue('Horizontal Split Case');
        $choiceHorizontalSplitCase->setAlias('fire_pump_shaft_horizontal_split_case');
        $manager->persist($choiceHorizontalSplitCase);

        /** @var DynamicFieldDropboxChoices $choiceVerticalInLine */
        $choiceVerticalInLine = new DynamicFieldDropboxChoices();
        $choiceVerticalInLine->setField($shaft);
        $choiceVerticalInLine->setOptionValue('Vertical In-Line');
        $choiceVerticalInLine->setAlias('fire_pump_shaft_vertical_in_line');
        $manager->persist($choiceVerticalInLine);

        /** @var DynamicFieldDropboxChoices $choiceEndSuction */
        $choiceEndSuction = new DynamicFieldDropboxChoices();
        $choiceEndSuction->setField($shaft);
        $choiceEndSuction->setOptionValue('End Suction');
        $choiceEndSuction->setAlias('fire_pump_shaft_end_suction');
        $manager->persist($choiceEndSuction);
        /**************  End choices list ***************/


        /** @var DynamicField $make */
        $make = new DynamicField();
        $make->setName('Make');
        $make->setAlias('fire_pump_make');
        $make->setType($this->getReference('dynamic-field-type-dropdown'));
        $make->setDevice($firePump);
        $manager->persist($make);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceAllisChalmers */
        $choiceAllisChalmers = new DynamicFieldDropboxChoices();
        $choiceAllisChalmers->setField($make);
        $choiceAllisChalmers->setOptionValue('Allis Chalmers');
        $choiceAllisChalmers->setAlias('fire_pump_make_allis_chalmers');
        $manager->persist($choiceAllisChalmers);

        /** @var DynamicFieldDropboxChoices $choiceAurora */
        $choiceAurora = new DynamicFieldDropboxChoices();
        $choiceAurora->setField($make);
        $choiceAurora->setOptionValue('Aurora');
        $choiceAurora->setAlias('fire_pump_make_aurora');
        $manager->persist($choiceAurora);

        /** @var DynamicFieldDropboxChoices $choiceChicago */
        $choiceChicago = new DynamicFieldDropboxChoices();
        $choiceChicago->setField($make);
        $choiceChicago->setOptionValue('Chicago');
        $choiceChicago->setAlias('fire_pump_make_chicago');
        $manager->persist($choiceChicago);

        /** @var DynamicFieldDropboxChoices $choiceFairbanksMorse */
        $choiceFairbanksMorse = new DynamicFieldDropboxChoices();
        $choiceFairbanksMorse->setField($make);
        $choiceFairbanksMorse->setOptionValue('Fairbanks Morse');
        $choiceFairbanksMorse->setAlias('fire_pump_make_fairbanks_morse');
        $manager->persist($choiceFairbanksMorse);

        /** @var DynamicFieldDropboxChoices $choiceLeCourtney */
        $choiceLeCourtney = new DynamicFieldDropboxChoices();
        $choiceLeCourtney->setField($make);
        $choiceLeCourtney->setOptionValue('Le Courtney');
        $choiceLeCourtney->setAlias('fire_pump_make_le_courtney');
        $manager->persist($choiceLeCourtney);

        /** @var DynamicFieldDropboxChoices $choicePatterson */
        $choicePatterson = new DynamicFieldDropboxChoices();
        $choicePatterson->setField($make);
        $choicePatterson->setOptionValue('Patterson');
        $choicePatterson->setAlias('fire_pump_make_patterson');
        $manager->persist($choicePatterson);

        /** @var DynamicFieldDropboxChoices $choicePeerless */
        $choicePeerless = new DynamicFieldDropboxChoices();
        $choicePeerless->setField($make);
        $choicePeerless->setOptionValue('Peerless');
        $choicePeerless->setAlias('fire_pump_make_peerless');
        $manager->persist($choicePeerless);

        /** @var DynamicFieldDropboxChoices $choiceRediBuffalo */
        $choiceRediBuffalo = new DynamicFieldDropboxChoices();
        $choiceRediBuffalo->setField($make);
        $choiceRediBuffalo->setOptionValue('Redi-Buffalo');
        $choiceRediBuffalo->setAlias('fire_pump_make_redi_buffalo');
        $manager->persist($choiceRediBuffalo);

        /** @var DynamicFieldDropboxChoices $choiceSPP */
        $choiceSPP = new DynamicFieldDropboxChoices();
        $choiceSPP->setField($make);
        $choiceSPP->setOptionValue('SPP');
        $choiceSPP->setAlias('fire_pump_make_spp');
        $manager->persist($choiceSPP);

        /** @var DynamicFieldDropboxChoices $choiceVertical */
        $choiceVertical = new DynamicFieldDropboxChoices();
        $choiceVertical->setField($make);
        $choiceVertical->setOptionValue('Vertical');
        $choiceVertical->setAlias('fire_pump_make_vertical');
        $manager->persist($choiceVertical);

        /** @var DynamicFieldDropboxChoices $choiceMarathon */
        $choiceMarathon = new DynamicFieldDropboxChoices();
        $choiceMarathon->setField($make);
        $choiceMarathon->setOptionValue('Marathon');
        $choiceMarathon->setAlias('fire_pump_make_marathon');
        $manager->persist($choiceMarathon);

        /** @var DynamicFieldDropboxChoices $choice1500gpm */
        $choice1500gpm = new DynamicFieldDropboxChoices();
        $choice1500gpm->setField($make);
        $choice1500gpm->setOptionValue('1500gpm');
        $choice1500gpm->setAlias('fire_pump_make_1500gpm');
        $manager->persist($choice1500gpm);
        /**************  End choices list ***************/


        /** @var DynamicField $model */
        $model = new DynamicField();
        $model->setName('Model');
        $model->setAlias('model');
        $model->setType($this->getReference('dynamic-field-type-text'));
        $model->setDevice($firePump);
        $manager->persist($model);


        /** @var DynamicField $serialNumber */
        $serialNumber = new DynamicField();
        $serialNumber->setName('Serial Number');
        $serialNumber->setAlias('serial_number');
        $serialNumber->setType($this->getReference('dynamic-field-type-text'));
        $serialNumber->setDevice($firePump);
        $manager->persist($serialNumber);


        /** @var DynamicField $ratedGPM */
        $ratedGPM = new DynamicField();
        $ratedGPM->setName('Rated GPM');
        $ratedGPM->setAlias('rated_gpm');
        $ratedGPM->setType($this->getReference('dynamic-field-type-dropdown'));
        $ratedGPM->setDevice($firePump);
        $manager->persist($ratedGPM);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice25 */
        $choice25 = new DynamicFieldDropboxChoices();
        $choice25->setField($ratedGPM);
        $choice25->setOptionValue('25');
        $choice25->setAlias('fire_pump_rated_gpm_25');
        $manager->persist($choice25);

        /** @var DynamicFieldDropboxChoices $choice50 */
        $choice50 = new DynamicFieldDropboxChoices();
        $choice50->setField($ratedGPM);
        $choice50->setOptionValue('50');
        $choice50->setAlias('fire_pump_rated_gpm_50');
        $manager->persist($choice50);

        /** @var DynamicFieldDropboxChoices $choice75 */
        $choice75 = new DynamicFieldDropboxChoices();
        $choice75->setField($ratedGPM);
        $choice75->setOptionValue('75');
        $choice75->setAlias('fire_pump_rated_gpm_75');
        $manager->persist($choice75);

        /** @var DynamicFieldDropboxChoices $choice100 */
        $choice100 = new DynamicFieldDropboxChoices();
        $choice100->setField($ratedGPM);
        $choice100->setOptionValue('100');
        $choice100->setAlias('fire_pump_rated_gpm_100');
        $manager->persist($choice100);

        /** @var DynamicFieldDropboxChoices $choice150 */
        $choice150 = new DynamicFieldDropboxChoices();
        $choice150->setField($ratedGPM);
        $choice150->setOptionValue('150');
        $choice150->setAlias('fire_pump_rated_gpm_150');
        $manager->persist($choice150);

        /** @var DynamicFieldDropboxChoices $choice200 */
        $choice200 = new DynamicFieldDropboxChoices();
        $choice200->setField($ratedGPM);
        $choice200->setOptionValue('200');
        $choice200->setAlias('fire_pump_rated_gpm_200');
        $manager->persist($choice200);

        /** @var DynamicFieldDropboxChoices $choice250 */
        $choice250 = new DynamicFieldDropboxChoices();
        $choice250->setField($ratedGPM);
        $choice250->setOptionValue('250');
        $choice250->setAlias('fire_pump_rated_gpm_250');
        $manager->persist($choice250);

        /** @var DynamicFieldDropboxChoices $choice300 */
        $choice300 = new DynamicFieldDropboxChoices();
        $choice300->setField($ratedGPM);
        $choice300->setOptionValue('300');
        $choice300->setAlias('fire_pump_rated_gpm_300');
        $manager->persist($choice300);

        /** @var DynamicFieldDropboxChoices $choice400 */
        $choice400 = new DynamicFieldDropboxChoices();
        $choice400->setField($ratedGPM);
        $choice400->setOptionValue('400');
        $choice400->setAlias('fire_pump_rated_gpm_400');
        $manager->persist($choice400);

        /** @var DynamicFieldDropboxChoices $choice450 */
        $choice450 = new DynamicFieldDropboxChoices();
        $choice450->setField($ratedGPM);
        $choice450->setOptionValue('450');
        $choice450->setAlias('fire_pump_rated_gpm_450');
        $manager->persist($choice450);

        /** @var DynamicFieldDropboxChoices $choice500 */
        $choice500 = new DynamicFieldDropboxChoices();
        $choice500->setField($ratedGPM);
        $choice500->setOptionValue('500');
        $choice500->setAlias('fire_pump_rated_gpm_500');
        $manager->persist($choice500);

        /** @var DynamicFieldDropboxChoices $choice750 */
        $choice750 = new DynamicFieldDropboxChoices();
        $choice750->setField($ratedGPM);
        $choice750->setOptionValue('750');
        $choice750->setAlias('fire_pump_rated_gpm_750');
        $manager->persist($choice750);

        /** @var DynamicFieldDropboxChoices $choice1000 */
        $choice1000 = new DynamicFieldDropboxChoices();
        $choice1000->setField($ratedGPM);
        $choice1000->setOptionValue('1000');
        $choice1000->setAlias('fire_pump_rated_gpm_1000');
        $manager->persist($choice1000);

        /** @var DynamicFieldDropboxChoices $choice1250 */
        $choice1250 = new DynamicFieldDropboxChoices();
        $choice1250->setField($ratedGPM);
        $choice1250->setOptionValue('1250');
        $choice1250->setAlias('fire_pump_rated_gpm_1250');
        $manager->persist($choice1250);

        /** @var DynamicFieldDropboxChoices $choice1500 */
        $choice1500 = new DynamicFieldDropboxChoices();
        $choice1500->setField($ratedGPM);
        $choice1500->setOptionValue('1500');
        $choice1500->setAlias('fire_pump_rated_gpm_1500');
        $manager->persist($choice1500);

        /** @var DynamicFieldDropboxChoices $choice2000 */
        $choice2000 = new DynamicFieldDropboxChoices();
        $choice2000->setField($ratedGPM);
        $choice2000->setOptionValue('2000');
        $choice2000->setAlias('fire_pump_rated_gpm_2000');
        $manager->persist($choice2000);

        /** @var DynamicFieldDropboxChoices $choice2500 */
        $choice2500 = new DynamicFieldDropboxChoices();
        $choice2500->setField($ratedGPM);
        $choice2500->setOptionValue('2500');
        $choice2500->setAlias('fire_pump_rated_gpm_2500');
        $manager->persist($choice2500);

        /** @var DynamicFieldDropboxChoices $choice3000 */
        $choice3000 = new DynamicFieldDropboxChoices();
        $choice3000->setField($ratedGPM);
        $choice3000->setOptionValue('3000');
        $choice3000->setAlias('fire_pump_rated_gpm_3000');
        $manager->persist($choice3000);

        /** @var DynamicFieldDropboxChoices $choice3500 */
        $choice3500 = new DynamicFieldDropboxChoices();
        $choice3500->setField($ratedGPM);
        $choice3500->setOptionValue('3500');
        $choice3500->setAlias('fire_pump_rated_gpm_3500');
        $manager->persist($choice3500);

        /** @var DynamicFieldDropboxChoices $choice4000 */
        $choice4000 = new DynamicFieldDropboxChoices();
        $choice4000->setField($ratedGPM);
        $choice4000->setOptionValue('4000');
        $choice4000->setAlias('fire_pump_rated_gpm_4000');
        $manager->persist($choice4000);

        /** @var DynamicFieldDropboxChoices $choice4500 */
        $choice4500 = new DynamicFieldDropboxChoices();
        $choice4500->setField($ratedGPM);
        $choice4500->setOptionValue('4500');
        $choice4500->setAlias('fire_pump_rated_gpm_4500');
        $manager->persist($choice4500);

        /** @var DynamicFieldDropboxChoices $choice5000 */
        $choice5000 = new DynamicFieldDropboxChoices();
        $choice5000->setField($ratedGPM);
        $choice5000->setOptionValue('5000');
        $choice5000->setAlias('fire_pump_rated_gpm_5000');
        $manager->persist($choice5000);
        /**************  End choices list ***************/


        /** @var DynamicField $ratedPSI */
        $ratedPSI = new DynamicField();
        $ratedPSI->setName('Rated PSI');
        $ratedPSI->setAlias('rated_psi');
        $ratedPSI->setType($this->getReference('dynamic-field-type-text'));
        $ratedPSI->setValidation($this->getReference('validation-app-limit-digits4'));
        $ratedPSI->setDevice($firePump);
        $manager->persist($ratedPSI);


        /** @var DynamicField $pumpListed */
        $pumpListed = new DynamicField();
        $pumpListed->setName('Pump Listed');
        $pumpListed->setAlias('pump_listed');
        $pumpListed->setType($this->getReference('dynamic-field-type-dropdown'));
        $pumpListed->setDevice($firePump);
        $manager->persist($pumpListed);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceFM */
        $choiceFM = new DynamicFieldDropboxChoices();
        $choiceFM->setField($pumpListed);
        $choiceFM->setOptionValue('FM');
        $choiceFM->setAlias('fire_pump_pump_listed_fm');
        $manager->persist($choiceFM);

        /** @var DynamicFieldDropboxChoices $choiceUL */
        $choiceUL = new DynamicFieldDropboxChoices();
        $choiceUL->setField($pumpListed);
        $choiceUL->setOptionValue('UL');
        $choiceUL->setAlias('fire_pump_pump_listed_ul');
        $manager->persist($choiceUL);

        /** @var DynamicFieldDropboxChoices $choiceULC */
        $choiceULC = new DynamicFieldDropboxChoices();
        $choiceULC->setField($pumpListed);
        $choiceULC->setOptionValue('ULC');
        $choiceULC->setAlias('fire_pump_pump_listed_ulc');
        $manager->persist($choiceULC);

        /** @var DynamicFieldDropboxChoices $choiceNone */
        $choiceNone = new DynamicFieldDropboxChoices();
        $choiceNone->setField($pumpListed);
        $choiceNone->setOptionValue('None');
        $choiceNone->setAlias('fire_pump_pump_listed_none');
        $manager->persist($choiceNone);
        /**************  End choices list ***************/


        /** @var DynamicField $choiceApproved */
        $choiceApproved = new DynamicField();
        $choiceApproved->setName('Pump Approved');
        $choiceApproved->setAlias('pump_approved');
        $choiceApproved->setType($this->getReference('dynamic-field-type-dropdown'));
        $choiceApproved->setDevice($firePump);
        $manager->persist($choiceApproved);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceYes */
        $choiceYes = new DynamicFieldDropboxChoices();
        $choiceYes->setField($choiceApproved);
        $choiceYes->setOptionValue('YES');
        $choiceYes->setAlias('fire_pump_pump_approved_yes');
        $manager->persist($choiceYes);

        /** @var DynamicFieldDropboxChoices $choiceNo */
        $choiceNo = new DynamicFieldDropboxChoices();
        $choiceNo->setField($choiceApproved);
        $choiceNo->setOptionValue('NO');
        $choiceNo->setAlias('fire_pump_pump_approved_no');
        $manager->persist($choiceNo);
        /**************  End choices list ***************/


        /** @var DynamicField $pumpSuction */
        $pumpSuction = new DynamicField();
        $pumpSuction->setName('Pump Suction');
        $pumpSuction->setAlias('pump_suction');
        $pumpSuction->setType($this->getReference('dynamic-field-type-dropdown'));
        $pumpSuction->setDevice($firePump);
        $manager->persist($pumpSuction);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceCity */
        $choiceCity = new DynamicFieldDropboxChoices();
        $choiceCity->setField($pumpSuction);
        $choiceCity->setOptionValue('City');
        $choiceCity->setAlias('fire_pump_pump_suction_city');
        $manager->persist($choiceCity);

        /** @var DynamicFieldDropboxChoices $choiceTank */
        $choiceTank = new DynamicFieldDropboxChoices();
        $choiceTank->setField($pumpSuction);
        $choiceTank->setOptionValue('Tank');
        $choiceTank->setAlias('fire_pump_pump_suction_tank');
        $manager->persist($choiceTank);

        /** @var DynamicFieldDropboxChoices $choicePond */
        $choicePond = new DynamicFieldDropboxChoices();
        $choicePond->setField($pumpSuction);
        $choicePond->setOptionValue('Pond');
        $choicePond->setAlias('fire_pump_pump_suction_pond');
        $manager->persist($choicePond);

        /** @var DynamicFieldDropboxChoices $choiceOther */
        $choiceOther = new DynamicFieldDropboxChoices();
        $choiceOther->setField($pumpSuction);
        $choiceOther->setOptionValue('Other');
        $choiceOther->setAlias('fire_pump_pump_suction_other');
        $manager->persist($choiceOther);
        /**************  End choices list ***************/


        /** @var DynamicField $impellerDiameter */
        $impellerDiameter = new DynamicField();
        $impellerDiameter->setName('Impeller Diameter');
        $impellerDiameter->setAlias('impeller_diameter');
        $impellerDiameter->setType($this->getReference('dynamic-field-type-text'));
        $impellerDiameter->setValidation($this->getReference('validation-app-limit-number'));
        $impellerDiameter->setDevice($firePump);
        $manager->persist($impellerDiameter);


        /** @var DynamicField $dischargePipe */
        $dischargePipe = new DynamicField();
        $dischargePipe->setName('Discharge Pipe');
        $dischargePipe->setAlias('discharge_pipe');
        $dischargePipe->setType($this->getReference('dynamic-field-type-dropdown'));
        $dischargePipe->setDevice($firePump);
        $manager->persist($dischargePipe);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1Dot5 */
        $choice1Dot5 = new DynamicFieldDropboxChoices();
        $choice1Dot5->setField($dischargePipe);
        $choice1Dot5->setOptionValue('1.5"');
        $choice1Dot5->setAlias('fire_pump_discharge_pipe_1_dot_5');
        $manager->persist($choice1Dot5);

        /** @var DynamicFieldDropboxChoices $choice2 */
        $choice2 = new DynamicFieldDropboxChoices();
        $choice2->setField($dischargePipe);
        $choice2->setOptionValue('2"');
        $choice2->setAlias('fire_pump_discharge_pipe_2');
        $manager->persist($choice2);

        /** @var DynamicFieldDropboxChoices $choice2Dot5 */
        $choice2Dot5 = new DynamicFieldDropboxChoices();
        $choice2Dot5->setField($dischargePipe);
        $choice2Dot5->setOptionValue('2.5"');
        $choice2Dot5->setAlias('fire_pump_discharge_pipe_2_dot_5');
        $manager->persist($choice2Dot5);

        /** @var DynamicFieldDropboxChoices $choice3 */
        $choice3 = new DynamicFieldDropboxChoices();
        $choice3->setField($dischargePipe);
        $choice3->setOptionValue('3"');
        $choice3->setAlias('fire_pump_discharge_pipe_3');
        $manager->persist($choice3);

        /** @var DynamicFieldDropboxChoices $choice4 */
        $choice4 = new DynamicFieldDropboxChoices();
        $choice4->setField($dischargePipe);
        $choice4->setOptionValue('4"');
        $choice4->setAlias('fire_pump_discharge_pipe_4');
        $manager->persist($choice4);

        /** @var DynamicFieldDropboxChoices $choice6 */
        $choice6 = new DynamicFieldDropboxChoices();
        $choice6->setField($dischargePipe);
        $choice6->setOptionValue('6"');
        $choice6->setAlias('fire_pump_discharge_pipe_6');
        $manager->persist($choice6);

        /** @var DynamicFieldDropboxChoices $choice8 */
        $choice8 = new DynamicFieldDropboxChoices();
        $choice8->setField($dischargePipe);
        $choice8->setOptionValue('8"');
        $choice8->setAlias('fire_pump_discharge_pipe_8');
        $manager->persist($choice8);

        /** @var DynamicFieldDropboxChoices $choice10 */
        $choice10 = new DynamicFieldDropboxChoices();
        $choice10->setField($dischargePipe);
        $choice10->setOptionValue('10"');
        $choice10->setAlias('fire_pump_discharge_pipe_10');
        $manager->persist($choice10);

        /** @var DynamicFieldDropboxChoices $choice12 */
        $choice12 = new DynamicFieldDropboxChoices();
        $choice12->setField($dischargePipe);
        $choice12->setOptionValue('12"');
        $choice12->setAlias('fire_pump_discharge_pipe_12');
        $manager->persist($choice12);
        /**************  End choices list ***************/


        /** @var DynamicField $suctionPipe */
        $suctionPipe = new DynamicField();
        $suctionPipe->setName('Suction Pipe');
        $suctionPipe->setAlias('suction_pipe');
        $suctionPipe->setType($this->getReference('dynamic-field-type-dropdown'));
        $suctionPipe->setDevice($firePump);
        $manager->persist($suctionPipe);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1Dot5 */
        $choice1Dot5 = new DynamicFieldDropboxChoices();
        $choice1Dot5->setField($suctionPipe);
        $choice1Dot5->setOptionValue('1.5"');
        $choice1Dot5->setAlias('fire_pump_suction_pipe_1_dot_5');
        $manager->persist($choice1Dot5);

        /** @var DynamicFieldDropboxChoices $choice2 */
        $choice2 = new DynamicFieldDropboxChoices();
        $choice2->setField($suctionPipe);
        $choice2->setOptionValue('2"');
        $choice2->setAlias('fire_pump_suction_pipe_2');
        $manager->persist($choice2);

        /** @var DynamicFieldDropboxChoices $choice2Dot5 */
        $choice2Dot5 = new DynamicFieldDropboxChoices();
        $choice2Dot5->setField($suctionPipe);
        $choice2Dot5->setOptionValue('2.5"');
        $choice2Dot5->setAlias('fire_pump_suction_pipe_2_dot_5');
        $manager->persist($choice2Dot5);

        /** @var DynamicFieldDropboxChoices $choice3 */
        $choice3 = new DynamicFieldDropboxChoices();
        $choice3->setField($suctionPipe);
        $choice3->setOptionValue('3"');
        $choice3->setAlias('fire_pump_suction_pipe_3');
        $manager->persist($choice3);

        /** @var DynamicFieldDropboxChoices $choice4 */
        $choice4 = new DynamicFieldDropboxChoices();
        $choice4->setField($suctionPipe);
        $choice4->setOptionValue('4"');
        $choice4->setAlias('fire_pump_suction_pipe_4');
        $manager->persist($choice4);

        /** @var DynamicFieldDropboxChoices $choice6 */
        $choice6 = new DynamicFieldDropboxChoices();
        $choice6->setField($suctionPipe);
        $choice6->setOptionValue('6"');
        $choice6->setAlias('fire_pump_suction_pipe_6');
        $manager->persist($choice6);

        /** @var DynamicFieldDropboxChoices $choice8 */
        $choice8 = new DynamicFieldDropboxChoices();
        $choice8->setField($suctionPipe);
        $choice8->setOptionValue('8"');
        $choice8->setAlias('fire_pump_suction_pipe_8');
        $manager->persist($choice8);

        /** @var DynamicFieldDropboxChoices $choice10 */
        $choice10 = new DynamicFieldDropboxChoices();
        $choice10->setField($suctionPipe);
        $choice10->setOptionValue('10"');
        $choice10->setAlias('fire_pump_suction_pipe_10');
        $manager->persist($choice10);

        /** @var DynamicFieldDropboxChoices $choice12 */
        $choice12 = new DynamicFieldDropboxChoices();
        $choice12->setField($suctionPipe);
        $choice12->setOptionValue('12"');
        $choice12->setAlias('fire_pump_suction_pipe_12');
        $manager->persist($choice12);
        /**************  End choices list ***************/


        /** @var DynamicField $rotation */
        $rotation = new DynamicField();
        $rotation->setName('Rotation');
        $rotation->setAlias('rotation');
        $rotation->setType($this->getReference('dynamic-field-type-dropdown'));
        $rotation->setDevice($firePump);
        $manager->persist($rotation);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceClockwise */
        $choiceClockwise = new DynamicFieldDropboxChoices();
        $choiceClockwise->setField($rotation);
        $choiceClockwise->setOptionValue('Clockwise');
        $choiceClockwise->setAlias('fire_pump_rotation_clockwise');
        $manager->persist($choiceClockwise);

        /** @var DynamicFieldDropboxChoices $choiceCCClockwise */
        $choiceCCClockwise = new DynamicFieldDropboxChoices();
        $choiceCCClockwise->setField($rotation);
        $choiceCCClockwise->setOptionValue('CC Clockwise');
        $choiceCCClockwise->setAlias('fire_pump_rotation_cc_clockwise');
        $manager->persist($choiceCCClockwise);
        /**************  End choices list ***************/


        /** @var DynamicField $testHeaderLocation */
        $testHeaderLocation = new DynamicField();
        $testHeaderLocation->setName('Test Header Location');
        $testHeaderLocation->setAlias('test_header_location');
        $testHeaderLocation->setType($this->getReference('dynamic-field-type-text'));
        $testHeaderLocation->setDevice($firePump);
        $manager->persist($testHeaderLocation);
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 11;
    }
}
