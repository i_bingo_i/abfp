<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedDryValveSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Dry Valve *******/
        /** @var DeviceNamed $dryValve */
        $dryValve = new DeviceNamed();
        $dryValve->setName('Dry Valve System');
        $dryValve->setAlias('dry_valve_system');
        $dryValve->setParent($this->getReference('fire-sprinkler-system-device'));
        $dryValve->setCategory($this->getReference('device-category-fire'));
        $dryValve->setLevel(1);
        $manager->persist($dryValve);

        $this->addReference('dry-valve-system-device', $dryValve);
        /************ End device named ****************/



        /************  Dynamic fields list *************/
        /** @var DynamicField $size */
        $size = new DynamicField();
        $size->setName('Size');
        $size->setAlias('dry_valve_size');
        $size->setType($this->getReference('dynamic-field-type-dropdown'));
        $size->setDevice($dryValve);
        $manager->persist($size);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1 */
        $choice1 = new DynamicFieldDropboxChoices();
        $choice1->setField($size);
        $choice1->setOptionValue('1"');
        $choice1->setAlias('dry_valve_size_1');
        $manager->persist($choice1);

        /** @var DynamicFieldDropboxChoices $choice125 */
        $choice125 = new DynamicFieldDropboxChoices();
        $choice125->setField($size);
        $choice125->setOptionValue('1.25"');
        $choice125->setAlias('dry_valve_size_125');
        $manager->persist($choice125);

        /** @var DynamicFieldDropboxChoices $choice2 */
        $choice2 = new DynamicFieldDropboxChoices();
        $choice2->setField($size);
        $choice2->setOptionValue('2"');
        $choice2->setAlias('dry_valve_size_2');
        $manager->persist($choice2);

        /** @var DynamicFieldDropboxChoices $choice25 */
        $choice25 = new DynamicFieldDropboxChoices();
        $choice25->setField($size);
        $choice25->setOptionValue('2.5"');
        $choice25->setAlias('dry_valve_size_25');
        $manager->persist($choice25);

        /** @var DynamicFieldDropboxChoices $choice3 */
        $choice3 = new DynamicFieldDropboxChoices();
        $choice3->setField($size);
        $choice3->setOptionValue('3"');
        $choice3->setAlias('dry_valve_size_3');
        $manager->persist($choice3);

        /** @var DynamicFieldDropboxChoices $choice4 */
        $choice4 = new DynamicFieldDropboxChoices();
        $choice4->setField($size);
        $choice4->setOptionValue('4"');
        $choice4->setAlias('dry_valve_size_4');
        $manager->persist($choice4);

        /** @var DynamicFieldDropboxChoices $choice6 */
        $choice6 = new DynamicFieldDropboxChoices();
        $choice6->setField($size);
        $choice6->setOptionValue('6"');
        $choice6->setAlias('dry_valve_size_6');
        $manager->persist($choice6);

        /** @var DynamicFieldDropboxChoices $choice8 */
        $choice8 = new DynamicFieldDropboxChoices();
        $choice8->setField($size);
        $choice8->setOptionValue('8"');
        $choice8->setAlias('dry_valve_size_8');
        $manager->persist($choice8);
        /**************  End choices list ***************/


        /** @var DynamicField $make */
        $make = new DynamicField();
        $make->setName('Make');
        $make->setAlias('dry_valve_make');
        $make->setType($this->getReference('dynamic-field-type-text'));
        $make->setDevice($dryValve);
        $manager->persist($make);


        /** @var DynamicField $model */
        $model = new DynamicField();
        $model->setName('Model');
        $model->setAlias('model');
        $model->setType($this->getReference('dynamic-field-type-text'));
        $model->setDevice($dryValve);
        $manager->persist($model);


        /** @var DynamicField $serialNumber */
        $serialNumber = new DynamicField();
        $serialNumber->setName('Serial Number');
        $serialNumber->setAlias('serial_number');
        $serialNumber->setType($this->getReference('dynamic-field-type-text'));
        $serialNumber->setValidation($this->getReference('validation-app-font--uppercase'));
        $serialNumber->setDevice($dryValve);
        $manager->persist($serialNumber);


        /** @var DynamicField $numberOfLowPoints */
        $numberOfLowPoints = new DynamicField();
        $numberOfLowPoints->setName('Number of Low Points');
        $numberOfLowPoints->setAlias('number_of_low_points');
        $numberOfLowPoints->setType($this->getReference('dynamic-field-type-text'));
        $numberOfLowPoints->setValidation($this->getReference('validation-app-limit-digits'));
        $numberOfLowPoints->setDevice($dryValve);
        $manager->persist($numberOfLowPoints);


        /** @var DynamicField $waterSupplySource */
        $waterSupplySource = new DynamicField();
        $waterSupplySource->setName('Water Supply Source');
        $waterSupplySource->setAlias('water_supply_source');
        $waterSupplySource->setType($this->getReference('dynamic-field-type-dropdown'));
        $waterSupplySource->setDevice($dryValve);
        $manager->persist($waterSupplySource);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceCity */
        $choiceCity = new DynamicFieldDropboxChoices();
        $choiceCity->setField($waterSupplySource);
        $choiceCity->setOptionValue('City');
        $choiceCity->setAlias('dry_valve_water_supply_source_city');
        $manager->persist($choiceCity);

        /** @var DynamicFieldDropboxChoices $choiceTank */
        $choiceTank = new DynamicFieldDropboxChoices();
        $choiceTank->setField($waterSupplySource);
        $choiceTank->setOptionValue('Tank');
        $choiceTank->setAlias('dry_valve_water_supply_source_tank');
        $manager->persist($choiceTank);

        /** @var DynamicFieldDropboxChoices $choicePump */
        $choicePump = new DynamicFieldDropboxChoices();
        $choicePump->setField($waterSupplySource);
        $choicePump->setOptionValue('Pump');
        $choicePump->setAlias('dry_valve_water_supply_source_pump');
        $manager->persist($choicePump);

        /** @var DynamicFieldDropboxChoices $choicePond */
        $choicePond = new DynamicFieldDropboxChoices();
        $choicePond->setField($waterSupplySource);
        $choicePond->setOptionValue('Pond');
        $choicePond->setAlias('dry_valve_water_supply_source_pond');
        $manager->persist($choicePond);

        /** @var DynamicFieldDropboxChoices $choiceOther */
        $choiceOther = new DynamicFieldDropboxChoices();
        $choiceOther->setField($waterSupplySource);
        $choiceOther->setOptionValue('Other');
        $choiceOther->setAlias('dry_valve_water_supply_source_other');
        $manager->persist($choiceOther);
        /**************  End choices list ***************/


        /** @var DynamicField $itvLocation */
        $itvLocation = new DynamicField();
        $itvLocation->setName('ITV Location');
        $itvLocation->setAlias('itv_location');
        $itvLocation->setType($this->getReference('dynamic-field-type-text'));
        $itvLocation->setDevice($dryValve);
        $itvLocation->setUseLabel(true);
        $itvLocation->setLabelDescription("ITV Location: ");
        $manager->persist($itvLocation);
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
