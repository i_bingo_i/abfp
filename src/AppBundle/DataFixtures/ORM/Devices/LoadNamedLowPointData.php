<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedLowPointData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Low Point *******/
        /** @var DeviceNamed $lowPoint */
        $lowPoint = new DeviceNamed();
        $lowPoint->setName('Low Point');
        $lowPoint->setAlias('low_point');
        $lowPoint->setParent($this->getReference('dry-valve-system-device'));
        $lowPoint->setCategory($this->getReference('device-category-fire'));
        $lowPoint->setLevel(2);
        $manager->persist($lowPoint);

        $this->addReference('low-point-device', $lowPoint);
        /************ End device named ****************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 13;
    }
}
