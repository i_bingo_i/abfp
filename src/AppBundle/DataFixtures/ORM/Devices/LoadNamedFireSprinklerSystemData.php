<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedFireSprinklerSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Fire System *******/
        /** @var DeviceNamed $fireSystem */
        $fireSystem = new DeviceNamed();
        $fireSystem->setName('Fire Sprinkler System');
        $fireSystem->setAlias('fire_sprinkler_system');
        $fireSystem->setSort(3);
        $fireSystem->setCategory($this->getReference('device-category-fire'));
        $manager->persist($fireSystem);

        $this->addReference('fire-sprinkler-system-device', $fireSystem);
        /************ End device named ****************/



        /************  Dynamic fields list *************/
        /** @var DynamicField $size */
        $size = new DynamicField();
        $size->setName('Size');
        $size->setAlias('fire_system_size');
        $size->setType($this->getReference('dynamic-field-type-dropdown'));
        $size->setDevice($fireSystem);
        $manager->persist($size);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choice1 */
        $choice1 = new DynamicFieldDropboxChoices();
        $choice1->setField($size);
        $choice1->setOptionValue('1"');
        $choice1->setAlias('fire_system_size_1');
        $manager->persist($choice1);

        /** @var DynamicFieldDropboxChoices $choice1Dot25 */
        $choice1Dot25 = new DynamicFieldDropboxChoices();
        $choice1Dot25->setField($size);
        $choice1Dot25->setOptionValue('1.25"');
        $choice1Dot25->setAlias('fire_system_size_1_dot_25');
        $manager->persist($choice1Dot25);

        /** @var DynamicFieldDropboxChoices $choice1Dot5 */
        $choice1Dot5 = new DynamicFieldDropboxChoices();
        $choice1Dot5->setField($size);
        $choice1Dot5->setOptionValue('1.5"');
        $choice1Dot5->setAlias('fire_system_size_1_dot_5');
        $manager->persist($choice1Dot5);

        /** @var DynamicFieldDropboxChoices $choice2 */
        $choice2 = new DynamicFieldDropboxChoices();
        $choice2->setField($size);
        $choice2->setOptionValue('2"');
        $choice2->setAlias('fire_system_size_2');
        $manager->persist($choice2);

        /** @var DynamicFieldDropboxChoices $choice2Dot5 */
        $choice2Dot5 = new DynamicFieldDropboxChoices();
        $choice2Dot5->setField($size);
        $choice2Dot5->setOptionValue('2.5"');
        $choice2Dot5->setAlias('fire_system_size_2_dot_5');
        $manager->persist($choice2Dot5);

        /** @var DynamicFieldDropboxChoices $choice3 */
        $choice3 = new DynamicFieldDropboxChoices();
        $choice3->setField($size);
        $choice3->setOptionValue('3"');
        $choice3->setAlias('fire_system_size_3');
        $manager->persist($choice3);

        /** @var DynamicFieldDropboxChoices $choice4 */
        $choice4 = new DynamicFieldDropboxChoices();
        $choice4->setField($size);
        $choice4->setOptionValue('4"');
        $choice4->setAlias('fire_system_size_4');
        $manager->persist($choice4);

        /** @var DynamicFieldDropboxChoices $choice6 */
        $choice6 = new DynamicFieldDropboxChoices();
        $choice6->setField($size);
        $choice6->setOptionValue('6"');
        $choice6->setAlias('fire_system_size_6');
        $manager->persist($choice6);

        /** @var DynamicFieldDropboxChoices $choice8 */
        $choice8 = new DynamicFieldDropboxChoices();
        $choice8->setField($size);
        $choice8->setOptionValue('8"');
        $choice8->setAlias('fire_system_size_8');
        $manager->persist($choice8);

        /** @var DynamicFieldDropboxChoices $choice10 */
        $choice10 = new DynamicFieldDropboxChoices();
        $choice10->setField($size);
        $choice10->setOptionValue('10"');
        $choice10->setAlias('fire_system_size_10');
        $manager->persist($choice10);

        /** @var DynamicFieldDropboxChoices $choice12 */
        $choice12 = new DynamicFieldDropboxChoices();
        $choice12->setField($size);
        $choice12->setOptionValue('12"');
        $choice12->setAlias('fire_system_size_12');
        $manager->persist($choice12);
        /**************  End choices list ***************/


        /** @var DynamicField $NumberOfRisers */
        $NumberOfRisers = new DynamicField();
        $NumberOfRisers->setName('Number of Risers');
        $NumberOfRisers->setAlias('number_of_risers');
        $NumberOfRisers->setType($this->getReference('dynamic-field-type-text'));
        $NumberOfRisers->setDevice($fireSystem);
        $NumberOfRisers->setValidation($this->getReference('validation-app-limit-digits'));
        $NumberOfRisers->setUseLabel(true);
        $NumberOfRisers->setLabelAfter(true);
        $NumberOfRisers->setLabelDescription("Riser");
        $manager->persist($NumberOfRisers);


        /** @var DynamicField $NumberOfSections */
        $NumberOfSections = new DynamicField();
        $NumberOfSections->setName('Number of Sections');
        $NumberOfSections->setAlias('number_of_sections');
        $NumberOfSections->setType($this->getReference('dynamic-field-type-text'));
        $NumberOfSections->setDevice($fireSystem);
        $NumberOfSections->setValidation($this->getReference('validation-app-limit-digits'));
        $NumberOfSections->setUseLabel(true);
        $NumberOfSections->setLabelAfter(true);
        $NumberOfSections->setLabelDescription("Sections");
        $manager->persist($NumberOfSections);


        /** @var DynamicField $waterSupplySource */
        $waterSupplySource = new DynamicField();
        $waterSupplySource->setName('Water Supply Source');
        $waterSupplySource->setAlias('water_supply_source');
        $waterSupplySource->setType($this->getReference('dynamic-field-type-dropdown'));
        $waterSupplySource->setDevice($fireSystem);
        $manager->persist($waterSupplySource);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceCity */
        $choiceCity = new DynamicFieldDropboxChoices();
        $choiceCity->setField($waterSupplySource);
        $choiceCity->setOptionValue('City');
        $choiceCity->setAlias('fire_system_water_supply_source_city');
        $manager->persist($choiceCity);

        /** @var DynamicFieldDropboxChoices $choiceTank */
        $choiceTank = new DynamicFieldDropboxChoices();
        $choiceTank->setField($waterSupplySource);
        $choiceTank->setOptionValue('Tank');
        $choiceTank->setAlias('fire_system_water_supply_source_tank');
        $manager->persist($choiceTank);

        /** @var DynamicFieldDropboxChoices $choicePump */
        $choicePump = new DynamicFieldDropboxChoices();
        $choicePump->setField($waterSupplySource);
        $choicePump->setOptionValue('Pump');
        $choicePump->setAlias('fire_system_water_supply_source_pump');
        $manager->persist($choicePump);

        /** @var DynamicFieldDropboxChoices $choicePond */
        $choicePond = new DynamicFieldDropboxChoices();
        $choicePond->setField($waterSupplySource);
        $choicePond->setOptionValue('Pond');
        $choicePond->setAlias('fire_system_water_supply_source_pond');
        $manager->persist($choicePond);

        /** @var DynamicFieldDropboxChoices $choiceWell */
        $choiceWell = new DynamicFieldDropboxChoices();
        $choiceWell->setField($waterSupplySource);
        $choiceWell->setOptionValue('Well');
        $choiceWell->setAlias('fire_system_water_supply_source_well');
        $manager->persist($choiceWell);

        /** @var DynamicFieldDropboxChoices $choiceOther */
        $choiceOther = new DynamicFieldDropboxChoices();
        $choiceOther->setField($waterSupplySource);
        $choiceOther->setOptionValue('Other');
        $choiceOther->setAlias('fire_system_water_supply_source_other');
        $manager->persist($choiceOther);
        /**************  End choices list ***************/


        /** @var DynamicField $NumberOfHeads */
        $NumberOfHeads = new DynamicField();
        $NumberOfHeads->setName('Number of heads');
        $NumberOfHeads->setAlias('number_of_heads');
        $NumberOfHeads->setType($this->getReference('dynamic-field-type-text'));
        $NumberOfHeads->setDevice($fireSystem);
        $NumberOfHeads->setValidation($this->getReference('validation-app-limit-digits4'));
        $NumberOfHeads->setUseLabel(true);
        $NumberOfHeads->setLabelAfter(true);
        $NumberOfHeads->setLabelDescription("Heads");
        $manager->persist($NumberOfHeads);
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 11;
    }
}
