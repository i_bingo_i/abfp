<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\DynamicFieldDropboxChoices;

class LoadNamedAirCompressorData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Air Compressor *******/
        /** @var DeviceNamed $airCompressor */
        $airCompressor = new DeviceNamed();
        $airCompressor->setName('Air Compressor');
        $airCompressor->setAlias('air_compressor');
        $airCompressor->setParent($this->getReference('dry-valve-system-device'));
        $airCompressor->setCategory($this->getReference('device-category-fire'));
        $airCompressor->setLevel(2);
        $manager->persist($airCompressor);

        $this->addReference('air-compressor-device', $airCompressor);
        /************ End device named ****************/



        /************  Dynamic fields list *************/
        /** @var DynamicField $make */
        $make = new DynamicField();
        $make->setName('Make');
        $make->setAlias('make');
        $make->setType($this->getReference('dynamic-field-type-text'));
        $make->setDevice($airCompressor);
        $manager->persist($make);


        /** @var DynamicField $model */
        $model = new DynamicField();
        $model->setName('Model');
        $model->setAlias('model');
        $model->setType($this->getReference('dynamic-field-type-text'));
        $model->setDevice($airCompressor);
        $manager->persist($model);


        /** @var DynamicField $serialNumber */
        $serialNumber = new DynamicField();
        $serialNumber->setName('Serial Number');
        $serialNumber->setAlias('serial_number');
        $serialNumber->setType($this->getReference('dynamic-field-type-text'));
        $serialNumber->setDevice($airCompressor);
        $manager->persist($serialNumber);

        /** @var DynamicField $volts */
        $volts = new DynamicField();
        $volts->setName('Volts');
        $volts->setAlias('volts');
        $volts->setType($this->getReference('dynamic-field-type-dropdown'));
        $volts->setDevice($airCompressor);
        $manager->persist($volts);

        /************  Dropdown choices list *************/
        /** @var DynamicFieldDropboxChoices $choiceVolts120 */
        $choiceVolts120 = new DynamicFieldDropboxChoices();
        $choiceVolts120->setField($volts);
        $choiceVolts120->setOptionValue('120V');
        $choiceVolts120->setAlias('air_compressor_volts_120');
        $manager->persist($choiceVolts120);

        /** @var DynamicFieldDropboxChoices $choiceVolts240 */
        $choiceVolts240 = new DynamicFieldDropboxChoices();
        $choiceVolts240->setField($volts);
        $choiceVolts240->setOptionValue('240V');
        $choiceVolts240->setAlias('air_compressor_volts_240');
        $manager->persist($choiceVolts240);

        /** @var DynamicFieldDropboxChoices $choiceVolts480 */
        $choiceVolts480 = new DynamicFieldDropboxChoices();
        $choiceVolts480->setField($volts);
        $choiceVolts480->setOptionValue('480V');
        $choiceVolts480->setAlias('air_compressor_volts_480');
        $manager->persist($choiceVolts480);
        /**************  End fields list ***************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 13;
    }
}
