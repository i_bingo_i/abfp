<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DynamicFieldType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDynamicFieldTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Dynamic field type Text
        $typeText = new DynamicFieldType();
        $typeText->setName('Text');
        $typeText->setAlias('text');

        $manager->persist($typeText);
        $manager->flush();

        $this->addReference('dynamic-field-type-text', $typeText);


        // Dynamic field type Dropdown
        $typeDropdown = new DynamicFieldType();
        $typeDropdown->setName('Dropdown');
        $typeDropdown->setAlias('dropdown');

        $manager->persist($typeDropdown);
        $manager->flush();

        $this->addReference('dynamic-field-type-dropdown', $typeDropdown);


        // Dynamic field type Entity
        $typeEntity = new DynamicFieldType();
        $typeEntity->setName('Entity');
        $typeEntity->setAlias('entity');

        $manager->persist($typeEntity);
        $manager->flush();

        $this->addReference('dynamic-field-type-entity', $typeEntity);
    }

    public function getOrder()
    {
        return 1;
    }
}
