<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDeviceStandpipeSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named StandpipeSystem *******/
        /** @var DeviceNamed $standpipeSystem */
        $standpipeSystem = new DeviceNamed();
        $standpipeSystem->setName('Standpipe System');
        $standpipeSystem->setAlias('standpipe_system');
        $standpipeSystem->setParent($this->getReference('fire-sprinkler-system-device'));
        $standpipeSystem->setCategory($this->getReference('device-category-fire'));
        $standpipeSystem->setLevel(1);
        $manager->persist($standpipeSystem);

        $this->addReference('standpipe-system-device', $standpipeSystem);
        /************ End device named ****************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
