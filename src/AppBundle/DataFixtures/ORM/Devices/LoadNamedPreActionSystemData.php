<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedPreActionSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /******* Device named Pre-Action System *******/
        /** @var DeviceNamed $preActionSystem */
        $preActionSystem = new DeviceNamed();
        $preActionSystem->setName('Pre-Action System');
        $preActionSystem->setAlias('pre_action_system');
        $preActionSystem->setParent($this->getReference('fire-sprinkler-system-device'));
        $preActionSystem->setCategory($this->getReference('device-category-fire'));
        $preActionSystem->setLevel(1);
        $manager->persist($preActionSystem);

        $this->addReference('pre-action-system-device', $preActionSystem);
        /************ End device named ****************/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}
