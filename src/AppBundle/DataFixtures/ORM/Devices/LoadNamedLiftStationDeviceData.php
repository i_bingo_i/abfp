<?php

namespace AppBundle\DataFixtures\ORM\Devices;

use AppBundle\Entity\DeviceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadNamedLiftStationDeviceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $liftStation = new DeviceNamed();
        $liftStation->setName('Lift Station');
        $liftStation->setAlias('lift_station');
        $liftStation->setCategory($this->getReference('device-category-plumbing'));
        $manager->persist($liftStation);
        $this->addReference('lift-station-device', $liftStation);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
