<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\State;

class LoadStateData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $alabamaState = new State();
        $alabamaState->setName("Alabama");
        $alabamaState->setAlias('alabama');
        $alabamaState->setCode("AL");

        $manager->persist($alabamaState);
        $manager->flush();
        $this->addReference('state-alabama', $alabamaState);


        $alaskaState = new State();
        $alaskaState->setName("Alaska");
        $alaskaState->setAlias('alaska');
        $alaskaState->setCode("AK");

        $manager->persist($alaskaState);
        $manager->flush();
        $this->addReference('state-alaska', $alaskaState);


        $arizonaState = new State();
        $arizonaState->setName("Arizona");
        $arizonaState->setAlias('arizona');
        $arizonaState->setCode("AZ");

        $manager->persist($arizonaState);
        $manager->flush();
        $this->addReference('state-arizona', $arizonaState);


        $arkansasState = new State();
        $arkansasState->setName("Arkansas");
        $arkansasState->setAlias('arkansas');
        $arkansasState->setCode("AR");

        $manager->persist($arkansasState);
        $manager->flush();
        $this->addReference('state-arkansas', $arkansasState);


        $californiaState = new State();
        $californiaState->setName("California");
        $californiaState->setAlias('california');
        $californiaState->setCode("CA");

        $manager->persist($californiaState);
        $manager->flush();
        $this->addReference('state-california', $californiaState);


        $coloradoState = new State();
        $coloradoState->setName("Colorado");
        $coloradoState->setAlias('colorado');
        $coloradoState->setCode("CO");

        $manager->persist($coloradoState);
        $manager->flush();
        $this->addReference('state-colorado', $coloradoState);


        $connecticutState = new State();
        $connecticutState->setName("Connecticut");
        $connecticutState->setAlias('connecticut');
        $connecticutState->setCode("CT");

        $manager->persist($connecticutState);
        $manager->flush();
        $this->addReference('state-connecticut', $connecticutState);


        $delawareState = new State();
        $delawareState->setName("Delaware");
        $delawareState->setAlias('delaware');
        $delawareState->setCode("DE");

        $manager->persist($delawareState);
        $manager->flush();
        $this->addReference('state-delaware', $delawareState);

        $floridaState = new State();
        $floridaState->setName("Florida");
        $floridaState->setAlias('florida');
        $floridaState->setCode("FL");

        $manager->persist($floridaState);
        $manager->flush();
        $this->addReference('state-florida', $floridaState);


        $georgiaState = new State();
        $georgiaState->setName("Georgia");
        $georgiaState->setAlias('georgia');
        $georgiaState->setCode("GA");

        $manager->persist($georgiaState);
        $manager->flush();
        $this->addReference('state-georgia', $georgiaState);


        $hawaiiState = new State();
        $hawaiiState->setName("Hawaii");
        $hawaiiState->setAlias('hawaii');
        $hawaiiState->setCode("HI");

        $manager->persist($hawaiiState);
        $manager->flush();
        $this->addReference('state-hawaii', $hawaiiState);


        $idahoState = new State();
        $idahoState->setName("Idaho");
        $idahoState->setAlias('idaho');
        $idahoState->setCode("ID");

        $manager->persist($idahoState);
        $manager->flush();
        $this->addReference('state-idaho', $idahoState);

        $illinoisState = new State();
        $illinoisState->setName("Illinois");
        $illinoisState->setAlias('illinois');
        $illinoisState->setCode("IL");

        $manager->persist($illinoisState);
        $manager->flush();
        $this->addReference('state-illinois', $illinoisState);


        $indianaState = new State();
        $indianaState->setName("Indiana");
        $indianaState->setAlias('indiana');
        $indianaState->setCode("IN");

        $manager->persist($indianaState);
        $manager->flush();
        $this->addReference('state-indiana', $indianaState);


        $iowaState = new State();
        $iowaState->setName("Iowa");
        $iowaState->setAlias('iowa');
        $iowaState->setCode("IA");

        $manager->persist($iowaState);
        $manager->flush();
        $this->addReference('state-iowa', $iowaState);


        $kansasState = new State();
        $kansasState->setName("Kansas");
        $kansasState->setAlias('kansas');
        $kansasState->setCode("KS");

        $manager->persist($kansasState);
        $manager->flush();
        $this->addReference('state-kansas', $kansasState);


        $kentuckyState = new State();
        $kentuckyState->setName("Kentucky");
        $kentuckyState->setAlias('kentucky');
        $kentuckyState->setCode("KY");

        $manager->persist($kentuckyState);
        $manager->flush();
        $this->addReference('state-kentucky', $kentuckyState);


        $louisianaState = new State();
        $louisianaState->setName("Louisiana");
        $louisianaState->setAlias('louisiana');
        $louisianaState->setCode("LA");

        $manager->persist($louisianaState);
        $manager->flush();
        $this->addReference('state-louisiana', $louisianaState);


        $maineState = new State();
        $maineState->setName("Maine");
        $maineState->setAlias('maine');
        $maineState->setCode("ME");

        $manager->persist($maineState);
        $manager->flush();
        $this->addReference('state-maine', $maineState);


        $marylandState = new State();
        $marylandState->setName("Maryland");
        $marylandState->setAlias('maryland');
        $marylandState->setCode("MD");

        $manager->persist($marylandState);
        $manager->flush();
        $this->addReference('state-maryland', $marylandState);


        $massachusettsState = new State();
        $massachusettsState->setName("Massachusetts");
        $massachusettsState->setAlias('massachusetts');
        $massachusettsState->setCode("MA");

        $manager->persist($massachusettsState);
        $manager->flush();
        $this->addReference('state-massachusetts', $massachusettsState);


        $michiganState = new State();
        $michiganState->setName("Michigan");
        $michiganState->setAlias('michigan');
        $michiganState->setCode("MI");

        $manager->persist($michiganState);
        $manager->flush();
        $this->addReference('state-michigan', $michiganState);


        $minnesotaState = new State();
        $minnesotaState->setName("Minnesota");
        $minnesotaState->setAlias('minnesota');
        $minnesotaState->setCode("MN");

        $manager->persist($minnesotaState);
        $manager->flush();
        $this->addReference('state-minnesota', $minnesotaState);


        $mississippiState = new State();
        $mississippiState->setName("Mississippi");
        $mississippiState->setAlias('mississippi');
        $mississippiState->setCode("MS");

        $manager->persist($mississippiState);
        $manager->flush();
        $this->addReference('state-mississippi', $mississippiState);


        $missouriState = new State();
        $missouriState->setName("Missouri");
        $missouriState->setAlias('missouri');
        $missouriState->setCode("MO");

        $manager->persist($missouriState);
        $manager->flush();
        $this->addReference('state-missouri', $missouriState);


        $montanaState = new State();
        $montanaState->setName("Montana");
        $montanaState->setAlias('montana');
        $montanaState->setCode("MT");

        $manager->persist($montanaState);
        $manager->flush();
        $this->addReference('state-montana', $montanaState);


        $nebraskaState = new State();
        $nebraskaState->setName("Nebraska");
        $nebraskaState->setAlias('nebraska');
        $nebraskaState->setCode("NE");

        $manager->persist($nebraskaState);
        $manager->flush();
        $this->addReference('state-nebraska', $nebraskaState);


        $nevadaState = new State();
        $nevadaState->setName("Nevada");
        $nevadaState->setAlias('nevada');
        $nevadaState->setCode("NV");

        $manager->persist($nevadaState);
        $manager->flush();
        $this->addReference('state-nevada', $nevadaState);


        $newhampshireState = new State();
        $newhampshireState->setName("New Hampshire");
        $newhampshireState->setAlias('new hampshire');
        $newhampshireState->setCode("NH");

        $manager->persist($newhampshireState);
        $manager->flush();
        $this->addReference('state-new-hampshire', $newhampshireState);


        $newjerseyState = new State();
        $newjerseyState->setName("New Jersey");
        $newjerseyState->setAlias('new jersey');
        $newjerseyState->setCode("NJ");

        $manager->persist($newjerseyState);
        $manager->flush();
        $this->addReference('state-new-jersey', $newjerseyState);


        $newmexicoState = new State();
        $newmexicoState->setName("New Mexico");
        $newmexicoState->setAlias('new mexico');
        $newmexicoState->setCode("NM");

        $manager->persist($newmexicoState);
        $manager->flush();
        $this->addReference('state-new-mexico', $newmexicoState);


        $newyorkState = new State();
        $newyorkState->setName("New York");
        $newyorkState->setAlias('new york');
        $newyorkState->setCode("NY");

        $manager->persist($newyorkState);
        $manager->flush();
        $this->addReference('state-new-york', $newyorkState);


        $northcarolinaState = new State();
        $northcarolinaState->setName("North Carolina");
        $northcarolinaState->setAlias('north carolina');
        $northcarolinaState->setCode("NC");

        $manager->persist($northcarolinaState);
        $manager->flush();
        $this->addReference('state-north-carolina', $northcarolinaState);


        $northdakotaState = new State();
        $northdakotaState->setName("North Dakota");
        $northdakotaState->setAlias('north dakota');
        $northdakotaState->setCode("ND");

        $manager->persist($northdakotaState);
        $manager->flush();
        $this->addReference('state-north-dakota', $northdakotaState);


        $ohioState = new State();
        $ohioState->setName("Ohio");
        $ohioState->setAlias('ohio');
        $ohioState->setCode("OH");

        $manager->persist($ohioState);
        $manager->flush();
        $this->addReference('state-ohio', $ohioState);


        $oklahomaState = new State();
        $oklahomaState->setName("Oklahoma");
        $oklahomaState->setAlias('oklahoma');
        $oklahomaState->setCode("OK");

        $manager->persist($oklahomaState);
        $manager->flush();
        $this->addReference('state-oklahoma', $oklahomaState);


        $oregonState = new State();
        $oregonState->setName("Oregon");
        $oregonState->setAlias('oregon');
        $oregonState->setCode("OR");

        $manager->persist($oregonState);
        $manager->flush();
        $this->addReference('state-oregon', $oregonState);


        $pennsylvaniaState = new State();
        $pennsylvaniaState->setName("Pennsylvania");
        $pennsylvaniaState->setAlias('pennsylvania');
        $pennsylvaniaState->setCode("PA");

        $manager->persist($pennsylvaniaState);
        $manager->flush();
        $this->addReference('state-pennsylvania', $pennsylvaniaState);


        $rhodeislandState = new State();
        $rhodeislandState->setName("Rhode Island");
        $rhodeislandState->setAlias('rhode island');
        $rhodeislandState->setCode("RI");

        $manager->persist($rhodeislandState);
        $manager->flush();
        $this->addReference('state-rhode-island', $rhodeislandState);


        $southcarolinaState = new State();
        $southcarolinaState->setName("South Carolina");
        $southcarolinaState->setAlias('south carolina');
        $southcarolinaState->setCode("SC");

        $manager->persist($southcarolinaState);
        $manager->flush();
        $this->addReference('state-south-carolina', $southcarolinaState);


        $southdakotaState = new State();
        $southdakotaState->setName("South Dakota");
        $southdakotaState->setAlias('south dakota');
        $southdakotaState->setCode("SD");

        $manager->persist($southdakotaState);
        $manager->flush();
        $this->addReference('state-south-dakota', $southdakotaState);


        $tennesseeState = new State();
        $tennesseeState->setName("Tennessee");
        $tennesseeState->setAlias('tennessee');
        $tennesseeState->setCode("TN");

        $manager->persist($tennesseeState);
        $manager->flush();
        $this->addReference('state-tennessee', $tennesseeState);


        $texasState = new State();
        $texasState->setName("Texas");
        $texasState->setAlias('texas');
        $texasState->setCode("TX");

        $manager->persist($texasState);
        $manager->flush();
        $this->addReference('state-texas', $texasState);


        $utahState = new State();
        $utahState->setName("Utah");
        $utahState->setAlias('utah');
        $utahState->setCode("UT");

        $manager->persist($utahState);
        $manager->flush();
        $this->addReference('state-utah', $utahState);


        $vermontState = new State();
        $vermontState->setName("Vermont");
        $vermontState->setAlias('vermont');
        $vermontState->setCode("VT");

        $manager->persist($vermontState);
        $manager->flush();
        $this->addReference('state-vermont', $vermontState);


        $virginiaState = new State();
        $virginiaState->setName("Virginia");
        $virginiaState->setAlias('virginia');
        $virginiaState->setCode("VA");

        $manager->persist($virginiaState);
        $manager->flush();
        $this->addReference('state-virginia', $virginiaState);


        $washingtonState = new State();
        $washingtonState->setName("Washington");
        $washingtonState->setAlias('washington');
        $washingtonState->setCode("WA");

        $manager->persist($washingtonState);
        $manager->flush();
        $this->addReference('state-washington', $washingtonState);


        $westvirginiaState = new State();
        $westvirginiaState->setName("West Virginia");
        $westvirginiaState->setAlias('west virginia');
        $westvirginiaState->setCode("WV");

        $manager->persist($westvirginiaState);
        $manager->flush();
        $this->addReference('state-west-virginia', $westvirginiaState);


        $wisconsinState = new State();
        $wisconsinState->setName("Wisconsin");
        $wisconsinState->setAlias('wisconsin');
        $wisconsinState->setCode("WI");

        $manager->persist($wisconsinState);
        $manager->flush();
        $this->addReference('state-wisconsin', $wisconsinState);

        $wyomingState = new State();
        $wyomingState->setName("Wyoming");
        $wyomingState->setAlias('wyoming');
        $wyomingState->setCode("WY");

        $manager->persist($wyomingState);
        $manager->flush();
        $this->addReference('state-wyoming', $wyomingState);
    }

    public function getOrder()
    {
        return 1;
    }
}
