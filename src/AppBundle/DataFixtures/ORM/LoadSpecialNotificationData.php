<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\SpecialNotification;

class LoadSpecialNotificationData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /**
         *  BACKFLOW
         */

        $testSpecialNotificationBackflowOne = new SpecialNotification();
        $testSpecialNotificationBackflowOne->setName("Domestic Water to be off for 1 hour");
        $testSpecialNotificationBackflowOne->setAlias("backflow_domestic_water_to_be_off_for_1_hour");
        $testSpecialNotificationBackflowOne->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowOne);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-1-hour-backflow-special-notification', $testSpecialNotificationBackflowOne);


        $testSpecialNotificationBackflowTwo = new SpecialNotification();
        $testSpecialNotificationBackflowTwo->setName("Domestic Water to be off for 2 Hrs");
        $testSpecialNotificationBackflowTwo->setAlias("backflow_domestic_water_to_be_off_for_2_hour");
        $testSpecialNotificationBackflowTwo->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowTwo);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-2-hour-backflow-special-notification', $testSpecialNotificationBackflowTwo);

        $testSpecialNotificationBackflowThree = new SpecialNotification();
        $testSpecialNotificationBackflowThree->setName("Domestic Water to be off for 4 Hrs");
        $testSpecialNotificationBackflowThree->setAlias("backflow_domestic_water_to_be_off_for_4_hour");
        $testSpecialNotificationBackflowThree->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowThree);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-4-hour-backflow-special-notification', $testSpecialNotificationBackflowThree);

        $testSpecialNotificationBackflowFour = new SpecialNotification();
        $testSpecialNotificationBackflowFour->setName("Domestic Water will be off for 6 Hrs");
        $testSpecialNotificationBackflowFour->setAlias("backflow_domestic_water_will_be_off_for_6_hour");
        $testSpecialNotificationBackflowFour->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowFour);
        $manager->flush();
        $this->addReference('domestic-water-will-be-off-for-6-hour-backflow-special-notification', $testSpecialNotificationBackflowFour);

        $testSpecialNotificationBackflowFive = new SpecialNotification();
        $testSpecialNotificationBackflowFive->setName("Domestic Water to be off for 8 Hrs");
        $testSpecialNotificationBackflowFive->setAlias("backflow_domestic_water_to_be_off_for_8_hour");
        $testSpecialNotificationBackflowFive->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowFive);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-8-hour-backflow-special-notification', $testSpecialNotificationBackflowFive);

        $testSpecialNotificationBackflowSix = new SpecialNotification();
        $testSpecialNotificationBackflowSix->setName("Domestic Water to be off for 24 Hrs");
        $testSpecialNotificationBackflowSix->setAlias("backflow_domestic_water_to_be_off_for_24_hour");
        $testSpecialNotificationBackflowSix->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowSix);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-24-hour-backflow-special-notification', $testSpecialNotificationBackflowSix);

        $testSpecialNotificationBackflowSeven = new SpecialNotification();
        $testSpecialNotificationBackflowSeven->setName("Domestic Water Will Not be Affected");
        $testSpecialNotificationBackflowSeven->setAlias("backflow_domestic_water_will_not_be_affected");
        $testSpecialNotificationBackflowSeven->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowSeven);
        $manager->flush();
        $this->addReference('domestic-water-will-be-not-be-affected-backflow-special-notification', $testSpecialNotificationBackflowSeven);

        $testSpecialNotificationBackflowEight = new SpecialNotification();
        $testSpecialNotificationBackflowEight->setName("Fire Protection Supply Will Be Off for 1 Hour");
        $testSpecialNotificationBackflowEight->setAlias("backflow_fire_protection_supply_will_be_off_for_1_hour");
        $testSpecialNotificationBackflowEight->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowEight);
        $manager->flush();
        $this->addReference('fire-protection-supply-will-be-off-for-1-hour-backflow-special-notification', $testSpecialNotificationBackflowEight);

        $testSpecialNotificationBackflowNine = new SpecialNotification();
        $testSpecialNotificationBackflowNine->setName("Fire Protection Supply Will Be Off for 2 Hrs");
        $testSpecialNotificationBackflowNine->setAlias("backflow_fire_protection_supply_will_be_off_for_2_hours");
        $testSpecialNotificationBackflowNine->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowNine);
        $manager->flush();
        $this->addReference('fire-protection-supply-will-be-off-for-2-hours-backflow-special-notification', $testSpecialNotificationBackflowNine);

        $testSpecialNotificationBackflowTen = new SpecialNotification();
        $testSpecialNotificationBackflowTen->setName("Fire Protection Supply Will Be Off for 4 Hrs");
        $testSpecialNotificationBackflowTen->setAlias("backflow_fire_protection_supply_will_be_off_for_4_hours");
        $testSpecialNotificationBackflowTen->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowTen);
        $manager->flush();
        $this->addReference('fire-protection-supply-will-be-off-for-4-hours-backflow-special-notification', $testSpecialNotificationBackflowTen);

        $testSpecialNotificationBackflowEleven = new SpecialNotification();
        $testSpecialNotificationBackflowEleven->setName("Fire Protection Supply Will Be Off for 8 Hrs");
        $testSpecialNotificationBackflowEleven->setAlias("backflow_fire_protection_supply_will_be_off_for_8_hours");
        $testSpecialNotificationBackflowEleven->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowEleven);
        $manager->flush();
        $this->addReference('fire-protection-supply-will-be-off-for-8-hours-backflow-special-notification', $testSpecialNotificationBackflowEleven);

        $testSpecialNotificationBackflowTwelve = new SpecialNotification();
        $testSpecialNotificationBackflowTwelve->setName("Fire Protection Supply Will Be Off for 24 Hrs");
        $testSpecialNotificationBackflowTwelve->setAlias("backflow_fire_protection_supply_will_be_off_for_24_hours");
        $testSpecialNotificationBackflowTwelve->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowTwelve);
        $manager->flush();
        $this->addReference('fire-protection-supply-will-be-off-for-24-hours-backflow-special-notification', $testSpecialNotificationBackflowTwelve);

        $testSpecialNotificationBackflowThirteen = new SpecialNotification();
        $testSpecialNotificationBackflowThirteen->setName("Water Supply to Equipment will be off for 1 Hour");
        $testSpecialNotificationBackflowThirteen->setAlias("backflow_fire_supply_to_equipment_will_be_off_for_1_hour");
        $testSpecialNotificationBackflowThirteen->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowThirteen);
        $manager->flush();
        $this->addReference('fire-supply-to-equipment-will-be-of-for-1-hour-backflow-special-notification', $testSpecialNotificationBackflowThirteen);

        $testSpecialNotificationBackflowFourteen = new SpecialNotification();
        $testSpecialNotificationBackflowFourteen->setName("Water Supply to Equipment will be off for 2 Hour");
        $testSpecialNotificationBackflowFourteen->setAlias("backflow_fire_supply_to_equipment_will_be_off_for_2_hour");
        $testSpecialNotificationBackflowFourteen->setDivision($this->getReference('device-category-backflow'));
        $manager->persist($testSpecialNotificationBackflowFourteen);
        $manager->flush();
        $this->addReference('fire-supply-to-equipment-will-be-of-for-2-hour-backflow-special-notification', $testSpecialNotificationBackflowFourteen);

        /**
         *  FIRE
         */

        $testSpecialNotificationFireOne = new SpecialNotification();
        $testSpecialNotificationFireOne->setName("Fire Protection Will Be Off for 1 Hour");
        $testSpecialNotificationFireOne->setAlias("fire_fire_protection_will_be_off_for_1_hour");
        $testSpecialNotificationFireOne->setDivision($this->getReference('device-category-fire'));
        $manager->persist($testSpecialNotificationFireOne);
        $manager->flush();
        $this->addReference('fire-fire-protection-will-be-off-for-1-hour-division-fire-notification', $testSpecialNotificationFireOne);

        $testSpecialNotificationFireTwo = new SpecialNotification();
        $testSpecialNotificationFireTwo->setName("Fire Protection Will Be Off for 2 Hrs");
        $testSpecialNotificationFireTwo->setAlias("fire_fire_protection_will_be_off_for_2_hours");
        $testSpecialNotificationFireTwo->setDivision($this->getReference('device-category-fire'));
        $manager->persist($testSpecialNotificationFireTwo);
        $manager->flush();
        $this->addReference('fire-fire-protection-will-be-off-for-2-hours-division-fire-notification', $testSpecialNotificationFireTwo);

        $testSpecialNotificationFireThree = new SpecialNotification();
        $testSpecialNotificationFireThree->setName("Fire Protection Will Be Off for 4 Hrs");
        $testSpecialNotificationFireThree->setAlias("fire_fire_protection_will_be_off_for_4_hours");
        $testSpecialNotificationFireThree->setDivision($this->getReference('device-category-fire'));
        $manager->persist($testSpecialNotificationFireThree);
        $manager->flush();
        $this->addReference('fire-fire-protection-will-be-off-for-4-hours-division-fire-notification', $testSpecialNotificationFireThree);

        $testSpecialNotificationFireFour = new SpecialNotification();
        $testSpecialNotificationFireFour->setName("Fire Protection Will Be Off for 6 Hours");
        $testSpecialNotificationFireFour->setAlias("fire_fire_protection_will_be_off_for_6_hours");
        $testSpecialNotificationFireFour->setDivision($this->getReference('device-category-fire'));
        $manager->persist($testSpecialNotificationFireFour);
        $manager->flush();
        $this->addReference('fire-fire-protection-will-be-off-for-6-hours-division-fire-notification', $testSpecialNotificationFireFour);

        $testSpecialNotificationFireFive = new SpecialNotification();
        $testSpecialNotificationFireFive->setName("Fire Protection Will Be Off for 8 Hrs");
        $testSpecialNotificationFireFive->setAlias("fire_fire_protection_will_be_off_for_8_hours");
        $testSpecialNotificationFireFive->setDivision($this->getReference('device-category-fire'));
        $manager->persist($testSpecialNotificationFireFive);
        $manager->flush();
        $this->addReference('fire-fire-protection-will-be-off-for-8-hours-division-fire-notification', $testSpecialNotificationFireFive);

        $testSpecialNotificationFireSix = new SpecialNotification();
        $testSpecialNotificationFireSix->setName("Fire Protection Will Be Off for 24 Hrs");
        $testSpecialNotificationFireSix->setAlias("fire_fire_protection_will_be_off_for_24_hours");
        $testSpecialNotificationFireSix->setDivision($this->getReference('device-category-fire'));
        $manager->persist($testSpecialNotificationFireSix);
        $manager->flush();
        $this->addReference('fire-fire-protection-will-be-off-for-24-hours-division-fire-notification', $testSpecialNotificationFireSix);

        $testSpecialNotificationFireSeven = new SpecialNotification();
        $testSpecialNotificationFireSeven->setName("Fire Protection Will Not be Affected");
        $testSpecialNotificationFireSeven->setAlias("fire_fire_protection_will_not_be_affected");
        $testSpecialNotificationFireSeven->setDivision($this->getReference('device-category-fire'));
        $manager->persist($testSpecialNotificationFireSeven);
        $manager->flush();
        $this->addReference('fire-fire-protection-will-not-be-affected-division-fire-notification', $testSpecialNotificationFireSeven);

        $testSpecialNotificationFireEight = new SpecialNotification();
        $testSpecialNotificationFireEight->setName("Alarms May Sound During Repairs");
        $testSpecialNotificationFireEight->setAlias("fire_alarms_may_sound_during_repairs");
        $testSpecialNotificationFireEight->setDivision($this->getReference('device-category-fire'));
        $manager->persist($testSpecialNotificationFireEight);
        $manager->flush();
        $this->addReference('fire-fire-alarms-may-sound-during-repairs-division-fire-notification', $testSpecialNotificationFireEight);

        /**
         *  PLUMBING
         */

        $testSpecialNotificationPlumbingOne = new SpecialNotification();
        $testSpecialNotificationPlumbingOne->setName("Domestic Water to be off for 1 hour");
        $testSpecialNotificationPlumbingOne->setAlias("plumbing_domestic_water_to_be_off_for_1_hour");
        $testSpecialNotificationPlumbingOne->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($testSpecialNotificationPlumbingOne);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-1-hour-plumbing-special-notification', $testSpecialNotificationPlumbingOne);

        $testSpecialNotificationPlumbingTwo = new SpecialNotification();
        $testSpecialNotificationPlumbingTwo->setName("Domestic Water to be off for 2 Hrs");
        $testSpecialNotificationPlumbingTwo->setAlias("plumbing_domestic_water_to_be_off_for_2_hours");
        $testSpecialNotificationPlumbingTwo->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($testSpecialNotificationPlumbingTwo);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-2-hours-plumbing-special-notification', $testSpecialNotificationPlumbingTwo);

        $testSpecialNotificationPlumbingThree = new SpecialNotification();
        $testSpecialNotificationPlumbingThree->setName("Domestic Water to be off for 4 Hrs");
        $testSpecialNotificationPlumbingThree->setAlias("plumbing_domestic_water_to_be_off_for_4_hours");
        $testSpecialNotificationPlumbingThree->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($testSpecialNotificationPlumbingThree);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-4-hours-plumbing-special-notification', $testSpecialNotificationPlumbingThree);

        $testSpecialNotificationPlumbingFour = new SpecialNotification();
        $testSpecialNotificationPlumbingFour->setName("Domestic Water to be off for 6 Hrs");
        $testSpecialNotificationPlumbingFour->setAlias("plumbing_domestic_water_to_be_off_for_6_hours");
        $testSpecialNotificationPlumbingFour->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($testSpecialNotificationPlumbingFour);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-6-hours-plumbing-special-notification', $testSpecialNotificationPlumbingFour);

        $testSpecialNotificationPlumbingFive = new SpecialNotification();
        $testSpecialNotificationPlumbingFive->setName("Domestic Water to be off for 8 Hrs");
        $testSpecialNotificationPlumbingFive->setAlias("plumbing_domestic_water_to_be_off_for_8_hours");
        $testSpecialNotificationPlumbingFive->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($testSpecialNotificationPlumbingFive);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-8-hours-plumbing-special-notification', $testSpecialNotificationPlumbingFive);

        $testSpecialNotificationPlumbingSix = new SpecialNotification();
        $testSpecialNotificationPlumbingSix->setName("Domestic Water to be off for 24 Hrs");
        $testSpecialNotificationPlumbingSix->setAlias("plumbing_domestic_water_to_be_off_for_24_hours");
        $testSpecialNotificationPlumbingSix->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($testSpecialNotificationPlumbingSix);
        $manager->flush();
        $this->addReference('domestic-water-to-be-off-for-24-hours-plumbing-special-notification', $testSpecialNotificationPlumbingSix);

        $testSpecialNotificationPlumbingSeven = new SpecialNotification();
        $testSpecialNotificationPlumbingSeven->setName("Domestic Water Will Not be Affected");
        $testSpecialNotificationPlumbingSeven->setAlias("plumbing_domestic_water_will_not_be_affected");
        $testSpecialNotificationPlumbingSeven->setDivision($this->getReference('device-category-plumbing'));
        $manager->persist($testSpecialNotificationPlumbingSeven);
        $manager->flush();
        $this->addReference('domestic-water-will-not-be-affected-plumbing-special-notification', $testSpecialNotificationPlumbingSeven);

    }

    public function getOrder()
    {
        return 10;
    }
}
