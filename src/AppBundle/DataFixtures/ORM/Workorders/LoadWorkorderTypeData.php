<?php

namespace AppBundle\DataFixtures\ORM\Workorders;

use AppBundle\Entity\WorkorderType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadWorkorderTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $workorderGeneric = new WorkorderType();
        $workorderGeneric->setName('Generic Workorder');
        $workorderGeneric->setAlias('generic_workorder');
        $manager->persist($workorderGeneric);
        $manager->flush();

        $this->addReference('workorder-type-generic', $workorderGeneric);

        $workorderSimple = new WorkorderType();
        $workorderSimple->setName('Workorder');
        $workorderSimple->setAlias('simple_workorder');
        $manager->persist($workorderSimple);
        $manager->flush();

        $this->addReference('workorder-type-simple', $workorderSimple);

    }

    public function getOrder()
    {
        return 1;
    }
}
