<?php

namespace AppBundle\DataFixtures\ORM\Workorders;

use AppBundle\Entity\WorkorderLogType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadWorkorderLogTypeData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $workorderlLogType = new WorkorderLogType();
        $workorderlLogType->setName('WORKORDER WAS CREATED');
        $workorderlLogType->setAlias('workorder_was_created');

        $manager->persist($workorderlLogType);
        $manager->flush();

        $workorderLogType2 = new WorkorderLogType();
        $workorderLogType2->setName('WORKORDER WAS SCHEDULED');
        $workorderLogType2->setAlias('workorder_was_scheduled');

        $manager->persist($workorderLogType2);
        $manager->flush();

        $reportSend = new WorkorderLogType();
        $reportSend->setName('Reports sent');
        $reportSend->setAlias('reports_sent');

        $manager->persist($reportSend);
        $manager->flush();
        $paymentReceived = new WorkorderLogType();
        $paymentReceived->setName('Payment received');
        $paymentReceived->setAlias('payment_received');

        $manager->persist($paymentReceived);
        $manager->flush();

        $sendBillCreateInvoice = new WorkorderLogType();
        $sendBillCreateInvoice->setName('Bill Sent / Invoice Created');
        $sendBillCreateInvoice->setAlias('bill_sent_invoice_created');

        $manager->persist($sendBillCreateInvoice);
        $manager->flush();

        $workorderlLogType3 = new WorkorderLogType();
        $workorderlLogType3->setName('Scheduled date has come');
        $workorderlLogType3->setAlias('scheduled_date_has_come');

        $manager->persist($workorderlLogType3);
        $manager->flush();

        $workorderlLogType4 = new WorkorderLogType();
        $workorderlLogType4->setName('Scheduled date has passed');
        $workorderlLogType4->setAlias('scheduled_date_has_passed');

        $manager->persist($workorderlLogType4);
        $manager->flush();

        $done = new WorkorderLogType();
        $done->setName('DONE');
        $done->setAlias('done');

        $workorderLogInvoiceDraftWasSave = new WorkorderLogType();
        $workorderLogInvoiceDraftWasSave->setName('Invoice draft was save');
        $workorderLogInvoiceDraftWasSave->setAlias('invoice_draft_was_save');


        $manager->persist($workorderLogInvoiceDraftWasSave);
        $manager->flush();

        $workorderLogTypeInvoiceSubmitted = new WorkorderLogType();
        $workorderLogTypeInvoiceSubmitted->setName('Invoice submitted');
        $workorderLogTypeInvoiceSubmitted->setAlias('invoice_submitted');


        $manager->persist($workorderLogTypeInvoiceSubmitted);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}