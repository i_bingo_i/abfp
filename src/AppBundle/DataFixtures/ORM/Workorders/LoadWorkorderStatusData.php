<?php

namespace AppBundle\DataFixtures\ORM\Workorders;

use AppBundle\Entity\WorkorderStatus;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadWorkorderStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $toBeScheduled = new WorkorderStatus();
        $toBeScheduled->setName('To Be Scheduled');
        $toBeScheduled->setAlias('to_be_scheduled');
        $toBeScheduled->setCssClass('app-status-label--red');
        $toBeScheduled->setPriority(1);
        $manager->persist($toBeScheduled);
        $manager->flush();

        $this->addReference('workorder-status-to-be-scheduled', $toBeScheduled);


        $statusScheduled = new WorkorderStatus();
        $statusScheduled->setName('Scheduled');
        $statusScheduled->setAlias('scheduled');
        $statusScheduled->setCssClass('app-status-label--green');
        $statusScheduled->setPriority(2);
        $manager->persist($statusScheduled);
        $manager->flush();

        $this->addReference('workorder-status-scheduled', $statusScheduled);

        $toBeDoneToday = new WorkorderStatus();
        $toBeDoneToday->setName('TO BE DONE TODAY');
        $toBeDoneToday->setAlias('to_be_done_today');
        $toBeDoneToday->setCssClass('app-status-label--yellow');
        $toBeDoneToday->setPriority(3);
        $manager->persist($toBeDoneToday);
        $manager->flush();

        $this->addReference('workorder-status-to-be-done-today', $toBeDoneToday);

        $notCompleted = new WorkorderStatus();
        $notCompleted->setName('NOT COMPLETED');
        $notCompleted->setAlias('not_completed');
        $notCompleted->setCssClass('app-status-label--red');
        $notCompleted->setPriority(4);
        $manager->persist($notCompleted);
        $manager->flush();

        $this->addReference('workorder-status-not-completed', $notCompleted);

        $pendingPayment = new WorkorderStatus();
        $pendingPayment->setName('PENDING PAYMENT');
        $pendingPayment->setAlias('pending_payment');
        $pendingPayment->setCssClass('app-status-label--green');
        $pendingPayment->setPriority(6);
        $manager->persist($pendingPayment);
        $manager->flush();

        $this->addReference('workorder-status-pending-payment', $pendingPayment);

        $sendReports = new WorkorderStatus();
        $sendReports->setName('Send Reports');
        $sendReports->setAlias('send_reports');
        $sendReports->setCssClass('app-status-label--red');
        $sendReports->setPriority(7);
        $manager->persist($sendReports);
        $manager->flush();

        $this->addReference('workorder-status-send-reports', $sendReports);

        $statusSendBillCreateInvoice = new WorkorderStatus();
        $statusSendBillCreateInvoice->setName('Send Bill/Create Invoice');
        $statusSendBillCreateInvoice->setAlias('send_bill_create_invoice');
        $statusSendBillCreateInvoice->setCssClass('app-status-label--red');
        $statusSendBillCreateInvoice->setPriority(5);
        $manager->persist($statusSendBillCreateInvoice);
        $manager->flush();

        $this->addReference('workorder-status-send-bill-create-invoice', $statusScheduled);

        $done = new WorkorderStatus();
        $done->setName('DONE');
        $done->setAlias('done');
        $done->setCssClass('app-status-label--gray');
        $done->setPriority(8);
        $manager->persist($done);
        $manager->flush();

        $this->addReference('workorder-status-done', $done);

        $sendInvoice = new WorkorderStatus();
        $sendInvoice->setName('SEND INVOICE');
        $sendInvoice->setAlias('send_invoice');
        $sendInvoice->setCssClass('app-status-label--yellow');
        $sendInvoice->setPriority(6);
        $manager->persist($sendInvoice);
        $manager->flush();

        $this->addReference('workorder-status-send-invoice', $done);
    }

    public function getOrder()
    {
        return 1;
    }
}
