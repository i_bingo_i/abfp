<?php

namespace AppBundle\DataFixtures\ORM\Workorders;

use AppBundle\Entity\PaymentMethod;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPaymentMethodData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Bill
        $billPaymentMethod = new PaymentMethod();
        $billPaymentMethod->setName('Bill');
        $billPaymentMethod->setAlias('bill');
        $billPaymentMethod->setIsDefault(true);
        $manager->persist($billPaymentMethod);
        $manager->flush();

        $this->addReference('payment-method-bill', $billPaymentMethod);

        //Cash
        $cashPaymentMethod = new PaymentMethod();
        $cashPaymentMethod->setName('Cash');
        $cashPaymentMethod->setAlias('cash');
        $cashPaymentMethod->setIsDefault(false);
        $manager->persist($cashPaymentMethod);
        $manager->flush();

        $this->addReference('payment-method-cash', $cashPaymentMethod);

        //Check
        $checkPaymentMethod = new PaymentMethod();
        $checkPaymentMethod->setName('Check');
        $checkPaymentMethod->setAlias('check');
        $checkPaymentMethod->setIsDefault(false);
        $manager->persist($checkPaymentMethod);
        $manager->flush();

        $this->addReference('payment-method-check', $checkPaymentMethod);

        //Credit Card
        $creditCardPaymentMethod = new PaymentMethod();
        $creditCardPaymentMethod->setName('Credit Card');
        $creditCardPaymentMethod->setAlias('credit_card');
        $creditCardPaymentMethod->setIsDefault(false);
        $manager->persist($creditCardPaymentMethod);
        $manager->flush();

        $this->addReference('payment-method-credit-card', $creditCardPaymentMethod);
    }

    public function getOrder()
    {
        return 1;
    }
}
