<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\QuestionType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadQuestionTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $questionTypeGreen = new QuestionType();
        $questionTypeGreen->setName('Green');
        $questionTypeGreen->setAlias('green');
        $manager->persist($questionTypeGreen);
        $manager->flush();

        $this->addReference('question-type-green', $questionTypeGreen);

        $questionTypeBlue = new QuestionType();
        $questionTypeBlue->setName('Blue');
        $questionTypeBlue->setAlias('blue');
        $manager->persist($questionTypeBlue);
        $manager->flush();

        $this->addReference('question-type-blue', $questionTypeBlue);

        $questionTypeNotTestable = new QuestionType();
        $questionTypeNotTestable->setName('Not Testable');
        $questionTypeNotTestable->setAlias('not_testable');
        $manager->persist($questionTypeNotTestable);
        $manager->flush();

        $this->addReference('question-type-not-testable', $questionTypeNotTestable);
    }

    public function getOrder()
    {
        return 1;
    }
}
