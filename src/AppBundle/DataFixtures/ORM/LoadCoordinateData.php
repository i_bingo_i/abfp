<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Coordinate;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCoordinateData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $coordinate1 = new Coordinate();
        $coordinate1->setLatitude(40.7127837);
        $coordinate1->setLongitude(-74.0059413);
        $manager->persist($coordinate1);
        $manager->flush();

        $this->setReference('app-coordinate-1', $coordinate1);

        $coordinate2 = new Coordinate();
        $coordinate2->setLatitude(34.0522342);
        $coordinate2->setLongitude(-118.2436849);
        $manager->persist($coordinate2);
        $manager->flush();

        $this->setReference('app-coordinate-2', $coordinate2);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}