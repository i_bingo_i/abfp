<?php

namespace AppBundle\DataFixtures\ORM\Licenses;

use AppBundle\Entity\LicensesDropboxChoices;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLicensesDropboxChoicesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        // NICET Certification for NICET Certification Choices
        $NICETCertificationFieldChoice1 = new LicensesDropboxChoices();
        $NICETCertificationFieldChoice1->setField($this->getReference('nicet-cerification-field-for-nicet'));
        $NICETCertificationFieldChoice1->setOptionValue("Fire Alarms");
        $manager->persist($NICETCertificationFieldChoice1);
        $manager->flush();
        $this->addReference('nicet-cerification-field-choice-1', $NICETCertificationFieldChoice1);

        $NICETCertificationFieldChoice2 = new LicensesDropboxChoices();
        $NICETCertificationFieldChoice2->setField($this->getReference('nicet-cerification-field-for-nicet'));
        $NICETCertificationFieldChoice2->setOptionValue("Inspection and Testing alarms");
        $manager->persist($NICETCertificationFieldChoice2);
        $manager->flush();
        $this->addReference('nicet-cerification-field-choice-2', $NICETCertificationFieldChoice2);

        $NICETCertificationFieldChoice3 = new LicensesDropboxChoices();
        $NICETCertificationFieldChoice3->setField($this->getReference('nicet-cerification-field-for-nicet'));
        $NICETCertificationFieldChoice3->setOptionValue("Special Hazards");
        $manager->persist($NICETCertificationFieldChoice3);
        $manager->flush();
        $this->addReference('nicet-cerification-field-choice-3', $NICETCertificationFieldChoice3);


        $NICETCertificationFieldChoice4 = new LicensesDropboxChoices();
        $NICETCertificationFieldChoice4->setField($this->getReference('nicet-cerification-field-for-nicet'));
        $NICETCertificationFieldChoice4->setOptionValue("Inspection and Testing WBS");
        $manager->persist($NICETCertificationFieldChoice4);
        $manager->flush();
        $this->addReference('nicet-cerification-field-choice-4', $NICETCertificationFieldChoice4);

        $NICETCertificationFieldChoice5 = new LicensesDropboxChoices();
        $NICETCertificationFieldChoice5->setField($this->getReference('nicet-cerification-field-for-nicet'));
        $NICETCertificationFieldChoice5->setOptionValue("Water Based Systems Layout");
        $manager->persist($NICETCertificationFieldChoice5);
        $manager->flush();
        $this->addReference('nicet-cerification-field-choice-5', $NICETCertificationFieldChoice5);
        /****************************************************************/

        // NICET Level for NICET Certification Choices
        $NICETLevelFieldChoice1 = new LicensesDropboxChoices();
        $NICETLevelFieldChoice1->setField($this->getReference('nicet-level-field-for-nicet'));
        $NICETLevelFieldChoice1->setOptionValue("I");
        $manager->persist($NICETLevelFieldChoice1);
        $manager->flush();
        $this->addReference('nicet-level-field-choice-1', $NICETLevelFieldChoice1);

        $NICETLevelFieldChoice2 = new LicensesDropboxChoices();
        $NICETLevelFieldChoice2->setField($this->getReference('nicet-level-field-for-nicet'));
        $NICETLevelFieldChoice2->setOptionValue("II");
        $manager->persist($NICETLevelFieldChoice2);
        $manager->flush();
        $this->addReference('nicet-level-field-choice-2', $NICETLevelFieldChoice2);

        $NICETLevelFieldChoice3 = new LicensesDropboxChoices();
        $NICETLevelFieldChoice3->setField($this->getReference('nicet-level-field-for-nicet'));
        $NICETLevelFieldChoice3->setOptionValue("III");
        $manager->persist($NICETLevelFieldChoice3);
        $manager->flush();
        $this->addReference('nicet-level-field-choice-3', $NICETLevelFieldChoice3);

        $NICETLevelFieldChoice4 = new LicensesDropboxChoices();
        $NICETLevelFieldChoice4->setField($this->getReference('nicet-level-field-for-nicet'));
        $NICETLevelFieldChoice4->setOptionValue("IV");
        $manager->persist($NICETLevelFieldChoice4);
        $manager->flush();
        $this->addReference('nicet-level-field-choice-4', $NICETLevelFieldChoice4);
        /****************************************************************/

        // NAFED Certification for NAFED Certification Choices
        $NAFEDCertificationFieldChoice1 = new LicensesDropboxChoices();
        $NAFEDCertificationFieldChoice1->setField($this->getReference('nafed-certification-field-for-nafed'));
        $NAFEDCertificationFieldChoice1->setOptionValue("Portable Fire Extinguishers");
        $manager->persist($NAFEDCertificationFieldChoice1);
        $manager->flush();
        $this->addReference('nafed-cerification-field-choice-1', $NAFEDCertificationFieldChoice1);

        $NAFEDCertificationFieldChoice2 = new LicensesDropboxChoices();
        $NAFEDCertificationFieldChoice2->setField($this->getReference('nafed-certification-field-for-nafed'));
        $NAFEDCertificationFieldChoice2->setOptionValue("Pre-Engineered Kitchen Fire Extinguishing Systems");
        $manager->persist($NAFEDCertificationFieldChoice2);
        $manager->flush();
        $this->addReference('nafed-cerification-field-choice-2', $NAFEDCertificationFieldChoice2);

        $NAFEDCertificationFieldChoice3 = new LicensesDropboxChoices();
        $NAFEDCertificationFieldChoice3->setField($this->getReference('nafed-certification-field-for-nafed'));
        $NAFEDCertificationFieldChoice3->setOptionValue("Pre-Engineered Industrial Fire Extinguishing Systems");
        $manager->persist($NAFEDCertificationFieldChoice3);
        $manager->flush();
        $this->addReference('nafed-cerification-field-choice-3', $NAFEDCertificationFieldChoice3);

        $NAFEDCertificationFieldChoice4 = new LicensesDropboxChoices();
        $NAFEDCertificationFieldChoice4->setField($this->getReference('nafed-certification-field-for-nafed'));
        $NAFEDCertificationFieldChoice4->setOptionValue("Engineered Fire Suppression Systems");
        $manager->persist($NAFEDCertificationFieldChoice4);
        $manager->flush();
        $this->addReference('nafed-cerification-field-choice-4', $NAFEDCertificationFieldChoice4);

    }

    public function getOrder()
    {
        return 13;
    }
}
