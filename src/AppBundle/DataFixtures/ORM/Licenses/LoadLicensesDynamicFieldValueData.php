<?php

namespace AppBundle\DataFixtures\ORM\Licenses;

use AppBundle\Entity\LicensesDynamicFieldValue;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLicensesDynamicFieldValueData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // NICET Certification for NICET Certification
        $NICETCertificationFieldForNICETCertification = new LicensesDynamicFieldValue();
        $NICETCertificationFieldForNICETCertification->setOptionValue($this->getReference('nicet-cerification-field-choice-2'));
        $NICETCertificationFieldForNICETCertification->setField($this->getReference('nicet-cerification-field-for-nicet'));
        $NICETCertificationFieldForNICETCertification->setLicense($this->getReference('nicent-license-one'));
        $manager->persist($NICETCertificationFieldForNICETCertification);
        $manager->flush();

        $this->addReference('value-nicet-cerification-field-for-nicet', $NICETCertificationFieldForNICETCertification);


        // NICET Level for NICET Certification
        $NICETLevelFieldForNICETCertification = new LicensesDynamicFieldValue();
        $NICETLevelFieldForNICETCertification->setOptionValue($this->getReference('nicet-level-field-choice-4'));
        $NICETLevelFieldForNICETCertification->setField($this->getReference('nicet-level-field-for-nicet'));
        $NICETLevelFieldForNICETCertification->setLicense($this->getReference('nicent-license-one'));
        $manager->persist($NICETLevelFieldForNICETCertification);
        $manager->flush();

        $this->addReference('value-nicet-level-field-for-nicet', $NICETLevelFieldForNICETCertification);


        // NAFED Certification for NAFED Certification
        $NAFEDCertificationFieldForNAFEDCertification = new LicensesDynamicFieldValue();
        $NAFEDCertificationFieldForNAFEDCertification->setOptionValue($this->getReference('nafed-cerification-field-choice-2'));
        $NAFEDCertificationFieldForNAFEDCertification->setField($this->getReference('nafed-certification-field-for-nafed'));
        $NAFEDCertificationFieldForNAFEDCertification->setLicense($this->getReference('nafed-license-one'));
        $manager->persist($NAFEDCertificationFieldForNAFEDCertification);
        $manager->flush();

        $this->addReference('value-nafed-level-field-for-nafed', $NAFEDCertificationFieldForNAFEDCertification);
    }

    public function getOrder()
    {
        return 14;
    }
}
