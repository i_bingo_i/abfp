<?php

namespace AppBundle\DataFixtures\ORM\Licenses;

use AppBundle\Entity\LicensesNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLicensesNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $backflowLicenseNamedOne = new LicensesNamed();
        $backflowLicenseNamedOne->setName("Backflow License");
        $backflowLicenseNamedOne->setAlias("backflow license");
        $backflowLicenseNamedOne->addCategory($this->getReference('license-category-backflow'));
        $manager->persist($backflowLicenseNamedOne);
        $manager->flush();

        $this->addReference('backflow-license-named-one', $backflowLicenseNamedOne);

        $NAFEDLicenseNamedOne = new LicensesNamed();
        $NAFEDLicenseNamedOne->setName("NAFED License");
        $NAFEDLicenseNamedOne->setAlias("nafed license");
        $NAFEDLicenseNamedOne->addCategory($this->getReference('license-category-fire'));
        $manager->persist($NAFEDLicenseNamedOne);
        $manager->flush();

        $this->addReference('nafed-license-named-one', $NAFEDLicenseNamedOne);

        $plumbersLicenseNamedOne = new LicensesNamed();
        $plumbersLicenseNamedOne->setName("Plumbers License");
        $plumbersLicenseNamedOne->setAlias("plumbers license");
        $plumbersLicenseNamedOne->addCategory($this->getReference('license-category-plumbing'));
        $manager->persist($plumbersLicenseNamedOne);
        $manager->flush();

        $this->addReference('plumbers-license-named-one', $plumbersLicenseNamedOne);


        $NICETLicenseNamedOne = new LicensesNamed();
        $NICETLicenseNamedOne->setName("NICET License");
        $NICETLicenseNamedOne->setAlias("nicet license");
        $NICETLicenseNamedOne->addCategory($this->getReference('license-category-alarm'));
        $NICETLicenseNamedOne->addCategory($this->getReference('license-category-fire'));
        $manager->persist($NICETLicenseNamedOne);
        $manager->flush();

        $this->addReference('nicent-license-named-one', $NICETLicenseNamedOne);


        $driversLicenseNamedOne = new LicensesNamed();
        $driversLicenseNamedOne->setName("Drivers License");
        $driversLicenseNamedOne->setAlias("drivers license");
        $driversLicenseNamedOne->addCategory($this->getReference('license-category-driver'));
        $manager->persist($driversLicenseNamedOne);
        $manager->flush();

        $this->addReference('driver-license-named-one', $driversLicenseNamedOne);
    }

    public function getOrder()
    {
        return 10;
    }
}
