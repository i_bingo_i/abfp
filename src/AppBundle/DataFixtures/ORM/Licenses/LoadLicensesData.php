<?php

namespace AppBundle\DataFixtures\ORM\Licenses;

use AppBundle\Entity\Licenses;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLicensesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $backflowLicenseOne = new Licenses();
        $backflowLicenseOne->setCode("863650921346");
        $backflowLicenseOne->setState($this->getReference('state-alabama'));
        $backflowLicenseOne->setRenewalDate(new \DateTime("+3 months"));
        $backflowLicenseOne->setNamed($this->getReference('backflow-license-named-one'));
        $backflowLicenseOne->setContractorUser($this->getReference('contractor-user-james-hatfield-for-monarch'));
        $manager->persist($backflowLicenseOne);
        $manager->flush();

        $this->addReference('backflow-license-one', $backflowLicenseOne);

        $NAFEDLicenseOne = new Licenses();
        $NAFEDLicenseOne->setCode("761355732338");
        $NAFEDLicenseOne->setRenewalDate(new \DateTime("+1 month"));
        $NAFEDLicenseOne->setNamed($this->getReference('nafed-license-named-one'));
        $NAFEDLicenseOne->setContractorUser($this->getReference('contractor-user-kirk-hammet-for-monarch'));
        $manager->persist($NAFEDLicenseOne);
        $manager->flush();

        $this->addReference('nafed-license-one', $NAFEDLicenseOne);


        $plumbersLicenseOne = new Licenses();
        $plumbersLicenseOne->setCode("863650921346");
        $plumbersLicenseOne->setState($this->getReference('state-colorado'));
        $plumbersLicenseOne->setRenewalDate(new \DateTime("+2 months"));
        $plumbersLicenseOne->setNamed($this->getReference('plumbers-license-named-one'));
        $plumbersLicenseOne->setContractorUser($this->getReference('contractor-user-james-hatfield-for-monarch'));
        $manager->persist($plumbersLicenseOne);
        $manager->flush();

        $this->addReference('plumbers-license-one', $plumbersLicenseOne);


        $NICETLicenseOne = new Licenses();
        $NICETLicenseOne->setCode("521355789531");
        $NICETLicenseOne->setRenewalDate(new \DateTime("+4 months"));
        $NICETLicenseOne->setNamed($this->getReference('nicent-license-named-one'));
        $NICETLicenseOne->setContractorUser($this->getReference('contractor-user-kirk-hammet-for-monarch'));
        $manager->persist($NICETLicenseOne);
        $manager->flush();

        $this->addReference('nicent-license-one', $NICETLicenseOne);


        $driversLicenseOne = new Licenses();
        $driversLicenseOne->setCode("345673289519");
        $driversLicenseOne->setState($this->getReference('state-alaska'));
        $driversLicenseOne->setRenewalDate(new \DateTime("+1 month"));
        $driversLicenseOne->setNamed($this->getReference('driver-license-named-one'));
        $driversLicenseOne->setContractorUser($this->getReference('contractor-user-james-hatfield-for-monarch'));
        $manager->persist($driversLicenseOne);
        $manager->flush();

        $this->addReference('driver-license-one', $driversLicenseOne);

        $NAFEDMarkZ = new Licenses();
        $NAFEDMarkZ->setCode("764355232345");
        $NAFEDMarkZ->setRenewalDate(new \DateTime("+1 month"));
        $NAFEDMarkZ->setNamed($this->getReference('nafed-license-named-one'));
        $NAFEDMarkZ->setContractorUser($this->getReference('contractor-user-mark-zuckerberg'));
        $manager->persist($NAFEDMarkZ);
        $manager->flush();

        $this->addReference('nafed-license-mark-z', $NAFEDMarkZ);

        $backflowMask = new Licenses();
        $backflowMask->setCode("784325537345");
        $backflowMask->setRenewalDate(new \DateTime("+1 month"));
        $backflowMask->setNamed($this->getReference('backflow-license-named-one'));
        $backflowMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
        $manager->persist($backflowMask);
        $manager->flush();

        $this->addReference('nafed-license-elon-m', $backflowMask);

        $backflowFireJakFresko = new Licenses();
        $backflowFireJakFresko->setCode("712325537115");
        $backflowFireJakFresko->setRenewalDate(new \DateTime("+1 month"));
        $backflowFireJakFresko->setNamed($this->getReference('backflow-license-named-one'));
        $backflowFireJakFresko->setContractorUser($this->getReference('contractor-user-jak-fresko'));
        $manager->persist($backflowFireJakFresko);
        $manager->flush();

        $this->addReference('backflow-license-jak-f', $backflowFireJakFresko);

        $backflowFireJakFresko = new Licenses();
        $backflowFireJakFresko->setCode("712325537115");
        $backflowFireJakFresko->setRenewalDate(new \DateTime("+1 month"));
        $backflowFireJakFresko->setNamed($this->getReference('nafed-license-named-one'));
        $backflowFireJakFresko->setContractorUser($this->getReference('contractor-user-jak-fresko'));
        $manager->persist($backflowFireJakFresko);
        $manager->flush();

        $this->addReference('nafed-license-jak-f', $backflowFireJakFresko);



        /** New Contractors */

        /** Bill Gates */
        $backflowBillGates = new Licenses();
        $backflowBillGates->setCode("2342344323112");
        $backflowBillGates->setRenewalDate(new \DateTime("+4 month"));
        $backflowBillGates->setNamed($this->getReference('backflow-license-named-one'));
        $backflowBillGates->setContractorUser($this->getReference('contractor-user-bill-gates'));
        $manager->persist($backflowBillGates);
        $manager->flush();

        $this->addReference('backflow-license-bill-g', $backflowBillGates);


        $fireBillGates = new Licenses();
        $fireBillGates->setCode("612327767115");
        $fireBillGates->setRenewalDate(new \DateTime("+5 month"));
        $fireBillGates->setNamed($this->getReference('nafed-license-named-one'));
        $fireBillGates->setContractorUser($this->getReference('contractor-user-bill-gates'));
        $manager->persist($fireBillGates);
        $manager->flush();

        $this->addReference('nafed-license-bill-g', $fireBillGates);


        /** Sergey Brin */
        $backflowSergeyBrin = new Licenses();
        $backflowSergeyBrin->setCode("21212124356");
        $backflowSergeyBrin->setRenewalDate(new \DateTime("+4 month"));
        $backflowSergeyBrin->setNamed($this->getReference('backflow-license-named-one'));
        $backflowSergeyBrin->setContractorUser($this->getReference('contractor-user-sergey-brin'));
        $manager->persist($backflowSergeyBrin);
        $manager->flush();

        $this->addReference('backflow-license-sergey-b', $backflowSergeyBrin);


        $fireSergeyBrin= new Licenses();
        $fireSergeyBrin->setCode("12544565990");
        $fireSergeyBrin->setRenewalDate(new \DateTime("+5 month"));
        $fireSergeyBrin->setNamed($this->getReference('nafed-license-named-one'));
        $fireSergeyBrin->setContractorUser($this->getReference('contractor-user-sergey-brin'));
        $manager->persist($fireSergeyBrin);
        $manager->flush();

        $this->addReference('nafed-license-sergey-b', $fireSergeyBrin);


        /** Mark Zuckerberg */
        $backflowMarkZuckerberg = new Licenses();
        $backflowMarkZuckerberg->setCode("99000876552");
        $backflowMarkZuckerberg->setRenewalDate(new \DateTime("+4 month"));
        $backflowMarkZuckerberg->setNamed($this->getReference('backflow-license-named-one'));
        $backflowMarkZuckerberg->setContractorUser($this->getReference('contractor-user-mark-zuckerberg'));
        $manager->persist($backflowMarkZuckerberg);
        $manager->flush();

        $this->addReference('backflow-license-mark-z', $backflowMarkZuckerberg);



        /** Gabe Newell */
        $backflowGabeNewell = new Licenses();
        $backflowGabeNewell->setCode("99000876552");
        $backflowGabeNewell->setRenewalDate(new \DateTime("+4 month"));
        $backflowGabeNewell->setNamed($this->getReference('backflow-license-named-one'));
        $backflowGabeNewell->setContractorUser($this->getReference('contractor-user-gabe-newell'));
        $manager->persist($backflowGabeNewell);
        $manager->flush();

        $this->addReference('backflow-license-gabe-n', $backflowGabeNewell);


        $fireGabeNewell = new Licenses();
        $fireGabeNewell->setCode("345345343212");
        $fireGabeNewell->setRenewalDate(new \DateTime("+5 month"));
        $fireGabeNewell->setNamed($this->getReference('nafed-license-named-one'));
        $fireGabeNewell->setContractorUser($this->getReference('contractor-user-gabe-newell'));
        $manager->persist($fireGabeNewell);
        $manager->flush();

        $this->addReference('nafed-license-gabe-n', $fireGabeNewell);

        $plumbersFireJakFresko = new Licenses();
        $plumbersFireJakFresko->setCode("712325537115");
        $plumbersFireJakFresko->setRenewalDate(new \DateTime("+1 month"));
        $plumbersFireJakFresko->setNamed($this->getReference('plumbers-license-named-one'));
        $plumbersFireJakFresko->setContractorUser($this->getReference('contractor-user-jak-fresko'));
        $manager->persist($plumbersFireJakFresko);
        $manager->flush();

        $this->addReference('plumbers-license-jak-f', $plumbersFireJakFresko);


        $nicentFireJakFresko = new Licenses();
        $nicentFireJakFresko->setCode("712325537115");
        $nicentFireJakFresko->setRenewalDate(new \DateTime("+1 month"));
        $nicentFireJakFresko->setNamed($this->getReference('nicent-license-named-one'));
        $nicentFireJakFresko->setContractorUser($this->getReference('contractor-user-jak-fresko'));
        $manager->persist($nicentFireJakFresko);
        $manager->flush();

        $this->addReference('nicent-license-jak-f', $nicentFireJakFresko);


        $plumbersMask = new Licenses();
        $plumbersMask->setCode("784325537345");
        $plumbersMask->setRenewalDate(new \DateTime("+1 month"));
        $plumbersMask->setNamed($this->getReference('plumbers-license-named-one'));
        $plumbersMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
        $manager->persist($plumbersMask);
        $manager->flush();

        $this->addReference('plumbers-license-elon-m', $plumbersMask);


        $nicentMask = new Licenses();
        $nicentMask->setCode("784325537345");
        $nicentMask->setRenewalDate(new \DateTime("+1 month"));
        $nicentMask->setNamed($this->getReference('nicent-license-named-one'));
        $nicentMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
        $manager->persist($nicentMask);
        $manager->flush();

        $this->addReference('nicent-license-elon-m', $nicentMask);


        /** ********************************************************* **/
        /** Bill Gates */
//        $backflowBillGates = new Licenses();
//        $backflowBillGates->setCode("2342344323112");
//        $backflowBillGates->setRenewalDate(new \DateTime("+4 month"));
//        $backflowBillGates->setNamed($this->getReference('backflow-license-named-one'));
//        $backflowBillGates->setContractorUser($this->getReference('contractor-user-bill-gates'));
//        $manager->persist($backflowBillGates);
//        $manager->flush();
//
//        $this->addReference('backflow-license-bill-g', $backflowBillGates);
//
//
//        $fireBillGates = new Licenses();
//        $fireBillGates->setCode("612327767115");
//        $fireBillGates->setRenewalDate(new \DateTime("+5 month"));
//        $fireBillGates->setNamed($this->getReference('nafed-license-named-one'));
//        $fireBillGates->setContractorUser($this->getReference('contractor-user-bill-gates'));
//        $manager->persist($fireBillGates);
//        $manager->flush();
//
//        $this->addReference('nafed-license-bill-g', $fireBillGates);
//
//
//        /** Sergey Brin */
//        $backflowSergeyBrin = new Licenses();
//        $backflowSergeyBrin->setCode("21212124356");
//        $backflowSergeyBrin->setRenewalDate(new \DateTime("+4 month"));
//        $backflowSergeyBrin->setNamed($this->getReference('backflow-license-named-one'));
//        $backflowSergeyBrin->setContractorUser($this->getReference('contractor-user-sergey-brin'));
//        $manager->persist($backflowSergeyBrin);
//        $manager->flush();
//
//        $this->addReference('backflow-license-sergey-b', $backflowSergeyBrin);
//
//
//        $fireSergeyBrin= new Licenses();
//        $fireSergeyBrin->setCode("12544565990");
//        $fireSergeyBrin->setRenewalDate(new \DateTime("+5 month"));
//        $fireSergeyBrin->setNamed($this->getReference('nafed-license-named-one'));
//        $fireSergeyBrin->setContractorUser($this->getReference('contractor-user-sergey-brin'));
//        $manager->persist($fireSergeyBrin);
//        $manager->flush();
//
//        $this->addReference('nafed-license-sergey-b', $fireSergeyBrin);
//
//
//        /** Mark Zuckerberg */
//        $backflowMarkZuckerberg = new Licenses();
//        $backflowMarkZuckerberg->setCode("99000876552");
//        $backflowMarkZuckerberg->setRenewalDate(new \DateTime("+4 month"));
//        $backflowMarkZuckerberg->setNamed($this->getReference('backflow-license-named-one'));
//        $backflowMarkZuckerberg->setContractorUser($this->getReference('contractor-user-mark-zuckerberg'));
//        $manager->persist($backflowMarkZuckerberg);
//        $manager->flush();
//
//        $this->addReference('backflow-license-mark-z', $backflowMarkZuckerberg);
//
//        $NAFEDMarkZ = new Licenses();
//        $NAFEDMarkZ->setCode("764355232345");
//        $NAFEDMarkZ->setRenewalDate(new \DateTime("+1 month"));
//        $NAFEDMarkZ->setNamed($this->getReference('nafed-license-named-one'));
//        $NAFEDMarkZ->setContractorUser($this->getReference('contractor-user-mark-zuckerberg'));
//        $manager->persist($NAFEDMarkZ);
//        $manager->flush();
//
//        $this->addReference('nafed-license-mark-z', $NAFEDMarkZ);
//
//        /** Gabe Newell */
//        $backflowGabeNewell = new Licenses();
//        $backflowGabeNewell->setCode("99000876552");
//        $backflowGabeNewell->setRenewalDate(new \DateTime("+4 month"));
//        $backflowGabeNewell->setNamed($this->getReference('backflow-license-named-one'));
//        $backflowGabeNewell->setContractorUser($this->getReference('contractor-user-gabe-newell'));
//        $manager->persist($backflowGabeNewell);
//        $manager->flush();
//
//        $this->addReference('backflow-license-gabe-n', $backflowGabeNewell);
//
//
//        $fireGabeNewell = new Licenses();
//        $fireGabeNewell->setCode("345345343212");
//        $fireGabeNewell->setRenewalDate(new \DateTime("+5 month"));
//        $fireGabeNewell->setNamed($this->getReference('nafed-license-named-one'));
//        $fireGabeNewell->setContractorUser($this->getReference('contractor-user-gabe-newell'));
//        $manager->persist($fireGabeNewell);
//        $manager->flush();
//
//        $this->addReference('nafed-license-gabe-n', $fireGabeNewell);
//
//
//        /** Elon Mask */
//        $backflowMask = new Licenses();
//        $backflowMask->setCode("784325537345");
//        $backflowMask->setRenewalDate(new \DateTime("+1 month"));
//        $backflowMask->setNamed($this->getReference('backflow-license-named-one'));
//        $backflowMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
//        $manager->persist($backflowMask);
//        $manager->flush();
//
//        $this->addReference('backflow-license-elon-m', $backflowMask);
//
//
//        $fireMask = new Licenses();
//        $fireMask->setCode("345345343212");
//        $fireMask->setRenewalDate(new \DateTime("+5 month"));
//        $fireMask->setNamed($this->getReference('nafed-license-named-one'));
//        $fireMask->setContractorUser($this->getReference('contractor-user-gabe-newell'));
//        $manager->persist($fireMask);
//        $manager->flush();
//
//        $this->addReference('nafed-license-elon-m', $fireMask);


//        $plumbersFireJakFresko = new Licenses();
//        $plumbersFireJakFresko->setCode("712325537115");
//        $plumbersFireJakFresko->setRenewalDate(new \DateTime("+1 month"));
//        $plumbersFireJakFresko->setNamed($this->getReference('plumbers-license-named-one'));
//        $plumbersFireJakFresko->setContractorUser($this->getReference('contractor-user-jak-fresko'));
//        $manager->persist($plumbersFireJakFresko);
//        $manager->flush();
//
//        $this->addReference('plumbers-license-jak-f', $plumbersFireJakFresko);


//        $nicentFireJakFresko = new Licenses();
//        $nicentFireJakFresko->setCode("712325537115");
//        $nicentFireJakFresko->setRenewalDate(new \DateTime("+1 month"));
//        $nicentFireJakFresko->setNamed($this->getReference('nicent-license-named-one'));
//        $nicentFireJakFresko->setContractorUser($this->getReference('contractor-user-jak-fresko'));
//        $manager->persist($nicentFireJakFresko);
//        $manager->flush();
//
//        $this->addReference('nicent-license-jak-f', $nicentFireJakFresko);


//        $plumbersMask = new Licenses();
//        $plumbersMask->setCode("784325537345");
//        $plumbersMask->setRenewalDate(new \DateTime("+1 month"));
//        $plumbersMask->setNamed($this->getReference('plumbers-license-named-one'));
//        $plumbersMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
//        $manager->persist($plumbersMask);
//        $manager->flush();
//
//        $this->addReference('plumbers-license-elon-m', $plumbersMask);


//        $nicentMask = new Licenses();
//        $nicentMask->setCode("784325537345");
//        $nicentMask->setRenewalDate(new \DateTime("+1 month"));
//        $nicentMask->setNamed($this->getReference('nicent-license-named-one'));
//        $nicentMask->setContractorUser($this->getReference('contractor-user-elon-mask'));
//        $manager->persist($nicentMask);
//        $manager->flush();
//
//        $this->addReference('nicent-license-elon-m', $nicentMask);


        /** Dan Harbut */
        $backflowDanHarbut = new Licenses();
        $backflowDanHarbut->setCode("2324354435455");
        $backflowDanHarbut->setRenewalDate(new \DateTime("+4 month"));
        $backflowDanHarbut->setNamed($this->getReference('backflow-license-named-one'));
        $backflowDanHarbut->setContractorUser($this->getReference('contractor-user-dan-harbut'));
        $manager->persist($backflowDanHarbut);
        $manager->flush();

        $this->addReference('backflow-license-dan-h', $backflowDanHarbut);

        $NAFEDDanHarbut = new Licenses();
        $NAFEDDanHarbut->setCode("3324565677231");
        $NAFEDDanHarbut->setRenewalDate(new \DateTime("+2 month"));
        $NAFEDDanHarbut->setNamed($this->getReference('nafed-license-named-one'));
        $NAFEDDanHarbut->setContractorUser($this->getReference('contractor-user-dan-harbut'));
        $manager->persist($NAFEDDanHarbut);
        $manager->flush();

        $this->addReference('nafed-license-dan-h', $NAFEDDanHarbut);
    }

    public function getOrder()
    {
        return 12;
    }
}
