<?php

namespace AppBundle\DataFixtures\ORM\Licenses;

use AppBundle\Entity\LicensesDynamicField;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLicensesDynamicFieldData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // NICET Certification for NICET Certification
        $NICETCertificationFieldForNICETCertification = new LicensesDynamicField();
        $NICETCertificationFieldForNICETCertification->setName("NICET Certification");
        $NICETCertificationFieldForNICETCertification->setType($this->getReference('dynamic-field-type-dropdown'));
        $NICETCertificationFieldForNICETCertification->setAlias("nicet certification");
        $NICETCertificationFieldForNICETCertification->setLicense($this->getReference('nicent-license-named-one'));
        $manager->persist($NICETCertificationFieldForNICETCertification);
        $manager->flush();

        $this->addReference('nicet-cerification-field-for-nicet', $NICETCertificationFieldForNICETCertification);


        // NICET Level for NICET Certification
        $NICETLevelFieldForNICETCertification = new LicensesDynamicField();
        $NICETLevelFieldForNICETCertification->setName("NICET Level");
        $NICETLevelFieldForNICETCertification->setType($this->getReference('dynamic-field-type-dropdown'));
        $NICETLevelFieldForNICETCertification->setUseLabel(true);
        $NICETLevelFieldForNICETCertification->setLabelDescription("Level: ");
        $NICETLevelFieldForNICETCertification->setAlias("nicet level");
        $NICETLevelFieldForNICETCertification->setLicense($this->getReference('nicent-license-named-one'));
        $manager->persist($NICETLevelFieldForNICETCertification);
        $manager->flush();

        $this->addReference('nicet-level-field-for-nicet', $NICETLevelFieldForNICETCertification);


        // NAFED Certification for NAFED Certification
        $NAFEDCertificationFieldForNAFEDCertification = new LicensesDynamicField();
        $NAFEDCertificationFieldForNAFEDCertification->setName("NAFED Certification");
        $NAFEDCertificationFieldForNAFEDCertification->setType($this->getReference('dynamic-field-type-dropdown'));
        $NAFEDCertificationFieldForNAFEDCertification->setAlias("nafed certification");
        $NAFEDCertificationFieldForNAFEDCertification->setLicense($this->getReference('nafed-license-named-one'));
        $manager->persist($NAFEDCertificationFieldForNAFEDCertification);
        $manager->flush();

        $this->addReference('nafed-certification-field-for-nafed', $NAFEDCertificationFieldForNAFEDCertification);
    }

    public function getOrder()
    {
        return 12;
    }
}
