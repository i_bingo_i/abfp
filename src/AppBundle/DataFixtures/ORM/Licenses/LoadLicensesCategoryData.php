<?php

namespace AppBundle\DataFixtures\ORM\Licenses;

use AppBundle\Entity\LicensesCategory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLicensesCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //1
        $licenseCategoryOne = new LicensesCategory();
        $licenseCategoryOne->setName('Backflow');
        $licenseCategoryOne->setAlias('backflow');
        $licenseCategoryOne->setIcon('backflow_blue.svg');

        $manager->persist($licenseCategoryOne);
        $manager->flush();

        $this->addReference('license-category-backflow', $licenseCategoryOne);

        //2
        $licenseCategoryTwo = new LicensesCategory();
        $licenseCategoryTwo->setName('Fire');
        $licenseCategoryTwo->setAlias('fire');
        $licenseCategoryTwo->setIcon('fire_blue.svg');

        $manager->persist($licenseCategoryTwo);
        $manager->flush();

        $this->addReference('license-category-fire', $licenseCategoryTwo);

        //3
        $licenseCategoryThree = new LicensesCategory();
        $licenseCategoryThree->setName('Plumbing');
        $licenseCategoryThree->setAlias('plumbing');
        $licenseCategoryThree->setIcon('tool_blue.svg');

        $manager->persist($licenseCategoryThree);
        $manager->flush();

        $this->addReference('license-category-plumbing', $licenseCategoryThree);

        //4
        $licenseCategoryFour = new LicensesCategory();
        $licenseCategoryFour->setName('Alarm');
        $licenseCategoryFour->setAlias('alarm');
        $licenseCategoryFour->setIcon('alarm_blue.svg');

        $manager->persist($licenseCategoryFour);
        $manager->flush();

        $this->addReference('license-category-alarm', $licenseCategoryFour);

        //4
        $licenseCategoryFive = new LicensesCategory();
        $licenseCategoryFive->setName('Driver');
        $licenseCategoryFive->setAlias('driver');
        $licenseCategoryFive->setIcon('circle.svg');

        $manager->persist($licenseCategoryFive);
        $manager->flush();

        $this->addReference('license-category-driver', $licenseCategoryFive);
    }

    public function getOrder()
    {
        return 9;
    }
}
