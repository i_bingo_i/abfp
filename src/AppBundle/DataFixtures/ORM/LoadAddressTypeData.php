<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\AddressType;

class LoadAddressTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $addressTypeBilling = new AddressType();
        $addressTypeBilling->setName("Personal Address");
        $addressTypeBilling->setAlias("personal");
        $addressTypeBilling->setSort(3);
        $manager->persist($addressTypeBilling);
        $manager->flush();
        $this->addReference('address-type-personal', $addressTypeBilling);

        $addressTypeSiteAccount = new AddressType();
        $addressTypeSiteAccount->setName("Site Address");
        $addressTypeSiteAccount->setAlias("site address");
        $addressTypeSiteAccount->setSort(2);
        $manager->persist($addressTypeSiteAccount);
        $manager->flush();
        $this->addReference('address-type-site', $addressTypeSiteAccount);

        $addressTypeGroupAccount = new AddressType();
        $addressTypeGroupAccount->setName("Group Account Address");
        $addressTypeGroupAccount->setAlias("group account address");
        $addressTypeGroupAccount->setSort(2);
        $manager->persist($addressTypeGroupAccount);
        $manager->flush();
        $this->addReference('address-type-group', $addressTypeGroupAccount);

        $addressTypeCompany= new AddressType();
        $addressTypeCompany->setName("Company Address");
        $addressTypeCompany->setAlias("company address");
        $addressTypeCompany->setSort(1);
        $manager->persist($addressTypeCompany);
        $manager->flush();
        $this->addReference('address-type-company', $addressTypeCompany);

        $addressTypeContractor = new AddressType();
        $addressTypeContractor->setName("Contractor Address");
        $addressTypeContractor->setAlias("contractor address");
        $addressTypeContractor->setSort(4);
        $manager->persist($addressTypeContractor);
        $manager->flush();
        $this->addReference('address-type-contractor', $addressTypeContractor);

        $addressTypeContact = new AddressType();
        $addressTypeContact->setName("Contact Address");
        $addressTypeContact->setAlias("contact address");
        $addressTypeContact->setSort(5);
        $manager->persist($addressTypeContact);
        $manager->flush();
        $this->addReference('address-type-contact', $addressTypeContact);

        $oldSystemAddress= new AddressType();
        $oldSystemAddress->setName("Old System Master Address");
        $oldSystemAddress->setAlias("old system master address");
        $oldSystemAddress->setSort(6);
        $manager->persist($oldSystemAddress);
        $manager->flush();
        $this->addReference('address-type-old-system-address', $oldSystemAddress);

        $addressTypeMunicipality = new AddressType();
        $addressTypeMunicipality->setName("Municipality Address");
        $addressTypeMunicipality->setAlias("municipality");
        $addressTypeMunicipality->setSort(5);
        $manager->persist($addressTypeMunicipality);
        $manager->flush();
        $this->addReference('address-type-municipality', $addressTypeMunicipality);

        $billToAddress = new AddressType();
        $billToAddress->setName('Billing Address');
        $billToAddress->setAlias('billing');
        $billToAddress->setSort(7);
        $manager->persist($billToAddress);
        $this->addReference('address-type-billing', $billToAddress);

        $shipToType = new AddressType();
        $shipToType->setName('Ship to Address');
        $shipToType->setAlias('ship_to_address');
        $shipToType->setSort(8);
        $manager->persist($shipToType);
        $this->addReference('address-type-ship-to', $shipToType);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
