<?php

namespace AppBundle\DataFixtures\ORM\Agents;

use AppBundle\Entity\AgentChannel;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAgentChannelData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $agentChannel = new AgentChannel();
        $agentChannel->setIsDefault(1);
        $agentChannel->setNamed($this->getReference('email-channel-named'));
        $agentChannel->setAgent($this->getReference('agent-one'));
        $agentChannel->setDeviceCategory($this->getReference('device-category-backflow'));
        $manager->persist($agentChannel);
        $manager->flush();
        $this->addReference('agent-channel-one', $agentChannel);

        $agentChannel1 = new AgentChannel();
        $agentChannel1->setIsDefault(0);
        $agentChannel1->setNamed($this->getReference('mail-channel-named'));
        $agentChannel1->setAgent($this->getReference('agent-one'));
        $agentChannel1->setDeviceCategory($this->getReference('device-category-backflow'));
        $manager->persist($agentChannel1);
        $manager->flush();
        $this->addReference('agent-channel-two', $agentChannel1);

        $agentChannel2 = new AgentChannel();
        $agentChannel2->setIsDefault(1);
        $agentChannel2->setNamed($this->getReference('fax-channel-named'));
        $agentChannel2->setAgent($this->getReference('agent-one'));
        $agentChannel2->setDeviceCategory($this->getReference('device-category-fire'));
        $manager->persist($agentChannel2);
        $manager->flush();
        $this->addReference('agent-channel-three', $agentChannel2);

        $agentChannel3 = new AgentChannel();
        $agentChannel3->setIsDefault(1);
        $agentChannel3->setNamed($this->getReference('email-channel-named'));
        $agentChannel3->setAgent($this->getReference('agent-two'));
        $agentChannel3->setDeviceCategory($this->getReference('device-category-backflow'));
        $manager->persist($agentChannel3);
        $manager->flush();
        $this->addReference('agent-channel-four', $agentChannel3);

        $agentChannel5 = new AgentChannel();
        $agentChannel5->setNamed($this->getReference('upload-channel-named'));
        $agentChannel5->setAgent($this->getReference('agent-one'));
        $agentChannel5->setDeviceCategory($this->getReference('device-category-fire'));
        $manager->persist($agentChannel5);
        $manager->flush();
        $this->addReference('agent-channel-five', $agentChannel5);


        $agentChannel6 = new AgentChannel();
        $agentChannel6->setNamed($this->getReference('upload-channel-named'));
        $agentChannel6->setAgent($this->getReference('agent-one'));
        $agentChannel6->setDeviceCategory($this->getReference('device-category-backflow'));
        $manager->persist($agentChannel6);
        $manager->flush();
        $this->addReference('agent-channel-six', $agentChannel6);

        $agentChannel7 = new AgentChannel();
        $agentChannel7->setIsDefault(true);
        $agentChannel7->setNamed($this->getReference('upload-channel-named'));
        $agentChannel7->setAgent($this->getReference('agent-three'));
        $agentChannel7->setDeviceCategory($this->getReference('device-category-fire'));
        $manager->persist($agentChannel7);
        $manager->flush();
        $this->addReference('agent-channel-seven', $agentChannel7);
    }

    public function getOrder()
    {
        return 10;
    }
}