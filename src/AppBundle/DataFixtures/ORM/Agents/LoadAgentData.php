<?php

namespace AppBundle\DataFixtures\ORM\Agents;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Agent;

class LoadAgentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $agentOne = new Agent();
        $agentOne->setName('Apple');
        $agentOne->setWebsite('http://www.google.com');
        $agentOne->setPhone('(111) 111-1111');
        $agentOne->addContact($this->getReference('contact-fourteen'));
        $manager->persist($agentOne);
        $manager->flush();

        $this->addReference('agent-one', $agentOne);

        $agentTwo = new Agent();
        $agentTwo->setName('Bell Labs');
        $agentTwo->setWebsite('http://www.google.com');
        $agentTwo->setPhone('(311) 211-1111');
        $agentTwo->addContact($this->getReference('contact-eight'));
        $manager->persist($agentTwo);
        $manager->flush();

        $this->addReference('agent-two', $agentTwo);

        $agentThree = new Agent();
        $agentThree->setName('AT&T');
        $agentThree->setWebsite('https://www.att.com/');
        $agentThree->setPhone('(411) 311-1111');
        $agentThree->addContact($this->getReference('contact-nine'));
        $agentThree->addContact($this->getReference('contact-ten'));
        $manager->persist($agentThree);
        $manager->flush();

        $this->addReference('agent-three', $agentThree);
    }

    public function getOrder()
    {
        return 4;
    }
}
