<?php

namespace AppBundle\DataFixtures\ORM\Agents;

use AppBundle\Entity\AgentChannelDynamicFieldValue;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAgentChannelDynamicFieldValueData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $agentChannel = new AgentChannelDynamicFieldValue();
        $agentChannel->setValue('google@gmail.com');
        $agentChannel->setAgentChannel($this->getReference('agent-channel-one'));
        $agentChannel->setField($this->getReference('send-to-field-for-email-channel-named'));
        $manager->persist($agentChannel);
        $manager->flush();
        $this->addReference('agent-channel-dynamic-field-value-one', $agentChannel);

        $agentChannel1 = new AgentChannelDynamicFieldValue();
        $agentChannel1->setEntityValue($this->getReference("address-agent-channel-1"));
        $agentChannel1->setAgentChannel($this->getReference('agent-channel-two'));
        $agentChannel1->setField($this->getReference('send-to-field-for-mail-channel-named'));
        $manager->persist($agentChannel1);
        $manager->flush();
        $this->addReference('agent-channel-dynamic-field-value-two', $agentChannel1);

        $agentChannel2 = new AgentChannelDynamicFieldValue();
        $agentChannel2->setValue('1112223333');
        $agentChannel2->setAgentChannel($this->getReference('agent-channel-three'));
        $agentChannel2->setField($this->getReference('send-to-field-for-fax-channel-named'));
        $manager->persist($agentChannel2);
        $manager->flush();
        $this->addReference('agent-channel-dynamic-field-value-three', $agentChannel2);

        $agentChannel3 = new AgentChannelDynamicFieldValue();
        $agentChannel3->setValue('google@gmail.com');
        $agentChannel3->setAgentChannel($this->getReference('agent-channel-four'));
        $agentChannel3->setField($this->getReference('send-to-field-for-email-channel-named'));
        $manager->persist($agentChannel3);
        $manager->flush();
        $this->addReference('agent-channel-dynamic-field-value-four', $agentChannel3);

        $agentChannel5 = new AgentChannelDynamicFieldValue();
        $agentChannel5->setValue('google.com');
        $agentChannel5->setAgentChannel($this->getReference('agent-channel-five'));
        $agentChannel5->setField($this->getReference('send-to-field-for-upload-channel-named'));
        $manager->persist($agentChannel5);
        $manager->flush();

        $this->addReference('agent-channel-dynamic-field-value-five', $agentChannel5);


        $agentChannel6 = new AgentChannelDynamicFieldValue();
        $agentChannel6->setValue('yahoo.com');
        $agentChannel6->setAgentChannel($this->getReference('agent-channel-six'));
        $agentChannel6->setField($this->getReference('send-to-field-for-upload-channel-named'));
        $manager->persist($agentChannel6);
        $manager->flush();

        $this->addReference('agent-channel-dynamic-field-value-six', $agentChannel6);

        $agentChannel7 = new AgentChannelDynamicFieldValue();
        $agentChannel7->setValue('yahoo.com');
        $agentChannel7->setAgentChannel($this->getReference('agent-channel-seven'));
        $agentChannel7->setField($this->getReference('send-to-field-for-upload-channel-named'));
        $manager->persist($agentChannel7);
        $manager->flush();

        $this->addReference('agent-channel-dynamic-field-value-seven', $agentChannel7);
    }

    public function getOrder()
    {
        return 11;
    }
}