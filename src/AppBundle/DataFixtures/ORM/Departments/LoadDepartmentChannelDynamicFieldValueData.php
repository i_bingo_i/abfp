<?php

namespace AppBundle\DataFixtures\ORM\Departments;

use AppBundle\Entity\DepartmentChannelDynamicFieldValue;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDepartmentChannelDynamicFieldValueData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Value for Email channel for department one
        $departmentChannelDynamicFieldValue1 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue1->setField($this->getReference("send-to-field-for-email-channel-named"));
        $departmentChannelDynamicFieldValue1->setValue("mail@mail.com");
        $departmentChannelDynamicFieldValue1->setDepartmentChanel($this->getReference("email-department-channel-for-department-1"));
        $manager->persist($departmentChannelDynamicFieldValue1);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-1', $departmentChannelDynamicFieldValue1);

        // Value for Mail channel for department one
        $departmentChannelDynamicFieldValue2 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue2->setField($this->getReference("send-to-field-for-mail-channel-named"));
        $departmentChannelDynamicFieldValue2->setEntityValue($this->getReference("address-department-channel-1"));
        $departmentChannelDynamicFieldValue2->setDepartmentChanel($this->getReference("mail-department-channel-for-department-1"));
        $manager->persist($departmentChannelDynamicFieldValue2);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-2', $departmentChannelDynamicFieldValue2);

        // Value for Fax channel for department one
        $departmentChannelDynamicFieldValue3 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue3->setField($this->getReference("send-to-field-for-fax-channel-named"));
        $departmentChannelDynamicFieldValue3->setValue("1234567890");
        $departmentChannelDynamicFieldValue3->setDepartmentChanel($this->getReference("fax-department-channel-for-department-1"));
        $manager->persist($departmentChannelDynamicFieldValue3);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-3', $departmentChannelDynamicFieldValue3);

        // Value for Upload channel for department one
        $departmentChannelDynamicFieldValue4 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue4->setField($this->getReference("send-to-field-for-upload-channel-named"));
        $departmentChannelDynamicFieldValue4->setValue("http://web.cc.us/upload");
        $departmentChannelDynamicFieldValue4->setDepartmentChanel($this->getReference("upload-department-channel-for-department-1"));
        $manager->persist($departmentChannelDynamicFieldValue4);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-4', $departmentChannelDynamicFieldValue4);

        /*********************************************************************/

        // Value for Email channel for department two
        $departmentChannelDynamicFieldValue5 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue5->setField($this->getReference("send-to-field-for-email-channel-named"));
        $departmentChannelDynamicFieldValue5->setValue("mail-2@mail2.com");
        $departmentChannelDynamicFieldValue5->setDepartmentChanel($this->getReference("email-department-channel-for-department-2"));
        $manager->persist($departmentChannelDynamicFieldValue5);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-5', $departmentChannelDynamicFieldValue5);

        // Value for Mail channel for department two
        $departmentChannelDynamicFieldValue6 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue6->setField($this->getReference("send-to-field-for-mail-channel-named"));
        $departmentChannelDynamicFieldValue6->setEntityValue($this->getReference("address-department-channel-1"));
        $departmentChannelDynamicFieldValue6->setDepartmentChanel($this->getReference("mail-department-channel-for-department-2"));
        $manager->persist($departmentChannelDynamicFieldValue6);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-6', $departmentChannelDynamicFieldValue6);

        // Value for Fax channel for department two
        $departmentChannelDynamicFieldValue7 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue7->setField($this->getReference("send-to-field-for-fax-channel-named"));
        $departmentChannelDynamicFieldValue7->setValue("1234567890");
        $departmentChannelDynamicFieldValue7->setDepartmentChanel($this->getReference("fax-department-channel-for-department-2"));
        $manager->persist($departmentChannelDynamicFieldValue7);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-7', $departmentChannelDynamicFieldValue7);

        // Value for Upload channel for department two
        $departmentChannelDynamicFieldValue8 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue8->setField($this->getReference("send-to-field-for-upload-channel-named"));
        $departmentChannelDynamicFieldValue8->setValue("http://web-2.cc.us/upload2");
        $departmentChannelDynamicFieldValue8->setDepartmentChanel($this->getReference("upload-department-channel-for-department-2"));
        $manager->persist($departmentChannelDynamicFieldValue8);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-8', $departmentChannelDynamicFieldValue8);

        /*********************************************************************/

        // Value for Upload channel for department one
        $departmentChannelDynamicFieldValue9 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue9->setField($this->getReference("send-to-field-for-upload-channel-named"));
        $departmentChannelDynamicFieldValue9->setValue("http://web.cc.us/upload");
        $departmentChannelDynamicFieldValue9->setDepartmentChanel($this->getReference("upload-department-channel-for-department-5"));
        $manager->persist($departmentChannelDynamicFieldValue9);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-9', $departmentChannelDynamicFieldValue9);

        // Value for Email channel for department one
        $departmentChannelDynamicFieldValue10 = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue10->setField($this->getReference("send-to-field-for-email-channel-named"));
        $departmentChannelDynamicFieldValue10->setValue("mail@mail.com");
        $departmentChannelDynamicFieldValue10->setDepartmentChanel($this->getReference("email-department-channel-for-department-6"));
        $manager->persist($departmentChannelDynamicFieldValue10);
        $manager->flush();

        $this->addReference('department-channel-dynamic-field-value-10', $departmentChannelDynamicFieldValue10);
    }

    public function getOrder()
    {
        return 7;
    }
}
