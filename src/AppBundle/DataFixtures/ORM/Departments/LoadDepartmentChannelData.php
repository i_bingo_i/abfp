<?php

namespace AppBundle\DataFixtures\ORM\Departments;

use AppBundle\Entity\DepartmentChannel;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDepartmentChannelData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $emailDepartmentChannelForDepartment1 = new DepartmentChannel();
        $emailDepartmentChannelForDepartment1->setNamed($this->getReference("email-channel-named"));
        $emailDepartmentChannelForDepartment1->setDepartment($this->getReference("department-one"));
        $emailDepartmentChannelForDepartment1->setDevision($this->getReference('device-category-backflow'));

        $manager->persist($emailDepartmentChannelForDepartment1);
        $manager->flush();

        $this->addReference('email-department-channel-for-department-1', $emailDepartmentChannelForDepartment1);


        $mailDepartmentChannelForDepartment1 = new DepartmentChannel();
        $mailDepartmentChannelForDepartment1->setNamed($this->getReference("mail-channel-named"));
        $mailDepartmentChannelForDepartment1->setDepartment($this->getReference("department-one"));
        $mailDepartmentChannelForDepartment1->setDevision($this->getReference("device-category-backflow"));

        $manager->persist($mailDepartmentChannelForDepartment1);
        $manager->flush();

        $this->addReference('mail-department-channel-for-department-1', $mailDepartmentChannelForDepartment1);


        $faxDepartmentChannelForDepartment1 = new DepartmentChannel();
        $faxDepartmentChannelForDepartment1->setNamed($this->getReference("fax-channel-named"));
        $faxDepartmentChannelForDepartment1->setDepartment($this->getReference("department-one"));
        $faxDepartmentChannelForDepartment1->setDevision($this->getReference("device-category-backflow"));

        $manager->persist($faxDepartmentChannelForDepartment1);
        $manager->flush();

        $this->addReference('fax-department-channel-for-department-1', $faxDepartmentChannelForDepartment1);


        $uploadDepartmentChannelForDepartment1 = new DepartmentChannel();
        $uploadDepartmentChannelForDepartment1->setNamed($this->getReference("upload-channel-named"));
        $uploadDepartmentChannelForDepartment1->setDepartment($this->getReference("department-one"));
        $uploadDepartmentChannelForDepartment1->setDevision($this->getReference("device-category-backflow"));
        $uploadDepartmentChannelForDepartment1->setIsDefault(true);
        $uploadDepartmentChannelForDepartment1->setUploadFee("2");
        $uploadDepartmentChannelForDepartment1->setProcessingFee("15");
        $uploadDepartmentChannelForDepartment1->setFeesBasis($this->getReference("fees-basis-per-device"));
        $manager->persist($uploadDepartmentChannelForDepartment1);
        $manager->flush();

        $this->addReference('upload-department-channel-for-department-1', $uploadDepartmentChannelForDepartment1);

        /**************************************************/

        $emailDepartmentChannelForDepartment2 = new DepartmentChannel();
        $emailDepartmentChannelForDepartment2->setNamed($this->getReference("email-channel-named"));
        $emailDepartmentChannelForDepartment2->setDepartment($this->getReference("department-two"));
        $emailDepartmentChannelForDepartment2->setDevision($this->getReference("device-category-fire"));

        $manager->persist($emailDepartmentChannelForDepartment2);
        $manager->flush();

        $this->addReference('email-department-channel-for-department-2', $emailDepartmentChannelForDepartment2);


        $mailDepartmentChannelForDepartment2 = new DepartmentChannel();
        $mailDepartmentChannelForDepartment2->setNamed($this->getReference("mail-channel-named"));
        $mailDepartmentChannelForDepartment2->setDepartment($this->getReference("department-two"));
        $mailDepartmentChannelForDepartment2->setDevision($this->getReference("device-category-fire"));

        $manager->persist($mailDepartmentChannelForDepartment2);
        $manager->flush();

        $this->addReference('mail-department-channel-for-department-2', $mailDepartmentChannelForDepartment2);


        $faxDepartmentChannelForDepartment2 = new DepartmentChannel();
        $faxDepartmentChannelForDepartment2->setNamed($this->getReference("fax-channel-named"));
        $faxDepartmentChannelForDepartment2->setDepartment($this->getReference("department-two"));
        $faxDepartmentChannelForDepartment2->setDevision($this->getReference("device-category-fire"));

        $manager->persist($faxDepartmentChannelForDepartment2);
        $manager->flush();

        $this->addReference('fax-department-channel-for-department-2', $faxDepartmentChannelForDepartment2);


        $uploadDepartmentChannelForDepartment2 = new DepartmentChannel();
        $uploadDepartmentChannelForDepartment2->setNamed($this->getReference("upload-channel-named"));
        $uploadDepartmentChannelForDepartment2->setDepartment($this->getReference("department-two"));
        $uploadDepartmentChannelForDepartment2->setDevision($this->getReference("device-category-fire"));
        $uploadDepartmentChannelForDepartment2->setIsDefault(true);
        $uploadDepartmentChannelForDepartment2->setUploadFee("2");
        $uploadDepartmentChannelForDepartment2->setProcessingFee("15");
        $uploadDepartmentChannelForDepartment2->setFeesBasis($this->getReference("fees-basis-per-device"));
        $manager->persist($uploadDepartmentChannelForDepartment2);
        $manager->flush();

        $this->addReference('upload-department-channel-for-department-2', $uploadDepartmentChannelForDepartment2);

        /**************************************************/

        $uploadDepartmentChannelForDepartment5 = new DepartmentChannel();
        $uploadDepartmentChannelForDepartment5->setNamed($this->getReference("upload-channel-named"));
        $uploadDepartmentChannelForDepartment5->setDepartment($this->getReference("department-five"));
        $uploadDepartmentChannelForDepartment5->setDevision($this->getReference("device-category-backflow"));
        $uploadDepartmentChannelForDepartment5->setIsDefault(true);
        $uploadDepartmentChannelForDepartment5->setUploadFee("3");
        $uploadDepartmentChannelForDepartment5->setProcessingFee("7");
        $uploadDepartmentChannelForDepartment5->setFeesBasis($this->getReference("fees-basis-per-device"));
        $manager->persist($uploadDepartmentChannelForDepartment5);
        $manager->flush();

        $this->addReference('upload-department-channel-for-department-5', $uploadDepartmentChannelForDepartment5);


        $uploadDepartmentChannelForDepartment6 = new DepartmentChannel();
        $uploadDepartmentChannelForDepartment6->setNamed($this->getReference("email-channel-named"));
        $uploadDepartmentChannelForDepartment6->setDepartment($this->getReference("department-six"));
        $uploadDepartmentChannelForDepartment6->setDevision($this->getReference("device-category-fire"));
        $uploadDepartmentChannelForDepartment6->setIsDefault(true);
        $uploadDepartmentChannelForDepartment6->setFeesBasis($this->getReference("fees-basis-per-device"));
        $manager->persist($uploadDepartmentChannelForDepartment6);
        $manager->flush();

        $this->addReference('email-department-channel-for-department-6', $uploadDepartmentChannelForDepartment6);
    }

    public function getOrder()
    {
        return 6;
    }
}
