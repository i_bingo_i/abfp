<?php

namespace AppBundle\DataFixtures\ORM\Departments;

use AppBundle\Entity\ChannelDynamicField;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadChannelDynamicFieldData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $sendToFieldForChannelEmail = new ChannelDynamicField();
        $sendToFieldForChannelEmail->setName("Send to");
        $sendToFieldForChannelEmail->setAlias("send to");
        $sendToFieldForChannelEmail->setChannel($this->getReference("email-channel-named"));
        $sendToFieldForChannelEmail->setType($this->getReference("dynamic-field-type-text"));
        $sendToFieldForChannelEmail->setValidation($this->getReference("validation-app-validation-email"));
        $manager->persist($sendToFieldForChannelEmail);
        $manager->flush();

        $this->addReference('send-to-field-for-email-channel-named', $sendToFieldForChannelEmail);


        $sendToFieldForChannelMail = new ChannelDynamicField();
        $sendToFieldForChannelMail->setName("Send to");
        $sendToFieldForChannelMail->setAlias("send to");
        $sendToFieldForChannelMail->setChannel($this->getReference("mail-channel-named"));
        $sendToFieldForChannelMail->setType($this->getReference("dynamic-field-type-entity"));
        $manager->persist($sendToFieldForChannelMail);
        $manager->flush();

        $this->addReference('send-to-field-for-mail-channel-named', $sendToFieldForChannelMail);


        $sendToFieldForChannelFax = new ChannelDynamicField();
        $sendToFieldForChannelFax->setName("Send to");
        $sendToFieldForChannelFax->setAlias("send to");
        $sendToFieldForChannelFax->setChannel($this->getReference("fax-channel-named"));
        $sendToFieldForChannelFax->setType($this->getReference("dynamic-field-type-text"));
        $sendToFieldForChannelFax->setValidation($this->getReference("validation-app-validation-phone"));
        $manager->persist($sendToFieldForChannelFax);
        $manager->flush();

        $this->addReference('send-to-field-for-fax-channel-named', $sendToFieldForChannelFax);


        $sendToFieldForChannelUpload = new ChannelDynamicField();
        $sendToFieldForChannelUpload->setName("Send to");
        $sendToFieldForChannelUpload->setAlias("send to");
        $sendToFieldForChannelUpload->setChannel($this->getReference("upload-channel-named"));
        $sendToFieldForChannelUpload->setType($this->getReference("dynamic-field-type-text"));
        $sendToFieldForChannelUpload->setValidation($this->getReference("validation-app-validation-required"));
        $manager->persist($sendToFieldForChannelUpload);
        $manager->flush();

        $this->addReference('send-to-field-for-upload-channel-named', $sendToFieldForChannelUpload);
    }

    public function getOrder()
    {
        return 2;
    }
}
