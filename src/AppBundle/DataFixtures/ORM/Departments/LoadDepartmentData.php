<?php

namespace AppBundle\DataFixtures\ORM\Departments;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Department;

class LoadDepartmentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $department1 = new Department();
        $department1->setName('Chicago Water Supply Department');
        $department1->setDivision($this->getReference('device-category-backflow'));
        $department1->setMunicipality($this->getReference('municipality-chicago'));
        $department1->addContact($this->getReference('contact-one'));
        $department1->addContact($this->getReference('contact-jon-snow'));
        $manager->persist($department1);
        $manager->flush();

        $this->addReference('department-one', $department1);

        $department2 = new Department();
        $department2->setName('Chicago Fire Protection Department');
        $department2->setDivision($this->getReference('device-category-fire'));
        $department2->setMunicipality($this->getReference('municipality-chicago'));
        $department2->addContact($this->getReference('contact-two'));
        $department2->addContact($this->getReference('contact-three'));
        $manager->persist($department2);
        $manager->flush();

        $this->addReference('department-two', $department2);


        $department3 = new Department();
        $department3->setName('New York Water Supply Department');
        $department3->setDivision($this->getReference('device-category-backflow'));
        $department3->addContact($this->getReference('contact-16'));
        $department3->addContact($this->getReference('contact-17'));
        $department3->setMunicipality($this->getReference('municipality-new-york'));
        $manager->persist($department3);
        $manager->flush();

        $this->addReference('department-three', $department3);

        $department4 = new Department();
        $department4->setName('New York Fire Protection Department');
        $department4->setDivision($this->getReference('device-category-fire'));
        $department4->addContact($this->getReference('contact-twelve'));
        $department4->addContact($this->getReference('contact-15'));
        $department4->setMunicipality($this->getReference('municipality-new-york'));
        $manager->persist($department4);
        $manager->flush();

        $this->addReference('department-four', $department4);


        $department5 = new Department();
        $department5->setName('Portland Water Supply Department');
        $department5->setDivision($this->getReference('device-category-backflow'));
        $department5->setMunicipality($this->getReference('municipality-portland'));
//        $department5->addContact($this->getReference('contact-one'));
//        $department5->addContact($this->getReference('contact-jon-snow'));
        $manager->persist($department5);
        $manager->flush();

        $this->addReference('department-five', $department5);

        $department6 = new Department();
        $department6->setName('Portland Fire Protection Department');
        $department6->setDivision($this->getReference('device-category-fire'));
        $department6->setMunicipality($this->getReference('municipality-portland'));
//        $department6->addContact($this->getReference('contact-two'));
//        $department6->addContact($this->getReference('contact-three'));
        $manager->persist($department6);
        $manager->flush();

        $this->addReference('department-six', $department6);
    }

    public function getOrder()
    {
        return 5;
    }
}
