<?php

namespace AppBundle\DataFixtures\ORM\Departments;

use AppBundle\Entity\ChannelNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadChannelNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $channelEmail = new ChannelNamed();
        $channelEmail->setName("Email");
        $channelEmail->setAlias("email");
        $manager->persist($channelEmail);
        $manager->flush();

        $this->addReference('email-channel-named', $channelEmail);


        $channelMail = new ChannelNamed();
        $channelMail->setName("Mail");
        $channelMail->setAlias("mail");
        $manager->persist($channelMail);
        $manager->flush();

        $this->addReference('mail-channel-named', $channelMail);


        $channelFax = new ChannelNamed();
        $channelFax->setName("Fax");
        $channelFax->setAlias("fax");
        $manager->persist($channelFax);
        $manager->flush();

        $this->addReference('fax-channel-named', $channelFax);


        $channelUpload = new ChannelNamed();
        $channelUpload->setName("Upload");
        $channelUpload->setAlias("upload");
        $manager->persist($channelUpload);
        $manager->flush();

        $this->addReference('upload-channel-named', $channelUpload);
    }

    public function getOrder()
    {
        return 1;
    }
}
