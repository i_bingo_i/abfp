<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ProcessingStatsType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProcessingStatsTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $processingStatInfo = new ProcessingStatsType();
        $processingStatInfo->setName("Info");
        $processingStatInfo->setAlias("info");
        $manager->persist($processingStatInfo);
        $manager->flush();

        $this->addReference('processing-stat-type-info', $processingStatInfo);


        $processingStatWarning = new ProcessingStatsType();
        $processingStatWarning->setName("Warning");
        $processingStatWarning->setAlias("warning");
        $manager->persist($processingStatWarning);
        $manager->flush();

        $this->addReference('processing-stat-type-warning', $processingStatWarning);


        $processingStatError = new ProcessingStatsType();
        $processingStatError->setName("Error");
        $processingStatError->setAlias("error");
        $manager->persist($processingStatError);
        $manager->flush();

        $this->addReference('processing-stat-type-error', $processingStatError);
    }

    public function getOrder()
    {
        return 1;
    }
}
