<?php

namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\FeesBasis;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFeesBasisData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $feesBasisPerDevice = new FeesBasis();
        $feesBasisPerDevice->setName("per Device");
        $feesBasisPerDevice->setAlias("per device");

        $manager->persist($feesBasisPerDevice);
        $manager->flush();

        $this->addReference('fees-basis-per-device', $feesBasisPerDevice);


        $feesBasisPerSite = new FeesBasis();
        $feesBasisPerSite->setName("per Site");
        $feesBasisPerSite->setAlias("per site");
        $manager->persist($feesBasisPerSite);
        $manager->flush();

        $this->addReference('fees-basis-per-site', $feesBasisPerSite);

    }

    public function getOrder()
    {
        return 1;
    }
}
