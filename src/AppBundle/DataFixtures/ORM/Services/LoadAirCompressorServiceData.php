<?php
namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadAirCompressorServiceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $annualAirCompressorMaintenance */
        $annualAirCompressorMaintenance = new ServiceNamed();
        $annualAirCompressorMaintenance->setName('Annual Air Compressor Maintenance');
        $annualAirCompressorMaintenance->setFrequency($this->getReference('service-frequency-1-year'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('anti-freeze-system'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('section-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('riser-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('pre-action-system-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('deluge-system-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('standpipe-system-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('air-compressor-device'));
        $annualAirCompressorMaintenance->addDeviceNamed($this->getReference('low-point-device'));
        $annualAirCompressorMaintenance->setIsNeedSendMunicipalityReport(true);
        $annualAirCompressorMaintenance->setIsNeedMunicipalityFee(false);
        $manager->persist($annualAirCompressorMaintenance);
        $this->addReference('annual-air-compressor-maintenance', $annualAirCompressorMaintenance);

        /** @var ContractorService $csAnnualAirCompressorMaintenance */
        $csAnnualAirCompressorMaintenance = new ContractorService();
        $csAnnualAirCompressorMaintenance->setNamed($this->getReference('annual-air-compressor-maintenance'));
        $csAnnualAirCompressorMaintenance->setContractor($this->getReference('contractor-my-company'));
        $csAnnualAirCompressorMaintenance->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualAirCompressorMaintenance->setState($this->getReference('state-illinois'));
        $csAnnualAirCompressorMaintenance->setFixedPrice(285.00);
        $csAnnualAirCompressorMaintenance->setHoursPrice(285.00);
        $csAnnualAirCompressorMaintenance->setReference('NFPA 25');
        $csAnnualAirCompressorMaintenance->setEstimationTime('0.5');

        $manager->persist($csAnnualAirCompressorMaintenance);
        $manager->flush();
        $this->addReference('cs-annual-air-compressor-maintenance', $csAnnualAirCompressorMaintenance);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
