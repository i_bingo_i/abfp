<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairRiserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairRiser1 */
        $generalRepairRiser1 = new ServiceNamed();
        $generalRepairRiser1->setName('General repair');
        $generalRepairRiser1->addDeviceNamed($this->getReference('riser-device'));
        $generalRepairRiser1->setDeficiency('Please see comments');
        $generalRepairRiser1->setIsNeedSendMunicipalityReport(false);
        $generalRepairRiser1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairRiser1);
        $this->addReference('general-repair-riser-1', $generalRepairRiser1);

        $contractorGeneralRepairRiser1 = new ContractorService();
        $contractorGeneralRepairRiser1->setNamed($this->getReference('general-repair-riser-1'));
        $contractorGeneralRepairRiser1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairRiser1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairRiser1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairRiser1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairRiser1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-riser-1', $contractorGeneralRepairRiser1);



        /** @var ServiceNamed $generalRepairRiser2 */
        $generalRepairRiser2 = new ServiceNamed();
        $generalRepairRiser2->setName('General repair');
        $generalRepairRiser2->addDeviceNamed($this->getReference('riser-device'));
        $generalRepairRiser2->setDeficiency('Unidentified issue');
        $generalRepairRiser2->setIsNeedSendMunicipalityReport(false);
        $generalRepairRiser2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairRiser2);
        $this->addReference('general-repair-riser-2', $generalRepairRiser2);

        $contractorGeneralRepairRiser2 = new ContractorService();
        $contractorGeneralRepairRiser2->setNamed($this->getReference('general-repair-riser-2'));
        $contractorGeneralRepairRiser2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairRiser2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairRiser2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairRiser2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairRiser2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-riser-2', $contractorGeneralRepairRiser2);



        /** @var ServiceNamed $equipmentInstallationRiser1 */
        $equipmentInstallationRiser1 = new ServiceNamed();
        $equipmentInstallationRiser1->setName('Equipment installation');
        $equipmentInstallationRiser1->addDeviceNamed($this->getReference('riser-device'));
        $equipmentInstallationRiser1->setDeficiency('Please see comments');
        $equipmentInstallationRiser1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationRiser1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationRiser1);
        $this->addReference('equipment-installation-riser-1', $equipmentInstallationRiser1);

        $contractorEquipmentInstallationRiser1 = new ContractorService();
        $contractorEquipmentInstallationRiser1->setNamed($this->getReference('equipment-installation-riser-1'));
        $contractorEquipmentInstallationRiser1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationRiser1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationRiser1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationRiser1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationRiser1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-riser-1', $contractorEquipmentInstallationRiser1);



        /** @var ServiceNamed $inspectionNoticeToBeSentRiser1 */
        $inspectionNoticeToBeSentRiser1 = new ServiceNamed();
        $inspectionNoticeToBeSentRiser1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentRiser1->addDeviceNamed($this->getReference('riser-device'));
        $inspectionNoticeToBeSentRiser1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentRiser1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentRiser1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentRiser1);
        $this->addReference('inspection-notice-to-be-sent-riser-1', $inspectionNoticeToBeSentRiser1);

        $contractorInspectionNoticeToBeSentRiser1 = new ContractorService();
        $contractorInspectionNoticeToBeSentRiser1->setNamed($this->getReference('inspection-notice-to-be-sent-riser-1'));
        $contractorInspectionNoticeToBeSentRiser1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentRiser1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentRiser1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentRiser1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentRiser1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-riser-1', $contractorInspectionNoticeToBeSentRiser1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
