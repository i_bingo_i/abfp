<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairAntiFreezeSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairAntiFreezeSystem1 */
        $generalRepairAntiFreezeSystem1 = new ServiceNamed();
        $generalRepairAntiFreezeSystem1->setName('General repair');
        $generalRepairAntiFreezeSystem1->addDeviceNamed($this->getReference('anti-freeze-system'));
        $generalRepairAntiFreezeSystem1->setDeficiency('Please see comments');
        $generalRepairAntiFreezeSystem1->setIsNeedSendMunicipalityReport(false);
        $generalRepairAntiFreezeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairAntiFreezeSystem1);
        $this->addReference('general-repair-anti-freeze-system-1', $generalRepairAntiFreezeSystem1);

        $contractorGeneralRepairAntiFreezeSystem1 = new ContractorService();
        $contractorGeneralRepairAntiFreezeSystem1->setNamed($this->getReference('general-repair-anti-freeze-system-1'));
        $contractorGeneralRepairAntiFreezeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairAntiFreezeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairAntiFreezeSystem1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairAntiFreezeSystem1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairAntiFreezeSystem1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-anti-freeze-system-1', $contractorGeneralRepairAntiFreezeSystem1);



        /** @var ServiceNamed $generalRepairAntiFreezeSystem2 */
        $generalRepairAntiFreezeSystem2 = new ServiceNamed();
        $generalRepairAntiFreezeSystem2->setName('General repair');
        $generalRepairAntiFreezeSystem2->addDeviceNamed($this->getReference('anti-freeze-system'));
        $generalRepairAntiFreezeSystem2->setDeficiency('Unidentified issue');
        $generalRepairAntiFreezeSystem2->setIsNeedSendMunicipalityReport(false);
        $generalRepairAntiFreezeSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairAntiFreezeSystem2);
        $this->addReference('general-repair-anti-freeze-system-2', $generalRepairAntiFreezeSystem2);

        $contractorGeneralRepairAntiFreezeSystem2 = new ContractorService();
        $contractorGeneralRepairAntiFreezeSystem2->setNamed($this->getReference('general-repair-anti-freeze-system-2'));
        $contractorGeneralRepairAntiFreezeSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairAntiFreezeSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairAntiFreezeSystem2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairAntiFreezeSystem2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairAntiFreezeSystem2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-anti-freeze-system-2', $contractorGeneralRepairAntiFreezeSystem2);



        /** @var ServiceNamed $equipmentInstallationAntiFreezeSystem1 */
        $equipmentInstallationAntiFreezeSystem1 = new ServiceNamed();
        $equipmentInstallationAntiFreezeSystem1->setName('Equipment installation');
        $equipmentInstallationAntiFreezeSystem1->addDeviceNamed($this->getReference('anti-freeze-system'));
        $equipmentInstallationAntiFreezeSystem1->setDeficiency('Please see comments');
        $equipmentInstallationAntiFreezeSystem1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationAntiFreezeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationAntiFreezeSystem1);
        $this->addReference('equipment-installation-anti-freeze-system-1', $equipmentInstallationAntiFreezeSystem1);

        $contractorEquipmentInstallationAntiFreezeSystem1 = new ContractorService();
        $contractorEquipmentInstallationAntiFreezeSystem1->setNamed($this->getReference('equipment-installation-anti-freeze-system-1'));
        $contractorEquipmentInstallationAntiFreezeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationAntiFreezeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationAntiFreezeSystem1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationAntiFreezeSystem1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationAntiFreezeSystem1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-anti-freeze-system-1', $contractorEquipmentInstallationAntiFreezeSystem1);



        /** @var ServiceNamed $inspectionNoticeToBeSentAntiFreezeSystem1 */
        $inspectionNoticeToBeSentAntiFreezeSystem1 = new ServiceNamed();
        $inspectionNoticeToBeSentAntiFreezeSystem1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentAntiFreezeSystem1->addDeviceNamed($this->getReference('anti-freeze-system'));
        $inspectionNoticeToBeSentAntiFreezeSystem1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentAntiFreezeSystem1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentAntiFreezeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentAntiFreezeSystem1);
        $this->addReference('inspection-notice-to-be-sent-anti-freeze-system-1', $inspectionNoticeToBeSentAntiFreezeSystem1);

        $contractorInspectionNoticeToBeSentAntiFreezeSystem1 = new ContractorService();
        $contractorInspectionNoticeToBeSentAntiFreezeSystem1->setNamed($this->getReference('inspection-notice-to-be-sent-anti-freeze-system-1'));
        $contractorInspectionNoticeToBeSentAntiFreezeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentAntiFreezeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentAntiFreezeSystem1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentAntiFreezeSystem1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentAntiFreezeSystem1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-anti-freeze-system-1', $contractorInspectionNoticeToBeSentAntiFreezeSystem1);


        /** @var ServiceNamed $rechargeSystemAntiFreezeSystem1 */
        $rechargeSystemAntiFreezeSystem1 = new ServiceNamed();
        $rechargeSystemAntiFreezeSystem1->setName('Recharge System');
        $rechargeSystemAntiFreezeSystem1->addDeviceNamed($this->getReference('anti-freeze-system'));
        $rechargeSystemAntiFreezeSystem1->setDeficiency('Antifreeze Deteriorated');
        $rechargeSystemAntiFreezeSystem1->setIsNeedSendMunicipalityReport(false);
        $rechargeSystemAntiFreezeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($rechargeSystemAntiFreezeSystem1);
        $this->addReference('recharge-system-anti-freeze-system-1', $rechargeSystemAntiFreezeSystem1);

        $contractorRechargeSystemAntiFreezeSystem1 = new ContractorService();
        $contractorRechargeSystemAntiFreezeSystem1->setNamed($this->getReference('recharge-system-anti-freeze-system-1'));
        $contractorRechargeSystemAntiFreezeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorRechargeSystemAntiFreezeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRechargeSystemAntiFreezeSystem1->setState($this->getReference('state-illinois'));
        $contractorRechargeSystemAntiFreezeSystem1->setEstimationTime('0');

        $manager->persist($contractorRechargeSystemAntiFreezeSystem1);
        $manager->flush();
        $this->addReference('contractorService-recharge-system-anti-freeze-system-1', $contractorRechargeSystemAntiFreezeSystem1);


        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
