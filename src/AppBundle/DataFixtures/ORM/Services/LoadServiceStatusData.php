<?php

namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceStatus;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadServiceStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $underRO = new ServiceStatus();
        $underRO->setName('RETEST OPPORTUNITY');
        $underRO->setAlias('under_retest_opportunity');
        $underRO->setCssClass('app-status-label--yellow');
        $manager->persist($underRO);
        $manager->flush();

        $this->addReference('retest-opportunity-service-status', $underRO);

        $underRN = new ServiceStatus();
        $underRN->setName('UNDER RETEST NOTICE');
        $underRN->setAlias('under_retest_notice');
        $underRN->setCssClass('app-status-label--gray');
        $manager->persist($underRN);
        $manager->flush();

        $this->addReference('under-retest-notice-service-status', $underRN);

        $underWO = new ServiceStatus();
        $underWO->setName('UNDER WORKORDER');
        $underWO->setAlias('under_workorder');
        $underWO->setCssClass('app-status-label--gray');
        $manager->persist($underWO);
        $manager->flush();

        $this->addReference('under-work-order-service-status', $underWO);

        $underDELETED = new ServiceStatus();
        $underDELETED->setName('DELETED');
        $underDELETED->setAlias('deleted');
        $underDELETED->setCssClass('app-status-label--red');
        $manager->persist($underDELETED);
        $manager->flush();

        $this->addReference('retest-opportunity-deleted', $underDELETED);

        $underRepairOpportunity = new ServiceStatus();
        $underRepairOpportunity->setName('REPAIR OPPORTUNITY');
        $underRepairOpportunity->setAlias('repair_opportunity');
        $underRepairOpportunity->setCssClass('app-status-label--yellow');
        $manager->persist($underRepairOpportunity);
        $manager->flush();

        $this->addReference('repair-opportunity-service-status', $underRepairOpportunity);

        $underProposal = new ServiceStatus();
        $underProposal->setName('UNDER PROPOSAL');
        $underProposal->setAlias('under_proposal');
        $underProposal->setCssClass('app-status-label--gray');
        $manager->persist($underProposal);
        $manager->flush();

        $this->addReference('under-proposal-service-status', $underProposal);

        $resolved = new ServiceStatus();
        $resolved->setName('RESOLVED');
        $resolved->setAlias('resolved');
        $resolved->setCssClass('app-status-label--gray');
        $manager->persist($resolved);
        $manager->flush();

        $this->addReference('resolved-service-status', $resolved);

        $canceled = new ServiceStatus();
        $canceled->setName('CANCELED');
        $canceled->setAlias('canceled');
        $canceled->setCssClass('app-status-label--gray');
        $manager->persist($canceled);
        $manager->flush();

        $this->addReference('canceled-service-status', $canceled);

        $discarded = new ServiceStatus();
        $discarded->setName('DISCARDED');
        $discarded->setAlias('discarded');
        $discarded->setCssClass('app-status-label--gray');
        $manager->persist($discarded);
        $manager->flush();

        $this->addReference('discarded-service-status', $discarded);

        $omitted = new ServiceStatus();
        $omitted->setName('OMITTED');
        $omitted->setAlias('omitted');
        $omitted->setCssClass('app-status-label--gray');
        $manager->persist($omitted);
        $manager->flush();

        $this->addReference('omitted-service-status', $omitted);
    }

    public function getOrder()
    {
        return 1;
    }
}
