<?php
namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadRiserServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $fiveYearRiserGaugeReplacement */
        $fiveYearRiserGaugeReplacement = new ServiceNamed();
        $fiveYearRiserGaugeReplacement->setName('5 Year Riser Gauge Replacement');
        $fiveYearRiserGaugeReplacement->setFrequency($this->getReference('service-frequency-5-years'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('anti-freeze-system'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('section-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('riser-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('pre-action-system-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('deluge-system-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('standpipe-system-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('air-compressor-device'));
        $fiveYearRiserGaugeReplacement->addDeviceNamed($this->getReference('low-point-device'));
        $fiveYearRiserGaugeReplacement->setIsNeedSendMunicipalityReport(true);
        $fiveYearRiserGaugeReplacement->setIsNeedMunicipalityFee(false);
        $manager->persist($fiveYearRiserGaugeReplacement);
        $this->addReference('five-year-riser-gauge-replacement', $fiveYearRiserGaugeReplacement);

        /** @var ContractorService $csFiveYearRiserGaugeReplacement */
        $csFiveYearRiserGaugeReplacement = new ContractorService();
        $csFiveYearRiserGaugeReplacement->setNamed($this->getReference('five-year-riser-gauge-replacement'));
        $csFiveYearRiserGaugeReplacement->setContractor($this->getReference('contractor-my-company'));
        $csFiveYearRiserGaugeReplacement->setDeviceCategory($this->getReference('device-category-fire'));
        $csFiveYearRiserGaugeReplacement->setState($this->getReference('state-illinois'));
        $csFiveYearRiserGaugeReplacement->setFixedPrice(50.00);
        $csFiveYearRiserGaugeReplacement->setHoursPrice(50.00);
        $csFiveYearRiserGaugeReplacement->setReference('NFPA 25');
        $csFiveYearRiserGaugeReplacement->setEstimationTime('0.5');

        $manager->persist($csFiveYearRiserGaugeReplacement);
        $manager->flush();
        $this->addReference('cs-five-year-riser-gauge-replacement', $csFiveYearRiserGaugeReplacement);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
