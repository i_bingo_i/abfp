<?php

namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadFirePumpServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Annual Fire Pump Inspection service and Contractor Service
        /** @var ServiceNamed $annualFirePumpInspection */
        $annualFirePumpInspection = new ServiceNamed();
        $annualFirePumpInspection->setName('Annual Fire Pump Inspection');
        $annualFirePumpInspection->setFrequency($this->getReference('service-frequency-1-year'));
        $annualFirePumpInspection->addDeviceNamed($this->getReference('fire-pump-device'));
        $annualFirePumpInspection->setIsNeedSendMunicipalityReport(true);
        $annualFirePumpInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($annualFirePumpInspection);
        $this->addReference('annual-fire-pump-inspection', $annualFirePumpInspection);

        /** @var ContractorService $contractorServiceFirePump_1 */
        $contractorServiceFirePump_1 = new ContractorService();
        $contractorServiceFirePump_1->setNamed($this->getReference('annual-fire-pump-inspection'));
        $contractorServiceFirePump_1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceFirePump_1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceFirePump_1->setState($this->getReference('state-illinois'));
        $contractorServiceFirePump_1->setFixedPrice(600.00);
        $contractorServiceFirePump_1->setHoursPrice(600.00);
        $contractorServiceFirePump_1->setReference('NFPA 20');
        $contractorServiceFirePump_1->setEstimationTime('4');

        $manager->persist($contractorServiceFirePump_1);
        $manager->flush();
        $this->addReference('contractorService-fire-pump-one', $contractorServiceFirePump_1);
        //end Annual Fire Pump Inspection service and Contractor Service

        //Quarterly Fire Pump Inspection service and Contractor Service
        /** @var ServiceNamed $quarterlyFirePumpInspection */
        $quarterlyFirePumpInspection = new ServiceNamed();
        $quarterlyFirePumpInspection->setName('Quarterly Fire Pump Inspection');
        $quarterlyFirePumpInspection->setFrequency($this->getReference('service-frequency-3-months'));
        $quarterlyFirePumpInspection->addDeviceNamed($this->getReference('fire-pump-device'));
        $quarterlyFirePumpInspection->setIsNeedSendMunicipalityReport(true);
        $quarterlyFirePumpInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($quarterlyFirePumpInspection);
        $this->addReference('quarterly-fire-pump-inspection', $quarterlyFirePumpInspection);

        /** @var ContractorService $contractorServiceQuarterlyFirePump_1 */
        $contractorServiceQuarterlyFirePump_1 = new ContractorService();
        $contractorServiceQuarterlyFirePump_1->setNamed($this->getReference('quarterly-fire-pump-inspection'));
        $contractorServiceQuarterlyFirePump_1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceQuarterlyFirePump_1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceQuarterlyFirePump_1->setState($this->getReference('state-illinois'));
        $contractorServiceQuarterlyFirePump_1->setFixedPrice(300.00);
        $contractorServiceQuarterlyFirePump_1->setHoursPrice(300.00);
        $contractorServiceQuarterlyFirePump_1->setReference('NFPA 20');
        $contractorServiceQuarterlyFirePump_1->setEstimationTime('2');

        $manager->persist($contractorServiceQuarterlyFirePump_1);
        $manager->flush();
        $this->addReference('contractorService-quarterly-pump-one', $contractorServiceQuarterlyFirePump_1);
        //end Quarterly Fire Pump Inspection service and Contractor Service

        //Monthly Churn Test service and Contractor Service
        /** @var ServiceNamed $monthlyChurnTest */
        $monthlyChurnTest = new ServiceNamed();
        $monthlyChurnTest->setName('Monthly Churn Test');
        $monthlyChurnTest->setFrequency($this->getReference('service-frequency-1-month'));
        $monthlyChurnTest->addDeviceNamed($this->getReference('fire-pump-device'));
        $monthlyChurnTest->setIsNeedSendMunicipalityReport(true);
        $monthlyChurnTest->setIsNeedMunicipalityFee(true);
        $manager->persist($monthlyChurnTest);
        $this->addReference('monthly-churn-test', $monthlyChurnTest);

        /** @var ContractorService $contractorServiceMonthlyChurnTest_1 */
        $contractorServiceMonthlyChurnTest_1 = new ContractorService();
        $contractorServiceMonthlyChurnTest_1->setNamed($this->getReference('monthly-churn-test'));
        $contractorServiceMonthlyChurnTest_1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceMonthlyChurnTest_1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceMonthlyChurnTest_1->setState($this->getReference('state-illinois'));
        $contractorServiceMonthlyChurnTest_1->setFixedPrice(200.00);
        $contractorServiceMonthlyChurnTest_1->setHoursPrice(200.00);
        $contractorServiceMonthlyChurnTest_1->setReference('NFPA 20');
        $contractorServiceMonthlyChurnTest_1->setEstimationTime('1');

        $manager->persist($contractorServiceMonthlyChurnTest_1);
        $manager->flush();
        $this->addReference('contractorService-monthly-churn-test-one', $contractorServiceMonthlyChurnTest_1);
        //end Monthly Churn Test service and Contractor Service

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
