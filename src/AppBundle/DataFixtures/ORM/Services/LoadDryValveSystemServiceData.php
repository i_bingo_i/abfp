<?php
namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadDryValveSystemServiceData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $annualDryValveSystemPartialTripTest */
        $annualDryValveSystemPartialTripTest = new ServiceNamed();
        $annualDryValveSystemPartialTripTest->setName('Annual Dry Valve System Partial Trip Test');
        $annualDryValveSystemPartialTripTest->setFrequency($this->getReference('service-frequency-3-years'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('anti-freeze-system'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('section-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('riser-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('pre-action-system-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('deluge-system-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('standpipe-system-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('air-compressor-device'));
        $annualDryValveSystemPartialTripTest->addDeviceNamed($this->getReference('low-point-device'));
        $annualDryValveSystemPartialTripTest->setIsNeedSendMunicipalityReport(true);
        $annualDryValveSystemPartialTripTest->setIsNeedMunicipalityFee(true);
        $manager->persist($annualDryValveSystemPartialTripTest);
        $this->addReference('annual-dry-valve-system-partial-trip-test', $annualDryValveSystemPartialTripTest);

        /** @var ContractorService $csAnnualDryValveSystemPartialTripTest */
        $csAnnualDryValveSystemPartialTripTest = new ContractorService();
        $csAnnualDryValveSystemPartialTripTest->setNamed($this->getReference('annual-dry-valve-system-partial-trip-test'));
        $csAnnualDryValveSystemPartialTripTest->setContractor($this->getReference('contractor-my-company'));
        $csAnnualDryValveSystemPartialTripTest->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualDryValveSystemPartialTripTest->setState($this->getReference('state-illinois'));
        $csAnnualDryValveSystemPartialTripTest->setFixedPrice(75.00);
        $csAnnualDryValveSystemPartialTripTest->setHoursPrice(75.00);
        $csAnnualDryValveSystemPartialTripTest->setReference('NFPA 25');
        $csAnnualDryValveSystemPartialTripTest->setEstimationTime('1');

        $manager->persist($csAnnualDryValveSystemPartialTripTest);
        $manager->flush();
        $this->addReference('cs-annual-dry-valve-system-partial-trip-test', $csAnnualDryValveSystemPartialTripTest);

        /** @var ServiceNamed $annualDryValveSystemFullTripTest */
        $annualDryValveSystemFullTripTest = new ServiceNamed();
        $annualDryValveSystemFullTripTest->setName('Annual Dry Valve System Full Trip Test');
        $annualDryValveSystemFullTripTest->setFrequency($this->getReference('service-frequency-3-years'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('anti-freeze-system'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('section-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('riser-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('pre-action-system-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('deluge-system-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('standpipe-system-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('air-compressor-device'));
        $annualDryValveSystemFullTripTest->addDeviceNamed($this->getReference('low-point-device'));
        $annualDryValveSystemFullTripTest->setIsNeedSendMunicipalityReport(true);
        $annualDryValveSystemFullTripTest->setIsNeedMunicipalityFee(true);
        $manager->persist($annualDryValveSystemFullTripTest);
        $this->addReference('annual-dry-valve-system-full-trip-test', $annualDryValveSystemFullTripTest);

        /** @var ContractorService $csAnnualDryValveSystemFullTripTest */
        $csAnnualDryValveSystemFullTripTest = new ContractorService();
        $csAnnualDryValveSystemFullTripTest->setNamed($this->getReference('annual-dry-valve-system-full-trip-test'));
        $csAnnualDryValveSystemFullTripTest->setContractor($this->getReference('contractor-my-company'));
        $csAnnualDryValveSystemFullTripTest->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualDryValveSystemFullTripTest->setState($this->getReference('state-illinois'));
        $csAnnualDryValveSystemFullTripTest->setFixedPrice(150.00);
        $csAnnualDryValveSystemFullTripTest->setHoursPrice(150.00);
        $csAnnualDryValveSystemFullTripTest->setReference('NFPA 25');
        $csAnnualDryValveSystemFullTripTest->setEstimationTime('2');

        $manager->persist($csAnnualDryValveSystemFullTripTest);
        $manager->flush();
        $this->addReference('cs-annual-dry-valve-system-full-trip-test', $csAnnualDryValveSystemFullTripTest);


        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
