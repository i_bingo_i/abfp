<?php
namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadAntiFreezeSystemServiceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $annualAntiFreezeSystemTestAndInspection */
        $annualAntiFreezeSystemTestAndInspection = new ServiceNamed();
        $annualAntiFreezeSystemTestAndInspection->setName('Annual Antifreeze System Test & Inspection');
        $annualAntiFreezeSystemTestAndInspection->setFrequency($this->getReference('service-frequency-1-year'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('anti-freeze-system'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('section-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('riser-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('pre-action-system-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('deluge-system-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('standpipe-system-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('air-compressor-device'));
        $annualAntiFreezeSystemTestAndInspection->addDeviceNamed($this->getReference('low-point-device'));
        $annualAntiFreezeSystemTestAndInspection->setIsNeedSendMunicipalityReport(true);
        $annualAntiFreezeSystemTestAndInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($annualAntiFreezeSystemTestAndInspection);
        $this->addReference('annual-anti-freeze-system-test-and-inspection', $annualAntiFreezeSystemTestAndInspection);

        /** @var ContractorService $csAnnualAntiFreezeSystemTestAndInspection */
        $csAnnualAntiFreezeSystemTestAndInspection = new ContractorService();
        $csAnnualAntiFreezeSystemTestAndInspection->setNamed($this->getReference('annual-anti-freeze-system-test-and-inspection'));
        $csAnnualAntiFreezeSystemTestAndInspection->setContractor($this->getReference('contractor-my-company'));
        $csAnnualAntiFreezeSystemTestAndInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualAntiFreezeSystemTestAndInspection->setState($this->getReference('state-illinois'));
        $csAnnualAntiFreezeSystemTestAndInspection->setFixedPrice(275.00);
        $csAnnualAntiFreezeSystemTestAndInspection->setHoursPrice(275.00);
        $csAnnualAntiFreezeSystemTestAndInspection->setReference('NFPA 25');
        $csAnnualAntiFreezeSystemTestAndInspection->setEstimationTime('1');

        $manager->persist($csAnnualAntiFreezeSystemTestAndInspection);
        $manager->flush();
        $this->addReference('cs-annual-anti-freeze-system-test-and-inspection', $csAnnualAntiFreezeSystemTestAndInspection);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
