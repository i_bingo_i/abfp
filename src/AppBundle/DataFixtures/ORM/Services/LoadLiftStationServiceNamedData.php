<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadLiftStationServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $liftStationDeviceTestOne */
        $liftStationDeviceTestOne = new ServiceNamed();
        $liftStationDeviceTestOne->setName('Test One Lift Station Device');
        $liftStationDeviceTestOne->setFrequency($this->getReference('service-frequency-1-year'));
        $liftStationDeviceTestOne->addDeviceNamed($this->getReference('lift-station-device'));
        $liftStationDeviceTestOne->setIsNeedMunicipalityFee(true);
        $liftStationDeviceTestOne->setIsNeedSendMunicipalityReport(true);
        $manager->persist($liftStationDeviceTestOne);
        $this->addReference('test-one-lift-station-device', $liftStationDeviceTestOne);

        /** @var ContractorService $csLiftStationDeviceTestOne */
        $csLiftStationDeviceTestOne = new ContractorService();
        $csLiftStationDeviceTestOne->setNamed($this->getReference('test-one-lift-station-device'));
        $csLiftStationDeviceTestOne->setContractor($this->getReference('contractor-my-company'));
        $csLiftStationDeviceTestOne->setDeviceCategory($this->getReference('device-category-plumbing'));
        $csLiftStationDeviceTestOne->setState($this->getReference('state-illinois'));
        $csLiftStationDeviceTestOne->setFixedPrice(50.00);
        $csLiftStationDeviceTestOne->setHoursPrice(50.00);
        $csLiftStationDeviceTestOne->setReference('NFPA 25');
        $csLiftStationDeviceTestOne->setEstimationTime('0');

        $manager->persist($csLiftStationDeviceTestOne);
        $manager->flush();
        $this->addReference('cs-test-one-lift-station-device', $csLiftStationDeviceTestOne);

        $manager->flush();


        /** @var ServiceNamed $liftStationDeviceTestTwo */
        $liftStationDeviceTestTwo = new ServiceNamed();
        $liftStationDeviceTestTwo->setName('Test Two Lift Station Device');
        $liftStationDeviceTestTwo->setFrequency($this->getReference('service-frequency-1-year'));
        $liftStationDeviceTestTwo->addDeviceNamed($this->getReference('lift-station-device'));
        $liftStationDeviceTestTwo->setIsNeedMunicipalityFee(true);
        $liftStationDeviceTestTwo->setIsNeedSendMunicipalityReport(true);
        $manager->persist($liftStationDeviceTestTwo);
        $this->addReference('test-two-lift-station-device', $liftStationDeviceTestTwo);

        /** @var ContractorService $csLiftStationDeviceTestTwo */
        $csLiftStationDeviceTestTwo = new ContractorService();
        $csLiftStationDeviceTestTwo->setNamed($this->getReference('test-two-lift-station-device'));
        $csLiftStationDeviceTestTwo->setContractor($this->getReference('contractor-my-company'));
        $csLiftStationDeviceTestTwo->setDeviceCategory($this->getReference('device-category-plumbing'));
        $csLiftStationDeviceTestTwo->setState($this->getReference('state-illinois'));
        $csLiftStationDeviceTestTwo->setFixedPrice(50.00);
        $csLiftStationDeviceTestTwo->setHoursPrice(50.00);
        $csLiftStationDeviceTestTwo->setReference('NFPA 25');
        $csLiftStationDeviceTestTwo->setEstimationTime('0');

        $manager->persist($csLiftStationDeviceTestTwo);
        $manager->flush();
        $this->addReference('cs-test-two-lift-station-device', $csLiftStationDeviceTestTwo);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
