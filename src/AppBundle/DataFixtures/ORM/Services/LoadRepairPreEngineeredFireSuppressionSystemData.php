<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairPreEngineeredFireSuppressionSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairRepairPreEngineeredFireSuppression1 */
        $generalRepairRepairPreEngineeredFireSuppression1 = new ServiceNamed();
        $generalRepairRepairPreEngineeredFireSuppression1->setName('General repair');
        $generalRepairRepairPreEngineeredFireSuppression1->addDeviceNamed($this->getReference('pre_engineered_fire_suppression_system_device'));
        $generalRepairRepairPreEngineeredFireSuppression1->setDeficiency('Please see comments');
        $generalRepairRepairPreEngineeredFireSuppression1->setIsNeedSendMunicipalityReport(false);
        $generalRepairRepairPreEngineeredFireSuppression1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairRepairPreEngineeredFireSuppression1);
        $this->addReference('general-repair-pre-engineered-fire-suppression-1', $generalRepairRepairPreEngineeredFireSuppression1);

        $contractorGeneralRepairRepairPreEngineeredFireSuppression1 = new ContractorService();
        $contractorGeneralRepairRepairPreEngineeredFireSuppression1->setNamed($this->getReference('general-repair-pre-engineered-fire-suppression-1'));
        $contractorGeneralRepairRepairPreEngineeredFireSuppression1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairRepairPreEngineeredFireSuppression1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairRepairPreEngineeredFireSuppression1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairRepairPreEngineeredFireSuppression1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairRepairPreEngineeredFireSuppression1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-pre-engineered-fire-suppression-1', $contractorGeneralRepairRepairPreEngineeredFireSuppression1);



        /** @var ServiceNamed $generalRepairRepairPreEngineeredFireSuppression2 */
        $generalRepairRepairPreEngineeredFireSuppression2 = new ServiceNamed();
        $generalRepairRepairPreEngineeredFireSuppression2->setName('General repair');
        $generalRepairRepairPreEngineeredFireSuppression2->addDeviceNamed($this->getReference('pre_engineered_fire_suppression_system_device'));
        $generalRepairRepairPreEngineeredFireSuppression2->setDeficiency('Unidentified issue');
        $generalRepairRepairPreEngineeredFireSuppression2->setIsNeedSendMunicipalityReport(false);
        $generalRepairRepairPreEngineeredFireSuppression2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairRepairPreEngineeredFireSuppression2);
        $this->addReference('general-repair-pre-engineered-fire-suppression-2', $generalRepairRepairPreEngineeredFireSuppression2);

        $contractorGeneralRepairRepairPreEngineeredFireSuppression2 = new ContractorService();
        $contractorGeneralRepairRepairPreEngineeredFireSuppression2->setNamed($this->getReference('general-repair-pre-engineered-fire-suppression-2'));
        $contractorGeneralRepairRepairPreEngineeredFireSuppression2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairRepairPreEngineeredFireSuppression2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairRepairPreEngineeredFireSuppression2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairRepairPreEngineeredFireSuppression2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairRepairPreEngineeredFireSuppression2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-pre-engineered-fire-suppression-2', $contractorGeneralRepairRepairPreEngineeredFireSuppression2);



        /** @var ServiceNamed $equipmentInstallationPreEngineeredFireSuppression1 */
        $equipmentInstallationPreEngineeredFireSuppression1 = new ServiceNamed();
        $equipmentInstallationPreEngineeredFireSuppression1->setName('Equipment installation');
        $equipmentInstallationPreEngineeredFireSuppression1->addDeviceNamed($this->getReference('pre_engineered_fire_suppression_system_device'));
        $equipmentInstallationPreEngineeredFireSuppression1->setDeficiency('Please see comments');
        $equipmentInstallationPreEngineeredFireSuppression1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationPreEngineeredFireSuppression1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationPreEngineeredFireSuppression1);
        $this->addReference('equipment-installation-pre-engineered-fire-suppression-1', $equipmentInstallationPreEngineeredFireSuppression1);

        $contractorEquipmentInstallationPreEngineeredFireSuppression1 = new ContractorService();
        $contractorEquipmentInstallationPreEngineeredFireSuppression1->setNamed($this->getReference('equipment-installation-pre-engineered-fire-suppression-1'));
        $contractorEquipmentInstallationPreEngineeredFireSuppression1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationPreEngineeredFireSuppression1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationPreEngineeredFireSuppression1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationPreEngineeredFireSuppression1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationPreEngineeredFireSuppression1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-pre-engineered-fire-suppression-1', $contractorEquipmentInstallationPreEngineeredFireSuppression1);



        /** @var ServiceNamed $inspectionNoticeToBeSentPreEngineeredFireSuppression1 */
        $inspectionNoticeToBeSentPreEngineeredFireSuppression1 = new ServiceNamed();
        $inspectionNoticeToBeSentPreEngineeredFireSuppression1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentPreEngineeredFireSuppression1->addDeviceNamed($this->getReference('pre_engineered_fire_suppression_system_device'));
        $inspectionNoticeToBeSentPreEngineeredFireSuppression1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentPreEngineeredFireSuppression1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentPreEngineeredFireSuppression1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentPreEngineeredFireSuppression1);
        $this->addReference('inspection-notice-to-be-sent-pre-engineered-fire-suppression-1', $inspectionNoticeToBeSentPreEngineeredFireSuppression1);

        $contractorInspectionNoticeToBeSentPreEngineeredFireSuppression1 = new ContractorService();
        $contractorInspectionNoticeToBeSentPreEngineeredFireSuppression1->setNamed($this->getReference('inspection-notice-to-be-sent-pre-engineered-fire-suppression-1'));
        $contractorInspectionNoticeToBeSentPreEngineeredFireSuppression1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentPreEngineeredFireSuppression1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentPreEngineeredFireSuppression1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentPreEngineeredFireSuppression1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentPreEngineeredFireSuppression1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-pre-engineered-fire-suppression-1', $contractorInspectionNoticeToBeSentPreEngineeredFireSuppression1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
