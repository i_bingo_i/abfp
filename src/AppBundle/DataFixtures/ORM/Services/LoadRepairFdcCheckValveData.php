<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairFdcCheckValveData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairFdcCheckValve1 */
        $generalRepairFdcCheckValve1 = new ServiceNamed();
        $generalRepairFdcCheckValve1->setName('General repair');
        $generalRepairFdcCheckValve1->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $generalRepairFdcCheckValve1->setDeficiency('Please see comments');
        $generalRepairFdcCheckValve1->setIsNeedSendMunicipalityReport(false);
        $generalRepairFdcCheckValve1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFdcCheckValve1);
        $this->addReference('general-repair-fdc-check-valve-1', $generalRepairFdcCheckValve1);

        $contractorGeneralRepairFdcCheckValve1 = new ContractorService();
        $contractorGeneralRepairFdcCheckValve1->setNamed($this->getReference('general-repair-fdc-check-valve-1'));
        $contractorGeneralRepairFdcCheckValve1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFdcCheckValve1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFdcCheckValve1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFdcCheckValve1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFdcCheckValve1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fdc-check-valve-1', $contractorGeneralRepairFdcCheckValve1);



        /** @var ServiceNamed $generalRepairFdcCheckValve2 */
        $generalRepairFdcCheckValve2 = new ServiceNamed();
        $generalRepairFdcCheckValve2->setName('General repair');
        $generalRepairFdcCheckValve2->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $generalRepairFdcCheckValve2->setDeficiency('Unidentified issue');
        $generalRepairFdcCheckValve2->setIsNeedSendMunicipalityReport(false);
        $generalRepairFdcCheckValve2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFdcCheckValve2);
        $this->addReference('general-repair-fdc-check-valve-2', $generalRepairFdcCheckValve2);

        $contractorGeneralRepairFdcCheckValve2 = new ContractorService();
        $contractorGeneralRepairFdcCheckValve2->setNamed($this->getReference('general-repair-fdc-check-valve-2'));
        $contractorGeneralRepairFdcCheckValve2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFdcCheckValve2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFdcCheckValve2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFdcCheckValve2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFdcCheckValve2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fdc-check-valve-2', $contractorGeneralRepairFdcCheckValve2);


        /** @var ServiceNamed $equipmentInstallationFdcCheckValve1 */
        $equipmentInstallationFdcCheckValve1 = new ServiceNamed();
        $equipmentInstallationFdcCheckValve1->setName('Equipment installation');
        $equipmentInstallationFdcCheckValve1->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $equipmentInstallationFdcCheckValve1->setDeficiency('Please see comments');
        $equipmentInstallationFdcCheckValve1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationFdcCheckValve1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationFdcCheckValve1);
        $this->addReference('equipment-installation-fdc-check-valve-1', $equipmentInstallationFdcCheckValve1);

        $contractorEquipmentInstallationFdcCheckValve1 = new ContractorService();
        $contractorEquipmentInstallationFdcCheckValve1->setNamed($this->getReference('equipment-installation-fdc-check-valve-1'));
        $contractorEquipmentInstallationFdcCheckValve1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationFdcCheckValve1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationFdcCheckValve1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationFdcCheckValve1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationFdcCheckValve1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-fdc-check-valve-1', $contractorEquipmentInstallationFdcCheckValve1);

        /** @var ServiceNamed $inspectionNoticeToBeSentFdcCheckValve1 */
        $inspectionNoticeToBeSentFdcCheckValve1 = new ServiceNamed();
        $inspectionNoticeToBeSentFdcCheckValve1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentFdcCheckValve1->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $inspectionNoticeToBeSentFdcCheckValve1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentFdcCheckValve1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentFdcCheckValve1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentFdcCheckValve1);
        $this->addReference('inspection-notice-to-be-sent-fdc-check-valve-1', $inspectionNoticeToBeSentFdcCheckValve1);

        $contractorInspectionNoticeToBeSentFdcCheckValve1 = new ContractorService();
        $contractorInspectionNoticeToBeSentFdcCheckValve1->setNamed($this->getReference('inspection-notice-to-be-sent-fdc-check-valve-1'));
        $contractorInspectionNoticeToBeSentFdcCheckValve1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentFdcCheckValve1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentFdcCheckValve1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentFdcCheckValve1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentFdcCheckValve1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-fdc-check-valve-1', $contractorInspectionNoticeToBeSentFdcCheckValve1);


        /** @var ServiceNamed $replaceFdcCapsFdcCheckValve1 */
        $replaceFdcCapsFdcCheckValve1 = new ServiceNamed();
        $replaceFdcCapsFdcCheckValve1->setName('Replace FDC caps');
        $replaceFdcCapsFdcCheckValve1->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $replaceFdcCapsFdcCheckValve1->setDeficiency('FDC caps missing/damaged');
        $replaceFdcCapsFdcCheckValve1->setIsNeedSendMunicipalityReport(false);
        $replaceFdcCapsFdcCheckValve1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceFdcCapsFdcCheckValve1);
        $this->addReference('replace-fdc-caps-fdc-check-valve-1', $replaceFdcCapsFdcCheckValve1);

        $contractorReplaceFdcCapsFdcCheckValve1 = new ContractorService();
        $contractorReplaceFdcCapsFdcCheckValve1->setNamed($this->getReference('replace-fdc-caps-fdc-check-valve-1'));
        $contractorReplaceFdcCapsFdcCheckValve1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceFdcCapsFdcCheckValve1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceFdcCapsFdcCheckValve1->setState($this->getReference('state-illinois'));
        $contractorReplaceFdcCapsFdcCheckValve1->setEstimationTime('0');

        $manager->persist($contractorReplaceFdcCapsFdcCheckValve1);
        $manager->flush();
        $this->addReference('contractorService-replace-fdc-caps-fdc-check-valve-1', $contractorReplaceFdcCapsFdcCheckValve1);


        /** @var ServiceNamed $replaceFdcCapsFdcCheckValve2 */
        $replaceFdcCapsFdcCheckValve2 = new ServiceNamed();
        $replaceFdcCapsFdcCheckValve2->setName('Replace FDC caps');
        $replaceFdcCapsFdcCheckValve2->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $replaceFdcCapsFdcCheckValve2->setDeficiency('FDC caps painted shut');
        $replaceFdcCapsFdcCheckValve2->setIsNeedSendMunicipalityReport(false);
        $replaceFdcCapsFdcCheckValve2->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceFdcCapsFdcCheckValve2);
        $this->addReference('replace-fdc-caps-fdc-check-valve-2', $replaceFdcCapsFdcCheckValve2);

        $contractorReplaceFdcCapsFdcCheckValve2 = new ContractorService();
        $contractorReplaceFdcCapsFdcCheckValve2->setNamed($this->getReference('replace-fdc-caps-fdc-check-valve-2'));
        $contractorReplaceFdcCapsFdcCheckValve2->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceFdcCapsFdcCheckValve2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceFdcCapsFdcCheckValve2->setState($this->getReference('state-illinois'));
        $contractorReplaceFdcCapsFdcCheckValve2->setEstimationTime('0');

        $manager->persist($contractorReplaceFdcCapsFdcCheckValve2);
        $manager->flush();
        $this->addReference('contractorService-replace-fdc-caps-fdc-check-valve-2', $contractorReplaceFdcCapsFdcCheckValve2);


        /** @var ServiceNamed $replaceBallDripFdcCheckValve1 */
        $replaceBallDripFdcCheckValve1 = new ServiceNamed();
        $replaceBallDripFdcCheckValve1->setName('Replace ball drip');
        $replaceBallDripFdcCheckValve1->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $replaceBallDripFdcCheckValve1->setDeficiency('Ball drip on FDC leaking');
        $replaceBallDripFdcCheckValve1->setIsNeedSendMunicipalityReport(false);
        $replaceBallDripFdcCheckValve1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceBallDripFdcCheckValve1);
        $this->addReference('replace-ball-drip-fdc-check-valve-1', $replaceBallDripFdcCheckValve1);

        $contractorReplaceBallDripFdcCheckValve1 = new ContractorService();
        $contractorReplaceBallDripFdcCheckValve1->setNamed($this->getReference('replace-ball-drip-fdc-check-valve-1'));
        $contractorReplaceBallDripFdcCheckValve1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceBallDripFdcCheckValve1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceBallDripFdcCheckValve1->setState($this->getReference('state-illinois'));
        $contractorReplaceBallDripFdcCheckValve1->setEstimationTime('0');

        $manager->persist($contractorReplaceBallDripFdcCheckValve1);
        $manager->flush();
        $this->addReference('contractorService-replace-ball-drip-fdc-check-valve-1', $contractorReplaceBallDripFdcCheckValve1);


        /** @var ServiceNamed $installBallDripFdcCheckValve1 */
        $installBallDripFdcCheckValve1 = new ServiceNamed();
        $installBallDripFdcCheckValve1->setName('Install ball drip');
        $installBallDripFdcCheckValve1->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $installBallDripFdcCheckValve1->setDeficiency('Ball drip missing on FDC');
        $installBallDripFdcCheckValve1->setIsNeedSendMunicipalityReport(false);
        $installBallDripFdcCheckValve1->setIsNeedMunicipalityFee(false);
        $manager->persist($installBallDripFdcCheckValve1);
        $this->addReference('install-ball-drip-fdc-check-valve-1', $installBallDripFdcCheckValve1);

        $contractorInstallBallDripFdcCheckValve1 = new ContractorService();
        $contractorInstallBallDripFdcCheckValve1->setNamed($this->getReference('install-ball-drip-fdc-check-valve-1'));
        $contractorInstallBallDripFdcCheckValve1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallBallDripFdcCheckValve1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallBallDripFdcCheckValve1->setState($this->getReference('state-illinois'));
        $contractorInstallBallDripFdcCheckValve1->setEstimationTime('0');

        $manager->persist($contractorInstallBallDripFdcCheckValve1);
        $manager->flush();
        $this->addReference('contractorService-install-ball-drip-fdc-check-valve-1', $contractorInstallBallDripFdcCheckValve1);


        /** @var ServiceNamed $installNewFdcSignFdcCheckValve1 */
        $installNewFdcSignFdcCheckValve1 = new ServiceNamed();
        $installNewFdcSignFdcCheckValve1->setName('Install new FDC sign');
        $installNewFdcSignFdcCheckValve1->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $installNewFdcSignFdcCheckValve1->setDeficiency('FDC sign missing');
        $installNewFdcSignFdcCheckValve1->setIsNeedSendMunicipalityReport(false);
        $installNewFdcSignFdcCheckValve1->setIsNeedMunicipalityFee(false);
        $manager->persist($installNewFdcSignFdcCheckValve1);
        $this->addReference('install-new-fdc-sign-fdc-check-valve-1', $installNewFdcSignFdcCheckValve1);

        $contractorInstallNewFdcSignFdcCheckValve1 = new ContractorService();
        $contractorInstallNewFdcSignFdcCheckValve1->setNamed($this->getReference('install-new-fdc-sign-fdc-check-valve-1'));
        $contractorInstallNewFdcSignFdcCheckValve1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallNewFdcSignFdcCheckValve1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallNewFdcSignFdcCheckValve1->setState($this->getReference('state-illinois'));
        $contractorInstallNewFdcSignFdcCheckValve1->setEstimationTime('0');

        $manager->persist($contractorInstallNewFdcSignFdcCheckValve1);
        $manager->flush();
        $this->addReference('contractorService-install-new-fdc-sign-fdc-check-valve-1', $contractorInstallNewFdcSignFdcCheckValve1);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
