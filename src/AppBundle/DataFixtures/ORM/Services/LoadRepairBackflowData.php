<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairBackflowData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $testRebuildCheckValve */
        $testRebuildCheckValve = new ServiceNamed();
        $testRebuildCheckValve->setName('Rebuild check valve');
        $testRebuildCheckValve->addDeviceNamed($this->getReference('backflow-device'));
        $testRebuildCheckValve->setDeficiency('Check valve #1 is leaking/failing');
        $testRebuildCheckValve->setIsNeedSendMunicipalityReport(false);
        $testRebuildCheckValve->setIsNeedMunicipalityFee(false);
        $manager->persist($testRebuildCheckValve);
        $this->addReference('test-rebuild-check-valve-backflow-device', $testRebuildCheckValve);

        $contractorService100 = new ContractorService();
        $contractorService100->setNamed($this->getReference('test-rebuild-check-valve-backflow-device'));
        $contractorService100->setContractor($this->getReference('contractor-my-company'));
        $contractorService100->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService100->setState($this->getReference('state-illinois'));
        $contractorService100->setEstimationTime('0');

        $manager->persist($contractorService100);
        $manager->flush();
        $this->addReference('contractorService-test-rebuild-check-valve-backflow-device', $contractorService100);




        /** @var ServiceNamed $testRebuildCheckValve2 */
        $testRebuildCheckValve2 = new ServiceNamed();
        $testRebuildCheckValve2->setName('Rebuild check valve');
        $testRebuildCheckValve2->addDeviceNamed($this->getReference('backflow-device'));
        $testRebuildCheckValve2->setDeficiency('Check valve #2 is leaking/failing');
        $testRebuildCheckValve2->setIsNeedSendMunicipalityReport(false);
        $testRebuildCheckValve2->setIsNeedMunicipalityFee(false);
        $manager->persist($testRebuildCheckValve2);
        $this->addReference('test-rebuild-check-valve-2-backflow-device', $testRebuildCheckValve2);

        $contractorService101 = new ContractorService();
        $contractorService101->setNamed($this->getReference('test-rebuild-check-valve-2-backflow-device'));
        $contractorService101->setContractor($this->getReference('contractor-my-company'));
        $contractorService101->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService101->setState($this->getReference('state-illinois'));
        $contractorService101->setEstimationTime('0');

        $manager->persist($contractorService101);
        $manager->flush();
        $this->addReference('contractorService-test-rebuild-check-valve-2-backflow-device', $contractorService101);




        /** @var ServiceNamed $testReplaceShutOffValve */
        $testReplaceShutOffValve = new ServiceNamed();
        $testReplaceShutOffValve->setName('Replace shut-off valve');
        $testReplaceShutOffValve->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceShutOffValve->setDeficiency('Shut-off valve #1 Inoperative');
        $testReplaceShutOffValve->setIsNeedSendMunicipalityReport(false);
        $testReplaceShutOffValve->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceShutOffValve);
        $this->addReference('test-shut-off-valve-backflow-device', $testReplaceShutOffValve);

        $contractorService102 = new ContractorService();
        $contractorService102->setNamed($this->getReference('test-shut-off-valve-backflow-device'));
        $contractorService102->setContractor($this->getReference('contractor-my-company'));
        $contractorService102->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService102->setState($this->getReference('state-illinois'));
        $contractorService102->setEstimationTime('0');

        $manager->persist($contractorService102);
        $manager->flush();
        $this->addReference('contractorService-test-shut-off-valve-backflow-device', $contractorService102);




        /** @var ServiceNamed $testReplaceShutOffValve2 */
        $testReplaceShutOffValve2 = new ServiceNamed();
        $testReplaceShutOffValve2->setName('Replace shut-off valve');
        $testReplaceShutOffValve2->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceShutOffValve2->setDeficiency('Shut-off valve #2 Inoperative');
        $testReplaceShutOffValve2->setIsNeedSendMunicipalityReport(false);
        $testReplaceShutOffValve2->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceShutOffValve2);
        $this->addReference('test-replace-shut-off-valve-2-backflow-device', $testReplaceShutOffValve2);

        $contractorService103 = new ContractorService();
        $contractorService103->setNamed($this->getReference('test-replace-shut-off-valve-2-backflow-device'));
        $contractorService103->setContractor($this->getReference('contractor-my-company'));
        $contractorService103->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService103->setState($this->getReference('state-illinois'));
        $contractorService103->setEstimationTime('0');

        $manager->persist($contractorService103);
        $manager->flush();
        $this->addReference('contractorService-test-replace-shut-off-valve-2-backflow-device', $contractorService103);




        /** @var ServiceNamed $testRepackValvePacking */
        $testRepackValvePacking = new ServiceNamed();
        $testRepackValvePacking->setName('Repack valve packing');
        $testRepackValvePacking->addDeviceNamed($this->getReference('backflow-device'));
        $testRepackValvePacking->setDeficiency('Shut-off valve #1 packing leaking');
        $testRepackValvePacking->setIsNeedSendMunicipalityReport(false);
        $testRepackValvePacking->setIsNeedMunicipalityFee(false);
        $manager->persist($testRepackValvePacking);
        $this->addReference('test-repack-valve-packing-backflow-device', $testRepackValvePacking);

        $contractorService104 = new ContractorService();
        $contractorService104->setNamed($this->getReference('test-repack-valve-packing-backflow-device'));
        $contractorService104->setContractor($this->getReference('contractor-my-company'));
        $contractorService104->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService104->setState($this->getReference('state-illinois'));
        $contractorService104->setEstimationTime('0');

        $manager->persist($contractorService104);
        $manager->flush();
        $this->addReference('contractorService-test-repack-valve-packing-backflow-device', $contractorService104);




        /** @var ServiceNamed $testRepackValvePacking2 */
        $testRepackValvePacking2 = new ServiceNamed();
        $testRepackValvePacking2->setName('Repack valve packing');
        $testRepackValvePacking2->addDeviceNamed($this->getReference('backflow-device'));
        $testRepackValvePacking2->setDeficiency('Shut-off valve #2 packing leaking');
        $testRepackValvePacking2->setIsNeedSendMunicipalityReport(false);
        $testRepackValvePacking2->setIsNeedMunicipalityFee(false);
        $manager->persist($testRepackValvePacking2);
        $this->addReference('test-repack-valve-packing-2-backflow-device', $testRepackValvePacking2);

        $contractorService105 = new ContractorService();
        $contractorService105->setNamed($this->getReference('test-repack-valve-packing-2-backflow-device'));
        $contractorService105->setContractor($this->getReference('contractor-my-company'));
        $contractorService105->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService105->setState($this->getReference('state-illinois'));
        $contractorService105->setEstimationTime('0');

        $manager->persist($contractorService105);
        $manager->flush();
        $this->addReference('contractorService-test-repack-valve-packing-2-backflow-device', $contractorService105);




        /** @var ServiceNamed $testRebuildReliefValve */
        $testRebuildReliefValve = new ServiceNamed();
        $testRebuildReliefValve->setName('Rebuild relief valve');
        $testRebuildReliefValve->addDeviceNamed($this->getReference('backflow-device'));
        $testRebuildReliefValve->setDeficiency('Air inlet is leaking');
        $testRebuildReliefValve->setIsNeedSendMunicipalityReport(false);
        $testRebuildReliefValve->setIsNeedMunicipalityFee(false);
        $manager->persist($testRebuildReliefValve);
        $this->addReference('test-rebuild-relief-valve-backflow-device', $testRebuildReliefValve);

        $contractorService106 = new ContractorService();
        $contractorService106->setNamed($this->getReference('test-rebuild-relief-valve-backflow-device'));
        $contractorService106->setContractor($this->getReference('contractor-my-company'));
        $contractorService106->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService106->setState($this->getReference('state-illinois'));
        $contractorService106->setEstimationTime('0');

        $manager->persist($contractorService106);
        $manager->flush();
        $this->addReference('contractorService-test-rebuild-relief-valve-backflow-device', $contractorService106);




        /** @var ServiceNamed $testUnplugRebuildReliefValve */
        $testUnplugRebuildReliefValve = new ServiceNamed();
        $testUnplugRebuildReliefValve->setName('Unplug/rebuild relief valve');
        $testUnplugRebuildReliefValve->addDeviceNamed($this->getReference('backflow-device'));
        $testUnplugRebuildReliefValve->setDeficiency('Air inlet is plugged');
        $testUnplugRebuildReliefValve->setIsNeedSendMunicipalityReport(false);
        $testUnplugRebuildReliefValve->setIsNeedMunicipalityFee(false);
        $manager->persist($testUnplugRebuildReliefValve);
        $this->addReference('test-unplug-rebuild-relief-valve-backflow-device', $testUnplugRebuildReliefValve);

        $contractorService107 = new ContractorService();
        $contractorService107->setNamed($this->getReference('test-unplug-rebuild-relief-valve-backflow-device'));
        $contractorService107->setContractor($this->getReference('contractor-my-company'));
        $contractorService107->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService107->setState($this->getReference('state-illinois'));
        $contractorService107->setEstimationTime('0');

        $manager->persist($contractorService107);
        $manager->flush();
        $this->addReference('contractorService-test-unplug-rebuild-relief-valve-backflow-device', $contractorService107);




        /** @var ServiceNamed $testTotalHARDPartsRebuild */
        $testTotalHARDPartsRebuild = new ServiceNamed();
        $testTotalHARDPartsRebuild->setName('Total HARD parts rebuild');
        $testTotalHARDPartsRebuild->addDeviceNamed($this->getReference('backflow-device'));
        $testTotalHARDPartsRebuild->setDeficiency('Both check valves and relief valve are leaking/failing');
        $testTotalHARDPartsRebuild->setIsNeedSendMunicipalityReport(false);
        $testTotalHARDPartsRebuild->setIsNeedMunicipalityFee(false);
        $manager->persist($testTotalHARDPartsRebuild);
        $this->addReference('test-total-hard-parts-rebuild-backflow-device', $testTotalHARDPartsRebuild);

        $contractorService108 = new ContractorService();
        $contractorService108->setNamed($this->getReference('test-total-hard-parts-rebuild-backflow-device'));
        $contractorService108->setContractor($this->getReference('contractor-my-company'));
        $contractorService108->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService108->setState($this->getReference('state-illinois'));
        $contractorService108->setEstimationTime('0');

        $manager->persist($contractorService108);
        $manager->flush();
        $this->addReference('contractorService-test-total-hard-parts-rebuild-backflow-device', $contractorService108);




        /** @var ServiceNamed $testTotalSOFTPartsRebuild */
        $testTotalSOFTPartsRebuild = new ServiceNamed();
        $testTotalSOFTPartsRebuild->setName('Total SOFT parts rebuild');
        $testTotalSOFTPartsRebuild->addDeviceNamed($this->getReference('backflow-device'));
        $testTotalSOFTPartsRebuild->setDeficiency('Both check valves and relief valve are leaking/failing');
        $testTotalSOFTPartsRebuild->setIsNeedSendMunicipalityReport(false);
        $testTotalSOFTPartsRebuild->setIsNeedMunicipalityFee(false);
        $manager->persist($testTotalSOFTPartsRebuild);
        $this->addReference('test-total-soft-parts-rebuild-backflow-device', $testTotalSOFTPartsRebuild);

        $contractorService109 = new ContractorService();
        $contractorService109->setNamed($this->getReference('test-total-soft-parts-rebuild-backflow-device'));
        $contractorService109->setContractor($this->getReference('contractor-my-company'));
        $contractorService109->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService109->setState($this->getReference('state-illinois'));
        $contractorService109->setEstimationTime('0');

        $manager->persist($contractorService109);
        $manager->flush();
        $this->addReference('contractorService-test-total-soft-parts-rebuild-backflow-device', $contractorService109);




        /** @var ServiceNamed $testTotalHARDPartsRebuild2 */
        $testTotalHARDPartsRebuild2 = new ServiceNamed();
        $testTotalHARDPartsRebuild2->setName('Total HARD parts rebuild');
        $testTotalHARDPartsRebuild2->addDeviceNamed($this->getReference('backflow-device'));
        $testTotalHARDPartsRebuild2->setDeficiency('Both check valves are leaking/failing');
        $testTotalHARDPartsRebuild2->setIsNeedSendMunicipalityReport(false);
        $testTotalHARDPartsRebuild2->setIsNeedMunicipalityFee(false);
        $manager->persist($testTotalHARDPartsRebuild2);
        $this->addReference('test-total-hard-parts-rebuild-2-backflow-device', $testTotalHARDPartsRebuild2);

        $contractorService110 = new ContractorService();
        $contractorService110->setNamed($this->getReference('test-total-hard-parts-rebuild-2-backflow-device'));
        $contractorService110->setContractor($this->getReference('contractor-my-company'));
        $contractorService110->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService110->setState($this->getReference('state-illinois'));
        $contractorService110->setEstimationTime('0');

        $manager->persist($contractorService110);
        $manager->flush();
        $this->addReference('contractorService-test-total-hard-parts-rebuild-2-backflow-device', $contractorService110);




        /** @var ServiceNamed $testTotalSOFTPartsRebuild2 */
        $testTotalSOFTPartsRebuild2 = new ServiceNamed();
        $testTotalSOFTPartsRebuild2->setName('Total SOFT parts rebuild');
        $testTotalSOFTPartsRebuild2->addDeviceNamed($this->getReference('backflow-device'));
        $testTotalSOFTPartsRebuild2->setDeficiency('Both check valves are leaking/failing');
        $testTotalSOFTPartsRebuild2->setIsNeedSendMunicipalityReport(false);
        $testTotalSOFTPartsRebuild2->setIsNeedMunicipalityFee(false);
        $manager->persist($testTotalSOFTPartsRebuild2);
        $this->addReference('test-total-soft-parts-rebuild-2-backflow-device', $testTotalSOFTPartsRebuild2);

        $contractorService111 = new ContractorService();
        $contractorService111->setNamed($this->getReference('test-total-soft-parts-rebuild-2-backflow-device'));
        $contractorService111->setContractor($this->getReference('contractor-my-company'));
        $contractorService111->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService111->setState($this->getReference('state-illinois'));
        $contractorService111->setEstimationTime('0');

        $manager->persist($contractorService111);
        $manager->flush();
        $this->addReference('contractorService-test-total-soft-parts-rebuild-2-backflow-device', $contractorService111);




        /** @var ServiceNamed $testReplacePoppetAssembly */
        $testReplacePoppetAssembly = new ServiceNamed();
        $testReplacePoppetAssembly->setName('Replace poppet assembly');
        $testReplacePoppetAssembly->addDeviceNamed($this->getReference('backflow-device'));
        $testReplacePoppetAssembly->setDeficiency('Broken #1 poppet assembly');
        $testReplacePoppetAssembly->setIsNeedSendMunicipalityReport(false);
        $testReplacePoppetAssembly->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplacePoppetAssembly);
        $this->addReference('test-replace-poppet-assembly', $testReplacePoppetAssembly);

        $contractorService112 = new ContractorService();
        $contractorService112->setNamed($this->getReference('test-replace-poppet-assembly'));
        $contractorService112->setContractor($this->getReference('contractor-my-company'));
        $contractorService112->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService112->setState($this->getReference('state-illinois'));
        $contractorService112->setEstimationTime('0');

        $manager->persist($contractorService112);
        $manager->flush();
        $this->addReference('contractorService-test-replace-poppet-assembly', $contractorService112);





        /** @var ServiceNamed $testReplacePoppetAssembly2 */
        $testReplacePoppetAssembly2 = new ServiceNamed();
        $testReplacePoppetAssembly2->setName('Replace poppet assembly');
        $testReplacePoppetAssembly2->addDeviceNamed($this->getReference('backflow-device'));
        $testReplacePoppetAssembly2->setDeficiency('Broken #2 poppet assembly');
        $testReplacePoppetAssembly2->setIsNeedSendMunicipalityReport(false);
        $testReplacePoppetAssembly2->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplacePoppetAssembly2);
        $this->addReference('test-replace-poppet-assembly-2', $testReplacePoppetAssembly2);

        $contractorService113 = new ContractorService();
        $contractorService113->setNamed($this->getReference('test-replace-poppet-assembly-2'));
        $contractorService113->setContractor($this->getReference('contractor-my-company'));
        $contractorService113->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService113->setState($this->getReference('state-illinois'));
        $contractorService113->setEstimationTime('0');

        $manager->persist($contractorService113);
        $manager->flush();
        $this->addReference('contractorService-test-replace-poppet-assembly-2', $contractorService113);




        /** @var ServiceNamed $testInstallNewDevice */
        $testInstallNewDevice = new ServiceNamed();
        $testInstallNewDevice->setName('Install new device');
        $testInstallNewDevice->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewDevice->setDeficiency('By-Pass device missing');
        $testInstallNewDevice->setIsNeedSendMunicipalityReport(false);
        $testInstallNewDevice->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewDevice);
        $this->addReference('test-install-new-device', $testInstallNewDevice);

        $contractorService114 = new ContractorService();
        $contractorService114->setNamed($this->getReference('test-install-new-device'));
        $contractorService114->setContractor($this->getReference('contractor-my-company'));
        $contractorService114->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService114->setState($this->getReference('state-illinois'));
        $contractorService114->setEstimationTime('0');

        $manager->persist($contractorService114);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-device', $contractorService114);




        /** @var ServiceNamed $testInstallNewDevice2 */
        $testInstallNewDevice2 = new ServiceNamed();
        $testInstallNewDevice2->setName('Install new device');
        $testInstallNewDevice2->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewDevice2->setDeficiency('By-Pass device frozen/damaged');
        $testInstallNewDevice2->setIsNeedSendMunicipalityReport(false);
        $testInstallNewDevice2->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewDevice2);
        $this->addReference('test-install-new-device2', $testInstallNewDevice2);

        $contractorService115 = new ContractorService();
        $contractorService115->setNamed($this->getReference('test-install-new-device2'));
        $contractorService115->setContractor($this->getReference('contractor-my-company'));
        $contractorService115->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService115->setState($this->getReference('state-illinois'));
        $contractorService115->setEstimationTime('0');

        $manager->persist($contractorService115);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-device2', $contractorService115);




        /** @var ServiceNamed $testInstallNewSensingLine */
        $testInstallNewSensingLine = new ServiceNamed();
        $testInstallNewSensingLine->setName('Install new sensing line');
        $testInstallNewSensingLine->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewSensingLine->setDeficiency('Damaged sensing line');
        $testInstallNewSensingLine->setIsNeedSendMunicipalityReport(false);
        $testInstallNewSensingLine->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewSensingLine);
        $this->addReference('test-intall-new-sensing-line', $testInstallNewSensingLine);

        $contractorService116 = new ContractorService();
        $contractorService116->setNamed($this->getReference('test-intall-new-sensing-line'));
        $contractorService116->setContractor($this->getReference('contractor-my-company'));
        $contractorService116->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService116->setState($this->getReference('state-illinois'));
        $contractorService116->setEstimationTime('0');

        $manager->persist($contractorService116);
        $manager->flush();
        $this->addReference('contractorService-test-intall-new-sensing-line', $contractorService116);





        /** @var ServiceNamed $testInstallNewTestCock1 */
        $testInstallNewTestCock1 = new ServiceNamed();
        $testInstallNewTestCock1->setName('Install new test cock #1');
        $testInstallNewTestCock1->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewTestCock1->setDeficiency('Damaged test cock #1');
        $testInstallNewTestCock1->setIsNeedSendMunicipalityReport(false);
        $testInstallNewTestCock1->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewTestCock1);
        $this->addReference('test-intall-new-test-cock-1', $testInstallNewTestCock1);

        $contractorService117 = new ContractorService();
        $contractorService117->setNamed($this->getReference('test-intall-new-test-cock-1'));
        $contractorService117->setContractor($this->getReference('contractor-my-company'));
        $contractorService117->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService117->setState($this->getReference('state-illinois'));
        $contractorService117->setEstimationTime('0');

        $manager->persist($contractorService117);
        $manager->flush();
        $this->addReference('contractorService-test-intall-new-test-cock-1', $contractorService117);




        /** @var ServiceNamed $testInstallNewTestCock2 */
        $testInstallNewTestCock2 = new ServiceNamed();
        $testInstallNewTestCock2->setName('Install new test cock #2');
        $testInstallNewTestCock2->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewTestCock2->setDeficiency('Damaged test cock #2');
        $testInstallNewTestCock2->setIsNeedSendMunicipalityReport(false);
        $testInstallNewTestCock2->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewTestCock2);
        $this->addReference('test-intall-new-test-cock-2', $testInstallNewTestCock2);

        $contractorService118 = new ContractorService();
        $contractorService118->setNamed($this->getReference('test-intall-new-test-cock-2'));
        $contractorService118->setContractor($this->getReference('contractor-my-company'));
        $contractorService118->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService118->setState($this->getReference('state-illinois'));
        $contractorService118->setEstimationTime('0');

        $manager->persist($contractorService118);
        $manager->flush();
        $this->addReference('contractorService-test-intall-new-test-cock-2', $contractorService118);





        /** @var ServiceNamed $testInstallNewTestCock3 */
        $testInstallNewTestCock3 = new ServiceNamed();
        $testInstallNewTestCock3->setName('Install new test cock #3');
        $testInstallNewTestCock3->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewTestCock3->setDeficiency('Damaged test cock #3');
        $testInstallNewTestCock3->setIsNeedSendMunicipalityReport(false);
        $testInstallNewTestCock3->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewTestCock3);
        $this->addReference('test-intall-new-test-cock-3', $testInstallNewTestCock3);

        $contractorService119 = new ContractorService();
        $contractorService119->setNamed($this->getReference('test-intall-new-test-cock-3'));
        $contractorService119->setContractor($this->getReference('contractor-my-company'));
        $contractorService119->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService119->setState($this->getReference('state-illinois'));
        $contractorService119->setEstimationTime('0');

        $manager->persist($contractorService119);
        $manager->flush();
        $this->addReference('contractorService-test-intall-new-test-cock-3', $contractorService119);





        /** @var ServiceNamed $testInstallNewTestCock4 */
        $testInstallNewTestCock4 = new ServiceNamed();
        $testInstallNewTestCock4->setName('Install new test cock #4');
        $testInstallNewTestCock4->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewTestCock4->setDeficiency('Damaged test cock #4');
        $testInstallNewTestCock4->setIsNeedSendMunicipalityReport(false);
        $testInstallNewTestCock4->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewTestCock4);
        $this->addReference('test-intall-new-test-cock-4', $testInstallNewTestCock4);

        $contractorService120 = new ContractorService();
        $contractorService120->setNamed($this->getReference('test-intall-new-test-cock-4'));
        $contractorService120->setContractor($this->getReference('contractor-my-company'));
        $contractorService120->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService120->setState($this->getReference('state-illinois'));
        $contractorService120->setEstimationTime('0');

        $manager->persist($contractorService120);
        $manager->flush();
        $this->addReference('contractorService-test-intall-new-test-cock-4', $contractorService120);




        /** @var ServiceNamed $testRepairValveBody */
        $testRepairValveBody = new ServiceNamed();
        $testRepairValveBody->setName('Repair valve body');
        $testRepairValveBody->addDeviceNamed($this->getReference('backflow-device'));
        $testRepairValveBody->setDeficiency('Device leaking from valve body');
        $testRepairValveBody->setIsNeedSendMunicipalityReport(false);
        $testRepairValveBody->setIsNeedMunicipalityFee(false);
        $manager->persist($testRepairValveBody);
        $this->addReference('test-repair-valve-body', $testRepairValveBody);

        $contractorService121 = new ContractorService();
        $contractorService121->setNamed($this->getReference('test-repair-valve-body'));
        $contractorService121->setContractor($this->getReference('contractor-my-company'));
        $contractorService121->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService121->setState($this->getReference('state-illinois'));
        $contractorService121->setEstimationTime('0');

        $manager->persist($contractorService121);
        $manager->flush();
        $this->addReference('contractorService-test-repair-valve-body', $contractorService121);




        /** @var ServiceNamed $testRepairValveBody */
        $testReplaceDevice = new ServiceNamed();
        $testReplaceDevice->setName('Replace device');
        $testReplaceDevice->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceDevice->setDeficiency('Device leaking from valve body');
        $testReplaceDevice->setIsNeedSendMunicipalityReport(false);
        $testReplaceDevice->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceDevice);
        $this->addReference('test-replace-device', $testReplaceDevice);

        $contractorService122 = new ContractorService();
        $contractorService122->setNamed($this->getReference('test-replace-device'));
        $contractorService122->setContractor($this->getReference('contractor-my-company'));
        $contractorService122->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService122->setState($this->getReference('state-illinois'));
        $contractorService122->setEstimationTime('0');

        $manager->persist($contractorService122);
        $manager->flush();
        $this->addReference('contractorService-test-replace-device', $contractorService122);




        /** @var ServiceNamed $testInstallNewDevice3 */
        $testInstallNewDevice3 = new ServiceNamed();
        $testInstallNewDevice3->setName('Install new device');
        $testInstallNewDevice3->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewDevice3->setDeficiency('Device Missing');
        $testInstallNewDevice3->setIsNeedSendMunicipalityReport(false);
        $testInstallNewDevice3->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewDevice3);
        $this->addReference('test-install-new-device-3', $testInstallNewDevice3);

        $contractorService123 = new ContractorService();
        $contractorService123->setNamed($this->getReference('test-install-new-device-3'));
        $contractorService123->setContractor($this->getReference('contractor-my-company'));
        $contractorService123->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService123->setState($this->getReference('state-illinois'));
        $contractorService123->setEstimationTime('0');

        $manager->persist($contractorService123);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-device-3', $contractorService123);




        /** @var ServiceNamed $testReplaceORingsWithNew */
        $testReplaceORingsWithNew = new ServiceNamed();
        $testReplaceORingsWithNew->setName('Replace O-Rings with new');
        $testReplaceORingsWithNew->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceORingsWithNew->setDeficiency('Device O-Rings missing or damaged');
        $testReplaceORingsWithNew->setIsNeedSendMunicipalityReport(false);
        $testReplaceORingsWithNew->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceORingsWithNew);
        $this->addReference('test-replace-o-rings-with-new', $testReplaceORingsWithNew);

        $contractorService124 = new ContractorService();
        $contractorService124->setNamed($this->getReference('test-replace-o-rings-with-new'));
        $contractorService124->setContractor($this->getReference('contractor-my-company'));
        $contractorService124->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService124->setState($this->getReference('state-illinois'));
        $contractorService124->setEstimationTime('0');

        $manager->persist($contractorService124);
        $manager->flush();
        $this->addReference('contractorService-test-replace-o-rings-with-new', $contractorService124);




        /** @var ServiceNamed $testReplaceDevice2 */
        $testReplaceDevice2 = new ServiceNamed();
        $testReplaceDevice2->setName('Replace device');
        $testReplaceDevice2->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceDevice2->setDeficiency('Freeze damage');
        $testReplaceDevice2->setIsNeedSendMunicipalityReport(false);
        $testReplaceDevice2->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceDevice2);
        $this->addReference('test-replace-device-2', $testReplaceDevice2);

        $contractorService125 = new ContractorService();
        $contractorService125->setNamed($this->getReference('test-replace-device-2'));
        $contractorService125->setContractor($this->getReference('contractor-my-company'));
        $contractorService125->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService125->setState($this->getReference('state-illinois'));
        $contractorService125->setEstimationTime('0');

        $manager->persist($contractorService125);
        $manager->flush();
        $this->addReference('contractorService-test-replace-device-2', $contractorService125);




        /** @var ServiceNamed $testInstallCorrectly */
        $testInstallCorrectly = new ServiceNamed();
        $testInstallCorrectly->setName('Install correctly');
        $testInstallCorrectly->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallCorrectly->setDeficiency('Improper Installation');
        $testInstallCorrectly->setIsNeedSendMunicipalityReport(false);
        $testInstallCorrectly->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallCorrectly);
        $this->addReference('test-install-correctly', $testInstallCorrectly);


        $contractorService126 = new ContractorService();
        $contractorService126->setNamed($this->getReference('test-install-correctly'));
        $contractorService126->setContractor($this->getReference('contractor-my-company'));
        $contractorService126->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService126->setState($this->getReference('state-illinois'));
        $contractorService126->setEstimationTime('0');

        $manager->persist($contractorService126);
        $manager->flush();
        $this->addReference('contractorService-test-install-correctly', $contractorService126);




        /** @var ServiceNamed $testTotalHARDPartsRebuild3 */
        $testTotalHARDPartsRebuild3 = new ServiceNamed();
        $testTotalHARDPartsRebuild3->setName('Total HARD parts rebuild');
        $testTotalHARDPartsRebuild3->addDeviceNamed($this->getReference('backflow-device'));
        $testTotalHARDPartsRebuild3->setDeficiency('Low test results');
        $testTotalHARDPartsRebuild3->setIsNeedSendMunicipalityReport(false);
        $testTotalHARDPartsRebuild3->setIsNeedMunicipalityFee(false);
        $manager->persist($testTotalHARDPartsRebuild3);
        $this->addReference('test-total-hard-parts-rebuild-3', $testTotalHARDPartsRebuild3);

        $contractorService127 = new ContractorService();
        $contractorService127->setNamed($this->getReference('test-total-hard-parts-rebuild-3'));
        $contractorService127->setContractor($this->getReference('contractor-my-company'));
        $contractorService127->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService127->setState($this->getReference('state-illinois'));
        $contractorService127->setEstimationTime('0');

        $manager->persist($contractorService127);
        $manager->flush();
        $this->addReference('contractorService-test-total-hard-parts-rebuild-3', $contractorService127);



        /** @var ServiceNamed $testTotalSOFTPartsRebuild3 */
        $testTotalSOFTPartsRebuild3 = new ServiceNamed();
        $testTotalSOFTPartsRebuild3->setName('Total SOFT parts rebuild');
        $testTotalSOFTPartsRebuild3->addDeviceNamed($this->getReference('backflow-device'));
        $testTotalSOFTPartsRebuild3->setDeficiency('Low test results');
        $testTotalSOFTPartsRebuild3->setIsNeedSendMunicipalityReport(false);
        $testTotalSOFTPartsRebuild3->setIsNeedMunicipalityFee(false);
        $manager->persist($testTotalSOFTPartsRebuild3);
        $this->addReference('test-total-soft-parts-rebuild-3', $testTotalSOFTPartsRebuild3);

        $contractorService128 = new ContractorService();
        $contractorService128->setNamed($this->getReference('test-total-soft-parts-rebuild-3'));
        $contractorService128->setContractor($this->getReference('contractor-my-company'));
        $contractorService128->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService128->setState($this->getReference('state-illinois'));
        $contractorService128->setEstimationTime('0');

        $manager->persist($contractorService128);
        $manager->flush();
        $this->addReference('contractorService-test-total-soft-parts-rebuild-3', $contractorService128);



        /** @var ServiceNamed $testInstallNewTestCock5 */
        $testInstallNewTestCock5 = new ServiceNamed();
        $testInstallNewTestCock5->setName('Install new test cock #1');
        $testInstallNewTestCock5->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewTestCock5->setDeficiency('Missing test cock #1');
        $testInstallNewTestCock5->setIsNeedSendMunicipalityReport(false);
        $testInstallNewTestCock5->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewTestCock5);
        $this->addReference('test-install-new-test-cock-5', $testInstallNewTestCock5);

        $contractorService129 = new ContractorService();
        $contractorService129->setNamed($this->getReference('test-install-new-test-cock-5'));
        $contractorService129->setContractor($this->getReference('contractor-my-company'));
        $contractorService129->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService129->setState($this->getReference('state-illinois'));
        $contractorService129->setEstimationTime('0');

        $manager->persist($contractorService129);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-test-cock-5', $contractorService129);




        /** @var ServiceNamed $testInstallNewTestCock6 */
        $testInstallNewTestCock6 = new ServiceNamed();
        $testInstallNewTestCock6->setName('Install new test cock #2');
        $testInstallNewTestCock6->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewTestCock6->setDeficiency('Missing test cock #2');
        $testInstallNewTestCock6->setIsNeedSendMunicipalityReport(false);
        $testInstallNewTestCock6->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewTestCock6);
        $this->addReference('test-install-new-test-cock-6', $testInstallNewTestCock6);

        $contractorService130 = new ContractorService();
        $contractorService130->setNamed($this->getReference('test-install-new-test-cock-6'));
        $contractorService130->setContractor($this->getReference('contractor-my-company'));
        $contractorService130->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService130->setState($this->getReference('state-illinois'));
        $contractorService130->setEstimationTime('0');

        $manager->persist($contractorService130);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-test-cock-6', $contractorService130);




        /** @var ServiceNamed $testInstallNewTestCock7 */
        $testInstallNewTestCock7 = new ServiceNamed();
        $testInstallNewTestCock7->setName('Install new test cock #3');
        $testInstallNewTestCock7->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewTestCock7->setDeficiency('Missing test cock #3');
        $testInstallNewTestCock7->setIsNeedSendMunicipalityReport(false);
        $testInstallNewTestCock7->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewTestCock7);
        $this->addReference('test-install-new-test-cock-7', $testInstallNewTestCock7);

        $contractorService131 = new ContractorService();
        $contractorService131->setNamed($this->getReference('test-install-new-test-cock-7'));
        $contractorService131->setContractor($this->getReference('contractor-my-company'));
        $contractorService131->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService131->setState($this->getReference('state-illinois'));
        $contractorService131->setEstimationTime('0');

        $manager->persist($contractorService131);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-test-cock-7', $contractorService131);




        /** @var ServiceNamed $testInstallNewTestCock8 */
        $testInstallNewTestCock8 = new ServiceNamed();
        $testInstallNewTestCock8->setName('Install new test cock #4');
        $testInstallNewTestCock8->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewTestCock8->setDeficiency('Missing test cock #4');
        $testInstallNewTestCock8->setIsNeedSendMunicipalityReport(false);
        $testInstallNewTestCock8->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewTestCock8);
        $this->addReference('test-install-new-test-cock-8', $testInstallNewTestCock8);

        $contractorService132 = new ContractorService();
        $contractorService132->setNamed($this->getReference('test-install-new-test-cock-8'));
        $contractorService132->setContractor($this->getReference('contractor-my-company'));
        $contractorService132->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService132->setState($this->getReference('state-illinois'));
        $contractorService132->setEstimationTime('0');

        $manager->persist($contractorService132);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-test-cock-8', $contractorService132);




        /** @var ServiceNamed $testReplaceSpringAssembly */
        $testReplaceSpringAssembly = new ServiceNamed();
        $testReplaceSpringAssembly->setName('Replace spring assembly');
        $testReplaceSpringAssembly->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceSpringAssembly->setDeficiency('Missing/broken spring on #1 check valve');
        $testReplaceSpringAssembly->setIsNeedSendMunicipalityReport(false);
        $testReplaceSpringAssembly->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceSpringAssembly);
        $this->addReference('test-replace-spring-assembly', $testReplaceSpringAssembly);

        $contractorService133 = new ContractorService();
        $contractorService133->setNamed($this->getReference('test-replace-spring-assembly'));
        $contractorService133->setContractor($this->getReference('contractor-my-company'));
        $contractorService133->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService133->setState($this->getReference('state-illinois'));
        $contractorService133->setEstimationTime('0');

        $manager->persist($contractorService133);
        $manager->flush();
        $this->addReference('contractorService-test-replace-spring-assembly', $contractorService133);




        /** @var ServiceNamed $testReplaceSpringAssembly2 */
        $testReplaceSpringAssembly2 = new ServiceNamed();
        $testReplaceSpringAssembly2->setName('Replace spring assembly');
        $testReplaceSpringAssembly2->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceSpringAssembly2->setDeficiency('Missing/broken spring on #2 check valve');
        $testReplaceSpringAssembly2->setIsNeedSendMunicipalityReport(false);
        $testReplaceSpringAssembly2->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceSpringAssembly2);
        $this->addReference('test-replace-spring-assembly-2', $testReplaceSpringAssembly2);

        $contractorService134 = new ContractorService();
        $contractorService134->setNamed($this->getReference('test-replace-spring-assembly-2'));
        $contractorService134->setContractor($this->getReference('contractor-my-company'));
        $contractorService134->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService134->setState($this->getReference('state-illinois'));
        $contractorService134->setEstimationTime('0');

        $manager->persist($contractorService134);
        $manager->flush();
        $this->addReference('contractorService-test-replace-spring-assembly-2', $contractorService134);




        /** @var ServiceNamed $testReplaceSpringAssembly3 */
        $testReplaceSpringAssembly3 = new ServiceNamed();
        $testReplaceSpringAssembly3->setName('Replace spring assembly');
        $testReplaceSpringAssembly3->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceSpringAssembly3->setDeficiency('Missing/broken spring on relief valve');
        $testReplaceSpringAssembly3->setIsNeedSendMunicipalityReport(false);
        $testReplaceSpringAssembly3->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceSpringAssembly3);
        $this->addReference('test-replace-spring-assembly-3', $testReplaceSpringAssembly3);

        $contractorService135 = new ContractorService();
        $contractorService135->setNamed($this->getReference('test-replace-spring-assembly-3'));
        $contractorService135->setContractor($this->getReference('contractor-my-company'));
        $contractorService135->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService135->setState($this->getReference('state-illinois'));
        $contractorService135->setEstimationTime('0');

        $manager->persist($contractorService135);
        $manager->flush();
        $this->addReference('contractorService-test-replace-spring-assembly-3', $contractorService135);




        /** @var ServiceNamed $testInstallAirGap */
        $testInstallAirGap = new ServiceNamed();
        $testInstallAirGap->setName('Install air-gap');
        $testInstallAirGap->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallAirGap->setDeficiency('No air-gap');
        $testInstallAirGap->setIsNeedSendMunicipalityReport(false);
        $testInstallAirGap->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallAirGap);
        $this->addReference('test-install-air-gap', $testInstallAirGap);

        $contractorService136 = new ContractorService();
        $contractorService136->setNamed($this->getReference('test-install-air-gap'));
        $contractorService136->setContractor($this->getReference('contractor-my-company'));
        $contractorService136->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService136->setState($this->getReference('state-illinois'));
        $contractorService136->setEstimationTime('0');

        $manager->persist($contractorService136);
        $manager->flush();
        $this->addReference('contractorService-test-install-air-gap', $contractorService136);




        /** @var ServiceNamed $testInstallDrainLine */
        $testInstallDrainLine = new ServiceNamed();
        $testInstallDrainLine->setName('Install drain line');
        $testInstallDrainLine->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallDrainLine->setDeficiency('No drain line');
        $testInstallDrainLine->setIsNeedSendMunicipalityReport(false);
        $testInstallDrainLine->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallDrainLine);
        $this->addReference('test-install-drain-line', $testInstallDrainLine);

        $contractorService137 = new ContractorService();
        $contractorService137->setNamed($this->getReference('test-install-drain-line'));
        $contractorService137->setContractor($this->getReference('contractor-my-company'));
        $contractorService137->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService137->setState($this->getReference('state-illinois'));
        $contractorService137->setEstimationTime('0');

        $manager->persist($contractorService137);
        $manager->flush();
        $this->addReference('contractorService-test-install-drain-line', $contractorService137);




        /** @var ServiceNamed $testRodFloorDrainToClearDebris */
        $testRodFloorDrainToClearDebris = new ServiceNamed();
        $testRodFloorDrainToClearDebris->setName('Rod floor drain to clear debris');
        $testRodFloorDrainToClearDebris->addDeviceNamed($this->getReference('backflow-device'));
        $testRodFloorDrainToClearDebris->setDeficiency('Floor drain backing up');
        $testRodFloorDrainToClearDebris->setIsNeedSendMunicipalityReport(false);
        $testRodFloorDrainToClearDebris->setIsNeedMunicipalityFee(false);
        $manager->persist($testRodFloorDrainToClearDebris);
        $this->addReference('test-rod-floor-drain-to-clear-debris', $testRodFloorDrainToClearDebris);

        $contractorService138 = new ContractorService();
        $contractorService138->setNamed($this->getReference('test-rod-floor-drain-to-clear-debris'));
        $contractorService138->setContractor($this->getReference('contractor-my-company'));
        $contractorService138->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService138->setState($this->getReference('state-illinois'));
        $contractorService138->setEstimationTime('0');

        $manager->persist($contractorService138);
        $manager->flush();
        $this->addReference('contractorService-test-rod-floor-drain-to-clear-debris', $contractorService138);




        /** @var ServiceNamed $testAddYStrainer */
        $testAddYStrainer = new ServiceNamed();
        $testAddYStrainer->setName('Add Y-Strainer');
        $testAddYStrainer->addDeviceNamed($this->getReference('backflow-device'));
        $testAddYStrainer->setDeficiency('No Y-Strainer, debris entering device');
        $testAddYStrainer->setIsNeedSendMunicipalityReport(false);
        $testAddYStrainer->setIsNeedMunicipalityFee(false);
        $manager->persist($testAddYStrainer);
        $this->addReference('test-add-y-strainer', $testAddYStrainer);

        $contractorService139 = new ContractorService();
        $contractorService139->setNamed($this->getReference('test-add-y-strainer'));
        $contractorService139->setContractor($this->getReference('contractor-my-company'));
        $contractorService139->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService139->setState($this->getReference('state-illinois'));
        $contractorService139->setEstimationTime('0');

        $manager->persist($contractorService139);
        $manager->flush();
        $this->addReference('contractorService-test-add-y-strainer', $contractorService139);




        /** @var ServiceNamed $testInstallNewSensingLine2 */
        $testInstallNewSensingLine2 = new ServiceNamed();
        $testInstallNewSensingLine2->setName('Install new sensing line');
        $testInstallNewSensingLine2->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewSensingLine2->setDeficiency('Plugged sensing line');
        $testInstallNewSensingLine2->setIsNeedSendMunicipalityReport(false);
        $testInstallNewSensingLine2->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewSensingLine2);
        $this->addReference('test-install-new-sensing-line-2', $testInstallNewSensingLine2);

        $contractorService140 = new ContractorService();
        $contractorService140->setNamed($this->getReference('test-install-new-sensing-line-2'));
        $contractorService140->setContractor($this->getReference('contractor-my-company'));
        $contractorService140->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService140->setState($this->getReference('state-illinois'));
        $contractorService140->setEstimationTime('0');

        $manager->persist($contractorService140);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-sensing-line-2', $contractorService140);



        /** @var ServiceNamed $testRebuildReliefValve2 */
        $testRebuildReliefValve2 = new ServiceNamed();
        $testRebuildReliefValve2->setName('Rebuild relief valve');
        $testRebuildReliefValve2->addDeviceNamed($this->getReference('backflow-device'));
        $testRebuildReliefValve2->setDeficiency('Relief valve is leaking/failing');
        $testRebuildReliefValve2->setIsNeedSendMunicipalityReport(false);
        $testRebuildReliefValve2->setIsNeedMunicipalityFee(false);
        $manager->persist($testRebuildReliefValve2);
        $this->addReference('test-rebuild-relief-valve-2', $testRebuildReliefValve2);

        $contractorService141 = new ContractorService();
        $contractorService141->setNamed($this->getReference('test-rebuild-relief-valve-2'));
        $contractorService141->setContractor($this->getReference('contractor-my-company'));
        $contractorService141->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService141->setState($this->getReference('state-illinois'));
        $contractorService141->setEstimationTime('0');

        $manager->persist($contractorService141);
        $manager->flush();
        $this->addReference('contractorService-test-rebuild-relief-valve-2', $contractorService141);



        /** @var ServiceNamed $testRebuildReliefValve3 */
        $testRebuildReliefValve3 = new ServiceNamed();
        $testRebuildReliefValve3->setName('Rebuild relief valve');
        $testRebuildReliefValve3->addDeviceNamed($this->getReference('backflow-device'));
        $testRebuildReliefValve3->setDeficiency('Relief valve is not opening');
        $testRebuildReliefValve3->setIsNeedSendMunicipalityReport(false);
        $testRebuildReliefValve3->setIsNeedMunicipalityFee(false);
        $manager->persist($testRebuildReliefValve3);
        $this->addReference('test-rebuild-relief-valve-3', $testRebuildReliefValve3);

        $contractorService142 = new ContractorService();
        $contractorService142->setNamed($this->getReference('test-rebuild-relief-valve-3'));
        $contractorService142->setContractor($this->getReference('contractor-my-company'));
        $contractorService142->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService142->setState($this->getReference('state-illinois'));
        $contractorService142->setEstimationTime('0');

        $manager->persist($contractorService142);
        $manager->flush();
        $this->addReference('contractorService-test-rebuild-relief-valve-3', $contractorService142);



        /** @var ServiceNamed $testReplaceWithNew */
        $testReplaceWithNew = new ServiceNamed();
        $testReplaceWithNew->setName('Replace with new');
        $testReplaceWithNew->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceWithNew->setDeficiency('Warped check covers');
        $testReplaceWithNew->setIsNeedSendMunicipalityReport(false);
        $testReplaceWithNew->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceWithNew);
        $this->addReference('test-replace-with-new', $testReplaceWithNew);

        $contractorService143 = new ContractorService();
        $contractorService143->setNamed($this->getReference('test-replace-with-new'));
        $contractorService143->setContractor($this->getReference('contractor-my-company'));
        $contractorService143->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService143->setState($this->getReference('state-illinois'));
        $contractorService143->setEstimationTime('0');

        $manager->persist($contractorService143);
        $manager->flush();
        $this->addReference('contractorService-test-replace-with-new', $contractorService143);



        /** @var ServiceNamed $testReplaceWithNew2 */
        $testReplaceWithNew2 = new ServiceNamed();
        $testReplaceWithNew2->setName('Replace with new');
        $testReplaceWithNew2->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceWithNew2->setDeficiency('Y-Strainer damaged internally');
        $testReplaceWithNew2->setIsNeedSendMunicipalityReport(false);
        $testReplaceWithNew2->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceWithNew2);
        $this->addReference('test-replace-with-new-2', $testReplaceWithNew2);

        $contractorService144 = new ContractorService();
        $contractorService144->setNamed($this->getReference('test-replace-with-new-2'));
        $contractorService144->setContractor($this->getReference('contractor-my-company'));
        $contractorService144->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService144->setState($this->getReference('state-illinois'));
        $contractorService144->setEstimationTime('0');

        $manager->persist($contractorService144);
        $manager->flush();
        $this->addReference('contractorService-test-replace-with-new-2', $contractorService144);




        /** @var ServiceNamed $testReplaceWithNew3 */
        $testReplaceWithNew3 = new ServiceNamed();
        $testReplaceWithNew3->setName('Replace with new');
        $testReplaceWithNew3->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceWithNew3->setDeficiency('Y-Strainer missing screen');
        $testReplaceWithNew3->setIsNeedSendMunicipalityReport(false);
        $testReplaceWithNew3->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceWithNew3);
        $this->addReference('test-replace-with-new-3', $testReplaceWithNew3);

        $contractorService145 = new ContractorService();
        $contractorService145->setNamed($this->getReference('test-replace-with-new-3'));
        $contractorService145->setContractor($this->getReference('contractor-my-company'));
        $contractorService145->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService145->setState($this->getReference('state-illinois'));
        $contractorService145->setEstimationTime('0');

        $manager->persist($contractorService145);
        $manager->flush();
        $this->addReference('contractorService-test-replace-with-new-3', $contractorService145);



        /** @var ServiceNamed $testInstallNewByPassDeviceAndMeter */
        $testInstallNewByPassDeviceAndMeter = new ServiceNamed();
        $testInstallNewByPassDeviceAndMeter->setName('Install new by-pass device and meter');
        $testInstallNewByPassDeviceAndMeter->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewByPassDeviceAndMeter->setDeficiency('By-Pass meter and device have freeze damage');
        $testInstallNewByPassDeviceAndMeter->setIsNeedSendMunicipalityReport(false);
        $testInstallNewByPassDeviceAndMeter->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewByPassDeviceAndMeter);
        $this->addReference('test-install-new-by-pass-device-and-meter', $testInstallNewByPassDeviceAndMeter);

        $contractorService146 = new ContractorService();
        $contractorService146->setNamed($this->getReference('test-install-new-by-pass-device-and-meter'));
        $contractorService146->setContractor($this->getReference('contractor-my-company'));
        $contractorService146->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService146->setState($this->getReference('state-illinois'));
        $contractorService146->setEstimationTime('0');

        $manager->persist($contractorService146);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-by-pass-device-and-meter', $contractorService146);



        /** @var ServiceNamed $testInstallNewMeter */
        $testInstallNewMeter = new ServiceNamed();
        $testInstallNewMeter->setName('Install new meter');
        $testInstallNewMeter->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewMeter->setDeficiency('By-Pass meter has freeze damage');
        $testInstallNewMeter->setIsNeedSendMunicipalityReport(false);
        $testInstallNewMeter->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewMeter);
        $this->addReference('test-install-new-meter', $testInstallNewMeter);

        $contractorService147 = new ContractorService();
        $contractorService147->setNamed($this->getReference('test-install-new-meter'));
        $contractorService147->setContractor($this->getReference('contractor-my-company'));
        $contractorService147->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService147->setState($this->getReference('state-illinois'));
        $contractorService147->setEstimationTime('0');

        $manager->persist($contractorService147);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-meter', $contractorService147);



        /** @var ServiceNamed $testInstallNewApprovedDevice */
        $testInstallNewApprovedDevice = new ServiceNamed();
        $testInstallNewApprovedDevice->setName('Install new approved device');
        $testInstallNewApprovedDevice->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewApprovedDevice->setDeficiency('Repair parts are no longer made');
        $testInstallNewApprovedDevice->setIsNeedSendMunicipalityReport(false);
        $testInstallNewApprovedDevice->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewApprovedDevice);
        $this->addReference('test-install-new-approved-device', $testInstallNewApprovedDevice);

        $contractorService148 = new ContractorService();
        $contractorService148->setNamed($this->getReference('test-install-new-approved-device'));
        $contractorService148->setContractor($this->getReference('contractor-my-company'));
        $contractorService148->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService148->setState($this->getReference('state-illinois'));
        $contractorService148->setEstimationTime('0');

        $manager->persist($contractorService148);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-approved-device', $contractorService148);



        /** @var ServiceNamed $testInstallNewApprovedDevice2 */
        $testInstallNewApprovedDevice2 = new ServiceNamed();
        $testInstallNewApprovedDevice2->setName('Install new approved device');
        $testInstallNewApprovedDevice2->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewApprovedDevice2->setDeficiency('Currently no backflow preventer installed');
        $testInstallNewApprovedDevice2->setIsNeedSendMunicipalityReport(false);
        $testInstallNewApprovedDevice2->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewApprovedDevice2);
        $this->addReference('test-install-new-approved-device-2', $testInstallNewApprovedDevice2);

        $contractorService149 = new ContractorService();
        $contractorService149->setNamed($this->getReference('test-install-new-approved-device-2'));
        $contractorService149->setContractor($this->getReference('contractor-my-company'));
        $contractorService149->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService149->setState($this->getReference('state-illinois'));
        $contractorService149->setEstimationTime('0');

        $manager->persist($contractorService149);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-approved-device-2', $contractorService149);



        /** @var ServiceNamed $testInstallPressureRectifier */
        $testInstallPressureRectifier = new ServiceNamed();
        $testInstallPressureRectifier->setName('Install pressure rectifier');
        $testInstallPressureRectifier->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallPressureRectifier->setDeficiency('Major pressure fluctuation');
        $testInstallPressureRectifier->setIsNeedSendMunicipalityReport(false);
        $testInstallPressureRectifier->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallPressureRectifier);
        $this->addReference('test-install-pressure-rectifier', $testInstallPressureRectifier);

        $contractorService150 = new ContractorService();
        $contractorService150->setNamed($this->getReference('test-install-pressure-rectifier'));
        $contractorService150->setContractor($this->getReference('contractor-my-company'));
        $contractorService150->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService150->setState($this->getReference('state-illinois'));
        $contractorService150->setEstimationTime('0');

        $manager->persist($contractorService150);
        $manager->flush();
        $this->addReference('contractorService-test-install-pressure-rectifier', $contractorService150);



        /** @var ServiceNamed $testReplaceReliefLid */
        $testReplaceReliefLid = new ServiceNamed();
        $testReplaceReliefLid->setName('Replace relief lid');
        $testReplaceReliefLid->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceReliefLid->setDeficiency('Relief lid is pitted and leaking');
        $testReplaceReliefLid->setIsNeedSendMunicipalityReport(false);
        $testReplaceReliefLid->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceReliefLid);
        $this->addReference('test-replace-relief-lid', $testReplaceReliefLid);

        $contractorService151 = new ContractorService();
        $contractorService151->setNamed($this->getReference('test-replace-relief-lid'));
        $contractorService151->setContractor($this->getReference('contractor-my-company'));
        $contractorService151->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService151->setState($this->getReference('state-illinois'));
        $contractorService151->setEstimationTime('0');

        $manager->persist($contractorService151);
        $manager->flush();
        $this->addReference('contractorService-test-replace-relief-lid', $contractorService151);



        /** @var ServiceNamed $testInstallNewApprovedDevice3 */
        $testInstallNewApprovedDevice3 = new ServiceNamed();
        $testInstallNewApprovedDevice3->setName('Install new approved device');
        $testInstallNewApprovedDevice3->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewApprovedDevice3->setDeficiency('Brass seats are pitted and chipped');
        $testInstallNewApprovedDevice3->setIsNeedSendMunicipalityReport(false);
        $testInstallNewApprovedDevice3->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewApprovedDevice3);
        $this->addReference('test-install-new-approved-device-3', $testInstallNewApprovedDevice3);

        $contractorService152 = new ContractorService();
        $contractorService152->setNamed($this->getReference('test-install-new-approved-device-3'));
        $contractorService152->setContractor($this->getReference('contractor-my-company'));
        $contractorService152->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService152->setState($this->getReference('state-illinois'));
        $contractorService152->setEstimationTime('0');

        $manager->persist($contractorService152);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-approved-device-3', $contractorService152);



        /** @var ServiceNamed $testCleanAndServiceYStrainer */
        $testCleanAndServiceYStrainer = new ServiceNamed();
        $testCleanAndServiceYStrainer->setName('Clean and service Y-Strainer');
        $testCleanAndServiceYStrainer->addDeviceNamed($this->getReference('backflow-device'));
        $testCleanAndServiceYStrainer->setDeficiency('Y-strainer is plugged');
        $testCleanAndServiceYStrainer->setIsNeedSendMunicipalityReport(false);
        $testCleanAndServiceYStrainer->setIsNeedMunicipalityFee(false);
        $manager->persist($testCleanAndServiceYStrainer);
        $this->addReference('test-clean-and-service-y-strainer', $testCleanAndServiceYStrainer);

        $contractorService153 = new ContractorService();
        $contractorService153->setNamed($this->getReference('test-clean-and-service-y-strainer'));
        $contractorService153->setContractor($this->getReference('contractor-my-company'));
        $contractorService153->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService153->setState($this->getReference('state-illinois'));
        $contractorService153->setEstimationTime('0');

        $manager->persist($contractorService153);
        $manager->flush();
        $this->addReference('contractorService-test-clean-and-service-y-strainer', $contractorService153);



        /** @var ServiceNamed $testInstallHammerArrestor */
        $testInstallHammerArrestor = new ServiceNamed();
        $testInstallHammerArrestor->setName('Install hammer arrestor');
        $testInstallHammerArrestor->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallHammerArrestor->setDeficiency('Water hammer is damaging device');
        $testInstallHammerArrestor->setIsNeedSendMunicipalityReport(false);
        $testInstallHammerArrestor->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallHammerArrestor);
        $this->addReference('test-install-hammer-arrestor', $testInstallHammerArrestor);

        $contractorService154 = new ContractorService();
        $contractorService154->setNamed($this->getReference('test-install-hammer-arrestor'));
        $contractorService154->setContractor($this->getReference('contractor-my-company'));
        $contractorService154->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService154->setState($this->getReference('state-illinois'));
        $contractorService154->setEstimationTime('0');

        $manager->persist($contractorService154);
        $manager->flush();
        $this->addReference('contractorService-test-install-hammer-arrestor', $contractorService154);



        /** @var ServiceNamed $testTotalHardPartsRebuild4 */
        $testTotalHardPartsRebuild4 = new ServiceNamed();
        $testTotalHardPartsRebuild4->setName('Total HARD parts rebuild');
        $testTotalHardPartsRebuild4->addDeviceNamed($this->getReference('backflow-device'));
        $testTotalHardPartsRebuild4->setDeficiency('Broken hard parts');
        $testTotalHardPartsRebuild4->setIsNeedSendMunicipalityReport(false);
        $testTotalHardPartsRebuild4->setIsNeedMunicipalityFee(false);
        $manager->persist($testTotalHardPartsRebuild4);
        $this->addReference('test-total-hard-parts-rebuild-4', $testTotalHardPartsRebuild4);

        $contractorService155 = new ContractorService();
        $contractorService155->setNamed($this->getReference('test-total-hard-parts-rebuild-4'));
        $contractorService155->setContractor($this->getReference('contractor-my-company'));
        $contractorService155->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService155->setState($this->getReference('state-illinois'));
        $contractorService155->setEstimationTime('0');

        $manager->persist($contractorService155);
        $manager->flush();
        $this->addReference('contractorService-test-total-hard-parts-rebuild-4', $contractorService155);



        /** @var ServiceNamed $testReplaceWithNew4 */
        $testReplaceWithNew4 = new ServiceNamed();
        $testReplaceWithNew4->setName('Replace with new');
        $testReplaceWithNew4->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceWithNew4->setDeficiency('Broken fitting');
        $testReplaceWithNew4->setIsNeedSendMunicipalityReport(false);
        $testReplaceWithNew4->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceWithNew4);
        $this->addReference('test-replace-with-new-4', $testReplaceWithNew4);

        $contractorService156 = new ContractorService();
        $contractorService156->setNamed($this->getReference('test-replace-with-new-4'));
        $contractorService156->setContractor($this->getReference('contractor-my-company'));
        $contractorService156->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService156->setState($this->getReference('state-illinois'));
        $contractorService156->setEstimationTime('0');

        $manager->persist($contractorService156);
        $manager->flush();
        $this->addReference('contractorService-test-replace-with-new-4', $contractorService156);



        /** @var ServiceNamed $testReplaceBulkHeadFittingSpecialOrder */
        $testReplaceBulkHeadFittingSpecialOrder = new ServiceNamed();
        $testReplaceBulkHeadFittingSpecialOrder->setName('Replace bulk head fitting (special order)');
        $testReplaceBulkHeadFittingSpecialOrder->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceBulkHeadFittingSpecialOrder->setDeficiency('Broken bulk head');
        $testReplaceBulkHeadFittingSpecialOrder->setIsNeedSendMunicipalityReport(false);
        $testReplaceBulkHeadFittingSpecialOrder->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceBulkHeadFittingSpecialOrder);
        $this->addReference('test-replace-bulk-head-fitting-special-order', $testReplaceBulkHeadFittingSpecialOrder);

        $contractorService157 = new ContractorService();
        $contractorService157->setNamed($this->getReference('test-replace-bulk-head-fitting-special-order'));
        $contractorService157->setContractor($this->getReference('contractor-my-company'));
        $contractorService157->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService157->setState($this->getReference('state-illinois'));
        $contractorService157->setEstimationTime('0');

        $manager->persist($contractorService157);
        $manager->flush();
        $this->addReference('contractorService-test-replace-bulk-head-fitting-special-order', $contractorService157);



        /** @var ServiceNamed $testRepairAsNeeded */
        $testRepairAsNeeded = new ServiceNamed();
        $testRepairAsNeeded->setName('Repair as needed');
        $testRepairAsNeeded->addDeviceNamed($this->getReference('backflow-device'));
        $testRepairAsNeeded->setDeficiency('Device is leaking and not from relief valve');
        $testRepairAsNeeded->setIsNeedSendMunicipalityReport(false);
        $testRepairAsNeeded->setIsNeedMunicipalityFee(false);
        $manager->persist($testRepairAsNeeded);
        $this->addReference('test-repair-as-needed', $testRepairAsNeeded);

        $contractorService171 = new ContractorService();
        $contractorService171->setNamed($this->getReference('test-repair-as-needed'));
        $contractorService171->setContractor($this->getReference('contractor-my-company'));
        $contractorService171->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService171->setState($this->getReference('state-illinois'));
        $contractorService171->setEstimationTime('0');

        $manager->persist($contractorService171);
        $manager->flush();
        $this->addReference('contractorService-test-repair-as-needed', $contractorService171);



        /** @var ServiceNamed $testRebuildReilefValveHardParts */
        $testRebuildReilefValveHardParts = new ServiceNamed();
        $testRebuildReilefValveHardParts->setName('Rebuild relief valve (hard parts)');
        $testRebuildReilefValveHardParts->addDeviceNamed($this->getReference('backflow-device'));
        $testRebuildReilefValveHardParts->setDeficiency('Relief valve VT broken/warped');
        $testRebuildReilefValveHardParts->setIsNeedSendMunicipalityReport(false);
        $testRebuildReilefValveHardParts->setIsNeedMunicipalityFee(false);
        $manager->persist($testRebuildReilefValveHardParts);
        $this->addReference('test-rebuild-reilef-valve-hard-parts', $testRebuildReilefValveHardParts);

        $contractorService158 = new ContractorService();
        $contractorService158->setNamed($this->getReference('test-rebuild-reilef-valve-hard-parts'));
        $contractorService158->setContractor($this->getReference('contractor-my-company'));
        $contractorService158->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService158->setState($this->getReference('state-illinois'));
        $contractorService158->setEstimationTime('0');

        $manager->persist($contractorService158);
        $manager->flush();
        $this->addReference('contractorService-test-rebuild-reilef-valve-hard-parts', $contractorService158);



        /** @var ServiceNamed $testFileDecommissionReport */
        $testFileDecommissionReport = new ServiceNamed();
        $testFileDecommissionReport->setName('File decommission report');
        $testFileDecommissionReport->addDeviceNamed($this->getReference('backflow-device'));
        $testFileDecommissionReport->setDeficiency('Device no longer in use');
        $testFileDecommissionReport->setIsNeedSendMunicipalityReport(false);
        $testFileDecommissionReport->setIsNeedMunicipalityFee(false);
        $manager->persist($testFileDecommissionReport);
        $this->addReference('test-file-decommission-report', $testFileDecommissionReport);

        $contractorService159 = new ContractorService();
        $contractorService159->setNamed($this->getReference('test-file-decommission-report'));
        $contractorService159->setContractor($this->getReference('contractor-my-company'));
        $contractorService159->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService159->setState($this->getReference('state-illinois'));
        $contractorService159->setEstimationTime('0');

        $manager->persist($contractorService159);
        $manager->flush();
        $this->addReference('contractorService-test-file-decommission-report', $contractorService159);



        /** @var ServiceNamed $testInstallVacuumBreaker */
        $testInstallVacuumBreaker = new ServiceNamed();
        $testInstallVacuumBreaker->setName('Install vacuum breaker');
        $testInstallVacuumBreaker->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallVacuumBreaker->setDeficiency('Site needs a vacuum breaker Installed');
        $testInstallVacuumBreaker->setIsNeedSendMunicipalityReport(false);
        $testInstallVacuumBreaker->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallVacuumBreaker);
        $this->addReference('test-install-vacuum-breaker', $testInstallVacuumBreaker);

        $contractorService160 = new ContractorService();
        $contractorService160->setNamed($this->getReference('test-install-vacuum-breaker'));
        $contractorService160->setContractor($this->getReference('contractor-my-company'));
        $contractorService160->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService160->setState($this->getReference('state-illinois'));
        $contractorService160->setEstimationTime('0');

        $manager->persist($contractorService160);
        $manager->flush();
        $this->addReference('contractorService-test-install-vacuum-breaker', $contractorService160);



        /** @var ServiceNamed $testInstallNewShutOffValve */
        $testInstallNewShutOffValve = new ServiceNamed();
        $testInstallNewShutOffValve->setName('Install new shut off valve');
        $testInstallNewShutOffValve->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewShutOffValve->setDeficiency('Shut off valve #1 missing');
        $testInstallNewShutOffValve->setIsNeedSendMunicipalityReport(false);
        $testInstallNewShutOffValve->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewShutOffValve);
        $this->addReference('test-install-new-shut-off-valve', $testInstallNewShutOffValve);

        $contractorService161 = new ContractorService();
        $contractorService161->setNamed($this->getReference('test-install-new-shut-off-valve'));
        $contractorService161->setContractor($this->getReference('contractor-my-company'));
        $contractorService161->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService161->setState($this->getReference('state-illinois'));
        $contractorService161->setEstimationTime('0');

        $manager->persist($contractorService161);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-shut-off-valve', $contractorService161);



        /** @var ServiceNamed $testInstallNewShutOffValve2 */
        $testInstallNewShutOffValve2 = new ServiceNamed();
        $testInstallNewShutOffValve2->setName('Install new shut off valve');
        $testInstallNewShutOffValve2->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewShutOffValve2->setDeficiency('Shut off valve #2 missing');
        $testInstallNewShutOffValve2->setIsNeedSendMunicipalityReport(false);
        $testInstallNewShutOffValve2->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewShutOffValve2);
        $this->addReference('test-install-new-shut-off-valve-2', $testInstallNewShutOffValve2);

        $contractorService162 = new ContractorService();
        $contractorService162->setNamed($this->getReference('test-install-new-shut-off-valve-2'));
        $contractorService162->setContractor($this->getReference('contractor-my-company'));
        $contractorService162->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService162->setState($this->getReference('state-illinois'));
        $contractorService162->setEstimationTime('0');

        $manager->persist($contractorService162);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-shut-off-valve-2', $contractorService162);



        /** @var ServiceNamed $testCleanInterValveBodyPorts */
        $testCleanInterValveBodyPorts = new ServiceNamed();
        $testCleanInterValveBodyPorts->setName('Clean inter valve body ports');
        $testCleanInterValveBodyPorts->addDeviceNamed($this->getReference('backflow-device'));
        $testCleanInterValveBodyPorts->setDeficiency('Supply line plugged');
        $testCleanInterValveBodyPorts->setIsNeedSendMunicipalityReport(false);
        $testCleanInterValveBodyPorts->setIsNeedMunicipalityFee(false);
        $manager->persist($testCleanInterValveBodyPorts);
        $this->addReference('test-clean-inter-valve-body-ports', $testCleanInterValveBodyPorts);

        $contractorService163 = new ContractorService();
        $contractorService163->setNamed($this->getReference('test-clean-inter-valve-body-ports'));
        $contractorService163->setContractor($this->getReference('contractor-my-company'));
        $contractorService163->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService163->setState($this->getReference('state-illinois'));
        $contractorService163->setEstimationTime('0');

        $manager->persist($contractorService163);
        $manager->flush();
        $this->addReference('contractorService-test-clean-inter-valve-body-ports', $contractorService163);



        /** @var ServiceNamed $testSoftPartsRebuildAndValveBodyCleaning */
        $testSoftPartsRebuildAndValveBodyCleaning = new ServiceNamed();
        $testSoftPartsRebuildAndValveBodyCleaning->setName('Soft parts rebuild and valve body cleaning');
        $testSoftPartsRebuildAndValveBodyCleaning->addDeviceNamed($this->getReference('backflow-device'));
        $testSoftPartsRebuildAndValveBodyCleaning->setDeficiency('Hydraulic moan coming from device');
        $testSoftPartsRebuildAndValveBodyCleaning->setIsNeedSendMunicipalityReport(false);
        $testSoftPartsRebuildAndValveBodyCleaning->setIsNeedMunicipalityFee(false);
        $manager->persist($testSoftPartsRebuildAndValveBodyCleaning);
        $this->addReference('test-soft-parts-rebuild-and-valve-body-cleaning', $testSoftPartsRebuildAndValveBodyCleaning);

        $contractorService164 = new ContractorService();
        $contractorService164->setNamed($this->getReference('test-soft-parts-rebuild-and-valve-body-cleaning'));
        $contractorService164->setContractor($this->getReference('contractor-my-company'));
        $contractorService164->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService164->setState($this->getReference('state-illinois'));
        $contractorService164->setEstimationTime('0');

        $manager->persist($contractorService164);
        $manager->flush();
        $this->addReference('contractorService-test-soft-parts-rebuild-and-valve-body-cleaning', $contractorService164);



        /** @var ServiceNamed $testReplaceRebuildCheckValve */
        $testReplaceRebuildCheckValve = new ServiceNamed();
        $testReplaceRebuildCheckValve->setName('Replace/rebuild check valve');
        $testReplaceRebuildCheckValve->addDeviceNamed($this->getReference('backflow-device'));
        $testReplaceRebuildCheckValve->setDeficiency('Booster pump check failing');
        $testReplaceRebuildCheckValve->setIsNeedSendMunicipalityReport(false);
        $testReplaceRebuildCheckValve->setIsNeedMunicipalityFee(false);
        $manager->persist($testReplaceRebuildCheckValve);
        $this->addReference('test-replace-rebuild-check-valve', $testReplaceRebuildCheckValve);

        $contractorService165 = new ContractorService();
        $contractorService165->setNamed($this->getReference('test-replace-rebuild-check-valve'));
        $contractorService165->setContractor($this->getReference('contractor-my-company'));
        $contractorService165->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService165->setState($this->getReference('state-illinois'));
        $contractorService165->setEstimationTime('0');

        $manager->persist($contractorService165);
        $manager->flush();
        $this->addReference('contractorService-test-replace-rebuild-check-valve', $contractorService165);



        /** @var ServiceNamed $testRepairLeakingPipeUpstream */
        $testRepairLeakingPipeUpstream = new ServiceNamed();
        $testRepairLeakingPipeUpstream->setName('Repair leaking pipe upstream');
        $testRepairLeakingPipeUpstream->addDeviceNamed($this->getReference('backflow-device'));
        $testRepairLeakingPipeUpstream->setDeficiency('Main supply leaking');
        $testRepairLeakingPipeUpstream->setIsNeedSendMunicipalityReport(false);
        $testRepairLeakingPipeUpstream->setIsNeedMunicipalityFee(false);
        $manager->persist($testRepairLeakingPipeUpstream);
        $this->addReference('test-repair-leaking-pipe-upstream', $testRepairLeakingPipeUpstream);

        $contractorService166 = new ContractorService();
        $contractorService166->setNamed($this->getReference('test-repair-leaking-pipe-upstream'));
        $contractorService166->setContractor($this->getReference('contractor-my-company'));
        $contractorService166->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService166->setState($this->getReference('state-illinois'));
        $contractorService166->setEstimationTime('0');

        $manager->persist($contractorService166);
        $manager->flush();
        $this->addReference('contractorService-test-repair-leaking-pipe-upstream', $contractorService166);



        /** @var ServiceNamed $testRepairLeakingPipeDownstream */
        $testRepairLeakingPipeDownstream = new ServiceNamed();
        $testRepairLeakingPipeDownstream->setName('Repair leaking pipe downstream');
        $testRepairLeakingPipeDownstream->addDeviceNamed($this->getReference('backflow-device'));
        $testRepairLeakingPipeDownstream->setDeficiency('Piping downstream of backflow leaking');
        $testRepairLeakingPipeDownstream->setIsNeedSendMunicipalityReport(false);
        $testRepairLeakingPipeDownstream->setIsNeedMunicipalityFee(false);
        $manager->persist($testRepairLeakingPipeDownstream);
        $this->addReference('test-repair-leaking-pipe-downstream', $testRepairLeakingPipeDownstream);

        $contractorService167 = new ContractorService();
        $contractorService167->setNamed($this->getReference('test-repair-leaking-pipe-downstream'));
        $contractorService167->setContractor($this->getReference('contractor-my-company'));
        $contractorService167->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService167->setState($this->getReference('state-illinois'));
        $contractorService167->setEstimationTime('0');

        $manager->persist($contractorService167);
        $manager->flush();
        $this->addReference('contractorService-test-repair-leaking-pipe-downstream', $contractorService167);



        /** @var ServiceNamed $testInstallNewApprovedDevice4 */
        $testInstallNewApprovedDevice4 = new ServiceNamed();
        $testInstallNewApprovedDevice4->setName('Install new approved device');
        $testInstallNewApprovedDevice4->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewApprovedDevice4->setDeficiency('Obsolete device');
        $testInstallNewApprovedDevice4->setIsNeedSendMunicipalityReport(false);
        $testInstallNewApprovedDevice4->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewApprovedDevice4);
        $this->addReference('test-install-new-approved-device-4', $testInstallNewApprovedDevice4);

        $contractorService168 = new ContractorService();
        $contractorService168->setNamed($this->getReference('test-install-new-approved-device-4'));
        $contractorService168->setContractor($this->getReference('contractor-my-company'));
        $contractorService168->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService168->setState($this->getReference('state-illinois'));
        $contractorService168->setEstimationTime('0');

        $manager->persist($contractorService168);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-approved-device-4', $contractorService168);



        /** @var ServiceNamed $testInstallNewApprovedDevice5 */
        $testInstallNewApprovedDevice5 = new ServiceNamed();
        $testInstallNewApprovedDevice5->setName('Install new approved device');
        $testInstallNewApprovedDevice5->addDeviceNamed($this->getReference('backflow-device'));
        $testInstallNewApprovedDevice5->setDeficiency('Device is not repairable');
        $testInstallNewApprovedDevice5->setIsNeedSendMunicipalityReport(false);
        $testInstallNewApprovedDevice5->setIsNeedMunicipalityFee(false);
        $manager->persist($testInstallNewApprovedDevice5);
        $this->addReference('test-install-new-approved-device-5', $testInstallNewApprovedDevice5);

        $contractorService169 = new ContractorService();
        $contractorService169->setNamed($this->getReference('test-install-new-approved-device-5'));
        $contractorService169->setContractor($this->getReference('contractor-my-company'));
        $contractorService169->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService169->setState($this->getReference('state-illinois'));
        $contractorService169->setEstimationTime('0');

        $manager->persist($contractorService169);
        $manager->flush();
        $this->addReference('contractorService-test-install-new-approved-device-5', $contractorService169);



        /** @var ServiceNamed $postRepairBackflowInspection */
        $postRepairBackflowInspection = new ServiceNamed();
        $postRepairBackflowInspection->setName('Post-repair Backflow Inspection');
        $postRepairBackflowInspection->addDeviceNamed($this->getReference('backflow-device'));
        $postRepairBackflowInspection->setDeficiency('Device requires post-repair test');
        $postRepairBackflowInspection->setIsNeedSendMunicipalityReport(true);
        $postRepairBackflowInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($postRepairBackflowInspection);
        $this->addReference('post-repair-backflow-inspection', $postRepairBackflowInspection);

        $contractorService170 = new ContractorService();
        $contractorService170->setNamed($this->getReference('post-repair-backflow-inspection'));
        $contractorService170->setContractor($this->getReference('contractor-my-company'));
        $contractorService170->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorService170->setState($this->getReference('state-illinois'));
        $contractorService170->setEstimationTime('0');

        $manager->persist($contractorService170);
        $manager->flush();
        $this->addReference('contractorService-post-repair-backflow-inspection', $contractorService170);



        /** @var ServiceNamed $testGeneralRepair1 */
        $testGeneralRepair1 = new ServiceNamed();
        $testGeneralRepair1->setName('General repair');
        $testGeneralRepair1->addDeviceNamed($this->getReference('backflow-device'));
        $testGeneralRepair1->setDeficiency('Please see comments');
        $testGeneralRepair1->setIsNeedSendMunicipalityReport(false);
        $testGeneralRepair1->setIsNeedMunicipalityFee(false);
        $manager->persist($testGeneralRepair1);
        $this->addReference('test-general-repair-1', $testGeneralRepair1);

        $contractorServiceTestGeneralRepair1 = new ContractorService();
        $contractorServiceTestGeneralRepair1->setNamed($this->getReference('test-general-repair-1'));
        $contractorServiceTestGeneralRepair1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceTestGeneralRepair1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceTestGeneralRepair1->setState($this->getReference('state-illinois'));
        $contractorServiceTestGeneralRepair1->setEstimationTime('0');

        $manager->persist($contractorServiceTestGeneralRepair1);
        $manager->flush();
        $this->addReference('contractorService-test-general-repair-1', $contractorServiceTestGeneralRepair1);



        /** @var ServiceNamed $testGeneralRepair2 */
        $testGeneralRepair2 = new ServiceNamed();
        $testGeneralRepair2->setName('General repair');
        $testGeneralRepair2->addDeviceNamed($this->getReference('backflow-device'));
        $testGeneralRepair2->setDeficiency('Unidentified issue');
        $testGeneralRepair2->setIsNeedSendMunicipalityReport(false);
        $testGeneralRepair2->setIsNeedMunicipalityFee(false);
        $manager->persist($testGeneralRepair2);
        $this->addReference('test-general-repair-2', $testGeneralRepair2);

        $contractorServiceTestGeneralRepair2 = new ContractorService();
        $contractorServiceTestGeneralRepair2->setNamed($this->getReference('test-general-repair-2'));
        $contractorServiceTestGeneralRepair2->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceTestGeneralRepair2->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceTestGeneralRepair2->setState($this->getReference('state-illinois'));
        $contractorServiceTestGeneralRepair2->setEstimationTime('0');

        $manager->persist($contractorServiceTestGeneralRepair2);
        $manager->flush();
        $this->addReference('contractorService-test-general-repair-2', $contractorServiceTestGeneralRepair2);



        /** @var ServiceNamed $testEquipmentInstallation1 */
        $testEquipmentInstallation1 = new ServiceNamed();
        $testEquipmentInstallation1->setName('Equipment installation');
        $testEquipmentInstallation1->addDeviceNamed($this->getReference('backflow-device'));
        $testEquipmentInstallation1->setDeficiency('Please see comments');
        $testEquipmentInstallation1->setIsNeedSendMunicipalityReport(false);
        $testEquipmentInstallation1->setIsNeedMunicipalityFee(false);
        $manager->persist($testEquipmentInstallation1);
        $this->addReference('test-equipment-installation-1', $testEquipmentInstallation1);

        $contractorServiceTestEquipmentInstallation1 = new ContractorService();
        $contractorServiceTestEquipmentInstallation1->setNamed($this->getReference('test-equipment-installation-1'));
        $contractorServiceTestEquipmentInstallation1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceTestEquipmentInstallation1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceTestEquipmentInstallation1->setState($this->getReference('state-illinois'));
        $contractorServiceTestEquipmentInstallation1->setEstimationTime('0');

        $manager->persist($contractorServiceTestEquipmentInstallation1);
        $manager->flush();
        $this->addReference('contractorService-test-equipment-installation-1', $contractorServiceTestEquipmentInstallation1);



        /** @var ServiceNamed $testInspectionNoticeToBeSent1 */
        $testInspectionNoticeToBeSent1 = new ServiceNamed();
        $testInspectionNoticeToBeSent1->setName('Inspection Notice to be sent');
        $testInspectionNoticeToBeSent1->addDeviceNamed($this->getReference('backflow-device'));
        $testInspectionNoticeToBeSent1->setDeficiency('Some inspections for device past due or incomplete');
        $testInspectionNoticeToBeSent1->setIsNeedSendMunicipalityReport(false);
        $testInspectionNoticeToBeSent1->setIsNeedMunicipalityFee(false);
        $manager->persist($testInspectionNoticeToBeSent1);
        $this->addReference('test-inspection-notice-to-be-sent', $testInspectionNoticeToBeSent1);

        $contractorServicetestInspectionNoticeToBeSent1 = new ContractorService();
        $contractorServicetestInspectionNoticeToBeSent1->setNamed($this->getReference('test-inspection-notice-to-be-sent'));
        $contractorServicetestInspectionNoticeToBeSent1->setContractor($this->getReference('contractor-my-company'));
        $contractorServicetestInspectionNoticeToBeSent1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServicetestInspectionNoticeToBeSent1->setState($this->getReference('state-illinois'));
        $contractorServicetestInspectionNoticeToBeSent1->setEstimationTime('0');

        $manager->persist($contractorServicetestInspectionNoticeToBeSent1);
        $manager->flush();
        $this->addReference('contractorService-test-inspection-notice-to-be-sent', $contractorServicetestInspectionNoticeToBeSent1);


        /** @var ServiceNamed $replaceOneCamCheckAssemlby1 */
        $replaceOneCamCheckAssemlby1 = new ServiceNamed();
        $replaceOneCamCheckAssemlby1->setName('Replace #1 cam check assemlby');
        $replaceOneCamCheckAssemlby1->addDeviceNamed($this->getReference('backflow-device'));
        $replaceOneCamCheckAssemlby1->setDeficiency('#1 Cam Check is leaking/failing');
        $replaceOneCamCheckAssemlby1->setIsNeedSendMunicipalityReport(false);
        $replaceOneCamCheckAssemlby1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceOneCamCheckAssemlby1);
        $this->addReference('replace-one-cam-check-assemlby', $replaceOneCamCheckAssemlby1);

        $contractorServiceReplaceOneCamCheckAssemlby1 = new ContractorService();
        $contractorServiceReplaceOneCamCheckAssemlby1->setNamed($this->getReference('replace-one-cam-check-assemlby'));
        $contractorServiceReplaceOneCamCheckAssemlby1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceReplaceOneCamCheckAssemlby1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceReplaceOneCamCheckAssemlby1->setState($this->getReference('state-illinois'));
        $contractorServiceReplaceOneCamCheckAssemlby1->setEstimationTime('0');

        $manager->persist($contractorServiceReplaceOneCamCheckAssemlby1);
        $manager->flush();
        $this->addReference('contractorService-replace-one-cam-check-assemlby', $contractorServiceReplaceOneCamCheckAssemlby1);


        /** @var ServiceNamed $replaceTwoCamCheckAssemlby1 */
        $replaceTwoCamCheckAssemlby1 = new ServiceNamed();
        $replaceTwoCamCheckAssemlby1->setName('Replace #2 cam check assemlby');
        $replaceTwoCamCheckAssemlby1->addDeviceNamed($this->getReference('backflow-device'));
        $replaceTwoCamCheckAssemlby1->setDeficiency('#2 Cam Check is leaking/failing');
        $replaceTwoCamCheckAssemlby1->setIsNeedSendMunicipalityReport(false);
        $replaceTwoCamCheckAssemlby1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceTwoCamCheckAssemlby1);
        $this->addReference('replace-two-cam-check-assemlby', $replaceTwoCamCheckAssemlby1);

        $contractorServiceReplaceTwoCamCheckAssemlby1 = new ContractorService();
        $contractorServiceReplaceTwoCamCheckAssemlby1->setNamed($this->getReference('replace-two-cam-check-assemlby'));
        $contractorServiceReplaceTwoCamCheckAssemlby1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceReplaceTwoCamCheckAssemlby1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceReplaceTwoCamCheckAssemlby1->setState($this->getReference('state-illinois'));
        $contractorServiceReplaceTwoCamCheckAssemlby1->setEstimationTime('0');

        $manager->persist($contractorServiceReplaceTwoCamCheckAssemlby1);
        $manager->flush();
        $this->addReference('contractorService-replace-two-cam-check-assemlby', $contractorServiceReplaceTwoCamCheckAssemlby1);


        /** @var ServiceNamed $replaceBothCamChecks1 */
        $replaceBothCamChecks1 = new ServiceNamed();
        $replaceBothCamChecks1->setName('Replace both Cam Checks');
        $replaceBothCamChecks1->addDeviceNamed($this->getReference('backflow-device'));
        $replaceBothCamChecks1->setDeficiency('Both Cam Checks are leaking/failing');
        $replaceBothCamChecks1->setIsNeedSendMunicipalityReport(false);
        $replaceBothCamChecks1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceBothCamChecks1);
        $this->addReference('replace-both-cam-checks-1', $replaceBothCamChecks1);

        $contractorServiceReplaceBothCamChecks1 = new ContractorService();
        $contractorServiceReplaceBothCamChecks1->setNamed($this->getReference('replace-both-cam-checks-1'));
        $contractorServiceReplaceBothCamChecks1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceReplaceBothCamChecks1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceReplaceBothCamChecks1->setState($this->getReference('state-illinois'));
        $contractorServiceReplaceBothCamChecks1->setEstimationTime('0');

        $manager->persist($contractorServiceReplaceBothCamChecks1);
        $manager->flush();
        $this->addReference('contractorService-replace-both-cam-checks-1', $contractorServiceReplaceBothCamChecks1);


        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
