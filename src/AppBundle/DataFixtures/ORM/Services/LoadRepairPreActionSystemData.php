<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairPreActionSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairPreActionSystem1 */
        $generalRepairPreActionSystem1 = new ServiceNamed();
        $generalRepairPreActionSystem1->setName('General repair');
        $generalRepairPreActionSystem1->addDeviceNamed($this->getReference('pre-action-system-device'));
        $generalRepairPreActionSystem1->setDeficiency('Please see comments');
        $generalRepairPreActionSystem1->setIsNeedSendMunicipalityReport(false);
        $generalRepairPreActionSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairPreActionSystem1);
        $this->addReference('general-repair-pre-action-system-1', $generalRepairPreActionSystem1);

        $contractorGeneralRepairPreActionSystem1 = new ContractorService();
        $contractorGeneralRepairPreActionSystem1->setNamed($this->getReference('general-repair-pre-action-system-1'));
        $contractorGeneralRepairPreActionSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairPreActionSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairPreActionSystem1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairPreActionSystem1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairPreActionSystem1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-pre-action-system-1', $contractorGeneralRepairPreActionSystem1);



        /** @var ServiceNamed $generalRepairPreActionSystem2 */
        $generalRepairPreActionSystem2 = new ServiceNamed();
        $generalRepairPreActionSystem2->setName('General repair');
        $generalRepairPreActionSystem2->addDeviceNamed($this->getReference('pre-action-system-device'));
        $generalRepairPreActionSystem2->setDeficiency('Unidentified issue');
        $generalRepairPreActionSystem2->setIsNeedSendMunicipalityReport(false);
        $generalRepairPreActionSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairPreActionSystem2);
        $this->addReference('general-repair-pre-action-system-2', $generalRepairPreActionSystem2);

        $contractorGeneralRepairPreActionSystem2 = new ContractorService();
        $contractorGeneralRepairPreActionSystem2->setNamed($this->getReference('general-repair-pre-action-system-2'));
        $contractorGeneralRepairPreActionSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairPreActionSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairPreActionSystem2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairPreActionSystem2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairPreActionSystem2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-pre-action-system-2', $contractorGeneralRepairPreActionSystem2);



        /** @var ServiceNamed $equipmentInstallationPreActionSystem1 */
        $equipmentInstallationPreActionSystem1 = new ServiceNamed();
        $equipmentInstallationPreActionSystem1->setName('Equipment installation');
        $equipmentInstallationPreActionSystem1->addDeviceNamed($this->getReference('pre-action-system-device'));
        $equipmentInstallationPreActionSystem1->setDeficiency('Please see comments');
        $equipmentInstallationPreActionSystem1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationPreActionSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationPreActionSystem1);
        $this->addReference('equipment-installation-pre-action-system-1', $equipmentInstallationPreActionSystem1);

        $contractorEquipmentInstallationPreActionSystem1 = new ContractorService();
        $contractorEquipmentInstallationPreActionSystem1->setNamed($this->getReference('equipment-installation-pre-action-system-1'));
        $contractorEquipmentInstallationPreActionSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationPreActionSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationPreActionSystem1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationPreActionSystem1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationPreActionSystem1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-pre-action-system-1', $contractorEquipmentInstallationPreActionSystem1);



        /** @var ServiceNamed $inspectionNoticeToBeSentPreActionSystem1 */
        $inspectionNoticeToBeSentPreActionSystem1 = new ServiceNamed();
        $inspectionNoticeToBeSentPreActionSystem1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentPreActionSystem1->addDeviceNamed($this->getReference('pre-action-system-device'));
        $inspectionNoticeToBeSentPreActionSystem1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentPreActionSystem1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentPreActionSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentPreActionSystem1);
        $this->addReference('inspection-notice-to-be-sent-pre-action-system-1', $inspectionNoticeToBeSentPreActionSystem1);

        $contractorInspectionNoticeToBeSentPreActionSystem1 = new ContractorService();
        $contractorInspectionNoticeToBeSentPreActionSystem1->setNamed($this->getReference('inspection-notice-to-be-sent-pre-action-system-1'));
        $contractorInspectionNoticeToBeSentPreActionSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentPreActionSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentPreActionSystem1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentPreActionSystem1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentPreActionSystem1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-pre-action-system-1', $contractorInspectionNoticeToBeSentPreActionSystem1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
