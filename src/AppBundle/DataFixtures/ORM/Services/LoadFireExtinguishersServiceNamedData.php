<?php

namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadFireExtinguishersServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Annual Fire Extinguisher(s) Inspection service and Contractor Service
        /** @var ServiceNamed $annualFireExtinguisherInspection */
        $annualFireExtinguisherInspection = new ServiceNamed();
        $annualFireExtinguisherInspection->setName('Annual Fire Extinguisher(s) Inspection');
        $annualFireExtinguisherInspection->setFrequency($this->getReference('service-frequency-1-year'));
        $annualFireExtinguisherInspection->addDeviceNamed($this->getReference('fire-root-extinguishers-device'));
        $annualFireExtinguisherInspection->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $annualFireExtinguisherInspection->setIsNeedSendMunicipalityReport(true);
        $annualFireExtinguisherInspection->setIsNeedMunicipalityFee(false);
        $manager->persist($annualFireExtinguisherInspection);
        $this->addReference('annual-fire-extinguisher-inspection', $annualFireExtinguisherInspection);

        /** @var ContractorService $contractorServiceFireExtinguisher1 */
        $contractorServiceFireExtinguisher1 = new ContractorService();
        $contractorServiceFireExtinguisher1->setNamed($this->getReference('annual-fire-extinguisher-inspection'));
        $contractorServiceFireExtinguisher1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceFireExtinguisher1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceFireExtinguisher1->setState($this->getReference('state-illinois'));
        $contractorServiceFireExtinguisher1->setFixedPrice(60.00);
        $contractorServiceFireExtinguisher1->setHoursPrice(60.00);
        $contractorServiceFireExtinguisher1->setReference('NFPA 10');
        $contractorServiceFireExtinguisher1->setEstimationTime('0');

        $manager->persist($contractorServiceFireExtinguisher1);
        $manager->flush();
        $this->addReference('contractorService-fire-extinguisher-1', $contractorServiceFireExtinguisher1);
        //end Annual Fire Extinguisher(s) Inspection service and Contractor Service

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
