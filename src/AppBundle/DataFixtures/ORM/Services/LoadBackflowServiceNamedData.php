<?php

namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadBackflowServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Annual Backflow Inspection service and Contractor Service

        //TODO delete deprecated fields
        /** @var ServiceNamed $serviceNamedAnnualBackflow */
        $serviceNamedAnnualBackflow = new ServiceNamed();
        $serviceNamedAnnualBackflow->setName('Annual Backflow Inspection');
        $serviceNamedAnnualBackflow->setFrequency($this->getReference('service-frequency-1-year'));
        $serviceNamedAnnualBackflow->addDeviceNamed($this->getReference('backflow-device'));
        $serviceNamedAnnualBackflow->setIsNeedSendMunicipalityReport(true);
        $serviceNamedAnnualBackflow->setIsNeedMunicipalityFee(true);
        $manager->persist($serviceNamedAnnualBackflow);
        $this->addReference('annual-backflow-inspection', $serviceNamedAnnualBackflow);

        /** @var ContractorService $contractorServiceAnnualBackflow_1 */
        $contractorServiceAnnualBackflow_1 = new ContractorService();
        $contractorServiceAnnualBackflow_1->setNamed($this->getReference('annual-backflow-inspection'));
        $contractorServiceAnnualBackflow_1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceAnnualBackflow_1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceAnnualBackflow_1->setState($this->getReference('state-illinois'));
        $contractorServiceAnnualBackflow_1->setFixedPrice(100.00);
        $contractorServiceAnnualBackflow_1->setHoursPrice(100.00);
        $contractorServiceAnnualBackflow_1->setReference('35 Ill. Adm. Code 607.104(a) and (b)');
        $contractorServiceAnnualBackflow_1->setCode('153.000');
        $contractorServiceAnnualBackflow_1->setEstimationTime('0.25');

        $manager->persist($contractorServiceAnnualBackflow_1);
        $manager->flush();
        $this->addReference('contractor-service-annual-backflow-one', $contractorServiceAnnualBackflow_1);
        //end Annual Backflow Inspection service and Contractor Service

        //Annual Fire Line Flush service and Contractor Service
        /** @var ServiceNamed $serviceNamedAnnualFireLineFlush */
        $serviceNamedAnnualFireLineFlush = new ServiceNamed();
        $serviceNamedAnnualFireLineFlush->setName('Annual Fire Line Flush');
        $serviceNamedAnnualFireLineFlush->setFrequency($this->getReference('service-frequency-1-year'));
        $serviceNamedAnnualFireLineFlush->addDeviceNamed($this->getReference('backflow-device'));
        $serviceNamedAnnualFireLineFlush->setIsNeedSendMunicipalityReport(true);
        $serviceNamedAnnualFireLineFlush->setIsNeedMunicipalityFee(false);
        $manager->persist($serviceNamedAnnualFireLineFlush);
        $this->addReference('annual-fire-line-flush-inspection', $serviceNamedAnnualFireLineFlush);

        /** @var ContractorService $contractorServiceAnnualFireLineFlushInspection_1 */
        $contractorServiceAnnualFireLineFlushInspection_1 = new ContractorService();
        $contractorServiceAnnualFireLineFlushInspection_1->setNamed($this->getReference('annual-fire-line-flush-inspection'));
        $contractorServiceAnnualFireLineFlushInspection_1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceAnnualFireLineFlushInspection_1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceAnnualFireLineFlushInspection_1->setState($this->getReference('state-illinois'));
        $contractorServiceAnnualFireLineFlushInspection_1->setFixedPrice(700.00);
        $contractorServiceAnnualFireLineFlushInspection_1->setHoursPrice(700.00);
        $contractorServiceAnnualFireLineFlushInspection_1->setEstimationTime('3');

        $manager->persist($contractorServiceAnnualFireLineFlushInspection_1);
        $manager->flush();
        $this->addReference('contractor-service-annual-fire-line-flush-one', $contractorServiceAnnualFireLineFlushInspection_1);
        //end Annual Fire Line Flush service and Contractor Service

        //Annual Y-strainer Cleaning service and Contractor Service
        /** @var ServiceNamed $serviceNamedAnnualYStrainerCleaning */
        $serviceNamedAnnualYStrainerCleaning = new ServiceNamed();
        $serviceNamedAnnualYStrainerCleaning->setName('Annual Y-strainer Cleaning');
        $serviceNamedAnnualYStrainerCleaning->setFrequency($this->getReference('service-frequency-1-year'));
        $serviceNamedAnnualYStrainerCleaning->addDeviceNamed($this->getReference('backflow-device'));
        $serviceNamedAnnualYStrainerCleaning->setIsNeedSendMunicipalityReport(true);
        $serviceNamedAnnualYStrainerCleaning->setIsNeedMunicipalityFee(false);
        $manager->persist($serviceNamedAnnualYStrainerCleaning);
        $this->addReference('annual-y-strainer-cleaning-inspection', $serviceNamedAnnualYStrainerCleaning);

        /** @var ContractorService $contractorServiceAnnualYStrainerCleaning_1 */
        $contractorServiceAnnualYStrainerCleaning_1 = new ContractorService();
        $contractorServiceAnnualYStrainerCleaning_1->setNamed($this->getReference('annual-y-strainer-cleaning-inspection'));
        $contractorServiceAnnualYStrainerCleaning_1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceAnnualYStrainerCleaning_1->setDeviceCategory($this->getReference('device-category-backflow'));
        $contractorServiceAnnualYStrainerCleaning_1->setState($this->getReference('state-illinois'));
        $contractorServiceAnnualYStrainerCleaning_1->setFixedPrice(80.00);
        $contractorServiceAnnualYStrainerCleaning_1->setHoursPrice(80.00);
        $contractorServiceAnnualYStrainerCleaning_1->setEstimationTime('0.5');

        $manager->persist($contractorServiceAnnualYStrainerCleaning_1);
        $manager->flush();
        $this->addReference('contractor-service-annual-y-strainer-cleaning-one', $contractorServiceAnnualYStrainerCleaning_1);
        //end Annual Y-strainer Cleaning service and Contractor Service

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
