<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairFirePumpData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairFirePump1 */
        $generalRepairFirePump1 = new ServiceNamed();
        $generalRepairFirePump1->setName('General repair');
        $generalRepairFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $generalRepairFirePump1->setDeficiency('Please see comments');
        $generalRepairFirePump1->setIsNeedSendMunicipalityReport(false);
        $generalRepairFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFirePump1);
        $this->addReference('general-repair-fire-pump-1', $generalRepairFirePump1);

        $contractorGeneralRepairFirePump1 = new ContractorService();
        $contractorGeneralRepairFirePump1->setNamed($this->getReference('general-repair-fire-pump-1'));
        $contractorGeneralRepairFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFirePump1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFirePump1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFirePump1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-pump-1', $contractorGeneralRepairFirePump1);



        /** @var ServiceNamed $generalRepairFirePump2 */
        $generalRepairFirePump2 = new ServiceNamed();
        $generalRepairFirePump2->setName('General repair');
        $generalRepairFirePump2->addDeviceNamed($this->getReference('fire-pump-device'));
        $generalRepairFirePump2->setDeficiency('Unidentified issue');
        $generalRepairFirePump2->setIsNeedSendMunicipalityReport(false);
        $generalRepairFirePump2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFirePump2);
        $this->addReference('general-repair-fire-pump-2', $generalRepairFirePump2);

        $contractorGeneralRepairFirePump2 = new ContractorService();
        $contractorGeneralRepairFirePump2->setNamed($this->getReference('general-repair-fire-pump-2'));
        $contractorGeneralRepairFirePump2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFirePump2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFirePump2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFirePump2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFirePump2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-pump-2', $contractorGeneralRepairFirePump2);



        /** @var ServiceNamed $equipmentInstallationFirePump1 */
        $equipmentInstallationFirePump1 = new ServiceNamed();
        $equipmentInstallationFirePump1->setName('Equipment installation');
        $equipmentInstallationFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $equipmentInstallationFirePump1->setDeficiency('Please see comments');
        $equipmentInstallationFirePump1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationFirePump1);
        $this->addReference('equipment-installation-fire-pump-1', $equipmentInstallationFirePump1);

        $contractorEquipmentInstallationFirePump1 = new ContractorService();
        $contractorEquipmentInstallationFirePump1->setNamed($this->getReference('equipment-installation-fire-pump-1'));
        $contractorEquipmentInstallationFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationFirePump1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationFirePump1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationFirePump1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-fire-pump-1', $contractorEquipmentInstallationFirePump1);



        /** @var ServiceNamed $inspectionNoticeToBeSentFirePump1 */
        $inspectionNoticeToBeSentFirePump1 = new ServiceNamed();
        $inspectionNoticeToBeSentFirePump1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $inspectionNoticeToBeSentFirePump1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentFirePump1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentFirePump1);
        $this->addReference('inspection-notice-to-be-sent-fire-pump-1', $inspectionNoticeToBeSentFirePump1);

        $contractorInspectionNoticeToBeSentFirePump1 = new ContractorService();
        $contractorInspectionNoticeToBeSentFirePump1->setNamed($this->getReference('inspection-notice-to-be-sent-fire-pump-1'));
        $contractorInspectionNoticeToBeSentFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentFirePump1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentFirePump1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentFirePump1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-fire-pump-1', $contractorInspectionNoticeToBeSentFirePump1);


        /** @var ServiceNamed $rebuildFirePumpFirePump1 */
        $rebuildFirePumpFirePump1 = new ServiceNamed();
        $rebuildFirePumpFirePump1->setName('Rebuild Fire Pump');
        $rebuildFirePumpFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $rebuildFirePumpFirePump1->setDeficiency('Pump is not operational');
        $rebuildFirePumpFirePump1->setIsNeedSendMunicipalityReport(false);
        $rebuildFirePumpFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($rebuildFirePumpFirePump1);
        $this->addReference('rebuild-fire-pump-fire-pump-1', $rebuildFirePumpFirePump1);

        $contractorRebuildFirePumpFirePump1 = new ContractorService();
        $contractorRebuildFirePumpFirePump1->setNamed($this->getReference('rebuild-fire-pump-fire-pump-1'));
        $contractorRebuildFirePumpFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorRebuildFirePumpFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRebuildFirePumpFirePump1->setState($this->getReference('state-illinois'));
        $contractorRebuildFirePumpFirePump1->setEstimationTime('0');

        $manager->persist($contractorRebuildFirePumpFirePump1);
        $manager->flush();
        $this->addReference('contractorService-rebuild-fire-pump-fire-pump-1', $contractorRebuildFirePumpFirePump1);


        /** @var ServiceNamed $repackFirePumpFirePump1 */
        $repackFirePumpFirePump1 = new ServiceNamed();
        $repackFirePumpFirePump1->setName('Repack Fire Pump');
        $repackFirePumpFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $repackFirePumpFirePump1->setDeficiency('Pump is leaking more than specs allow');
        $repackFirePumpFirePump1->setIsNeedSendMunicipalityReport(false);
        $repackFirePumpFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($repackFirePumpFirePump1);
        $this->addReference('repack-fire-pump-fire-pump-1', $repackFirePumpFirePump1);

        $contractorRepackFirePumpFirePump1 = new ContractorService();
        $contractorRepackFirePumpFirePump1->setNamed($this->getReference('repack-fire-pump-fire-pump-1'));
        $contractorRepackFirePumpFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorRepackFirePumpFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRepackFirePumpFirePump1->setState($this->getReference('state-illinois'));
        $contractorRepackFirePumpFirePump1->setEstimationTime('0');

        $manager->persist($contractorRepackFirePumpFirePump1);
        $manager->flush();
        $this->addReference('contractorService-repack-fire-pump-fire-pump-1', $contractorRepackFirePumpFirePump1);


        /** @var ServiceNamed $replaceFirePumpFirePump1 */
        $replaceFirePumpFirePump1 = new ServiceNamed();
        $replaceFirePumpFirePump1->setName('Replace Fire Pump');
        $replaceFirePumpFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $replaceFirePumpFirePump1->setDeficiency('Pump is not Operational/Needs to be replaced');
        $replaceFirePumpFirePump1->setIsNeedSendMunicipalityReport(false);
        $replaceFirePumpFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceFirePumpFirePump1);
        $this->addReference('replace-fire-pump-fire-pump-1', $replaceFirePumpFirePump1);

        $contractorReplaceFirePumpFirePump1 = new ContractorService();
        $contractorReplaceFirePumpFirePump1->setNamed($this->getReference('replace-fire-pump-fire-pump-1'));
        $contractorReplaceFirePumpFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceFirePumpFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceFirePumpFirePump1->setState($this->getReference('state-illinois'));
        $contractorReplaceFirePumpFirePump1->setEstimationTime('0');

        $manager->persist($contractorReplaceFirePumpFirePump1);
        $manager->flush();
        $this->addReference('contractorService-replace-fire-pump-fire-pump-1', $contractorReplaceFirePumpFirePump1);


        /** @var ServiceNamed $unplugSensingLineFirePump1 */
        $unplugSensingLineFirePump1 = new ServiceNamed();
        $unplugSensingLineFirePump1->setName('Unplug sensing line');
        $unplugSensingLineFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $unplugSensingLineFirePump1->setDeficiency('Sensing line plugged');
        $unplugSensingLineFirePump1->setIsNeedSendMunicipalityReport(false);
        $unplugSensingLineFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($unplugSensingLineFirePump1);
        $this->addReference('unplug-sensing-line-fire-pump-1', $unplugSensingLineFirePump1);

        $contractorUnplugSensingLineFirePump1 = new ContractorService();
        $contractorUnplugSensingLineFirePump1->setNamed($this->getReference('unplug-sensing-line-fire-pump-1'));
        $contractorUnplugSensingLineFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorUnplugSensingLineFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorUnplugSensingLineFirePump1->setState($this->getReference('state-illinois'));
        $contractorUnplugSensingLineFirePump1->setEstimationTime('0');

        $manager->persist($contractorUnplugSensingLineFirePump1);
        $manager->flush();
        $this->addReference('contractorService-unplug-sensing-line-fire-pump-1', $contractorUnplugSensingLineFirePump1);


        /** @var ServiceNamed $adjustFirePumpCutInOffPressureFirePump1 */
        $adjustFirePumpCutInOffPressureFirePump1 = new ServiceNamed();
        $adjustFirePumpCutInOffPressureFirePump1->setName('Adjust Fire Pump Cut In/Off Pressure');
        $adjustFirePumpCutInOffPressureFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $adjustFirePumpCutInOffPressureFirePump1->setDeficiency('Fire Pump On/Off pressure not to spec');
        $adjustFirePumpCutInOffPressureFirePump1->setIsNeedSendMunicipalityReport(false);
        $adjustFirePumpCutInOffPressureFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($adjustFirePumpCutInOffPressureFirePump1);
        $this->addReference('adjust-fire-pump-cut-in-off-pressure-fire-pump-1', $adjustFirePumpCutInOffPressureFirePump1);

        $contractorAdjustFirePumpCutInOffPressureFirePump1 = new ContractorService();
        $contractorAdjustFirePumpCutInOffPressureFirePump1->setNamed($this->getReference('adjust-fire-pump-cut-in-off-pressure-fire-pump-1'));
        $contractorAdjustFirePumpCutInOffPressureFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorAdjustFirePumpCutInOffPressureFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorAdjustFirePumpCutInOffPressureFirePump1->setState($this->getReference('state-illinois'));
        $contractorAdjustFirePumpCutInOffPressureFirePump1->setEstimationTime('0');

        $manager->persist($contractorAdjustFirePumpCutInOffPressureFirePump1);
        $manager->flush();
        $this->addReference('contractorService-adjust-fire-pump-cut-in-off-pressure-fire-pump-1', $contractorAdjustFirePumpCutInOffPressureFirePump1);


        /** @var ServiceNamed $replaceJockeyPumpFirePump1 */
        $replaceJockeyPumpFirePump1 = new ServiceNamed();
        $replaceJockeyPumpFirePump1->setName('Replace jockey pump');
        $replaceJockeyPumpFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $replaceJockeyPumpFirePump1->setDeficiency('Jockey pump Inoperative');
        $replaceJockeyPumpFirePump1->setIsNeedSendMunicipalityReport(false);
        $replaceJockeyPumpFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceJockeyPumpFirePump1);
        $this->addReference('replace-jockey-pump-fire-pump-1', $replaceJockeyPumpFirePump1);

        $contractorReplaceJockeyPumpFirePump1 = new ContractorService();
        $contractorReplaceJockeyPumpFirePump1->setNamed($this->getReference('replace-jockey-pump-fire-pump-1'));
        $contractorReplaceJockeyPumpFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceJockeyPumpFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceJockeyPumpFirePump1->setState($this->getReference('state-illinois'));
        $contractorReplaceJockeyPumpFirePump1->setEstimationTime('0');

        $manager->persist($contractorReplaceJockeyPumpFirePump1);
        $manager->flush();
        $this->addReference('contractorService-replace-jockey-pump-fire-pump-1', $contractorReplaceJockeyPumpFirePump1);


        /** @var ServiceNamed $adjustJockeyPumpOnOffSettingsFirePump1 */
        $adjustJockeyPumpOnOffSettingsFirePump1 = new ServiceNamed();
        $adjustJockeyPumpOnOffSettingsFirePump1->setName('Adjust jockey pump on/off settings');
        $adjustJockeyPumpOnOffSettingsFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $adjustJockeyPumpOnOffSettingsFirePump1->setDeficiency('Jockey pump settings incorrect');
        $adjustJockeyPumpOnOffSettingsFirePump1->setIsNeedSendMunicipalityReport(false);
        $adjustJockeyPumpOnOffSettingsFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($adjustJockeyPumpOnOffSettingsFirePump1);
        $this->addReference('adjust-jockey-pump-on-off-settings-fire-pump-1', $adjustJockeyPumpOnOffSettingsFirePump1);

        $contractorAdjustJockeyPumpOnOffSettingsFirePump1 = new ContractorService();
        $contractorAdjustJockeyPumpOnOffSettingsFirePump1->setNamed($this->getReference('adjust-jockey-pump-on-off-settings-fire-pump-1'));
        $contractorAdjustJockeyPumpOnOffSettingsFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorAdjustJockeyPumpOnOffSettingsFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorAdjustJockeyPumpOnOffSettingsFirePump1->setState($this->getReference('state-illinois'));
        $contractorAdjustJockeyPumpOnOffSettingsFirePump1->setEstimationTime('0');

        $manager->persist($contractorAdjustJockeyPumpOnOffSettingsFirePump1);
        $manager->flush();
        $this->addReference('contractorService-adjust-jockey-pump-on-off-settings-fire-pump-1', $contractorAdjustJockeyPumpOnOffSettingsFirePump1);


        /** @var ServiceNamed $replaceMercoidSwitchFirePump1 */
        $replaceMercoidSwitchFirePump1 = new ServiceNamed();
        $replaceMercoidSwitchFirePump1->setName('Replace mercoid switch');
        $replaceMercoidSwitchFirePump1->addDeviceNamed($this->getReference('fire-pump-device'));
        $replaceMercoidSwitchFirePump1->setDeficiency('Mercoid switch Inop');
        $replaceMercoidSwitchFirePump1->setIsNeedSendMunicipalityReport(false);
        $replaceMercoidSwitchFirePump1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceMercoidSwitchFirePump1);
        $this->addReference('replace-mercoid-switch-fire-pump-1', $replaceMercoidSwitchFirePump1);

        $contractorReplaceMercoidSwitchFirePump1 = new ContractorService();
        $contractorReplaceMercoidSwitchFirePump1->setNamed($this->getReference('replace-mercoid-switch-fire-pump-1'));
        $contractorReplaceMercoidSwitchFirePump1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceMercoidSwitchFirePump1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceMercoidSwitchFirePump1->setState($this->getReference('state-illinois'));
        $contractorReplaceMercoidSwitchFirePump1->setEstimationTime('0');

        $manager->persist($contractorReplaceMercoidSwitchFirePump1);
        $manager->flush();
        $this->addReference('contractorService-replace-mercoid-switch-fire-pump-1', $contractorReplaceMercoidSwitchFirePump1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
