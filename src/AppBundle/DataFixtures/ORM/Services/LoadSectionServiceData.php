<?php
namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadSectionServiceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $fiveYearSectionGaugeReplacement */
        $fiveYearSectionGaugeReplacement = new ServiceNamed();
        $fiveYearSectionGaugeReplacement->setName('5 Year Section Gauge Replacement');
        $fiveYearSectionGaugeReplacement->setFrequency($this->getReference('service-frequency-5-years'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('anti-freeze-system'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('section-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('riser-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('pre-action-system-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('deluge-system-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('standpipe-system-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('air-compressor-device'));
        $fiveYearSectionGaugeReplacement->addDeviceNamed($this->getReference('low-point-device'));
        $fiveYearSectionGaugeReplacement->setIsNeedSendMunicipalityReport(true);
        $fiveYearSectionGaugeReplacement->setIsNeedMunicipalityFee(false);
        $manager->persist($fiveYearSectionGaugeReplacement);
        $this->addReference('five-year-section-gauge-replacement', $fiveYearSectionGaugeReplacement);

        /** @var ContractorService $csFiveYearSectionGaugeReplacement */
        $csFiveYearSectionGaugeReplacement = new ContractorService();
        $csFiveYearSectionGaugeReplacement->setNamed($this->getReference('five-year-section-gauge-replacement'));
        $csFiveYearSectionGaugeReplacement->setContractor($this->getReference('contractor-my-company'));
        $csFiveYearSectionGaugeReplacement->setDeviceCategory($this->getReference('device-category-fire'));
        $csFiveYearSectionGaugeReplacement->setState($this->getReference('state-illinois'));
        $csFiveYearSectionGaugeReplacement->setFixedPrice(50.00);
        $csFiveYearSectionGaugeReplacement->setHoursPrice(50.00);
        $csFiveYearSectionGaugeReplacement->setReference('NFPA 25');
        $csFiveYearSectionGaugeReplacement->setEstimationTime('0.5');

        $manager->persist($csFiveYearSectionGaugeReplacement);
        $manager->flush();
        $this->addReference('cs-five-year-section-gauge-replacement', $csFiveYearSectionGaugeReplacement);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
