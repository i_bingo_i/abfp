<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairStandpipeSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairStandpipeSystem1 */
        $generalRepairStandpipeSystem1 = new ServiceNamed();
        $generalRepairStandpipeSystem1->setName('General repair');
        $generalRepairStandpipeSystem1->addDeviceNamed($this->getReference('standpipe-system-device'));
        $generalRepairStandpipeSystem1->setDeficiency('Please see comments');
        $generalRepairStandpipeSystem1->setIsNeedSendMunicipalityReport(false);
        $generalRepairStandpipeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairStandpipeSystem1);
        $this->addReference('general-repair-standpipe-system-1', $generalRepairStandpipeSystem1);

        $contractorGeneralRepairStandpipeSystem1 = new ContractorService();
        $contractorGeneralRepairStandpipeSystem1->setNamed($this->getReference('general-repair-standpipe-system-1'));
        $contractorGeneralRepairStandpipeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairStandpipeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairStandpipeSystem1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairStandpipeSystem1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairStandpipeSystem1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-standpipe-system-1', $contractorGeneralRepairStandpipeSystem1);



        /** @var ServiceNamed $generalRepairStandpipeSystem2 */
        $generalRepairStandpipeSystem2 = new ServiceNamed();
        $generalRepairStandpipeSystem2->setName('General repair');
        $generalRepairStandpipeSystem2->addDeviceNamed($this->getReference('standpipe-system-device'));
        $generalRepairStandpipeSystem2->setDeficiency('Unidentified issue');
        $generalRepairStandpipeSystem2->setIsNeedSendMunicipalityReport(false);
        $generalRepairStandpipeSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairStandpipeSystem2);
        $this->addReference('general-repair-standpipe-system-2', $generalRepairStandpipeSystem2);

        $contractorGeneralRepairStandpipeSystem2 = new ContractorService();
        $contractorGeneralRepairStandpipeSystem2->setNamed($this->getReference('general-repair-standpipe-system-2'));
        $contractorGeneralRepairStandpipeSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairStandpipeSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairStandpipeSystem2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairStandpipeSystem2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairStandpipeSystem2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-standpipe-system-2', $contractorGeneralRepairStandpipeSystem2);



        /** @var ServiceNamed $equipmentInstallationStandpipeSystem1 */
        $equipmentInstallationStandpipeSystem1 = new ServiceNamed();
        $equipmentInstallationStandpipeSystem1->setName('Equipment installation');
        $equipmentInstallationStandpipeSystem1->addDeviceNamed($this->getReference('standpipe-system-device'));
        $equipmentInstallationStandpipeSystem1->setDeficiency('Please see comments');
        $equipmentInstallationStandpipeSystem1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationStandpipeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationStandpipeSystem1);
        $this->addReference('equipment-installation-standpipe-system-1', $equipmentInstallationStandpipeSystem1);

        $contractorEquipmentInstallationStandpipeSystem1 = new ContractorService();
        $contractorEquipmentInstallationStandpipeSystem1->setNamed($this->getReference('equipment-installation-standpipe-system-1'));
        $contractorEquipmentInstallationStandpipeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationStandpipeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationStandpipeSystem1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationStandpipeSystem1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationStandpipeSystem1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-standpipe-system-1', $contractorEquipmentInstallationStandpipeSystem1);



        /** @var ServiceNamed $inspectionNoticeToBeSentStandpipeSystem1 */
        $inspectionNoticeToBeSentStandpipeSystem1 = new ServiceNamed();
        $inspectionNoticeToBeSentStandpipeSystem1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentStandpipeSystem1->addDeviceNamed($this->getReference('standpipe-system-device'));
        $inspectionNoticeToBeSentStandpipeSystem1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentStandpipeSystem1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentStandpipeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentStandpipeSystem1);
        $this->addReference('inspection-notice-to-be-sent-standpipe-system-1', $inspectionNoticeToBeSentStandpipeSystem1);

        $contractorInspectionNoticeToBeSentStandpipeSystem1 = new ContractorService();
        $contractorInspectionNoticeToBeSentStandpipeSystem1->setNamed($this->getReference('inspection-notice-to-be-sent-standpipe-system-1'));
        $contractorInspectionNoticeToBeSentStandpipeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentStandpipeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentStandpipeSystem1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentStandpipeSystem1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentStandpipeSystem1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-standpipe-system-1', $contractorInspectionNoticeToBeSentStandpipeSystem1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
