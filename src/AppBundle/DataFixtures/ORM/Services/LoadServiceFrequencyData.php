<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceFrequency;

class LoadServiceFrequencyData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $serviceFrequencyOne = new ServiceFrequency();
        $serviceFrequencyOne->setName('1 month');
        $serviceFrequencyOne->setAlias('1_month');
        $serviceFrequencyOne->setMounthFrequency('1');
        $manager->persist($serviceFrequencyOne);
        $manager->flush();

        $this->addReference('service-frequency-1-month', $serviceFrequencyOne);

        $serviceFrequencyTwo = new ServiceFrequency();
        $serviceFrequencyTwo->setName('3 months');
        $serviceFrequencyTwo->setAlias('3_months');
        $serviceFrequencyTwo->setMounthFrequency('3');
        $manager->persist($serviceFrequencyTwo);
        $manager->flush();

        $this->addReference('service-frequency-3-months', $serviceFrequencyTwo);

        $serviceFrequencyThree = new ServiceFrequency();
        $serviceFrequencyThree->setName('6 months');
        $serviceFrequencyThree->setAlias('6_months');
        $serviceFrequencyThree->setMounthFrequency('6');
        $manager->persist($serviceFrequencyThree);
        $manager->flush();

        $this->addReference('service-frequency-6-months', $serviceFrequencyThree);

        $serviceFrequencyFour = new ServiceFrequency();
        $serviceFrequencyFour->setName('1 year');
        $serviceFrequencyFour->setAlias('1_year');
        $serviceFrequencyFour->setMounthFrequency('12');
        $manager->persist($serviceFrequencyFour);
        $manager->flush();

        $this->addReference('service-frequency-1-year', $serviceFrequencyFour);

        $serviceFrequencyFive = new ServiceFrequency();
        $serviceFrequencyFive->setName('2 years');
        $serviceFrequencyFive->setAlias('2_years');
        $serviceFrequencyFive->setMounthFrequency('24');
        $manager->persist($serviceFrequencyFive);
        $manager->flush();

        $this->addReference('service-frequency-2-years', $serviceFrequencyFive);

        $serviceFrequencySix = new ServiceFrequency();
        $serviceFrequencySix->setName('5 years');
        $serviceFrequencySix->setAlias('5_years');
        $serviceFrequencySix->setMounthFrequency('60');
        $manager->persist($serviceFrequencySix);
        $manager->flush();

        $this->addReference('service-frequency-5-years', $serviceFrequencySix);

        $serviceFrequencySeven = new ServiceFrequency();
        $serviceFrequencySeven->setName('6 years');
        $serviceFrequencySeven->setAlias('6_years');
        $serviceFrequencySeven->setMounthFrequency('72');
        $manager->persist($serviceFrequencySeven);
        $manager->flush();

        $this->addReference('service-frequency-6-years', $serviceFrequencySeven);

        $serviceFrequencyEight = new ServiceFrequency();
        $serviceFrequencyEight->setName('10 years');
        $serviceFrequencyEight->setAlias('10_years');
        $serviceFrequencyEight->setMounthFrequency('120');
        $manager->persist($serviceFrequencyEight);
        $manager->flush();

        $this->addReference('service-frequency-10-years', $serviceFrequencyEight);

        $serviceFrequencyNine = new ServiceFrequency();
        $serviceFrequencyNine->setName('non-repeatable');
        $serviceFrequencyNine->setAlias('non_repeatable');
        $serviceFrequencyNine->setMounthFrequency('0');
        $manager->persist($serviceFrequencyNine);
        $manager->flush();

        $this->addReference('service-frequency-non-repeatable', $serviceFrequencyNine);

        $serviceFrequencyTen = new ServiceFrequency();
        $serviceFrequencyTen->setName('12 years');
        $serviceFrequencyTen->setAlias('12_years');
        $serviceFrequencyTen->setMounthFrequency('144');
        $manager->persist($serviceFrequencyTen);
        $manager->flush();

        $this->addReference('service-frequency-12-years', $serviceFrequencyTen);

        $serviceFrequencyEleven = new ServiceFrequency();
        $serviceFrequencyEleven->setName('3 years');
        $serviceFrequencyEleven->setAlias('3_years');
        $serviceFrequencyEleven->setMounthFrequency('36');
        $manager->persist($serviceFrequencyEleven);
        $manager->flush();

        $this->addReference('service-frequency-3-years', $serviceFrequencyEleven);
    }

    public function getOrder()
    {
        return 3;
    }
}
