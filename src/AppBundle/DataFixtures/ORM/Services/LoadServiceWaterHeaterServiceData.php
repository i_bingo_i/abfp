<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadServiceWaterHeaterServiceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $waterHeaterDeviceTestOne */
        $waterHeaterDeviceTestOne = new ServiceNamed();
        $waterHeaterDeviceTestOne->setName('Test One Water Heater Device');
        $waterHeaterDeviceTestOne->setFrequency($this->getReference('service-frequency-1-year'));
        $waterHeaterDeviceTestOne->addDeviceNamed($this->getReference('water-heater-device'));
        $waterHeaterDeviceTestOne->setIsNeedMunicipalityFee(true);
        $waterHeaterDeviceTestOne->setIsNeedSendMunicipalityReport(true);
        $manager->persist($waterHeaterDeviceTestOne);
        $this->addReference('test-one-water-heater-device', $waterHeaterDeviceTestOne);

        /** @var ContractorService $csWaterHeaterDeviceTestOne */
        $csWaterHeaterDeviceTestOne = new ContractorService();
        $csWaterHeaterDeviceTestOne->setNamed($this->getReference('test-one-water-heater-device'));
        $csWaterHeaterDeviceTestOne->setContractor($this->getReference('contractor-my-company'));
        $csWaterHeaterDeviceTestOne->setDeviceCategory($this->getReference('device-category-plumbing'));
        $csWaterHeaterDeviceTestOne->setState($this->getReference('state-illinois'));
        $csWaterHeaterDeviceTestOne->setFixedPrice(50.00);
        $csWaterHeaterDeviceTestOne->setHoursPrice(50.00);
        $csWaterHeaterDeviceTestOne->setReference('NFPA 25');
        $csWaterHeaterDeviceTestOne->setEstimationTime('0');

        $manager->persist($csWaterHeaterDeviceTestOne);
        $manager->flush();
        $this->addReference('cs-test-one-water-heater-device', $csWaterHeaterDeviceTestOne);

        $manager->flush();



        /** @var ServiceNamed $waterHeaterDeviceTestTwo */
        $waterHeaterDeviceTestTwo = new ServiceNamed();
        $waterHeaterDeviceTestTwo->setName('Test Two Water Heater Device');
        $waterHeaterDeviceTestTwo->setFrequency($this->getReference('service-frequency-1-year'));
        $waterHeaterDeviceTestTwo->addDeviceNamed($this->getReference('water-heater-device'));
        $waterHeaterDeviceTestTwo->setIsNeedMunicipalityFee(true);
        $waterHeaterDeviceTestTwo->setIsNeedSendMunicipalityReport(true);
        $manager->persist($waterHeaterDeviceTestTwo);
        $this->addReference('test-two-water-heater-device', $waterHeaterDeviceTestTwo);

        /** @var ContractorService $csWaterHeaterDeviceTestTwo */
        $csWaterHeaterDeviceTestTwo = new ContractorService();
        $csWaterHeaterDeviceTestTwo->setNamed($this->getReference('test-two-water-heater-device'));
        $csWaterHeaterDeviceTestTwo->setContractor($this->getReference('contractor-my-company'));
        $csWaterHeaterDeviceTestTwo->setDeviceCategory($this->getReference('device-category-plumbing'));
        $csWaterHeaterDeviceTestTwo->setState($this->getReference('state-illinois'));
        $csWaterHeaterDeviceTestTwo->setFixedPrice(50.00);
        $csWaterHeaterDeviceTestTwo->setHoursPrice(50.00);
        $csWaterHeaterDeviceTestTwo->setReference('NFPA 25');
        $csWaterHeaterDeviceTestTwo->setEstimationTime('0');

        $manager->persist($csWaterHeaterDeviceTestTwo);
        $manager->flush();
        $this->addReference('cs-test-two-water-heater-device', $csWaterHeaterDeviceTestTwo);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
