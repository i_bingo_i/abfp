<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairLiftStationData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $liftStationNonFrequency1 */
        $liftStationNonFrequency1 = new ServiceNamed();
        $liftStationNonFrequency1->setName('Lift Station non-frequency service 1');
        $liftStationNonFrequency1->setDeficiency('Lift Station deficiency 1');
        $liftStationNonFrequency1->addDeviceNamed($this->getReference('lift-station-device'));
        $liftStationNonFrequency1->setIsNeedMunicipalityFee(true);
        $liftStationNonFrequency1->setIsNeedSendMunicipalityReport(true);
        $manager->persist($liftStationNonFrequency1);
        $this->addReference('lift-station-non-frequency-service-1', $liftStationNonFrequency1);

        $contractorLiftStationNonFrequency1 = new ContractorService();
        $contractorLiftStationNonFrequency1->setNamed($this->getReference('lift-station-non-frequency-service-1'));
        $contractorLiftStationNonFrequency1->setContractor($this->getReference('contractor-my-company'));
        $contractorLiftStationNonFrequency1->setDeviceCategory($this->getReference('device-category-plumbing'));
        $contractorLiftStationNonFrequency1->setState($this->getReference('state-illinois'));
        $contractorLiftStationNonFrequency1->setEstimationTime('0');

        $manager->persist($contractorLiftStationNonFrequency1);
        $manager->flush();
        $this->addReference('contractorService-lift-station-non-frequency-service-1', $contractorLiftStationNonFrequency1);



        /** @var ServiceNamed $liftStationNonFrequency2 */
        $liftStationNonFrequency2 = new ServiceNamed();
        $liftStationNonFrequency2->setName('Lift Station non-frequency service 2');
        $liftStationNonFrequency2->setDeficiency('Lift Station deficiency 2');
        $liftStationNonFrequency2->addDeviceNamed($this->getReference('lift-station-device'));
        $liftStationNonFrequency2->setIsNeedMunicipalityFee(true);
        $liftStationNonFrequency2->setIsNeedSendMunicipalityReport(true);
        $manager->persist($liftStationNonFrequency2);
        $this->addReference('lift-station-non-frequency-service-2', $liftStationNonFrequency2);

        $contractorLiftStationNonFrequency2 = new ContractorService();
        $contractorLiftStationNonFrequency2->setNamed($this->getReference('lift-station-non-frequency-service-2'));
        $contractorLiftStationNonFrequency2->setContractor($this->getReference('contractor-my-company'));
        $contractorLiftStationNonFrequency2->setDeviceCategory($this->getReference('device-category-plumbing'));
        $contractorLiftStationNonFrequency2->setState($this->getReference('state-illinois'));
        $contractorLiftStationNonFrequency2->setEstimationTime('0');

        $manager->persist($contractorLiftStationNonFrequency2);
        $manager->flush();
        $this->addReference('contractorService-lift-station-non-frequency-service-2', $contractorLiftStationNonFrequency2);



        /** @var ServiceNamed $liftStationNonFrequency3 */
        $liftStationNonFrequency3 = new ServiceNamed();
        $liftStationNonFrequency3->setName('Lift Station non-frequency service 3');
        $liftStationNonFrequency3->setDeficiency('Lift Station deficiency 3');
        $liftStationNonFrequency3->addDeviceNamed($this->getReference('lift-station-device'));
        $liftStationNonFrequency3->setIsNeedMunicipalityFee(true);
        $liftStationNonFrequency3->setIsNeedSendMunicipalityReport(true);
        $manager->persist($liftStationNonFrequency3);
        $this->addReference('lift-station-non-frequency-service-3', $liftStationNonFrequency3);

        $contractorLiftStationNonFrequency3 = new ContractorService();
        $contractorLiftStationNonFrequency3->setNamed($this->getReference('lift-station-non-frequency-service-3'));
        $contractorLiftStationNonFrequency3->setContractor($this->getReference('contractor-my-company'));
        $contractorLiftStationNonFrequency3->setDeviceCategory($this->getReference('device-category-plumbing'));
        $contractorLiftStationNonFrequency3->setState($this->getReference('state-illinois'));
        $contractorLiftStationNonFrequency3->setEstimationTime('0');

        $manager->persist($contractorLiftStationNonFrequency3);
        $manager->flush();
        $this->addReference('contractorService-lift-station-non-frequency-service-3', $contractorLiftStationNonFrequency3);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
