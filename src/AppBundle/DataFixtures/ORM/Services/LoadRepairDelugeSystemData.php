<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairDelugeSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairDelugeSystem1 */
        $generalRepairDelugeSystem1 = new ServiceNamed();
        $generalRepairDelugeSystem1->setName('General repair');
        $generalRepairDelugeSystem1->addDeviceNamed($this->getReference('deluge-system-device'));
        $generalRepairDelugeSystem1->setDeficiency('Please see comments');
        $generalRepairDelugeSystem1->setIsNeedSendMunicipalityReport(false);
        $generalRepairDelugeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairDelugeSystem1);
        $this->addReference('general-repair-deluge-system-1', $generalRepairDelugeSystem1);

        $contractorGeneralRepairDelugeSystem1 = new ContractorService();
        $contractorGeneralRepairDelugeSystem1->setNamed($this->getReference('general-repair-deluge-system-1'));
        $contractorGeneralRepairDelugeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairDelugeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairDelugeSystem1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairDelugeSystem1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairDelugeSystem1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-deluge-system-1', $contractorGeneralRepairDelugeSystem1);



        /** @var ServiceNamed $generalRepairDelugeSystem2 */
        $generalRepairDelugeSystem2 = new ServiceNamed();
        $generalRepairDelugeSystem2->setName('General repair');
        $generalRepairDelugeSystem2->addDeviceNamed($this->getReference('deluge-system-device'));
        $generalRepairDelugeSystem2->setDeficiency('Unidentified issue');
        $generalRepairDelugeSystem2->setIsNeedSendMunicipalityReport(false);
        $generalRepairDelugeSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairDelugeSystem2);
        $this->addReference('general-repair-deluge-system-2', $generalRepairDelugeSystem2);

        $contractorGeneralRepairDelugeSystem2 = new ContractorService();
        $contractorGeneralRepairDelugeSystem2->setNamed($this->getReference('general-repair-deluge-system-2'));
        $contractorGeneralRepairDelugeSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairDelugeSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairDelugeSystem2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairDelugeSystem2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairDelugeSystem2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-deluge-system-2', $contractorGeneralRepairDelugeSystem2);



        /** @var ServiceNamed $equipmentInstallationDelugeSystem1 */
        $equipmentInstallationDelugeSystem1 = new ServiceNamed();
        $equipmentInstallationDelugeSystem1->setName('Equipment installation');
        $equipmentInstallationDelugeSystem1->addDeviceNamed($this->getReference('deluge-system-device'));
        $equipmentInstallationDelugeSystem1->setDeficiency('Please see comments');
        $equipmentInstallationDelugeSystem1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationDelugeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationDelugeSystem1);
        $this->addReference('equipment-installation-deluge-system-1', $equipmentInstallationDelugeSystem1);

        $contractorEquipmentInstallationDelugeSystem1 = new ContractorService();
        $contractorEquipmentInstallationDelugeSystem1->setNamed($this->getReference('equipment-installation-deluge-system-1'));
        $contractorEquipmentInstallationDelugeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationDelugeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationDelugeSystem1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationDelugeSystem1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationDelugeSystem1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-deluge-system-1', $contractorEquipmentInstallationDelugeSystem1);



        /** @var ServiceNamed $inspectionNoticeToBeSentDelugeSystem1 */
        $inspectionNoticeToBeSentDelugeSystem1 = new ServiceNamed();
        $inspectionNoticeToBeSentDelugeSystem1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentDelugeSystem1->addDeviceNamed($this->getReference('deluge-system-device'));
        $inspectionNoticeToBeSentDelugeSystem1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentDelugeSystem1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentDelugeSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentDelugeSystem1);
        $this->addReference('inspection-notice-to-be-sent-deluge-system-1', $inspectionNoticeToBeSentDelugeSystem1);

        $contractorInspectionNoticeToBeSentDelugeSystem1 = new ContractorService();
        $contractorInspectionNoticeToBeSentDelugeSystem1->setNamed($this->getReference('inspection-notice-to-be-sent-deluge-system-1'));
        $contractorInspectionNoticeToBeSentDelugeSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentDelugeSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentDelugeSystem1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentDelugeSystem1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentDelugeSystem1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-deluge-system-1', $contractorInspectionNoticeToBeSentDelugeSystem1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
