<?php
namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadFireSprinklerSystemServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $fiveYearObstructionInvestigation */
        $fiveYearObstructionInvestigation = new ServiceNamed();
        $fiveYearObstructionInvestigation->setName('5 Year Obstruction Investigation with Check Valve and Pipe Inspection');
        $fiveYearObstructionInvestigation->setFrequency($this->getReference('service-frequency-5-years'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('anti-freeze-system'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('section-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('riser-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('pre-action-system-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('deluge-system-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('standpipe-system-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('air-compressor-device'));
        $fiveYearObstructionInvestigation->addDeviceNamed($this->getReference('low-point-device'));
        $fiveYearObstructionInvestigation->setIsNeedSendMunicipalityReport(true);
        $fiveYearObstructionInvestigation->setIsNeedMunicipalityFee(true);
        $manager->persist($fiveYearObstructionInvestigation);
        $this->addReference('five-year-obstruction-investigation-with-check-valve-and-pipe-inspection', $fiveYearObstructionInvestigation);

        /** @var ContractorService $csFiveYearObstructionInvestigation */
        $csFiveYearObstructionInvestigation = new ContractorService();
        $csFiveYearObstructionInvestigation->setNamed($this->getReference('five-year-obstruction-investigation-with-check-valve-and-pipe-inspection'));
        $csFiveYearObstructionInvestigation->setContractor($this->getReference('contractor-my-company'));
        $csFiveYearObstructionInvestigation->setDeviceCategory($this->getReference('device-category-fire'));
        $csFiveYearObstructionInvestigation->setState($this->getReference('state-illinois'));
        $csFiveYearObstructionInvestigation->setFixedPrice(600.00);
        $csFiveYearObstructionInvestigation->setHoursPrice(600.00);
        $csFiveYearObstructionInvestigation->setReference('NFPA 25');
        $csFiveYearObstructionInvestigation->setEstimationTime('4');

        $manager->persist($csFiveYearObstructionInvestigation);
        $manager->flush();
        $this->addReference('cs-five-year-obstruction-investigation-with-check-valve-and-pipe-inspection', $csFiveYearObstructionInvestigation);


        /** @var ServiceNamed $annualFireSprinklerSystemInspection */
        $annualFireSprinklerSystemInspection = new ServiceNamed();
        $annualFireSprinklerSystemInspection->setName('Annual Fire Sprinkler System Inspection');
        $annualFireSprinklerSystemInspection->setFrequency($this->getReference('service-frequency-1-year'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('anti-freeze-system'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('section-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('riser-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('pre-action-system-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('deluge-system-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('standpipe-system-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('air-compressor-device'));
        $annualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('low-point-device'));
        $annualFireSprinklerSystemInspection->setIsNeedSendMunicipalityReport(true);
        $annualFireSprinklerSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($annualFireSprinklerSystemInspection);
        $this->addReference('annual-fire-sprinkler-system-inspection', $annualFireSprinklerSystemInspection);

        /** @var ContractorService $csAnnualFireSprinklerSystemInspection */
        $csAnnualFireSprinklerSystemInspection = new ContractorService();
        $csAnnualFireSprinklerSystemInspection->setNamed($this->getReference('annual-fire-sprinkler-system-inspection'));
        $csAnnualFireSprinklerSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csAnnualFireSprinklerSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualFireSprinklerSystemInspection->setState($this->getReference('state-illinois'));
        $csAnnualFireSprinklerSystemInspection->setFixedPrice(275.00);
        $csAnnualFireSprinklerSystemInspection->setHoursPrice(275.00);
        $csAnnualFireSprinklerSystemInspection->setReference('NFPA 25');
        $csAnnualFireSprinklerSystemInspection->setEstimationTime('2');

        $manager->persist($csAnnualFireSprinklerSystemInspection);
        $manager->flush();
        $this->addReference('cs-annual-fire-sprinkler-system-inspection', $csAnnualFireSprinklerSystemInspection);


        /** @var ServiceNamed $biAnnualFireSprinklerSystemInspection */
        $biAnnualFireSprinklerSystemInspection = new ServiceNamed();
        $biAnnualFireSprinklerSystemInspection->setName('Bi-Annual Fire Sprinkler System Inspection');
        $biAnnualFireSprinklerSystemInspection->setFrequency($this->getReference('service-frequency-6-months'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('anti-freeze-system'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('section-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('riser-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('pre-action-system-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('deluge-system-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('standpipe-system-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('air-compressor-device'));
        $biAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('low-point-device'));
        $biAnnualFireSprinklerSystemInspection->setIsNeedSendMunicipalityReport(true);
        $biAnnualFireSprinklerSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($biAnnualFireSprinklerSystemInspection);
        $this->addReference('bi-annual-fire-sprinkler-system-inspection', $biAnnualFireSprinklerSystemInspection);

        /** @var ContractorService $csBiAnnualFireSprinklerSystemInspection */
        $csBiAnnualFireSprinklerSystemInspection = new ContractorService();
        $csBiAnnualFireSprinklerSystemInspection->setNamed($this->getReference('bi-annual-fire-sprinkler-system-inspection'));
        $csBiAnnualFireSprinklerSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csBiAnnualFireSprinklerSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csBiAnnualFireSprinklerSystemInspection->setState($this->getReference('state-illinois'));
        $csBiAnnualFireSprinklerSystemInspection->setFixedPrice(225.00);
        $csBiAnnualFireSprinklerSystemInspection->setHoursPrice(225.00);
        $csBiAnnualFireSprinklerSystemInspection->setReference('NFPA 25');
        $csBiAnnualFireSprinklerSystemInspection->setEstimationTime('2');

        $manager->persist($csBiAnnualFireSprinklerSystemInspection);
        $manager->flush();
        $this->addReference('cs-bi-annual-fire-sprinkler-system-inspection', $csBiAnnualFireSprinklerSystemInspection);


        /** @var ServiceNamed $quarterlyAnnualFireSprinklerSystemInspection */
        $quarterlyAnnualFireSprinklerSystemInspection = new ServiceNamed();
        $quarterlyAnnualFireSprinklerSystemInspection->setName('Quarterly Fire Sprinkler System Inspection');
        $quarterlyAnnualFireSprinklerSystemInspection->setFrequency($this->getReference('service-frequency-3-months'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('anti-freeze-system'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('section-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('riser-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('pre-action-system-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('deluge-system-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('standpipe-system-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('air-compressor-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->addDeviceNamed($this->getReference('low-point-device'));
        $quarterlyAnnualFireSprinklerSystemInspection->setIsNeedSendMunicipalityReport(true);
        $quarterlyAnnualFireSprinklerSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($quarterlyAnnualFireSprinklerSystemInspection);
        $this->addReference('quarterly-annual-fire-sprinkler-system-inspection', $quarterlyAnnualFireSprinklerSystemInspection);

        /** @var ContractorService $csQuarterlyAnnualFireSprinklerSystemInspection */
        $csQuarterlyAnnualFireSprinklerSystemInspection = new ContractorService();
        $csQuarterlyAnnualFireSprinklerSystemInspection->setNamed($this->getReference('quarterly-annual-fire-sprinkler-system-inspection'));
        $csQuarterlyAnnualFireSprinklerSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csQuarterlyAnnualFireSprinklerSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csQuarterlyAnnualFireSprinklerSystemInspection->setState($this->getReference('state-illinois'));
        $csQuarterlyAnnualFireSprinklerSystemInspection->setFixedPrice(225.00);
        $csQuarterlyAnnualFireSprinklerSystemInspection->setHoursPrice(225.00);
        $csQuarterlyAnnualFireSprinklerSystemInspection->setReference('NFPA 25');
        $csQuarterlyAnnualFireSprinklerSystemInspection->setEstimationTime('2');

        $manager->persist($csQuarterlyAnnualFireSprinklerSystemInspection);
        $manager->flush();
        $this->addReference('cs-quarterly-annual-fire-sprinkler-system-inspection', $csQuarterlyAnnualFireSprinklerSystemInspection);


        /** @var ServiceNamed $annualFireHydrantInspection */
        $annualFireHydrantInspection = new ServiceNamed();
        $annualFireHydrantInspection->setName('Annual Fire Hydrant Inspection');
        $annualFireHydrantInspection->setFrequency($this->getReference('service-frequency-1-year'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('anti-freeze-system'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('section-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('riser-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('pre-action-system-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('deluge-system-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('standpipe-system-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('air-compressor-device'));
        $annualFireHydrantInspection->addDeviceNamed($this->getReference('low-point-device'));
        $annualFireHydrantInspection->setIsNeedSendMunicipalityReport(true);
        $annualFireHydrantInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($annualFireHydrantInspection);
        $this->addReference('annual-fire-hydrant-inspection', $annualFireHydrantInspection);

        /** @var ContractorService $csAnnualFireHydrantInspection */
        $csAnnualFireHydrantInspection = new ContractorService();
        $csAnnualFireHydrantInspection->setNamed($this->getReference('annual-fire-hydrant-inspection'));
        $csAnnualFireHydrantInspection->setContractor($this->getReference('contractor-my-company'));
        $csAnnualFireHydrantInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualFireHydrantInspection->setState($this->getReference('state-illinois'));
        $csAnnualFireHydrantInspection->setFixedPrice(300.00);
        $csAnnualFireHydrantInspection->setHoursPrice(300.00);
        $csAnnualFireHydrantInspection->setReference('NFPA 291');
        $csAnnualFireHydrantInspection->setEstimationTime('0');

        $manager->persist($csAnnualFireHydrantInspection);
        $manager->flush();
        $this->addReference('cs-annual-fire-hydrant-inspection', $csAnnualFireHydrantInspection);

        /** @var ServiceNamed $annualStandpipeSystemInspection */
        $annualStandpipeSystemInspection = new ServiceNamed();
        $annualStandpipeSystemInspection->setName('Annual Standpipe System Inspection');
        $annualStandpipeSystemInspection->setFrequency($this->getReference('service-frequency-1-year'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('anti-freeze-system'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('section-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('riser-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('pre-action-system-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('deluge-system-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('standpipe-system-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('air-compressor-device'));
        $annualStandpipeSystemInspection->addDeviceNamed($this->getReference('low-point-device'));
        $annualStandpipeSystemInspection->setIsNeedSendMunicipalityReport(true);
        $annualStandpipeSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($annualStandpipeSystemInspection);
        $this->addReference('annual-standpipe-system-inspection', $annualStandpipeSystemInspection);

        /** @var ContractorService $csAnnualStandpipeSystemInspection */
        $csAnnualStandpipeSystemInspection = new ContractorService();
        $csAnnualStandpipeSystemInspection->setNamed($this->getReference('annual-standpipe-system-inspection'));
        $csAnnualStandpipeSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csAnnualStandpipeSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualStandpipeSystemInspection->setState($this->getReference('state-illinois'));
        $csAnnualStandpipeSystemInspection->setFixedPrice(200.00);
        $csAnnualStandpipeSystemInspection->setHoursPrice(200.00);
        $csAnnualStandpipeSystemInspection->setReference('NFPA 25');
        $csAnnualStandpipeSystemInspection->setEstimationTime('1');

        $manager->persist($csAnnualStandpipeSystemInspection);
        $manager->flush();
        $this->addReference('cs-annual-standpipe-system-inspection', $csAnnualStandpipeSystemInspection);


        /** @var ServiceNamed $biAnnualPreActionSystemInspection */
        $biAnnualPreActionSystemInspection = new ServiceNamed();
        $biAnnualPreActionSystemInspection->setName('Bi-Annual Pre-Action System Inspection');
        $biAnnualPreActionSystemInspection->setFrequency($this->getReference('service-frequency-6-months'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('anti-freeze-system'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('fdc-check-valve-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('section-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('riser-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('pre-action-system-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('deluge-system-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('standpipe-system-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('air-compressor-device'));
        $biAnnualPreActionSystemInspection->addDeviceNamed($this->getReference('low-point-device'));
        $biAnnualPreActionSystemInspection->setIsNeedSendMunicipalityReport(true);
        $biAnnualPreActionSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($biAnnualPreActionSystemInspection);
        $this->addReference('bi-annual-pre-action-system-inspection', $biAnnualPreActionSystemInspection);

        /** @var ContractorService $csBiAnnualPreActionSystemInspection */
        $csBiAnnualPreActionSystemInspection = new ContractorService();
        $csBiAnnualPreActionSystemInspection->setNamed($this->getReference('bi-annual-pre-action-system-inspection'));
        $csBiAnnualPreActionSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csBiAnnualPreActionSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csBiAnnualPreActionSystemInspection->setState($this->getReference('state-illinois'));
        $csBiAnnualPreActionSystemInspection->setFixedPrice(325.00);
        $csBiAnnualPreActionSystemInspection->setHoursPrice(325.00);
        $csBiAnnualPreActionSystemInspection->setReference('NFPA 13');
        $csBiAnnualPreActionSystemInspection->setEstimationTime('2');

        $manager->persist($csBiAnnualPreActionSystemInspection);
        $manager->flush();
        $this->addReference('cs-bi-annual-pre-action-system-inspection', $csBiAnnualPreActionSystemInspection);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
