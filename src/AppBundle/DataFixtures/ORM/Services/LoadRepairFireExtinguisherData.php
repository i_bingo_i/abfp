<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairFireExtinguisherData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairFireExtinguisher1 */
        $generalRepairFireExtinguisher1 = new ServiceNamed();
        $generalRepairFireExtinguisher1->setName('General repair');
        $generalRepairFireExtinguisher1->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $generalRepairFireExtinguisher1->setDeficiency('Please see comments');
        $generalRepairFireExtinguisher1->setIsNeedSendMunicipalityReport(false);
        $generalRepairFireExtinguisher1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFireExtinguisher1);
        $this->addReference('general-repair-fire-extinguisher-1', $generalRepairFireExtinguisher1);

        $contractorGeneralRepairFireExtinguisher1 = new ContractorService();
        $contractorGeneralRepairFireExtinguisher1->setNamed($this->getReference('general-repair-fire-extinguisher-1'));
        $contractorGeneralRepairFireExtinguisher1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFireExtinguisher1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFireExtinguisher1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFireExtinguisher1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFireExtinguisher1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-extinguisher-1', $contractorGeneralRepairFireExtinguisher1);



        /** @var ServiceNamed $generalRepairFireExtinguisher2 */
        $generalRepairFireExtinguisher2 = new ServiceNamed();
        $generalRepairFireExtinguisher2->setName('General repair');
        $generalRepairFireExtinguisher2->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $generalRepairFireExtinguisher2->setDeficiency('Unidentified issue');
        $generalRepairFireExtinguisher2->setIsNeedSendMunicipalityReport(false);
        $generalRepairFireExtinguisher2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFireExtinguisher2);
        $this->addReference('general-repair-fire-extinguisher-2', $generalRepairFireExtinguisher2);

        $contractorGeneralRepairFireExtinguisher2 = new ContractorService();
        $contractorGeneralRepairFireExtinguisher2->setNamed($this->getReference('general-repair-fire-extinguisher-2'));
        $contractorGeneralRepairFireExtinguisher2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFireExtinguisher2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFireExtinguisher2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFireExtinguisher2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFireExtinguisher2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-extinguisher-2', $contractorGeneralRepairFireExtinguisher2);



        /** @var ServiceNamed $equipmentInstallationFireExtinguisher1 */
        $equipmentInstallationFireExtinguisher1 = new ServiceNamed();
        $equipmentInstallationFireExtinguisher1->setName('Equipment installation');
        $equipmentInstallationFireExtinguisher1->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $equipmentInstallationFireExtinguisher1->setDeficiency('Please see comments');
        $equipmentInstallationFireExtinguisher1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationFireExtinguisher1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationFireExtinguisher1);
        $this->addReference('equipment-installation-fire-extinguisher-1', $equipmentInstallationFireExtinguisher1);

        $contractorEquipmentInstallationFireExtinguisher1 = new ContractorService();
        $contractorEquipmentInstallationFireExtinguisher1->setNamed($this->getReference('equipment-installation-fire-extinguisher-1'));
        $contractorEquipmentInstallationFireExtinguisher1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationFireExtinguisher1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationFireExtinguisher1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationFireExtinguisher1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationFireExtinguisher1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-fire-extinguisher-1', $contractorEquipmentInstallationFireExtinguisher1);



        /** @var ServiceNamed $inspectionNoticeToBeSentFireExtinguisher1 */
        $inspectionNoticeToBeSentFireExtinguisher1 = new ServiceNamed();
        $inspectionNoticeToBeSentFireExtinguisher1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentFireExtinguisher1->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $inspectionNoticeToBeSentFireExtinguisher1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentFireExtinguisher1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentFireExtinguisher1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentFireExtinguisher1);
        $this->addReference('inspection-notice-to-be-sent-fire-extinguisher-1', $inspectionNoticeToBeSentFireExtinguisher1);

        $contractorInspectionNoticeToBeSentFireExtinguisher1 = new ContractorService();
        $contractorInspectionNoticeToBeSentFireExtinguisher1->setNamed($this->getReference('inspection-notice-to-be-sent-fire-extinguisher-1'));
        $contractorInspectionNoticeToBeSentFireExtinguisher1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentFireExtinguisher1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentFireExtinguisher1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentFireExtinguisher1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentFireExtinguisher1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-fire-extinguisher-1', $contractorInspectionNoticeToBeSentFireExtinguisher1);


        /** @var ServiceNamed $replaceExtinguisherFireExtinguisher1 */
        $replaceExtinguisherFireExtinguisher1 = new ServiceNamed();
        $replaceExtinguisherFireExtinguisher1->setName('Replace Extinguisher');
        $replaceExtinguisherFireExtinguisher1->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $replaceExtinguisherFireExtinguisher1->setDeficiency('Extinguisher Outdated');
        $replaceExtinguisherFireExtinguisher1->setIsNeedSendMunicipalityReport(false);
        $replaceExtinguisherFireExtinguisher1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceExtinguisherFireExtinguisher1);
        $this->addReference('replace-extinguisher-fire-extinguisher-1', $replaceExtinguisherFireExtinguisher1);

        $contractorReplaceExtinguisherFireExtinguisher1 = new ContractorService();
        $contractorReplaceExtinguisherFireExtinguisher1->setNamed($this->getReference('replace-extinguisher-fire-extinguisher-1'));
        $contractorReplaceExtinguisherFireExtinguisher1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceExtinguisherFireExtinguisher1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceExtinguisherFireExtinguisher1->setState($this->getReference('state-illinois'));
        $contractorReplaceExtinguisherFireExtinguisher1->setEstimationTime('0');

        $manager->persist($contractorReplaceExtinguisherFireExtinguisher1);
        $manager->flush();
        $this->addReference('contractorService-replace-extinguisher-fire-extinguisher-1', $contractorReplaceExtinguisherFireExtinguisher1);


        /** @var ServiceNamed $replaceExtinguisherFireExtinguisher2 */
        $replaceExtinguisherFireExtinguisher2 = new ServiceNamed();
        $replaceExtinguisherFireExtinguisher2->setName('Replace Extinguisher');
        $replaceExtinguisherFireExtinguisher2->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $replaceExtinguisherFireExtinguisher2->setDeficiency('Extinguisher Discharged');
        $replaceExtinguisherFireExtinguisher2->setIsNeedSendMunicipalityReport(false);
        $replaceExtinguisherFireExtinguisher2->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceExtinguisherFireExtinguisher2);
        $this->addReference('replace-extinguisher-fire-extinguisher-2', $replaceExtinguisherFireExtinguisher2);

        $contractorReplaceExtinguisherFireExtinguisher2 = new ContractorService();
        $contractorReplaceExtinguisherFireExtinguisher2->setNamed($this->getReference('replace-extinguisher-fire-extinguisher-2'));
        $contractorReplaceExtinguisherFireExtinguisher2->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceExtinguisherFireExtinguisher2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceExtinguisherFireExtinguisher2->setState($this->getReference('state-illinois'));
        $contractorReplaceExtinguisherFireExtinguisher2->setEstimationTime('0');

        $manager->persist($contractorReplaceExtinguisherFireExtinguisher2);
        $manager->flush();
        $this->addReference('contractorService-replace-extinguisher-fire-extinguisher-2', $contractorReplaceExtinguisherFireExtinguisher2);


        /** @var ServiceNamed $replaceExtinguisherFireExtinguisher3 */
        $replaceExtinguisherFireExtinguisher3 = new ServiceNamed();
        $replaceExtinguisherFireExtinguisher3->setName('Replace Extinguisher');
        $replaceExtinguisherFireExtinguisher3->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $replaceExtinguisherFireExtinguisher3->setDeficiency('Extinguisher Dented/Damage to body');
        $replaceExtinguisherFireExtinguisher3->setIsNeedSendMunicipalityReport(false);
        $replaceExtinguisherFireExtinguisher3->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceExtinguisherFireExtinguisher3);
        $this->addReference('replace-extinguisher-fire-extinguisher-3', $replaceExtinguisherFireExtinguisher3);

        $contractorReplaceExtinguisherFireExtinguisher3 = new ContractorService();
        $contractorReplaceExtinguisherFireExtinguisher3->setNamed($this->getReference('replace-extinguisher-fire-extinguisher-3'));
        $contractorReplaceExtinguisherFireExtinguisher3->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceExtinguisherFireExtinguisher3->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceExtinguisherFireExtinguisher3->setState($this->getReference('state-illinois'));
        $contractorReplaceExtinguisherFireExtinguisher3->setEstimationTime('0');

        $manager->persist($contractorReplaceExtinguisherFireExtinguisher3);
        $manager->flush();
        $this->addReference('contractorService-replace-extinguisher-fire-extinguisher-3', $contractorReplaceExtinguisherFireExtinguisher3);


        /** @var ServiceNamed $rechargeExtinguisherFireExtinguisher1 */
        $rechargeExtinguisherFireExtinguisher1 = new ServiceNamed();
        $rechargeExtinguisherFireExtinguisher1->setName('Recharge Extinguisher');
        $rechargeExtinguisherFireExtinguisher1->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $rechargeExtinguisherFireExtinguisher1->setDeficiency('Extinguisher Discharged');
        $rechargeExtinguisherFireExtinguisher1->setIsNeedSendMunicipalityReport(false);
        $rechargeExtinguisherFireExtinguisher1->setIsNeedMunicipalityFee(false);
        $manager->persist($rechargeExtinguisherFireExtinguisher1);
        $this->addReference('recharge-extinguisher-fire-extinguisher-1', $rechargeExtinguisherFireExtinguisher1);

        $contractorRechargeExtinguisherFireExtinguisher1 = new ContractorService();
        $contractorRechargeExtinguisherFireExtinguisher1->setNamed($this->getReference('recharge-extinguisher-fire-extinguisher-1'));
        $contractorRechargeExtinguisherFireExtinguisher1->setContractor($this->getReference('contractor-my-company'));
        $contractorRechargeExtinguisherFireExtinguisher1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRechargeExtinguisherFireExtinguisher1->setState($this->getReference('state-illinois'));
        $contractorRechargeExtinguisherFireExtinguisher1->setEstimationTime('0');

        $manager->persist($contractorRechargeExtinguisherFireExtinguisher1);
        $manager->flush();
        $this->addReference('contractorService-recharge-extinguisher-fire-extinguisher-1', $contractorRechargeExtinguisherFireExtinguisher1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
