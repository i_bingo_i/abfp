<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairDryValveSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairDryValveSystem1 */
        $generalRepairDryValveSystem1 = new ServiceNamed();
        $generalRepairDryValveSystem1->setName('General repair');
        $generalRepairDryValveSystem1->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $generalRepairDryValveSystem1->setDeficiency('Please see comments');
        $generalRepairDryValveSystem1->setIsNeedSendMunicipalityReport(false);
        $generalRepairDryValveSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairDryValveSystem1);
        $this->addReference('general-repair-dry-valve-system-1', $generalRepairDryValveSystem1);

        $contractorGeneralRepairDryValveSystem1 = new ContractorService();
        $contractorGeneralRepairDryValveSystem1->setNamed($this->getReference('general-repair-dry-valve-system-1'));
        $contractorGeneralRepairDryValveSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairDryValveSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairDryValveSystem1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairDryValveSystem1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairDryValveSystem1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-dry-valve-system-1', $contractorGeneralRepairDryValveSystem1);



        /** @var ServiceNamed $generalRepairDryValveSystem2 */
        $generalRepairDryValveSystem2 = new ServiceNamed();
        $generalRepairDryValveSystem2->setName('General repair');
        $generalRepairDryValveSystem2->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $generalRepairDryValveSystem2->setDeficiency('Unidentified issue');
        $generalRepairDryValveSystem2->setIsNeedSendMunicipalityReport(false);
        $generalRepairDryValveSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairDryValveSystem2);
        $this->addReference('general-repair-dry-valve-system-2', $generalRepairDryValveSystem2);

        $contractorGeneralRepairDryValveSystem2 = new ContractorService();
        $contractorGeneralRepairDryValveSystem2->setNamed($this->getReference('general-repair-dry-valve-system-2'));
        $contractorGeneralRepairDryValveSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairDryValveSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairDryValveSystem2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairDryValveSystem2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairDryValveSystem2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-dry-valve-system-2', $contractorGeneralRepairDryValveSystem2);


        /** @var ServiceNamed $equipmentInstallationDryValveSystem1 */
        $equipmentInstallationDryValveSystem1 = new ServiceNamed();
        $equipmentInstallationDryValveSystem1->setName('Equipment installation');
        $equipmentInstallationDryValveSystem1->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $equipmentInstallationDryValveSystem1->setDeficiency('Please see comments');
        $equipmentInstallationDryValveSystem1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationDryValveSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationDryValveSystem1);
        $this->addReference('equipment-installation-dry-valve-system-1', $equipmentInstallationDryValveSystem1);

        $contractorEquipmentInstallationDryValveSystem1 = new ContractorService();
        $contractorEquipmentInstallationDryValveSystem1->setNamed($this->getReference('equipment-installation-dry-valve-system-1'));
        $contractorEquipmentInstallationDryValveSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationDryValveSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationDryValveSystem1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationDryValveSystem1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationDryValveSystem1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-dry-valve-system-1', $contractorEquipmentInstallationDryValveSystem1);


        /** @var ServiceNamed $inspectionNoticeToBeSentDryValveSystem1 */
        $inspectionNoticeToBeSentDryValveSystem1 = new ServiceNamed();
        $inspectionNoticeToBeSentDryValveSystem1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentDryValveSystem1->addDeviceNamed($this->getReference('dry-valve-system-device'));
        $inspectionNoticeToBeSentDryValveSystem1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentDryValveSystem1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentDryValveSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentDryValveSystem1);
        $this->addReference('inspection-notice-to-be-sent-dry-valve-system-1', $inspectionNoticeToBeSentDryValveSystem1);

        $contractorInspectionNoticeToBeSentDryValveSystem1 = new ContractorService();
        $contractorInspectionNoticeToBeSentDryValveSystem1->setNamed($this->getReference('inspection-notice-to-be-sent-dry-valve-system-1'));
        $contractorInspectionNoticeToBeSentDryValveSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentDryValveSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentDryValveSystem1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentDryValveSystem1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentDryValveSystem1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-dry-valve-system-1', $contractorInspectionNoticeToBeSentDryValveSystem1);

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
