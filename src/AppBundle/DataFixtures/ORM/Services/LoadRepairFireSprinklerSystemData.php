<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairFireSprinklerSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairFireSprinklerSystem1 */
        $generalRepairFireSprinklerSystem1 = new ServiceNamed();
        $generalRepairFireSprinklerSystem1->setName('General repair');
        $generalRepairFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $generalRepairFireSprinklerSystem1->setDeficiency('Please see comments');
        $generalRepairFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $generalRepairFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFireSprinklerSystem1);
        $this->addReference('general-repair-fire-sprinkler-system-1', $generalRepairFireSprinklerSystem1);

        $contractorGeneralRepairFireSprinklerSystem1 = new ContractorService();
        $contractorGeneralRepairFireSprinklerSystem1->setNamed($this->getReference('general-repair-fire-sprinkler-system-1'));
        $contractorGeneralRepairFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-sprinkler-system-1', $contractorGeneralRepairFireSprinklerSystem1);



        /** @var ServiceNamed $generalRepairFireSprinklerSystem2 */
        $generalRepairFireSprinklerSystem2 = new ServiceNamed();
        $generalRepairFireSprinklerSystem2->setName('General repair');
        $generalRepairFireSprinklerSystem2->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $generalRepairFireSprinklerSystem2->setDeficiency('Unidentified issue');
        $generalRepairFireSprinklerSystem2->setIsNeedSendMunicipalityReport(false);
        $generalRepairFireSprinklerSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFireSprinklerSystem2);
        $this->addReference('general-repair-fire-sprinkler-system-2', $generalRepairFireSprinklerSystem2);

        $contractorGeneralRepairFireSprinklerSystem2 = new ContractorService();
        $contractorGeneralRepairFireSprinklerSystem2->setNamed($this->getReference('general-repair-fire-sprinkler-system-2'));
        $contractorGeneralRepairFireSprinklerSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFireSprinklerSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFireSprinklerSystem2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFireSprinklerSystem2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFireSprinklerSystem2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-sprinkler-system-2', $contractorGeneralRepairFireSprinklerSystem2);



        /** @var ServiceNamed $equipmentInstallationFireSprinklerSystem1 */
        $equipmentInstallationFireSprinklerSystem1 = new ServiceNamed();
        $equipmentInstallationFireSprinklerSystem1->setName('Equipment installation');
        $equipmentInstallationFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $equipmentInstallationFireSprinklerSystem1->setDeficiency('Please see comments');
        $equipmentInstallationFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationFireSprinklerSystem1);
        $this->addReference('equipment-installation-fire-sprinkler-system-1', $equipmentInstallationFireSprinklerSystem1);

        $contractorEquipmentInstallationFireSprinklerSystem1 = new ContractorService();
        $contractorEquipmentInstallationFireSprinklerSystem1->setNamed($this->getReference('equipment-installation-fire-sprinkler-system-1'));
        $contractorEquipmentInstallationFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-fire-sprinkler-system-1', $contractorEquipmentInstallationFireSprinklerSystem1);



        /** @var ServiceNamed $inspectionNoticeToBeSentFireSprinklerSystem1 */
        $inspectionNoticeToBeSentFireSprinklerSystem1 = new ServiceNamed();
        $inspectionNoticeToBeSentFireSprinklerSystem1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $inspectionNoticeToBeSentFireSprinklerSystem1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentFireSprinklerSystem1);
        $this->addReference('inspection-notice-to-be-sent-fire-sprinkler-system-1', $inspectionNoticeToBeSentFireSprinklerSystem1);

        $contractorInspectionNoticeToBeSentFireSprinklerSystem1 = new ContractorService();
        $contractorInspectionNoticeToBeSentFireSprinklerSystem1->setNamed($this->getReference('inspection-notice-to-be-sent-fire-sprinkler-system-1'));
        $contractorInspectionNoticeToBeSentFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-fire-sprinkler-system-1', $contractorInspectionNoticeToBeSentFireSprinklerSystem1);


        /** @var ServiceNamed $replaceHeaterWithNewFireSprinklerSystem1 */
        $replaceHeaterWithNewFireSprinklerSystem1 = new ServiceNamed();
        $replaceHeaterWithNewFireSprinklerSystem1->setName('Replace heater with new');
        $replaceHeaterWithNewFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceHeaterWithNewFireSprinklerSystem1->setDeficiency('Sprinkler room heater inop');
        $replaceHeaterWithNewFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceHeaterWithNewFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceHeaterWithNewFireSprinklerSystem1);
        $this->addReference('replace-heater-with-new-fire-sprinkler-system-1', $replaceHeaterWithNewFireSprinklerSystem1);

        $contractorReplaceHeaterWithNewFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceHeaterWithNewFireSprinklerSystem1->setNamed($this->getReference('replace-heater-with-new-fire-sprinkler-system-1'));
        $contractorReplaceHeaterWithNewFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceHeaterWithNewFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceHeaterWithNewFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceHeaterWithNewFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceHeaterWithNewFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-heater-with-new-fire-sprinkler-system-1', $contractorReplaceHeaterWithNewFireSprinklerSystem1);


        /** @var ServiceNamed $installNewTamperOnControlValveFireSprinklerSystem1 */
        $installNewTamperOnControlValveFireSprinklerSystem1 = new ServiceNamed();
        $installNewTamperOnControlValveFireSprinklerSystem1->setName('Install new tamper on control valve');
        $installNewTamperOnControlValveFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $installNewTamperOnControlValveFireSprinklerSystem1->setDeficiency('Missing tamper switch');
        $installNewTamperOnControlValveFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $installNewTamperOnControlValveFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($installNewTamperOnControlValveFireSprinklerSystem1);
        $this->addReference('install-new-tamper-on-control-valve-fire-sprinkler-system-1', $installNewTamperOnControlValveFireSprinklerSystem1);

        $contractorInstallNewTamperOnControlValveFireSprinklerSystem1 = new ContractorService();
        $contractorInstallNewTamperOnControlValveFireSprinklerSystem1->setNamed($this->getReference('install-new-tamper-on-control-valve-fire-sprinkler-system-1'));
        $contractorInstallNewTamperOnControlValveFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallNewTamperOnControlValveFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallNewTamperOnControlValveFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorInstallNewTamperOnControlValveFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorInstallNewTamperOnControlValveFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-install-new-tamper-on-control-valve-fire-sprinkler-system-1', $contractorInstallNewTamperOnControlValveFireSprinklerSystem1);


        /** @var ServiceNamed $replaceFlowSwitchFireSprinklerSystem1 */
        $replaceFlowSwitchFireSprinklerSystem1 = new ServiceNamed();
        $replaceFlowSwitchFireSprinklerSystem1->setName('Replace flow switch');
        $replaceFlowSwitchFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceFlowSwitchFireSprinklerSystem1->setDeficiency('Defective flow switch');
        $replaceFlowSwitchFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceFlowSwitchFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceFlowSwitchFireSprinklerSystem1);
        $this->addReference('replace-flow-switch-fire-sprinkler-system-1', $replaceFlowSwitchFireSprinklerSystem1);

        $contractorReplaceFlowSwitchFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceFlowSwitchFireSprinklerSystem1->setNamed($this->getReference('replace-flow-switch-fire-sprinkler-system-1'));
        $contractorReplaceFlowSwitchFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceFlowSwitchFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceFlowSwitchFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceFlowSwitchFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceFlowSwitchFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-flow-switch-fire-sprinkler-system-1', $contractorReplaceFlowSwitchFireSprinklerSystem1);


        /** @var ServiceNamed $replaceSprinklerHeadFireSprinklerSystem1 */
        $replaceSprinklerHeadFireSprinklerSystem1 = new ServiceNamed();
        $replaceSprinklerHeadFireSprinklerSystem1->setName('Replace sprinkler head');
        $replaceSprinklerHeadFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceSprinklerHeadFireSprinklerSystem1->setDeficiency('Sprinkler head leaking');
        $replaceSprinklerHeadFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceSprinklerHeadFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceSprinklerHeadFireSprinklerSystem1);
        $this->addReference('replace-sprinkler-head-fire-sprinkler-system-1', $replaceSprinklerHeadFireSprinklerSystem1);

        $contractorReplaceSprinklerHeadFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceSprinklerHeadFireSprinklerSystem1->setNamed($this->getReference('replace-sprinkler-head-fire-sprinkler-system-1'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceSprinklerHeadFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-sprinkler-head-fire-sprinkler-system-1', $contractorReplaceSprinklerHeadFireSprinklerSystem1);


        /** @var ServiceNamed $replaceSprinklerHeadFireSprinklerSystem2 */
        $replaceSprinklerHeadFireSprinklerSystem2 = new ServiceNamed();
        $replaceSprinklerHeadFireSprinklerSystem2->setName('Replace sprinkler head');
        $replaceSprinklerHeadFireSprinklerSystem2->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceSprinklerHeadFireSprinklerSystem2->setDeficiency('Sprinkler head painted');
        $replaceSprinklerHeadFireSprinklerSystem2->setIsNeedSendMunicipalityReport(false);
        $replaceSprinklerHeadFireSprinklerSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceSprinklerHeadFireSprinklerSystem2);
        $this->addReference('replace-sprinkler-head-fire-sprinkler-system-2', $replaceSprinklerHeadFireSprinklerSystem2);

        $contractorReplaceSprinklerHeadFireSprinklerSystem2 = new ContractorService();
        $contractorReplaceSprinklerHeadFireSprinklerSystem2->setNamed($this->getReference('replace-sprinkler-head-fire-sprinkler-system-2'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem2->setState($this->getReference('state-illinois'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem2->setEstimationTime('0');

        $manager->persist($contractorReplaceSprinklerHeadFireSprinklerSystem2);
        $manager->flush();
        $this->addReference('contractorService-replace-sprinkler-head-fire-sprinkler-system-2', $contractorReplaceSprinklerHeadFireSprinklerSystem2);


        /** @var ServiceNamed $replaceSprinklerHeadFireSprinklerSystem3 */
        $replaceSprinklerHeadFireSprinklerSystem3 = new ServiceNamed();
        $replaceSprinklerHeadFireSprinklerSystem3->setName('Replace sprinkler head');
        $replaceSprinklerHeadFireSprinklerSystem3->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceSprinklerHeadFireSprinklerSystem3->setDeficiency('Sprinkler head corroded');
        $replaceSprinklerHeadFireSprinklerSystem3->setIsNeedSendMunicipalityReport(false);
        $replaceSprinklerHeadFireSprinklerSystem3->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceSprinklerHeadFireSprinklerSystem3);
        $this->addReference('replace-sprinkler-head-fire-sprinkler-system-3', $replaceSprinklerHeadFireSprinklerSystem3);

        $contractorReplaceSprinklerHeadFireSprinklerSystem3 = new ContractorService();
        $contractorReplaceSprinklerHeadFireSprinklerSystem3->setNamed($this->getReference('replace-sprinkler-head-fire-sprinkler-system-3'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem3->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem3->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem3->setState($this->getReference('state-illinois'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem3->setEstimationTime('0');

        $manager->persist($contractorReplaceSprinklerHeadFireSprinklerSystem3);
        $manager->flush();
        $this->addReference('contractorService-replace-sprinkler-head-fire-sprinkler-system-3', $contractorReplaceSprinklerHeadFireSprinklerSystem3);


        /** @var ServiceNamed $replaceSprinklerHeadFireSprinklerSystem4 */
        $replaceSprinklerHeadFireSprinklerSystem4 = new ServiceNamed();
        $replaceSprinklerHeadFireSprinklerSystem4->setName('Replace sprinkler head');
        $replaceSprinklerHeadFireSprinklerSystem4->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceSprinklerHeadFireSprinklerSystem4->setDeficiency('Sprinkler head has physical damage');
        $replaceSprinklerHeadFireSprinklerSystem4->setIsNeedSendMunicipalityReport(false);
        $replaceSprinklerHeadFireSprinklerSystem4->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceSprinklerHeadFireSprinklerSystem4);
        $this->addReference('replace-sprinkler-head-fire-sprinkler-system-4', $replaceSprinklerHeadFireSprinklerSystem4);

        $contractorReplaceSprinklerHeadFireSprinklerSystem4 = new ContractorService();
        $contractorReplaceSprinklerHeadFireSprinklerSystem4->setNamed($this->getReference('replace-sprinkler-head-fire-sprinkler-system-4'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem4->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem4->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem4->setState($this->getReference('state-illinois'));
        $contractorReplaceSprinklerHeadFireSprinklerSystem4->setEstimationTime('0');

        $manager->persist($contractorReplaceSprinklerHeadFireSprinklerSystem4);
        $manager->flush();
        $this->addReference('contractorService-replace-sprinkler-head-fire-sprinkler-system-4', $contractorReplaceSprinklerHeadFireSprinklerSystem4);


        /** @var ServiceNamed $replaceSprinklerPipeFireSprinklerSystem1 */
        $replaceSprinklerPipeFireSprinklerSystem1 = new ServiceNamed();
        $replaceSprinklerPipeFireSprinklerSystem1->setName('Replace sprinkler pipe');
        $replaceSprinklerPipeFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceSprinklerPipeFireSprinklerSystem1->setDeficiency('Sprinkler pipe is deteriorated/corroded');
        $replaceSprinklerPipeFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceSprinklerPipeFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceSprinklerPipeFireSprinklerSystem1);
        $this->addReference('replace-sprinkler-pipe-fire-sprinkler-system-1', $replaceSprinklerPipeFireSprinklerSystem1);

        $contractorReplaceSprinklerPipeFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceSprinklerPipeFireSprinklerSystem1->setNamed($this->getReference('replace-sprinkler-pipe-fire-sprinkler-system-1'));
        $contractorReplaceSprinklerPipeFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceSprinklerPipeFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceSprinklerPipeFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceSprinklerPipeFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceSprinklerPipeFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-sprinkler-pipe-fire-sprinkler-system-1', $contractorReplaceSprinklerPipeFireSprinklerSystem1);


        /** @var ServiceNamed $installNewSprinklerHeadAndPipeFireSprinklerSystem1 */
        $installNewSprinklerHeadAndPipeFireSprinklerSystem1 = new ServiceNamed();
        $installNewSprinklerHeadAndPipeFireSprinklerSystem1->setName('Install new sprinkler head and pipe');
        $installNewSprinklerHeadAndPipeFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $installNewSprinklerHeadAndPipeFireSprinklerSystem1->setDeficiency('Missing sprinkler head');
        $installNewSprinklerHeadAndPipeFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $installNewSprinklerHeadAndPipeFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($installNewSprinklerHeadAndPipeFireSprinklerSystem1);
        $this->addReference('install-new-sprinkler-head-and-pipe-fire-sprinkler-system-1', $installNewSprinklerHeadAndPipeFireSprinklerSystem1);

        $contractorInstallNewSprinklerHeadAndPipeFireSprinklerSystem1 = new ContractorService();
        $contractorInstallNewSprinklerHeadAndPipeFireSprinklerSystem1->setNamed($this->getReference('install-new-sprinkler-head-and-pipe-fire-sprinkler-system-1'));
        $contractorInstallNewSprinklerHeadAndPipeFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallNewSprinklerHeadAndPipeFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallNewSprinklerHeadAndPipeFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorInstallNewSprinklerHeadAndPipeFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorInstallNewSprinklerHeadAndPipeFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-install-new-sprinkler-head-and-pipe-fire-sprinkler-system-1', $contractorInstallNewSprinklerHeadAndPipeFireSprinklerSystem1);


        /** @var ServiceNamed $repackValvePackingFireSprinklerSystem1 */
        $repackValvePackingFireSprinklerSystem1 = new ServiceNamed();
        $repackValvePackingFireSprinklerSystem1->setName('Repack valve packing');
        $repackValvePackingFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $repackValvePackingFireSprinklerSystem1->setDeficiency('Valve packing leaking');
        $repackValvePackingFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $repackValvePackingFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($repackValvePackingFireSprinklerSystem1);
        $this->addReference('repack-valve-packing-fire-sprinkler-system-1', $repackValvePackingFireSprinklerSystem1);

        $contractorRepackValvePackingFireSprinklerSystem1 = new ContractorService();
        $contractorRepackValvePackingFireSprinklerSystem1->setNamed($this->getReference('repack-valve-packing-fire-sprinkler-system-1'));
        $contractorRepackValvePackingFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorRepackValvePackingFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRepackValvePackingFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorRepackValvePackingFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorRepackValvePackingFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-repack-valve-packing-fire-sprinkler-system-1', $contractorRepackValvePackingFireSprinklerSystem1);


        /** @var ServiceNamed $replaceGaugeFireSprinklerSystem1 */
        $replaceGaugeFireSprinklerSystem1 = new ServiceNamed();
        $replaceGaugeFireSprinklerSystem1->setName('Replace gauge');
        $replaceGaugeFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceGaugeFireSprinklerSystem1->setDeficiency('Broken pressure gauge');
        $replaceGaugeFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceGaugeFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceGaugeFireSprinklerSystem1);
        $this->addReference('replace-gauge-fire-sprinkler-system-1', $replaceGaugeFireSprinklerSystem1);

        $contractorReplaceGaugeFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceGaugeFireSprinklerSystem1->setNamed($this->getReference('replace-gauge-fire-sprinkler-system-1'));
        $contractorReplaceGaugeFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceGaugeFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceGaugeFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceGaugeFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceGaugeFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-gauge-fire-sprinkler-system-1', $contractorReplaceGaugeFireSprinklerSystem1);


        /** @var ServiceNamed $replaceGaugeFireSprinklerSystem2 */
        $replaceGaugeFireSprinklerSystem2 = new ServiceNamed();
        $replaceGaugeFireSprinklerSystem2->setName('Replace gauge');
        $replaceGaugeFireSprinklerSystem2->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceGaugeFireSprinklerSystem2->setDeficiency('Outdated pressure gauge');
        $replaceGaugeFireSprinklerSystem2->setIsNeedSendMunicipalityReport(false);
        $replaceGaugeFireSprinklerSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceGaugeFireSprinklerSystem2);
        $this->addReference('replace-gauge-fire-sprinkler-system-2', $replaceGaugeFireSprinklerSystem2);

        $contractorReplaceGaugeFireSprinklerSystem2 = new ContractorService();
        $contractorReplaceGaugeFireSprinklerSystem2->setNamed($this->getReference('replace-gauge-fire-sprinkler-system-2'));
        $contractorReplaceGaugeFireSprinklerSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceGaugeFireSprinklerSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceGaugeFireSprinklerSystem2->setState($this->getReference('state-illinois'));
        $contractorReplaceGaugeFireSprinklerSystem2->setEstimationTime('0');

        $manager->persist($contractorReplaceGaugeFireSprinklerSystem2);
        $manager->flush();
        $this->addReference('contractorService-replace-gauge-fire-sprinkler-system-2', $contractorReplaceGaugeFireSprinklerSystem2);


        /** @var ServiceNamed $replaceControlValveFireSprinklerSystem1 */
        $replaceControlValveFireSprinklerSystem1 = new ServiceNamed();
        $replaceControlValveFireSprinklerSystem1->setName('Replace Control valve');
        $replaceControlValveFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceControlValveFireSprinklerSystem1->setDeficiency('Control Valve inoperative');
        $replaceControlValveFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceControlValveFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceControlValveFireSprinklerSystem1);
        $this->addReference('replace-control-valve-fire-sprinkler-system-1', $replaceControlValveFireSprinklerSystem1);

        $contractorReplaceControlValveFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceControlValveFireSprinklerSystem1->setNamed($this->getReference('replace-control-valve-fire-sprinkler-system-1'));
        $contractorReplaceControlValveFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceControlValveFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceControlValveFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceControlValveFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceControlValveFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-control-valve-fire-sprinkler-system-1', $contractorReplaceControlValveFireSprinklerSystem1);


        /** @var ServiceNamed $installTestHeaderHoseValvesFireSprinklerSystem1 */
        $installTestHeaderHoseValvesFireSprinklerSystem1 = new ServiceNamed();
        $installTestHeaderHoseValvesFireSprinklerSystem1->setName('Install Test Header Hose Valve(s)');
        $installTestHeaderHoseValvesFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $installTestHeaderHoseValvesFireSprinklerSystem1->setDeficiency('Missing Test Header Hose Valve(s)');
        $installTestHeaderHoseValvesFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $installTestHeaderHoseValvesFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($installTestHeaderHoseValvesFireSprinklerSystem1);
        $this->addReference('install-test-header-hose-valves-fire-sprinkler-system-1', $installTestHeaderHoseValvesFireSprinklerSystem1);

        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem1 = new ContractorService();
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem1->setNamed($this->getReference('install-test-header-hose-valves-fire-sprinkler-system-1'));
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorInstallTestHeaderHoseValvesFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-install-test-header-hose-valves-fire-sprinkler-system-1', $contractorInstallTestHeaderHoseValvesFireSprinklerSystem1);


        /** @var ServiceNamed $installTestHeaderHoseValvesFireSprinklerSystem2 */
        $installTestHeaderHoseValvesFireSprinklerSystem2 = new ServiceNamed();
        $installTestHeaderHoseValvesFireSprinklerSystem2->setName('Install Test Header Hose Valve(s)');
        $installTestHeaderHoseValvesFireSprinklerSystem2->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $installTestHeaderHoseValvesFireSprinklerSystem2->setDeficiency('Damaged Test Header Hose Valve(s)');
        $installTestHeaderHoseValvesFireSprinklerSystem2->setIsNeedSendMunicipalityReport(false);
        $installTestHeaderHoseValvesFireSprinklerSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($installTestHeaderHoseValvesFireSprinklerSystem2);
        $this->addReference('install-test-header-hose-valves-fire-sprinkler-system-2', $installTestHeaderHoseValvesFireSprinklerSystem2);

        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem2 = new ContractorService();
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem2->setNamed($this->getReference('install-test-header-hose-valves-fire-sprinkler-system-2'));
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem2->setState($this->getReference('state-illinois'));
        $contractorInstallTestHeaderHoseValvesFireSprinklerSystem2->setEstimationTime('0');

        $manager->persist($contractorInstallTestHeaderHoseValvesFireSprinklerSystem2);
        $manager->flush();
        $this->addReference('contractorService-install-test-header-hose-valves-fire-sprinkler-system-2', $contractorInstallTestHeaderHoseValvesFireSprinklerSystem2);


        /** @var ServiceNamed $installNewFlowSwitchFireSprinklerSystem1 */
        $installNewFlowSwitchFireSprinklerSystem1 = new ServiceNamed();
        $installNewFlowSwitchFireSprinklerSystem1->setName('Install new flow switch');
        $installNewFlowSwitchFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $installNewFlowSwitchFireSprinklerSystem1->setDeficiency('Flow switch inoperative');
        $installNewFlowSwitchFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $installNewFlowSwitchFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($installNewFlowSwitchFireSprinklerSystem1);
        $this->addReference('install-new-flow-switch-fire-sprinkler-system-1', $installNewFlowSwitchFireSprinklerSystem1);

        $contractorInstallNewFlowSwitchFireSprinklerSystem1 = new ContractorService();
        $contractorInstallNewFlowSwitchFireSprinklerSystem1->setNamed($this->getReference('install-new-flow-switch-fire-sprinkler-system-1'));
        $contractorInstallNewFlowSwitchFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallNewFlowSwitchFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallNewFlowSwitchFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorInstallNewFlowSwitchFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorInstallNewFlowSwitchFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-install-new-flow-switch-fire-sprinkler-system-1', $contractorInstallNewFlowSwitchFireSprinklerSystem1);


        /** @var ServiceNamed $installNewFlowSwitchFireSprinklerSystem2 */
        $installNewFlowSwitchFireSprinklerSystem2 = new ServiceNamed();
        $installNewFlowSwitchFireSprinklerSystem2->setName('Install new flow switch');
        $installNewFlowSwitchFireSprinklerSystem2->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $installNewFlowSwitchFireSprinklerSystem2->setDeficiency('Flow switch seal leaking');
        $installNewFlowSwitchFireSprinklerSystem2->setIsNeedSendMunicipalityReport(false);
        $installNewFlowSwitchFireSprinklerSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($installNewFlowSwitchFireSprinklerSystem2);
        $this->addReference('install-new-flow-switch-fire-sprinkler-system-2', $installNewFlowSwitchFireSprinklerSystem2);

        $contractorInstallNewFlowSwitchFireSprinklerSystem2 = new ContractorService();
        $contractorInstallNewFlowSwitchFireSprinklerSystem2->setNamed($this->getReference('install-new-flow-switch-fire-sprinkler-system-2'));
        $contractorInstallNewFlowSwitchFireSprinklerSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallNewFlowSwitchFireSprinklerSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallNewFlowSwitchFireSprinklerSystem2->setState($this->getReference('state-illinois'));
        $contractorInstallNewFlowSwitchFireSprinklerSystem2->setEstimationTime('0');

        $manager->persist($contractorInstallNewFlowSwitchFireSprinklerSystem2);
        $manager->flush();
        $this->addReference('contractorService-install-new-flow-switch-fire-sprinkler-system-2', $contractorInstallNewFlowSwitchFireSprinklerSystem2);


        /** @var ServiceNamed $relocateMoveSprinklerHeadsFireSprinklerSystem1 */
        $relocateMoveSprinklerHeadsFireSprinklerSystem1 = new ServiceNamed();
        $relocateMoveSprinklerHeadsFireSprinklerSystem1->setName('Relocate/Move Sprinkler Head(s)');
        $relocateMoveSprinklerHeadsFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $relocateMoveSprinklerHeadsFireSprinklerSystem1->setDeficiency('Sprinkler Head(s) Needs Relocating');
        $relocateMoveSprinklerHeadsFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $relocateMoveSprinklerHeadsFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($relocateMoveSprinklerHeadsFireSprinklerSystem1);
        $this->addReference('relocate-move-sprinkler-heads-fire-sprinkler-system-1', $relocateMoveSprinklerHeadsFireSprinklerSystem1);

        $contractorRelocateMoveSprinklerHeadsFireSprinklerSystem1 = new ContractorService();
        $contractorRelocateMoveSprinklerHeadsFireSprinklerSystem1->setNamed($this->getReference('relocate-move-sprinkler-heads-fire-sprinkler-system-1'));
        $contractorRelocateMoveSprinklerHeadsFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorRelocateMoveSprinklerHeadsFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRelocateMoveSprinklerHeadsFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorRelocateMoveSprinklerHeadsFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorRelocateMoveSprinklerHeadsFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-relocate-move-sprinkler-heads-fire-sprinkler-system-1', $contractorRelocateMoveSprinklerHeadsFireSprinklerSystem1);


        /** @var ServiceNamed $locateLeakAndRepairFireSprinklerSystem1 */
        $locateLeakAndRepairFireSprinklerSystem1 = new ServiceNamed();
        $locateLeakAndRepairFireSprinklerSystem1->setName('Locate leak and repair');
        $locateLeakAndRepairFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $locateLeakAndRepairFireSprinklerSystem1->setDeficiency('Unknown leak in fire system piping');
        $locateLeakAndRepairFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $locateLeakAndRepairFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($locateLeakAndRepairFireSprinklerSystem1);
        $this->addReference('locate-leak-and-repair-fire-sprinkler-system-1', $locateLeakAndRepairFireSprinklerSystem1);

        $contractorLocateLeakAndRepairFireSprinklerSystem1 = new ContractorService();
        $contractorLocateLeakAndRepairFireSprinklerSystem1->setNamed($this->getReference('locate-leak-and-repair-fire-sprinkler-system-1'));
        $contractorLocateLeakAndRepairFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorLocateLeakAndRepairFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorLocateLeakAndRepairFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorLocateLeakAndRepairFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorLocateLeakAndRepairFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-locate-leak-and-repair-fire-sprinkler-system-1', $contractorLocateLeakAndRepairFireSprinklerSystem1);


        /** @var ServiceNamed $pipeMainDrainToOutsideFireSprinklerSystem1 */
        $pipeMainDrainToOutsideFireSprinklerSystem1 = new ServiceNamed();
        $pipeMainDrainToOutsideFireSprinklerSystem1->setName('Pipe main drain to outside');
        $pipeMainDrainToOutsideFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $pipeMainDrainToOutsideFireSprinklerSystem1->setDeficiency('Main drain not piped to outside');
        $pipeMainDrainToOutsideFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $pipeMainDrainToOutsideFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($pipeMainDrainToOutsideFireSprinklerSystem1);
        $this->addReference('pipe-main-drain-to-outside-fire-sprinkler-system-1', $pipeMainDrainToOutsideFireSprinklerSystem1);

        $contractorPipeMainDrainToOutsideFireSprinklerSystem1 = new ContractorService();
        $contractorPipeMainDrainToOutsideFireSprinklerSystem1->setNamed($this->getReference('pipe-main-drain-to-outside-fire-sprinkler-system-1'));
        $contractorPipeMainDrainToOutsideFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorPipeMainDrainToOutsideFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorPipeMainDrainToOutsideFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorPipeMainDrainToOutsideFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorPipeMainDrainToOutsideFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-pipe-main-drain-to-outside-fire-sprinkler-system-1', $contractorPipeMainDrainToOutsideFireSprinklerSystem1);


        /** @var ServiceNamed $pipeMainDrainToFloorDrainFireSprinklerSystem1 */
        $pipeMainDrainToFloorDrainFireSprinklerSystem1 = new ServiceNamed();
        $pipeMainDrainToFloorDrainFireSprinklerSystem1->setName('Pipe main drain to floor drain');
        $pipeMainDrainToFloorDrainFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $pipeMainDrainToFloorDrainFireSprinklerSystem1->setDeficiency('Main drain not piped to floor drain');
        $pipeMainDrainToFloorDrainFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $pipeMainDrainToFloorDrainFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($pipeMainDrainToFloorDrainFireSprinklerSystem1);
        $this->addReference('pipe-main-drain-to-floor-drain-fire-sprinkler-system-1', $pipeMainDrainToFloorDrainFireSprinklerSystem1);

        $contractorPipeMainDrainToFloorDrainFireSprinklerSystem1 = new ContractorService();
        $contractorPipeMainDrainToFloorDrainFireSprinklerSystem1->setNamed($this->getReference('pipe-main-drain-to-floor-drain-fire-sprinkler-system-1'));
        $contractorPipeMainDrainToFloorDrainFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorPipeMainDrainToFloorDrainFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorPipeMainDrainToFloorDrainFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorPipeMainDrainToFloorDrainFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorPipeMainDrainToFloorDrainFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-pipe-main-drain-to-floor-drain-fire-sprinkler-system-1', $contractorPipeMainDrainToFloorDrainFireSprinklerSystem1);


        /** @var ServiceNamed $replaceSprinklerHeadsWithNewFireSprinklerSystem1 */
        $replaceSprinklerHeadsWithNewFireSprinklerSystem1 = new ServiceNamed();
        $replaceSprinklerHeadsWithNewFireSprinklerSystem1->setName('Replace sprinkler heads with new');
        $replaceSprinklerHeadsWithNewFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceSprinklerHeadsWithNewFireSprinklerSystem1->setDeficiency('Obsolete sprinkler heads');
        $replaceSprinklerHeadsWithNewFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceSprinklerHeadsWithNewFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceSprinklerHeadsWithNewFireSprinklerSystem1);
        $this->addReference('replace-sprinkler-heads-with-new-fire-sprinkler-system-1', $replaceSprinklerHeadsWithNewFireSprinklerSystem1);

        $contractorReplaceSprinklerHeadsWithNewFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceSprinklerHeadsWithNewFireSprinklerSystem1->setNamed($this->getReference('replace-sprinkler-heads-with-new-fire-sprinkler-system-1'));
        $contractorReplaceSprinklerHeadsWithNewFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceSprinklerHeadsWithNewFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceSprinklerHeadsWithNewFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceSprinklerHeadsWithNewFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceSprinklerHeadsWithNewFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-sprinkler-heads-with-new-fire-sprinkler-system-1', $contractorReplaceSprinklerHeadsWithNewFireSprinklerSystem1);


        /** @var ServiceNamed $replaceRepairTrimRingFireSprinklerSystem1 */
        $replaceRepairTrimRingFireSprinklerSystem1 = new ServiceNamed();
        $replaceRepairTrimRingFireSprinklerSystem1->setName('Replace/repair trim ring');
        $replaceRepairTrimRingFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceRepairTrimRingFireSprinklerSystem1->setDeficiency('Missing/damage trim on sprinkler head');
        $replaceRepairTrimRingFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceRepairTrimRingFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceRepairTrimRingFireSprinklerSystem1);
        $this->addReference('replace-repair-trim-ring-fire-sprinkler-system-1', $replaceRepairTrimRingFireSprinklerSystem1);

        $contractorReplaceRepairTrimRingFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceRepairTrimRingFireSprinklerSystem1->setNamed($this->getReference('replace-repair-trim-ring-fire-sprinkler-system-1'));
        $contractorReplaceRepairTrimRingFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceRepairTrimRingFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceRepairTrimRingFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceRepairTrimRingFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceRepairTrimRingFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-repair-trim-ring-fire-sprinkler-system-1', $contractorReplaceRepairTrimRingFireSprinklerSystem1);


        /** @var ServiceNamed $sendToLabForTestingFireSprinklerSystem1 */
        $sendToLabForTestingFireSprinklerSystem1 = new ServiceNamed();
        $sendToLabForTestingFireSprinklerSystem1->setName('Send to lab for testing');
        $sendToLabForTestingFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $sendToLabForTestingFireSprinklerSystem1->setDeficiency('Outdated sprinkler heads');
        $sendToLabForTestingFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $sendToLabForTestingFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($sendToLabForTestingFireSprinklerSystem1);
        $this->addReference('send-to-lab-for-testing-fire-sprinkler-system-1', $sendToLabForTestingFireSprinklerSystem1);

        $contractorSendToLabForTestingFireSprinklerSystem1 = new ContractorService();
        $contractorSendToLabForTestingFireSprinklerSystem1->setNamed($this->getReference('send-to-lab-for-testing-fire-sprinkler-system-1'));
        $contractorSendToLabForTestingFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorSendToLabForTestingFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorSendToLabForTestingFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorSendToLabForTestingFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorSendToLabForTestingFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-send-to-lab-for-testing-fire-sprinkler-system-1', $contractorSendToLabForTestingFireSprinklerSystem1);


        /** @var ServiceNamed $replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1 */
        $replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1 = new ServiceNamed();
        $replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setName('Replace all fire sprinkler heads outdated');
        $replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setDeficiency('Outdated sprinkler heads');
        $replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1);
        $this->addReference('replace-all-fire-sprinkler-heads-outdated-fire-sprinkler-system-1', $replaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1);

        $contractorReplaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setNamed($this->getReference('replace-all-fire-sprinkler-heads-outdated-fire-sprinkler-system-1'));
        $contractorReplaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-all-fire-sprinkler-heads-outdated-fire-sprinkler-system-1', $contractorReplaceAllFireSprinklerHeadsOutdatedFireSprinklerSystem1);


        /** @var ServiceNamed $replaceAllRecalledSprinklerHeadsFireSprinklerSystem1 */
        $replaceAllRecalledSprinklerHeadsFireSprinklerSystem1 = new ServiceNamed();
        $replaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setName('Replace all recalled sprinkler heads');
        $replaceAllRecalledSprinklerHeadsFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setDeficiency('Recalled sprinkler heads');
        $replaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceAllRecalledSprinklerHeadsFireSprinklerSystem1);
        $this->addReference('replace-all-recalled-sprinkler-heads-fire-sprinkler-system-1', $replaceAllRecalledSprinklerHeadsFireSprinklerSystem1);

        $contractorReplaceAllRecalledSprinklerHeadsFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setNamed($this->getReference('replace-all-recalled-sprinkler-heads-fire-sprinkler-system-1'));
        $contractorReplaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceAllRecalledSprinklerHeadsFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceAllRecalledSprinklerHeadsFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-all-recalled-sprinkler-heads-fire-sprinkler-system-1', $contractorReplaceAllRecalledSprinklerHeadsFireSprinklerSystem1);


        /** @var ServiceNamed $correctSprinklerHeadOrientationFireSprinklerSystem1 */
        $correctSprinklerHeadOrientationFireSprinklerSystem1 = new ServiceNamed();
        $correctSprinklerHeadOrientationFireSprinklerSystem1->setName('Correct sprinkler head orientation');
        $correctSprinklerHeadOrientationFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $correctSprinklerHeadOrientationFireSprinklerSystem1->setDeficiency('Sprinkler head orientation incorrect');
        $correctSprinklerHeadOrientationFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $correctSprinklerHeadOrientationFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($correctSprinklerHeadOrientationFireSprinklerSystem1);
        $this->addReference('correct-sprinkler-head-orientation-fire-sprinkler-system-1', $correctSprinklerHeadOrientationFireSprinklerSystem1);

        $contractorCorrectSprinklerHeadOrientationFireSprinklerSystem1 = new ContractorService();
        $contractorCorrectSprinklerHeadOrientationFireSprinklerSystem1->setNamed($this->getReference('correct-sprinkler-head-orientation-fire-sprinkler-system-1'));
        $contractorCorrectSprinklerHeadOrientationFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorCorrectSprinklerHeadOrientationFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorCorrectSprinklerHeadOrientationFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorCorrectSprinklerHeadOrientationFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorCorrectSprinklerHeadOrientationFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-correct-sprinkler-head-orientation-fire-sprinkler-system-1', $contractorCorrectSprinklerHeadOrientationFireSprinklerSystem1);


        /** @var ServiceNamed $repairLeaksOnSensingLineFireSprinklerSystem1 */
        $repairLeaksOnSensingLineFireSprinklerSystem1 = new ServiceNamed();
        $repairLeaksOnSensingLineFireSprinklerSystem1->setName('Repair leaks on sensing line');
        $repairLeaksOnSensingLineFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $repairLeaksOnSensingLineFireSprinklerSystem1->setDeficiency('Air pressure sensing line leaking');
        $repairLeaksOnSensingLineFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $repairLeaksOnSensingLineFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($repairLeaksOnSensingLineFireSprinklerSystem1);
        $this->addReference('repair-leaks-on-sensing-line-fire-sprinkler-system-1', $repairLeaksOnSensingLineFireSprinklerSystem1);

        $contractorRepairLeaksOnSensingLineFireSprinklerSystem1 = new ContractorService();
        $contractorRepairLeaksOnSensingLineFireSprinklerSystem1->setNamed($this->getReference('repair-leaks-on-sensing-line-fire-sprinkler-system-1'));
        $contractorRepairLeaksOnSensingLineFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorRepairLeaksOnSensingLineFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRepairLeaksOnSensingLineFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorRepairLeaksOnSensingLineFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorRepairLeaksOnSensingLineFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-repair-leaks-on-sensing-line-fire-sprinkler-system-1', $contractorRepairLeaksOnSensingLineFireSprinklerSystem1);


        /** @var ServiceNamed $drainRiserAndReplaceShutOffValveFireSprinklerSystem1 */
        $drainRiserAndReplaceShutOffValveFireSprinklerSystem1 = new ServiceNamed();
        $drainRiserAndReplaceShutOffValveFireSprinklerSystem1->setName('Drain riser and replace shut-off valve');
        $drainRiserAndReplaceShutOffValveFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $drainRiserAndReplaceShutOffValveFireSprinklerSystem1->setDeficiency('Gauge shut-off valve inoperative');
        $drainRiserAndReplaceShutOffValveFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $drainRiserAndReplaceShutOffValveFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($drainRiserAndReplaceShutOffValveFireSprinklerSystem1);
        $this->addReference('drain-riser-and-replace-shut-off-valve-fire-sprinkler-system-1', $drainRiserAndReplaceShutOffValveFireSprinklerSystem1);

        $contractorDrainRiserAndReplaceShutOffValveFireSprinklerSystem1 = new ContractorService();
        $contractorDrainRiserAndReplaceShutOffValveFireSprinklerSystem1->setNamed($this->getReference('drain-riser-and-replace-shut-off-valve-fire-sprinkler-system-1'));
        $contractorDrainRiserAndReplaceShutOffValveFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorDrainRiserAndReplaceShutOffValveFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorDrainRiserAndReplaceShutOffValveFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorDrainRiserAndReplaceShutOffValveFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorDrainRiserAndReplaceShutOffValveFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-drain-riser-and-replace-shut-off-valve-fire-sprinkler-system-1', $contractorDrainRiserAndReplaceShutOffValveFireSprinklerSystem1);


        /** @var ServiceNamed $repairHydrantFireSprinklerSystem1 */
        $repairHydrantFireSprinklerSystem1 = new ServiceNamed();
        $repairHydrantFireSprinklerSystem1->setName('Repair hydrant');
        $repairHydrantFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $repairHydrantFireSprinklerSystem1->setDeficiency('Fire hydrant inoperative');
        $repairHydrantFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $repairHydrantFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($repairHydrantFireSprinklerSystem1);
        $this->addReference('repair-hydrant-fire-sprinkler-system-1', $repairHydrantFireSprinklerSystem1);

        $contractorRepairHydrantFireSprinklerSystem1 = new ContractorService();
        $contractorRepairHydrantFireSprinklerSystem1->setNamed($this->getReference('repair-hydrant-fire-sprinkler-system-1'));
        $contractorRepairHydrantFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorRepairHydrantFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRepairHydrantFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorRepairHydrantFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorRepairHydrantFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-repair-hydrant-fire-sprinkler-system-1', $contractorRepairHydrantFireSprinklerSystem1);


        /** @var ServiceNamed $repairHydrantFireSprinklerSystem2 */
        $repairHydrantFireSprinklerSystem2 = new ServiceNamed();
        $repairHydrantFireSprinklerSystem2->setName('Repair hydrant');
        $repairHydrantFireSprinklerSystem2->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $repairHydrantFireSprinklerSystem2->setDeficiency('Fire hydrant leaking');
        $repairHydrantFireSprinklerSystem2->setIsNeedSendMunicipalityReport(false);
        $repairHydrantFireSprinklerSystem2->setIsNeedMunicipalityFee(false);
        $manager->persist($repairHydrantFireSprinklerSystem2);
        $this->addReference('repair-hydrant-fire-sprinkler-system-2', $repairHydrantFireSprinklerSystem2);

        $contractorRepairHydrantFireSprinklerSystem2 = new ContractorService();
        $contractorRepairHydrantFireSprinklerSystem2->setNamed($this->getReference('repair-hydrant-fire-sprinkler-system-2'));
        $contractorRepairHydrantFireSprinklerSystem2->setContractor($this->getReference('contractor-my-company'));
        $contractorRepairHydrantFireSprinklerSystem2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRepairHydrantFireSprinklerSystem2->setState($this->getReference('state-illinois'));
        $contractorRepairHydrantFireSprinklerSystem2->setEstimationTime('0');

        $manager->persist($contractorRepairHydrantFireSprinklerSystem2);
        $manager->flush();
        $this->addReference('contractorService-repair-hydrant-fire-sprinkler-system-2', $contractorRepairHydrantFireSprinklerSystem2);


        /** @var ServiceNamed $replaceHydrantFireSprinklerSystem1 */
        $replaceHydrantFireSprinklerSystem1 = new ServiceNamed();
        $replaceHydrantFireSprinklerSystem1->setName('Replace hydrant');
        $replaceHydrantFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceHydrantFireSprinklerSystem1->setDeficiency('Fire hydrant damaged');
        $replaceHydrantFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceHydrantFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceHydrantFireSprinklerSystem1);
        $this->addReference('replace-hydrant-fire-sprinkler-system-1', $replaceHydrantFireSprinklerSystem1);

        $contractorReplaceHydrantFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceHydrantFireSprinklerSystem1->setNamed($this->getReference('replace-hydrant-fire-sprinkler-system-1'));
        $contractorReplaceHydrantFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceHydrantFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceHydrantFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceHydrantFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceHydrantFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-hydrant-fire-sprinkler-system-1', $contractorReplaceHydrantFireSprinklerSystem1);


        /** @var ServiceNamed $systemFlushFireSprinklerSystem1 */
        $systemFlushFireSprinklerSystem1 = new ServiceNamed();
        $systemFlushFireSprinklerSystem1->setName('System Flush');
        $systemFlushFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $systemFlushFireSprinklerSystem1->setDeficiency('Excessive Debris in system');
        $systemFlushFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $systemFlushFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($systemFlushFireSprinklerSystem1);
        $this->addReference('system-flush-fire-sprinkler-system-1', $systemFlushFireSprinklerSystem1);

        $contractorSystemFlushFireSprinklerSystem1 = new ContractorService();
        $contractorSystemFlushFireSprinklerSystem1->setNamed($this->getReference('system-flush-fire-sprinkler-system-1'));
        $contractorSystemFlushFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorSystemFlushFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorSystemFlushFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorSystemFlushFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorSystemFlushFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-system-flush-fire-sprinkler-system-1', $contractorSystemFlushFireSprinklerSystem1);


        /** @var ServiceNamed $hydrostaticTestFireSprinklerSystem1 */
        $hydrostaticTestFireSprinklerSystem1 = new ServiceNamed();
        $hydrostaticTestFireSprinklerSystem1->setName('Hydrostatic Test');
        $hydrostaticTestFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $hydrostaticTestFireSprinklerSystem1->setDeficiency('System needs Hydrostatic Testing');
        $hydrostaticTestFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $hydrostaticTestFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($hydrostaticTestFireSprinklerSystem1);
        $this->addReference('hydrostatic-test-fire-sprinkler-system-1', $hydrostaticTestFireSprinklerSystem1);

        $contractorHydrostaticTestFireSprinklerSystem1 = new ContractorService();
        $contractorHydrostaticTestFireSprinklerSystem1->setNamed($this->getReference('hydrostatic-test-fire-sprinkler-system-1'));
        $contractorHydrostaticTestFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorHydrostaticTestFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorHydrostaticTestFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorHydrostaticTestFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorHydrostaticTestFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-hydrostatic-test-fire-sprinkler-system-1', $contractorHydrostaticTestFireSprinklerSystem1);


        /** @var ServiceNamed $rewireTamperSwitchFireSprinklerSystem1 */
        $rewireTamperSwitchFireSprinklerSystem1 = new ServiceNamed();
        $rewireTamperSwitchFireSprinklerSystem1->setName('Rewire tamper switch');
        $rewireTamperSwitchFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $rewireTamperSwitchFireSprinklerSystem1->setDeficiency('Tamper switch not wired correctly');
        $rewireTamperSwitchFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $rewireTamperSwitchFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($rewireTamperSwitchFireSprinklerSystem1);
        $this->addReference('rewire-tamper-switch-fire-sprinkler-system-1', $rewireTamperSwitchFireSprinklerSystem1);

        $contractorRewireTamperSwitchFireSprinklerSystem1 = new ContractorService();
        $contractorRewireTamperSwitchFireSprinklerSystem1->setNamed($this->getReference('rewire-tamper-switch-fire-sprinkler-system-1'));
        $contractorRewireTamperSwitchFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorRewireTamperSwitchFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRewireTamperSwitchFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorRewireTamperSwitchFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorRewireTamperSwitchFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-rewire-tamper-switch-fire-sprinkler-system-1', $contractorRewireTamperSwitchFireSprinklerSystem1);


        /** @var ServiceNamed $replaceTamperSwitchFireSprinklerSystem1 */
        $replaceTamperSwitchFireSprinklerSystem1 = new ServiceNamed();
        $replaceTamperSwitchFireSprinklerSystem1->setName('Replace tamper switch');
        $replaceTamperSwitchFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceTamperSwitchFireSprinklerSystem1->setDeficiency('Tamper switch not working correctly');
        $replaceTamperSwitchFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceTamperSwitchFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceTamperSwitchFireSprinklerSystem1);
        $this->addReference('replace-tamper-switch-fire-sprinkler-system-1', $replaceTamperSwitchFireSprinklerSystem1);

        $contractorReplaceTamperSwitchFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceTamperSwitchFireSprinklerSystem1->setNamed($this->getReference('replace-tamper-switch-fire-sprinkler-system-1'));
        $contractorReplaceTamperSwitchFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceTamperSwitchFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceTamperSwitchFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceTamperSwitchFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceTamperSwitchFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-tamper-switch-fire-sprinkler-system-1', $contractorReplaceTamperSwitchFireSprinklerSystem1);


        /** @var ServiceNamed $installTamperCorrectlyFireSprinklerSystem1 */
        $installTamperCorrectlyFireSprinklerSystem1 = new ServiceNamed();
        $installTamperCorrectlyFireSprinklerSystem1->setName('Install tamper correctly');
        $installTamperCorrectlyFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $installTamperCorrectlyFireSprinklerSystem1->setDeficiency('Tamper switch not installed correctly');
        $installTamperCorrectlyFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $installTamperCorrectlyFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($installTamperCorrectlyFireSprinklerSystem1);
        $this->addReference('install-tamper-correctly-fire-sprinkler-system-1', $installTamperCorrectlyFireSprinklerSystem1);

        $contractorInstallTamperCorrectlyFireSprinklerSystem1 = new ContractorService();
        $contractorInstallTamperCorrectlyFireSprinklerSystem1->setNamed($this->getReference('install-tamper-correctly-fire-sprinkler-system-1'));
        $contractorInstallTamperCorrectlyFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallTamperCorrectlyFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallTamperCorrectlyFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorInstallTamperCorrectlyFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorInstallTamperCorrectlyFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-install-tamper-correctly-fire-sprinkler-system-1', $contractorInstallTamperCorrectlyFireSprinklerSystem1);


        /** @var ServiceNamed $installTamperSwitchFireSprinklerSystem1 */
        $installTamperSwitchFireSprinklerSystem1 = new ServiceNamed();
        $installTamperSwitchFireSprinklerSystem1->setName('Install tamper switch');
        $installTamperSwitchFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $installTamperSwitchFireSprinklerSystem1->setDeficiency('Tamper switch missing');
        $installTamperSwitchFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $installTamperSwitchFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($installTamperSwitchFireSprinklerSystem1);
        $this->addReference('install-tamper-switch-fire-sprinkler-system-1', $installTamperSwitchFireSprinklerSystem1);

        $contractorInstallTamperSwitchFireSprinklerSystem1 = new ContractorService();
        $contractorInstallTamperSwitchFireSprinklerSystem1->setNamed($this->getReference('install-tamper-switch-fire-sprinkler-system-1'));
        $contractorInstallTamperSwitchFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallTamperSwitchFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallTamperSwitchFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorInstallTamperSwitchFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorInstallTamperSwitchFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-install-tamper-switch-fire-sprinkler-system-1', $contractorInstallTamperSwitchFireSprinklerSystem1);


        /** @var ServiceNamed $syncStrobeLightsFireSprinklerSystem1 */
        $syncStrobeLightsFireSprinklerSystem1 = new ServiceNamed();
        $syncStrobeLightsFireSprinklerSystem1->setName('Sync Strobe Lights');
        $syncStrobeLightsFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $syncStrobeLightsFireSprinklerSystem1->setDeficiency('Strobe Lights out of Sync');
        $syncStrobeLightsFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $syncStrobeLightsFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($syncStrobeLightsFireSprinklerSystem1);
        $this->addReference('sync-strobe-lights-fire-sprinkler-system-1', $syncStrobeLightsFireSprinklerSystem1);

        $contractorSyncStrobeLightsFireSprinklerSystem1 = new ContractorService();
        $contractorSyncStrobeLightsFireSprinklerSystem1->setNamed($this->getReference('sync-strobe-lights-fire-sprinkler-system-1'));
        $contractorSyncStrobeLightsFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorSyncStrobeLightsFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorSyncStrobeLightsFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorSyncStrobeLightsFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorSyncStrobeLightsFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-sync-strobe-lights-fire-sprinkler-system-1', $contractorSyncStrobeLightsFireSprinklerSystem1);


        /** @var ServiceNamed $addTamperSwitchesToSystemFireSprinklerSystem1 */
        $addTamperSwitchesToSystemFireSprinklerSystem1 = new ServiceNamed();
        $addTamperSwitchesToSystemFireSprinklerSystem1->setName('Add Tamper Switches to System');
        $addTamperSwitchesToSystemFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $addTamperSwitchesToSystemFireSprinklerSystem1->setDeficiency('Tamper Switches not Connected');
        $addTamperSwitchesToSystemFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $addTamperSwitchesToSystemFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($addTamperSwitchesToSystemFireSprinklerSystem1);
        $this->addReference('add-tamper-switches-to-system-fire-sprinkler-system-1', $addTamperSwitchesToSystemFireSprinklerSystem1);

        $contractorAddTamperSwitchesToSystemFireSprinklerSystem1 = new ContractorService();
        $contractorAddTamperSwitchesToSystemFireSprinklerSystem1->setNamed($this->getReference('add-tamper-switches-to-system-fire-sprinkler-system-1'));
        $contractorAddTamperSwitchesToSystemFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorAddTamperSwitchesToSystemFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorAddTamperSwitchesToSystemFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorAddTamperSwitchesToSystemFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorAddTamperSwitchesToSystemFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-add-tamper-switches-to-system-fire-sprinkler-system-1', $contractorAddTamperSwitchesToSystemFireSprinklerSystem1);


        /** @var ServiceNamed $replaceHornFireSprinklerSystem1 */
        $replaceHornFireSprinklerSystem1 = new ServiceNamed();
        $replaceHornFireSprinklerSystem1->setName('Replace horn');
        $replaceHornFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceHornFireSprinklerSystem1->setDeficiency('Horn not working');
        $replaceHornFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceHornFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceHornFireSprinklerSystem1);
        $this->addReference('replace-horn-fire-sprinkler-system-1', $replaceHornFireSprinklerSystem1);

        $contractorReplaceHornFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceHornFireSprinklerSystem1->setNamed($this->getReference('replace-horn-fire-sprinkler-system-1'));
        $contractorReplaceHornFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceHornFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceHornFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceHornFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceHornFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-horn-fire-sprinkler-system-1', $contractorReplaceHornFireSprinklerSystem1);


        /** @var ServiceNamed $replaceStrobeFireSprinklerSystem1 */
        $replaceStrobeFireSprinklerSystem1 = new ServiceNamed();
        $replaceStrobeFireSprinklerSystem1->setName('Replace strobe');
        $replaceStrobeFireSprinklerSystem1->addDeviceNamed($this->getReference('fire-sprinkler-system-device'));
        $replaceStrobeFireSprinklerSystem1->setDeficiency('Strobe not working');
        $replaceStrobeFireSprinklerSystem1->setIsNeedSendMunicipalityReport(false);
        $replaceStrobeFireSprinklerSystem1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceStrobeFireSprinklerSystem1);
        $this->addReference('replace-strobe-fire-sprinkler-system-1', $replaceStrobeFireSprinklerSystem1);

        $contractorReplaceStrobeFireSprinklerSystem1 = new ContractorService();
        $contractorReplaceStrobeFireSprinklerSystem1->setNamed($this->getReference('replace-strobe-fire-sprinkler-system-1'));
        $contractorReplaceStrobeFireSprinklerSystem1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceStrobeFireSprinklerSystem1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceStrobeFireSprinklerSystem1->setState($this->getReference('state-illinois'));
        $contractorReplaceStrobeFireSprinklerSystem1->setEstimationTime('0');

        $manager->persist($contractorReplaceStrobeFireSprinklerSystem1);
        $manager->flush();
        $this->addReference('contractorService-replace-strobe-fire-sprinkler-system-1', $contractorReplaceStrobeFireSprinklerSystem1);


        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
