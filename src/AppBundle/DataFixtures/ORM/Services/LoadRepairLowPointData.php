<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairLowPointData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairLowPoint1 */
        $generalRepairLowPoint1 = new ServiceNamed();
        $generalRepairLowPoint1->setName('General repair');
        $generalRepairLowPoint1->addDeviceNamed($this->getReference('low-point-device'));
        $generalRepairLowPoint1->setDeficiency('Please see comments');
        $generalRepairLowPoint1->setIsNeedSendMunicipalityReport(false);
        $generalRepairLowPoint1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairLowPoint1);
        $this->addReference('general-repair-low-point-1', $generalRepairLowPoint1);

        $contractorGeneralRepairLowPoint1 = new ContractorService();
        $contractorGeneralRepairLowPoint1->setNamed($this->getReference('general-repair-low-point-1'));
        $contractorGeneralRepairLowPoint1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairLowPoint1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairLowPoint1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairLowPoint1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairLowPoint1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-low-point-1', $contractorGeneralRepairLowPoint1);



        /** @var ServiceNamed $generalRepairLowPoint2 */
        $generalRepairLowPoint2 = new ServiceNamed();
        $generalRepairLowPoint2->setName('General repair');
        $generalRepairLowPoint2->addDeviceNamed($this->getReference('low-point-device'));
        $generalRepairLowPoint2->setDeficiency('Unidentified issue');
        $generalRepairLowPoint2->setIsNeedSendMunicipalityReport(false);
        $generalRepairLowPoint2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairLowPoint2);
        $this->addReference('general-repair-low-point-2', $generalRepairLowPoint2);

        $contractorGeneralRepairLowPoint2 = new ContractorService();
        $contractorGeneralRepairLowPoint2->setNamed($this->getReference('general-repair-low-point-2'));
        $contractorGeneralRepairLowPoint2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairLowPoint2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairLowPoint2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairLowPoint2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairLowPoint2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-low-point-2', $contractorGeneralRepairLowPoint2);



        /** @var ServiceNamed $equipmentInstallationLowPoint1 */
        $equipmentInstallationLowPoint1 = new ServiceNamed();
        $equipmentInstallationLowPoint1->setName('Equipment installation');
        $equipmentInstallationLowPoint1->addDeviceNamed($this->getReference('low-point-device'));
        $equipmentInstallationLowPoint1->setDeficiency('Please see comments');
        $equipmentInstallationLowPoint1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationLowPoint1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationLowPoint1);
        $this->addReference('equipment-installation-low-point-1', $equipmentInstallationLowPoint1);

        $contractorEquipmentInstallationLowPoint1 = new ContractorService();
        $contractorEquipmentInstallationLowPoint1->setNamed($this->getReference('equipment-installation-low-point-1'));
        $contractorEquipmentInstallationLowPoint1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationLowPoint1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationLowPoint1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationLowPoint1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationLowPoint1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-low-point-1', $contractorEquipmentInstallationLowPoint1);



        /** @var ServiceNamed $inspectionNoticeToBeSentLowPoint1 */
        $inspectionNoticeToBeSentLowPoint1 = new ServiceNamed();
        $inspectionNoticeToBeSentLowPoint1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentLowPoint1->addDeviceNamed($this->getReference('low-point-device'));
        $inspectionNoticeToBeSentLowPoint1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentLowPoint1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentLowPoint1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentLowPoint1);
        $this->addReference('inspection-notice-to-be-sent-low-point-1', $inspectionNoticeToBeSentLowPoint1);

        $contractorInspectionNoticeToBeSentLowPoint1 = new ContractorService();
        $contractorInspectionNoticeToBeSentLowPoint1->setNamed($this->getReference('inspection-notice-to-be-sent-low-point-1'));
        $contractorInspectionNoticeToBeSentLowPoint1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentLowPoint1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentLowPoint1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentLowPoint1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentLowPoint1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-low-point-1', $contractorInspectionNoticeToBeSentLowPoint1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
