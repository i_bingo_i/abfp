<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairAirCompressorData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairAirCompressor1 */
        $generalRepairAirCompressor1 = new ServiceNamed();
        $generalRepairAirCompressor1->setName('General repair');
        $generalRepairAirCompressor1->addDeviceNamed($this->getReference('air-compressor-device'));
        $generalRepairAirCompressor1->setDeficiency('Please see comments');
        $generalRepairAirCompressor1->setIsNeedSendMunicipalityReport(false);
        $generalRepairAirCompressor1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairAirCompressor1);
        $this->addReference('general-repair-air-compressor-1', $generalRepairAirCompressor1);

        $contractorServiceGeneralRepairAirCompressor1 = new ContractorService();
        $contractorServiceGeneralRepairAirCompressor1->setNamed($this->getReference('general-repair-air-compressor-1'));
        $contractorServiceGeneralRepairAirCompressor1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceGeneralRepairAirCompressor1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceGeneralRepairAirCompressor1->setState($this->getReference('state-illinois'));
        $contractorServiceGeneralRepairAirCompressor1->setEstimationTime('0');

        $manager->persist($contractorServiceGeneralRepairAirCompressor1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-air-compressor-1', $contractorServiceGeneralRepairAirCompressor1);



        /** @var ServiceNamed $generalRepairAirCompressor2 */
        $generalRepairAirCompressor2 = new ServiceNamed();
        $generalRepairAirCompressor2->setName('General repair');
        $generalRepairAirCompressor2->addDeviceNamed($this->getReference('air-compressor-device'));
        $generalRepairAirCompressor2->setDeficiency('Unidentified issue');
        $generalRepairAirCompressor2->setIsNeedSendMunicipalityReport(false);
        $generalRepairAirCompressor2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairAirCompressor2);
        $this->addReference('general-repair-air-compressor-2', $generalRepairAirCompressor2);

        $contractorServiceGeneralRepairAirCompressor2 = new ContractorService();
        $contractorServiceGeneralRepairAirCompressor2->setNamed($this->getReference('general-repair-air-compressor-2'));
        $contractorServiceGeneralRepairAirCompressor2->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceGeneralRepairAirCompressor2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceGeneralRepairAirCompressor2->setState($this->getReference('state-illinois'));
        $contractorServiceGeneralRepairAirCompressor2->setEstimationTime('0');

        $manager->persist($contractorServiceGeneralRepairAirCompressor2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-air-compressor-2', $contractorServiceGeneralRepairAirCompressor2);



        /** @var ServiceNamed $equipmentInstallationAirCompressor1 */
        $equipmentInstallationAirCompressor1 = new ServiceNamed();
        $equipmentInstallationAirCompressor1->setName('Equipment installation');
        $equipmentInstallationAirCompressor1->addDeviceNamed($this->getReference('air-compressor-device'));
        $equipmentInstallationAirCompressor1->setDeficiency('Please see comments');
        $equipmentInstallationAirCompressor1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationAirCompressor1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationAirCompressor1);
        $this->addReference('equipment-installation-air-compressor-1', $equipmentInstallationAirCompressor1);

        $contractorServiceEquipmentInstallationAirCompressor1 = new ContractorService();
        $contractorServiceEquipmentInstallationAirCompressor1->setNamed($this->getReference('equipment-installation-air-compressor-1'));
        $contractorServiceEquipmentInstallationAirCompressor1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceEquipmentInstallationAirCompressor1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceEquipmentInstallationAirCompressor1->setState($this->getReference('state-illinois'));
        $contractorServiceEquipmentInstallationAirCompressor1->setEstimationTime('0');

        $manager->persist($contractorServiceEquipmentInstallationAirCompressor1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-air-compressor-1', $contractorServiceEquipmentInstallationAirCompressor1);



        /** @var ServiceNamed $inspectionNoticeToBeSentAirCompressor1 */
        $inspectionNoticeToBeSentAirCompressor1 = new ServiceNamed();
        $inspectionNoticeToBeSentAirCompressor1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentAirCompressor1->addDeviceNamed($this->getReference('air-compressor-device'));
        $inspectionNoticeToBeSentAirCompressor1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentAirCompressor1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentAirCompressor1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentAirCompressor1);
        $this->addReference('inspection-notice-to-be-sent-air-compressor-1', $inspectionNoticeToBeSentAirCompressor1);

        $contractorServiceInspectionNoticeToBeSentAirCompressor1 = new ContractorService();
        $contractorServiceInspectionNoticeToBeSentAirCompressor1->setNamed($this->getReference('inspection-notice-to-be-sent-air-compressor-1'));
        $contractorServiceInspectionNoticeToBeSentAirCompressor1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceInspectionNoticeToBeSentAirCompressor1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceInspectionNoticeToBeSentAirCompressor1->setState($this->getReference('state-illinois'));
        $contractorServiceInspectionNoticeToBeSentAirCompressor1->setEstimationTime('0');

        $manager->persist($contractorServiceInspectionNoticeToBeSentAirCompressor1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-air-compressor-1', $contractorServiceInspectionNoticeToBeSentAirCompressor1);


        /** @var ServiceNamed $replaceAirCompressorAirCompressor1 */
        $replaceAirCompressorAirCompressor1 = new ServiceNamed();
        $replaceAirCompressorAirCompressor1->setName('Replace air compressor');
        $replaceAirCompressorAirCompressor1->addDeviceNamed($this->getReference('air-compressor-device'));
        $replaceAirCompressorAirCompressor1->setDeficiency('Air compressor inoperative');
        $replaceAirCompressorAirCompressor1->setIsNeedSendMunicipalityReport(false);
        $replaceAirCompressorAirCompressor1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceAirCompressorAirCompressor1);
        $this->addReference('replace-air-compressor-air-compressor-1', $replaceAirCompressorAirCompressor1);

        $contractorReplaceAirCompressorAirCompressor1 = new ContractorService();
        $contractorReplaceAirCompressorAirCompressor1->setNamed($this->getReference('replace-air-compressor-air-compressor-1'));
        $contractorReplaceAirCompressorAirCompressor1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceAirCompressorAirCompressor1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceAirCompressorAirCompressor1->setState($this->getReference('state-illinois'));
        $contractorReplaceAirCompressorAirCompressor1->setEstimationTime('0');

        $manager->persist($contractorReplaceAirCompressorAirCompressor1);
        $manager->flush();
        $this->addReference('contractorService-replace-air-compressor-air-compressor-1', $contractorReplaceAirCompressorAirCompressor1);


        /** @var ServiceNamed $replaceCheckValveAirCompressor1 */
        $replaceCheckValveAirCompressor1 = new ServiceNamed();
        $replaceCheckValveAirCompressor1->setName('Replace check valve');
        $replaceCheckValveAirCompressor1->addDeviceNamed($this->getReference('air-compressor-device'));
        $replaceCheckValveAirCompressor1->setDeficiency('Air check valve leaking back');
        $replaceCheckValveAirCompressor1->setIsNeedSendMunicipalityReport(false);
        $replaceCheckValveAirCompressor1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceCheckValveAirCompressor1);
        $this->addReference('replace-check-valve-air-compressor-1', $replaceCheckValveAirCompressor1);

        $contractorReplaceCheckValveAirCompressor1 = new ContractorService();
        $contractorReplaceCheckValveAirCompressor1->setNamed($this->getReference('replace-check-valve-air-compressor-1'));
        $contractorReplaceCheckValveAirCompressor1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceCheckValveAirCompressor1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceCheckValveAirCompressor1->setState($this->getReference('state-illinois'));
        $contractorReplaceCheckValveAirCompressor1->setEstimationTime('0');

        $manager->persist($contractorReplaceCheckValveAirCompressor1);
        $manager->flush();
        $this->addReference('contractorService-replace-check-valve-air-compressor-1', $contractorReplaceCheckValveAirCompressor1);



        /** @var ServiceNamed $relocateAirCompressorAirCompressor1 */
        $relocateAirCompressorAirCompressor1 = new ServiceNamed();
        $relocateAirCompressorAirCompressor1->setName('Relocate air compressor');
        $relocateAirCompressorAirCompressor1->addDeviceNamed($this->getReference('air-compressor-device'));
        $relocateAirCompressorAirCompressor1->setDeficiency('Air compressor needs relocation');
        $relocateAirCompressorAirCompressor1->setIsNeedSendMunicipalityReport(false);
        $relocateAirCompressorAirCompressor1->setIsNeedMunicipalityFee(false);
        $manager->persist($relocateAirCompressorAirCompressor1);
        $this->addReference('relocate-air-compressor-air-compressor-1', $relocateAirCompressorAirCompressor1);

        $contractorRelocateAirCompressorAirCompressor1 = new ContractorService();
        $contractorRelocateAirCompressorAirCompressor1->setNamed($this->getReference('relocate-air-compressor-air-compressor-1'));
        $contractorRelocateAirCompressorAirCompressor1->setContractor($this->getReference('contractor-my-company'));
        $contractorRelocateAirCompressorAirCompressor1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRelocateAirCompressorAirCompressor1->setState($this->getReference('state-illinois'));
        $contractorRelocateAirCompressorAirCompressor1->setEstimationTime('0');

        $manager->persist($contractorRelocateAirCompressorAirCompressor1);
        $manager->flush();
        $this->addReference('contractorService-relocate-air-compressor-air-compressor-1', $contractorRelocateAirCompressorAirCompressor1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
