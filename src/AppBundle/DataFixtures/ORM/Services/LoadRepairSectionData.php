<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairSectionData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairSection1 */
        $generalRepairSection1 = new ServiceNamed();
        $generalRepairSection1->setName('General repair');
        $generalRepairSection1->addDeviceNamed($this->getReference('section-device'));
        $generalRepairSection1->setDeficiency('Please see comments');
        $generalRepairSection1->setIsNeedSendMunicipalityReport(false);
        $generalRepairSection1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairSection1);
        $this->addReference('general-repair-section-1', $generalRepairSection1);

        $contractorGeneralRepairSection1 = new ContractorService();
        $contractorGeneralRepairSection1->setNamed($this->getReference('general-repair-section-1'));
        $contractorGeneralRepairSection1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairSection1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairSection1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairSection1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairSection1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-section-1', $contractorGeneralRepairSection1);



        /** @var ServiceNamed $generalRepairSection2 */
        $generalRepairSection2 = new ServiceNamed();
        $generalRepairSection2->setName('General repair');
        $generalRepairSection2->addDeviceNamed($this->getReference('section-device'));
        $generalRepairSection2->setDeficiency('Unidentified issue');
        $generalRepairSection2->setIsNeedSendMunicipalityReport(false);
        $generalRepairSection2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairSection2);
        $this->addReference('general-repair-section-2', $generalRepairSection2);

        $contractorGeneralRepairSection2 = new ContractorService();
        $contractorGeneralRepairSection2->setNamed($this->getReference('general-repair-section-2'));
        $contractorGeneralRepairSection2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairSection2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairSection2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairSection2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairSection2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-section-2', $contractorGeneralRepairSection2);



        /** @var ServiceNamed $equipmentInstallationSection1 */
        $equipmentInstallationSection1 = new ServiceNamed();
        $equipmentInstallationSection1->setName('Equipment installation');
        $equipmentInstallationSection1->addDeviceNamed($this->getReference('section-device'));
        $equipmentInstallationSection1->setDeficiency('Please see comments');
        $equipmentInstallationSection1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationSection1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationSection1);
        $this->addReference('equipment-installation-section-1', $equipmentInstallationSection1);

        $contractorEquipmentInstallationSection1 = new ContractorService();
        $contractorEquipmentInstallationSection1->setNamed($this->getReference('equipment-installation-section-1'));
        $contractorEquipmentInstallationSection1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationSection1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationSection1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationSection1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationSection1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-section-1', $contractorEquipmentInstallationSection1);



        /** @var ServiceNamed $inspectionNoticeToBeSentSection1 */
        $inspectionNoticeToBeSentSection1 = new ServiceNamed();
        $inspectionNoticeToBeSentSection1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentSection1->addDeviceNamed($this->getReference('section-device'));
        $inspectionNoticeToBeSentSection1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentSection1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentSection1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentSection1);
        $this->addReference('inspection-notice-to-be-sent-section-1', $inspectionNoticeToBeSentSection1);

        $contractorInspectionNoticeToBeSentSection1 = new ContractorService();
        $contractorInspectionNoticeToBeSentSection1->setNamed($this->getReference('inspection-notice-to-be-sent-section-1'));
        $contractorInspectionNoticeToBeSentSection1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentSection1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentSection1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentSection1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentSection1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-section-1', $contractorInspectionNoticeToBeSentSection1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
