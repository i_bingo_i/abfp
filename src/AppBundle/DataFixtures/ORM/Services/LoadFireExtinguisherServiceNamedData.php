<?php

namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadFireExtinguisherServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //6 Year Hydrotest Inspection service and Contractor Service
        /** @var ServiceNamed $fire6YearHydrotest */
        $fire6YearHydrotest = new ServiceNamed();
        $fire6YearHydrotest->setName('6 Year Hydrotest');
        $fire6YearHydrotest->setFrequency($this->getReference('service-frequency-6-years'));
        $fire6YearHydrotest->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $fire6YearHydrotest->addDeviceNamed($this->getReference('fire-root-extinguishers-device'));
        $fire6YearHydrotest->setIsNeedSendMunicipalityReport(true);
        $fire6YearHydrotest->setIsNeedMunicipalityFee(false);
        $manager->persist($fire6YearHydrotest);
        $this->addReference('fire-6-year-hydrotest', $fire6YearHydrotest);

        /** @var ContractorService $contractorServiceFire6YearHydrotest1 */
        $contractorServiceFire6YearHydrotest1 = new ContractorService();
        $contractorServiceFire6YearHydrotest1->setNamed($this->getReference('fire-6-year-hydrotest'));
        $contractorServiceFire6YearHydrotest1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceFire6YearHydrotest1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceFire6YearHydrotest1->setState($this->getReference('state-illinois'));
        $contractorServiceFire6YearHydrotest1->setFixedPrice(30.00);
        $contractorServiceFire6YearHydrotest1->setHoursPrice(30.00);
        $contractorServiceFire6YearHydrotest1->setReference('NFPA 10');
        $contractorServiceFire6YearHydrotest1->setEstimationTime('0');

        $manager->persist($contractorServiceFire6YearHydrotest1);
        $manager->flush();
        $this->addReference('contractorService-fire-6-year-hydrotest-1', $contractorServiceFire6YearHydrotest1);

        //end 6 Year Hydrotest Inspection service and Contractor Service

        //12 Year Hydrotest Inspection service and Contractor Service
        /** @var ServiceNamed $fire12YearHydrotest */
        $fire12YearHydrotest = new ServiceNamed();
        $fire12YearHydrotest->setName('12 Year Hydrotest');
        $fire12YearHydrotest->setFrequency($this->getReference('service-frequency-12-years'));
        $fire12YearHydrotest->addDeviceNamed($this->getReference('fire-extinguisher-device'));
        $fire12YearHydrotest->addDeviceNamed($this->getReference('fire-root-extinguishers-device'));
        $fire12YearHydrotest->setIsNeedSendMunicipalityReport(true);
        $fire12YearHydrotest->setIsNeedMunicipalityFee(false);
        $manager->persist($fire12YearHydrotest);
        $this->addReference('fire-12-year-hydrotest', $fire12YearHydrotest);

        /** @var ContractorService $contractorServiceFire12YearHydrotest1 */
        $contractorServiceFire12YearHydrotest1 = new ContractorService();
        $contractorServiceFire12YearHydrotest1->setNamed($this->getReference('fire-12-year-hydrotest'));
        $contractorServiceFire12YearHydrotest1->setContractor($this->getReference('contractor-my-company'));
        $contractorServiceFire12YearHydrotest1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorServiceFire12YearHydrotest1->setState($this->getReference('state-illinois'));
        $contractorServiceFire12YearHydrotest1->setFixedPrice(30.00);
        $contractorServiceFire12YearHydrotest1->setHoursPrice(30.00);
        $contractorServiceFire12YearHydrotest1->setReference('NFPA 10');
        $contractorServiceFire12YearHydrotest1->setEstimationTime('0');

        $manager->persist($contractorServiceFire12YearHydrotest1);
        $manager->flush();
        $this->addReference('contractorService-fire-12-year-hydrotest-1', $contractorServiceFire12YearHydrotest1);
        //end 12 Year Hydrotest Inspection service and Contractor Service

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
