<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairWaterHeaterData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $waterHeaterNonFrequency1 */
        $waterHeaterNonFrequency1 = new ServiceNamed();
        $waterHeaterNonFrequency1->setName('Water Heater non-frequency service 1');
        $waterHeaterNonFrequency1->setDeficiency('Water Heater deficiency 1');
        $waterHeaterNonFrequency1->addDeviceNamed($this->getReference('water-heater-device'));
        $waterHeaterNonFrequency1->setIsNeedMunicipalityFee(true);
        $waterHeaterNonFrequency1->setIsNeedSendMunicipalityReport(true);
        $manager->persist($waterHeaterNonFrequency1);
        $this->addReference('water-heater-non-frequency-service-1', $waterHeaterNonFrequency1);

        $contractorWaterHeaterNonFrequency1 = new ContractorService();
        $contractorWaterHeaterNonFrequency1->setNamed($this->getReference('water-heater-non-frequency-service-1'));
        $contractorWaterHeaterNonFrequency1->setContractor($this->getReference('contractor-my-company'));
        $contractorWaterHeaterNonFrequency1->setDeviceCategory($this->getReference('device-category-plumbing'));
        $contractorWaterHeaterNonFrequency1->setState($this->getReference('state-illinois'));
        $contractorWaterHeaterNonFrequency1->setEstimationTime('0');

        $manager->persist($contractorWaterHeaterNonFrequency1);
        $manager->flush();
        $this->addReference('contractorService-water-heater-non-frequency-service-1', $contractorWaterHeaterNonFrequency1);



        /** @var ServiceNamed $waterHeaterNonFrequency2 */
        $waterHeaterNonFrequency2 = new ServiceNamed();
        $waterHeaterNonFrequency2->setName('Water Heater non-frequency service 2');
        $waterHeaterNonFrequency2->setDeficiency('Water Heater deficiency 2');
        $waterHeaterNonFrequency2->addDeviceNamed($this->getReference('water-heater-device'));
        $waterHeaterNonFrequency2->setIsNeedMunicipalityFee(true);
        $waterHeaterNonFrequency2->setIsNeedSendMunicipalityReport(true);
        $manager->persist($waterHeaterNonFrequency2);
        $this->addReference('water-heater-non-frequency-service-2', $waterHeaterNonFrequency2);

        $contractorWaterHeaterNonFrequency2 = new ContractorService();
        $contractorWaterHeaterNonFrequency2->setNamed($this->getReference('water-heater-non-frequency-service-2'));
        $contractorWaterHeaterNonFrequency2->setContractor($this->getReference('contractor-my-company'));
        $contractorWaterHeaterNonFrequency2->setDeviceCategory($this->getReference('device-category-plumbing'));
        $contractorWaterHeaterNonFrequency2->setState($this->getReference('state-illinois'));
        $contractorWaterHeaterNonFrequency2->setEstimationTime('0');

        $manager->persist($contractorWaterHeaterNonFrequency2);
        $manager->flush();
        $this->addReference('contractorService-water-heater-non-frequency-service-2', $contractorWaterHeaterNonFrequency2);



        /** @var ServiceNamed $waterHeaterNonFrequency3 */
        $waterHeaterNonFrequency3 = new ServiceNamed();
        $waterHeaterNonFrequency3->setName('Water Heater non-frequency service 3');
        $waterHeaterNonFrequency3->setDeficiency('Water Heater deficiency 3');
        $waterHeaterNonFrequency3->addDeviceNamed($this->getReference('water-heater-device'));
        $waterHeaterNonFrequency3->setIsNeedMunicipalityFee(true);
        $waterHeaterNonFrequency3->setIsNeedSendMunicipalityReport(true);
        $manager->persist($waterHeaterNonFrequency3);
        $this->addReference('water-heater-non-frequency-service-3', $waterHeaterNonFrequency3);

        $contractorWaterHeaterNonFrequency3 = new ContractorService();
        $contractorWaterHeaterNonFrequency3->setNamed($this->getReference('water-heater-non-frequency-service-3'));
        $contractorWaterHeaterNonFrequency3->setContractor($this->getReference('contractor-my-company'));
        $contractorWaterHeaterNonFrequency3->setDeviceCategory($this->getReference('device-category-plumbing'));
        $contractorWaterHeaterNonFrequency3->setState($this->getReference('state-illinois'));
        $contractorWaterHeaterNonFrequency3->setEstimationTime('0');

        $manager->persist($contractorWaterHeaterNonFrequency3);
        $manager->flush();
        $this->addReference('contractorService-water-heater-non-frequency-service-3', $contractorWaterHeaterNonFrequency3);


        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
