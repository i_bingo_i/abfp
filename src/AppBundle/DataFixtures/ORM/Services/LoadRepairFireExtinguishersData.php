<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairFireExtinguishersData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairFireExtinguishers1 */
        $generalRepairFireExtinguishers1 = new ServiceNamed();
        $generalRepairFireExtinguishers1->setName('General repair');
        $generalRepairFireExtinguishers1->addDeviceNamed($this->getReference('fire-root-extinguishers-device'));
        $generalRepairFireExtinguishers1->setDeficiency('Please see comments');
        $generalRepairFireExtinguishers1->setIsNeedSendMunicipalityReport(false);
        $generalRepairFireExtinguishers1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFireExtinguishers1);
        $this->addReference('general-repair-fire-extinguishers-1', $generalRepairFireExtinguishers1);

        $contractorGeneralRepairFireExtinguishers1 = new ContractorService();
        $contractorGeneralRepairFireExtinguishers1->setNamed($this->getReference('general-repair-fire-extinguishers-1'));
        $contractorGeneralRepairFireExtinguishers1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFireExtinguishers1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFireExtinguishers1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFireExtinguishers1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFireExtinguishers1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-extinguishers-1', $contractorGeneralRepairFireExtinguishers1);



        /** @var ServiceNamed $generalRepairFireExtinguishers2 */
        $generalRepairFireExtinguishers2 = new ServiceNamed();
        $generalRepairFireExtinguishers2->setName('General repair');
        $generalRepairFireExtinguishers2->addDeviceNamed($this->getReference('fire-root-extinguishers-device'));
        $generalRepairFireExtinguishers2->setDeficiency('Unidentified issue');
        $generalRepairFireExtinguishers2->setIsNeedSendMunicipalityReport(false);
        $generalRepairFireExtinguishers2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFireExtinguishers2);
        $this->addReference('general-repair-fire-extinguishers-2', $generalRepairFireExtinguishers2);

        $contractorGeneralRepairFireExtinguishers2 = new ContractorService();
        $contractorGeneralRepairFireExtinguishers2->setNamed($this->getReference('general-repair-fire-extinguishers-2'));
        $contractorGeneralRepairFireExtinguishers2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFireExtinguishers2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFireExtinguishers2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFireExtinguishers2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFireExtinguishers2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-extinguishers-2', $contractorGeneralRepairFireExtinguishers2);



        /** @var ServiceNamed $equipmentInstallationFireExtinguishers1 */
        $equipmentInstallationFireExtinguishers1 = new ServiceNamed();
        $equipmentInstallationFireExtinguishers1->setName('Equipment installation');
        $equipmentInstallationFireExtinguishers1->addDeviceNamed($this->getReference('fire-root-extinguishers-device'));
        $equipmentInstallationFireExtinguishers1->setDeficiency('Please see comments');
        $equipmentInstallationFireExtinguishers1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationFireExtinguishers1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationFireExtinguishers1);
        $this->addReference('equipment-installation-fire-extinguishers-1', $equipmentInstallationFireExtinguishers1);

        $contractorEquipmentInstallationFireExtinguishers1 = new ContractorService();
        $contractorEquipmentInstallationFireExtinguishers1->setNamed($this->getReference('equipment-installation-fire-extinguishers-1'));
        $contractorEquipmentInstallationFireExtinguishers1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationFireExtinguishers1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationFireExtinguishers1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationFireExtinguishers1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationFireExtinguishers1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-fire-extinguishers-1', $contractorEquipmentInstallationFireExtinguishers1);



        /** @var ServiceNamed $inspectionNoticeToBeSentFireExtinguishers1 */
        $inspectionNoticeToBeSentFireExtinguishers1 = new ServiceNamed();
        $inspectionNoticeToBeSentFireExtinguishers1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentFireExtinguishers1->addDeviceNamed($this->getReference('fire-root-extinguishers-device'));
        $inspectionNoticeToBeSentFireExtinguishers1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentFireExtinguishers1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentFireExtinguishers1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentFireExtinguishers1);
        $this->addReference('inspection-notice-to-be-sent-fire-extinguishers-1', $inspectionNoticeToBeSentFireExtinguishers1);

        $contractorInspectionNoticeToBeSentFireExtinguishers1 = new ContractorService();
        $contractorInspectionNoticeToBeSentFireExtinguishers1->setNamed($this->getReference('inspection-notice-to-be-sent-fire-extinguishers-1'));
        $contractorInspectionNoticeToBeSentFireExtinguishers1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentFireExtinguishers1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentFireExtinguishers1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentFireExtinguishers1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentFireExtinguishers1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-fire-extinguishers-1', $contractorInspectionNoticeToBeSentFireExtinguishers1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
