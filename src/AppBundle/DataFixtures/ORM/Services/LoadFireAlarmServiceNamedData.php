<?php

namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadFireAlarmServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //Annual Fire Alarm System Inspection service and Contractor Service
        /** @var ServiceNamed $annualFireAlarmSystemInspection */
        $annualFireAlarmSystemInspection = new ServiceNamed();
        $annualFireAlarmSystemInspection->setName('Annual Fire Alarm System Inspection');
        $annualFireAlarmSystemInspection->setFrequency($this->getReference('service-frequency-1-year'));
        $annualFireAlarmSystemInspection->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $annualFireAlarmSystemInspection->setIsNeedSendMunicipalityReport(true);
        $annualFireAlarmSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($annualFireAlarmSystemInspection);
        $this->addReference('annual-fire-alarm-system-inspection', $annualFireAlarmSystemInspection);

        /** @var ContractorService $csAnnualFireAlarmSystemInspection */
        $csAnnualFireAlarmSystemInspection = new ContractorService();
        $csAnnualFireAlarmSystemInspection->setNamed($this->getReference('annual-fire-alarm-system-inspection'));
        $csAnnualFireAlarmSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csAnnualFireAlarmSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualFireAlarmSystemInspection->setState($this->getReference('state-illinois'));
        $csAnnualFireAlarmSystemInspection->setFixedPrice(300.00);
        $csAnnualFireAlarmSystemInspection->setHoursPrice(300.00);
        $csAnnualFireAlarmSystemInspection->setReference('NFPA 72');
        $csAnnualFireAlarmSystemInspection->setEstimationTime('3');

        $manager->persist($csAnnualFireAlarmSystemInspection);
        $manager->flush();
        $this->addReference('cs-annual-fire-alarm-system-inspection', $csAnnualFireAlarmSystemInspection);

        //end Annual Fire Alarm System Inspection service and Contractor Service

        //Bi-Annual Fire Alarm System Inspection service and Contractor Service
        /** @var ServiceNamed $biAnnualFireAlarmSystemInspection */
        $biAnnualFireAlarmSystemInspection = new ServiceNamed();
        $biAnnualFireAlarmSystemInspection->setName('Bi-Annual Fire Alarm System Inspection');
        $biAnnualFireAlarmSystemInspection->setFrequency($this->getReference('service-frequency-6-months'));
        $biAnnualFireAlarmSystemInspection->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $biAnnualFireAlarmSystemInspection->setIsNeedSendMunicipalityReport(true);
        $biAnnualFireAlarmSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($biAnnualFireAlarmSystemInspection);
        $this->addReference('bi-annual-fire-alarm-system-inspection', $biAnnualFireAlarmSystemInspection);

        /** @var ContractorService $csBiAnnualFireAlarmSystemInspection */
        $csBiAnnualFireAlarmSystemInspection = new ContractorService();
        $csBiAnnualFireAlarmSystemInspection->setNamed($this->getReference('bi-annual-fire-alarm-system-inspection'));
        $csBiAnnualFireAlarmSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csBiAnnualFireAlarmSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csBiAnnualFireAlarmSystemInspection->setState($this->getReference('state-illinois'));
        $csBiAnnualFireAlarmSystemInspection->setFixedPrice(225.00);
        $csBiAnnualFireAlarmSystemInspection->setHoursPrice(225.00);
        $csBiAnnualFireAlarmSystemInspection->setReference('NFPA 72');
        $csBiAnnualFireAlarmSystemInspection->setEstimationTime('3');

        $manager->persist($csBiAnnualFireAlarmSystemInspection);
        $manager->flush();
        $this->addReference('cs-bi-annual-fire-alarm-system-inspection', $csBiAnnualFireAlarmSystemInspection);

        //end Bi-Annual Fire Alarm System Inspection service and Contractor Service

        //Quarterly Fire Alarm System Inspection service and Contractor Service
        /** @var ServiceNamed $quarterlyFireAlarmSystemInspection */
        $quarterlyFireAlarmSystemInspection = new ServiceNamed();
        $quarterlyFireAlarmSystemInspection->setName('Quarterly Fire Alarm System Inspection');
        $quarterlyFireAlarmSystemInspection->setFrequency($this->getReference('service-frequency-3-months'));
        $quarterlyFireAlarmSystemInspection->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $quarterlyFireAlarmSystemInspection->setIsNeedSendMunicipalityReport(true);
        $quarterlyFireAlarmSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($quarterlyFireAlarmSystemInspection);
        $this->addReference('quarterly-fire-alarm-system-inspection', $quarterlyFireAlarmSystemInspection);

        /** @var ContractorService $csQuarterlyFireAlarmSystemInspection */
        $csQuarterlyFireAlarmSystemInspection = new ContractorService();
        $csQuarterlyFireAlarmSystemInspection->setNamed($this->getReference('quarterly-fire-alarm-system-inspection'));
        $csQuarterlyFireAlarmSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csQuarterlyFireAlarmSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csQuarterlyFireAlarmSystemInspection->setState($this->getReference('state-illinois'));
        $csQuarterlyFireAlarmSystemInspection->setFixedPrice(200.00);
        $csQuarterlyFireAlarmSystemInspection->setHoursPrice(200.00);
        $csQuarterlyFireAlarmSystemInspection->setReference('NFPA 72');
        $csQuarterlyFireAlarmSystemInspection->setEstimationTime('2');

        $manager->persist($csQuarterlyFireAlarmSystemInspection);
        $manager->flush();
        $this->addReference('cs-quarterly-fire-alarm-system-inspection', $csQuarterlyFireAlarmSystemInspection);

        //end Quarterly Fire Alarm System Inspection service and Contractor Service

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
