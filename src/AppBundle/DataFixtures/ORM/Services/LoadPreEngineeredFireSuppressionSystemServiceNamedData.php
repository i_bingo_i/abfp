<?php
namespace AppBundle\DataFixtures\ORM\Services;

use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ContractorService;

class LoadPreEngineeredFireSuppressionSystemServiceNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Annual Pre-Engineered System Inspection service and Contractor Service
        /** @var ServiceNamed $annualPreEngineeredSystemInspection */
        $annualPreEngineeredSystemInspection = new ServiceNamed();
        $annualPreEngineeredSystemInspection->setName('Annual Pre-Engineered System Inspection');
        $annualPreEngineeredSystemInspection->setFrequency($this->getReference('service-frequency-1-year'));
        $annualPreEngineeredSystemInspection->addDeviceNamed($this->getReference('pre_engineered_fire_suppression_system_device'));
        $annualPreEngineeredSystemInspection->setIsNeedSendMunicipalityReport(true);
        $annualPreEngineeredSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($annualPreEngineeredSystemInspection);
        $this->addReference('annual-pre-engineered-system-inspection', $annualPreEngineeredSystemInspection);

        /** @var ContractorService $csAnnualPreEngineeredSystemInspection */
        $csAnnualPreEngineeredSystemInspection = new ContractorService();
        $csAnnualPreEngineeredSystemInspection->setNamed($this->getReference('annual-pre-engineered-system-inspection'));
        $csAnnualPreEngineeredSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $csAnnualPreEngineeredSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $csAnnualPreEngineeredSystemInspection->setState($this->getReference('state-illinois'));
        $csAnnualPreEngineeredSystemInspection->setFixedPrice(95.00);
        $csAnnualPreEngineeredSystemInspection->setHoursPrice(95.00);
        $csAnnualPreEngineeredSystemInspection->setReference('NFPA 96');
        $csAnnualPreEngineeredSystemInspection->setEstimationTime('1');

        $manager->persist($csAnnualPreEngineeredSystemInspection);
        $manager->flush();
        $this->addReference('cs-annual-pre-engineered-system-inspection', $csAnnualPreEngineeredSystemInspection);
        // end Annual Pre-Engineered System Inspection service and Contractor Service

        // Bi-Annual Pre-Engineered System Inspection service and Contractor Service
        /** @var ServiceNamed $biAnnualPreEngineeredSystemInspection */
        $biAnnualPreEngineeredSystemInspection = new ServiceNamed();
        $biAnnualPreEngineeredSystemInspection->setName('Bi-Annual Pre-Engineered System Inspection');
        $biAnnualPreEngineeredSystemInspection->setFrequency($this->getReference('service-frequency-6-months'));
        $biAnnualPreEngineeredSystemInspection->addDeviceNamed($this->getReference('pre_engineered_fire_suppression_system_device'));
        $biAnnualPreEngineeredSystemInspection->setIsNeedSendMunicipalityReport(true);
        $biAnnualPreEngineeredSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($biAnnualPreEngineeredSystemInspection);
        $this->addReference('bi-annual-pre-engineered-system-inspection', $biAnnualPreEngineeredSystemInspection);

        /** @var ContractorService $biAnnualPreEngineeredSystemInspection */
        $biAnnualPreEngineeredSystemInspection = new ContractorService();
        $biAnnualPreEngineeredSystemInspection->setNamed($this->getReference('bi-annual-pre-engineered-system-inspection'));
        $biAnnualPreEngineeredSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $biAnnualPreEngineeredSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $biAnnualPreEngineeredSystemInspection->setState($this->getReference('state-illinois'));
        $biAnnualPreEngineeredSystemInspection->setFixedPrice(95.00);
        $biAnnualPreEngineeredSystemInspection->setHoursPrice(95.00);
        $biAnnualPreEngineeredSystemInspection->setReference('NFPA 96');
        $biAnnualPreEngineeredSystemInspection->setEstimationTime('1');

        $manager->persist($biAnnualPreEngineeredSystemInspection);
        $manager->flush();
        $this->addReference('cs-bi-annual-pre-engineered-system-inspection', $biAnnualPreEngineeredSystemInspection);
        // end Bi-Annual Pre-Engineered System Inspection service and Contractor Service


        // Quarterly Pre-Engineered System Inspection service and Contractor Service
        /** @var ServiceNamed $quarterlyPreEngineeredSystemInspection */
        $quarterlyPreEngineeredSystemInspection = new ServiceNamed();
        $quarterlyPreEngineeredSystemInspection->setName('Quarterly Pre-Engineered System Inspection');
        $quarterlyPreEngineeredSystemInspection->setFrequency($this->getReference('service-frequency-3-months'));
        $quarterlyPreEngineeredSystemInspection->addDeviceNamed($this->getReference('pre_engineered_fire_suppression_system_device'));
        $quarterlyPreEngineeredSystemInspection->setIsNeedSendMunicipalityReport(true);
        $quarterlyPreEngineeredSystemInspection->setIsNeedMunicipalityFee(true);
        $manager->persist($quarterlyPreEngineeredSystemInspection);
        $this->addReference('quarterly-pre-engineered-system-inspection', $quarterlyPreEngineeredSystemInspection);

        /** @var ContractorService $quarterlyPreEngineeredSystemInspection */
        $quarterlyPreEngineeredSystemInspection = new ContractorService();
        $quarterlyPreEngineeredSystemInspection->setNamed($this->getReference('quarterly-pre-engineered-system-inspection'));
        $quarterlyPreEngineeredSystemInspection->setContractor($this->getReference('contractor-my-company'));
        $quarterlyPreEngineeredSystemInspection->setDeviceCategory($this->getReference('device-category-fire'));
        $quarterlyPreEngineeredSystemInspection->setState($this->getReference('state-illinois'));
        $quarterlyPreEngineeredSystemInspection->setFixedPrice(95.00);
        $quarterlyPreEngineeredSystemInspection->setHoursPrice(95.00);
        $quarterlyPreEngineeredSystemInspection->setReference('NFPA 96');
        $quarterlyPreEngineeredSystemInspection->setEstimationTime('1');

        $manager->persist($quarterlyPreEngineeredSystemInspection);
        $manager->flush();
        $this->addReference('cs-quarterly-pre-engineered-system-inspection', $quarterlyPreEngineeredSystemInspection);
        // end Quarterly Pre-Engineered System Inspection service and Contractor Service

        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
