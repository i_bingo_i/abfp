<?php

namespace AppBundle\DataFixtures\ORM\Services;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Entity\ContractorService;

class LoadRepairFireAlarmControlPanelData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var ServiceNamed $generalRepairFireAlarmControlPanel1 */
        $generalRepairFireAlarmControlPanel1 = new ServiceNamed();
        $generalRepairFireAlarmControlPanel1->setName('General repair');
        $generalRepairFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $generalRepairFireAlarmControlPanel1->setDeficiency('Please see comments');
        $generalRepairFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $generalRepairFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFireAlarmControlPanel1);
        $this->addReference('general-repair-fire-alarm-control-panel-1', $generalRepairFireAlarmControlPanel1);

        $contractorGeneralRepairFireAlarmControlPanel1 = new ContractorService();
        $contractorGeneralRepairFireAlarmControlPanel1->setNamed($this->getReference('general-repair-fire-alarm-control-panel-1'));
        $contractorGeneralRepairFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-alarm-control-panel-1', $contractorGeneralRepairFireAlarmControlPanel1);



        /** @var ServiceNamed $generalRepairFireAlarmControlPanel2 */
        $generalRepairFireAlarmControlPanel2 = new ServiceNamed();
        $generalRepairFireAlarmControlPanel2->setName('General repair');
        $generalRepairFireAlarmControlPanel2->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $generalRepairFireAlarmControlPanel2->setDeficiency('Unidentified issue');
        $generalRepairFireAlarmControlPanel2->setIsNeedSendMunicipalityReport(false);
        $generalRepairFireAlarmControlPanel2->setIsNeedMunicipalityFee(false);
        $manager->persist($generalRepairFireAlarmControlPanel2);
        $this->addReference('general-repair-fire-alarm-control-panel-2', $generalRepairFireAlarmControlPanel2);

        $contractorGeneralRepairFireAlarmControlPanel2 = new ContractorService();
        $contractorGeneralRepairFireAlarmControlPanel2->setNamed($this->getReference('general-repair-fire-alarm-control-panel-2'));
        $contractorGeneralRepairFireAlarmControlPanel2->setContractor($this->getReference('contractor-my-company'));
        $contractorGeneralRepairFireAlarmControlPanel2->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorGeneralRepairFireAlarmControlPanel2->setState($this->getReference('state-illinois'));
        $contractorGeneralRepairFireAlarmControlPanel2->setEstimationTime('0');

        $manager->persist($contractorGeneralRepairFireAlarmControlPanel2);
        $manager->flush();
        $this->addReference('contractorService-general-repair-fire-alarm-control-panel-2', $contractorGeneralRepairFireAlarmControlPanel2);



        /** @var ServiceNamed $equipmentInstallationFireAlarmControlPanel1 */
        $equipmentInstallationFireAlarmControlPanel1 = new ServiceNamed();
        $equipmentInstallationFireAlarmControlPanel1->setName('Equipment installation');
        $equipmentInstallationFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $equipmentInstallationFireAlarmControlPanel1->setDeficiency('Please see comments');
        $equipmentInstallationFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $equipmentInstallationFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($equipmentInstallationFireAlarmControlPanel1);
        $this->addReference('equipment-installation-fire-alarm-control-panel-1', $equipmentInstallationFireAlarmControlPanel1);

        $contractorEquipmentInstallationFireAlarmControlPanel1 = new ContractorService();
        $contractorEquipmentInstallationFireAlarmControlPanel1->setNamed($this->getReference('equipment-installation-fire-alarm-control-panel-1'));
        $contractorEquipmentInstallationFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorEquipmentInstallationFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorEquipmentInstallationFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorEquipmentInstallationFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorEquipmentInstallationFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-equipment-installation-fire-alarm-control-panel-1', $contractorEquipmentInstallationFireAlarmControlPanel1);


        /** @var ServiceNamed $inspectionNoticeToBeSentFireAlarmControlPanel1 */
        $inspectionNoticeToBeSentFireAlarmControlPanel1 = new ServiceNamed();
        $inspectionNoticeToBeSentFireAlarmControlPanel1->setName('Inspection Notice to be sent');
        $inspectionNoticeToBeSentFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $inspectionNoticeToBeSentFireAlarmControlPanel1->setDeficiency('Some inspections for device past due or incomplete');
        $inspectionNoticeToBeSentFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $inspectionNoticeToBeSentFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectionNoticeToBeSentFireAlarmControlPanel1);
        $this->addReference('inspection-notice-to-be-sent-fire-alarm-control-panel-1', $inspectionNoticeToBeSentFireAlarmControlPanel1);

        $contractorInspectionNoticeToBeSentFireAlarmControlPanel1 = new ContractorService();
        $contractorInspectionNoticeToBeSentFireAlarmControlPanel1->setNamed($this->getReference('inspection-notice-to-be-sent-fire-alarm-control-panel-1'));
        $contractorInspectionNoticeToBeSentFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectionNoticeToBeSentFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectionNoticeToBeSentFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorInspectionNoticeToBeSentFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorInspectionNoticeToBeSentFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-inspection-notice-to-be-sent-fire-alarm-control-panel-1', $contractorInspectionNoticeToBeSentFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceExteriorStrobeFireAlarmControlPanel1 */
        $replaceExteriorStrobeFireAlarmControlPanel1 = new ServiceNamed();
        $replaceExteriorStrobeFireAlarmControlPanel1->setName('Replace exterior strobe');
        $replaceExteriorStrobeFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceExteriorStrobeFireAlarmControlPanel1->setDeficiency('Exterior strobe not working');
        $replaceExteriorStrobeFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceExteriorStrobeFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceExteriorStrobeFireAlarmControlPanel1);
        $this->addReference('replace-exterior-strobe-fire-alarm-control-panel-1', $replaceExteriorStrobeFireAlarmControlPanel1);

        $contractorReplaceExteriorStrobeFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceExteriorStrobeFireAlarmControlPanel1->setNamed($this->getReference('replace-exterior-strobe-fire-alarm-control-panel-1'));
        $contractorReplaceExteriorStrobeFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceExteriorStrobeFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceExteriorStrobeFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceExteriorStrobeFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceExteriorStrobeFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-exterior-strobe-fire-alarm-control-panel-1', $contractorReplaceExteriorStrobeFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceLocalFireBellFireAlarmControlPanel1 */
        $replaceLocalFireBellFireAlarmControlPanel1 = new ServiceNamed();
        $replaceLocalFireBellFireAlarmControlPanel1->setName('Replace local fire bell');
        $replaceLocalFireBellFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceLocalFireBellFireAlarmControlPanel1->setDeficiency('Local fire bell not working');
        $replaceLocalFireBellFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceLocalFireBellFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceLocalFireBellFireAlarmControlPanel1);
        $this->addReference('replace-local-fire-bell-fire-alarm-control-panel-1', $replaceLocalFireBellFireAlarmControlPanel1);

        $contractorReplaceLocalFireBellFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceLocalFireBellFireAlarmControlPanel1->setNamed($this->getReference('replace-local-fire-bell-fire-alarm-control-panel-1'));
        $contractorReplaceLocalFireBellFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceLocalFireBellFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceLocalFireBellFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceLocalFireBellFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceLocalFireBellFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-local-fire-bell-fire-alarm-control-panel-1', $contractorReplaceLocalFireBellFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceSmokeDetectorFireAlarmControlPanel1 */
        $replaceSmokeDetectorFireAlarmControlPanel1 = new ServiceNamed();
        $replaceSmokeDetectorFireAlarmControlPanel1->setName('Replace smoke detector');
        $replaceSmokeDetectorFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceSmokeDetectorFireAlarmControlPanel1->setDeficiency('Smoke detector not working');
        $replaceSmokeDetectorFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceSmokeDetectorFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceSmokeDetectorFireAlarmControlPanel1);
        $this->addReference('replace-smoke-detector-fire-alarm-control-panel-1', $replaceSmokeDetectorFireAlarmControlPanel1);

        $contractorReplaceSmokeDetectorFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceSmokeDetectorFireAlarmControlPanel1->setNamed($this->getReference('replace-smoke-detector-fire-alarm-control-panel-1'));
        $contractorReplaceSmokeDetectorFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceSmokeDetectorFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceSmokeDetectorFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceSmokeDetectorFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceSmokeDetectorFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-smoke-detector-fire-alarm-control-panel-1', $contractorReplaceSmokeDetectorFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceHeatDetectorFireAlarmControlPanel1 */
        $replaceHeatDetectorFireAlarmControlPanel1 = new ServiceNamed();
        $replaceHeatDetectorFireAlarmControlPanel1->setName('Replace heat detector');
        $replaceHeatDetectorFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceHeatDetectorFireAlarmControlPanel1->setDeficiency('Heat detector not working');
        $replaceHeatDetectorFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceHeatDetectorFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceHeatDetectorFireAlarmControlPanel1);
        $this->addReference('replace-heat-detector-fire-alarm-control-panel-1', $replaceHeatDetectorFireAlarmControlPanel1);

        $contractorReplaceHeatDetectorFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceHeatDetectorFireAlarmControlPanel1->setNamed($this->getReference('replace-heat-detector-fire-alarm-control-panel-1'));
        $contractorReplaceHeatDetectorFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceHeatDetectorFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceHeatDetectorFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceHeatDetectorFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceHeatDetectorFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-heat-detector-fire-alarm-control-panel-1', $contractorReplaceHeatDetectorFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceBatteryForSecondaryPowerFireAlarmControlPanel1 */
        $replaceBatteryForSecondaryPowerFireAlarmControlPanel1 = new ServiceNamed();
        $replaceBatteryForSecondaryPowerFireAlarmControlPanel1->setName('Replace battery for secondary power');
        $replaceBatteryForSecondaryPowerFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceBatteryForSecondaryPowerFireAlarmControlPanel1->setDeficiency('Battery for secondary power supply failed');
        $replaceBatteryForSecondaryPowerFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceBatteryForSecondaryPowerFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceBatteryForSecondaryPowerFireAlarmControlPanel1);
        $this->addReference('replace-battery-for-secondary-power-fire-alarm-control-panel-1', $replaceBatteryForSecondaryPowerFireAlarmControlPanel1);

        $contractorReplaceBatteryForSecondaryPowerFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceBatteryForSecondaryPowerFireAlarmControlPanel1->setNamed($this->getReference('replace-battery-for-secondary-power-fire-alarm-control-panel-1'));
        $contractorReplaceBatteryForSecondaryPowerFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceBatteryForSecondaryPowerFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceBatteryForSecondaryPowerFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceBatteryForSecondaryPowerFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceBatteryForSecondaryPowerFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-battery-for-secondary-power-fire-alarm-control-panel-1', $contractorReplaceBatteryForSecondaryPowerFireAlarmControlPanel1);


        /** @var ServiceNamed $addReplaceLowAirAlarmSwitchFireAlarmControlPanel1 */
        $addReplaceLowAirAlarmSwitchFireAlarmControlPanel1 = new ServiceNamed();
        $addReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setName('Add/Replace Low Air Alarm Switch');
        $addReplaceLowAirAlarmSwitchFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $addReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setDeficiency('Low Air Switch Missing or Damaged');
        $addReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $addReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($addReplaceLowAirAlarmSwitchFireAlarmControlPanel1);
        $this->addReference('add-replace-low-air-alarm-switch-fire-alarm-control-panel-1', $addReplaceLowAirAlarmSwitchFireAlarmControlPanel1);

        $contractorAddReplaceLowAirAlarmSwitchFireAlarmControlPanel1 = new ContractorService();
        $contractorAddReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setNamed($this->getReference('add-replace-low-air-alarm-switch-fire-alarm-control-panel-1'));
        $contractorAddReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorAddReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorAddReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorAddReplaceLowAirAlarmSwitchFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorAddReplaceLowAirAlarmSwitchFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-add-replace-low-air-alarm-switch-fire-alarm-control-panel-1', $contractorAddReplaceLowAirAlarmSwitchFireAlarmControlPanel1);


        /** @var ServiceNamed $installNewStrobeLightFireAlarmControlPanel1 */
        $installNewStrobeLightFireAlarmControlPanel1 = new ServiceNamed();
        $installNewStrobeLightFireAlarmControlPanel1->setName('Install New Strobe Light');
        $installNewStrobeLightFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $installNewStrobeLightFireAlarmControlPanel1->setDeficiency('Strobe Light Missing or Needed');
        $installNewStrobeLightFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $installNewStrobeLightFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($installNewStrobeLightFireAlarmControlPanel1);
        $this->addReference('install-new-strobe-light-fire-alarm-control-panel-1', $installNewStrobeLightFireAlarmControlPanel1);

        $contractorInstallNewStrobeLightFireAlarmControlPanel1 = new ContractorService();
        $contractorInstallNewStrobeLightFireAlarmControlPanel1->setNamed($this->getReference('install-new-strobe-light-fire-alarm-control-panel-1'));
        $contractorInstallNewStrobeLightFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallNewStrobeLightFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallNewStrobeLightFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorInstallNewStrobeLightFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorInstallNewStrobeLightFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-install-new-strobe-light-fire-alarm-control-panel-1', $contractorInstallNewStrobeLightFireAlarmControlPanel1);


        /** @var ServiceNamed $connectSupervisorySwitchFireAlarmControlPanel1 */
        $connectSupervisorySwitchFireAlarmControlPanel1 = new ServiceNamed();
        $connectSupervisorySwitchFireAlarmControlPanel1->setName('Connect Supervisory Switch');
        $connectSupervisorySwitchFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $connectSupervisorySwitchFireAlarmControlPanel1->setDeficiency('Switch not working or missing');
        $connectSupervisorySwitchFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $connectSupervisorySwitchFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($connectSupervisorySwitchFireAlarmControlPanel1);
        $this->addReference('connect-supervisory-switch-fire-alarm-control-panel-1', $connectSupervisorySwitchFireAlarmControlPanel1);

        $contractorConnectSupervisorySwitchFireAlarmControlPanel1 = new ContractorService();
        $contractorConnectSupervisorySwitchFireAlarmControlPanel1->setNamed($this->getReference('connect-supervisory-switch-fire-alarm-control-panel-1'));
        $contractorConnectSupervisorySwitchFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorConnectSupervisorySwitchFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorConnectSupervisorySwitchFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorConnectSupervisorySwitchFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorConnectSupervisorySwitchFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-connect-supervisory-switch-fire-alarm-control-panel-1', $contractorConnectSupervisorySwitchFireAlarmControlPanel1);


        /** @var ServiceNamed $inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1 */
        $inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1 = new ServiceNamed();
        $inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setName('Inspect and bring on-line Fire Alarm Panel');
        $inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setDeficiency('Panel not working at this time');
        $inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1);
        $this->addReference('inspect-and-bring-on-line-fire-alarm-panel-fire-alarm-control-panel-1', $inspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1);

        $contractorInspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1 = new ContractorService();
        $contractorInspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setNamed($this->getReference('inspect-and-bring-on-line-fire-alarm-panel-fire-alarm-control-panel-1'));
        $contractorInspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorInspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorInspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorInspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-inspect-and-bring-on-line-fire-alarm-panel-fire-alarm-control-panel-1', $contractorInspectAndBringOnLineFireAlarmPanelFireAlarmControlPanel1);


        /** @var ServiceNamed $connectFlowSwitchToAlarmPanelFireAlarmControlPanel1 */
        $connectFlowSwitchToAlarmPanelFireAlarmControlPanel1 = new ServiceNamed();
        $connectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setName('Connect Flow Switch to Alarm Panel');
        $connectFlowSwitchToAlarmPanelFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $connectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setDeficiency('Flow Switch not monitored by FACP');
        $connectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $connectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($connectFlowSwitchToAlarmPanelFireAlarmControlPanel1);
        $this->addReference('connect-flow-switch-to-alarm-panel-fire-alarm-control-panel-1', $connectFlowSwitchToAlarmPanelFireAlarmControlPanel1);

        $contractorConnectFlowSwitchToAlarmPanelFireAlarmControlPanel1 = new ContractorService();
        $contractorConnectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setNamed($this->getReference('connect-flow-switch-to-alarm-panel-fire-alarm-control-panel-1'));
        $contractorConnectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorConnectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorConnectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorConnectFlowSwitchToAlarmPanelFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorConnectFlowSwitchToAlarmPanelFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-connect-flow-switch-to-alarm-panel-fire-alarm-control-panel-1', $contractorConnectFlowSwitchToAlarmPanelFireAlarmControlPanel1);


        /** @var ServiceNamed $investigateAlarmPanelDeviceFireAlarmControlPanel1 */
        $investigateAlarmPanelDeviceFireAlarmControlPanel1 = new ServiceNamed();
        $investigateAlarmPanelDeviceFireAlarmControlPanel1->setName('Investigate Alarm Panel/Device');
        $investigateAlarmPanelDeviceFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $investigateAlarmPanelDeviceFireAlarmControlPanel1->setDeficiency('Device not operating correctly');
        $investigateAlarmPanelDeviceFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $investigateAlarmPanelDeviceFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($investigateAlarmPanelDeviceFireAlarmControlPanel1);
        $this->addReference('investigate-alarm-panel-device-fire-alarm-control-panel-1', $investigateAlarmPanelDeviceFireAlarmControlPanel1);

        $contractorInvestigateAlarmPanelDeviceFireAlarmControlPanel1 = new ContractorService();
        $contractorInvestigateAlarmPanelDeviceFireAlarmControlPanel1->setNamed($this->getReference('investigate-alarm-panel-device-fire-alarm-control-panel-1'));
        $contractorInvestigateAlarmPanelDeviceFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorInvestigateAlarmPanelDeviceFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInvestigateAlarmPanelDeviceFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorInvestigateAlarmPanelDeviceFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorInvestigateAlarmPanelDeviceFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-investigate-alarm-panel-device-fire-alarm-control-panel-1', $contractorInvestigateAlarmPanelDeviceFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceTransceiverBatteriesFireAlarmControlPanel1 */
        $replaceTransceiverBatteriesFireAlarmControlPanel1 = new ServiceNamed();
        $replaceTransceiverBatteriesFireAlarmControlPanel1->setName('Replace Transceiver Batteries');
        $replaceTransceiverBatteriesFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceTransceiverBatteriesFireAlarmControlPanel1->setDeficiency('Transceiver Batteries did not pass testing');
        $replaceTransceiverBatteriesFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceTransceiverBatteriesFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceTransceiverBatteriesFireAlarmControlPanel1);
        $this->addReference('replace-transceiver-batteries-fire-alarm-control-panel-1', $replaceTransceiverBatteriesFireAlarmControlPanel1);

        $contractorReplaceTransceiverBatteriesFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceTransceiverBatteriesFireAlarmControlPanel1->setNamed($this->getReference('replace-transceiver-batteries-fire-alarm-control-panel-1'));
        $contractorReplaceTransceiverBatteriesFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceTransceiverBatteriesFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceTransceiverBatteriesFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceTransceiverBatteriesFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceTransceiverBatteriesFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-transceiver-batteries-fire-alarm-control-panel-1', $contractorReplaceTransceiverBatteriesFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceFirePullStationFireAlarmControlPanel1 */
        $replaceFirePullStationFireAlarmControlPanel1 = new ServiceNamed();
        $replaceFirePullStationFireAlarmControlPanel1->setName('Replace Fire Pull Station');
        $replaceFirePullStationFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceFirePullStationFireAlarmControlPanel1->setDeficiency('Pull Station Inoperative');
        $replaceFirePullStationFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceFirePullStationFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceFirePullStationFireAlarmControlPanel1);
        $this->addReference('replace-fire-pull-station-fire-alarm-control-panel-1', $replaceFirePullStationFireAlarmControlPanel1);

        $contractorReplaceFirePullStationFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceFirePullStationFireAlarmControlPanel1->setNamed($this->getReference('replace-fire-pull-station-fire-alarm-control-panel-1'));
        $contractorReplaceFirePullStationFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceFirePullStationFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceFirePullStationFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceFirePullStationFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceFirePullStationFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-fire-pull-station-fire-alarm-control-panel-1', $contractorReplaceFirePullStationFireAlarmControlPanel1);


        /** @var ServiceNamed $installNewFirePanelFireAlarmControlPanel1 */
        $installNewFirePanelFireAlarmControlPanel1 = new ServiceNamed();
        $installNewFirePanelFireAlarmControlPanel1->setName('Install New Fire Panel');
        $installNewFirePanelFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $installNewFirePanelFireAlarmControlPanel1->setDeficiency('Panel Inoperative and needs to be replaced');
        $installNewFirePanelFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $installNewFirePanelFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($installNewFirePanelFireAlarmControlPanel1);
        $this->addReference('install-new-fire-panel-fire-alarm-control-panel-1', $installNewFirePanelFireAlarmControlPanel1);

        $contractorInstallNewFirePanelFireAlarmControlPanel1 = new ContractorService();
        $contractorInstallNewFirePanelFireAlarmControlPanel1->setNamed($this->getReference('install-new-fire-panel-fire-alarm-control-panel-1'));
        $contractorInstallNewFirePanelFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallNewFirePanelFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallNewFirePanelFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorInstallNewFirePanelFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorInstallNewFirePanelFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-install-new-fire-panel-fire-alarm-control-panel-1', $contractorInstallNewFirePanelFireAlarmControlPanel1);


        /** @var ServiceNamed $recalibrateFlowSwitchesFireAlarmControlPanel1 */
        $recalibrateFlowSwitchesFireAlarmControlPanel1 = new ServiceNamed();
        $recalibrateFlowSwitchesFireAlarmControlPanel1->setName('Recalibrate Flow Switches');
        $recalibrateFlowSwitchesFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $recalibrateFlowSwitchesFireAlarmControlPanel1->setDeficiency('Flow Switches not sending Signal');
        $recalibrateFlowSwitchesFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $recalibrateFlowSwitchesFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($recalibrateFlowSwitchesFireAlarmControlPanel1);
        $this->addReference('recalibrate-flow-switches-fire-alarm-control-panel-1', $recalibrateFlowSwitchesFireAlarmControlPanel1);

        $contractorRecalibrateFlowSwitchesFireAlarmControlPanel1 = new ContractorService();
        $contractorRecalibrateFlowSwitchesFireAlarmControlPanel1->setNamed($this->getReference('recalibrate-flow-switches-fire-alarm-control-panel-1'));
        $contractorRecalibrateFlowSwitchesFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorRecalibrateFlowSwitchesFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRecalibrateFlowSwitchesFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorRecalibrateFlowSwitchesFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorRecalibrateFlowSwitchesFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-recalibrate-flow-switches-fire-alarm-control-panel-1', $contractorRecalibrateFlowSwitchesFireAlarmControlPanel1);


        /** @var ServiceNamed $recalibrateTamperSwitchesFireAlarmControlPanel1 */
        $recalibrateTamperSwitchesFireAlarmControlPanel1 = new ServiceNamed();
        $recalibrateTamperSwitchesFireAlarmControlPanel1->setName('Recalibrate Tamper Switches');
        $recalibrateTamperSwitchesFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $recalibrateTamperSwitchesFireAlarmControlPanel1->setDeficiency('Tamper Switches not sending Signal');
        $recalibrateTamperSwitchesFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $recalibrateTamperSwitchesFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($recalibrateTamperSwitchesFireAlarmControlPanel1);
        $this->addReference('recalibrate-tamper-switches-fire-alarm-control-panel-1', $recalibrateTamperSwitchesFireAlarmControlPanel1);

        $contractorRecalibrateTamperSwitchesFireAlarmControlPanel1 = new ContractorService();
        $contractorRecalibrateTamperSwitchesFireAlarmControlPanel1->setNamed($this->getReference('recalibrate-tamper-switches-fire-alarm-control-panel-1'));
        $contractorRecalibrateTamperSwitchesFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorRecalibrateTamperSwitchesFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRecalibrateTamperSwitchesFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorRecalibrateTamperSwitchesFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorRecalibrateTamperSwitchesFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-recalibrate-tamper-switches-fire-alarm-control-panel-1', $contractorRecalibrateTamperSwitchesFireAlarmControlPanel1);


        /** @var ServiceNamed $rewireTamperSwitchFireAlarmControlPanel1 */
        $rewireTamperSwitchFireAlarmControlPanel1 = new ServiceNamed();
        $rewireTamperSwitchFireAlarmControlPanel1->setName('Rewire tamper switch');
        $rewireTamperSwitchFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $rewireTamperSwitchFireAlarmControlPanel1->setDeficiency('Tamper switch not wired correctly');
        $rewireTamperSwitchFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $rewireTamperSwitchFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($rewireTamperSwitchFireAlarmControlPanel1);
        $this->addReference('rewire-tamper-switch-fire-alarm-control-panel-1', $rewireTamperSwitchFireAlarmControlPanel1);

        $contractorRewireTamperSwitchFireAlarmControlPanel1 = new ContractorService();
        $contractorRewireTamperSwitchFireAlarmControlPanel1->setNamed($this->getReference('rewire-tamper-switch-fire-alarm-control-panel-1'));
        $contractorRewireTamperSwitchFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorRewireTamperSwitchFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorRewireTamperSwitchFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorRewireTamperSwitchFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorRewireTamperSwitchFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-rewire-tamper-switch-fire-alarm-control-panel-1', $contractorRewireTamperSwitchFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceTamperSwitchFireAlarmControlPanel1 */
        $replaceTamperSwitchFireAlarmControlPanel1 = new ServiceNamed();
        $replaceTamperSwitchFireAlarmControlPanel1->setName('Replace tamper switch');
        $replaceTamperSwitchFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceTamperSwitchFireAlarmControlPanel1->setDeficiency('Tamper switch not working correctly');
        $replaceTamperSwitchFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceTamperSwitchFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceTamperSwitchFireAlarmControlPanel1);
        $this->addReference('replace-tamper-switch-fire-alarm-control-panel-1', $replaceTamperSwitchFireAlarmControlPanel1);

        $contractorReplaceTamperSwitchFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceTamperSwitchFireAlarmControlPanel1->setNamed($this->getReference('replace-tamper-switch-fire-alarm-control-panel-1'));
        $contractorReplaceTamperSwitchFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceTamperSwitchFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceTamperSwitchFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceTamperSwitchFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceTamperSwitchFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-tamper-switch-fire-alarm-control-panel-1', $contractorReplaceTamperSwitchFireAlarmControlPanel1);


        /** @var ServiceNamed $installTamperCorrectlyFireAlarmControlPanel1 */
        $installTamperCorrectlyFireAlarmControlPanel1 = new ServiceNamed();
        $installTamperCorrectlyFireAlarmControlPanel1->setName('Install tamper correctly');
        $installTamperCorrectlyFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $installTamperCorrectlyFireAlarmControlPanel1->setDeficiency('Tamper switch not installed correctly');
        $installTamperCorrectlyFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $installTamperCorrectlyFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($installTamperCorrectlyFireAlarmControlPanel1);
        $this->addReference('install-tamper-correctly-fire-alarm-control-panel-1', $installTamperCorrectlyFireAlarmControlPanel1);

        $contractorInstallTamperCorrectlyFireAlarmControlPanel1 = new ContractorService();
        $contractorInstallTamperCorrectlyFireAlarmControlPanel1->setNamed($this->getReference('install-tamper-correctly-fire-alarm-control-panel-1'));
        $contractorInstallTamperCorrectlyFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallTamperCorrectlyFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallTamperCorrectlyFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorInstallTamperCorrectlyFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorInstallTamperCorrectlyFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-install-tamper-correctly-fire-alarm-control-panel-1', $contractorInstallTamperCorrectlyFireAlarmControlPanel1);


        /** @var ServiceNamed $installTamperSwitchFireAlarmControlPanel1 */
        $installTamperSwitchFireAlarmControlPanel1 = new ServiceNamed();
        $installTamperSwitchFireAlarmControlPanel1->setName('Install tamper switch');
        $installTamperSwitchFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $installTamperSwitchFireAlarmControlPanel1->setDeficiency('Tamper switch missing');
        $installTamperSwitchFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $installTamperSwitchFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($installTamperSwitchFireAlarmControlPanel1);
        $this->addReference('install-tamper-switch-fire-alarm-control-panel-1', $installTamperSwitchFireAlarmControlPanel1);

        $contractorInstallTamperSwitchFireAlarmControlPanel1 = new ContractorService();
        $contractorInstallTamperSwitchFireAlarmControlPanel1->setNamed($this->getReference('install-tamper-switch-fire-alarm-control-panel-1'));
        $contractorInstallTamperSwitchFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorInstallTamperSwitchFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorInstallTamperSwitchFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorInstallTamperSwitchFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorInstallTamperSwitchFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-install-tamper-switch-fire-alarm-control-panel-1', $contractorInstallTamperSwitchFireAlarmControlPanel1);


        /** @var ServiceNamed $syncStrobeLightsFireAlarmControlPanel1 */
        $syncStrobeLightsFireAlarmControlPanel1 = new ServiceNamed();
        $syncStrobeLightsFireAlarmControlPanel1->setName('Sync Strobe Lights');
        $syncStrobeLightsFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $syncStrobeLightsFireAlarmControlPanel1->setDeficiency('Strobe Lights out of Sync');
        $syncStrobeLightsFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $syncStrobeLightsFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($syncStrobeLightsFireAlarmControlPanel1);
        $this->addReference('sync-strobe-lights-fire-alarm-control-panel-1', $syncStrobeLightsFireAlarmControlPanel1);

        $contractorSyncStrobeLightsFireAlarmControlPanel1 = new ContractorService();
        $contractorSyncStrobeLightsFireAlarmControlPanel1->setNamed($this->getReference('sync-strobe-lights-fire-alarm-control-panel-1'));
        $contractorSyncStrobeLightsFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorSyncStrobeLightsFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorSyncStrobeLightsFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorSyncStrobeLightsFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorSyncStrobeLightsFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-sync-strobe-lights-fire-alarm-control-panel-1', $contractorSyncStrobeLightsFireAlarmControlPanel1);


        /** @var ServiceNamed $addTamperSwitchesToSystemFireAlarmControlPanel1 */
        $addTamperSwitchesToSystemFireAlarmControlPanel1 = new ServiceNamed();
        $addTamperSwitchesToSystemFireAlarmControlPanel1->setName('Add Tamper Switches to System');
        $addTamperSwitchesToSystemFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $addTamperSwitchesToSystemFireAlarmControlPanel1->setDeficiency('Tamper Switches not Connected');
        $addTamperSwitchesToSystemFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $addTamperSwitchesToSystemFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($addTamperSwitchesToSystemFireAlarmControlPanel1);
        $this->addReference('add-tamper-switches-to-system-fire-alarm-control-panel-1', $addTamperSwitchesToSystemFireAlarmControlPanel1);

        $contractorAddTamperSwitchesToSystemFireAlarmControlPanel1 = new ContractorService();
        $contractorAddTamperSwitchesToSystemFireAlarmControlPanel1->setNamed($this->getReference('add-tamper-switches-to-system-fire-alarm-control-panel-1'));
        $contractorAddTamperSwitchesToSystemFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorAddTamperSwitchesToSystemFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorAddTamperSwitchesToSystemFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorAddTamperSwitchesToSystemFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorAddTamperSwitchesToSystemFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-add-tamper-switches-to-system-fire-alarm-control-panel-1', $contractorAddTamperSwitchesToSystemFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceHornFireAlarmControlPanel1 */
        $replaceHornFireAlarmControlPanel1 = new ServiceNamed();
        $replaceHornFireAlarmControlPanel1->setName('Replace horn');
        $replaceHornFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceHornFireAlarmControlPanel1->setDeficiency('Horn not working');
        $replaceHornFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceHornFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceHornFireAlarmControlPanel1);
        $this->addReference('replace-horn-fire-alarm-control-panel-1', $replaceHornFireAlarmControlPanel1);

        $contractorReplaceHornFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceHornFireAlarmControlPanel1->setNamed($this->getReference('replace-horn-fire-alarm-control-panel-1'));
        $contractorReplaceHornFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceHornFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceHornFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceHornFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceHornFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-horn-fire-alarm-control-panel-1', $contractorReplaceHornFireAlarmControlPanel1);


        /** @var ServiceNamed $replaceStrobeFireAlarmControlPanel1 */
        $replaceStrobeFireAlarmControlPanel1 = new ServiceNamed();
        $replaceStrobeFireAlarmControlPanel1->setName('Replace strobe');
        $replaceStrobeFireAlarmControlPanel1->addDeviceNamed($this->getReference('fire-alarm-control-panel-device'));
        $replaceStrobeFireAlarmControlPanel1->setDeficiency('Strobe not working');
        $replaceStrobeFireAlarmControlPanel1->setIsNeedSendMunicipalityReport(false);
        $replaceStrobeFireAlarmControlPanel1->setIsNeedMunicipalityFee(false);
        $manager->persist($replaceStrobeFireAlarmControlPanel1);
        $this->addReference('replace-strobe-fire-alarm-control-panel-1', $replaceStrobeFireAlarmControlPanel1);

        $contractorReplaceStrobeFireAlarmControlPanel1 = new ContractorService();
        $contractorReplaceStrobeFireAlarmControlPanel1->setNamed($this->getReference('replace-strobe-fire-alarm-control-panel-1'));
        $contractorReplaceStrobeFireAlarmControlPanel1->setContractor($this->getReference('contractor-my-company'));
        $contractorReplaceStrobeFireAlarmControlPanel1->setDeviceCategory($this->getReference('device-category-fire'));
        $contractorReplaceStrobeFireAlarmControlPanel1->setState($this->getReference('state-illinois'));
        $contractorReplaceStrobeFireAlarmControlPanel1->setEstimationTime('0');

        $manager->persist($contractorReplaceStrobeFireAlarmControlPanel1);
        $manager->flush();
        $this->addReference('contractorService-replace-strobe-fire-alarm-control-panel-1', $contractorReplaceStrobeFireAlarmControlPanel1);



        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
