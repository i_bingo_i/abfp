<?php

namespace AppBundle\DataFixtures\ORM\Contractors;

use AppBundle\Entity\Contractor;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadContractorData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contractor1 = new Contractor();

        $contractor1->setName('Monarch');
        $contractor1->setWebsite('https://monarch.com');
        $contractor1->setStateLicenseNumber('123321');
        $contractor1->setStateLicenseExpirationDate(new \DateTime());
        $contractor1->setCoiExpiration(new \DateTime());
        $contractor1->setLogo('/uploads/logo/company_a.png');
        $contractor1->setType($this->getReference('contractor-type-partner'));
        $contractor1->setAddress($this->getReference('address-fifteen'));
        $contractor1->setDateCreate(new \DateTime());
        $contractor1->setDateUpdate($contractor1->getDateCreate());
        $contractor1->setPhone('(123) 456-7890');

        $manager->persist($contractor1);
        $manager->flush();

        $this->addReference('contractor-partner-with-coi-expiration', $contractor1);

        $contractor2 = new Contractor();

        $contractor2->setName("Contractor2");
        $contractor2->setWebsite('https://www.yahoo.com/');
        $contractor2->setStateLicenseNumber('123321');
        $contractor2->setStateLicenseExpirationDate(new \DateTime());
        $contractor2->setCoiExpiration(new \DateTime());
        $contractor2->setLogo('/uploads/logo/company_b.png');
        $contractor2->setType($this->getReference('contractor-type-competitor'));
        $contractor2->setAddress($this->getReference('address-thirteen'));
        $contractor2->setDateCreate(new \DateTime());
        $contractor2->setDateUpdate($contractor2->getDateCreate());
        $contractor2->setPhone('(098) 765-4321');

        $manager->persist($contractor2);
        $manager->flush();

        $this->addReference('contractor-competitor', $contractor2);

        $contractor3 = new Contractor();

        $contractor3->setName('American Backflow & Fire Prevention');
        $contractor3->setWebsite('www.americanbackflowprevention.com');
        $contractor3->setStateLicenseNumber('123321');
        $contractor3->setStateLicenseExpirationDate(new \DateTime());
        $contractor3->setCoiExpiration(new \DateTime());
        $contractor3->setLogo('/uploads/logo/abfp_logo_png.jpg');
        $contractor3->setType($this->getReference('contractor-type-my-company'));
        $contractor3->setAddress($this->getReference('address-american-backflow'));
        $contractor3->setDateCreate(new \DateTime());
        $contractor3->setDateUpdate($contractor3->getDateCreate());
        $contractor3->setPhone('(777) 888-8888');

        $manager->persist($contractor3);
        $manager->flush();

        $this->addReference('contractor-my-company', $contractor3);


        $contractor4 = new Contractor();

        $contractor4->setName("Contractor no coi date");
        $contractor4->setWebsite('http://www.google.com');
        $contractor4->setStateLicenseNumber('123321');
        $contractor4->setStateLicenseExpirationDate(new \DateTime());
//        $contractor4->setLogo('/uploads/logo/1498116995_9149.jpeg');
        $contractor4->setType($this->getReference('contractor-type-partner'));
        $contractor4->setAddress($this->getReference('address-fourteen'));
        $contractor4->setDateCreate(new \DateTime());
        $contractor4->setDateUpdate($contractor4->getDateCreate());

        $manager->persist($contractor4);
        $manager->flush();

        $this->addReference('contractor-partner-without-coi-expiration', $contractor4);


        $contractor5 = new Contractor();
        $contractor5->setName('IDAP Group');
        $contractor5->setWebsite('http://idapgroup.com');
        $contractor5->setStateLicenseNumber('123456');
        $contractor5->setStateLicenseExpirationDate(new \DateTime());
        $contractor5->setDateCreate(new \DateTime());
        $contractor5->setDateUpdate(new \DateTime());
        $contractor5->setType($this->getReference('contractor-type-partner'));
        $contractor5->setAddress($this->getReference('address-for-account-for-changing-contact-person-roles'));

        $manager->persist($contractor5);
        $manager->flush();

        $this->addReference('contractor-partner-idap-group', $contractor5);

    }

    public function getOrder()
    {
        return 8;
    }
}
