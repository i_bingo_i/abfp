<?php

namespace AppBundle\DataFixtures\ORM\Contractors;

use AppBundle\Entity\ContractorType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadContractorTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contractorType1 = new ContractorType();
        $contractorType1->setName('Partner');
        $contractorType1->setAlias('partner');
        $contractorType1->setDateCreate(new \DateTime());
        $contractorType1->setDateUpdate($contractorType1->getDateCreate());
        $contractorType1->setSort(2);

        $manager->persist($contractorType1);
        $manager->flush();
        $this->addReference('contractor-type-partner', $contractorType1);

        $contractorType2 = new ContractorType();
        $contractorType2->setName('Competitor');
        $contractorType2->setAlias('competitor');
        $contractorType2->setDateCreate(new \DateTime());
        $contractorType2->setDateUpdate($contractorType2->getDateCreate());
        $contractorType2->setSort(3);

        $manager->persist($contractorType2);
        $manager->flush();
        $this->addReference('contractor-type-competitor', $contractorType2);

        $contractorType3 = new ContractorType();
        $contractorType3->setName('My Company');
        $contractorType3->setAlias('my_company');
        $contractorType3->setDateCreate(new \DateTime());
        $contractorType3->setDateUpdate($contractorType3->getDateCreate());
        $contractorType3->setSort(1);

        $manager->persist($contractorType3);
        $manager->flush();
        $this->addReference('contractor-type-my-company', $contractorType3);
    }

    public function getOrder()
    {
        return 5;
    }
}