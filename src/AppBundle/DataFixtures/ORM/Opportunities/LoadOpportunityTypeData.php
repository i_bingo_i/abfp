<?php

namespace AppBundle\DataFixtures\ORM\Opportunities;

use AppBundle\Entity\OpportunityType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadOpportunityTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $opportunityTypeRetest = new OpportunityType();
        $opportunityTypeRetest->setName('Retest');
        $opportunityTypeRetest->setAlisa('retest');

        $manager->persist($opportunityTypeRetest);
        $manager->flush();
        $this->addReference('opportunity-type-one', $opportunityTypeRetest);

        $opportunityTypeRepair = new OpportunityType();
        $opportunityTypeRepair->setName('Repair');
        $opportunityTypeRepair->setAlisa('repair');

        $manager->persist($opportunityTypeRepair);
        $manager->flush();
        $this->addReference('opportunity-type-repair', $opportunityTypeRepair);
    }

    public function getOrder()
    {
        return 1;
    }
}
