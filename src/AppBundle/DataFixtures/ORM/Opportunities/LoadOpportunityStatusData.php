<?php

namespace AppBundle\DataFixtures\ORM\Opportunities;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\OpportunityStatus;

class OpportunityStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $opportunityStatusPastDue = new OpportunityStatus();
        $opportunityStatusPastDue->setName('Past Due');
        $opportunityStatusPastDue->setAlisa('past_due');
        $opportunityStatusPastDue->setSort(1);

        $manager->persist($opportunityStatusPastDue);
        $manager->flush();
        $this->addReference('opportunity-status-past-due', $opportunityStatusPastDue);


        $opportunityStatusRNtoBeCreated = new OpportunityStatus();
        $opportunityStatusRNtoBeCreated->setName('RN To Be Created');
        $opportunityStatusRNtoBeCreated->setAlisa('rn_to_be_created');
        $opportunityStatusRNtoBeCreated->setSort(2);

        $manager->persist($opportunityStatusRNtoBeCreated);
        $manager->flush();
        $this->addReference('opportunity-status-rn-to-be-created', $opportunityStatusRNtoBeCreated);


//        $opportunityStatusOmitted = new OpportunityStatus();
//        $opportunityStatusOmitted->setName('Omitted');
//        $opportunityStatusOmitted->setAlisa('omitted');
//        $opportunityStatusOmitted->setSort(3);
//
//        $manager->persist($opportunityStatusOmitted);
//        $manager->flush();
//        $this->addReference('opportunity-status-omitted', $opportunityStatusOmitted);
    }

    public function getOrder()
    {
        return 1;
    }
}
