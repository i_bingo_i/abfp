<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Address;

class LoadAddressesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $oldSystemAddress = new Address();
        $oldSystemAddress->setAddress("Old system address 111");
        $oldSystemAddress->setCity("New York");
        $oldSystemAddress->setZip("23345");
        $oldSystemAddress->setState($this->getReference('state-new-york'));
        $oldSystemAddress->setAddressType($this->getReference('address-type-old-system-address'));
        $manager->persist($oldSystemAddress);
        $manager->flush();
        $this->addReference('special-old-system-address-for-account', $oldSystemAddress);

        /*******************************************************************/
        $addressOneContractor = new Address();
        $addressOneContractor->setAddress("Some address for addresses street One building 1");
        $addressOneContractor->setCity("Kiev");
        $addressOneContractor->setZip("0005");
        $addressOneContractor->setState($this->getReference('state-new-york'));
        $addressOneContractor->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressOneContractor);
        $manager->flush();
        $this->addReference('address-one-for-contractor', $addressOneContractor);

        /******************************************************************/

        $addressOne = new Address();
        $addressOne->setAddress("Some address for addresses street One building 1");
        $addressOne->setCity("Kiev");
        $addressOne->setZip("0005");
        $addressOne->setState($this->getReference('state-new-york'));
        $addressOne->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressOne);
        $manager->flush();

        $this->addReference('address-one', $addressOne);

        $addressOneEmpty = new Address();
        $addressOneEmpty->setState($this->getReference('state-new-york'));
        $addressOneEmpty->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressOneEmpty);
        $manager->flush();
        $this->addReference('address-one-empty', $addressOneEmpty);

        /******************************************************************/

        $addressTwoEmpty = new Address();
        $addressTwoEmpty->setState($this->getReference('state-alaska'));
        $addressTwoEmpty->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressTwoEmpty);
        $manager->flush();
        $this->addReference('address-two-empty', $addressTwoEmpty);

        $addressTwo = new Address();
        $addressTwo->setAddress("Street and Building and other info");
        $addressTwo->setCity("Lviv");
        $addressTwo->setZip("121212");
        $addressTwo->setState($this->getReference('state-oregon'));
        $addressTwo->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressTwo);
        $manager->flush();
        $this->addReference('address-two', $addressTwo);

        /******************************************************************/

        $addressThree = new Address();
        $addressThree->setAddress("Just info about address");
        $addressThree->setCity("Paris");
        $addressThree->setZip("54454");
        $addressThree->setState($this->getReference('state-new-mexico'));
        $addressThree->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressThree);
        $manager->flush();
        $this->addReference('address-three', $addressThree);

        $addressFour = new Address();
        $addressFour->setAddress("fdfdseeewefafsdfsdfs sfsdfsdfv sefef34 dddd 33");
        $addressFour->setCity("Kiev");
        $addressFour->setZip("567890");
        $addressFour->setState($this->getReference('state-nevada'));
        $addressFour->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressFour);
        $manager->flush();
        $this->addReference('address-four', $addressFour);

        /******************************************************************/

        $addressFiveEmpty = new Address();
        $addressFiveEmpty->setState($this->getReference('state-alaska'));
        $addressFiveEmpty->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressFiveEmpty);
        $manager->flush();
        $this->addReference('address-five-empty', $addressFiveEmpty);

        $addressSixEmpty = new Address();
        $addressSixEmpty->setState($this->getReference('state-alaska'));
        $addressSixEmpty->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressSixEmpty);
        $manager->flush();
        $this->addReference('address-six-empty', $addressSixEmpty);

        /******************************************************************/

        $addressSevenEmpty = new Address();
        $addressSevenEmpty->setState($this->getReference('state-alaska'));
        $addressSevenEmpty->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressSevenEmpty);
        $manager->flush();
        $this->addReference('address-seven-empty', $addressSevenEmpty);

        $addressEightEmpty = new Address();
        $addressEightEmpty->setState($this->getReference('state-alaska'));
        $addressEightEmpty->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressEightEmpty);
        $manager->flush();
        $this->addReference('address-eight-empty', $addressEightEmpty);

        /******************************************************************/

        $addressNineEmpty1 = new Address();
        $addressNineEmpty1->setState($this->getReference('state-alaska'));
        $addressNineEmpty1->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressNineEmpty1);
        $manager->flush();
        $this->addReference('address-nine-empty-1', $addressNineEmpty1);

        $addressTenEmpty1 = new Address();
        $addressTenEmpty1->setState($this->getReference('state-alaska'));
        $addressTenEmpty1->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressTenEmpty1);
        $manager->flush();
        $this->addReference('address-ten-empty-1', $addressTenEmpty1);

        /******************************************************************/

        $addressNineEmpty2 = new Address();
        $addressNineEmpty2->setState($this->getReference('state-illinois'));
        $addressNineEmpty2->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressNineEmpty2);
        $manager->flush();
        $this->addReference('address-nine-empty-2', $addressNineEmpty2);

        $addressTenEmpty2 = new Address();
        $addressTenEmpty2->setState($this->getReference('state-illinois'));
        $addressTenEmpty2->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressTenEmpty2);
        $manager->flush();
        $this->addReference('address-ten-empty-2', $addressTenEmpty2);

        /******************************************************************/

        $addressForAccountOne = new Address();
        $addressForAccountOne->setState($this->getReference('state-massachusetts'));
        $addressForAccountOne->setAddress("Test address");
        $addressForAccountOne->setCity('Boston');
        $addressForAccountOne->setZip('09000');
        $addressForAccountOne->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountOne);
        $manager->flush();

        $this->addReference('address-for-account-one', $addressForAccountOne);

        /******************************************************************/

        $addressForGroupAccountOne = new Address();
        $addressForGroupAccountOne->setState($this->getReference('state-massachusetts'));
        $addressForGroupAccountOne->setAddress("some Test address");
        $addressForGroupAccountOne->setCity('Boston');
        $addressForGroupAccountOne->setZip('09000');
        $addressForGroupAccountOne->setAddressType($this->getReference('address-type-group'));
        $manager->persist($addressForGroupAccountOne);
        $manager->flush();

        $this->addReference('address-for-group-account-one', $addressForGroupAccountOne);

        /******************************************************************/

        $addressForAccountTwo = new Address();
        $addressForAccountTwo->setState($this->getReference('state-massachusetts'));
        $addressForAccountTwo->setAddress("Some address");
        $addressForAccountTwo->setCity('Boston');
        $addressForAccountTwo->setZip('09000');
        $addressForAccountTwo->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountTwo);
        $manager->flush();

        $this->addReference('address-for-account-two', $addressForAccountTwo);

        /******************************************************************/

        $addressForAccountThree = new Address();
        $addressForAccountThree->setState($this->getReference('state-massachusetts'));
        $addressForAccountThree->setAddress("Test address for account");
        $addressForAccountThree->setCity('New York');
        $addressForAccountThree->setZip('09000');
        $addressForAccountThree->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountThree);
        $manager->flush();

        $this->addReference('address-for-account-three', $addressForAccountThree);

        /******************************************************************/

        $addressForAccountFive = new Address();
        $addressForAccountFive->setState($this->getReference('state-alabama'));
        $addressForAccountFive->setAddress("Test address for account");
        $addressForAccountFive->setCity('Kiev');
        $addressForAccountFive->setZip('354792');
        $addressForAccountFive->setAddressType($this->getReference('address-type-group'));
        $manager->persist($addressForAccountFive);
        $manager->flush();

        $this->addReference('address-for-account-five', $addressForAccountFive);

        /******************************************************************/

        $addressForAccountMcDonalds = new Address();
        $addressForAccountMcDonalds->setState($this->getReference('state-alabama'));
        $addressForAccountMcDonalds->setAddress("some test address lines");
        $addressForAccountMcDonalds->setCity('Odessa');
        $addressForAccountMcDonalds->setZip('354792');
        $addressForAccountMcDonalds->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountMcDonalds);
        $manager->flush();

        $this->addReference('address-for-account-mc-donalds', $addressForAccountMcDonalds);

        /******************************************************************/

        $addressForAccountNoName = new Address();
        $addressForAccountNoName->setState($this->getReference('state-illinois'));
        $addressForAccountNoName->setAddress("test address");
        $addressForAccountNoName->setCity('some city');
        $addressForAccountNoName->setZip('0');
        $addressForAccountNoName->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountNoName);
        $manager->flush();

        $this->addReference('address-for-account-no-name', $addressForAccountNoName);

        /******************************************************************/

        $addressForAccountSix = new Address();
        $addressForAccountSix->setState($this->getReference('state-california'));
        $addressForAccountSix->setAddress("test address");
        $addressForAccountSix->setCity('City');
        $addressForAccountSix->setZip('999999');
        $addressForAccountSix->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountSix);
        $manager->flush();

        $this->addReference('address-for-account-six', $addressForAccountSix);

        /******************************************************************/

        $addressForAccountSeven = new Address();
        $addressForAccountSeven->setState($this->getReference('state-alaska'));
        $addressForAccountSeven->setAddress("Str # 1234!");
        $addressForAccountSeven->setCity('Some city');
        $addressForAccountSeven->setZip('111111');
        $addressForAccountSeven->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountSeven);
        $manager->flush();

        $this->addReference('address-for-account-seven', $addressForAccountSeven);

        /******************************************************************/

        $addressForAccountTest = new Address();
        $addressForAccountTest->setState($this->getReference('state-alabama'));
        $addressForAccountTest->setAddress("efuwygierwhfiheiwh");
        $addressForAccountTest->setCity('City');
        $addressForAccountTest->setZip('324234');
        $addressForAccountTest->setAddressType($this->getReference('address-type-group'));
        $manager->persist($addressForAccountTest);
        $manager->flush();

        $this->addReference('address-for-account-test', $addressForAccountTest);

        /******************************************************************/

        $addressForAccount10 = new Address();
        $addressForAccount10->setState($this->getReference('state-alabama'));
        $addressForAccount10->setAddress("Tets Ndegw");
        $addressForAccount10->setCity('defwikoiojgip90e');
        $addressForAccount10->setZip('3424234');
        $addressForAccount10->setAddressType($this->getReference('address-type-group'));
        $manager->persist($addressForAccount10);
        $manager->flush();

        $this->addReference('address-for-account-10', $addressForAccount10);

        /******************************************************************/

        $addressForAccount89 = new Address();
        $addressForAccount89->setState($this->getReference('state-california'));
        $addressForAccount89->setAddress("Tets Ndegw");
        $addressForAccount89->setCity('City');
        $addressForAccount89->setZip('999999');
        $addressForAccount89->setAddressType($this->getReference('address-type-group'));
        $manager->persist($addressForAccount89);
        $manager->flush();

        $this->addReference('address-for-account-89', $addressForAccount89);

        /******************************************************************/


        $addressForAccountNine = new Address();
        $addressForAccountNine->setState($this->getReference('state-alaska'));
        $addressForAccountNine->setAddress("Str # 1234!");
        $addressForAccountNine->setCity('Some city fhs');
        $addressForAccountNine->setZip('111111');
        $addressForAccountNine->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountNine);
        $manager->flush();

        $this->addReference('address-for-account-nine', $addressForAccountNine);

        /******************************************************************/

        $addressForAccountAcc = new Address();
        $addressForAccountAcc->setState($this->getReference('state-california'));
        $addressForAccountAcc->setAddress("Some address");
        $addressForAccountAcc->setCity('City of fdskh');
        $addressForAccountAcc->setZip('999999');
        $addressForAccountAcc->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountAcc);
        $manager->flush();

        $this->addReference('address-for-account-acc', $addressForAccountAcc);

        /******************************************************************/

        $addressForAccountBfh = new Address();
        $addressForAccountBfh->setState($this->getReference('state-california'));
        $addressForAccountBfh->setAddress("Str Alefwhfefrrnbndfv");
        $addressForAccountBfh->setCity('Some city');
        $addressForAccountBfh->setZip('111111');
        $addressForAccountBfh->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountBfh);
        $manager->flush();

        $this->addReference('address-for-account-bfh', $addressForAccountBfh);

        /******************************************************************/

        $addressForAccountDeletedOne = new Address();
        $addressForAccountDeletedOne->setState($this->getReference('state-alaska'));
        $addressForAccountDeletedOne->setAddress("Some address info for deleted account 11111");
        $addressForAccountDeletedOne->setCity('Kiev');
        $addressForAccountDeletedOne->setZip('121212');
        $addressForAccountDeletedOne->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountDeletedOne);
        $manager->flush();

        $this->addReference('address-for-account-deleted-one', $addressForAccountDeletedOne);

        /******************************************************************/

        $addressForAccountDeletedTwo = new Address();
        $addressForAccountDeletedTwo->setState($this->getReference('state-alaska'));
        $addressForAccountDeletedTwo->setAddress("Some address info for deleted account 22222");
        $addressForAccountDeletedTwo->setCity('Lviv');
        $addressForAccountDeletedTwo->setZip('3333333');
        $addressForAccountDeletedTwo->setAddressType($this->getReference('address-type-group'));
        $manager->persist($addressForAccountDeletedTwo);
        $manager->flush();

        $this->addReference('address-for-account-deleted-two', $addressForAccountDeletedTwo);

        /******************************************************************/

        $addressForAccountForChangingContactPersonRolesTesting = new Address();
        $addressForAccountForChangingContactPersonRolesTesting->setState($this->getReference('state-alabama'));
        $addressForAccountForChangingContactPersonRolesTesting->setAddress("17A, Kharkivske Way");
        $addressForAccountForChangingContactPersonRolesTesting->setCity('Kyiv');
        $addressForAccountForChangingContactPersonRolesTesting->setZip('01999');
        $addressForAccountDeletedTwo->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressForAccountForChangingContactPersonRolesTesting);
        $manager->flush();

        $this->addReference('address-for-account-for-changing-contact-person-roles', $addressForAccountForChangingContactPersonRolesTesting);
        /******************************************************************/

        $addressForCompanyOne = new Address();
        $addressForCompanyOne->setState($this->getReference('state-colorado'));
        $addressForCompanyOne->setAddress("Some address for Company One street One building 1");
        $addressForCompanyOne->setCity('Kiev');
        $addressForCompanyOne->setZip('00005');
        $addressForCompanyOne->setAddressType($this->getReference('address-type-company'));
        $manager->persist($addressForCompanyOne);
        $manager->flush();

        $this->addReference('address-for-company-one', $addressForCompanyOne);

        /******************************************************************/

        $addressForCompanyTwo = new Address();
        $addressForCompanyTwo->setState($this->getReference('state-new-york'));
        $addressForCompanyTwo->setAddress("Another address for Company Two street  street 1");
        $addressForCompanyTwo->setCity('New York');
        $addressForCompanyTwo->setZip('232323');
        $addressForCompanyTwo->setAddressType($this->getReference('address-type-company'));
        $manager->persist($addressForCompanyTwo);
        $manager->flush();

        $this->addReference('address-for-company-two', $addressForCompanyTwo);

        /******************************************************************/

        $addressForCompanyThree = new Address();
        $addressForCompanyThree->setState($this->getReference('state-kentucky'));
        $addressForCompanyThree->setAddress("Another address for Company Three");
        $addressForCompanyThree->setCity('Lviv');
        $addressForCompanyThree->setZip('222222');
        $addressForCompanyThree->setAddressType($this->getReference('address-type-company'));
        $manager->persist($addressForCompanyThree);
        $manager->flush();

        $this->addReference('address-for-company-three', $addressForCompanyThree);

        /******************************************************************/

        $addressForCompanyFour = new Address();
        $addressForCompanyFour->setState($this->getReference('state-new-hampshire'));
        $addressForCompanyFour->setAddress("12 address for Company Four");
        $addressForCompanyFour->setCity('Odessa');
        $addressForCompanyFour->setZip('688900');
        $addressForCompanyFour->setAddressType($this->getReference('address-type-company'));
        $manager->persist($addressForCompanyFour);
        $manager->flush();

        $this->addReference('address-for-company-four', $addressForCompanyFour);

        /******************************************************************/

        $addressForCompanyMcDonalds = new Address();
        $addressForCompanyMcDonalds->setState($this->getReference('state-new-hampshire'));
        $addressForCompanyMcDonalds->setAddress("Lyskivska, str. 7-a, app, 149");
        $addressForCompanyMcDonalds->setCity('Berezan');
        $addressForCompanyMcDonalds->setZip('1991');
        $addressForCompanyMcDonalds->setAddressType($this->getReference('address-type-company'));
        $manager->persist($addressForCompanyMcDonalds);
        $manager->flush();

        $this->addReference('address-for-company-mc-donalds', $addressForCompanyMcDonalds);

        $addressEleven = new Address();
        $addressEleven->setAddress('1540 N Old Rand Rd');
        $addressEleven->setCity('Chicago');
        $addressEleven->setState($this->getReference('state-illinois'));
        $addressEleven->setZip('60084');
        $addressEleven->setAddressType($this->getReference('address-type-contractor'));
        $manager->persist($addressEleven);
        $manager->flush();
        $this->addReference('address-american-backflow', $addressEleven);

        $addressTwelve = new Address();
        $addressTwelve->setAddress('600 East Broadway');
        $addressTwelve->setCity('Chicago');
        $addressTwelve->setState($this->getReference('state-illinois'));
        $addressTwelve->setZip('65201');
        $manager->persist($addressTwelve);
        $manager->flush();
        $this->addReference('address-twelve', $addressTwelve);

        $addressThirteen = new Address();
        $addressThirteen->setAddress('601 East Broadway');
        $addressThirteen->setCity('Chicago');
        $addressThirteen->setState($this->getReference('state-illinois'));
        $addressThirteen->setZip('65201');
        $addressThirteen->setAddressType($this->getReference('address-type-contractor'));
        $manager->persist($addressThirteen);
        $manager->flush();
        $this->addReference('address-thirteen', $addressThirteen);

        $addressFourteen = new Address();
        $addressFourteen->setAddress('602 East Broadway');
        $addressFourteen->setCity('Chicago');
        $addressFourteen->setState($this->getReference('state-illinois'));
        $addressFourteen->setZip('65201');
        $addressFourteen->setAddressType($this->getReference('address-type-contractor'));
        $manager->persist($addressFourteen);
        $manager->flush();
        $this->addReference('address-fourteen', $addressFourteen);

        $addressFifteen = new Address();
        $addressFifteen->setAddress('36 N. Apple');
        $addressFifteen->setCity('Chicago');
        $addressFifteen->setState($this->getReference('state-illinois'));
        $addressFifteen->setZip('60052');
        $addressFifteen->setAddressType($this->getReference('address-type-contractor'));
        $manager->persist($addressFifteen);
        $manager->flush();
        $this->addReference('address-fifteen', $addressFifteen);

        $addressFifteen2 = new Address();
        $addressFifteen2->setAddress('36 N. Apple');
        $addressFifteen2->setCity('Chicago');
        $addressFifteen2->setState($this->getReference('state-illinois'));
        $addressFifteen2->setZip('60052');
        $manager->persist($addressFifteen2);
        $manager->flush();
        $this->addReference('address-fifteen-2', $addressFifteen2);

        $addressFifteen3 = new Address();
        $addressFifteen3->setAddress('36 N. Apple');
        $addressFifteen3->setCity('Chicago');
        $addressFifteen3->setState($this->getReference('state-illinois'));
        $addressFifteen3->setZip('60052');
        $manager->persist($addressFifteen3);
        $manager->flush();
        $this->addReference('address-fifteen-3', $addressFifteen3);

        $addressFifteen4 = new Address();
        $addressFifteen4->setAddress('36 N. Apple');
        $addressFifteen4->setCity('Chicago');
        $addressFifteen4->setState($this->getReference('state-illinois'));
        $addressFifteen4->setZip('60052');
        $manager->persist($addressFifteen4);
        $manager->flush();
        $this->addReference('address-fifteen-4', $addressFifteen4);

        /************************************************/

        $addressDepartmentChannel1 = new Address();
        $addressDepartmentChannel1->setAddress("Street 1");
        $addressDepartmentChannel1->setCity("Chicago");
        $addressDepartmentChannel1->setZip("1234");
        $addressDepartmentChannel1->setState($this->getReference('state-illinois'));
        $addressDepartmentChannel1->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressDepartmentChannel1);
        $manager->flush();
        $this->addReference('address-department-channel-1', $addressDepartmentChannel1);


        $addressDepartmentChannel2 = new Address();
        $addressDepartmentChannel2->setAddress("Street 1");
        $addressDepartmentChannel2->setCity("Chicago");
        $addressDepartmentChannel2->setZip("1234");
        $addressDepartmentChannel2->setState($this->getReference('state-illinois'));
        $addressDepartmentChannel2->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressDepartmentChannel2);
        $manager->flush();
        $this->addReference('address-department-channel-2', $addressDepartmentChannel2);

        $address16 = new Address();
        $address16->setAddress('Street 2');
        $address16->setCity('Chicago');
        $address16->setZip('1234');
        $address16->setState($this->getReference('state-illinois'));
        $address16->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($address16);
        $manager->flush();
        $this->addReference('address-sixteen', $address16);

        $address17 = new Address();
        $address17->setAddress('Street 3');
        $address17->setCity('Chicago');
        $address17->setZip('1234');
        $address17->setState($this->getReference('state-illinois'));
        $address17->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($address17);
        $manager->flush();
        $this->addReference('address-seventeen', $address17);

        $address18 = new Address();
        $address18->setAddress('Street 4');
        $address18->setCity('Chicago');
        $address18->setZip('1234');
        $address18->setState($this->getReference('state-illinois'));
        $address18->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($address18);
        $manager->flush();
        $this->addReference('address-eighteen', $address18);

        /************************************************/

        $addressAgentChannel1 = new Address();
        $addressAgentChannel1->setAddress("Street 1");
        $addressAgentChannel1->setCity("Lviv");
        $addressAgentChannel1->setZip("222222");
        $addressAgentChannel1->setState($this->getReference('state-illinois'));
        $addressAgentChannel1->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressAgentChannel1);
        $manager->flush();
        $this->addReference('address-agent-channel-1', $addressAgentChannel1);


        $addressAgentChannel2 = new Address();
        $addressAgentChannel2->setAddress("Street or no street 23");
        $addressAgentChannel2->setCity("Kiev");
        $addressAgentChannel2->setZip("23456");
        $addressAgentChannel2->setState($this->getReference('state-alaska'));
        $addressAgentChannel2->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressAgentChannel2);
        $manager->flush();
        $this->addReference('address-agent-channel-2', $addressAgentChannel2);


        $addressAgentChannel3 = new Address();
        $addressAgentChannel3->setAddress("Build");
        $addressAgentChannel3->setCity("Dnepr");
        $addressAgentChannel3->setZip("88998");
        $addressAgentChannel3->setState($this->getReference('state-new-york'));
        $addressAgentChannel3->setAddressType($this->getReference('address-type-personal'));
        $manager->persist($addressAgentChannel3);
        $manager->flush();
        $this->addReference('address-agent-channel-3', $addressAgentChannel3);

        $addressBankOfNewYork = new Address();
        $addressBankOfNewYork->setAddress('225 Liberty Street');
        $addressBankOfNewYork->setCity('New York');
        $addressBankOfNewYork->setZip('10286');
        $addressBankOfNewYork->setState($this->getReference('state-new-york'));
        $addressBankOfNewYork->setAddressType($this->getReference('address-type-site'));
        $manager->persist($addressBankOfNewYork);
        $manager->flush();
        $this->addReference('address-bank-of-new-york', $addressBankOfNewYork);
        
        $addressNewYourkCity = new Address();
        $addressNewYourkCity->setAddress('City Hall');
        $addressNewYourkCity->setCity('New York');
        $addressNewYourkCity->setZip('10007');
        $addressNewYourkCity->setState($this->getReference('state-new-york'));
        $manager->persist($addressNewYourkCity);
        $manager->flush();
        $this->addReference('address-new-york-city', $addressNewYourkCity);
    }

    public function getOrder()
    {
        return 2;
    }
}
