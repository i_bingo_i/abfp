<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\RangeQuestionStatus;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRangeQuestionStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $closedTightHeldRangeStatus = new RangeQuestionStatus();
        $closedTightHeldRangeStatus->setName('Closed Tight/Held');
        $closedTightHeldRangeStatus->setAlias('closed_tight_held');
        $closedTightHeldRangeStatus->setIsFail(0);
        $manager->persist($closedTightHeldRangeStatus);
        $manager->flush();

        $this->addReference('range-status-closed-tight-held', $closedTightHeldRangeStatus);


        $leakedRangeStatus = new RangeQuestionStatus();
        $leakedRangeStatus->setName('Leaked');
        $leakedRangeStatus->setAlias('leaked');
        $leakedRangeStatus->setIsFail(1);
        $manager->persist($leakedRangeStatus);
        $manager->flush();

        $this->addReference('range-status-leaked', $leakedRangeStatus);


        $openRangeStatus = new RangeQuestionStatus();
        $openRangeStatus->setName('Open');
        $openRangeStatus->setAlias('open');
        $openRangeStatus->setIsFail(0);
        $manager->persist($openRangeStatus);
        $manager->flush();

        $this->addReference('range-status-open', $openRangeStatus);

        $didNotOpenRangeStatus = new RangeQuestionStatus();
        $didNotOpenRangeStatus->setName('Did Not Open');
        $didNotOpenRangeStatus->setAlias('did_not_open');
        $didNotOpenRangeStatus->setIsFail(1);
        $manager->persist($didNotOpenRangeStatus);
        $manager->flush();

        $this->addReference('range-status-did-not-open', $didNotOpenRangeStatus);
    }

    public function getOrder()
    {
        return 1;
    }
}
