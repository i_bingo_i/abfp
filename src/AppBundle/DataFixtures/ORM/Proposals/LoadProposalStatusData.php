<?php

namespace AppBundle\DataFixtures\ORM\Proposals;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ProposalStatus;

class LoadProposalStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $draft = new ProposalStatus();
        $draft->setName('Draft');
        $draft->setAlias('draft');
        $draft->setPriority(0);
        $draft->setCssClass('app-status-label--yellow');
        $manager->persist($draft);
        $manager->flush();
        $this->addReference('proposal-status-draft', $draft);

        $toBeSent = new ProposalStatus();
        $toBeSent->setName('To Be Sent');
        $toBeSent->setAlias('to_be_sent');
        $toBeSent->setCssClass('app-status-label--yellow');
        $toBeSent->setPriority(1);
        $manager->persist($toBeSent);
        $manager->flush();
        $this->addReference('proposal-status-to-be-sent', $toBeSent);

        $replacing = new ProposalStatus();
        $replacing->setName('Replacing');
        $replacing->setAlias('replacing');
        $replacing->setCssClass('app-status-label--yellow');
        $replacing->setPriority(1);
        $manager->persist($replacing);
        $manager->flush();
        $this->addReference('proposal-status-replacing', $replacing);


        $sentViaMail = new ProposalStatus();
        $sentViaMail->setName('Sent via Mail');
        $sentViaMail->setAlias('sent_via_mail');
        $sentViaMail->setCssClass('app-status-label--green');
        $sentViaMail->setPriority(2);
        $manager->persist($sentViaMail);
        $manager->flush();
        $this->addReference('proposal-status-sent-via-mail', $sentViaMail);

        $sentViaEmail = new ProposalStatus();
        $sentViaEmail->setName('Sent via Email');
        $sentViaEmail->setAlias('sent_via_email');
        $sentViaEmail->setCssClass('app-status-label--green');
        $sentViaEmail->setPriority(2);
        $manager->persist($sentViaEmail);
        $manager->flush();
        $this->addReference('proposal-status-sent-via-email', $sentViaEmail);

        $sentViaMailAndEmail = new ProposalStatus();
        $sentViaMailAndEmail->setName('Sent via Mail and Email');
        $sentViaMailAndEmail->setAlias('sent_via_mail_and_email');
        $sentViaMailAndEmail->setCssClass('app-status-label--green');
        $sentViaMailAndEmail->setPriority(3);
        $manager->persist($sentViaMailAndEmail);
        $manager->flush();
        $this->addReference('proposal-status-sent-via-mail-and-email', $sentViaMailAndEmail);

        $callNeeded = new ProposalStatus();
        $callNeeded->setName('Call Needed');
        $callNeeded->setAlias('call_needed');
        $callNeeded->setCssClass('app-status-label--red');
        $callNeeded->setPriority(4);
        $manager->persist($callNeeded);
        $manager->flush();
        $this->addReference('proposal-status-call-needed', $callNeeded);

        $clientWasCalled = new ProposalStatus();
        $clientWasCalled->setName('Client Was Called');
        $clientWasCalled->setAlias('client_was_called');
        $clientWasCalled->setCssClass('app-status-label--green');
        $clientWasCalled->setPriority(5);
        $manager->persist($clientWasCalled);
        $manager->flush();
        $this->addReference('proposal-status-client-was-called', $clientWasCalled);

        $sendEmailReminder = new ProposalStatus();
        $sendEmailReminder->setName('Send Email Reminder');
        $sendEmailReminder->setAlias('send_email_reminder');
        $sendEmailReminder->setCssClass('app-status-label--red');
        $sendEmailReminder->setPriority(6);
        $manager->persist($sendEmailReminder);
        $manager->flush();
        $this->addReference('proposal-status-send-email-reminder', $sendEmailReminder);

        $emailReminderSent = new ProposalStatus();
        $emailReminderSent->setName('Email Reminder Sent');
        $emailReminderSent->setAlias('email_reminder_sent');
        $emailReminderSent->setCssClass('app-status-label--green');
        $emailReminderSent->setPriority(7);
        $manager->persist($emailReminderSent);
        $manager->flush();
        $this->addReference('proposal-status-email-reminder-sent', $emailReminderSent);

        $pastDue = new ProposalStatus();
        $pastDue->setName('Past Due');
        $pastDue->setAlias('past_due');
        $pastDue->setCssClass('app-status-label--red');
        $pastDue->setPriority(8);
        $manager->persist($pastDue);
        $manager->flush();
        $this->addReference('proposal-status-past-due', $pastDue);

        $sendPastDueEmail = new ProposalStatus();
        $sendPastDueEmail->setName('Sent Past Due Email');
        $sendPastDueEmail->setAlias('send_past_due_email');
        $sendPastDueEmail->setCssClass('app-status-label--red');
        $sendPastDueEmail->setPriority(9);
        $manager->persist($sendPastDueEmail);
        $manager->flush();
        $this->addReference('proposal-status-send-past-due-email', $sendPastDueEmail);

        $rejectedByClient = new ProposalStatus();
        $rejectedByClient->setName('Rejected By Client');
        $rejectedByClient->setAlias('rejected_by_client');
        $rejectedByClient->setCssClass('app-status-label--red');
        $rejectedByClient->setPriority(11);
        $manager->persist($rejectedByClient);
        $manager->flush();
        $this->addReference('proposal-status-rejected-by-client', $rejectedByClient);

        $noResponse = new ProposalStatus();
        $noResponse->setName('No Response');
        $noResponse->setAlias('no_response');
        $noResponse->setCssClass('app-status-label--red');
        $noResponse->setPriority(10);
        $manager->persist($noResponse);
        $manager->flush();
        $this->addReference('proposal-status-no-response', $noResponse);

//        $woCreated = new ProposalStatus();
//        $woCreated->setName('WO CREATED');
//        $woCreated->setAlias('wo_created');
//        $woCreated->setPriority(10);
//        $manager->persist($woCreated);
//        $manager->flush();
//        $this->addReference('proposal-status-wo-created', $woCreated);

        $deleted = new ProposalStatus();
        $deleted->setName('Deleted');
        $deleted->setAlias('deleted');
        $deleted->setCssClass('app-status-label--gray');
        $deleted->setPriority(12);
        $manager->persist($deleted);
        $manager->flush();
        $this->addReference('proposal-status-deleted', $deleted);

        $resetNextTime = new ProposalStatus();
        $resetNextTime->setName('Services Were Reset');
        $resetNextTime->setAlias('reset_services_to_next_time');
        $resetNextTime->setCssClass('app-status-label--gray');
        $resetNextTime->setPriority(12);
        $manager->persist($resetNextTime);
        $manager->flush();
        $this->addReference('proposal-status-reset-services-to-next-time', $resetNextTime);

        $workorderWasCreated = new ProposalStatus();
        $workorderWasCreated->setName('Workorder created');
        $workorderWasCreated->setAlias('workorder_created');
        $workorderWasCreated->setCssClass('app-status-label--gray');
        $workorderWasCreated->setPriority(12);
        $manager->persist($workorderWasCreated);
        $manager->flush();
        $this->addReference('workorder-created', $workorderWasCreated);


        $servicesDiscarded = new ProposalStatus();
        $servicesDiscarded->setName('Services Discarded');
        $servicesDiscarded->setAlias('services_discarded');
        $servicesDiscarded->setCssClass('app-status-label--gray');
        $servicesDiscarded->setPriority(12);
        $manager->persist($servicesDiscarded);
        $manager->flush();
        $this->addReference('services-discarded', $servicesDiscarded);

        $replaced = new ProposalStatus();
        $replaced->setName('Replaced');
        $replaced->setAlias('replaced');
        $replaced->setCssClass('app-status-label--gray');
        $replaced->setPriority(12);
        $manager->persist($replaced);
        $manager->flush();
        $this->addReference('replaced', $replaced);
    }

    public function getOrder()
    {
        return 1;
    }
}
