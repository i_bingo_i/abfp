<?php

namespace AppBundle\DataFixtures\ORM\Proposals;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ProposalLifeCycle;

class LoadProposalLifeCycleData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $proposalLifeCycle = new ProposalLifeCycle();
        $proposalLifeCycle->setName('Proposal Life Cycle');
        $proposalLifeCycle->setAlias('proposal_life_cycle');
        $manager->persist($proposalLifeCycle);
        $manager->flush();
        $this->addReference('proposal-life-cycle', $proposalLifeCycle);


        $retestNoticeLifeCycle = new ProposalLifeCycle();
        $retestNoticeLifeCycle->setName('Retest Notice Life Cycle');
        $retestNoticeLifeCycle->setAlias('retest_notice_life_cycle');
        $manager->persist($retestNoticeLifeCycle);
        $manager->flush();
        $this->addReference('retest-notice-life-cycle', $retestNoticeLifeCycle);

    }

    public function getOrder()
    {
        return 1;
    }
}
