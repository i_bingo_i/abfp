<?php

namespace AppBundle\DataFixtures\ORM\Proposals;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ProposalType;

class LoadProposalTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $retestProposalType = new ProposalType();
        $retestProposalType->setName('Retest');
        $retestProposalType->setAlias('retest');
        $manager->persist($retestProposalType);
        $manager->flush();

        $this->addReference('proposal-type-retest', $retestProposalType);

        $repairProposalType = new ProposalType();
        $repairProposalType->setName('Repair');
        $repairProposalType->setAlias('repair');
        $manager->persist($repairProposalType);
        $manager->flush();

        $this->addReference('proposal-type-repair', $repairProposalType);


        $customProposalType = new ProposalType();
        $customProposalType->setName('Custom');
        $customProposalType->setAlias('custom');
        $manager->persist($customProposalType);
        $manager->flush();

        $this->addReference('proposal-type-custom', $customProposalType);
    }

    public function getOrder()
    {
        return 1;
    }
}
