<?php

namespace AppBundle\DataFixtures\ORM\Proposals;

use AppBundle\Entity\ProposalLogActionNamed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class LoadProposalLogActionNamedData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $proposalLogType = new ProposalLogActionNamed();
        $proposalLogType->setName('Send via email');
        $proposalLogType->setAlias('send_via_email');
        $proposalLogType->setRoute('admin_letter_proposal');

        $manager->persist($proposalLogType);
        $manager->flush();

        $proposalLogType2 = new ProposalLogActionNamed();
        $proposalLogType2->setName('Notice was sent via Mail');
        $proposalLogType2->setAlias('notice_was_sent_via_mail');
        $proposalLogType2->setRoute('admin_letter_mail_proposal');

        $manager->persist($proposalLogType2);
        $manager->flush();

        $proposalLogType3 = new ProposalLogActionNamed();
        $proposalLogType3->setName('Client was called');
        $proposalLogType3->setAlias('client_was_called');
        $proposalLogType3->setRoute('admin_proposal_client_was_called');

        $manager->persist($proposalLogType3);
        $manager->flush();

        $proposalLogType4 = new ProposalLogActionNamed();
        $proposalLogType4->setName('Reset Services to Next Time');
        $proposalLogType4->setAlias('reset_services_to_next_time');
        $proposalLogType4->setRoute('admin_proposal_reset_services_next_time');

        $manager->persist($proposalLogType4);
        $manager->flush();

        $proposalLogType5 = new ProposalLogActionNamed();
        $proposalLogType5->setName('Send Email Reminder');
        $proposalLogType5->setAlias('send_email_reminder');
        $proposalLogType5->setRoute('admin_letter_email_reminder');

        $manager->persist($proposalLogType5);
        $manager->flush();

        $proposalLogType6 = new ProposalLogActionNamed();
        $proposalLogType6->setName('Link');
        $proposalLogType6->setAlias('link');

        $manager->persist($proposalLogType6);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}