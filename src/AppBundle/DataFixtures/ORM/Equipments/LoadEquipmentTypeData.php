<?php
namespace AppBundle\DataFixtures\ORM\Equipments;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\EquipmentType;

class LoadEquipmentTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $backflowEquipmentType = new EquipmentType();
        $backflowEquipmentType->setName("Backflow Equipment");
        $backflowEquipmentType->setAlias('backflow equipment');
        $manager->persist($backflowEquipmentType);
        $manager->flush();
        $this->addReference('backflow-equipment-type', $backflowEquipmentType);
    }

    public function getOrder()
    {
        return 1;
    }
}
