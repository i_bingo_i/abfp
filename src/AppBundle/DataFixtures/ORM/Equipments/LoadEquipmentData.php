<?php
namespace AppBundle\DataFixtures\ORM\Equipments;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Equipment;

class LoadEquipmentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $equipment1 = new Equipment();
        $equipment1->setMake("Gauge");
        $equipment1->setModel("Turbo");
        $equipment1->setSerialNumber("23232232323");
        $equipment1->setLastCalibratedDate(new \DateTime("10.12.2014"));
        $equipment1->setType($this->getReference("backflow-equipment-type"));
        $equipment1->setCompanySuppliedGauge(true);
        $equipment1->setContractorUser($this->getReference("contractor-user-dan-harbut"));
        $manager->persist($equipment1);
        $manager->flush();

        $this->addReference('equipment-1', $equipment1);


        $equipment2 = new Equipment();
        $equipment2->setMake("Gauge 1");
        $equipment2->setModel("Super");
        $equipment2->setSerialNumber("ER244234334");
        $equipment2->setType($this->getReference("backflow-equipment-type"));
        $equipment2->setLastCalibratedDate(new \DateTime("10.12.2016"));
        $equipment2->setCompanySuppliedGauge(true);
        $equipment2->setContractorUser($this->getReference("contractor-user-john-doe-for-monarch"));
        $manager->persist($equipment2);
        $manager->flush();

        $this->addReference('equipment-2', $equipment2);


        $equipment3 = new Equipment();
        $equipment3->setMake("New Gauge");
        $equipment3->setModel("Lightsaber");
        $equipment3->setSerialNumber("86654542323");
        $equipment3->setLastCalibratedDate(new \DateTime("19.02.2016"));
        $equipment3->setType($this->getReference("backflow-equipment-type"));
        $equipment3->setContractorUser($this->getReference("contractor-user-john-doe-for-monarch"));
        $manager->persist($equipment3);
        $manager->flush();

        $this->addReference('equipment-3', $equipment3);


        $equipment4 = new Equipment();
        $equipment4->setMake("Good Gauge 2");
        $equipment4->setModel("Death Star");
        $equipment4->setSerialNumber("45434534535");
        $equipment4->setLastCalibratedDate(new \DateTime("22.07.2017"));
        $equipment4->setType($this->getReference("backflow-equipment-type"));
        $manager->persist($equipment4);
        $manager->flush();

        $this->addReference('equipment-4', $equipment4);


        $equipment5 = new Equipment();
        $equipment5->setMake("Not Bad Gauge");
        $equipment5->setModel("Millennium Falcon");
        $equipment5->setSerialNumber("TP56734334");
        $equipment5->setLastCalibratedDate(new \DateTime("01.03.2017"));
        $equipment5->setType($this->getReference("backflow-equipment-type"));
        $equipment5->setContractorUser($this->getReference("contractor-user-john-bishop-for-monarch"));
        $manager->persist($equipment5);
        $manager->flush();

        $this->addReference('equipment-5', $equipment5);


        $equipment6 = new Equipment();
        $equipment6->setMake("Very Good Gauge");
        $equipment6->setModel("R2D2");
        $equipment6->setSerialNumber("7754543453");
        $equipment6->setLastCalibratedDate(new \DateTime("12.12.2016"));
        $equipment6->setType($this->getReference("backflow-equipment-type"));
        $equipment6->setContractorUser($this->getReference("contractor-user-james-hatfield-for-monarch"));
        $manager->persist($equipment6);
        $manager->flush();

        $this->addReference('equipment-6', $equipment6);


        $equipment7 = new Equipment();
        $equipment7->setMake("Super Good Gauge");
        $equipment7->setModel("R2D2");
        $equipment7->setSerialNumber("4545457876");
        $equipment7->setLastCalibratedDate(new \DateTime("12.12.2017"));
        $equipment7->setType($this->getReference("backflow-equipment-type"));
        $equipment7->setContractorUser($this->getReference("contractor-user-gabe-newell"));
        $manager->persist($equipment7);
        $manager->flush();

        $this->addReference('equipment-7', $equipment7);

        $equipment8 = new Equipment();
        $equipment8->setMake("Strange Device. Don't touch this");
        $equipment8->setModel("C-3PO");
        $equipment8->setSerialNumber("54654645");
        $equipment8->setLastCalibratedDate(new \DateTime("02.02.2018"));
        $equipment8->setType($this->getReference("backflow-equipment-type"));
        $equipment8->setContractorUser($this->getReference("contractor-user-mark-zuckerberg"));
        $manager->persist($equipment8);
        $manager->flush();

        $this->addReference('equipment-8', $equipment8);


        $equipment9 = new Equipment();
        $equipment9->setMake("Super Device for everyone");
        $equipment9->setModel("RoboCop");
        $equipment9->setSerialNumber("5667689890");
        $equipment9->setLastCalibratedDate(new \DateTime("03.03.2018"));
        $equipment9->setType($this->getReference("backflow-equipment-type"));
        $equipment9->setContractorUser($this->getReference("contractor-user-sergey-brin"));
        $manager->persist($equipment9);
        $manager->flush();

        $this->addReference('equipment-9', $equipment9);


        $equipment10 = new Equipment();
        $equipment10->setMake("Main Gauge");
        $equipment10->setModel("T-1000");
        $equipment10->setSerialNumber("8785544398");
        $equipment10->setLastCalibratedDate(new \DateTime("04.04.2018"));
        $equipment10->setType($this->getReference("backflow-equipment-type"));
        $equipment10->setContractorUser($this->getReference("contractor-user-bill-gates"));
        $manager->persist($equipment10);
        $manager->flush();

        $this->addReference('equipment-10', $equipment10);

        $equipment11 = new Equipment();
        $equipment11->setMake("Main Gauge for SuperAdmin");
        $equipment11->setModel("T-1000");
        $equipment11->setSerialNumber("AA0001AA");
        $equipment11->setLastCalibratedDate(new \DateTime("05.05.2018"));
        $equipment11->setType($this->getReference("backflow-equipment-type"));
        $equipment11->setContractorUser($this->getReference("contractor-user-dan-harbut"));
        $manager->persist($equipment11);
        $manager->flush();

        $this->addReference('equipment-11', $equipment11);

        $equipment12 = new Equipment();
        $equipment12->setMake("Main Gauge for SuperAdmin");
        $equipment12->setModel("T-800");
        $equipment12->setSerialNumber("AA0987OI");
        $equipment12->setLastCalibratedDate(new \DateTime("02.03.2018"));
        $equipment12->setType($this->getReference("backflow-equipment-type"));
        $equipment12->setContractorUser($this->getReference("contractor-user-elon-mask"));
        $manager->persist($equipment12);
        $manager->flush();

        $this->addReference('equipment-12', $equipment12);
    }

    public function getOrder()
    {
        return 10;
    }
}
