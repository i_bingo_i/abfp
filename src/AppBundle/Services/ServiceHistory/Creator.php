<?php

namespace AppBundle\Services\ServiceHistory;

use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\ServiceHistoryManager;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalHistory;
use AppBundle\Entity\Repository\ProposalHistoryRepository;
use AppBundle\Entity\Repository\ServiceHistoryRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\User;
use AppBundle\Entity\ServiceHistory;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Creator
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var  TokenStorage */
    private $tokenStorage;
    /** @var ContainerInterface */
    private $container;
    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;
    /** @var ProposalHistoryRepository */
    private $proposalHistoryRepository;
    /** @var DepartmentChannelManager */
    private $departmentChannelManager;
    /** @var ServiceHistoryManager */
    private $serviceHistoryManager;

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorage $tokenStorage
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, TokenStorage $tokenStorage, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;
        $this->container = $container;

        $this->serviceHistoryRepository = $this->objectManager->getRepository('AppBundle:ServiceHistory');
        $this->proposalHistoryRepository = $this->objectManager->getRepository('AppBundle:ProposalHistory');
        $this->departmentChannelManager = $this->container->get('app.department_channel.manager');
        $this->serviceHistoryManager = $this->container->get('app.service_history.manager');
    }

    /**
     * @param Service $service
     */
    public function create(Service $service)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;

        /** @var ServiceHistory $serviceHistoryItem */
        $serviceHistoryItem = new ServiceHistory();
        $serviceHistoryItem->setNamed($service->getNamed());
        $serviceHistoryItem->setWorkorder($service->getWorkorder());
        $serviceHistoryItem->setDevice($service->getDevice());
        $serviceHistoryItem->setAccount($service->getAccount());
        $serviceHistoryItem->setDeleted($service->getDeleted());
        $serviceHistoryItem->setComment($service->getComment());
        $serviceHistoryItem->setCompanyLastTested($service->getCompanyLastTested());
        $serviceHistoryItem->setFixedPrice($service->getFixedPrice());
        $serviceHistoryItem->setHourlyPrice($service->getHourlyPrice());
        $serviceHistoryItem->setLastTested($service->getLastTested());
        $serviceHistoryItem->setInspectionDue($service->getInspectionDue());
        $serviceHistoryItem->setOwnerEntity($service);
        $serviceHistoryItem->setAuthor($user);
        $serviceHistoryItem->setLastReport($service->getLastReport());
        $serviceHistoryItem->setDateCreate(new \DateTime());
        $serviceHistoryItem->setDateUpdate($service->getDateUpdate());
        $serviceHistoryItem->setContractorService($service->getContractorService());
        $serviceHistoryItem->setDirectlyWO($service->getDirectlyWO());
        $serviceHistoryItem->setAddedFromIos($service->getAddedFromIos());

        /** @var Proposal $proposal */
        $proposal = $service->getProposal();
        if ($proposal) {
            /** @var ProposalHistory $proposalHistory */
            $proposalHistory = $this->proposalHistoryRepository->findLastByProposal($proposal);
            $serviceHistoryItem->setProposal($proposalHistory);
        } else {
            $serviceHistoryItem->setProposal();
        }

        $serviceHistoryItem->setStatus($service->getStatus());
        $serviceHistoryItem->setDepartmentChannel($this->setDepartmentChannel($serviceHistoryItem));
        $serviceHistoryItem->setTestDate($service->getTestDate());
        $this->setMunicipalityFee($serviceHistoryItem);
        $serviceHistoryItem->setDepartmentChannelWithFeesWillVary(
            $this->checkIfDepartmentChannelWithFeesWillVary($serviceHistoryItem)
        );

        $this->serviceHistoryManager->save($serviceHistoryItem);
    }

    /**
     * @param $servicesArray
     * @param $servicesHistoryArray
     * @return array
     */
    public function checkAndGetServicesHistory($servicesArray, $servicesHistoryArray)
    {
        if (empty($servicesHistoryArray)) {
            $serviceHistoryArray = $this->createAndGetAllServicesHistoryItems($servicesArray);
        } else {
            $serviceHistoryArray = $this->compareServicesAndGetServicesHistoryItems(
                $servicesArray,
                $servicesHistoryArray
            );
        }

        return $serviceHistoryArray;
    }

    /**
     * @param $servicesArray
     * @return array
     */
    private function createAndGetAllServicesHistoryItems($servicesArray)
    {
        foreach ($servicesArray as $service) {
            $this->create($service);
        }

        $serviceHistoryArray = $this->serviceHistoryRepository->getLastByService($servicesArray);

        return $serviceHistoryArray;
    }

    /**
     * @param $servicesArray
     * @param $servicesHistoryArray
     * @return array
     */
    private function compareServicesAndGetServicesHistoryItems($servicesArray, $servicesHistoryArray)
    {
        $servicesHistoryIdsArray = [];

        /** @var ServiceHistory $serviceHistory */
        foreach ($servicesHistoryArray as $serviceHistory) {
            $servicesHistoryIdsArray[] = $serviceHistory->getOwnerEntity()->getId();
        }
        /** @var Service $service */
        foreach ($servicesArray as $service) {
            if (!in_array($service->getId(), $servicesHistoryIdsArray)) {
                $this->create($service);
            }
        }
        $serviceHistoryArray = $this->serviceHistoryRepository->getLastByService($servicesArray);

        return $serviceHistoryArray;
    }

    /**
     * @param ServiceHistory $serviceHistory
     */
    private function setMunicipalityFee(ServiceHistory $serviceHistory)
    {
        /** @var DepartmentChannel $departmentChannel */
        $departmentChannel = $serviceHistory->getDepartmentChannel();
        $munAndProcFee = $this->departmentChannelManager->municipalityFee($departmentChannel);

        if ($serviceHistory->getNamed()->getIsNeedMunicipalityFee()) {
            $serviceHistory->setMunicipalityFee($munAndProcFee);
        }
    }

    /**
     * @param ServiceHistory $serviceHistory
     * @return bool
     */
    private function checkIfDepartmentChannelWithFeesWillVary(ServiceHistory $serviceHistory)
    {
        /** @var DepartmentChannel $departmentChannel */
        $departmentChannel = $serviceHistory->getDepartmentChannel();

        if (!$departmentChannel instanceof DepartmentChannel) {
            return false;
        }

        $isNeedMunicipalityFee = $serviceHistory->getNamed()->getIsNeedMunicipalityFee();

        if ($departmentChannel->getFeesWillVary() and $isNeedMunicipalityFee) {
            return true;
        }

        return false;
    }

    /**
     * @param ServiceHistory $serviceHistory
     * @return DepartmentChannel
     */
    private function setDepartmentChannel(ServiceHistory $serviceHistory)
    {
        /** @var Municipality $munAndProcFee */
        $municipality = $serviceHistory->getAccount()->getMunicipality();
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $serviceHistory->getDevice()->getNamed()->getCategory();
        /** @var DepartmentChannel $departmentChannel */
        $departmentChannel = $this->departmentChannelManager->getDefaultChannel($municipality, $deviceCategory);

        return $departmentChannel;
    }
}
