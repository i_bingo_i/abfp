<?php

namespace AppBundle\Services;

use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\DeviceManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\DeviceRepository;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output;

class DevicesProcessing
{
    /** @var ContainerInterface */
    private $container;
    /** @var  ObjectManager */
    private $objectManager;
    /** @var  DeviceManager */
    private $deviceManager;
    /** @var  DeviceRepository */
    private $deviceRepository;

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->deviceManager = $this->container->get('app.device.manager');
        $this->deviceRepository = $objectManager->getRepository("AppBundle:Device");
    }


    public function reSavedDevicesWithTitle()
    {
        /** @var Device[] $devices */
        $devices = $this->deviceRepository->getDevicesWithEmptyTitle();

        $output = new Output\ConsoleOutput();
        $progress = new ProgressBar($output, count($devices));
        $progress->start();

        $i=0;
        foreach ($devices as $device) {
            $device->setTitle($this->deviceManager->getTitle($device));
            $this->objectManager->persist($device);

            $progress->advance();

            if ($i % 1000 == 0 and $i!=0) {
                $this->objectManager->flush();
            }
            $i++;
        }

        $this->objectManager->flush();
        $progress->finish();
    }
}