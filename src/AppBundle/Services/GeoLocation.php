<?php

namespace AppBundle\Services;

use Geocoder\Model\Coordinates;
use AppBundle\Entity\Address;
use Bazinga\Bundle\GeocoderBundle\Geocoder\LoggableGeocoder;
//Geocoder exceptions
use Geocoder\Exception\NoResult;
use Geocoder\Exception\QuotaExceeded;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class GeoLocation
{
    /** @var ContainerInterface */
    private $container;
    /** @var LoggableGeocoder */
    private $bazingaGeocoder;
    /** @var Session */
    private $session;

    public function __construct(ContainerInterface $container)
    {
        /** @var ContainerInterface */
        $this->container = $container;
        /** @var LoggableGeocoder */
        $this->bazingaGeocoder = $this->container->get('bazinga_geocoder.geocoder');
        $this->session = $this->container->get('session');
    }

    //TODO: відрефакторити цей метод таким чином щоб він не був привязаний до сутності Address і був більш універсальним
    /**
     * @param Address $address
     * @return bool|Coordinates
     */
    public function getCoordinatesByAddress(Address $address)
    {
        $providers = $this->bazingaGeocoder->getProviders();
        $googleMapsProvider = $providers['google_maps'];
        $accountAddress = $address->getAddress().' '.$address->getCity().', '.$address->getZip();
        try {
            $geocodeQueryResult = $googleMapsProvider->geocode($accountAddress);
        } catch (NoResult $exception) {
            $this->session->getFlashBag()->set('failed', 'Can not get coordinates by this address');
            return false;
        } catch (QuotaExceeded $exception) {
            $this->session->getFlashBag()->set('failed', 'Daily quota of connections with Google Maps has been exceeded!');
            return false;
        }
        $resultAddress = $geocodeQueryResult->first();
        $coordinates = $resultAddress->getCoordinates();

        return $coordinates;
    }
}
