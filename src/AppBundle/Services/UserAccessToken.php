<?php

namespace AppBundle\Services;

use AppBundle\Entity\User;

class UserAccessToken
{
    /**
     * @param User $user
     * @return string
     */
    public function createUserAccessToken(User $user)
    {
        $dataCreated = date('c');
        $uniqueId = uniqid();
        $accessToken = md5($uniqueId . $dataCreated . $user->getPassword());

        return $accessToken;
    }
}
