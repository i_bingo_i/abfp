<?php

namespace AppBundle\Services\Tree;

use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\FrozenServiceForProposal;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderDeviceInfo;
use AppBundle\Entity\WorkorderFrozenContent;
use Doctrine\Common\Persistence\ObjectManager;

class Grouping
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var DeviceManager */
    private $deviceManager;

    private $tree;
    private $devicesCollection;
    private $deviceLevels;

    /**
     * Grouping constructor.
     * @param ObjectManager $objectManager
     * @param DeviceManager $deviceManager
     */
    public function __construct(ObjectManager $objectManager, DeviceManager $deviceManager)
    {
        $this->objectManager = $objectManager;
        $this->deviceManager = $deviceManager;
    }

    /**
     * @param Workorder $workorder
     * @return mixed
     */
    public function getTreeForWorkorder(Workorder $workorder)
    {
        $this->initTree();
        /** @var Service[] $services */
        $services = $workorder->getServices();

        if ($workorder->getIsFrozen()) {
            /** @var WorkorderFrozenContent $services */
            $services = $workorder->getFrozenContent();
        }

        $this->addServicesToDeviceCollection($services);
        $this->mergeDevicesToLevel($this->devicesCollection['threeLevel'], 'twoLevel');
        $this->mergeDevicesToLevel($this->devicesCollection['twoLevel'], 'rootLevel');
        $this->sortDeviceCollection();
        $this->devicesCollection['rootLevel'] = $this->setWoDeviceInfo($this->devicesCollection['rootLevel'], $workorder);
        $this->composeTree();

        return $this->tree;
    }

    /**
     * @param Proposal $proposal
     * @return array
     */
    public function getTreeForProposal(Proposal $proposal)
    {
        $this->initTree();
        /** @var Service[] $services */
        $services = $proposal->getServices();

        if ($proposal->getIsFrozen()) {
            /** @var FrozenServiceForProposal $services */
            $services = $proposal->getFrozen()->getFrozenServices();
        }

        $this->addServicesToDeviceCollection($services);
        $this->mergeDevicesToLevel($this->devicesCollection['threeLevel'], 'twoLevel');
        $this->mergeDevicesToLevel($this->devicesCollection['twoLevel'], 'rootLevel');
        $this->sortDeviceCollection();
        $this->devicesCollection['rootLevel'] = $this->setRepairsInfo($this->devicesCollection['rootLevel'], $proposal);
        $this->composeTree();

        return $this->tree;
    }

    private function initTree()
    {
        unset($this->tree);
        unset($this->devicesCollection);
        unset($this->deviceLevels);

        $this->tree = [];
        $this->devicesCollection = [];
        $this->deviceLevels = [
            0 => 'rootLevel',
            1 => 'twoLevel',
            2 => 'threeLevel'
        ];

        foreach ($this->deviceLevels as $level) {
            $this->devicesCollection[$level] = [];
        }
    }

    private function composeTree()
    {
        foreach ($this->devicesCollection['rootLevel'] as $deviceNode) {
            /** @var Device $device */
            $device = $deviceNode['device'];
            $accountId = $device->getAccount()->getId();
            $this->tree[$accountId]['devices'][] = $deviceNode;
        }
    }

    /**
     * @param $deviceCollection
     * @param $toLevel
     */
    private function mergeDevicesToLevel($deviceCollection, $toLevel)
    {
        foreach ($deviceCollection as $item) {
            $parentId = $item['parentId'];
            $this->devicesCollection[$toLevel][$parentId]['subDevices'][] = $item;
        }
    }

    private function sortDeviceCollection()
    {
        $sortedArray = [];
        $sortArray = [
            0 => [],
            1 => [],
            2 => [],
            3 => []
        ];

        foreach ($this->devicesCollection['rootLevel'] as $item) {
            $sortWeight = $item['sort'];
            $sortArray[$sortWeight][] = $item;
        }

        $sortedArray = array_merge($sortedArray, $sortArray[3]);
        $sortedArray = array_merge($sortedArray, $sortArray[2]);
        $sortedArray = array_merge($sortedArray, $sortArray[1]);
        $sortedArray = array_merge($sortedArray, $sortArray[0]);

        $this->devicesCollection['rootLevel'] = $sortedArray;
    }

    /**
     * @param $services
     */
    public function addServicesToDeviceCollection($services)
    {
        foreach ($services as $service) {
            $serviceHistory = false;

            if ($service instanceof FrozenServiceForProposal or $service instanceof WorkorderFrozenContent) {
                /** @var ServiceHistory $frozeService */
                $serviceHistory = $service->getServiceHistory();
                $service = $service->getServiceHistory()->getOwnerEntity();
            }

            /** @var Account $account */
            $account = $service->getAccount();
            $this->addServiceToDevicesCollection($service, $serviceHistory);

            if (!$this->isAccountInTree($account)) {
                $this->addAccountToTree($account);
            }
        }
    }

    /**
     * @param Service $service
     * @param $serviceHistory
     */
    private function addServiceToDevicesCollection(Service $service, $serviceHistory)
    {
        /** @var Device $device */
        $device = $service->getDevice();
        $deviceId = $device->getId();
        $deviceLevel = $this->deviceManager->getDeviceLevel($device);
        $deviceLevelKey = $this->deviceLevels[$deviceLevel];

        $this->addDeviceToCollection($device);

        if (!$serviceHistory) {
            $this->devicesCollection[$deviceLevelKey][$deviceId]['services'][] = $service;
        } else {
            $this->devicesCollection[$deviceLevelKey][$deviceId]['services'][] = $serviceHistory;
        }

        if ($service->getNamed()->getFrequency()) {
            $this->devicesCollection[$deviceLevelKey][$deviceId]['isInspections'] = true;
        } else {
            $this->devicesCollection[$deviceLevelKey][$deviceId]['isRepairs'] = true;
        }
    }

    /**
     * @param Device $device
     */
    private function addDeviceToCollection(Device $device)
    {
        /** @var Device $parent */
        $parent = $device->getParent();
        $deviceLevel = $this->deviceManager->getDeviceLevel($device);
        $deviceLevelKey = $this->deviceLevels[$deviceLevel];
        $deviceId = $device->getId();

        if (!$this->isDeviceInDeviceTree($device, $deviceLevelKey)) {
            $newDevice = [];
            $newDevice['device'] = $device;
            $newDevice['parentId'] = $parent ? $parent->getId() : 0;
            $newDevice['sort'] = $device->getNamed()->getSort();
            $newDevice['level'] = $deviceLevel;
            $newDevice['services'] = [];
            $newDevice['subDevices'] = [];
            $newDevice['isInspections'] = false;
            $newDevice['isRepairs'] = false;

            $this->devicesCollection[$deviceLevelKey][$deviceId] = $newDevice;
        }

        if ($parent) {
            $this->addDeviceToCollection($parent);
        }
    }

    /**
     * @param $collection
     * @param Workorder $workorder
     * @return mixed
     */
    private function setWoDeviceInfo($collection, Workorder $workorder)
    {
        foreach ($collection as $key => $item) {
            $item['deviceRepairInfo'] = $this->getWoDeviceInfoByWorkorder($item['device'], $workorder);
            $collection[$key] = $item;

            if ($item['subDevices']) {
                $collection[$key]['subDevices'] = $this->setWoDeviceInfo($item['subDevices'], $workorder);
            }
        }

        return $collection;
    }

    /**
     * @param Device $device
     * @param Workorder $workorder
     * @return mixed
     */
    private function getWoDeviceInfoByWorkorder(Device $device, Workorder $workorder)
    {
        $workorderlId = $workorder->getId();
        return $device->getWorkorderDeviceInfo()
            ->filter(function (WorkorderDeviceInfo $info) use ($workorderlId) {
                $infoWorkorderlId = $info->getWorkorder()->getId();
                return $infoWorkorderlId == $workorderlId;
            })->first();
    }

    /**
     * @param $collection
     * @param Proposal $proposal
     * @return mixed
     */
    private function setRepairsInfo($collection, Proposal $proposal)
    {
        foreach ($collection as $key => $item) {
            $item['deviceRepairInfo'] = $this->getRepairDeviceInfoByProposal($item['device'], $proposal);
            $collection[$key] = $item;

            if ($item['subDevices']) {
                $collection[$key]['subDevices'] = $this->setRepairsInfo($item['subDevices'], $proposal);
            }
        }

        return $collection;
    }

    /**
     * @param Device $device
     * @param Proposal $proposal
     * @return mixed
     */
    private function getRepairDeviceInfoByProposal(Device $device, Proposal $proposal)
    {
        $proposalId = $proposal->getId();
        return $device->getRepairDeviceInfo()
            ->filter(function (RepairDeviceInfo $info) use ($proposalId) {
                $infoProposal = $info->getProposal();
                if ($infoProposal) {
                    $infoProposalId = $infoProposal->getId();
                    return $infoProposalId == $proposalId;
                }
                return false;

            })->first();
    }

    /**
     * @param Device $device
     * @param $deviceLevelKey
     * @return bool
     */
    private function isDeviceInDeviceTree(Device $device, $deviceLevelKey)
    {
        if (isset($this->devicesCollection[$deviceLevelKey][$device->getId()])) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @return bool
     */
    private function isAccountInTree(Account $account)
    {
        if (isset($this->tree[$account->getId()])) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     */
    private function addAccountToTree(Account $account)
    {
        $accountId = $account->getId();
        $this->tree[$accountId] = [];
        $this->tree[$accountId]['account'] = $account;
        $this->tree[$accountId]['devices'] = [];
    }
}
