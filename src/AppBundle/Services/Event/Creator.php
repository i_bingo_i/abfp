<?php

namespace AppBundle\Services\Event;

use AppBundle\Entity\EntityManager\EventManager;
use AppBundle\Entity\Event;
use AppBundle\Entity\Repository\EventRepository;
use AppBundle\Entity\Workorder;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\EventTypeRepository;
use AppBundle\Entity\Repository\CoordinateRepository;

class Creator
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var EventTypeRepository */
    private $eventTypeRepository;
    /** @var CoordinateRepository */
    private $coordinateRepository;
    /** @var EventManager */
    private $eventManager;
    /** @var EventRepository */
    private $eventRepository;

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $serviceContainer;

        $this->eventTypeRepository = $this->objectManager->getRepository('AppBundle:EventType');
        $this->coordinateRepository = $this->objectManager->getRepository('AppBundle:Coordinate');
        $this->eventRepository = $this->objectManager->getRepository('AppBundle:Event');
        $this->eventManager = $this->serviceContainer->get('app.event.manager');
    }

    /**
     * @param Workorder|null $workorder
     * @param array $data
     * @param string $type
     * @return Event
     */
    public function create(?Workorder $workorder, array $data = [], string $type = 'workorder')
    {
        $type = $this->eventTypeRepository->findOneBy(['alias' => $type]);
        $coordinate = $this->coordinateRepository->find(1);
        $fromDate = new \DateTime($data['startDate'] . ' ' . $data['startTime']);
        $toDate = new \DateTime($data['startDate'] . ' ' . $data['endTime']);

        $event = new Event();
        $event->setFromDate($fromDate);
        $event->setToDate($toDate);
        $event->setWorkorder($workorder);
        $event->setType($type);
        $event->setUser($data['user']);
        $event->setCoordinate($coordinate);
        $event->setCanFinishWO($this->isCanFinishWO($event));
        $event->setIsTimeCritical($data['isTimeCritical']);
        $this->eventManager->save($event);

        return $event;
    }

    /**
     * @param Event $event
     * @return bool
     */
    public function isCanFinishWO(Event $event)
    {
        /** @var Event|null $theOldestToDateEvent */
        $eventCanFinishWo = $this->eventRepository->getCanFinishWo($event->getWorkorder());
        $res = true;

        if (!empty($eventCanFinishWo)
            && $eventCanFinishWo->getToDate()->getTimestamp() < $event->getToDate()->getTimestamp()
        ) {
            $eventCanFinishWo->setCanFinishWO(false);
            $this->objectManager->persist($eventCanFinishWo);
        } elseif (!empty($eventCanFinishWo)) {
            $res = false;
        }

        return $res;
    }
}
