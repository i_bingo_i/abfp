<?php

namespace AppBundle\Services\Event;

use AppBundle\Entity\Event;
use AppBundle\Entity\Repository\EventRepository;
use AppBundle\Entity\Workorder;
use Doctrine\Common\Persistence\ObjectManager;

class Compose
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var EventRepository */
    private $eventRepository;

    /**
     * Compose constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        $this->eventRepository = $this->objectManager->getRepository('AppBundle:Event');
    }

    /**
     * @param Workorder $workorder
     * @return array
     */
    public function composeByWorkorder(Workorder $workorder)
    {
        $maxEventDate = $this->eventRepository->getMaxToDate($workorder);
        $events = $this->eventRepository->getEventsByWorkorder($workorder);
        $sortedEvents = [];
        /** @var Event $event */
        foreach ($events as $event) {
            $sortedEvents[$event->getFromDate()->format("m/d/Y")][] = [
                "id" => $event->getId(),
                "from" => $event->getFromDate()->format("h:i A"),
                "to" => $event->getToDate()->format("h:i A"),
                "userFName" => $event->getUser()->getUser()->getFirstName(),
                "userLName" => $event->getUser()->getUser()->getLastName(),
                "cUserId" => $event->getUser()->getId(),
                "contractor" => $event->getUser()->getContractor()->getName(),
                "canFinishWO" => $event->getCanFinishWO(),
                "opportunityToFinishWo" => $maxEventDate == $event->getToDate()->format("Y-m-d H:i:s") ? true : false
            ];
        }

        return $sortedEvents;
    }
}
