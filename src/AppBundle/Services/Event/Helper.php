<?php

namespace AppBundle\Services\Event;

use AppBundle\Entity\Event;
use AppBundle\Entity\Repository\EventRepository;
use AppBundle\Entity\Workorder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\EntityManager\EventManager;
use Doctrine\Common\Persistence\ObjectManager;

class Helper
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var EventManager */
    private $eventManager;
    /** @var EventRepository */
    private $eventRepository;

    /**
     * Helper constructor.
     * @param ContainerInterface $serviceContainer
     * @param ObjectManager $objectManager
     */
    public function __construct(ContainerInterface $serviceContainer, ObjectManager $objectManager)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->eventManager = $this->serviceContainer->get('app.event.manager');
        $this->eventRepository = $this->objectManager->getRepository('AppBundle:Event');
    }

    /**
     * Generating array with time from 00:00 to 23:00. Step half an hour.
     * @return array
     */
    public function generateTime()
    {
        $midnight = new \DateTime('12:00 AM');
        $res = [$midnight->format('h:i A')];

        for ($i = $midnight->format('h:i A'); '11:30 PM' != $i;) {
            $midnight->add(\DateInterval::createFromDateString('30 minutes'));
            $res[] = $i = $midnight->format('h:i A');
        }

        return $res;
    }

    /**
     * @param Event $event
     * @param $msocEventId
     */
    public function setNewMsOutlookCalendarEventId(Event $event, $msocEventId)
    {
        $event->setMsOutlookCalendarEventId($msocEventId);
        $this->eventManager->save($event);
    }

    /**
     * @param Workorder $workorder
     * @param Event $event
     */
    public function changeCanFinishWo(Workorder $workorder, Event $event)
    {
        $this->resetCanFinishWoEvent($workorder);

        $event->setCanFinishWO(true);
        $this->objectManager->flush();
    }

    /**
     * @param Workorder $workorder
     * @param Event $event
     */
    private function resetCanFinishWoEvent(Workorder $workorder)
    {
        $canFinishWoEvent = $this->eventRepository->findOneBy(['workorder' => $workorder, 'canFinishWO' => true]);

        if ($canFinishWoEvent instanceof Event) {
            $canFinishWoEvent->setCanFinishWO(false);
        }
    }
}
