<?php

namespace AppBundle\Services\Letter;


use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use AppBundle\Entity\EntityManager\LetterManager;
use AppBundle\Entity\Letter;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class LetterActionManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var Session */
    private $session;

    /**
     * InvoiceManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;
        $this->session = $this->container->get("session");
    }

    /**
     * @param SendFormViaEmailDTO $sendInvoiceViaEmailDTO
     * @return Letter|bool
     */
    public function sendViaEmailForWorkorder(SendFormViaEmailDTO $sendInvoiceViaEmailDTO)
    {
        /** @var LetterCreator $letterCreator */
        $letterCreator = $this->container->get("letter.creator.service");
        /** @var Letter $newLetter */
        $newLetter = $letterCreator->createBySendInvoiceViaEmailDTO($sendInvoiceViaEmailDTO);
        $this->objectManager->flush();
        /** @var LetterManager $letterManager */
        $letterManager = $this->container->get("app.letter.manager");

        $letterResponse = $letterManager->sendLetter($newLetter);

        if ($letterResponse) {
            return $newLetter;
        }

        return false;
//        if ($letterResponse === true) {
//            $this->session->getFlashBag()->add("success", 'Message was sent');
//        }
    }
}