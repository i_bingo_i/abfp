<?php

namespace AppBundle\Services\Letter;

use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use AppBundle\Entity\Letter;
use AppBundle\Entity\LetterAttachment;
use Doctrine\Common\Persistence\ObjectManager;

class LetterCreator
{
    /** @var ObjectManager */
    private $om;

    public function __construct(ObjectManager $objectManager)
    {
        $this->om = $objectManager;
    }

    /**
     * @param SendFormViaEmailDTO $sendInvoiceViaEmailDTO
     * @return Letter
     */
    public function createBySendInvoiceViaEmailDTO(SendFormViaEmailDTO $sendInvoiceViaEmailDTO)
    {
        $newLetter = new Letter();
        /** @var LetterAttachment $attachment */
        foreach ($sendInvoiceViaEmailDTO->getAttachments() as $attachment) {
            $newLetter->addAttachment($attachment);
            $attachment->setLetter($newLetter);
            $this->om->persist($attachment);
        }

        $newLetter->setWorkorder($sendInvoiceViaEmailDTO->getWorkorder());
        $newLetter->setBody($sendInvoiceViaEmailDTO->getEmailBody());
        $newLetter->setSubject($sendInvoiceViaEmailDTO->getEmailSubject());
        $newLetter->setFrom($sendInvoiceViaEmailDTO->getFrom());
        $newLetter->setEmails($sendInvoiceViaEmailDTO->getTo());

        $this->om->persist($newLetter);

        return $newLetter;
    }
}