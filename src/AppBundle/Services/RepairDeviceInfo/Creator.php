<?php

namespace AppBundle\Services\RepairDeviceInfo;

use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\RepairDeviceInfoManager;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\Proposal\CalculateFee;

class Creator
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var \AppBundle\Entity\Repository\RepairDeviceInfo */
    private $repairDeviceInfoRepository;
    /** @var CalculateFee */
    private $calculateFeeService;
    /** @var RepairDeviceInfoManager */
    private $repairDeviceInfoManager;
    /** @var ServiceRepository */
    private $serviceRepository;

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->repairDeviceInfoRepository = $this->objectManager->getRepository('AppBundle:RepairDeviceInfo');
        $this->calculateFeeService = $this->container->get('app.proposal_calculate_fee.service');
        $this->repairDeviceInfoManager = $this->container->get('app.repair_device_info.manager');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
    }


    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     * @param Proposal $existsProposal
     * @param Device $device
     */
    public function edit(RepairDeviceInfo $repairDeviceInfo, Proposal $existsProposal, Device $device)
    {
        $existsRepairDeviceInfo = $this->repairDeviceInfoRepository->findOneBy(
            [
                'proposal' => $existsProposal,
                'device' => $device
            ]
        );

        if (!$repairDeviceInfo->getDevice() && !$repairDeviceInfo->getProposal() && $existsRepairDeviceInfo) {
            $existsRepairDeviceInfo->setSpecialNotification($repairDeviceInfo->getSpecialNotification());
            $existsRepairDeviceInfo->setPrice($repairDeviceInfo->getPrice());
            $existsRepairDeviceInfo->setEstimationTime($repairDeviceInfo->getEstimationTime());
            $existsRepairDeviceInfo->setComments($repairDeviceInfo->getComments());
            $this->calculateFeeService->calculateFee($existsRepairDeviceInfo);
            $this->repairDeviceInfoManager->save($existsRepairDeviceInfo);

        } elseif ($repairDeviceInfo->getDevice() && $repairDeviceInfo->getDevice()) {
            $this->calculateFeeService->calculateFee($existsRepairDeviceInfo);
            $this->repairDeviceInfoManager->save($repairDeviceInfo);

        } else {
            $this->creating($device, $existsProposal, $repairDeviceInfo);
        }
    }

    /**
     * @param Device $device
     * @param Proposal $proposal
     * @param $repairDeviceInfo
     * @return RepairDeviceInfo
     */
    public function creating(Device $device, Proposal $proposal, $repairDeviceInfo)
    {
        if ($repairDeviceInfo instanceof RepairDeviceInfo) {
            $repairDeviceInfo->setProposal($proposal);
            $repairDeviceInfo->setDevice($device);
            $this->calculateFeeService->calculateFee($repairDeviceInfo);
        } else {
            $repairDeviceInfo = $this->create($device, $proposal, $repairDeviceInfo);
        }

        $this->repairDeviceInfoManager->save($repairDeviceInfo);

        return $repairDeviceInfo;
    }

    /**
     * @param Proposal $proposal
     * @param bool $isNeedCreateInspectionForProposal
     */
    public function creatingForInspection(Proposal $proposal, $isNeedCreateInspectionForProposal = false)
    {
        $services = $proposal->getServices();
        $devicesAndServices = [];
        /** @var Service $service */
        foreach ($services as $service) {
            $devicesAndServices[$service->getDevice()->getId()]['device'] = $service->getDevice();
            $devicesAndServices[$service->getDevice()->getId()]['services'][] = $service;
        }

        foreach ($devicesAndServices as $item) {
            /** @var RepairDeviceInfo $repairDeviceInfo */
            $repairDeviceInfo = $this->repairDeviceInfoRepository->findOneBy(
                ['device'=>$item['device'], 'proposal'=>$proposal]
            );

            if ($isNeedCreateInspectionForProposal) {
                $this->createForRepairInspection($repairDeviceInfo, $proposal, $item);
            } else {
                $this->createForRetestInspection($repairDeviceInfo, $proposal, $item);
            }
        }
    }

    /**
     * @param Device $device
     * @param $services
     * @return RepairDeviceInfo
     */
    public function createInfoForInspectionsWithoutProposal(Device $device, $services)
    {
        $repairDeviceInfo = new RepairDeviceInfo();
        $repairDeviceInfo->setDevice($device);
        $repairDeviceInfo->setInspectionPrice(
            $this->calculateFeeService->calculateSubtotalInspectionsFee($services)
        );
        $repairDeviceInfo->setInspectionEstimationTime(
            $this->calculateFeeService->calculateInspectionsEstimatedTime($services)
        );
        $repairDeviceInfo->setEstimationTime(0);
        $repairDeviceInfo->setPrice(0);
        $this->calculateFeeService->calculateFee($repairDeviceInfo, $services);
        $this->repairDeviceInfoManager->save($repairDeviceInfo);

        return $repairDeviceInfo;
    }

    /**
     * @param Device $device
     * @param Proposal $proposal
     */
    public function checkEventsOnChangeInfo(Device $device, Proposal $proposal)
    {
        /** @var \AppBundle\Entity\RepairDeviceInfo $repairDeviceInfo */
        $repairDeviceInfo = $this->repairDeviceInfoRepository->findOneBy(
            ['device'=>$device, 'proposal'=>$proposal]
        );
        $repairDeviceInfoServices = $this->serviceRepository->findBy(
            ['proposal' => $proposal, 'device' => $device]
        );

        $checkIfGlueRepairDeviceInfo = $repairDeviceInfo->getIsGlued();

        if ($repairDeviceInfoServices) {
            if ($checkIfGlueRepairDeviceInfo) {
                $this->calculateGlueForInspection($repairDeviceInfo);
            } else {
                $this->updateDeviceInfoTotals(
                    $repairDeviceInfo,
                    $repairDeviceInfoServices
                );
            }
        } else {
            $this->repairDeviceInfoManager->remove($repairDeviceInfo);
        }
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     */
    public function calculateGlueForInspection(RepairDeviceInfo $repairDeviceInfo)
    {
        /** @var Proposal $proposal */
        $proposal = $repairDeviceInfo->getProposal();
        /** @var Device $device */
        $device = $repairDeviceInfo->getDevice();
        $inspectionInGlueRepairDeviceInfo = $this->serviceRepository->getInspectionsByDeviceAndProposal(
            $device,
            $proposal
        );

        if ($inspectionInGlueRepairDeviceInfo) {
            $this->updateInspectionDeviceInfoTotals(
                $repairDeviceInfo,
                $inspectionInGlueRepairDeviceInfo
            );
        } else {
            $this->unGluedWithServicesAndInspections($repairDeviceInfo);
        }
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     */
    public function reCalculateGlueAfterDeletedLatestRepairServices(RepairDeviceInfo $repairDeviceInfo)
    {
        /** @var Proposal $proposal */
        $proposal = $repairDeviceInfo->getProposal();
        /** @var Device $device */
        $device = $repairDeviceInfo->getDevice();
        $repairServicesInGlueRepairDeviceInfo = $this->serviceRepository->getRepairServicesByDeviceAndProposal(
            $device,
            $proposal
        );

        if (!$repairServicesInGlueRepairDeviceInfo) {
            $repairDeviceInfo->setEstimationTime(0);
            $repairDeviceInfo->setPrice(0);

            $this->calculateFeeService->calculateFee($repairDeviceInfo);
            $this->repairDeviceInfoManager->save($repairDeviceInfo);
        }
    }

    /**
     * @param Device $device
     * @param Proposal $proposal
     * @param array $data
     * @return RepairDeviceInfo
     */
    private function create(Device $device, Proposal $proposal, array $data)
    {
        $specialNotificationRepository = $this->objectManager->getRepository('AppBundle:SpecialNotification');
        $setSpecialNotification = $specialNotificationRepository->find($data['specialNotification']);

        $repairDeviceInfo = new RepairDeviceInfo();
        $repairDeviceInfo->setDevice($device);
        $repairDeviceInfo->setProposal($proposal);
        $repairDeviceInfo->setSpecialNotification($setSpecialNotification);
        $repairDeviceInfo->setEstimationTime($data['estimationTime']);
        $repairDeviceInfo->setComments($data['comments']);
        $repairDeviceInfo->setPrice($data['price']);

        return $repairDeviceInfo;
    }

    /**
     * @param Proposal $proposal
     * @param $deviceAndServices
     * @return RepairDeviceInfo
     */
    private function createInspection(Proposal $proposal, $deviceAndServices)
    {
        $repairDeviceInfo = $this->createBasicRepairDeviceInfo($proposal, $deviceAndServices);

        $repairDeviceInfo->setPrice(
            $this->calculateFeeService->calculateSubtotalInspectionsFee($deviceAndServices['services'])
        );
        $repairDeviceInfo->setEstimationTime(
            $this->calculateFeeService->calculateInspectionsEstimatedTime($deviceAndServices['services'])
        );
        $this->calculateFeeService->calculateFee($repairDeviceInfo);

        return $repairDeviceInfo;
    }

    /**
     * @param $repairDeviceInfo
     * @param Proposal $proposal
     * @param $item
     */
    private function createForRepairInspection($repairDeviceInfo, Proposal $proposal, $item)
    {
        if (!empty($repairDeviceInfo)) {
            $repairDeviceInfo = $this->setGlueWithServicesAndInspections($repairDeviceInfo);
            $this->updateInspectionDeviceInfoTotals(
                $repairDeviceInfo,
                $item['services']
            );
        } else {
            $repairDeviceInfo = $this->updateInspectionForProposal($proposal, $item);
            $repairDeviceInfo->setEstimationTime(0);
            $repairDeviceInfo->setPrice(0);
            $repairDeviceInfo = $this->setGlueWithServicesAndInspections($repairDeviceInfo);

            $this->repairDeviceInfoManager->save($repairDeviceInfo);
        }
    }

    /**
     * @param $repairDeviceInfo
     * @param Proposal $proposal
     * @param $item
     */
    private function createForRetestInspection($repairDeviceInfo, Proposal $proposal, $item)
    {
        if (!empty($repairDeviceInfo)) {
            $this->updateDeviceInfoTotals($repairDeviceInfo, $item['services']);
        } else {
            $repairDeviceInfo = $this->createInspection($proposal, $item);
            $this->repairDeviceInfoManager->save($repairDeviceInfo);
        }
    }

    /**
     * @param Proposal $proposal
     * @param $deviceAndServices
     * @return RepairDeviceInfo
     */
    private function createBasicRepairDeviceInfo(Proposal $proposal, $deviceAndServices)
    {
        $repairDeviceInfo = new RepairDeviceInfo();
        $repairDeviceInfo->setDevice($deviceAndServices['device']);
        $repairDeviceInfo->setProposal($proposal);

        return $repairDeviceInfo;
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     * @return RepairDeviceInfo
     */
    private function setGlueWithServicesAndInspections(RepairDeviceInfo $repairDeviceInfo)
    {
        $repairDeviceInfo->setIsGlued(true);

        return $repairDeviceInfo;
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     */
    private function unGluedWithServicesAndInspections(RepairDeviceInfo $repairDeviceInfo)
    {
        $repairDeviceInfo->setInspectionPrice(0);
        $repairDeviceInfo->setInspectionEstimationTime(0);
        $repairDeviceInfo->setIsGlued(false);
        $this->calculateFeeService->calculateFee($repairDeviceInfo);

        $this->repairDeviceInfoManager->save($repairDeviceInfo);
    }

    /**
     * @param Proposal $proposal
     * @param $deviceAndServices
     * @return RepairDeviceInfo
     */
    private function updateInspectionForProposal(Proposal $proposal, $deviceAndServices)
    {
        $repairDeviceInfo = $this->createBasicRepairDeviceInfo($proposal, $deviceAndServices);

        $repairDeviceInfo->setInspectionPrice(
            $this->calculateFeeService->calculateSubtotalInspectionsFee($deviceAndServices['services'])
        );
        $repairDeviceInfo->setInspectionEstimationTime(
            $this->calculateFeeService->calculateInspectionsEstimatedTime($deviceAndServices['services'])
        );
        $this->calculateFeeService->calculateFee($repairDeviceInfo);

        return $repairDeviceInfo;
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     * @param $services
     */
    private function updateInspectionDeviceInfoTotals(RepairDeviceInfo $repairDeviceInfo, $services)
    {
        $repairDeviceInfo->setInspectionPrice(
            $this->calculateFeeService->calculateSubtotalInspectionsFee($services)
        );
        $repairDeviceInfo->setInspectionEstimationTime(
            $this->calculateFeeService->calculateInspectionsEstimatedTime($services)
        );
        $this->calculateFeeService->calculateFee($repairDeviceInfo);

        $this->repairDeviceInfoManager->save($repairDeviceInfo);
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     * @param $services
     */
    private function updateDeviceInfoTotals(RepairDeviceInfo $repairDeviceInfo, $services)
    {
        $repairDeviceInfo->setPrice(
            $this->calculateFeeService->calculateSubtotalInspectionsFee($services)
        );
        $repairDeviceInfo->setEstimationTime(
            $this->calculateFeeService->calculateInspectionsEstimatedTime($services)
        );
        $this->calculateFeeService->calculateFee($repairDeviceInfo);

        $this->repairDeviceInfoManager->save($repairDeviceInfo);
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     * @param Device $device
     * @param $services
     */
    public function updateInfoForRepairServicesWithoutProposal(
        RepairDeviceInfo $repairDeviceInfo,
        Device $device,
        $services
    ) {
        $repairDeviceInfo->setDevice($device);
        $this->calculateFeeService->calculateFee($repairDeviceInfo, $services);

        $this->repairDeviceInfoManager->save($repairDeviceInfo);
    }
}
