<?php

namespace AppBundle\Services\ContractorUser;

use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\ContractorUserHistory;
use Doctrine\Common\Persistence\ObjectManager;

class HistoryCreator
{
    /** @var  ObjectManager */
    private $objectManager;

    /**
     * HistoryCreator constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param ContractorUser $contractorUser
     */
    public function create(ContractorUser $contractorUser)
    {
        $contractorUserHistory = new ContractorUserHistory();

        $contractorUserHistory->setContractor($contractorUser->getContractor());
        $contractorUserHistory->setUser($contractorUser->getUser());
        $contractorUserHistory->setOwnerEntity($contractorUser);
        $contractorUserHistory->setDateSave(new \DateTime());
        $contractorUserHistory->setDeleted($contractorUser->getDeleted());
        $contractorUserHistory->setActive($contractorUser->getActive());

        $this->objectManager->persist($contractorUserHistory);
        $this->objectManager->flush();
    }
}
