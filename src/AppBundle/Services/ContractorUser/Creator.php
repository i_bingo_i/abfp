<?php

namespace AppBundle\Services\ContractorUser;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Creator
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var ContractorRepository */
    private $contractorRepository;

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;

        $this->contractorRepository = $this->objectManager->getRepository('AppBundle:Contractor');
    }

    /**
     * @return ContractorUser
     */
    public function prepareCreate()
    {
        /** @var ContractorUser $contractorUser */
        $contractorUser = new ContractorUser();
        /** @var User $user */
        $user = new User();
        /** @var Contractor $contractor */
        $contractor = $this->contractorRepository->findOneBy(['name' => 'IDAP Group']);

        $contractorUser->setUser($user);
        $contractorUser->setContractor($contractor);

        return $contractorUser;
    }
}
