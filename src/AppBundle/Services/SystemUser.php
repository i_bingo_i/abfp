<?php

namespace AppBundle\Services;

use AppBundle\Entity\User;
use AppBundle\Entity\Repository\UserRepository;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SystemUser
{
    /** @var TokenStorageInterface */
    private $tokenStorage;
    /** @var ObjectManager  */
    private $objectManager;
    /** @var UserRepository */
    private $userRepository;

    /**
     * SystemUser constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ObjectManager $objectManager, TokenStorageInterface $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->tokenStorage = $tokenStorage;
        $this->userRepository = $this->objectManager->getRepository('AppBundle:User');
    }

    /**
     * @param null $authorToken
     * @return User|bool|mixed|null|object
     */
    public function getCurrent($authorToken = null)
    {
        if ($authorToken) {
            return $this->userRepository->findOneBy(['accessToken' => $authorToken]);
        }

        $systemToken = $this->tokenStorage->getToken();
        if ($systemToken) {
            return $systemToken->getUser();
        }

        return false;
    }
}
