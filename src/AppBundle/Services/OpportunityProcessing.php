<?php

namespace AppBundle\Services;

use AppBundle\Entity\EntityManager\OpportunityManager;
use AppBundle\Entity\EntityManager\ProcessingStatsManager;
use AppBundle\Entity\Opportunity;
use AppBundle\Entity\Repository\OpportunityRepository;
use AppBundle\Entity\Repository\OpportunityStatusRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Twig\AppExtension;

class OpportunityProcessing
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var AppExtension */
    private $appTwigExtention;
    /** @var OpportunityStatusRepository */
    private $opportunityStatusRepository;
    /** @var OpportunityRepository */
    private $opportunityRepository;
    /** @var ProcessingStatsManager */
    private $processingStatsManager;
    /** @var OpportunityManager */
    private $opportunityManager;

    private const STATUS_PAST_DUE = 'past_due';

    /**
     * OpportunityProcessing constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->objectManager = $objectManager;
        $this->container = $container;
        $this->appTwigExtention = $this->container->get('app.twig_extension');

        $this->opportunityStatusRepository = $this->objectManager->getRepository('AppBundle:OpportunityStatus');
        $this->opportunityRepository = $this->objectManager->getRepository('AppBundle:Opportunity');
        $this->processingStatsManager = $this->container->get('app.processing_stats.manager');
        $this->opportunityManager = $this->container->get('app.opportunity.manager');
    }

    /**
     * Initiated by Midnight Check - next day after the Opportunity Due Date
     */
    public function pastDue()
    {
        $status = $this->opportunityStatusRepository->findOneBy(['alisa' => self::STATUS_PAST_DUE]);
        $opportunities = $this->opportunityRepository->getOverDue($status);

        if (!empty($opportunities)) {
            $success = 0;
            /** @var Opportunity $proposal */
            foreach ($opportunities as $opportunity) {
                $this->opportunityManager->setStatus($opportunity, self::STATUS_PAST_DUE);
                $this->opportunityManager->update($opportunity);
                $success++;
            }
            $this->processingStatsManager->createMessageByAlias(
                "Success finish in Past Due check. Count of opportunity: " .
                count($opportunities).". Success processing: " . $success
            );
        } else {
            $this->processingStatsManager
                ->createMessageByAlias("Nothing to do in Past Due check. No Opportunities for processing.");
        }
    }
}
