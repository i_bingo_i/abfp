<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Finder\Finder;

class Files
{
    private $rootPath = __DIR__ . "/../../../web";

    /**
     * @param UploadedFile $file
     * @param null $prefix
     * @return string
     */
    public function getRandomFileName(UploadedFile $file, $prefix = null)
    {
        $extension = $file->guessExtension();
        if (!$extension) {
            $extension = 'bin';
        }

        if($prefix){
            return $prefix . '_' . time() . '_' . rand(1, 99999) . '.' . $extension;
        }

        return time() . '_' . rand(1, 99999) . '.' . $extension;
    }

    /**
     * @param UploadedFile $file
     * @param $savePath
     * @param null $fileName
     *
     * @return bool
     */
    public function saveUploadFile(UploadedFile $file, $savePath, $fileName = null)
    {
        if(!is_dir($this->rootPath .$savePath)) mkdir($this->rootPath .$savePath);

        if ($file) {
            $fullSavePath = $this->rootPath . $savePath;

            if (!$fileName) {
                $fileName = $this->getRandomFileName($file);
            }
            $file->move($fullSavePath, $fileName);

            chmod("{$fullSavePath}/{$fileName}", 0777);

            return $fileName;
        }

        return false;
    }

    /**
     * @param $fullPath
     * @return int|null
     */
    public function getExistFileSize($fullPath)
    {
        $fileName = $this->getFileNameByFullPath($fullPath);
        $pathWithoutName = $this->rootPath.str_replace($fileName, "", $fullPath);

        $finder = new Finder();
        $finder->files()->in($pathWithoutName)->name($fileName);
        foreach ($finder as $file) {
            return $file->getSize();
        }

        return null;
    }

    /**
     * @param $fullPath
     * @return mixed
     */
    public function getFileNameByFullPath($fullPath)
    {
        if (stristr($fullPath, "/") !== false) {
            $explodedPath = explode("/", $fullPath);
            return array_pop($explodedPath);
        }

        return $fullPath;
    }
}
