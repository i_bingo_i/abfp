<?php

namespace AppBundle\Services\Device;

use AppBundle\Entity\Device;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\DynamicFieldDropboxChoicesRepository;
use AppBundle\Entity\Repository\DynamicFieldValueRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DynamicFieldsService
{
    /** @var ObjectManager  */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var DeviceManager  */
    private $deviceManager;
    /** @var DeviceRepository  */
    private $deviceRepository;
    /** @var DynamicFieldValueRepository */
    private $dynamicFieldValueRepository;
    /** @var DynamicFieldDropboxChoicesRepository */
    private $dynamicFieldDropboxChoicesRepositry;

    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->deviceManager = $this->serviceContainer->get('app.device.manager');

        $this->deviceRepository = $this->objectManager->getRepository("AppBundle:Device");
        $this->dynamicFieldValueRepository = $this->objectManager->getRepository("AppBundle:DynamicFieldValue");
        $this->dynamicFieldDropboxChoicesRepositry = $this->objectManager->getRepository("AppBundle:DynamicFieldDropboxChoices");
    }

    /**
     * @param $entity
     */
    public function save($entity)
    {
        $this->objectManager->persist($entity);
        $this->objectManager->flush();
    }

    /**
     * @param Device $device
     * @return array
     */
    public function dynamicValueInField(Device $device)
    {
        $deviceInfo = '';
        $showWarning = false;

        /** @var DynamicFieldValue $field */
        foreach ($device->getFields() as $field) {
            if ($field->getField()->getIsShow()) {
                if (!is_null($field->getValue()) or !is_null($field->getOptionValue())) {
                    $deviceInfo = $this->checkDynamicField($field, $deviceInfo);
                } elseif (empty($field->getValue()) and empty($field->getOptionValue())) {
                    $showWarning = true;
                }
            }
        }
        $deviceInfo = (strlen($deviceInfo)>3) ? substr($deviceInfo, 0, strlen($deviceInfo)-2) : $deviceInfo;

        return ['deviceInfo' => $deviceInfo, 'showWarning' => $showWarning];
    }

    /**
     * @param Device $device
     * @return string
     */
    public function getDynamicFields(Device $device)
    {
        $deviceInfo = '';

        extract($this->dynamicValueInField($device), EXTR_OVERWRITE);

        if ($device->getLocation()) {
            $deviceInfo .= (strlen($deviceInfo) >= 1) ? (", " . $device->getLocation()) : $device->getLocation();
        }

        return $deviceInfo;
    }

    /**
     * @param DynamicFieldValue $field
     * @param $deviceInfo
     * @return string
     */
    public function checkDynamicField(DynamicFieldValue $field, $deviceInfo)
    {
        if (!is_null($field->getValue())) {
            $deviceInfo = $this->addLabel($field->getValue(), $deviceInfo, $field);
        } elseif (!empty($field->getOptionValue())) {
            $deviceInfo = $this->addLabel($field->getOptionValue()->getOptionValue(), $deviceInfo, $field);
        }

        return $deviceInfo;
    }

    /**
     * @param $value
     * @param $label
     * @param $deviceInfo
     * @return string
     */
    public function addLabelBeforeValue($value, $label, $deviceInfo)
    {
        $deviceInfo .= $label . $value.", ";
        return $deviceInfo;
    }

    /**
     * @param $value
     * @param $label
     * @param $deviceInfo
     * @return string
     */
    public function addLabelAfterValue($value, $label, $deviceInfo)
    {
        $deviceInfo .= $value." ".$label.", ";
        return $deviceInfo;
    }

    /**
     * @param $fieldsArray
     * @param Device $deviceForUpdate
     * @return Device
     */
    public function updateDevicesFields($fieldsArray, Device $deviceForUpdate)
    {
        foreach ($fieldsArray as $key => $value) {
            /** @var DynamicFieldValue $fieldValue */
            $fieldValue = $this->dynamicFieldValueRepository->find($key);
            if (!is_null($fieldValue)) {
                /** @var DynamicField $dynamicField */
                $dynamicField = $fieldValue->getField();
                if ($dynamicField->getDevice() === $deviceForUpdate->getNamed()) {
                    $fieldValue = $this->setFieldValue($dynamicField, $fieldValue, $value);
                    $this->save($fieldValue);
                }
            }
        }

        return $deviceForUpdate;
    }

    /**
     * @param DynamicField $field
     * @param DynamicFieldValue $newValue
     * @param $value
     * @return DynamicFieldValue
     */
    private function setFieldValue(DynamicField $field, DynamicFieldValue $newValue, $value)
    {
        if ($field->getType()->getAlias() == "text") {
            $newValue->setValue($value);
        } elseif ($field->getType()->getAlias() == "dropdown") {
            /** @var DynamicFieldDropboxChoices $dropboxChoice */
            $dropboxChoice = $this->dynamicFieldDropboxChoicesRepositry->findOneBy(
                [
                    "optionValue" => $value,
                    "field" => $field->getId()
                ]
            );
            if ($dropboxChoice) {
                $newValue->setOptionValue($dropboxChoice);
            }
        }

        return $newValue;
    }

    /**
     * @param $value
     * @param $deviceInfo
     * @param DynamicFieldValue $field
     * @return string
     */
    private function addLabel($value, $deviceInfo, DynamicFieldValue $field)
    {

        if ($field->getField()->getUseLabel() and !$field->getField()->getLabelAfter()) {
            $deviceInfo = $this->addLabelBeforeValue($value, $field->getField()->getLabelDescription(), $deviceInfo);
        } elseif ($field->getField()->getLabelAfter()) {
            $deviceInfo = $this->addLabelAfterValue($value, $field->getField()->getLabelDescription(), $deviceInfo);
        } elseif (!$field->getField()->getUseLabel()) {
            $deviceInfo .= $value.", ";
        }

        return $deviceInfo;
    }


}