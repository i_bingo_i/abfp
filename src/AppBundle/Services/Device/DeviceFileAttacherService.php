<?php

namespace AppBundle\Services\Device;


use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Services\Files;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\TrashRequests\Creator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DeviceFileAttacherService
{

    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var Creator */
    private $trashRequestCreator;
    /** @var DeviceRepository  */
    private $deviceRepository;
    /** @var DeviceManager */
    private $deviceManager;

    private $errorMessages = [];

    /**
     * DeviceFileAttacher constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->trashRequestCreator = $this->serviceContainer->get('app.trash_requests.creator.service');
        $this->deviceManager = $this->serviceContainer->get('app.device.manager');

        $this->deviceRepository = $this->objectManager->getRepository('AppBundle:Device');
    }


    /**
     * @param $deviceID
     * @param null|UploadedFile $file
     * @param $route
     * @param $accessToken
     * @return Device|bool|null|object
     */
    public function attachById($deviceID, ?UploadedFile $file, $route, $accessToken)
    {
        $device = $this->deviceRepository->find($deviceID);

        $this->checkingForAttach($device, $file);

        if (!empty($this->errorMessages)) {
            $fullPath = $this->saveFileIfItExist($file);
            $this->trashRequestCreator->create(
                $route,
                json_encode(["deviceID" => $deviceID]),
                $this->errorMessages,
                $fullPath,
                $accessToken
            );

            return false;
        }

        $saveImagePath = "/uploads/devices";
        $filePath = $this->serviceContainer->get('app.images')->saveUploadFile($file, $saveImagePath);

        $device->setPhoto($saveImagePath."/".$filePath);

        $this->deviceManager->save($device);

        return $device;


    }

    /**
     * @param Device|null $device
     * @param null|UploadedFile $file
     */
    private function checkingForAttach(?Device $device, ?UploadedFile $file)
    {
        $this->errorMessages = [];

        if (is_null($device)) {
            $this->errorMessages[] = "Device not found";
        }

        if (is_null($file)) {
            $this->errorMessages[] = "File has not received";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Device can not be edited</b>.<br>"], $this->errorMessages);
        }
    }

    /**
     * @param null|UploadedFile $file
     * @return null|string
     */
    private function saveFileIfItExist(?UploadedFile $file)
    {
        $fullPath = null;
        if (isset($file)) {
            /** @var Files $filesService */
            $filesService = $this->serviceContainer->get('app.files');

            $fileName = $filesService->getRandomFileName($file, 'device_');
            $savePath = "/uploads/bad_request_storage/";
            $filesService->saveUploadFile($file, $savePath, $fileName);
            $fullPath = $savePath . $fileName;
        }

        return $fullPath;
    }
}