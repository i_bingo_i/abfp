<?php

namespace AppBundle\Services\Device;

use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\DeviceStatusRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\TrashRequests\Creator;

class DeviceUpdaterService
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var Creator */
    private $trashRequestCreator;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var DeviceStatusRepository */
    private $deviceStatusRepository;
    /** @var DeviceManager */
    private $deviceManager;
    /** @var DynamicFieldsService */
    private $dynamicFieldsService;

    private $errorMessages = [];

    /**
     * DeviceCreatorService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->trashRequestCreator = $this->serviceContainer->get('app.trash_requests.creator.service');
        $this->deviceManager = $this->serviceContainer->get('app.device.manager');
        $this->dynamicFieldsService = $this->serviceContainer->get('app.dynamic_fields.service');

        $this->deviceRepository = $this->objectManager->getRepository("AppBundle:Device");
        $this->deviceStatusRepository = $this->objectManager->getRepository("AppBundle:DeviceStatus");
    }

    /**
     * @param $params
     * @param $route
     * @return bool|mixed
     */
    public function createByParams($params, $route)
    {
        $parsedData = $this->parseData($params);
        $this->checkingForCreate($parsedData);
        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]
            );
            return false;
        }

        return $this->updateDeviceByParseData($parsedData);
    }

    /**
     * @param $data
     * @return array
     */
    private function parseData($data)
    {
        $parsedData = [];

        $parsedData["device"] = null;
        if(isset($data["deviceID"])) {
            $parsedData["device"] = $this->deviceRepository->find($data["deviceID"]);
        }

        $parsedData["location"] = null;
        if(isset($data["location"])) {
            $parsedData["location"] = $data["location"];
        }

        $parsedData["comment"] = null;
        if (isset($data["comment"])) {
            $parsedData["comment"] = $data["comment"];
        }

        $parsedData["note"] = null;
        if (isset($data["note"])) {
            $parsedData["note"] = $data["note"];
        }

        $parsedData["status"] = null;
        if (isset($data["statusAlias"])) {
            $parsedData["status"] = $this->deviceStatusRepository->findOneBy(["alias" => $data["statusAlias"]]);
        }

        $parsedData["title"] = null;
        if (!is_null($parsedData["device"]) and !is_bool($parsedData["device"])) {
            $parsedData["title"] = $this->deviceManager->getTitle($parsedData["device"]);
        }

        $parsedData["fields"] = null;
        if (isset($data["fields"])) {
            $parsedData["fields"] = $data["fields"];
        }

        return $parsedData;
    }

    private function checkingForCreate($parsedData)
    {
        $this->errorMessages = [];

        if (is_null($parsedData['device'])) {
            $this->errorMessages[] = "Device not found";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Device can not be added</b>.<br>"], $this->errorMessages);
        }
    }

    /**
     * @param $parsedData
     * @return mixed
     */
    private function updateDeviceByParseData($parsedData)
    {
        /** @var Device $device */
        $device = $parsedData["device"];
        if (!is_null($parsedData["location"])) {$device->setLocation($parsedData["location"]);}
        if (!is_null($parsedData["comment"])) {$device->setComments($parsedData["comment"]);}
        if (!is_null($parsedData["note"])) {$device->setNoteToTester($parsedData["note"]);}
        if (!is_null($parsedData["status"])) {$device->setStatus($parsedData["status"]);}
        if (!is_null($parsedData["title"])) {$device->setTitle($parsedData["title"]);}
        if (!is_null($parsedData["fields"])) {
            $device = $this->dynamicFieldsService->updateDevicesFields($parsedData["fields"], $device);
        }
        $this->deviceManager->save($device);
        
        return $device;
    }
}