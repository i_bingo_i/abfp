<?php

namespace AppBundle\Services\Device;

use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\TrashRequests\Creator;

class AlarmPanelsAttacherService
{
    private const ALARM_PANEL_ALIAS = "fire_alarm_control_panel";

    /** @var  ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var Creator */
    private $trashRequestCreator;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var DeviceNamedRepository */
    private $deviceNamedRepository;
    /** @var DeviceManager */
    private $deviceManager;

    private $errorMessages = [];

    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->trashRequestCreator = $this->serviceContainer->get('app.trash_requests.creator.service');
        $this->deviceManager = $this->serviceContainer->get('app.device.manager');

        $this->deviceRepository = $this->objectManager->getRepository("AppBundle:Device");
        $this->deviceNamedRepository = $this->objectManager->getRepository("AppBundle:DeviceNamed");
    }

    /**
     * @param $params
     * @param $route
     * @return bool|mixed
     */
    public function attachById($params, $route)
    {
        $parseData = $this->parseData($params);
        $this->checkingForAttach($parseData);

        if (!empty($this->errorMessages)) {
            $this->trashRequestCreator->create(
                $route,
                json_encode($params),
                $this->errorMessages,
                null,
                $params["accessToken"]
            );

            return false;
        }

        $device = $parseData["device"];
        $device->setAlarmPanel($parseData["alarmPanel"]);
        $this->deviceManager->save($device);

        return $parseData["device"];
    }

    /**
     * @param $data
     * @return array
     */
    private function parseData($data)
    {
        $parseData = [];

        $parseData["device"] = null;
        if (isset($data["deviceID"])) {
            $parseData["device"] = $this->deviceRepository->find($data["deviceID"]);
        }

        $parseData["alarmPanel"] = null;
        if (isset($data["alarmID"])) {

            /** @var DeviceNamed $deviceNameAlarmPanel */
            $deviceNameAlarmPanel = $this->deviceNamedRepository->findOneBy(['alias' => $this::ALARM_PANEL_ALIAS]);

            $parseData["alarmPanel"] = ($this->deviceRepository->findOneBy([
                "id" => $data["alarmID"],
                "named" => $deviceNameAlarmPanel
            ])) ?? false;

        }

        return $parseData;
    }

    /**
     * @param $parseData
     * @param $params
     */
    private function checkingForAttach($parseData)
    {
        $this->errorMessages = [];

        if (is_null($parseData["device"])) {
            $this->errorMessages[] = "Device not found";
        }

        if (is_bool($parseData["alarmPanel"])) {
            $this->errorMessages[] = "Alarm Panel not found";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>FACP can not be linked/unlinked to device</b>.<br>"], $this->errorMessages);
        }
    }
}