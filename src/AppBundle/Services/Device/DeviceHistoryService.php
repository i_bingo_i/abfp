<?php

namespace AppBundle\Services\Device;

use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceHistory;
use AppBundle\Entity\EntityManager\DeviceHistoryManager;
use AppBundle\Entity\Repository\DeviceHistoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DeviceHistoryService
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $container;
    /** @var DeviceHistoryRepository */
    private $deviceHistoryRepository;
    /** @var DeviceHistoryManager */
    private $deviceHistoryManager;

    /**
     * DeviceHistoryService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->deviceHistoryRepository = $this->objectManager->getRepository('AppBundle:DeviceHistory');
        $this->deviceHistoryManager = $this->container->get('app.device_history.manager');
    }

    /**
     * @param Device $device
     * @return DeviceHistory
     */
    public function checkAndGetDeviceHistory(Device $device)
    {
        /** @var DeviceHistory $deviceHistory */
        $deviceHistory = $this->deviceHistoryRepository->getLastRecord($device);
        if (!$deviceHistory) {
            $this->deviceHistoryManager->createDeviceHistoryItem($device);
        }
        /** @var DeviceHistory $deviceHistory */
        $deviceHistory = $this->deviceHistoryRepository->getLastRecord($device);

        return $deviceHistory;
    }
}
