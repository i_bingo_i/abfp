<?php

namespace AppBundle\Services\Account;

use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use Doctrine\Common\Persistence\ObjectManager;

class DivisionsUnderAccount
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var AccountRepository */
    private $accountRepository;
    /** @var DeviceRepository */
    private $deviceRepository;

    /**
     * DivisionsUnderAccount constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        $this->accountRepository = $this->objectManager->getRepository('AppBundle:Account');
        $this->deviceRepository = $this->objectManager->getRepository('AppBundle:Device');
    }

    // TODO: Зробити для Account ManyToMany -> DeviceCategory а цей код видалити
    /**
     * @param Account $account
     * @return DeviceCategory[]
     */
    public function getDivisions(Account $account)
    {
        if ($account->getType()->getAlias() === 'account') {
            return $this->getDivisionsForSiteAccount($account);
        }

        return $this->getDivisionsForGroupAccount($account);
    }

    // TODO: Зробити для Account ManyToMany -> DeviceCategory а цей код видалити
    /**
     * @param Account $groupAccount
     * @return DeviceCategory[]
     */
    private function getDivisionsForGroupAccount(Account $groupAccount)
    {
        /** @var DeviceCategory[] $divisions */
        $divisions = [];
        /** @var Account $childrenAccount */
        $childrenAccount = $this->accountRepository->findBy([
            'parent' => $groupAccount,
            'deleted' => false
        ]);

        /** @var Account $child */
        foreach ($childrenAccount as $child) {
            /** @var DeviceCategory[] $childDivisions */
            $childDivisions = $this->getDivisionsForSiteAccount($child);
            /** @var DeviceCategory $childDivision */
            foreach ($childDivisions as $childDivision) {
                $divisions[$childDivision->getId()] = $childDivision;
            }
        }

        return $divisions;
    }

    // TODO: Зробити для Account ManyToMany -> DeviceCategory а цей код видалити
    /**
     * @param Account $siteAccount
     * @return DeviceCategory[]
     */
    private function getDivisionsForSiteAccount(Account $siteAccount)
    {
        /** @var DeviceCategory[] $divisions */
        $divisions = [];
        /** @var Device[] $devices */
        $devices = $this->deviceRepository->findBy([
            'account' => $siteAccount,
            'deleted'=> false
        ]);

        /** @var Device $device */
        foreach ($devices as $device) {
            /** @var DeviceCategory $division */
            $division = $device->getNamed()->getCategory();
            $divisions[$division->getId()] = $division;
        }

        return $divisions;
    }
}
