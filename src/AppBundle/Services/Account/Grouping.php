<?php

namespace AppBundle\Services\Account;

use AppBundle\Entity\Account;

class Grouping
{
    private $groupedAccounts;

    /**
     * @param $accounts
     * @return array
     */
    public function grouping($accounts)
    {
        unset($this->groupedAccounts);
        $this->groupedAccounts = [];
        $this->groupingGroupAccouns($accounts);
        $this->groupingSiteAccounts($accounts);

        return $this->groupedAccounts;
    }

    /**
     * @param $accounts
     */
    private function groupingGroupAccouns($accounts)
    {
        /** @var Account $account */
        foreach ($accounts as $account) {
            if ($account->getType()->getAlias() == 'group account') {
                $accountId = $account->getId();
                $this->groupedAccounts[$accountId]['account'] = $account;
                $this->groupedAccounts[$accountId]['children'] = [];
            }
        }
    }

    /**
     * @param $accounts
     */
    private function groupingSiteAccounts($accounts)
    {
        /** @var Account $account */
        foreach ($accounts as $account) {
            if ($account->getType()->getAlias() == 'account') {
                /** @var Account $parent */
                $parent = $account->getParent();
                if ($parent and isset($this->groupedAccounts[$parent->getId()])) {
                    $this->groupedAccounts[$parent->getId()]['children'][] = $account;
                } else {
                    $this->groupedAccounts[$account->getId()]['account'] = $account;
                }
            }
        }
    }
}
