<?php

namespace AppBundle\Services\Account;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\EntityManager\AddressManager;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;
use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\AccountManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Authorizer
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var MessageRepository */
    private $messageRepository;
    /** @var AddressManager */
    private $addressManager;
    /** @var AccountManager */
    private $accountManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var AccountContactPersonManager */
    private $accountContactPersonManager;

    /**
     * Authorizer constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->objectManager = $objectManager;
        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
        $this->messageRepository = $this->objectManager->getRepository('AppBundle:Message');
        $this->serviceContainer = $serviceContainer;
        $this->addressManager = $this->serviceContainer->get('app.address.manager');
        $this->accountManager = $this->serviceContainer->get('app.account.manager');
        $this->accountContactPersonManager = $this->serviceContainer->get('app.account_contact_person.manager');
    }


    /**
     * @param Account $account
     * @return bool
     */
    public function checkForErrorAddress(Account $account)
    {
        $accountContacts = $this->accountContactPersonRepository->getAccountContactPersonsForAccount($account);

        /** @var AccountContactPerson $contact */
        foreach ($accountContacts as $contact) {
            if ($contact->getAuthorizer() and count($contact->getResponsibilities())) {
                $address = $contact->getSendingAddress();

                // if authorizer address has an incorrect address return error
                if ($this->addressManager->isInvalid($address)) {
                    return true;
                }
            }
        }

        // if there are not account contacts return error
        if (!$accountContacts) {
            return true;
        }

        return false;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     *
     * @return bool
     */
    public function checkOwnAuthorizerByDivision(Account $account, DeviceCategory $division)
    {
        $accountDivisions = $this->accountManager->getAccountDivisions($account);

        $accountDivisionsAlias = array();
        foreach ($accountDivisions as $key => $value) {
            $accountDivisionsAlias[] = $key;
        }

        if (!in_array($division->getAlias(), $accountDivisionsAlias)) {
            return false;
        }

        return true;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @return Account
     */
    public function getAccountWithAuthorizerForDivision(Account $account, DeviceCategory $division)
    {
        if ($account->getParent() and !$this->accountContactPersonManager->isAuthorize($account, $division)) {
            $ownerAccount = $account->getParent();

            return $ownerAccount;
        }

        return $account;
    }
}
