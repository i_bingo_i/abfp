<?php

namespace AppBundle\Services\Account;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountHistory;
use AppBundle\Entity\EntityManager\AccountHistoryManager;
use AppBundle\Entity\Repository\AccountHistoryRepository;
use AppBundle\Entity\Workorder;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WorkorderBundle\Services\WorkorderService;

class AccountHistoryService
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface  */
    private $container;
    /** @var AccountHistoryRepository */
    private $accountHistoryRepository;
    /** @var AccountHistoryManager */
    private $accountHistoryManager;
    /** @var WorkorderService */
    private $workorderService;

    /**
     * AccountHistoryService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->accountHistoryRepository = $this->objectManager->getRepository('AppBundle:AccountHistory');
        $this->accountHistoryManager = $this->container->get('app.account_history.manager');
        $this->workorderService = $this->container->get('workorder.workorder_service.service');
    }

    /**
     * @param Account $account
     * @return AccountHistory
     */
    public function checkAndGetAccountHistory(Account $account)
    {
        /** @var AccountHistory $accountHistory */
        $accountHistory = $this->accountHistoryRepository->getLastRecord($account);
        if (!$accountHistory) {
            $this->accountHistoryManager->createAccountHistoryItem($account);
        }
        /** @var AccountHistory $accountHistory */
        $accountHistory = $this->accountHistoryRepository->getLastRecord($account);

        return $accountHistory;
    }

    /**
     * @param Account $account
     * @param Workorder $workorder
     * @return AccountHistory
     */
    public function checkAndGetAccountHistoryForFrozeWo(Account $account, Workorder $workorder)
    {
        $param['beforeDate'] = $this->workorderService->getFrozenDate($workorder)->format("Y-m-d H:i:s");

        /** @var AccountHistory $accountHistory */
        $accountHistory = $this->accountHistoryRepository->getLastRecord($account, $param);
        if (!$accountHistory) {
            $this->accountHistoryManager->createAccountHistoryItem($account);
        }
        /** @var AccountHistory $accountHistory */
        $accountHistory = $this->accountHistoryRepository->getLastRecord($account, $param);

        return $accountHistory;
    }
}
