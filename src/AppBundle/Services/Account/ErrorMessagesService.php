<?php

namespace AppBundle\Services\Account;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\MessageManager;
use AppBundle\Entity\Message;
use AppBundle\Entity\Repository\MessageRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\AccountContactPersonRepository;

class ErrorMessagesService
{
    /** @var ContainerInterface */
    private $container;
    /** @var ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var MessageRepository */
    private $messageRepository;
    /** @var MessageManager */
    private $messageManager;
    /** @var DivisionsUnderAccount */
    private $divisionsUnderAccountService;

    private const AUTHORIZER_ERROR_MESSAGES_BY_DIVISION = [
        'backflow' => "No Primary Authorizer Contact for Backflow Services",
        'fire' => "No Primary Authorizer Contact for Fire Services",
        'plumbing' => "No Primary Authorizer Contact for Plumbing Services",
        'alarm' => "No Primary Authorizer Contact for Alarm Services"
    ];
    private const PAYMENT_CONTACT_ERROR_MESSAGE_BY_DIVISION = [
        'backflow' => "No Primary Payment Contact for Backflow Services",
        'fire' => "No Primary Payment Contact for Fire Services",
        'plumbing' => "No Primary Payment Contact for Plumbing Services",
        'alarm' => "No Primary Payment Contact for Alarm Services"
    ];
    private const OWNER_AUTHORIZER_ERROR_MESSAGE = 'This site uses its own Authorizer(s)';
    private const ACP_ON_GROUP_ACCOUNT_LEVEL_ERROR_MESSAGE = 'Please see Contact Person(s) on Group Account level';
    private const ACCESS_CONTACT_ERROR_MESSAGE = 'No Primary Access Contact';

    /**
     * ErrorMessagesService constructor.
     * @param ContainerInterface $container
     * @param ObjectManager $objectManager
     */
    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->messageManager = $this->container->get('app.message.manager');
        $this->divisionsUnderAccountService = $this->container->get('app.account_divions_under_account.service');
        $this->messageRepository = $this->objectManager->getRepository('AppBundle:Message');
        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
    }

    /**
     * @param Account $account
     * @return array
     */
    public function getErrorMessages(Account $account)
    {
        $errorMessages = [];

        /** @var Message $messageSiteHasOwnAuthorizer */
        $messageSiteHasOwnAuthorizer = $this->messageRepository->findOneBy(['alias' => 'site_has_own_authorizer']);
        $isOwnerAuthorizer = $this->messageManager->isMessage($account->getMessages(), $messageSiteHasOwnAuthorizer);
        if ($isOwnerAuthorizer) {
            $errorMessages[] = self::OWNER_AUTHORIZER_ERROR_MESSAGE;
        }

        if ($account->getParent()) {
            $errorMessages[] = self::ACP_ON_GROUP_ACCOUNT_LEVEL_ERROR_MESSAGE;

            return $errorMessages;
        }

        /** @var DeviceCategory[] $divisions */
        $divisions = $this->divisionsUnderAccountService->getDivisions($account);
        /** @var DeviceCategory $division */
        foreach ($divisions as $division) {
            $isAuthorizerDivision = $this->isAutorizerByDivision($account, $division);
            $isPaymentContactDivision = $this->isPaymentByDivisionAndAccount($account, $division);
            if (!$isAuthorizerDivision) {
                $errorMessages[] = self::AUTHORIZER_ERROR_MESSAGES_BY_DIVISION[$division->getAlias()];
            }
            if (!$isPaymentContactDivision) {
                $errorMessages[] = self::PAYMENT_CONTACT_ERROR_MESSAGE_BY_DIVISION[$division->getAlias()];
            }
        }

        /** @var AccountContactPerson $accessContact */
        $accessContact =  $this->accountContactPersonRepository->findOneBy([
            'account' => $account,
            'accessPrimary' => true,
            'deleted' => false
        ]);
        if (!$accessContact) {
            $errorMessages[] = self::ACCESS_CONTACT_ERROR_MESSAGE;
        }

        return $errorMessages;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @return bool
     */
    private function isAutorizerByDivision(Account $account, DeviceCategory $division)
    {
        /** @var AccountContactPerson[] $contacts */
        $contacts =  $this->accountContactPersonRepository->findBy([
            'account' => $account,
            'deleted' => false
        ]);

        /** @var AccountContactPerson $contact */
        foreach ($contacts as $contact) {
            foreach ($contact->getResponsibilities() as $responsibility) {
                if ($responsibility === $division) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @return bool
     */
    private function isPaymentByDivisionAndAccount(Account $account, DeviceCategory $division)
    {
        /** @var AccountContactPerson[] $contacts */
        $contacts =  $this->accountContactPersonRepository->findBy([
            'account' => $account,
            'deleted' => false
        ]);

        /** @var AccountContactPerson $contact */
        foreach ($contacts as $contact) {
            foreach ($contact->getPaymentResponsibilities() as $responsibility) {
                if ($responsibility === $division) {
                    return true;
                }
            }
        }

        return false;
    }
}
