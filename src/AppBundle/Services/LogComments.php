<?php

namespace AppBundle\Services;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\ContactPerson;

class LogComments
{
    /**
     * Options params[]:
     * comment <string> null by default
     * authorizer <AccountContactPerson> null by default
     *
     * @param $params
     * @return mixed
     */
    public function parseByParams($params)
    {
        /** @var AccountContactPerson $authorizer */
        $authorizer = $params['authorizer'] ?? null;
        $comment = $params['comment'] ?? null;

        if (!empty($authorizer)) {
            /** @var ContactPerson $contactPerson */
            $contactPerson = $authorizer->getContactPerson();
            $authorizerTitle = 'Authorized by ';
            $authorizerTitle .= $contactPerson->getFirstName() . ' ' . $contactPerson->getLastName() . '. ';
            $comment = $authorizerTitle . $comment;
        }

        return $comment;
    }
}
