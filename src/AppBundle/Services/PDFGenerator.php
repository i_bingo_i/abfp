<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Knp\Snappy\GeneratorInterface;

class PDFGenerator
{
    /** @var ContainerInterface */
    private $container;

    /**
     * PDFGenerator constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Return file when string
     * if need save file use function "file_put_contents"
     *
     * @param $html
     * @param array $options
     *
     * @return string
     */
    public function make($html, $options = [])
    {
        /** @var GeneratorInterface $knpSnappy */
        $knpSnappy = $this->container->get('knp_snappy.pdf');
        $overwroteOptions = $this->overwriteOptions($options);

        return $knpSnappy->getOutputFromHtml($html, $overwroteOptions);
    }

    /**
     * Change default options
     *
     * @param $options
     * @return array
     */
    private function overwriteOptions($options)
    {
        $defaultOptions = [
            'page-height' => 310,
            'page-width' => 220,
            'margin-left' => 10,
            'margin-right' => 10,
            'margin-bottom' => 30,
            'margin-top' => 20,
            'header-spacing' => 5,
            'footer-spacing' => 5
        ];

        foreach ($options as $key => $option) {
            $defaultOptions[$key] = $option;
        }

        return $defaultOptions;
    }
}
