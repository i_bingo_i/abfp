<?php

namespace AppBundle\Services;

class GeoDistance
{
    const EARTH_RADIUS = 6371009;

    /**
     * @param float $lat1 latitude of the first point IN RADIANS
     * @param float $lng1 longitude of the first point IN RADIANS
     * @param float $lat2 latitude of the second point IN RADIANS
     * @param float $lng2 longitude of the second point IN RADIANS
     * @return float distance between two points
     */
    public function getDistanceBetweenPoints(float $lat1, float $lng1, float $lat2, float $lng2) : float
    {
        $length = self::distanceRadians($lat1, $lng1, $lat2, $lng2);

        return $length * self::EARTH_RADIUS;
    }

    /**
     * @param float $lat1 latitude of the first point IN RADIANS
     * @param float $lng1 longitude of the first point IN RADIANS
     * @param float $lat2 latitude of the second point IN RADIANS
     * @param float $lng2 longitude of the second point in RADIANS
     * @param float $maxDistance maximum distance between points
     * @return bool true, if the distance between points not greater than $maxDistance, false - otherwise
     */
    public function isDistanceBetweenPointsGreaterThan(
        float $lat1,
        float $lng1,
        float $lat2,
        float $lng2,
        float $maxDistance
    ) : bool {
        $distance = $this->getDistanceBetweenPoints($lat1, $lng1, $lat2, $lng2);

        return ($distance <= $maxDistance) ? true : false;
    }

    /**
     * @param float $x
     * @return float
     */
    private static function hav(float $x) : float
    {
        $sinHalf = sin($x * 0.5);
        return $sinHalf * $sinHalf;
    }

    /**
     * @param float $x
     * @return float
     */
    private static function arcHav(float $x) : float
    {
        return 2 * asin(sqrt($x));
    }

    /**
     * @param float $latitude1
     * @param float $latitude2
     * @param float $longitudesDiff
     * @return float
     */
    private static function havDistance(float $latitude1, float $latitude2, float $longitudesDiff) : float
    {
        return self::hav($latitude1 - $latitude2) + self::hav($longitudesDiff) * cos($latitude1) * cos($latitude2);
    }

    /**
     * @param float $latitude1
     * @param float $longitude1
     * @param float $latitude2
     * @param float $longitude2
     * @return float
     */
    private static function distanceRadians(float $latitude1, float $longitude1, float $latitude2, float $longitude2) : float
    {
        return self::arcHav(self::havDistance($latitude1, $latitude2, $longitude1 - $longitude2));
    }
}
