<?php

namespace AppBundle\Services\Workorder;

use AppBundle\Entity\Workorder;
use AppBundle\Services\AccountContactPerson\Authorizer;
use AppBundle\Services\Event\Compose;
use AppBundle\Services\PDFGenerator;
use AppBundle\Services\Tree\Grouping;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bridge\Twig\TwigEngine;

class PDFManager
{
    /** @var ContainerInterface */
    private $container;
    /** @var TwigEngine $templating */
    private $templating;
    private $webDirPath;
    private $defaultSavePdfPath;

    /**
     * PDFManager constructor.
     * @param ContainerInterface $container
     * @param $webDirPath
     */
    public function __construct(ContainerInterface $container, $webDirPath)
    {
        $this->container = $container;
        $this->templating = $this->container->get('templating');

        $this->webDirPath = $webDirPath;
        $this->defaultSavePdfPath = $this->webDirPath . '/uploads/workorders/';
    }

    /**
     * Return file when string
     *
     * @param Workorder $workorder
     *
     * @return string
     */
    public function create(Workorder $workorder)
    {
        /** @var PDFGenerator $pdfGenerator */
        $pdfGenerator = $this->container->get('app.pdf_generator');
        $divisionAlias = $workorder->getDivision()->getAlias();

        $assetsPackage = 'pdf';
        $headerHtml = $this->templating->render("AdminBundle:Workorder:pdf/header.html.twig", [
            'assetsPackage' => $assetsPackage
        ]);
        $footerHtml = $this->templating->render("AdminBundle:Workorder:pdf/footer-{$divisionAlias}.html.twig", [
            'assetsPackage' => $assetsPackage
        ]);
        $htmlTemplate = $this->renderToHtml($workorder, $assetsPackage);

        return $pdfGenerator->make($htmlTemplate, [
            'header-html' => $headerHtml,
            'footer-html' => $footerHtml
        ]);
    }

    /**
     * Return WO pdf file name
     *
     * @param Workorder $workorder
     * @param string|null $savePdfPath
     *
     * @return string
     */
    public function save(Workorder $workorder, $savePdfPath = null)
    {
        $stringPdf = $this->create($workorder);

        if (!$savePdfPath) {
            $savePdfPath = $this->defaultSavePdfPath;
        }

        $fileName = $this->getFileName($workorder);
        file_put_contents($savePdfPath . $fileName, $stringPdf);

        return $fileName;
    }

    /**
     * @param Workorder $workorder
     * @param null $assetsPackage
     *
     * @return string
     */
    public function renderToHtml(Workorder $workorder, $assetsPackage = null)
    {
        /** @var Grouping $treeService */
        $treeService = $this->container->get('app.tree.service');
        /** @var Authorizer $acpAuthorizerService */
        $acpAuthorizerService = $this->container->get('app.account_contact_person_authorizer.service');
        /** @var Compose $eventCompose */
        $eventCompose = $this->container->get('app.event_compose.service');

        $divisionAlias = $workorder->getDivision()->getAlias();
        $template = "AdminBundle:Workorder:pdf/index-{$divisionAlias}.html.twig";

        return $this->templating->render($template, [
            'tree' => $treeService->getTreeForWorkorder($workorder),
            'groupingServices' => $eventCompose->composeByWorkorder($workorder),
            'accountAuthorizer' => $acpAuthorizerService->getAccountAuthorizerForWorkorder($workorder),
            'parameters' => $workorder,
            'assetsPackage' => $assetsPackage
        ]);
    }

    /**
     * @param Workorder $workorder
     *
     * @return string
     */
    public function getFileName(Workorder $workorder)
    {
        $division = $workorder ? $workorder->getDivision()->getName() : '';
        $dateCreate = $workorder->getDateCreate()->format('m-d-Y');
        $workorderId = $workorder->getId();

        return $division . '_Workorder_' . $dateCreate . '_id_' . $workorderId . '.pdf';
    }
}
