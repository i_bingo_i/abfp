<?php

namespace AppBundle\Services\Opportunity;

use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\Account\Authorizer;
use AppBundle\Entity\Account;
use AppBundle\Entity\Opportunity;
use AppBundle\Entity\DeviceCategory;

class Grouping
{
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var Authorizer */
    private $accountAuthorizerService;

    /**
     * Grouping constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->accountAuthorizerService = $this->serviceContainer->get('app.account_authorizer.service');
    }

    /**
     * Result:
     * (array)[
     *      (array)[
     *          'division' => (entity)DeviceCategory,
     *          'account' => (entity)Account
     *          'opportunities' => (array)[
     *              (entity)Opportunity,
     *              ...
     *          ]
     *      ],
     *      ...
     * ]
     * @param Opportunity[] $opportunities
     * @return array
     */
    public function groupingByDivisionAndAccount($opportunities)
    {
        $result = [];
        /** @var Opportunity $opportunity */
        foreach ($opportunities as $opportunity) {
            /** @var Account $account */
            $account = $opportunity->getAccount();
            /** @var DeviceCategory $division */
            $division = $opportunity->getDivision();

            $ownerAccount = $this->accountAuthorizerService->getAccountWithAuthorizerForDivision($account, $division);

            $resultId = $division->getId() . $ownerAccount->getId();
            if (empty($result[$resultId])) {
                $result[$resultId]['division'] = $division;
                $result[$resultId]['account'] = $ownerAccount;
            }

            $result[$resultId]['opportunities'][] = $opportunity;
        }

        return $result;
    }
}
