<?php

namespace AppBundle\Services\Coordinates;

use AppBundle\Entity\Account;
use AppBundle\Entity\Coordinate;
use AppBundle\Entity\Repository\CoordinateRepository;
use AppBundle\Services\GeoLocation;
use Doctrine\Common\Persistence\ObjectManager;
use Geocoder\Model\Coordinates;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Helper
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var GeoLocation */
    private $geoLocationService;
    /** @var CoordinateRepository */
    private $coordinateRepository;

    /**
     * Helper constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $serviceContainer;

        $this->geoLocationService = $this->serviceContainer->get('app.geolocation_service');
        $this->coordinateRepository = $this->objectManager->getRepository('AppBundle:Coordinate');
    }

    /**
     * @param Account $account
     */
    public function refreshByAccount(Account $account)
    {
        $coordinatesObject = $this->geoLocationService->getCoordinatesByAddress($account->getAddress());

        if ($coordinatesObject instanceof Coordinates) {
            $coordinate = $this->coordinateRepository->findOneBy([
                'latitude' => $coordinatesObject->getLatitude(),
                'longitude' => $coordinatesObject->getLongitude(),
            ]);

            if ($coordinate == null) {
                $coordinate = new Coordinate();
                $coordinate->setLatitude($coordinatesObject->getLatitude());
                $coordinate->setLongitude($coordinatesObject->getLongitude());
                $this->objectManager->persist($coordinate);
            }

            $account->setCoordinate($coordinate);
        }

        $this->objectManager->flush();
    }
}
