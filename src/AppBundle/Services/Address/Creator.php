<?php

namespace AppBundle\Services\Address;

use AppBundle\Entity\Address;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\EntityManager\AddressManager;
use AppBundle\Entity\Repository\AddressTypeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Creator
{
    private const BILLING_ADDRESS_TYPE = 'billing';
    private const PERSONAL_ADDRESS_TYPE = 'personal';
    private const MUNICIPALITY_ADDRESS_TYPE = 'municipality';

    /** @var ObjectManager */
    private $objectManager;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var AddressManager */
    private $addressManager;

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;

        $this->addressTypeRepository = $this->objectManager->getRepository('AppBundle:AddressType');
        $this->addressManager = $this->serviceContainer->get('app.address.manager');
    }

    /**
     * @deprecated This method is deprecated. Please use /src/AppBundle/Creators/AddressCreator.php
     * @return Address
     */
    public function createWithBillingType()
    {
        /** @var Address $address */
        $address = $this->createByType(self::BILLING_ADDRESS_TYPE);

        return $address;
    }

    /**
     * @deprecated This method is deprecated. Please use /src/AppBundle/Creators/AddressCreator.php
     * @return Address
     */
    public function createWithPersonalType()
    {
        /** @var Address $address */
        $address = $this->createByType(self::PERSONAL_ADDRESS_TYPE);

        return $address;
    }

    /**
     * @deprecated This method is deprecated. Please use /src/AppBundle/Creators/AddressCreator.php
     * @return Address
     */
    public function createWithMunicipalityType()
    {
        /** @var Address $address */
        $address = $this->createByType(self::MUNICIPALITY_ADDRESS_TYPE);

        return $address;
    }

    /**
     * @param Address $address
     * @return Address
     */
    public function cloneAddress(Address $address)
    {
        /** @var Address $newAddress */
        $newAddress = clone $address;
        $this->addressManager->save($newAddress);

        return $newAddress;
    }

    /**
     * @param Address $address
     * @param string|null $addressType
     */
    public function save(Address $address, $addressType = null)
    {
        if (!empty($addressType)) {
            $this->setType($address, $addressType);
        }

        $this->addressManager->save($address);
    }

    /**
     * @param Address $address
     * @param $addressType
     */
    public function setType(Address $address, $addressType)
    {
        /** @var AddressType $addressType */
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => $addressType]);

        $address->setAddressType($addressType);
    }

    /**
     * @param $addressTypeAlias
     * @return Address
     */
    private function createByType($addressTypeAlias)
    {
        $address = new Address();
        $this->setType($address, $addressTypeAlias);
        $this->objectManager->persist($address);

        return $address;
    }
}
