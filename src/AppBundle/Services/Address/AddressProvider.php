<?php

namespace AppBundle\Services\Address;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use InvoiceBundle\Entity\Customer;

class AddressProvider
{
    /** @var Company */
    public $otherCompany;

    /**
     * @param AccountContactPerson $accountContactPerson
     * Corresponding AccountCompany, ContactPerson, ContactPersonCompany, OtherCompany
     * @param array $customers
     * @param Company $otherCompany
     *
     * @return array
     */
    public function getPersonalBillingAddresses(
        AccountContactPerson $accountContactPerson,
        array $customers = [],
        ?Company $otherCompany = null
    ) {
        $res = [
            'Account' => [],
            'Account Person' => [],
            'Person Company' => [],
            'Account Company' => [],
            'Other Company' => []
        ];

        $res['Account']['name'] = $accountContactPerson->getAccount()->getName();
        $res['Account']['owner'] = $accountContactPerson->getAccount();
        $res['Account']['personal'] = $accountContactPerson->getAccount()->getAddress();

        if (!empty($accountContactPerson->getContactPerson())) {
            /** @var ContactPerson $contactPerson */
            $contactPerson = $accountContactPerson->getContactPerson();

            $name = $contactPerson->getFirstName() . ' ' . $contactPerson->getLastName();
            $res['Account Person']['name'] = $name;
            $res['Account Person']['owner'] = $accountContactPerson->getContactPerson();
            $res['Account Person']['billing'] = $this->getBilling($customers['Account Person']);
            $res['Account Person']['personal'] = $accountContactPerson->getContactPerson()->getPersonalAddress();

            if (!empty($accountContactPerson->getContactPerson()->getCompany())) {
                $company = $accountContactPerson->getContactPerson()->getCompany();
                $res['Person Company']['name'] = $company->getName();
                $res['Person Company']['owner'] = $accountContactPerson->getContactPerson()->getCompany();
                $res['Person Company']['billing'] = $this->getBilling($customers['Person Company']);
                $res['Person Company']['personal'] = $accountContactPerson->getContactPerson()->getCompany()
                    ->getAddress();
            }
        }

        if (!empty($accountContactPerson->getAccount()->getCompany())) {
            /** @var Company $company */
            $company = $accountContactPerson->getAccount()->getCompany();
            $res['Account Company']['name'] = $company->getName();
            $res['Account Company']['owner'] = $company;
            $res['Account Company']['billing'] = $this->getBilling($customers['Account Company']);
            $res['Account Company']['personal'] = $accountContactPerson->getAccount()->getCompany()->getAddress();
        }

        if (!empty($otherCompany)) {
            $res['Other Company']['name'] = $otherCompany->getName();
            $res['Other Company']['owner'] = $otherCompany;
            $res['Other Company']['billing'] = $this->getBilling($customers['Other Company']);
            $res['Other Company']['personal'] = $otherCompany->getAddress();
        }

        return $res;
    }

    /**
     * @param Customer|null $customer
     * @return \AppBundle\Entity\Address|null
     */
    public function getBilling(?Customer $customer)
    {
        $res = null;

        if ($customer) {
            $res = $customer->getBillingAddress();
        }

        return $res;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @return \AppBundle\Entity\Address
     */
    public static function getBillToAddressByACP(AccountContactPerson $accountContactPerson)
    {
        $billToAddress = $accountContactPerson->getCustomer()->getBillingAddress();

        if (!empty($accountContactPerson->getCustomBillingAddress()->getAddress())) {
            $billToAddress = $accountContactPerson->getCustomBillingAddress();
        }

        return $billToAddress;
    }

    /**
     * @param Address $address
     */
    public static function setInNullAddress(Address $address)
    {
        $address->setAddress();
        $address->setCity();
        $address->setZip();
        $address->setState();
    }
}
