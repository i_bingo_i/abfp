<?php

namespace AppBundle\Services;

use AppBundle\Entity\ElasticaRepository\ContractorRepository;
use AppBundle\Entity\ElasticaRepository\MunicipalityRepository;
use AppBundle\Entity\ElasticaRepository\ServiceNamedRepository;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use AppBundle\Entity\ElasticaRepository\AccountRepository;
use AppBundle\Entity\ElasticaRepository\ContactPersonRepository;
use AppBundle\Entity\ElasticaRepository\CompanyRepository;
use AppBundle\Entity\ElasticaRepository\ISearchRepository;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\AccountType;
use AppBundle\Entity\Repository\AccountTypeRepository;

class Search
{
    const DEFAULT_MAX_ITEMS = 15;
    const DEFAULT_MAX_RESULTS_COUNT = 500;

    /** @var RepositoryManagerInterface */
    private $fosElasticaRepositoryManager;
    /** @var ObjectManager $objectManager */
    private $objectManager;

    /**
     * Search constructor.
     * @param RepositoryManagerInterface $fosElasticaRepositoryManager
     * @param ObjectManager $objectManager
     */
    public function __construct(RepositoryManagerInterface $fosElasticaRepositoryManager, ObjectManager $objectManager)
    {
        $this->fosElasticaRepositoryManager = $fosElasticaRepositoryManager;
        $this->objectManager = $objectManager;
    }

    /**
     * @param $searchOptions
     * @param int $maxItems
     * @param int $maxResultsCount
     * @return mixed
     */
    public function searchEverywhere(
        $searchOptions,
        $maxItems = self::DEFAULT_MAX_ITEMS,
        $maxResultsCount = self::DEFAULT_MAX_RESULTS_COUNT
    ) {
        if ($this->isSearch($searchOptions)) {
            $treatmentedSearchOptions = $this->treatmentSearchOptions($searchOptions);

            /** @var AccountRepository $elasticAccountRepository */
            $elasticAccountRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:Account');
            $result['accounts'] = $this->searchByElasticRepository(
                $elasticAccountRepository,
                $treatmentedSearchOptions,
                $maxItems,
                $maxResultsCount
            );

            /** @var ContactPersonRepository $elasticContactPersonRepository */
            $elasticContactPersonRepository = $this->fosElasticaRepositoryManager
                ->getRepository('AppBundle:ContactPerson');
            $result['contacts'] = $this->searchByElasticRepository(
                $elasticContactPersonRepository,
                $treatmentedSearchOptions,
                $maxItems,
                $maxResultsCount
            );

            /** @var CompanyRepository $elasticCompanyRepository */
            $elasticCompanyRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:Company');
            $result['companies'] = $this->searchByElasticRepository(
                $elasticCompanyRepository,
                $treatmentedSearchOptions,
                $maxItems,
                $maxResultsCount
            );
        }

        return isset($result) ? $result : null;
    }

    /**
     * @param $searchOptions
     * @param int $maxItems
     * @param int $maxResultsCount
     * @return mixed|null
     */
    public function searchCompany(
        $searchOptions,
        $maxItems = self::DEFAULT_MAX_ITEMS,
        $maxResultsCount = self::DEFAULT_MAX_RESULTS_COUNT
    ) {
        if ($this->isSearch($searchOptions)) {
            $treatmentedSearchOptions = $this->treatmentSearchOptions($searchOptions);

            /** @var CompanyRepository $elasticCompanyRepository */
            $elasticCompanyRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:Company');
            return $this->searchByElasticRepository(
                $elasticCompanyRepository,
                $treatmentedSearchOptions,
                $maxItems,
                $maxResultsCount
            );
        }

        return isset($result) ? $result : null;
    }

    /**
     * @param $searchOptions
     * @param int $maxItems
     * @param int $maxResultsCount
     * @return mixed|null
     */
    public function searchContactPerson(
        $searchOptions,
        $maxItems = self::DEFAULT_MAX_ITEMS,
        $maxResultsCount = self::DEFAULT_MAX_RESULTS_COUNT
    ) {
        if ($this->isSearch($searchOptions)) {
            $treatmentedSearchOptions = $this->treatmentSearchOptions($searchOptions);

            /** @var ContactPersonRepository $elasticContactPersonRepository */
            $elasticContactPersonRepository = $this->fosElasticaRepositoryManager->
                getRepository('AppBundle:ContactPerson');

            return $this->searchByElasticRepository(
                $elasticContactPersonRepository,
                $treatmentedSearchOptions,
                $maxItems,
                $maxResultsCount
            );
        }

        return isset($result) ? $result : null;
    }

    /**
     * @param $searchOptions
     * @param int $maxItems
     * @param int $maxResultsCount
     * @return mixed|null
     */
    public function searchSiteAccount(
        $searchOptions,
        $maxItems = self::DEFAULT_MAX_ITEMS,
        $maxResultsCount = self::DEFAULT_MAX_RESULTS_COUNT
    ) {
        if ($this->isSearch($searchOptions)) {
            $treatmentedSearchOptions = $this->treatmentSearchOptions($searchOptions);
            /** @var AccountTypeRepository $accountTypeRepository */
            $accountTypeRepository = $this->objectManager->getRepository('AppBundle:AccountType');
            /** @var AccountType $accountTypeSiteAccount */
            $accountTypeSiteAccount = $accountTypeRepository->findOneBy(['alias' => 'account']);

            /** @var AccountRepository $elasticAccountRepository */
            $elasticAccountRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:Account');
            $searchResult = $elasticAccountRepository->findAccountByType(
                $treatmentedSearchOptions,
                $accountTypeSiteAccount,
                $maxItems,
                $maxResultsCount
            );
            if (!$this->isResult($searchResult)) {
                $searchResult['message'] = "No results";
            }

            return $searchResult;
        }

        return isset($result) ? $result : null;
    }

    /**
     * Search contractor by some letters
     * @param $searchOptions
     * @return array
     */
    public function searchContractor($searchOptions)
    {
        $treatmentedSearchOptions = trim($searchOptions);
        /** @var ContractorRepository $elasticContractorRepository */
        $elasticContractorRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:Contractor');

        $searchResult = $elasticContractorRepository->findWithCustomQuery($treatmentedSearchOptions);
        if (!$this->isResult($searchResult)) {
            $searchResult['message'] = "No results";
        }

        return $searchResult;
    }

    /**
     * Search Municipality by some letters
     * @param $searchOptions
     * @return array
     */
    public function searchMunicipalityForAccount($searchOptions)
    {
        $treatmentedSearchOptions = trim($searchOptions);
        /** @var MunicipalityRepository $elasticContractorRepository */
        $elasticContractorRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:Municipality');

        $searchResult = $elasticContractorRepository->findWithCustomQuery($treatmentedSearchOptions);
        if (!$this->isResult($searchResult)) {
            $searchResult['message'] = "No results";
        }

        return $searchResult;
    }

    /**
     * @param $searchOptions
     * @param int $maxItems
     * @param int $maxResultsCount
     * @return array|null
     */
    public function searchContractorExtended(
        $searchOptions,
        $maxItems = self::DEFAULT_MAX_ITEMS,
        $maxResultsCount = self::DEFAULT_MAX_RESULTS_COUNT
    ) {
        $treatmentedSearchOptions = $this->treatmentSearchOptions($searchOptions);
        /** @var ContractorRepository $elasticContractorRepository */
        $elasticContractorRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:Contractor');

        $searchResult = $elasticContractorRepository->findContractor(
            $treatmentedSearchOptions,
            $maxItems,
            $maxResultsCount
        );

        if (!$this->isResult($searchResult)) {
            $searchResult['message'] = "No results";
        }

        return $searchResult;
    }

    /**
     * @param $searchOptions
     * @param int $maxItems
     * @param int $maxResultsCount
     * @return array
     */
    public function searchMunicipality(
        $searchOptions,
        $maxItems = self::DEFAULT_MAX_ITEMS,
        $maxResultsCount = self::DEFAULT_MAX_RESULTS_COUNT
    ) {
        $treatmentedSearchOptions = $this->treatmentSearchOptions($searchOptions);
        /** @var MunicipalityRepository $elasticContractorRepository */
        $elasticContractorRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:Municipality');

        $searchResult = $elasticContractorRepository->findMunicipality(
            $treatmentedSearchOptions,
            $maxItems,
            $maxResultsCount
        );

        if (!$this->isResult($searchResult)) {
            $searchResult['message'] = "No results";
        }

        return $searchResult;
    }

    /**
     * @param $searchOptions
     * @return array
     */
    public function searchServiceNameNooFrequency($searchOptions)
    {
        $treatmentedSearchOptions = [];
        foreach ($searchOptions as $key => $option) {
            $treatmentedSearchOptions[$key] = trim($option);
        }
//        $treatmentedSearchOptions = trim($searchOptions);
        /** @var ServiceNamedRepository $elasticContractorRepository */
        $elasticContractorRepository = $this->fosElasticaRepositoryManager->getRepository('AppBundle:ServiceNamed');

        $searchResult = $elasticContractorRepository->findWithCustomQuery($treatmentedSearchOptions);
        if (!$this->isResult($searchResult)) {
            $searchResult['message'] = "No results";
        }

        return $searchResult;
    }

    /**
     * @param $searchOptions
     * @return mixed
     */
    private function treatmentSearchOptions($searchOptions)
    {
        if (isset($searchOptions['searchPhrase'])) {
            $searchOptions['searchPhrase'] = trim($searchOptions['searchPhrase']);
        }

        if (!isset($searchOptions['clientType'])) {
            $searchOptions['clientType'] = [];
        }

        if (!isset($searchOptions['a_page']) or $searchOptions['a_page'] == 0) {
            $searchOptions['a_page'] = 1;
        }

        if (!isset($searchOptions['c_page']) or $searchOptions['c_page'] == 0) {
            $searchOptions['c_page'] = 1;
        }

        if (!isset($searchOptions['cn_page']) or $searchOptions['cn_page'] == 0) {
            $searchOptions['cn_page'] = 1;
        }

        return $searchOptions;
    }

    /**
     * @param $searchOptions
     * @return bool
     */
    private function isSearch($searchOptions)
    {
        if (isset($searchOptions['searchPhrase'])
            and ($searchOptions['searchPhrase'] or $searchOptions['searchPhrase'] === '0')) {
            return true;
        }

        return false;
    }

    /**
     * @param $searchResult
     * @return bool
     */
    private function isResult($searchResult)
    {
        if (isset($searchResult['count']) and $searchResult['count'] == 0) {
            return false;
        }

        return true;
    }

    /**
     * @param ISearchRepository $repository
     * @param $searchOptions
     * @param $maxItems
     * @param $maxResultsCount
     * @return mixed
     */
    private function searchByElasticRepository(
        ISearchRepository $repository,
        $searchOptions,
        $maxItems,
        $maxResultsCount
    ) {
        $searchResult = $repository->findWithCustomQuery($searchOptions, $maxItems, $maxResultsCount);
        if (!$this->isResult($searchResult)) {
            $searchResult['message'] = "No results";
        }

        return $searchResult;
    }
}
