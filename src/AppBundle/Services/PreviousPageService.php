<?php

namespace AppBundle\Services;

use AppBundle\Entity\Device;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Router;

class PreviousPageService
{
    private const SIMILARITY_VALUE = 95;
    /** @var ContainerInterface */
    private $container;
    /** @var object|\Symfony\Component\HttpFoundation\Session\Session */
    private $session;
    /** @var Router */
    private $router;

    /**
     * PreviousPageService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->session = $this->container->get('session');
        $this->router = $this->container->get('router');
    }

    /**
     * @param Request $request
     * @param Device $device
     * @return mixed|string
     */
    public function getByRequestAndDevice(Request $request, Device $device)
    {
        $this->removeLogin();
        if ($this->session->has('previousPage')) {
            $previousPage = $this->session->get('previousPage');
        } elseif ($request->headers->has('referer')) {
            $previousPage = $request->headers->get('referer');
            $this->session->set('previousPage', $previousPage);
        } else {
            $previousPage = $this->router->generate('admin_device_view', ["device" => $device->getId()], true);
            $this->session->set('previousPage', $previousPage);
        }

        return $previousPage;
    }

    /**
     * @param Request $request
     * @param Device $device
     * @return array|string
     */
    public function getForInspectionByRequestAndDevice(Request $request, Device $device)
    {
        $this->removeLogin();

        $currentPage = $request->getUri();
        $referPreviousPage = $request->headers->has('referer') ? $request->headers->get('referer') : false;
        $requestPreviousPage = $request->request->has("prevPage") ? $request->request->get("prevPage") : false;
        $sessionPreviousPage = $this->session->has('previousPage') ? $this->session->get('previousPage') : false;

        if ($requestPreviousPage) {
            $previousPage = $requestPreviousPage;
        } elseif($referPreviousPage and !$this->isPathsSimilar($referPreviousPage, $currentPage)){
            $previousPage = $referPreviousPage;
        } elseif($sessionPreviousPage and !$this->isPathsSimilar($sessionPreviousPage, $currentPage)) {
            $previousPage = $sessionPreviousPage;
        } else {
            $previousPage = $this->router->generate('admin_device_view', ["device" => $device->getId()], true);
        }

        $this->session->set('previousPage', $previousPage);

        return $previousPage;
    }

    /**
     *
     */
    private function removeLogin()
    {
        if ($this->session->has('previousPage') and stristr($this->session->get('previousPage'), 'login') !== false) {
            $this->session->remove('previousPage');
        }
    }

    /**
     * @param $path1
     * @param $path2
     * @return bool
     */
    private function isPathsSimilar($path1, $path2)
    {
        similar_text(
            $this->clearPathString($path1),
            $this->clearPathString($path2),
            $similar
        );

        if ($similar > $this::SIMILARITY_VALUE) {
            return true;
        }

        return false;
    }

    /**
     * @param $pathString
     * @return string
     */
    private function clearPathString($pathString)
    {
        if (strpos($pathString, "?") > 0) {
            return strstr($pathString, '?', true);
        }

        return $pathString;
    }
}
