<?php

namespace AppBundle\Services\Service;

use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Service;
use AppBundle\Entity\Workorder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Unlinked
{
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ContainerInterface  */
    private $container;

    /**
     * Unlinked constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->serviceManager = $this->container->get('app.service.manager');
    }

    public function unlinkFromWorkorderAndResetStatus(Workorder $workorder)
    {
        $services = $workorder->getServices();
        /** @var Service $service */
        foreach ($services as $service) {
            $opportunityType = 'retest';
            if (!$service->getNamed()->getFrequency()) {
                $opportunityType = 'repair';

                if (!$service->getLastReport()) {
                    $this->serviceManager->attachOpportunity($service, $opportunityType);
                } else {
                    $this->serviceManager->setResolvedStatus($service);
                }
            } else {
                $this->serviceManager->attachOpportunity($service, $opportunityType);
            }

            $service = $this->unlinkFromWorkorder($service);

            $service->setProposal(null);
            $service->setDirectlyWO(null);
            $service->setAddedFromIos(false);

            $workorder->removeService($service);
        }
    }

    /**
     * @param Service $service
     * @return Service
     */
    public function unlinkFromWorkorder(Service $service)
    {
        $service = $this->resetWorkorder($service);

        return $service;
    }

    /**
     * @param Service $service
     * @return Service
     */
    private function resetWorkorder(Service $service)
    {
        $service->setWorkorder(null);
        $service->setLastReport();

        return $service;
    }
}
