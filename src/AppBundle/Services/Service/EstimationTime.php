<?php

namespace AppBundle\Services\Service;

use AppBundle\Entity\Proposal;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\RepairDeviceInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;

class EstimationTime
{
    /** @var ObjectManager $objectManager */
    private $objectManager;
    /** @var  ContractorServiceRepository */
    private $contractorServiceRepository;
    /** @var RepairDeviceInfo */
    private $repairDeviceInfoRepository;
    /** @var ContainerInterface */
    private $container;
    /** @var ServiceRepository */
    private $serviceRepository;

    /**
     * EstimationTime constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->container = $container;

        $this->proposalManager = $this->container->get('app.proposal.manager');
        $this->objectManager = $objectManager;
        $this->contractorServiceRepository = $this->objectManager->getRepository('AppBundle:ContractorService');
        $this->repairDeviceInfoRepository = $this->objectManager->getRepository('AppBundle:RepairDeviceInfo');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
    }


    /**
     * @param Proposal $proposal
     * @return float|int
     */
    private function calcTotalRetest(Proposal $proposal)
    {

        $services = $this->serviceRepository->findBy(['proposal' => $proposal, 'deleted' => false]);

        $inspectionsEstimatedTime = 0;
        /** @var Service $service */
        foreach ($services as $service) {
            if ($service->getContractorService()) {
                $inspectionsEstimatedTime += $service->getContractorService()->getEstimationTime();
            }
        }

        return $inspectionsEstimatedTime;
    }

    /**
     * @param Proposal $proposal
     */
    public function saveNewTotalEstimationTime(Proposal $proposal)
    {
        $totalEstimationTime = $this->calcTotalRetest($proposal);

        $proposal->setTotalEstimationTime($totalEstimationTime);
        $this->proposalManager->save($proposal);
    }
}
