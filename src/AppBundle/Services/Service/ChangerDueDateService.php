<?php

namespace AppBundle\Services\Service;

use AppBundle\Entity\Account;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\ClientTypeRepository;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ChangerDueDateService
{
    /** @var ContainerInterface */
    private $container;
    /** @var ObjectManager */
    private $objectManager;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ClientTypeRepository */
    private $clientTypeRepository;
    /** @var AccountManager */
    private $accountManager;

    /**
     * ChangeDueDateService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->serviceNamedRepository = $this->objectManager->getRepository('AppBundle:ServiceNamed');
        $this->serviceManager = $this->container->get('app.service.manager');
        $this->clientTypeRepository = $this->objectManager->getRepository('AppBundle:ClientType');
        $this->accountManager = $this->container->get('app.account.manager');
    }

    /**
     * @param array $data
     * @return array
     */
    public function changeInspectionDueDate($data)
    {
        $serviceName = $data['named'] ?? $this->serviceNamedRepository->findOneBy(
            ['name' => 'Annual Backflow Inspection']
        );
        /** @var \DateTime $yearTo */
        $yearTo = \DateTime::createFromFormat('Y', $data['yearTo']);
        $inspections = $this->serviceRepository->getInspectionsByOldDueDateAndServiceNamed(
            $serviceName,
            $data['months'],
            $data['year'],
            $yearTo
        );
        $allCount = $this->serviceRepository->getCountInspectionsByOldDueDateAndServiceNamed(
            $serviceName,
            $data['months'],
            $data['year'],
            $yearTo
        );

        /** @var Service $inspection */
        foreach ($inspections as $inspection) {
            $account = $inspection->getAccount();

            $this->serviceManager->resetOpportunity($inspection);

            $this->changingInspection($inspection, $yearTo);

            $this->serviceManager->attachOpportunity($inspection);

            $this->setAccountStatus($account);
        }

        return ['done' => count($inspections), 'allCount' => $allCount];
    }

    /**
     * @param Service $inspection
     * @param \DateTime $dateTo
     */
    private function changingInspection(Service $inspection, \DateTime $dateTo)
    {
        $range = (integer) $dateTo->format('Y') - (integer) $inspection->getInspectionDue()->format('Y');
        $inspectionDue = clone $inspection->getInspectionDue()->modify("+{$range} years");
        $inspection->setInspectionDue($inspectionDue);
    }

    /**
     * @param Account $account
     */
    private function setAccountStatus(Account $account)
    {
        if ($account->getClientType()->getAlias() != 'inactive') {
            $clientType = $this->clientTypeRepository->findOneBy(['alias' => 'inactive']);
            $account->setClientType($clientType);

            $this->accountManager->update($account);
        }
    }

}