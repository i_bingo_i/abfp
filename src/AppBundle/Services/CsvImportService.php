<?php

namespace AppBundle\Services;

use AppBundle\Entity\Address;
use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\ContractorUserHistory;
use AppBundle\Entity\Equipment;
use AppBundle\Entity\Licenses;
use AppBundle\Entity\LicensesDynamicFieldValue;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\EquipmentRepository;
use AppBundle\Entity\Repository\EquipmentTypeRepository;
use AppBundle\Entity\Repository\LicensesCategoryRepository;
use AppBundle\Entity\Repository\LicensesDynamicFieldRepository;
use AppBundle\Entity\Repository\LicensesNamedRepository;
use AppBundle\Entity\Repository\LicensesRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\State;
use Symfony\Component\DependencyInjection\ContainerInterface;
use League\Csv\Reader;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\Repository\AddressRepository;
use \AppBundle\Entity\Repository\ContractorUserRepository;

class CsvImportService
{
    /** @var ContainerInterface */
    private $container;
    /** @var EntityManager */
    private $objectManager;
    /** @var ContractorRepository */
    private $contractorRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var StateRepository */
    private $stateRepository;
    /** @var AddressRepository */
    private $addressRepository;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;
    /** @var LicensesCategoryRepository */
    private $licensesCategoryRepository;
    /** @var LicensesNamedRepository */
    private $licensesNamedRepository;
    /** @var LicensesRepository */
    private $licensesRepository;
    /** @var LicensesDynamicFieldRepository */
    private $licensesDynamicFieldRepository;
    /** @var EquipmentTypeRepository */
    private $equipmentTypeRepository;
    /** @var EquipmentRepository */
    private $equipmentRepository;
    /** @var array */
    private $csvData;

    const CSV_EXAMPLE_ROW_NO = 3;
    const CSV_USEFUL_DATA_BEGINNING = 4;

    const CONTRACTOR_COMPANY_COLUMN_NO = 1;
    const USER_OF_THE_SYSTEM_COLUMN_NO = 2;
    const USER_FIRST_NAME_COLUMN_NO = 3;
    const USER_LAST_NAME_COLUMN_NO = 4;
    const USER_EMAIL_COLUMN_NO = 10;
    const ADDRESS_STATE_COLUMN_NO = 8;
    const ADDRESS_FIRST_LINE_COLUMN_NO = 6;
    const ADDRESS_CITY_COLUMN_NO = 7;
    const ADDRESS_ZIP_COLUMN_NO = 9;
    const CONTRACTOR_USER_POSITION_COLUMN_NO = 5;
    const CONTRACTOR_USER_PERSONAL_EMAIL_COLUMN_NO = 11;
    const CONTRACTOR_USER_PERSONAL_CELL_COLUMN_NO = 12;
    const CONTRACTOR_USER_WORK_CELL_COLUMN_NO = 13;
    const CONTRACTOR_USER_FAX_COLUMN_NO = 14;
    //licenses
    const BACKFLOW_LICENSE_ID_COLUMN_NO = 19;
    const BACKFLOW_LICENSE_RENEWAL_DATE_COLUMN_NO = 20;
    const NICET_LICENSE_ID_COLUMN_NO = 27;
    const NICET_CERTIFICATION_COLUMN_NO = 28;
    const NICET_LICENSE_RENEWAL_DATE_COLUMN_NO = 29;
    const NAFED_LICENSE_ID_COLUMN_NO = 30;
    const NAFED_LICENSE_RENEWAL_DATE_COLUMN_NO = 31;
    const USER_PLUMBERS_LICENSE_ID_COLUMN_NO = 32;
    const USER_PLUMBERS_LICENSE_RENEWAL_DATE_COLUMN_NO = 33;
    //equipment
    const BACKFLOW_GAUGES_MAKE_COLUMN_NO = 21;
    const BACKFLOW_GAUGES_MODEL_COLUMN_NO = 22;
    const BACKFLOW_GAUGES_SN_COLUMN_NO = 23;
    const BACKFLOW_GAUGES_LAST_CALIBRATED_DATE_COLUMN_NO = 24;
    const BACKFLOW_GAUGES_NEXT_CALIBRATED_DATE_COLUMN_NO = 25;
    const BACKFLOW_GAUGES_COMPANY_SUPPLIED_GAUGE_COLUMN_NO = 26;
    //Microsoft Calendar ID
    const CONTRACTOR_USER_MICROSOFT_CALENDAR_ID = 34;
    //
    const USER_PASSWORD = 35;

    /**
     * CsvImportService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->objectManager = $this->container->get('app.entity_manager');

        $this->contractorRepository = $this->objectManager->getRepository('AppBundle:Contractor');
        $this->userRepository = $this->objectManager->getRepository('AppBundle:User');
        $this->addressTypeRepository = $this->objectManager->getRepository('AppBundle:AddressType');
        $this->stateRepository = $this->objectManager->getRepository('AppBundle:State');
        $this->addressRepository = $this->objectManager->getRepository('AppBundle:Address');
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');

        $this->licensesCategoryRepository = $this->objectManager->getRepository('AppBundle:LicensesCategory');
        $this->licensesNamedRepository = $this->objectManager->getRepository('AppBundle:LicensesNamed');
        $this->licensesRepository = $this->objectManager->getRepository('AppBundle:Licenses');
        $this->licensesDynamicFieldRepository = $this->objectManager->getRepository('AppBundle:LicensesDynamicField');

        $this->equipmentTypeRepository = $this->objectManager->getRepository('AppBundle:EquipmentType');
        $this->equipmentRepository = $this->objectManager->getRepository('AppBundle:Equipment');

    }

    public function parsingData($fileName)
    {
        $reader = Reader::createFromPath("./CsvImport/{$fileName}");
        $csvReadData = $reader->fetchAll();

        $this->csvData = [];
        for ($i = self::CSV_USEFUL_DATA_BEGINNING; $i < count($csvReadData); $i++) {
            $this->csvData[] = $csvReadData[$i];
        }
    }

    public function importDataFromCSVFile()
    {
        $contractors = $this->importContractors();
        $addresses = $this->importAddresses();
        $users = $this->importUsers();
        $contractorsUsers = $this->importContractorsUsers($addresses, $users, $contractors);
        $this->importBackflowLicenses($contractorsUsers);
        $this->importNICETLicenses($contractorsUsers);
        $this->importNAFEDLicenses($contractorsUsers);
        $this->importUserPlumbersLicenses($contractorsUsers);
        $this->importEquipment($contractorsUsers);
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function importContractors()
    {
        //returning value
        $contractors = [];

        //for missing contractors
        $missingContractorsCount = 0;
        $exceptionMassage = "CSV data import exception: the following contractors could not be found in the database of the Project:\n";

        foreach ($this->csvData as $key => $csvRow) {
            $contractorNamePart = substr($csvRow[self::CONTRACTOR_COMPANY_COLUMN_NO], 0, 35);
            if ($contractorNamePart == 'American Backflow & Fire Prevention') {
                $contractor = $this->contractorRepository->findOneBy(['name' => $contractorNamePart]);
            } else {
                $contractor = $this->contractorRepository->findOneBy(['name' => $csvRow[self::CONTRACTOR_COMPANY_COLUMN_NO]]);
            }
            if ($contractor == null) {
                $exceptionMassage .= '    '.$csvRow[self::CONTRACTOR_COMPANY_COLUMN_NO]."\n";
                $missingContractorsCount++;
            }
            $contractors[$key] = $contractor;
        }

        if ($missingContractorsCount > 0) {
            throw new \Exception($exceptionMassage);
        }

        return $contractors;
    }

    /**
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function importUsers()
    {
        $users = [];
        $passwords = [
            'catherined@backflowandfire.com' => 'Firecat210',
            'katiec@backflowandfire.com' => 'Chall@4511',
            'mandies@backflowandfire.com' => 'Rebecca#',
            'stephanieh@backflowandfire.com' => 'Lainey33',
            'catherinec@backflowandfire.com' => 'Meercat11',
            'christineq@backflowandfire.com' => '815Oakwood',
            'jennifers@backflowandfire.com' => 'Sommertime1',
            'danh@backflowandfire.com' => 'ABFPprez1',
            'davidl@backflowandfire.com' => 'Skidoo!200',
            'erikg@backflowandfire.com' => 'Bluesprings1!',
            'joshuaq@backflowandfire.com' => 'Westside58',
            'chuckt@backflowandfire.com' => 'Sprinkler1',
            'lukem@backflowandfire.com' => 'Dolton@509',
            'joes@backflowandfire.com' => 'BuddyLeo151',
            'logans@backflowandfire.com' => 'Sorenson1!',
            'stevere@backflowandfire.com' => 'Wedding18',
            'jenm@backflowandfire.com' => 'ABPqueen1',
            'briand@backflowandfire.com' => 'Dalejr1988',
            'leea@backflowandfire.com' => 'Darkknight1992',
            'joeh@backflowandfire.com' => 'Boiler@Vent',
            'dank@backflowandfire.com' => 'Asterct985',
            'Chrisw@backflowandfire.com' => 'Backflow2750',
        ];
        foreach ($this->csvData as $key => $csvRow) {
            if ($csvRow[self::USER_OF_THE_SYSTEM_COLUMN_NO] == 'Yes') {
                $user = $this->userRepository->findOneBy(
                    [
                        'firstName' => $csvRow[self::USER_FIRST_NAME_COLUMN_NO],
                        'lastName' => $csvRow[self::USER_LAST_NAME_COLUMN_NO],
                        'email' => $csvRow[self::USER_EMAIL_COLUMN_NO],

                    ]
                );
                if ($user == null) {
                    $accessTokenService = $this->container->get('api.user_access_token');
                    $user = new User();
                    $user->setFirstName($csvRow[self::USER_FIRST_NAME_COLUMN_NO]);
                    $user->setLastName($csvRow[self::USER_LAST_NAME_COLUMN_NO]);
                    $user->setEmail($csvRow[self::USER_EMAIL_COLUMN_NO]);
                    $user->addRole('ROLE_SUPER_ADMIN');
//                    if (array_key_exists($csvRow[self::USER_EMAIL_COLUMN_NO], $passwords)) {
//                        $user->setPlainPassword($passwords[$csvRow[self::USER_EMAIL_COLUMN_NO]]);
//                    }
                    $user->setPlainPassword($csvRow[self::USER_PASSWORD]);
                    $accessToken = $accessTokenService->createUserAccessToken($user);
                    $user->setAccessToken($accessToken);
                    $user->setEnabled(true);
                    $this->objectManager->persist($user);
                    $this->objectManager->flush();
                }
                $users[$key] = $user;
            }
        }

        return $users;
    }

    /**
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function importAddresses()
    {
        $addresses = [];

        $addressType = $this->addressTypeRepository->findOneBy(
            [
                'alias' => 'contractor address',
            ]
        );
        foreach ($this->csvData as $key => $csvRow) {
            $state = $this->stateRepository->findOneBy(
                [
                    'code' => $csvRow[self::ADDRESS_STATE_COLUMN_NO],
                ]
            );
            if ($state == null) {
                if (!empty($csvRow[self::ADDRESS_STATE_COLUMN_NO])) {
                    $state = new State();
                    $state->setCode($csvRow[self::ADDRESS_STATE_COLUMN_NO]);
                    $this->objectManager->persist($state);
                    $this->objectManager->flush();
                }
            }
            if (
                empty($csvRow[self::ADDRESS_FIRST_LINE_COLUMN_NO])
                && empty($csvRow[self::ADDRESS_CITY_COLUMN_NO])
                && empty($csvRow[self::ADDRESS_ZIP_COLUMN_NO])
                && empty($csvRow[self::ADDRESS_STATE_COLUMN_NO])
            ) {
                continue;
            }
            $address = new Address();
            $address->setAddress($csvRow[self::ADDRESS_FIRST_LINE_COLUMN_NO]);
            $address->setCity($csvRow[self::ADDRESS_CITY_COLUMN_NO]);
            $address->setZip($csvRow[self::ADDRESS_ZIP_COLUMN_NO]);
            $address->setState($state);
            $address->setAddressType($addressType);
            $this->objectManager->persist($address);
            $this->objectManager->flush();
            $addresses[$key] = $address;
        }

        return $addresses;
    }

    /**
     * @param array $addresses
     * @param array $users
     * @param array $contractors
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function importContractorsUsers(array $addresses, array $users, array $contractors)
    {
        $contractorsUsers = [];

        foreach ($this->csvData as $key => $csvRow) {
            //criteria to find one ContractorUser by
            $criteria = [
                'title' => $csvRow[self::CONTRACTOR_USER_POSITION_COLUMN_NO],
                'personalEmail' => $csvRow[self::CONTRACTOR_USER_PERSONAL_EMAIL_COLUMN_NO],
                'personalPhone' => $csvRow[self::CONTRACTOR_USER_PERSONAL_CELL_COLUMN_NO],
                'workPhone' => $csvRow[self::CONTRACTOR_USER_WORK_CELL_COLUMN_NO],
                'fax' => $csvRow[self::CONTRACTOR_USER_FAX_COLUMN_NO],
            ];
            if (!empty($addresses[$key])) {
                $criteria['address'] = $addresses[$key];
            }
            if (!empty($users[$key])) {
                $criteria['user'] = $users[$key];
            }
            if (!empty($contractors[$key])) {
                $criteria['contractor'] = $contractors[$key];
            }
            //find one ContractorUser by criteria
            $contractorUser = $this->contractorUserRepository->findOneBy($criteria);
            if ($contractorUser == null) {
                $contractorUser = new ContractorUser();
                $contractorUser->setTitle($csvRow[self::CONTRACTOR_USER_POSITION_COLUMN_NO]);
                $contractorUser->setPersonalEmail($csvRow[self::CONTRACTOR_USER_PERSONAL_EMAIL_COLUMN_NO]);
                $contractorUser->setPersonalPhone($csvRow[self::CONTRACTOR_USER_PERSONAL_CELL_COLUMN_NO]);
                $contractorUser->setWorkPhone($csvRow[self::CONTRACTOR_USER_WORK_CELL_COLUMN_NO]);
                $contractorUser->setFax($csvRow[self::CONTRACTOR_USER_FAX_COLUMN_NO]);
                $contractorUser->setMsCalendarId($csvRow[self::CONTRACTOR_USER_MICROSOFT_CALENDAR_ID]);
                if (!empty($users[$key])) {
                    $contractorUser->setUser($users[$key]);
                }
                if (!empty($addresses[$key])) {
                    $contractorUser->setAddress($addresses[$key]);
                }
                if (!empty($contractors[$key])) {
                    $contractorUser->setContractor($contractors[$key]);
                }
                $this->objectManager->persist($contractorUser);

                $contractorUserHistory = new ContractorUserHistory();
                $contractorUserHistory->setContractor($contractorUser->getContractor());
                $contractorUserHistory->setUser($contractorUser->getUser());
                $contractorUserHistory->setOwnerEntity($contractorUser);
                $contractorUserHistory->setDateSave(new \DateTime());
                $this->objectManager->persist($contractorUserHistory);

                $this->objectManager->flush();
            }
            $contractorsUsers[$key] = $contractorUser;
        }

        return $contractorsUsers;
    }

    /**
     * @param array $contractorsUsers
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function importBackflowLicenses(array $contractorsUsers)
    {
        $licenses = [];

        foreach ($this->csvData as $key => $csvRow) {
            $backflowLicensesNamed = $this->licensesNamedRepository->findOneBy(
                [
                    'alias' => 'backflow license',
                ]
            );
            $backflowLicenseRenewalDate = (!empty($csvRow[self::BACKFLOW_LICENSE_RENEWAL_DATE_COLUMN_NO])) ?
                \DateTime::createFromFormat('m/d/Y', $csvRow[self::BACKFLOW_LICENSE_RENEWAL_DATE_COLUMN_NO])
                : null;
            $backflowLicenseState = $this->stateRepository->findOneBy(
                [
                    'code' => $csvRow[self::ADDRESS_STATE_COLUMN_NO],
                ]
            );
            $backflowLicense = $this->licensesRepository->findOneBy(
                [
                    'code' => $csvRow[self::BACKFLOW_LICENSE_ID_COLUMN_NO],
                    'renewalDate' => $backflowLicenseRenewalDate,
                    'named' => $backflowLicensesNamed,
                    'state' => $backflowLicenseState,
                    'contractorUser' => $contractorsUsers[$key],
                    'deleted' => false,
                ]
            );
            if (
                empty($csvRow[self::BACKFLOW_LICENSE_ID_COLUMN_NO])
                && empty($csvRow[self::BACKFLOW_LICENSE_RENEWAL_DATE_COLUMN_NO])
            ) {
                continue;
            }
            if ($backflowLicense == null) {
                $backflowLicense = new Licenses();
                $backflowLicense->setCode($csvRow[self::BACKFLOW_LICENSE_ID_COLUMN_NO]);
                $backflowLicense->setRenewalDate($backflowLicenseRenewalDate);
                $backflowLicense->setNamed($backflowLicensesNamed);
                $backflowLicense->setState($backflowLicenseState);
                $backflowLicense->setContractorUser($contractorsUsers[$key]);
                $this->objectManager->persist($backflowLicense);
                $this->objectManager->flush();
            }
            $licenses['backflow'][$key] = $backflowLicense;
        }

        return $licenses;
    }

    /**
     * @param array $contractorsUsers
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function importNICETLicenses(array $contractorsUsers)
    {
        $licenses = [];

        foreach ($this->csvData as $key => $csvRow) {
            $nicetLicensesNamed = $this->licensesNamedRepository->findOneBy(
                [
                    'alias' => 'nicet license',
                ]
            );
            $nicetLicenseRenewalDate = (!empty($csvRow[self::NICET_LICENSE_RENEWAL_DATE_COLUMN_NO])) ?
                \DateTime::createFromFormat('m/d/Y', $csvRow[self::NICET_LICENSE_RENEWAL_DATE_COLUMN_NO])
                : null;
            $nicetLicenseState = $this->stateRepository->findOneBy(
                [
                    'code' => $csvRow[self::ADDRESS_STATE_COLUMN_NO],
                ]
            );
            $nicetLicense = $this->licensesRepository->findOneBy(
                [
                    'code' => $csvRow[self::NICET_LICENSE_ID_COLUMN_NO],
                    'renewalDate' => $nicetLicenseRenewalDate,
                    'named' => $nicetLicensesNamed,
                    'state' => $nicetLicenseState,
                    'contractorUser' => $contractorsUsers[$key],
                    'deleted' => false,

                ]
            );
            if (
                empty($csvRow[self::NICET_LICENSE_ID_COLUMN_NO])
                && empty($csvRow[self::NICET_CERTIFICATION_COLUMN_NO])
                && empty($csvRow[self::NICET_LICENSE_RENEWAL_DATE_COLUMN_NO])
            ) {
                continue;
            }
            if ($nicetLicense == null) {
                $nicetLicense = new Licenses();
                $nicetLicense->setCode($csvRow[self::NICET_LICENSE_ID_COLUMN_NO]);
                $nicetLicense->setRenewalDate($nicetLicenseRenewalDate);
                $nicetLicense->setNamed($nicetLicensesNamed);
                $nicetLicense->setState($nicetLicenseState);
                $nicetLicense->setContractorUser($contractorsUsers[$key]);
                $this->objectManager->persist($nicetLicense);
                $this->objectManager->flush();
                if (!empty($csvRow[self::NICET_CERTIFICATION_COLUMN_NO])) {
                    $nicetLicenseDynamicField = $this->licensesDynamicFieldRepository->findOneBy(
                        [
                            'alias' => 'nicet certification',
                        ]
                    );
                    $nicetLicenseDynamicFieldValue = new LicensesDynamicFieldValue();
                    $nicetLicenseDynamicFieldValue->setField($nicetLicenseDynamicField);
                    $nicetLicenseDynamicFieldValue->setLicense($nicetLicense);
                    $nicetLicenseDynamicFieldValue->setValue($csvRow[self::NICET_CERTIFICATION_COLUMN_NO]);
                    $this->objectManager->persist($nicetLicenseDynamicFieldValue);
                    $this->objectManager->flush();
                }
            }
            $licenses['nicet'][$key] = $nicetLicense;
        }

        return $licenses;
    }

    /**
     * @param array $contractorsUsers
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function importNAFEDLicenses(array $contractorsUsers)
    {
        $licenses = [];

        foreach ($this->csvData as $key => $csvRow) {
            $nafedLicensesNamed = $this->licensesNamedRepository->findOneBy(
                [
                    'alias' => 'nafed license',
                ]
            );
            $nafedLicenseRenewalDate = (!empty($csvRow[self::NAFED_LICENSE_RENEWAL_DATE_COLUMN_NO])) ?
                \DateTime::createFromFormat('m/d/Y', $csvRow[self::NAFED_LICENSE_RENEWAL_DATE_COLUMN_NO])
                : null;
            $nafedLicenseState = $this->stateRepository->findOneBy(
                [
                    'code' => $csvRow[self::ADDRESS_STATE_COLUMN_NO],
                ]
            );
            $nafedLicense = $this->licensesRepository->findOneBy(
                [
                    'code' => $csvRow[self::NAFED_LICENSE_ID_COLUMN_NO],
                    'renewalDate' => $nafedLicenseRenewalDate,
                    'named' => $nafedLicensesNamed,
                    'state' => $nafedLicenseState,
                    'contractorUser' => $contractorsUsers[$key],
                    'deleted' => false,
                ]
            );
            if (
                empty($csvRow[self::NAFED_LICENSE_ID_COLUMN_NO])
                && empty($csvRow[self::NAFED_LICENSE_RENEWAL_DATE_COLUMN_NO])
            ) {
                continue;
            }
            if ($nafedLicense == null) {
                $nafedLicense = new Licenses();
                $nafedLicense->setCode($csvRow[self::NAFED_LICENSE_ID_COLUMN_NO]);
                $nafedLicense->setRenewalDate($nafedLicenseRenewalDate);
                $nafedLicense->setNamed($nafedLicensesNamed);
                $nafedLicense->setState($nafedLicenseState);
                $nafedLicense->setContractorUser($contractorsUsers[$key]);
                $this->objectManager->persist($nafedLicense);
                $this->objectManager->flush();
            }
            $licenses['nafed'][$key] = $nafedLicense;
        }

        return $licenses;
    }

    /**
     * @param array $contractorsUsers
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function importUserPlumbersLicenses(array $contractorsUsers)
    {
        $licenses = [];

        foreach ($this->csvData as $key => $csvRow) {
            $userPlumbersLicensesNamed = $this->licensesNamedRepository->findOneBy(
                [
                    'alias' => 'plumbers license',
                ]
            );
            $userPlumbersLicenseRenewalDate = (!empty($csvRow[self::USER_PLUMBERS_LICENSE_RENEWAL_DATE_COLUMN_NO])) ?
                \DateTime::createFromFormat('m/d/Y', $csvRow[self::USER_PLUMBERS_LICENSE_RENEWAL_DATE_COLUMN_NO])
                : null;
            $userPlumbersLicenseState = $this->stateRepository->findOneBy(
                [
                    'code' => $csvRow[self::ADDRESS_STATE_COLUMN_NO],
                ]
            );
            $userPlumbersLicense = $this->licensesRepository->findOneBy(
                [
                    'code' => $csvRow[self::USER_PLUMBERS_LICENSE_ID_COLUMN_NO],
                    'renewalDate' => $userPlumbersLicenseRenewalDate,
                    'named' => $userPlumbersLicensesNamed,
                    'state' => $userPlumbersLicenseState,
                    'contractorUser' => $contractorsUsers[$key],
                    'deleted' => false,
                ]
            );
            if (
                empty($csvRow[self::USER_PLUMBERS_LICENSE_ID_COLUMN_NO])
                && empty($csvRow[self::USER_PLUMBERS_LICENSE_RENEWAL_DATE_COLUMN_NO])
            ) {
                continue;
            }
            if ($userPlumbersLicense == null) {
                $userPlumbersLicense = new Licenses();
                $userPlumbersLicense->setCode($csvRow[self::USER_PLUMBERS_LICENSE_ID_COLUMN_NO]);
                $userPlumbersLicense->setRenewalDate($userPlumbersLicenseRenewalDate);
                $userPlumbersLicense->setNamed($userPlumbersLicensesNamed);
                $userPlumbersLicense->setState($userPlumbersLicenseState);
                $userPlumbersLicense->setContractorUser($contractorsUsers[$key]);
                $this->objectManager->persist($userPlumbersLicense);
                $this->objectManager->flush();
            }
            $licenses['user_plumbers'] = $userPlumbersLicense;
        }

        return $licenses;
    }

    /**
     * @param array $contractorsUsers
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function importEquipment(array $contractorsUsers)
    {
        $equipment = [];

        foreach ($this->csvData as $key => $csvRow) {
            $equipmentTypeItem = $this->equipmentTypeRepository->findOneBy(
                [
                    'alias' => 'backflow equipment',
                ]
            );
            $equipmentLastCalibratedDate = (!empty($csvRow[self::BACKFLOW_GAUGES_LAST_CALIBRATED_DATE_COLUMN_NO])) ?
                \DateTime::createFromFormat('m/d/Y', $csvRow[self::BACKFLOW_GAUGES_LAST_CALIBRATED_DATE_COLUMN_NO])
                : null;
            $equipmentNextCalibratedDate = (!empty($csvRow[self::BACKFLOW_GAUGES_NEXT_CALIBRATED_DATE_COLUMN_NO]))
                ? \DateTime::createFromFormat('m/d/Y', $csvRow[self::BACKFLOW_GAUGES_NEXT_CALIBRATED_DATE_COLUMN_NO])
                : null;
            $companySuppliedGauge = ($csvRow[self::BACKFLOW_GAUGES_COMPANY_SUPPLIED_GAUGE_COLUMN_NO] == 'Yes')
                ? true
                : false;
            $equipmentItem = $this->equipmentRepository->findOneBy(
                [
                    'make' => $csvRow[self::BACKFLOW_GAUGES_MAKE_COLUMN_NO],
                    'model' => $csvRow[self::BACKFLOW_GAUGES_MODEL_COLUMN_NO],
                    'serialNumber' => $csvRow[self::BACKFLOW_GAUGES_SN_COLUMN_NO],
                    'lastCalibratedDate' => $equipmentLastCalibratedDate,
                    'nextCalibratedDate' => $equipmentNextCalibratedDate,
                    'companySuppliedGauge' => $companySuppliedGauge,
                    'type' => $equipmentTypeItem,
                    'contractorUser' => $contractorsUsers[$key],
                ]
            );
            if (
                empty($csvRow[self::BACKFLOW_GAUGES_MAKE_COLUMN_NO])
                && empty($csvRow[self::BACKFLOW_GAUGES_MODEL_COLUMN_NO])
                && empty($csvRow[self::BACKFLOW_GAUGES_SN_COLUMN_NO])
                && empty($csvRow[self::BACKFLOW_GAUGES_LAST_CALIBRATED_DATE_COLUMN_NO])
                && empty($csvRow[self::BACKFLOW_GAUGES_NEXT_CALIBRATED_DATE_COLUMN_NO])
                && empty($csvRow[self::BACKFLOW_GAUGES_COMPANY_SUPPLIED_GAUGE_COLUMN_NO])
            ) {
                continue;
            }
            if ($equipmentItem == null) {
                $equipmentItem = new Equipment();
                $equipmentItem->setMake($csvRow[self::BACKFLOW_GAUGES_MAKE_COLUMN_NO]);
                $equipmentItem->setModel($csvRow[self::BACKFLOW_GAUGES_MODEL_COLUMN_NO]);
                $equipmentItem->setSerialNumber($csvRow[self::BACKFLOW_GAUGES_SN_COLUMN_NO]);
                $equipmentItem->setLastCalibratedDate($equipmentLastCalibratedDate);
                $equipmentItem->setNextCalibratedDate($equipmentNextCalibratedDate);
                $equipmentItem->setCompanySuppliedGauge($companySuppliedGauge);
                $equipmentItem->setType($equipmentTypeItem);
                $equipmentItem->setContractorUser($contractorsUsers[$key]);
                $this->objectManager->persist($equipmentItem);
                $this->objectManager->flush();
            }
            $equipment[$key] = $equipmentItem;
        }

        return $equipment;
    }
}
