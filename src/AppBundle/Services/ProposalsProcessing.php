<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 9/28/17
 * Time: 13:51
 */

namespace AppBundle\Services;

use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\ContractorUserHistory;
use AppBundle\Entity\EntityManager\LetterManager;
use AppBundle\Entity\EntityManager\ProcessingStatsManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\ProcessingStats;
use AppBundle\Entity\ProcessingStatsType;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Repository\ProposalRepository;
use AppBundle\Services\Proposal\Freeze;
use AppBundle\Twig\AppExtension;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ProposalStatus;
use AppBundle\Entity\Repository\ProposalStatusRepository;
use AppBundle\Entity\EntityManager\ProposalLogManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProposalsProcessing
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var  ProposalRepository */
    private $proposalRepository;
    /** @var ProposalStatusRepository  */
    private $proposalStatusRepository;
    /** @var  ProposalLogManager */
    private $proposalLogManager;
    /** @var  ProposalManager */
    private $proposalManager;
    /** @var Freeze */
    private $freezeProposalService;
    /** @var  ProcessingStatsManager */
    private $processingStatsManager;
    /** @var LetterManager */
    private $letterManager;
    /** @var ContainerInterface */
    private $container;
    /** @var AppExtension */
    private $appTwigExtention;

    private const STATUS_CALL_NEEDED = 'call_needed';
    private const STATUS_SEND_EMAIL_REMINDER = 'send_email_reminder';
    private const STATUS_PAST_DUE = 'past_due';
    private const STATUS_NO_RESPONSE = 'no_response';
    private const STATUS_SEND_PAST_DUE_EMAIL = 'send_past_due_email';
    private const PROPOSAL_REPAIR_TYPE = 'repair';
    private const PROPOSAL_RETEST_TYPE = 'retest';

    /**
     * ProposalsProcessing constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     * @param AppExtension $appTwigExtention
     */
    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container,
        AppExtension $appTwigExtention
    ) {
        $this->objectManager = $objectManager;
        $this->container = $container;
        $this->appTwigExtention = $appTwigExtention;

        $this->proposalLogManager = $this->container->get('app.proposal_log.manager');
        $this->proposalManager = $this->container->get('app.proposal.manager');
        $this->processingStatsManager = $this->container->get('app.processing_stats.manager');
        $this->proposalRepository = $objectManager->getRepository("AppBundle:Proposal");
        $this->proposalStatusRepository = $objectManager->getRepository("AppBundle:ProposalStatus");
        $this->letterManager = $this->container->get('app.letter.manager');
        $this->freezeProposalService = $this->container->get('app.proposal.freeze.service');
    }

    /**
     * @return bool
     * Initiated by Midnight Check (call needed action)
     * -10 days from first sending date (or +20 days form earliest due date) for RETEST NOTICES
     */
    public function checkRetestNoticesForCallNeeding()
    {
        $retestProposals = $this->proposalRepository->getRetestProposalsWhichNeedCall(new \DateTime());

        $proposalStatusNeedCall = $this->proposalStatusRepository->findOneBy(["alias" => self::STATUS_CALL_NEEDED]);
        if (!$this->checkingForExistingCallNeededStatus($proposalStatusNeedCall)) {
            return false;
        }

        $ifWroteMessageOnEmptyRetestProposals =
            $this->writeMessageOnTheEmptyProposalsForNeedCallAction($retestProposals, self::PROPOSAL_RETEST_TYPE);

        if ($ifWroteMessageOnEmptyRetestProposals) {
            return false;
        }

        $successRetestProposals = $this->automaticActionForCallNeeded($retestProposals, $proposalStatusNeedCall);

        $this->writeMessageOnTheSuccessProposalsForNeedCallAction(
            $retestProposals,
            $successRetestProposals,
            self::PROPOSAL_RETEST_TYPE
        );

        return true;
    }

    /**
     * @return bool
     * Initiated by Midnight Check (call needed action)
     * -5 days from first sending date for ALL PROPOSALS
     */
    public function checkProposalsForCallNeeding()
    {
        $typesAlias = ["custom", "repair"];
        $proposals = $this->proposalRepository->getProposalsWhichNeedCall(new \DateTime(), $typesAlias);

        $proposalStatusNeedCall = $this->proposalStatusRepository->findOneBy(["alias" => self::STATUS_CALL_NEEDED]);
        if (!$this->checkingForExistingCallNeededStatus($proposalStatusNeedCall)) {
            return false;
        }

        $ifWroteMessageOnEmptyProposals =
            $this->writeMessageOnTheEmptyProposalsForNeedCallAction($proposals, self::PROPOSAL_REPAIR_TYPE);

        if ($ifWroteMessageOnEmptyProposals) {
            return false;
        }

        $successProposals = $this->automaticActionForCallNeeded($proposals, $proposalStatusNeedCall);

        $this->writeMessageOnTheSuccessProposalsForNeedCallAction(
            $proposals,
            $successProposals,
            self::PROPOSAL_REPAIR_TYPE
        );

        return true;
    }

    /**
     * Initiated by Midnight Check - 10 days before the Retest Notice Earliest Due Date
     */
    public function sendEmailReminder()
    {
        $today = new \DateTime();
        $status = $this->proposalStatusRepository->findOneBy(['alias' => self::STATUS_SEND_EMAIL_REMINDER]);
        $proposals = $this->proposalRepository
            ->getTenDaysBeforeRetestNoticeEarliestDueDate($today->modify('+10 days'), $status);

        if (!empty($proposals)) {
            $success = 0;
            /** @var Proposal $proposal */
            foreach ($proposals as $proposal) {
                $this->startAutomaticProposalActions(
                    $proposal,
                    self::STATUS_SEND_EMAIL_REMINDER,
                    'Send email reminder!'
                );

                $success++;
            }

            $this->processingStatsManager->createMessageByAlias(
                "Success finish in Send Email Reminder check. 
                Count of proposals: ".count($proposals).". Success processing: ".$success
            );
        } else {
            $this->processingStatsManager->createMessageByAlias("Nothing to do in Send Email Reminder check. 
            No Proposals for processing.");
        }
    }

    /**
     * Initiated by Midnight Check - next day after the Retest Notice Earliest Due Date
     */
    public function pastDue()
    {
        $today = new \DateTime();
        $status = $this->proposalStatusRepository->findOneBy(['alias' => self::STATUS_PAST_DUE]);
        $limitDate = $today->modify('-9 day');
        $proposals = $this->proposalRepository
            ->getNextSomeDaysAfterRetestNoticeEarliestDueDate($limitDate, $status);

        if (!empty($proposals)) {
            $success = 0;
            /** @var Proposal $proposal */
            foreach ($proposals as $proposal) {
                $this->startAutomaticProposalActions($proposal, self::STATUS_PAST_DUE, 'EARLIEST DUE DATE HAS PAST!');

                $success++;
            }
            $this->processingStatsManager->createMessageByAlias(
                "Success finish in Past Due check. Count of proposals: ".count($proposals).". 
                Success processing: ".$success
            );
        } else {
            $this->processingStatsManager->createMessageByAlias("Nothing to do in Past Due check. 
            No Proposals for processing.");
        }
    }

    /**
     * Initiated by Midnight Check - 10 days after the Retest Notice Earliest Due Date
     */
    public function noResponse()
    {
        $today = new \DateTime();
        $status = $this->proposalStatusRepository->findOneBy(['alias' => self::STATUS_NO_RESPONSE]);
        $limitDate = $today->modify('-10 day');
        $proposals = $this->proposalRepository
            ->getNextSomeDaysAfterRetestNoticeEarliestDueDate($limitDate, $status);

        if (!empty($proposals)) {
            $success = 0;
            /** @var Proposal $proposal */
            foreach ($proposals as $proposal) {
                $this->startAutomaticProposalActions($proposal, self::STATUS_NO_RESPONSE, 'No response from client');

                $success++;
            }
            $this->processingStatsManager->createMessageByAlias(
                "Success finish in No Response check. Count of proposals: ".count($proposals) .
                ". Success processing: ".$success
            );
        } else {
            $this->processingStatsManager
                ->createMessageByAlias("Nothing to do in No Response check. No Proposals for processing.");
        }
    }

    /**
     * Initiated by Midnight Check - 5 days after the Retest Notice Earliest Due Date
     */
    public function sendEmailPastDue()
    {
        $today = new \DateTime();
        /** @var ProposalStatus $status */
        $status = $this->proposalStatusRepository->findOneBy(['alias' => self::STATUS_SEND_PAST_DUE_EMAIL]);
        $limitDate = $today->modify('-5 day');
        $proposals = $this->proposalRepository
            ->getNextSomeDaysAfterRetestNoticeEarliestDueDate($limitDate, $status);

        if (!empty($proposals)) {
            $success = 0;
            /** @var Proposal $proposal */
            foreach ($proposals as $proposal) {
                $emailName = 'past_due_' . $proposal->getDivision()->getAlias();
                $letter = $this->letterManager->prepareLetter($proposal, $status->getAlias());

                if ($proposal->getDivision()->getAlias() == 'fire') {
                    $subject = 'Past Due Notification of Required Fire Safety Inspections';
                } elseif ($proposal->getDivision()->getAlias() == 'alarm') {
                    $subject = 'Past Due Notification of Required Fire Safety Inspections';
                } elseif ($proposal->getDivision()->getAlias() == 'plumbing') {
                    $subject = 'Past Due Notification of Required Backflow Testing for - ' .
                        $this->appTwigExtention->addressFilter($proposal->getAccount()->getAddress());
                } else {
                    $subject = 'Past Due Notification of Required Backflow Testing for - ' .
                        $this->appTwigExtention->addressFilter($proposal->getAccount()->getAddress());
                }

                $letter->setSubject($subject);

                $letter->setBody($this->container->get('templating')
                    ->render('@Admin/Letter/systemEmails/' . $emailName . '.html.twig', compact('proposal')));

                $letter = $this->letterManager->sendEmail(
                    $letter,
                    'send_past_due_email_retest_notice.html.twig',
                    self::STATUS_SEND_PAST_DUE_EMAIL
                );

                if ($letter) {
                    $this->startAutomaticProposalActions(
                        $proposal,
                        self::STATUS_SEND_PAST_DUE_EMAIL,
                        'Past due email reminder was sent',
                        null,
                        null,
                        $letter
                    );

                    $success++;
                }
            }

            $this->processingStatsManager->createMessageByAlias(
                "Success finish in Send Email Past Due check. Count of proposals: "
                .count($proposals).". Success processing: ".$success
            );
        } else {
            $this->processingStatsManager
                ->createMessageByAlias("Nothing to do in Send Email Past Due check. No Proposals for processing.");
        }
    }

    /**
     * @param Proposal $proposal
     */
    public function checkAutomaticRetestNoticeAction(Proposal $proposal)
    {
        $dateCreate = $proposal->getDateCreate();
        $earliestDueDate = $proposal->getEarliestDueDate();
        $diffDays = $this->diffDays($dateCreate, $earliestDueDate);

        if ($diffDays < 21) {
            $this->startAutomaticProposalActions(
                $proposal,
                self::STATUS_CALL_NEEDED,
                'Call needed!',
                'system'
            );
        }

        if ($diffDays < 11) {
            $this->startAutomaticProposalActions(
                $proposal,
                self::STATUS_SEND_EMAIL_REMINDER,
                'Send email reminder!',
                'system'
            );
        }

        if ($diffDays < 0) {
            $this->startAutomaticProposalActions(
                $proposal,
                self::STATUS_PAST_DUE,
                'Earliest due date has past!',
                'system'
            );

//          if (!$proposal->getIsFrozen()) {
//              $proposal = $this->freezeProposalService->freezing($proposal);
//          }
        }

        if ($diffDays < -10) {
            $this->startAutomaticProposalActions(
                $proposal,
                self::STATUS_NO_RESPONSE,
                'No response from client',
                'system'
            );
        }
    }

    /**
     * @param ProposalStatus $statusCallNeeded
     * @return bool
     */
    private function checkingForExistingCallNeededStatus(ProposalStatus $statusCallNeeded)
    {
        if (!$statusCallNeeded) {
            $this->processingStatsManager->createMessageByAlias(
                "Error during proposals processing in Call Needed check. 
                Message: There is no status with alias '".self::STATUS_CALL_NEEDED."'",
                "error"
            );

            return false;
        }

        return true;
    }

    /**
     * @param array $proposals
     * @return bool
     */
    private function writeMessageOnTheEmptyProposalsForNeedCallAction(array $proposals, $proposalType)
    {
        if (!$proposals) {
            if ($proposalType == self::PROPOSAL_RETEST_TYPE) {
                $message = "Nothing to do in Call Needed check. No Retest Notices for processing.";
            } else {
                $message = "Nothing to do in Call Needed check. No Proposals for processing.";
            }

            $this->processingStatsManager->createMessageByAlias($message);

            return true;
        }

        return false;
    }

    /**
     * @param array $proposals
     * @param $successCount
     */
    private function writeMessageOnTheSuccessProposalsForNeedCallAction(array $proposals, $successCount, $proposalType)
    {
        if ($proposalType == self::PROPOSAL_RETEST_TYPE) {
            $partMessageByProposalType = " Count of retest notices: ";
        } else {
            $partMessageByProposalType = " Count of proposals: ";
        }

        $this->processingStatsManager->createMessageByAlias(
            "Success finish in Call Needed check." .
            $partMessageByProposalType . count($proposals).". Success processing: ".$successCount
        );
    }

    /**
     * @param array $proposals
     * @param $proposalStatusNeedCall
     * @return int
     */
    private function automaticActionForCallNeeded(array $proposals, $proposalStatusNeedCall)
    {
        $success = 0;
        /** @var Proposal $proposal */
        foreach ($proposals as $proposal) {
            $this->startAutomaticProposalActions($proposal, null, 'Call needed!', null, $proposalStatusNeedCall);

            $success++;
        }

        return $success;
    }

    /**
     * @param \DateTime $dateTime
     * @return \DateTime
     */
    private function nullifyTime(\DateTime $dateTime)
    {
        $dateTime->setTime(0, 0, 0, 0);

        return $dateTime;
    }

    /**
     * @param \DateTime $dateOne
     * @param \DateTime $dateTwo
     * @return int
     */
    private function diffDays(\DateTime $dateOne, \DateTime $dateTwo)
    {
        $dateOneWithNullifiedTime = $this->nullifyTime($dateOne);
        $dateTwoWithNullifiedTime = $this->nullifyTime($dateTwo);

        $dateDiff = $dateOneWithNullifiedTime->diff($dateTwoWithNullifiedTime);
        $diffDays = $dateDiff->days;
        if ($dateDiff->invert) {
            $diffDays *= -1;
        }

        return $diffDays;
    }

    /**
     * @param Proposal $proposal
     * @param null $proposalStatusAlias
     * @param $logMessage
     * @param null $author
     * @param ProposalStatus|null $proposalStatus
     * @param null $letter
     */
    private function startAutomaticProposalActions(
        Proposal $proposal,
        $proposalStatusAlias = null,
        $logMessage,
        $author = null,
        ProposalStatus $proposalStatus = null,
        $letter = null
    ) {
        if ($proposalStatus) {
            $proposal->setStatus($proposalStatus);
        } else {
            $this->proposalManager->setStatus($proposal, $proposalStatusAlias);
        }

        $this->proposalManager->update($proposal);
        $this->proposalLogManager->create($proposal, $logMessage, null, $letter, $author);
    }
}
