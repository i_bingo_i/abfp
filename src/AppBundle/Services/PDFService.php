<?php

namespace AppBundle\Services;

use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Workorder;
use AppBundle\Services\AccountContactPerson\Authorizer;
use AppBundle\Services\Event\Compose;
use AppBundle\Services\Tree\Grouping;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class PDFService
{
    const ACTION_CREATE = 'create';
    const ACTION_VIEW = 'view';

    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var array Templates for create PDF */
    private $template = [
        'proposal' => [
            'retest' => [
                'backflow' => 'index-backflow',
                'fire' => 'index-fire',
                'plumbing' => 'index-plumbing',
                'alarm' => 'index-fire'
            ],
            'frozen-retest' => [
                'backflow' => 'index-frozen-backflow',
                'fire' => 'index-frozen-fire',
                'plumbing' => 'index-frozen-plumbing',
                'alarm' => 'index-frozen-fire'
            ],
            'repair' => [
                'backflow' => 'repair-backfllow',
                'fire' => 'repair-fire',
                'plumbing' => 'repair-plumbing',
                'alarm' => 'repair-fire'
            ]
        ],
        'workorder' => [
            'backflow' => 'index-backflow',
            'fire' => 'index-fire',
            'plumbing' => 'index-plumbing',
            'alarm' => 'index-fire'
        ]
    ];

    /** @var ProposalManager */
    private $proposalManager;
    /** @var Grouping $treeService */
    private $treeService;
    /** @var TokenStorage */
    private $tokenStorage;
    /**
     * PDFService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $serviceContainer;
        $this->proposalManager = $this->serviceContainer->get('app.proposal.manager');
        $this->treeService = $this->serviceContainer->get('app.tree.service');
        $this->tokenStorage = $this->serviceContainer->get('security.token_storage');
    }

    // TODO: потребує рефакторингу
    /**
     * @param $action
     * @param array $parameters
     * @return array|bool|mixed
     * @throws \Twig\Error\Error
     */
    public function create($action, $parameters = [])
    {
        $response = '';

        if ($parameters instanceof Workorder) {
            $response = $this->workorderAction($action, $parameters);
        } elseif ($parameters instanceof Proposal) {
            $response = $this->proposalAction($action, $parameters);
        }

        return $response;
    }

    /**
     * @param $action
     * @param Proposal $parameters
     * @return array|mixed
     * @throws \Twig\Error\Error
     */
    public function proposalAction($action, Proposal $parameters)
    {
        $divisionAlias = $parameters->getDivision()->getAlias();
        $proposalTypeAlias = $parameters->getType()->getAlias();
        if ($parameters->getIsFrozen() and $proposalTypeAlias == 'retest') {
            $templateName = $this->template['proposal']['frozen-retest'][$divisionAlias];
        } else {
            $templateName = $this->template['proposal'][$proposalTypeAlias][$divisionAlias];
        }

        if ($action === 'create') {
            return $this->creating(
                $templateName,
                $this->generatingName($parameters),
                $parameters
            );
        } elseif ($action === 'view') {
            return $this->view(
                $templateName,
                $this->generatingName($parameters),
                $parameters
            );
        }
    }

    /**
     * @param $action
     * @param Workorder $parameters
     * @return array
     * @throws \Twig\Error\Error
     */
    public function workorderAction($action, Workorder $parameters)
    {
        $divisionAlias = $parameters->getDivision()->getAlias();
        $templateName = $this->template['workorder'][$divisionAlias];

        if ($action === 'create') {
            return $this->creatingForWorkorder(
                $templateName,
                $this->generatingName($parameters),
                $parameters
            );
        } elseif ($action === 'view') {
            return $this->view(
                $templateName,
                $this->generatingName($parameters),
                $parameters
            );
        }
    }

    // TODO: потребує рефакторингу
    /**
     * @param $template
     * @param $namePDF
     * @param Proposal $proposal
     * @return mixed
     * @throws \Twig\Error\Error
     */
    public function creating($template, $namePDF, Proposal $proposal)
    {
        $snappy = $this->serviceContainer->get('knp_snappy.pdf');
        $templating = $this->serviceContainer->get('templating');
        $header = $templating->render('@Admin/Proposal/pdf/header.html.twig', compact('proposal'));
        $footer = $templating->render('@Admin/Proposal/pdf/footer.html.twig', compact('proposal'));

        if ($proposal->getType()->getAlias() == 'retest') {
            $tree = $this->treeService->getTreeForProposal($proposal);
        } else {
            $tree = $this->treeService->getTreeForProposal($proposal);
        }

        $this->removeProposalReport($namePDF);

        $snappy->generateFromHtml(
            $this->serviceContainer->get('templating')->render(
                "AdminBundle:Proposal/pdf:{$template}.html.twig",
                [
                    'parameters' => $proposal,
                    'tree' => $tree
                ]
            ),
            "/var/www/erp/web/uploads/proposals/{$namePDF}",
            [
                'page-height' => 310,
                'page-width' => 220,
                'margin-left' => 10,
                'margin-right' => 10,
                'margin-bottom' => 30,
                'margin-top' => 20,
                'header-html' => $header,
                'header-spacing' => 5,
                'footer-html' => $footer,
                'footer-spacing' => 5
            ]
        );
        chmod("/var/www/erp/web/uploads/proposals/{$namePDF}", 0777);
        return $namePDF;
    }

    /**
     * @param $template
     * @param $namePDF
     * @param Workorder $workorder
     * @return mixed
     * @throws \Twig\Error\Error
     */
    public function creatingForWorkorder($template, $namePDF, Workorder $workorder)
    {
        /** @var Grouping $treeService */
        $treeService = $this->serviceContainer->get('app.tree.service');
        /** @var Compose $eventCompose */
        $eventCompose = $this->serviceContainer->get('app.event_compose.service');
        /** @var Authorizer $acpAuthorizerService */
        $acpAuthorizerService = $this->serviceContainer->get('app.account_contact_person_authorizer.service');
        $snappy = $this->serviceContainer->get('knp_snappy.pdf');
        $templating = $this->serviceContainer->get('templating');
        $header = $templating->render('@Admin/Workorder/pdf/header.html.twig', compact('workorder'));
        $footer = $templating->render('@Admin/Workorder/pdf/footer.html.twig', compact('workorder'));

        $tree = $treeService->getTreeForWorkorder($workorder);
        $groupingServices = $eventCompose->composeByWorkorder($workorder);
        $accountAuthorizer =  $acpAuthorizerService->getAccountAuthorizerForWorkorder($workorder);

        if (file_exists("/var/www/erp/web/uploads/workorders/{$namePDF}")) {
            unlink("/var/www/erp/web/uploads/workorders/{$namePDF}");
        }
        $snappy->generateFromHtml(
            $this->serviceContainer->get('templating')->render(
                "AdminBundle:Workorder/pdf:{$template}.html.twig",
                [
                    'parameters' => $workorder,
                    'tree' => $tree,
                    'groupingServices' => $groupingServices,
                    'accountAuthorizer' => $accountAuthorizer
                ]
            ),
            "/var/www/erp/web/uploads/workorders/{$namePDF}",
            [
                'page-height' => 310,
                'page-width' => 220,
                'margin-left' => 10,
                'margin-right' => 10,
                'margin-bottom' => 30,
                'margin-top' => 20,
                'header-html' => $header,
                'header-spacing' => 5,
                'footer-html' => $footer,
                'footer-spacing' => 5
            ]
        );
        chmod("/var/www/erp/web/uploads/workorders/{$namePDF}", 0777);
        return $namePDF;
    }

    // TODO: потребує рефакторингу
    /**
     * @param $template
     * @param $namePDF
     * @param Proposal|Workorder $parameters
     * @return array
     * @throws \Twig\Error\Error
     */
    public function view($template, $namePDF, $parameters)
    {
        $snappy = $this->serviceContainer->get('knp_snappy.pdf');
        $tree = [];
        $res = [];
        $groupingServices = [];
        $accountAuthorizer = [];
        $header = '';
        $footer = '';
        $templatesDirectory = '';

        if ($parameters instanceof Proposal) {
            extract($this->proposalPrepareData($parameters), EXTR_OVERWRITE);
        } elseif ($parameters instanceof Workorder) {
            extract($this->workorderPrepareData($parameters), EXTR_OVERWRITE);
        }

        $res['output'] = $snappy->getOutputFromHtml(
            $this->serviceContainer->get('templating')->render(
                "AdminBundle:{$templatesDirectory}:{$template}.html.twig",
                [
                    'parameters' => $parameters,
                    'groupingServices' => $groupingServices,
                    'tree' => $tree,
                    'accountAuthorizer' => $accountAuthorizer
                ]
            ),
            [
                'page-height' => 310,
                'page-width' => 220,
                'margin-left' => 10,
                'margin-right' => 10,
                'margin-bottom' => 30,
                'margin-top' => 20,
                'header-html' => $header,
                'header-spacing' => 5,
                'footer-html' => $footer,
                'footer-spacing' => 5
            ]
        );
        $res['name'] = $namePDF;

        return $res;
    }

    /**
     * @param Workorder|Proposal $objectForPdf
     * @return string
     */
    public function generatingName($objectForPdf)
    {
        $proposalLabel = '';
        $dateOfProposal = (new \DateTime())->format('m-d-Y');
        $referenceId = 0;
        $division = $objectForPdf ? $objectForPdf->getDivision()->getName() : '';

        if ($objectForPdf instanceof Proposal) {
            $proposalLabels = [
                'retest' => 'Retest_Notice',
                'repair' => 'Proposal'
            ];
            $proposalTypeAlias = $objectForPdf->getType()->getAlias();
            $proposalLabel = $proposalLabels[$proposalTypeAlias];
            $dateOfProposal = $objectForPdf->getDateOfProposal()->format('m-d-Y');
            $referenceId = $objectForPdf->getReferenceId();

        } elseif ($objectForPdf instanceof Workorder) {
            $proposalLabel = 'Workorder';
            $dateOfProposal = $objectForPdf->getDateCreate()->format('m-d-Y');
            $referenceId = $objectForPdf->getId();
        }

        return $division . '_' . $proposalLabel . '_' . $dateOfProposal . '_' . 'id' . '_' . $referenceId . '.pdf';
    }

    /**
     * @param $namePDF
     */
    public function removeProposalReport($namePDF)
    {
        if (file_exists("/var/www/erp/web/uploads/proposals/{$namePDF}")) {
            unlink("/var/www/erp/web/uploads/proposals/{$namePDF}");
        }
    }

    /**
     * @param Proposal $proposal
     * @return array
     * @throws \Twig\Error\Error
     */
    private function proposalPrepareData(Proposal $proposal)
    {
        $templating = $this->serviceContainer->get('templating');
        $templatesDirectory = 'Proposal/pdf';
        $header = $templating->render('@Admin/Proposal/pdf/header.html.twig', compact('proposal'));
        $footer = $templating->render('@Admin/Proposal/pdf/footer.html.twig', compact('proposal'));

        $tree = $this->treeService->getTreeForProposal($proposal);

        $groupingServices = $this->proposalManager->groupingServices($proposal);

        return [
            'header' => $header,
            'footer' => $footer,
            'tree' => $tree,
            'groupingServices' => $groupingServices,
            'templatesDirectory' => $templatesDirectory
        ];
    }

    /**
     * @param Workorder $workorder
     * @return array
     * @throws \Twig\Error\Error
     */
    private function workorderPrepareData(Workorder $workorder)
    {
        $templating = $this->serviceContainer->get('templating');
        /** @var Grouping $treeService */
        $treeService = $this->serviceContainer->get('app.tree.service');
        /** @var Authorizer $acpAuthorizerService */
        $acpAuthorizerService = $this->serviceContainer->get('app.account_contact_person_authorizer.service');
        /** @var Compose $eventCompose */
        $eventCompose = $this->serviceContainer->get('app.event_compose.service');

        $templatesDirectory = 'Workorder/pdf';
        $header = $templating->render('@Admin/Workorder/pdf/header.html.twig', compact('workorder'));
        $footer = $templating->render('@Admin/Workorder/pdf/footer.html.twig', compact('workorder'));
        $tree = $treeService->getTreeForWorkorder($workorder);
        $groupingServices = $eventCompose->composeByWorkorder($workorder);
        $accountAuthorizer =  $acpAuthorizerService->getAccountAuthorizerForWorkorder($workorder);

        return [
            'header' => $header,
            'footer' => $footer,
            'tree' => $tree,
            'groupingServices' => $groupingServices,
            'templatesDirectory' => $templatesDirectory,
            'accountAuthorizer' => $accountAuthorizer
        ];
    }
}
