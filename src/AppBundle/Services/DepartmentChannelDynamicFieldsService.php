<?php

namespace AppBundle\Services;

use AppBundle\Entity\AgentChannel;
use AppBundle\Entity\AgentChannelDynamicFieldValue;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DepartmentChannelDynamicFieldValue;
use AppBundle\Services\Address\Creator as AddressCreator;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DepartmentChannelDynamicFieldsService
{
    /** @var ContainerInterface */
    private $container;
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AddressCreator */
    private $addressCreatorService;

    /**
     * DepartmentChannelDynamicFieldsService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->addressCreatorService = $container->get('app.address_creator.service');
    }

    /**
     * @param AgentChannel $agentChannel
     * @param DepartmentChannel $departmentChannel
     */
    public function createByAgentChannel(AgentChannel $agentChannel, DepartmentChannel $departmentChannel)
    {
        if (count($agentChannel->getDynamicFields())) {
            foreach ($agentChannel->getDynamicFields() as $dynamicField) {
                $this->createByAgentChannelDynamicField($dynamicField, $departmentChannel);
            }
        }
    }

    /**
     * @param AgentChannelDynamicFieldValue $agentChannelDynamicFieldValue
     * @param DepartmentChannel $departmentChannel
     */
    private function createByAgentChannelDynamicField(
        AgentChannelDynamicFieldValue $agentChannelDynamicFieldValue,
        DepartmentChannel $departmentChannel
    ) {
        $departmentChannelDynamicFieldValue = new DepartmentChannelDynamicFieldValue();
        $departmentChannelDynamicFieldValue->setValue($agentChannelDynamicFieldValue->getValue());
        $departmentChannelDynamicFieldValue->setField($agentChannelDynamicFieldValue->getField());
        $departmentChannelDynamicFieldValue->setDepartmentChanel($departmentChannel);
        if ($agentChannelDynamicFieldValue->getEntityValue()) {
            $newAddress = $this->addressCreatorService->cloneAddress($agentChannelDynamicFieldValue->getEntityValue());
            $departmentChannelDynamicFieldValue->setEntityValue($newAddress);
        }
        $this->save($departmentChannelDynamicFieldValue);
    }

    /**
     * @param DepartmentChannel $departmentChannel
     */
    public function removeAllFromDepartmentChannel(DepartmentChannel $departmentChannel)
    {
        foreach ($departmentChannel->getFields() as $field) {
            $departmentChannel->removeField($field);
            $this->objectManager->remove($field);
        }

        $this->objectManager->flush();
    }

    /**
     * @param $collection
     */
    private function save($collection)
    {
        $this->objectManager->persist($collection);
        $this->objectManager->flush();
    }
}
