<?php

namespace AppBundle\Services\PdfManagers;

use InvoiceBundle\Entity\Invoices;
use AppBundle\Services\PDFGenerator;
use InvoiceBundle\Repository\InvoiceLineRepository;
use QuickbooksBundle\QBQuery\AddressTrait;
use Symfony\Bridge\Twig\TwigEngine;

class PDFInvoiceManager extends BasePDFManager
{
    use AddressTrait;

    private const PATH_FOR_SAVE = '/uploads/invoices/';
    /** @var InvoiceLineRepository */
    private $lineRepository;

    /**
     * PDFInvoiceManager constructor.
     * @param TwigEngine $templating
     * @param PDFGenerator $pdfGenerator
     * @param $webDirPath
     * @param InvoiceLineRepository $lineRepository
     */
    public function __construct(
        TwigEngine $templating,
        PDFGenerator $pdfGenerator,
        $webDirPath,
        InvoiceLineRepository $lineRepository
    ) {
        parent::__construct($templating, $pdfGenerator, $webDirPath);

        $this->lineRepository = $lineRepository;

        $this->defaultSavePdfPath .= self::PATH_FOR_SAVE;
    }

    /**
     * @param Invoices $entity
     * @return string
     */
    public function getFileName($entity): string
    {
        $workorderId = $entity->getWorkorder()->getId();
        $qbInvoiceNumber = $entity->getAccountingSystemId();

        return "WO{$workorderId}_Invoice_{$qbInvoiceNumber}.pdf";
    }

    /**
     * @param Invoices $entity
     * @param null $assetsPackage
     * @return string
     * @throws \Twig\Error\Error
     */
    public function renderToHtml($entity, $assetsPackage = null): string
    {
        $division = $entity->getWorkorder()->getDivision();
        $invoiceLines = $this->lineRepository->findBy(['invoice' => $entity], ['serialNumber' => 'ASC']);
        $billToAddress = $this->getAddress($entity->getBillToAddress());
        $shipToAddress = $this->getAddress($entity->getShipToAddress());

        return $this->templating->render("AdminBundle:Invoice:pdf/{$division->getAlias()}.html.twig", [
            'invoice'=> $entity,
            'lines' => $invoiceLines,
            'assetsPackage' => $assetsPackage,
            'billToAddress' => $billToAddress,
            'shipToAddress' => $shipToAddress
        ]);
    }

    /**
     * @param Invoices $invoices
     * @return string
     * @throws \Twig\Error\Error
     */
    public function creating(Invoices $invoices)
    {
        return $this->create($invoices);
    }


    /***** This code only for developing invoice pdf *****/

    // TODO Delete this action in future
    public function createInvoice($division)
    {
        $assetsPackage = 'pdf';

        $headerHtml = $this->templating->render("AdminBundle:Invoice/pdf:header.html.twig", [
            'assetsPackage' => $assetsPackage
        ]);

        $footerHtml = $this->templating->render("AdminBundle:Invoice:pdf/footer.html.twig", [
            'assetsPackage' => $assetsPackage
        ]);

        $template = $this->templating->render("AdminBundle:Invoice:pdf/index-{$division}.html.twig", [
            'assetsPackage' => $assetsPackage
        ]);

        return $this->pdfGenerator->make($template, [
            'header-html' => $headerHtml,
            'footer-html' => $footerHtml
        ]);
    }
    /****** END *******/


    /**
     * @param Invoices $entity
     * @return string
     * @throws \Twig\Error\Error
     */
    protected function create($entity): string
    {
        $assetsPackage = 'pdf';

        $headerHtml = $this->templating->render("AdminBundle:Invoice/pdf:header.html.twig", [
            'invoice' => $entity,
            'assetsPackage' => $assetsPackage
        ]);

        $footerHtml = $this->templating->render("AdminBundle:Invoice:pdf/footer.html.twig", [
            'invoice' => $entity,
            'assetsPackage' => $assetsPackage
        ]);

        $template = $this->renderToHtml($entity, $assetsPackage);

        return $this->pdfGenerator->make($template, [
            'header-html' => $headerHtml,
            'footer-html' => $footerHtml
        ]);
    }
}
