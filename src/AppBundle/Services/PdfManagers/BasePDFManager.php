<?php

namespace AppBundle\Services\PdfManagers;

use AppBundle\Services\PDFGenerator;
use Symfony\Bridge\Twig\TwigEngine;

abstract class BasePDFManager
{
    /** @var TwigEngine */
    protected $templating;
    /** @var string */
    protected $defaultSavePdfPath;
    /** @var PDFGenerator */
    protected $pdfGenerator;
    /** @var string */
    protected $webDirPath;

    /**
     * BasePDFManager constructor.
     * @param TwigEngine $templating
     * @param PDFGenerator $pdfGenerator
     * @param string $webDirPath
     */
    public function __construct(TwigEngine $templating, PDFGenerator $pdfGenerator, $webDirPath)
    {
        $this->templating = $templating;
        $this->webDirPath = $webDirPath;
        $this->pdfGenerator = $pdfGenerator;
        $this->defaultSavePdfPath = $this->webDirPath;
    }

    /**
     * Return WO pdf file name
     *
     * @param object $entity
     * @param string|null $savePdfPath
     *
     * @return string
     */
    public function save($entity, $savePdfPath = null)
    {
        $stringPdf = $this->create($entity);

        if (!$savePdfPath) {
            $savePdfPath = $this->defaultSavePdfPath;
        }

        $fileName = $this->getFileName($entity);
        file_put_contents($savePdfPath . $fileName, $stringPdf);

        return $fileName;
    }

    /**
     * @param string $name
     */
    public function delete($name)
    {
        $pdfFile = $this->defaultSavePdfPath . '/' . $name;

        if (file_exists($pdfFile)) {
            unlink($pdfFile);
        }
    }

    /**
     * @param object $entity
     * @return string
     */
    abstract public function getFileName($entity) : string;

    /**
     * @param object $entity
     * @param string|null $assetsPackage
     * @return string
     */
    abstract public function renderToHtml($entity, $assetsPackage = null) : string;

    /**
     * @param object $entity
     * @return string
     */
    abstract protected function create($entity) : string;
}
