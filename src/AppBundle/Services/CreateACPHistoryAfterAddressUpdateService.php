<?php

namespace AppBundle\Services;

use AppBundle\Entity\AddressHistory;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Services\AccountContactPersonHistory\Creator;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CreateACPHistoryAfterAddressUpdateService
{
    /** @var ContainerInterface */
    private $container;
    /** @var  ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var Creator */
    private $accountContactPersonHistoryCreatorService;


    /**
     * CreateACPHistoryAfterAddressUpdateService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->accountContactPersonRepository = $this->objectManager->getRepository("AppBundle:AccountContactPerson");
        $this->accountContactPersonHistoryCreatorService =
            $this->container->get('app.account_contact_person_history_creator.service');
    }

    /**
     * @param AddressHistory $addressHistory
     */
    public function checkAndFindACPForCreateHistory(AddressHistory $addressHistory)
    {
        $accountContactPerson = $this->accountContactPersonRepository->findOneBy([
            "sendingAddress" => $addressHistory->getOwnerEntity()
        ]);

        if ($accountContactPerson) {
            $this->accountContactPersonHistoryCreatorService->create($accountContactPerson);
        }
    }
}
