<?php

namespace AppBundle\Services\AccountContactPerson;

use AppBundle\Creators\AddressCreator;
use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\AccountContactPersonHistory;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\Repository\AccountContactPersonHistoryRepository;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Workorder;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WorkorderBundle\Services\WorkorderService;

class Authorizer
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $serviceContainer;

    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var AccountContactPersonHistoryRepository */
    private $accountContactPersonHistoryRepository;

    /** @var \AppBundle\Services\AccountContactPersonHistory\Creator */
    private $accountContactPersonHistoryCreatorService;
    /** @var WorkorderService */
    private $workorderService;
    /** @var AddressCreator */
    private $addressCreator;

    /**
     * Authorizer constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $serviceContainer;

        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
        $this->accountContactPersonHistoryRepository =
            $this->objectManager->getRepository('AppBundle:AccountContactPersonHistory');

        $this->accountContactPersonHistoryCreatorService =
            $this->serviceContainer->get('app.account_contact_person_history_creator.service');
        $this->workorderService = $this->serviceContainer->get('workorder.workorder_service.service');
        $this->addressCreator = $this->serviceContainer->get('address.creator');
    }

    /**
     * @param Workorder $workorder
     * @return AccountContactPersonHistory|bool|mixed
     */
    public function getAccountAuthorizerForWorkorder(Workorder $workorder)
    {
        if ($workorder->getIsFrozen()) {
            $accountAuthorizer = $workorder->getAccountAuthorizer();

            return $accountAuthorizer ? $this->checkAndGetAuthorizerHistoryForFroze(
                $workorder,
                $accountAuthorizer
            ) : false;
        } else {
            $authorizer = $this->getPrimaryAuthorizersByAccountForDivisions($workorder);

            return array_shift($authorizer);
        }
    }

    /**
     * @param Workorder $workorder
     * @return AccountContactPerson|AccountContactPersonHistory|bool
     */
    public function getAuthorizerForWorkorder(Workorder $workorder)
    {
        $authorizer = $workorder->getAuthorizer();

        if ($workorder->getIsFrozen()) {
            return $authorizer ? $this->checkAndGetAuthorizerHistoryForFroze($workorder, $authorizer) : false;
        }

        return  $authorizer;
    }

    /**
     * @param Workorder $workorder
     * @return array
     */
    public function getPrimaryAuthorizersByAccountForDivisions(Workorder $workorder)
    {
        $authorizer = $this->accountContactPersonRepository->findPrimaryAuthorizersByAccountForDivisions(
            $workorder->getAccount(),
            [$workorder->getDivision()->getId()]
        );

        return $authorizer;
    }

    /**
     * @param Workorder $workorder
     * @return AccountContactPerson
     */
    public function getPrimaryPaymentByAccountForDivisions(Workorder $workorder)
    {
        /** @var AccountContactPerson $accountContactPerson */
        $accountContactPerson = $this->accountContactPersonRepository->findPrimaryPaymentByDivisionForAccount(
            $workorder->getAccount(),
            $workorder->getDivision()
        );

        if (!empty($accountContactPerson) && empty($accountContactPerson->getCustomBillingAddress())) {
            $address = $this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE);

            $accountContactPerson->setCustomBillingAddress($address);

            $this->objectManager->persist($address);
        }

        return $accountContactPerson;
    }

    /**
     * @param Workorder $workorder
     * @param AccountContactPerson $authorizer
     * @return AccountContactPersonHistory
     */
    private function checkAndGetAuthorizerHistoryForFroze(Workorder $workorder, AccountContactPerson $authorizer)
    {
        $param['beforeDate'] = $this->workorderService->getFrozenDate($workorder)->format("Y-m-d H:i:s");
        /** @var AccountContactPersonHistory $authorizerHistory */
        $authorizerHistory = $this->accountContactPersonHistoryRepository->getLastRecord(
            $authorizer,
            $param
        );
        if (!$authorizerHistory) {
            $this->accountContactPersonHistoryCreatorService->create($authorizer);
        }
        /** @var AccountContactPersonHistory $authorizerHistory */
        $authorizerHistory = $this->accountContactPersonHistoryRepository->getLastRecord($authorizer, $param);

        return $authorizerHistory;
    }
}
