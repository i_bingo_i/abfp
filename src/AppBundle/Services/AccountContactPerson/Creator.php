<?php

namespace AppBundle\Services\AccountContactPerson;

use AppBundle\Creators\AddressCreator;
use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Factories\AccountContactPersonFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

class Creator
{
    /** @var ContainerInterface */
    private $container;
    /** @var ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonManager */
    private $accountContactPersonManager;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var AddressCreator */
    private $addressCreator;

    /**
     * Creator constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->accountContactPersonManager = $this->container->get('app.account_contact_person.manager');
        $this->deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
        $this->addressCreator = $this->container->get('address.creator');
    }

    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @param bool $needSave
     * @return AccountContactPerson
     */
    public function create(Account $account, ContactPerson $contactPerson, $needSave = false)
    {
        /** @var DeviceCategory[] $responsibilities */
        $responsibilities = $this->deviceCategoryRepository->findAll();

        /** @var AccountContactPerson $acp */
        $acp = AccountContactPersonFactory::make($account, $contactPerson);
        $acp->setAccess(true);
        $acp->setAccessPrimary(true);
        $acp->setPayment(true);
        $this->createCustomBillingAddress($acp);

        /** @var DeviceCategory $responsibility */
        foreach ($responsibilities as $responsibility) {
            $acp->addResponsibility($responsibility);
            $acp->addPaymentResponsibility($responsibility);
        }

        if ($needSave) {
            $this->accountContactPersonManager->save($acp);
        }

        return $acp;
    }

    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @return AccountContactPerson
     */
    public function createForWorkOrder(Account $account, ContactPerson $contactPerson)
    {
        /** @var AccountContactPerson $acp */
        $acp = AccountContactPersonFactory::make($account, $contactPerson);
        $acp->setAccess(false);
        $acp->setAccessPrimary(false);
        $acp->setPayment(false);
        $acp->setSendingAddress($contactPerson->getPersonalAddress());
        $this->accountContactPersonManager->save($acp);

        return $acp;
    }

    /**
     * @param AccountContactPerson $acp
     */
    public function createCustomBillingAddress(AccountContactPerson $acp)
    {
        $acp->setCustomBillingAddress($this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE));
    }
}
