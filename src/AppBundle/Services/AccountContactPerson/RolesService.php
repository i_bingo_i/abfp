<?php

namespace AppBundle\Services\AccountContactPerson;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Services\Address\AddressProvider;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;

class RolesService
{
    /** @var ContainerInterface */
    private $container;
    /** @var ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonManager */
    private $accountContactPersonManager;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;

    /**
     * RolesService constructor.
     * @param ContainerInterface $container
     * @param ObjectManager $objectManager
     */
    public function __construct(ContainerInterface $container, ObjectManager $objectManager)
    {
        $this->container = $container;
        $this->objectManager = $objectManager;

        $this->accountContactPersonManager = $this->container->get('app.account_contact_person.manager');
        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
    }

    /**
     * @param AccountContactPerson $contact
     */
    public function setRoleAndResponsibility(AccountContactPerson $contact)
    {
        /** @var AccountContactPerson[] $accountContacts */
        $accountContacts = $this->accountContactPersonRepository->findBy([
            'account' => $contact->getAccount(),
            'deleted' => false
        ]);

        /** @var AccountContactPerson $accountContact */
        foreach ($accountContacts as $accountContact) {
            if ($accountContact === $contact) {
                continue;
            }

            //set for Authorizer responsibility
            if ($contact->getResponsibilities() and $accountContact->getResponsibilities()) {
                $this->clearResponsibilityByArray($accountContact, $contact->getResponsibilities(), null);
            }

            //set for payments responsibility
            if ($contact->getPaymentResponsibilities() and $accountContact->getPaymentResponsibilities()) {
                $this->clearResponsibilityByArray($accountContact, null, $contact->getPaymentResponsibilities());
            }

            if ($contact->getAccessPrimary() and $accountContact->getAccessPrimary()) {
                $accountContact->setAccessPrimary(false);
                $this->objectManager->persist($accountContact);
            }
        }
        $this->accountContactPersonManager->update($contact);
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @param Address|null $address
     * @param bool $flag
     */
    public function updateCustomBillingAddress(
        AccountContactPerson $accountContactPerson,
        ?Address $address,
        $flag = false
    ) {
        $customBillingAddress = $accountContactPerson->getCustomBillingAddress();

        if ($flag && !empty($address)) {
            $customBillingAddress->setAddress($address->getAddress());
            $customBillingAddress->setCity($address->getCity());
            $customBillingAddress->setState($address->getState());
            $customBillingAddress->setZip($address->getZip());

        } else {
            AddressProvider::setInNullAddress($customBillingAddress);
        }
    }

    /**
     * @param AccountContactPerson $contact
     * @param $responsibilities
     * @param $paymentResponsibilities
     */
    private function clearResponsibilityByArray(
        AccountContactPerson $contact,
        $responsibilities,
        $paymentResponsibilities
    ) {
        if ($responsibilities) {
            /** @var DeviceCategory $responsibility */
            foreach ($responsibilities as $responsibility) {
                $contact->removeResponsibility($responsibility);
                $this->objectManager->persist($contact);
            }
        }

        if ($paymentResponsibilities) {
            /** @var DeviceCategory $responsibility */
            foreach ($paymentResponsibilities as $responsibility) {
                $contact->removePaymentResponsibility($responsibility);
                $this->objectManager->persist($contact);
            }
        }
    }
}
