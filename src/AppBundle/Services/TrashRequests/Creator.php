<?php

namespace AppBundle\Services\TrashRequests;

use AppBundle\Entity\EntityManager\TrashRequestsManager;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\TrashRequests;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class Creator
{
    /** @var ContainerInterface  */
    private $serviceContainer;
    /** @var ObjectManager */
    private $objectManager;
    /** @var TrashRequestsManager */
    private $trashRequestsManager;
    /** @var UserRepository */
    private $userRepository;

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager,ContainerInterface $serviceContainer)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $serviceContainer;

        $this->trashRequestsManager = $this->serviceContainer->get('app.trash_requests.manager');

        $this->userRepository = $this->objectManager->getRepository("AppBundle:User");
    }

    /**
     * @deprecated This method will be removed in the following releases.
     * Instead it should be used "add" of the same class.
     *
     * @param $route
     * @param $params
     * @param array $messages
     * @param null $file
     * @param null $accessToken
     */
    public function create($route, $params, array $messages, $file = null, $accessToken = null)
    {
        $trashRequests = new TrashRequests();
        $trashRequests->setRoute($route);
        $trashRequests->setParams($params);
        $trashRequests->setMessage($this->prepareMessageBeforeSave($messages));
        $trashRequests->setFile($file);
        $trashRequests = $this->setAuthor($trashRequests, $accessToken);

        $this->trashRequestsManager->save($trashRequests);
    }

    /**
     * @param Request $request
     * @param $message
     */
    public function add(Request $request, $message)
    {
        $accessToken = $request->headers->get("X-ACCESS-TOKEN");
        $route = $request->getRequestUri();

        $this->create(
            $route,
            json_encode($request->request->all()),
            [$message],
            null,
            $accessToken
        );
    }

    /**
     * @param array $messages
     * @return null|string
     */
    private function prepareMessageBeforeSave(array $messages)
    {
        $result = null;

        foreach ($messages as $key => $value) {
            if (strpos($value, ".") === false) {
                $result .= $value . "." . " ";
            } else {
                $result .= $value . " ";
            }
        }

        return $result;
    }

    /**
     * @param TrashRequests $trashRequests
     * @param null $accessToken
     * @return TrashRequests
     */
    private function setAuthor(TrashRequests $trashRequests, $accessToken = null)
    {
        if (!is_null($accessToken)) {
            $user = $this->userRepository->findOneBy(["accessToken" => $accessToken]);
            $trashRequests->setAuthor($user);
        }

        return $trashRequests;
    }
}
