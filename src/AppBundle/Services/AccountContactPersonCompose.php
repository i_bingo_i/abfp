<?php

namespace AppBundle\Services;

use AppBundle\Entity\DeviceCategory;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\AccountContactPerson;

class AccountContactPersonCompose
{
    /** @var ObjectManager $objectManager */
    private $objectManager;

    /**
     * AccountContactPersonCompose constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param AccountContactPerson[] $contacts
     * @return array
     */
    public function compose($contacts)
    {
        $other = [];
        $primary = [];
        $responsibilities = [];
        /** @var AccountContactPerson $contact */
        foreach ($contacts as $contact) {
            if ($this->isPrimary($contact)) {
                $primary[] = $contact;
                if ($contact->getAuthorizer() && !empty($contact->getResponsibilities()->count())) {
                    /** @var DeviceCategory $responsibility */
                    foreach ($contact->getResponsibilities() as $responsibility) {
                        $responsibilities[$responsibility->getId()] = $responsibility;
                    }
                }
                continue;
            }
            $other[] = $contact;
        }

        ksort($responsibilities);

        return [
            'other' => $this->sortOther($other),
            'primary' => $this->sortPrimary($primary),
            'responsibilities' => $responsibilities
        ];
    }

    /**
     * @param AccountContactPerson $contact
     * @return bool
     */
    private function isPrimary(AccountContactPerson $contact)
    {
        if ($contact->getAccess() and $contact->getAccessPrimary()) {
            return true;
        }

        if ($contact->getPayment() and count($contact->getPaymentResponsibilities())) {
            return true;
        }

        if ($contact->getAuthorizer() and count($contact->getResponsibilities())) {
            return true;
        }

        return false;
    }

    /**
     * @param $contacts
     * @return mixed
     */
    private function separateByRole($contacts)
    {
        $separatedArray['authorizer'] =[];
        $separatedArray['access'] =[];
        $separatedArray['payments'] =[];
        /** @var AccountContactPerson $contact */
        foreach ($contacts as $contact) {
            if ($contact->getAuthorizer()) {
                $separatedArray['authorizer'][] = $contact;
                continue;
            }

            if ($contact->getAccess()) {
                $separatedArray['access'][] = $contact;
                continue;
            }

            if ($contact->getPayment()) {
                $separatedArray['payments'][] = $contact;
                continue;
            }
        }

        return $separatedArray;
    }

    /**
     * @param $contacts
     * @return array
     */
    private function sortOther($contacts)
    {
        $separatedArray = $this->separateByRole($contacts);

        return array_merge($separatedArray['authorizer'], $separatedArray['access'], $separatedArray['payments']);
    }


    /**
     * @param $contacts
     * @return array
     */
    private function sortPrimary($contacts)
    {
        $separatedArray = $this->separateByRole($contacts);

        usort($separatedArray['authorizer'], function ($contactA, $contactB) {
            $weightContactA = $this->getResponsibilityWeightForContact($contactA);
            $weightContactB = $this->getResponsibilityWeightForContact($contactB);

            if ($weightContactA == $weightContactB) {
                return 0;
            }

            return ($weightContactA < $weightContactB) ? -1 : 1;
        });
        $separatedArray['authorizer'] = array_reverse($separatedArray['authorizer']);

        return array_merge($separatedArray['authorizer'], $separatedArray['access'], $separatedArray['payments']);
    }

    /**
     * @param AccountContactPerson $contact
     * @return int|mixed
     */
    private function getResponsibilityWeightForContact(AccountContactPerson $contact)
    {
        $maxWeight = 0;

        /** @var DeviceCategory $responsibility */
        foreach ($contact->getResponsibilities() as $responsibility) {
            $weight = $responsibility->getWeight();
            if ($weight > $maxWeight) {
                $maxWeight = $weight;
            }
        }

        return $maxWeight;
    }
}
