<?php

namespace AppBundle\Services\Proposal;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;
use AppBundle\Entity\EntityManager\OpportunityManager;
use AppBundle\Entity\EntityManager\ProposalLifeCycleManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Message;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalLifeCycle;
use AppBundle\Entity\ProposalStatus;
use AppBundle\Entity\ProposalType;
use AppBundle\Entity\Repository\RepairDeviceInfo as RepairDeviceInfoRepository;

use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Repository\ProposalStatusRepository;
use AppBundle\Entity\Repository\ProposalTypeRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\User;
use AppBundle\Events\ProposalCreateEvent;
use AppBundle\Services\Files;
use AppBundle\Services\Opportunity\Grouping as OpportunityGrouping;
use AppBundle\Services\Service\EstimationTime;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\EntityManager\AccountManager;

use AppBundle\Traits\CollectionTrait;

class Creator
{
    use CollectionTrait;

    /** @var ObjectManager */
    private $objectManager;
    /** @var ProposalManager */
    private $proposalManager;
    /** @var TokenStorageInterface */
    private $securityTokenStorage;
    /** @var ProposalLifeCycleManager */
    private $proposalLifeCycleManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var OpportunityManager */
    private $opportunityManager;
    /** @var AccountContactPersonManager */
    private $accountContactPersonManager;
    /** @var AccountManager|object  */
    private $accountManager;

    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var Session */
    private $session;
    /** @var Freeze */
    private $freezeService;
    /** @var ProposalService */
    private $proposalService;
    /** @var OpportunityGrouping */
    private $opportunityGroupingService;
    /** @var EstimationTime */
    private $estimationTimeService;

    /** @var ProposalStatusRepository */
    private $proposalStatusRepository;
    /** @var ProposalTypeRepository */
    private $proposalTypeRepository;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var MessageRepository */
    private $messageRepository;
    /** @var $proposalRepository */
    private $proposalRepository;
    /** @var RepairDeviceInfoRepository */
    private $repairDeviceInfoRepository;

    /** @var ProposalStatus */
    private $proposalStatusToBeSent;
    /** @var ProposalStatus */
    private $proposalStatusDraft;
    /** @var \AppBundle\Services\RepairDeviceInfo\Creator */
    private $repairDeviceInfoCreatorService;

    /** @var array */
    public $proposalAndServiceStatuses = [
        'retest' => 'under_retest_notice',
        'repair' => 'under_proposal'
    ];

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param TokenStorageInterface $securityTokenStorage
     * @param ProposalManager $proposalManager
     * @param ProposalLifeCycleManager $proposalLifeCycleManager
     * @param ServiceManager $serviceManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(
        ObjectManager $objectManager,
        TokenStorageInterface $securityTokenStorage,
        ProposalManager $proposalManager,
        ProposalLifeCycleManager $proposalLifeCycleManager,
        ServiceManager $serviceManager,
        ContainerInterface $serviceContainer
    ) {
        $this->objectManager = $objectManager;
        $this->proposalManager = $proposalManager;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->proposalLifeCycleManager = $proposalLifeCycleManager;
        $this->serviceManager = $serviceManager;
        $this->serviceContainer = $serviceContainer;
        $this->dispatcher = $this->serviceContainer->get('event_dispatcher');
        $this->opportunityManager = $this->serviceContainer->get('app.opportunity.manager');
        $this->session = $this->serviceContainer->get('session');
        $this->freezeService = $this->serviceContainer->get('app.proposal.freeze.service');
        $this->opportunityGroupingService = $this->serviceContainer->get('app.opportunity_grouping.service');
        $this->accountContactPersonManager = $this->serviceContainer->get('app.account_contact_person.manager');
        $this->proposalService = $this->serviceContainer->get('app.proposal_service.service');
        $this->estimationTimeService = $this->serviceContainer->get('app.estimation_time');
        $this->accountManager = $this->serviceContainer->get('app.account.manager');

        $this->proposalStatusRepository = $this->objectManager->getRepository('AppBundle:ProposalStatus');
        $this->proposalTypeRepository = $this->objectManager->getRepository('AppBundle:ProposalType');
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');
        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->messageRepository = $this->objectManager->getRepository('AppBundle:Message');
        $this->proposalRepository = $this->objectManager->getRepository('AppBundle:Proposal');
        /** @var RepairDeviceInfoRepository repairDeviceInfoRepository */
        $this->repairDeviceInfoRepository = $this->objectManager->getRepository('AppBundle:RepairDeviceInfo');

        $this->proposalStatusToBeSent = $this->proposalStatusRepository->findOneBy(['alias' => 'to_be_sent']);
        $this->proposalStatusDraft = $this->proposalStatusRepository->findOneBy(['alias' => 'draft']);
        $this->repairDeviceInfoCreatorService = $this->serviceContainer->get('app.device_info_creator.service');
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @param $services
     *
     * @return Proposal
     */
    public function createRetestNotice(Account $account, DeviceCategory $division, $services)
    {
        /** @var Proposal $proposal */
        $proposal = $this->newProposal($account, $division);
        $proposal->setStatus($this->proposalStatusDraft);

        /** @var ProposalType $proposalTypeRetest */
        $proposalTypeRetest = $this->proposalTypeRepository->findOneBy(['alias' => 'retest']);
        $proposal->setType($proposalTypeRetest);

        /** @var ProposalLifeCycle $proposalLifeCycle */
        $proposalLifeCycle = $this->proposalLifeCycleManager->getProposalLifeCycle($proposal);
        $proposal->setLifeCycle($proposalLifeCycle);
        $proposal = $this->addDepartmentChannel($proposal);
        $this->proposalManager->save($proposal);

        /** Doing frozen services and bind proposal to service */
        $proposal = $this->proposalService->addServicesToProposal($proposal, $services);

        /** @var ProposalCreateEvent $event */
        $event = new ProposalCreateEvent($proposal);
        $this->dispatcher->dispatch('proposal.create', $event);

        return $proposal;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     *
     * @return Proposal|bool
     */
    public function createProposal(Account $account, DeviceCategory $division)
    {
        /** @var Proposal $proposal */
        $proposal = $this->newProposal($account, $division);
        $proposal->setStatus($this->proposalStatusDraft);

        /** @var ProposalType $proposalTypeRetest */
        $proposalTypeRetest = $this->proposalTypeRepository->findOneBy(['alias' => 'repair']);
        $proposal->setType($proposalTypeRetest);

        /** @var ProposalLifeCycle $proposalLifeCycle */
        $proposalLifeCycle = $this->proposalLifeCycleManager->getProposalLifeCycle($proposal);
        $proposal->setLifeCycle($proposalLifeCycle);
        $proposal = $this->addDepartmentChannel($proposal);

        return $proposal;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     * @param UploadedFile $proposalFile
     *
     * @return Proposal
     * @throws \Twig\Error\Error
     */
    public function createCustomProposal(Account $account, DeviceCategory $division, UploadedFile $proposalFile)
    {
        /** @var Message $messages */
        $messages = $this->messageRepository->findErrorByAccount($account);

        if (!empty($messages)) {
            /** @var Message $message */
            foreach ($messages as $message) {
                //custom proposal should not look at the "no_autorizer" error
                if (!$message->getAlias() == 'no_autorizer') {
                    return $this->session->getFlashBag()->add(
                        "failed",
                        'None proposals were created due to the issues with related Accounts.'
                    );
                }
            }
        }

        /** @var Proposal $proposal */
        $proposal = $this->newProposal($account, $division);
        $proposal->setStatus($this->proposalStatusDraft);
        $proposal->setEarliestDueDate(new \DateTime());

        /** @var ProposalType $proposalTypeCustom */
        $proposalTypeCustom = $this->proposalTypeRepository->findOneBy(['alias' => 'custom']);
        $proposal->setType($proposalTypeCustom);

        /** @var Files $fileService */
        $fileService = $this->serviceContainer->get('app.files');
        $proposalFileName = $this->generatedReportName($proposal);
        $saveFile = $fileService->saveUploadFile($proposalFile, "/uploads/proposals", $proposalFileName);
        $proposal->setReport($saveFile);

        /** @var ProposalLifeCycle $proposalLifeCycle */
        $proposalLifeCycle = $this->proposalLifeCycleManager->getProposalLifeCycle($proposal);
        $proposal->setLifeCycle($proposalLifeCycle);
        $proposal = $this->addDepartmentChannel($proposal);

        $proposal = $this->freezeService->freezing($proposal, false);

        /** @var ProposalCreateEvent $event */
        $event = new ProposalCreateEvent($proposal);
        $this->dispatcher->dispatch('proposal.create', $event);

        return $proposal;
    }

    /**
     * @param $opportunities
     * @param bool $froze
     *
     * @return Proposal[]|array
     * @throws \Twig\Error\Error
     */
    public function createByRetestOpportunity($opportunities, $froze = true)
    {
        /** @var Proposal[] $proposals */
        $proposals = [];
        $failsCount = 0;
        $groupedOpportunities = $this->opportunityGroupingService->groupingByDivisionAndAccount($opportunities);

        foreach ($groupedOpportunities as $group) {
            /** @var Service[] $services */
            $services = $this->opportunityManager->getServicesForArray($group['opportunities']);
            /** @var Account $account */
            $account = $group['account'];
            /** @var DeviceCategory $division */
            $division = $group['division'];
            /** @var Message $messages */
            $messages = $this->messageRepository->findErrorByAccount($account);

            if (!empty($messages)) {
                $failsCount++;
                continue;
            }

            $retestNotice = false;
            if (!$froze) {
                /** @var Proposal $retestNotice */
                $retestNotice = $this->proposalRepository->findDraftRetestNotice($account, $division);
            }

            if (!$retestNotice) {
                $retestNotice = $this->createRetestNotice($account, $division, $services);
            } else {
                /** Doing frozen services and bind proposal to service */
                $retestNotice = $this->proposalService->addServicesToProposal($retestNotice, $services);
            }

            $this->repairDeviceInfoCreatorService->creatingForInspection($retestNotice);

            $this->proposalService->calculateAndSetAllProposalTotals($retestNotice);
            $this->estimationTimeService->saveNewTotalEstimationTime($retestNotice);

            if ($froze) {
                $retestNotice = $this->freezeService->freezing($retestNotice);
            }

            $proposals[] = $retestNotice;
        }

        $proposalsCount = count($proposals);
        if (!$proposalsCount) {
            $this->session->getFlashBag()->add(
                "failed",
                'None retest notices were created due to the issues with related Accounts.'
            );

            return $proposals;
        }

        if ($proposalsCount) {
            $this->session->getFlashBag()->add(
                "success",
                $proposalsCount . ' retest notices were successfully created'
            );
        }

        if ($failsCount) {
            $this->session->getFlashBag()->add(
                "warning",
                $failsCount . ' retest notices were not created due to the issues with related Accounts'
            );
        }

        return $proposals;
    }

    /**
     * @param Account $account
     * @param DeviceCategory $division
     *
     * @return Proposal
     */
    private function newProposal(Account $account, DeviceCategory $division)
    {
        /** @var Proposal $proposal */
        $proposal = new Proposal();

        $proposal->setDivision($division);
        $proposal->setDateOfProposal(new \DateTime());
        $proposal->setReferenceId($this->proposalManager->generateReferenceId());
        $proposal->setAccount($account);

        /** @var User $currentUser */
        $currentUser = $this->securityTokenStorage->getToken()->getUser();
        /** @var ContractorUser $contractorUser */
        $contractorUser = $this->contractorUserRepository->findOneBy(['user' => $currentUser]);
        $proposal->setCreator($contractorUser);

        /** @var AccountContactPerson $accountContactPerson */
        $accountContactPerson = $this->accountContactPersonRepository->findAuthorizerByDivision($account, $division);
        $proposal->setRecipient($accountContactPerson);

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     *
     * @return Proposal
     */
    private function addDepartmentChannel(Proposal $proposal)
    {
        $departmentChannelRepository = $this->objectManager->getRepository('AppBundle:DepartmentChannel');
        $departmentRepository = $this->objectManager->getRepository('AppBundle:Department');
        $department = $departmentRepository->findOneBy(
            [
                'municipality' => $proposal->getAccount()->getMunicipality(),
                'division' => $proposal->getDivision()
            ]
        );
        $departmentChannel = $departmentChannelRepository->findOneBy(['department' => $department, 'isDefault' => 1]);
        $proposal->departmentChannel = $departmentChannel;

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     *
     * @return string
     */
    private function generatedReportName(Proposal $proposal)
    {
        $division = $proposal->getDivision()->getName();
        $dateOfProposal = $proposal->getDateOfProposal()->format('m-d-Y');
        $referenceId = $proposal->getReferenceId();
        $fileNameType = $proposal->getType()->getName() . '_Proposal';

        return $division . '_' . $fileNameType . '_' . $dateOfProposal . '_' . 'id' . '_' . $referenceId . '.pdf';
    }
}
