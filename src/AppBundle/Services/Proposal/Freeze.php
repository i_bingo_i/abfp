<?php

namespace AppBundle\Services\Proposal;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPersonHistory;
use AppBundle\Entity\AccountHistory;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\ContractorUserHistory;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\FrozenServiceForProposal;
use AppBundle\Entity\ProposalStatus;
use AppBundle\Entity\Repository\AccountContactPersonHistoryRepository;
use AppBundle\Entity\Repository\AccountHistoryRepository;
use AppBundle\Entity\Repository\ContractorUserHistoryRepository;
use AppBundle\Entity\Repository\ProposalStatusRepository;
use AppBundle\Entity\Repository\ServiceHistoryRepository;
use AppBundle\Entity\ServiceHistory;
use AppBundle\Services\PDFService;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\FrozenProposal;
use Symfony\Component\DependencyInjection\ContainerInterface;

use AppBundle\Traits\CollectionTrait;

class Freeze
{
    use CollectionTrait;

    /** @var ObjectManager */
    private $objectManager;
    /** @var ProposalManager */
    private $proposalManager;
    /** @var AccountContactPersonManager */
    private $accountContactPersonManager;
    /** @var PDFService */
    private $pdfService;

    /** @var ProposalStatusRepository */
    private $proposalStatusRepository;
    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;
    /** @var AccountHistoryRepository  */
    private $accountHistoryRepository;
    /** @var ContractorUserHistoryRepository  */
    private $contractorUserHistoryRepository;
    /** @var AccountContactPersonHistoryRepository  */
    private $accountContactPersonHistoryRepository;

    /**
     * Freeze constructor.
     * @param ObjectManager $objectManager
     * @param ProposalManager $proposalManager
     * @param AccountContactPersonManager $accountContactPersonManager
     * @param PDFService $pdfService
     */
    public function __construct(
        ObjectManager $objectManager,
        ProposalManager $proposalManager,
        AccountContactPersonManager $accountContactPersonManager,
        PDFService $pdfService
    ) {
        $this->objectManager = $objectManager;
        $this->proposalManager = $proposalManager;
        $this->accountContactPersonManager = $accountContactPersonManager;
        $this->pdfService = $pdfService;

        $this->proposalStatusRepository = $this->objectManager->getRepository('AppBundle:ProposalStatus');
        $this->serviceHistoryRepository = $this->objectManager->getRepository('AppBundle:ServiceHistory');
        $this->accountHistoryRepository = $this->objectManager->getRepository('AppBundle:AccountHistory');
        $this->contractorUserHistoryRepository = $this->objectManager
            ->getRepository('AppBundle:ContractorUserHistory');
        $this->accountContactPersonHistoryRepository = $this->objectManager
            ->getRepository('AppBundle:AccountContactPersonHistory');
    }

    /**
     * @param Proposal $proposal
     * @param bool $creatingPdf
     * @return Proposal
     */
    public function freezing(Proposal $proposal, $creatingPdf = true) : Proposal
    {
        /** @var FrozenProposal $frozenProposal */
        $frozenProposal = new FrozenProposal();
        /** @var Account $account */
        $account = $proposal->getAccount();
        /** @var DeviceCategory $division */
        $division = $proposal->getDivision();
        /** @var ContractorUser $creator */
        $creator = $proposal->getCreator();

        /** @var AccountHistory $lastAccountHistory */
        $lastAccountHistory = $this->accountHistoryRepository->getLastRecord($account);
        $frozenProposal->setAccount($lastAccountHistory);

        /** @var ContractorUserHistory $lastContractorUserHistory */
        $lastContractorUserHistory = $this->contractorUserHistoryRepository->getLastRecord($creator);
        $frozenProposal->setCreator($lastContractorUserHistory);

        /** @var AccountContactPersonHistory $accountContactPersonHistory */
        $accountContactPersonHistory = $this->accountContactPersonManager->getHistoryAuthorizer($account, $division);
        $frozenProposal->setRecipient($accountContactPersonHistory);

        $frozenProposal = $this->freezingServices($frozenProposal, $proposal->getServices());
        $this->objectManager->persist($frozenProposal);

        /** @var ProposalStatus $proposalStatusToBeSent */
        $proposalStatusToBeSent = $this->proposalStatusRepository->findOneBy(['alias' => 'to_be_sent']);
        $proposal->setStatus($proposalStatusToBeSent);
        $proposal->setIsFrozen(true);
        $proposal->setFrozen($frozenProposal);

        // Creating pdf
        if ($creatingPdf) {
            $report = $this->pdfService->create($this->pdfService::ACTION_CREATE, $proposal);
            $proposal->setReport($report);
        }

        $proposal->setRecipient(null);

        $this->objectManager->persist($proposal);
        $this->objectManager->flush();

        return $proposal;
    }

    /**
     * @param FrozenProposal $frozenProposal
     * @param $services
     * @return FrozenProposal
     */
    private function freezingServices(FrozenProposal $frozenProposal, $services) : FrozenProposal
    {
        $serviceIds = $this->getIdsArray($services);
        /** @var ServiceHistory[] $historyServices */
        $historyServices = $this->serviceHistoryRepository->getLastByService($serviceIds);

        /** @var ServiceHistory $historyService */
        foreach ($historyServices as $historyService) {
            /** @var FrozenServiceForProposal $frozenService */
            $frozenService = new FrozenServiceForProposal();
            $frozenService->setServiceHistory($historyService);
            $frozenService->setFrozenProposal($frozenProposal);
            $this->objectManager->persist($frozenService);

            $frozenProposal->addFrozenService($frozenService);
            $this->objectManager->persist($frozenProposal);
        }

        return $frozenProposal;
    }
}
