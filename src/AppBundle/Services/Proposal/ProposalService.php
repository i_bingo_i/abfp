<?php

namespace AppBundle\Services\Proposal;

use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\EntityManager\RepairDeviceInfoManager;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalType;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\FrozenServiceForProposalRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\RepairDeviceInfo as RepairDeviceInfoRepository;
use AppBundle\Entity\ServiceFrequency;
use AppBundle\Entity\FrozenServiceForProposal;
use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Service;
use ServiceBundle\Services\ServiceCreatorService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\Service\EstimationTime;
use ServiceBundle\Services\CalculateTotals;

use AppBundle\Traits\CollectionTrait;

class ProposalService
{
    use CollectionTrait;

    /** @var ObjectManager */
    private $objectManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ProposalManager */
    private $proposalManager;
    /** @var ContainerInterface */
    private $container;
    /** @var CalculateFee  */
    private $calculateFeeService;

    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var FrozenServiceForProposalRepository */
    private $frozenServiceForProposalRepository;
    /** @var RepairDeviceInfoRepository */
    private $repairDeviceInfoRepository;
    /** @var RepairDeviceInfoManager */
    private $repairDeviceInfoManager;

    private $deviceCategoryRepository;
    /** @var MunicipalityRepository */
    private $municipalityRepository;

    /** @var EstimationTime */
    private $estimationTimeService;
    /** @var CalculateTotals */
    private $serviceCalculateTotals;
    /** @var ServiceCreatorService */
    private $serviceCreatorService;

    /** @var array */
    public $proposalAndServiceStatuses = [
        'retest' => 'under_retest_notice',
        'repair' => 'under_proposal'
    ];
    /** @var DeviceCategoryRepository */


    /**
     * ProposalService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->objectManager = $this->container->get('app.entity_manager');
        $this->serviceManager = $this->container->get('app.service.manager');
        $this->proposalManager = $this->container->get('app.proposal.manager');
        $this->repairDeviceInfoManager = $this->container->get('app.repair_device_info.manager');
        $this->calculateFeeService = $this->container->get('app.proposal_calculate_fee.service');
        $this->estimationTimeService = $this->container->get('app.estimation_time');
        $this->serviceCalculateTotals = $this->container->get('service.calculate_totals.service');

        $this->frozenServiceForProposalRepository = $this->objectManager
            ->getRepository('AppBundle:FrozenServiceForProposal');
        $this->repairDeviceInfoRepository = $this->objectManager->getRepository('AppBundle:RepairDeviceInfo');
        $this->deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->municipalityRepository = $this->objectManager->getRepository('AppBundle:Municipality');
        $this->serviceCreatorService = $this->container->get('service.creator.service');
    }

    /**
     * @param ServiceNamed[] $repairsNamed
     * @param Device $device
     * @param Proposal $proposal
     *
     * @return Proposal
     */
    public function addRepairs($repairsNamed, Device $device, Proposal $proposal)
    {
        /** @var ServiceNamed $repairNamed */
        foreach ($repairsNamed as $repairNamed) {
            $service = $this->serviceManager->getDiscardAndResolvedService($device, $repairNamed);
            if (empty($service)) {
                $service = $this->serviceCreatorService->createNonFrequencyByServiceName($device, $repairNamed);
            }
            $service->setInspectionDue(new \DateTime());
            
            $service = $this->serviceManager->setStatus(
                $service,
                $this->proposalAndServiceStatuses[$proposal->getType()->getAlias()]
            );
            $this->serviceManager->resetOpportunity($service);
            $service->setProposal($proposal);
            $proposal->addService($service);
            $this->serviceManager->update($service);
        }

        $this->objectManager->persist($proposal);
        $this->objectManager->flush();

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     * @param $services
     *
     * @return Proposal
     */
    public function addServicesToProposal(Proposal $proposal, $services)
    {
        /** @var Service $service */
        foreach ($services as $service) {
            $service = $this->serviceManager->setStatus(
                $service,
                $this->proposalAndServiceStatuses[$proposal->getType()->getAlias()]
            );
            $this->serviceManager->resetOpportunity($service);
            $service->setProposal($proposal);
            $proposal->addService($service);
            $this->serviceManager->update($service);
        }
        $proposal = $this->setMinEarliestDueDate($proposal);
        $this->proposalManager->save($proposal);

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     * @return Proposal
     */
    public function setMinEarliestDueDate(Proposal $proposal)
    {
        $services = $this->serviceRepository->findBy(['proposal' => $proposal, 'deleted' => false]);

        $serviceIds = $this->getIdsArray($services);
        $earliestDueDateByServices = $this->serviceRepository->getEarliestDueDate($serviceIds);

        if ($earliestDueDateByServices) {
            $earliestDueDateByServicesValue = new \DateTime($earliestDueDateByServices);
        } else {
            //set DateCreate
            $proposalDateCreate = $proposal->getDateCreate();
            $earliestDueDateByServicesValue = $proposalDateCreate;
        }

        $proposal->setEarliestDueDate($earliestDueDateByServicesValue);

        return $proposal;
    }

    /**
     * @param Device $device
     * @param Proposal $proposal
     */
    public function excludeByDeviceFromProposal(Device $device, Proposal $proposal)
    {
        // TODO: NEED REFACTORING
        if ($proposal->getIsFrozen()) {
            /** @var FrozenServiceForProposal $frozenRepairServices */
            $frozenRepairServices = $this->frozenServiceForProposalRepository
                ->findRepairByDeviceForProposal($device, $proposal->getFrozen());

            /** @var FrozenServiceForProposal $frozenRepair */
            foreach ($frozenRepairServices as $frozenRepair) {
                /** @var Service $service */
                $service = $frozenRepair->getServiceHistory()->getOwnerEntity();
                $this->excludeFromProposal($service);
                $this->objectManager->remove($frozenRepair);
            }
        } else {
            /** @var Service $service */
            foreach ($proposal->getServices() as $service) {
                if ($device->getId() == $service->getDevice()->getId()) {
                    $this->excludeFromProposal($service);
                }
            }
        }

        $devicesCount = $proposal->getDevicesCount();
        if ($devicesCount > 0) {
            $proposal->setDevicesCount($devicesCount - 1);
        }
        $this->objectManager->persist($proposal);

        $this->objectManager->flush();

        // TODO: NEED REFACTORING
        $this->calculateAndSetAllProposalTotals($proposal);
    }

    /**
     * @param Service[] $services
     * @param Proposal $proposal
     *
     */
    public function setCounts($services, Proposal $proposal)
    {
        $accountsIds = [];
        $devicesIds = [];

        /** @var Service $service */
        foreach ($services as $service) {
            $accountId = $service->getAccount()->getId();
            $accountsIds[$accountId] = $accountId;
            $deviceId = $service->getDevice()->getId();
            $devicesIds[$deviceId] = $deviceId;
        }

        $proposal->setServicesCount(count($services));
        $proposal->setDevicesCount(count($devicesIds));
        $proposal->setSitesCount(count($accountsIds));
    }

    /**
     * @param Service $service
     * @return Proposal
     */
    public function deleteServiceFromProposal(Service $service)
    {
        /** @var Proposal $proposal */
        $proposal = $service->getProposal();
        /** @var Device $device */
        $device = $service->getDevice();
        /** @var ServiceFrequency $serviceFrequency */
        $serviceFrequency = $service->getNamed()->getFrequency();

        $this->excludeFromProposal($service);

        if (!$serviceFrequency) {
            $this->deleteServiceFromRepairProposalActions($service, $proposal, $device);
        }

        return $proposal;
    }

    /**
     * @param Proposal $proposal
     */
    public function calculateAndSetAllProposalTotals(Proposal $proposal)
    {
        $services = $this->serviceRepository->findBy(['proposal' => $proposal, 'deleted' => false]);
        $proposalType = $proposal->getType()->getAlias();

        $proposal->setAllTotalMunicAndProcFee($this->calculateTotalMunicAndProcFee($proposal));

        if ($proposalType == "retest") {
            $this->calculateAndSetAllRetestNoticeTotals($proposal, $services);
        }

        if ($proposalType == "repair") {
            $this->calculateAndSetAllRepairProposalTotals($proposal, $services);
        }

        $this->setCounts($services, $proposal);

        $this->estimationTimeService->saveNewTotalEstimationTime($proposal);

        $proposal->setIsMunicipalityFessWillVary($this->isProposalWithChannelWithFeesWillVary($proposal));

        $this->objectManager->flush();
    }

    /**
     * @param Proposal $proposal
     * @param $services
     */
    private function calculateAndSetAllRetestNoticeTotals(Proposal $proposal, $services)
    {
        $grandTotal = $this->calculateGrandTotal($proposal, $services);
        $proposal->setGrandTotal($grandTotal);
        $totalFee = $this->serviceCalculateTotals->calcTotalFee($services);
        $proposal->setServiceTotalFee($totalFee);
    }

    /**
     * @param Proposal $proposal
     * @param $services
     */
    private function calculateAndSetAllRepairProposalTotals(Proposal $proposal, $services)
    {
        $proposal->setAllTotalRepairsPrice(
            $this->repairDeviceInfoRepository->getTotalRepairsPrice($proposal)
        );
        $proposal->setAllGrandTotalPrice($this->calculateGrandTotalPrice($proposal));

        $grandTotal = $this->calculateGrandTotal($proposal, $services);
        $proposal->setGrandTotal($grandTotal);
        $totalFee = $this->serviceCalculateTotals->calcTotalFee($services);
        $proposal->setServiceTotalFee($totalFee);
    }

    /**
     * @param Proposal $proposal
     * @return float|int
     */
    private function calculateTotalMunicAndProcFee(Proposal $proposal)
    {
        $total = 0;
        /** @var RepairDeviceInfo[] $repairDeviceInfoes */
        $repairDeviceInfoes = $this->repairDeviceInfoRepository->getAllByProposal($proposal);

        /** @var RepairDeviceInfo $repairDeviceInfo */
        foreach ($repairDeviceInfoes as $repairDeviceInfo) {
            $repairDeviceInfoTotalMunAndProcFee = $repairDeviceInfo->getSubtotalMunicAndProcFee();
            $total += $repairDeviceInfoTotalMunAndProcFee;
        }

        return $total;
    }

    /**
     * @param Proposal $proposal
     * @return bool
     */
    private function isProposalWithChannelWithFeesWillVary(Proposal $proposal)
    {
        /** @var RepairDeviceInfo[] $repairDeviceInfoes */
        $repairDeviceInfoes = $this->repairDeviceInfoRepository->getAllByProposal($proposal);

        /** @var RepairDeviceInfo $repairDeviceInfo */
        foreach ($repairDeviceInfoes as $repairDeviceInfo) {
            if ($repairDeviceInfo->getDepartmentChannelWithFeesWillVary()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Service $service
     * @param Proposal $proposal
     * @param Device $device
     */
    private function deleteServiceFromRepairProposalActions(Service $service, Proposal $proposal, Device $device)
    {
        if ($proposal->getIsFrozen()) {
            /** @var FrozenServiceForProposal $frozenService */
            $frozenService = $this->frozenServiceForProposalRepository->findRepairByServiceForProposal($service);

            $this->objectManager->remove($frozenService);
            $this->objectManager->flush();
        }

        /** @var RepairDeviceInfo $repairDeviceInfo */
        $repairDeviceInfo = $this->repairDeviceInfoRepository->findOneBy([
            'proposal' => $proposal,
            'device' => $device
        ]);
        //calculate device fees
        $this->calculateFeeService->calculateFee($repairDeviceInfo);
        $this->repairDeviceInfoManager->save($repairDeviceInfo);
    }

    /**
     * @param Proposal $proposal
     * @return int|string
     */
    private function calculateGrandTotalPrice(Proposal $proposal)
    {
        $res = 0;

        $res += $proposal->getAllTotalMunicAndProcFee() ?? 0;
        $res += $proposal->getAllTotalRepairsPrice() ?? 0;
        $res += $proposal->getAdditionalCost() ?? 0;
        $res += !empty($res) ? $proposal->getAfterHoursWorkCost() : 0;
        $res -= !empty($res) ? $proposal->getDiscount() : 0;

        return $res;
    }

    /**
     * @param Service $service
     */
    private function excludeFromProposal(Service $service)
    {
        /** @var Proposal $proposal */
        $proposal = $service->getProposal();
        /** @var ServiceFrequency $serviceFrequency */
        $serviceFrequency = $service->getNamed()->getFrequency();

        $opportunityType = 'repair';
        if ($serviceFrequency) {
            $opportunityType = 'retest';
        }

        $service->setProposal(null);
        $this->serviceManager->attachOpportunity($service, $opportunityType);

        $servicesCount = $proposal->getServicesCount();
        if ($servicesCount > 0) {
            $proposal->setServicesCount($servicesCount - 1);
        }
        $this->objectManager->persist($proposal);
    }

    /**
     * @param Proposal $proposal
     * @param $services
     * @return float|int
     */
    private function calculateGrandTotal(Proposal $proposal, $services)
    {
        $totalMunAndProcFee = 0;
        $servicesTotalFixedPrice = $this->serviceCalculateTotals->calcServicesFixedPrice($services);
        /** @var RepairDeviceInfo[] $repairDeviceInfoes */
        $repairDeviceInfoes = $this->repairDeviceInfoRepository->getAllByProposal($proposal);

        foreach ($repairDeviceInfoes as $repairDeviceInfo) {
            $munAndProcFee = $repairDeviceInfo->getSubtotalMunicAndProcFee();

            if (!$repairDeviceInfo->getDepartmentChannelWithFeesWillVary() and $munAndProcFee) {
                $totalMunAndProcFee += $munAndProcFee;
            }
        }

        $total = $totalMunAndProcFee + $servicesTotalFixedPrice;

        return $total;
    }
}
