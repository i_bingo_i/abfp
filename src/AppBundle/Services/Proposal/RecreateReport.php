<?php

namespace AppBundle\Services\Proposal;

use AppBundle\Entity\Proposal;
use AppBundle\Services\PDFService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RecreateReport
{
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var PDFService */
    private $pdfService;
    /** @var ObjectManager */
    private $objectManager;

    /**
     * RecreateReport constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer, ObjectManager $objectManager)
    {
        $this->serviceContainer = $serviceContainer;
        $this->objectManager = $objectManager;

        $this->pdfService = $serviceContainer->get('app.pdf_service');
    }

    /**
     * @param array $retestNotices
     * @throws \Twig\Error\Error
     */
    public function recreate(array $retestNotices)
    {
        /** @var Proposal $retestNotice */
        foreach ($retestNotices as $retestNotice) {
            $this->pdfService->removeProposalReport($retestNotice->getReport());
            $report = $this->pdfService->create($this->pdfService::ACTION_CREATE, $retestNotice);
            $retestNotice->setReport($report);
            $this->objectManager->persist($retestNotice);
        }

        $this->objectManager->flush();
    }
}
