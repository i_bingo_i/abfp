<?php

namespace AppBundle\Services\Proposal;

use AppBundle\Entity\Account;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\ServiceHistoryRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use Doctrine\Common\Persistence\ObjectManager;
use ServiceBundle\Services\CalculateTotals;
use ServiceBundle\Services\ServiceHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

use AppBundle\Traits\CollectionTrait;

class CalculateFee
{
    use CollectionTrait;

    /** @var ObjectManager */
    private $objectManager;

    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var MunicipalityRepository */
    private $municipalityRepository;
    /** @var Division */
    private $divisionService;
    /** @var ContainerInterface */
    private $container;
    /** @var DepartmentChannelManager */
    private $departmentChannelManager;
    /** @var ServiceHistoryRepository */
    private $serviceHistoryRepository;
    /** @var ServiceHelper */
    private $serviceHelperService;
    /** @var CalculateTotals */
    private $serviceCalculateTotals;

    /**
     * CalculateFee constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->municipalityRepository = $this->objectManager->getRepository('AppBundle:Municipality');
        $this->serviceHistoryRepository = $this->objectManager->getRepository('AppBundle:ServiceHistory');

        $this->departmentChannelManager = $this->container->get('app.department_channel.manager');
        $this->divisionService = $this->container->get('app.division.service');
        $this->serviceHelperService = $this->container->get('service.service_helper.service');
        $this->serviceCalculateTotals = $this->container->get('service.calculate_totals.service');
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     * @param null $services
     */
    public function calculateFee(RepairDeviceInfo $repairDeviceInfo, $services = null)
    {
        $services = $this->getRepairDeviceInfoServices($repairDeviceInfo, $services);
        $ifServicesHaveDepartmentChannelWithFeesWillVary =
            $this->serviceHelperService->checkIfServicesHaveDepartmentChannelWithFeesWillVary($services);

        if ($ifServicesHaveDepartmentChannelWithFeesWillVary) {
            $subtotalMunicAndProcFee = 0;
            $repairDeviceInfo->setDepartmentChannelWithFeesWillVary(true);
        } else {
            $subtotalMunicAndProcFee = $this->serviceCalculateTotals->calculateSubtotalMunicAndProcFee($services);
            $repairDeviceInfo->setDepartmentChannelWithFeesWillVary(false);
        }
        $repairDeviceInfo->setSubtotalMunicAndProcFee($subtotalMunicAndProcFee);
        $repairDeviceInfo->setDeviceTotal(
            $subtotalMunicAndProcFee + $repairDeviceInfo->getPrice() + $repairDeviceInfo->getInspectionPrice()
        );
    }

    /**
     * @param RepairDeviceInfo $repairDeviceInfo
     * @param null $services
     * @return Service[]|array|null
     */
    private function getRepairDeviceInfoServices(RepairDeviceInfo $repairDeviceInfo, $services = null)
    {
        if (!$services) {
            $services = $this->serviceRepository->findBy(
                ['proposal' => $repairDeviceInfo->getProposal(), 'device' => $repairDeviceInfo->getDevice()]
            );
        }

        return $services;
    }

    /**
     * @param $services
     * @param int $existingValue
     * @return float|int
     */
    public function calculateSubtotalInspectionsFee($services, $existingValue = 0)
    {
        $subtotalInspectionsFee = $existingValue;
        /** @var Service $service */
        foreach ($services as $service) {
            $subtotalInspectionsFee += $service->getFixedPrice();
        }

        return $subtotalInspectionsFee;
    }

    /**
     * @param $services
     * @param int $existingValue
     * @return float|int
     */
    public function calculateInspectionsEstimatedTime($services, $existingValue = 0)
    {
        $inspectionsEstimatedTime = $existingValue;
        /** @var Service $service */
        foreach ($services as $service) {
            if ($service->getContractorService()) {
                $inspectionsEstimatedTime += $service->getContractorService()->getEstimationTime();
            }
        }

        return $inspectionsEstimatedTime;
    }
}
