<?php

namespace AppBundle\Services\Proposal;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalStatus;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\EntityManager\RepairDeviceInfoManager;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\ProposalStatusRepository;
use AppBundle\Entity\Repository\RepairDeviceInfo as RepairDeviceInfoRepository;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Service;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Events\ProposalCloneEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Replace
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ProposalManager */
    private $proposalManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ProposalStatusRepository */
    private $proposalStatusRepository;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var RepairDeviceInfoRepository */
    private $repairDeviceInfoRepository;
    /** @var RepairDeviceInfoManager */
    private $repairDeviceInfoManager;
    /** @var CalculateFee */
    private $calculateFeeService;
    /** @var ProposalService */
    private $proposalService;
    /** @var AccountContactPersonRepository  */
    private $accountContactPersonRepository;

    /**
     * Replace constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer, ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $serviceContainer;
        $this->dispatcher = $this->serviceContainer->get('event_dispatcher');

        $this->proposalStatusRepository = $this->objectManager->getRepository('AppBundle:ProposalStatus');
        $this->repairDeviceInfoRepository = $this->objectManager->getRepository('AppBundle:RepairDeviceInfo');
        $this->accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
        $this->proposalManager = $this->serviceContainer->get("app.proposal.manager");
        $this->serviceManager = $this->serviceContainer->get("app.service.manager");
        $this->repairDeviceInfoManager = $this->serviceContainer->get("app.repair_device_info.manager");
        $this->calculateFeeService = $this->serviceContainer->get('app.proposal_calculate_fee.service');
        $this->proposalService = $this->serviceContainer->get('app.proposal_service.service');
    }

    /**
     * @param Proposal $proposal
     * @param null $comment
     * @return Proposal
     */
    public function replace(Proposal $proposal, $comment = null)
    {
        /** @var Proposal $clonedProposal */
        $clonedProposal = $this->cloneProposal($proposal);
        /** @var AccountContactPerson $recipient */
        $recipient = $this->accountContactPersonRepository->findAuthorizerByDivision(
            $proposal->getAccount(),
            $proposal->getDivision()
        );
        $clonedProposal->setRecipient($recipient);

        /** @var ProposalStatus $statusReplaced */
        $statusReplaced = $this->proposalStatusRepository->findOneBy(['alias' => 'replaced']);
        $proposal->setStatus($statusReplaced);
        $proposal->setClone($clonedProposal);

        $this->replaceServices($proposal, $clonedProposal);
        $this->proposalManager->save($proposal);

        $this->cloneRepairDeviceInfo($proposal);

        $this->proposalService->calculateAndSetAllProposalTotals($clonedProposal);

        /** @var ProposalCloneEvent $cloneEvent */
        $cloneEvent = new ProposalCloneEvent($proposal, $clonedProposal, $comment);
        $this->dispatcher->dispatch('proposal.clone', $cloneEvent);

        return $clonedProposal;
    }

    /**
     * @param Proposal $originalProposal
     * @return Proposal
     */
    private function cloneProposal(Proposal $originalProposal)
    {
        $clonedReferenceId = $this->proposalManager->generateReferenceId();
        $nowDateTime = new \DateTime();
        /** @var ProposalStatus $statusReplacing */
        $statusReplacing = $this->proposalStatusRepository->findOneBy(['alias' => 'replacing']);
        //cloned original Proposal and set new values for some fields of the cloned Proposal
        $clonedProposal = clone $originalProposal;
        $clonedProposal->setReport(null);
        $clonedProposal->setFrozen(null);
        $clonedProposal->setFirstSendingDate(null);
        $clonedProposal->setReferenceId($clonedReferenceId);
        $clonedProposal->setIsFrozen(false);
        $clonedProposal->setDateCreate($nowDateTime);
        $clonedProposal->setDateUpdate($nowDateTime);
        $clonedProposal->setStatus($statusReplacing);
        $clonedProposal->setCloned($originalProposal);
        $this->setNewRecipient($originalProposal, $clonedProposal);
        if ($originalProposal->getType()->getAlias() == 'repair') {
            $clonedProposal->setAdditionalComment($this->setAdditionalComments($originalProposal));
        }

        $this->proposalManager->save($clonedProposal);

        return $clonedProposal;
    }

    /**
     * @param Proposal $proposal
     */
    private function cloneRepairDeviceInfo(Proposal $proposal)
    {
        /** @var RepairDeviceInfo[] $repairDevicesInfo */
        $repairDevicesInfo = $this->repairDeviceInfoRepository->getAllByProposal($proposal);
        /** @var Proposal $clonedProposal */
        $clonedProposal = $proposal->getClone();

        /** @var RepairDeviceInfo $info */
        foreach ($repairDevicesInfo as $info) {
            $clonedInfo = clone $info;
            $clonedInfo->setProposal($clonedProposal);
            $this->calculateFeeService->calculateFee($clonedInfo);
            $this->repairDeviceInfoManager->save($clonedInfo);
        }
    }

    /**
     * @param Proposal $replace
     * @param Proposal $replaced
     */
    private function replaceServices(Proposal $replace, Proposal $replaced)
    {
        $services = $replace->getServices();

        if ($services) {
            /** @var Service $service */
            foreach ($services as $service) {
                $service = $service->setProposal($replaced);
                $this->serviceManager->update($service);
            }
        }
    }

    /**
     * @param Proposal $originalProposal
     * @return string
     */
    private function setAdditionalComments(Proposal $originalProposal)
    {
        /** @var string $result */
        $result = $originalProposal->getAdditionalComment() ? $originalProposal->getAdditionalComment() . ', ' : '';
        $replacingText = "This proposal replaces previous one with letter ID {$originalProposal->getReferenceId()}";

        return $result . $replacingText;
    }

    /**
     * @param Proposal $replace
     * @param Proposal $replaced
     */
    private function setNewRecipient(Proposal $replace, Proposal $replaced)
    {
        /** @var Account $account */
        $account = $replace->getAccount();
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $replace->getDivision();
        /** @var AccountContactPerson $accountContactPerson */
        $accountContactPerson =
            $this->accountContactPersonRepository->findAuthorizerByDivision($account, $deviceCategory);

        $replaced->setRecipient($accountContactPerson);
    }
}
