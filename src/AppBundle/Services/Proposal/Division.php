<?php

namespace AppBundle\Services\Proposal;

use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use Doctrine\Common\Persistence\ObjectManager;

class Division
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;

    /**
     * CalculateFee constructor.
     *
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        /** @var ServiceRepository serviceRepository */
        $this->deviceCategoryRepository = $this->objectManager->getRepository('AppBundle:DeviceCategory');
    }

    /**
     * @param DeviceCategory $deviceCategory
     * @return DeviceCategory
     */
    public function temporaryForAlarmAndPlumbingDivisions(DeviceCategory $deviceCategory)
    {
        $division = $deviceCategory;
        if ($division->getAlias() == 'alarm') {
            $division = $this->deviceCategoryRepository->findOneBy(['alias' => 'fire']);

        } elseif ($division->getAlias() == 'plumbing') {
            $division = $this->deviceCategoryRepository->findOneBy(['alias' => 'backflow']);
        }

        return $division;
    }
}
