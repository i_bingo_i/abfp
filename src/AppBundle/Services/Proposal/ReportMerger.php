<?php

namespace AppBundle\Services\Proposal;

use AppBundle\Entity\Proposal;
use AppBundle\Entity\User;
use PDFMerger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ReportMerger
{
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var TokenStorageInterface */
    private $securityTokenStorage;

    private $reportsPath;
    private $mergedReportsPath;
    private $kernel;

    /**
     * ReportMerger constructor.
     *
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;

        $this->kernel = $this->serviceContainer->get('kernel');
        $this->reportsPath = $this->kernel->getRootDir()."/../web/uploads/proposals/";
        $this->mergedReportsPath = $this->kernel->getRootDir()."/../web/uploads/merged_reports/";
        $this->securityTokenStorage = $this->serviceContainer->get('security.token_storage');
    }

    /**
     * @param array $proposals
     * @return string
     * @throws \exception
     */
    public function execution($proposals)
    {
        $proposals = array_chunk($proposals, 100);
        $pathCollection = [];

        /** @var array $proposal */
        foreach ($proposals as $proposal) {
            $pathCollection[] = $this->merge($proposal);
        }

        if (count($pathCollection) > 1) {
            $pdfName = $this->merge($pathCollection, false);
            $this->deleteTemporaryFiles($pathCollection);
        }

        return $pdfName ?? $pathCollection[0];
    }

    /**
     * @param array $items
     * @param bool $isArrayOfObjects
     * @return string
     * @throws \exception
     */
    public function merge($items, $isArrayOfObjects = true)
    {
        /** @var PDFMerger $pdfMerger */
        $pdfMerger = new PDFMerger();

        if ($isArrayOfObjects) {
            /** @var Proposal $proposal */
            foreach ($items as $proposal) {
                $fullReportPath = $this->reportsPath . $proposal->getReport();
                $pdfMerger->addPDF($fullReportPath, 'all');
            }

        } else {
            /** @var string $item */
            foreach ($items as $fileName) {
                $fullReportPath = $this->kernel->getRootDir() . '/../web' . $fileName;
                $pdfMerger->addPDF($fullReportPath, 'all');
            }

        }

        $mergedReportName = $this->generateReportName();
        $fullMergedReportPath = $this->mergedReportsPath . $mergedReportName;
        $pdfMerger->merge('file', $fullMergedReportPath);
        unset($pdfMerger);

        return "/uploads/merged_reports/" . $mergedReportName;
    }

    /**
     * @return string
     */
    private function generateReportName()
    {
        /** @var User $user */
        $user = $this->securityTokenStorage->getToken()->getUser();
        $userLabel = 'User' . $user->getId() . '_' . $user->getFirstName();
        if ($user->getLastName()) {
            $userLabel .= '_' . $user->getLastName();
        }

        $randLabel = rand(10, 200)."_".time();

        return $userLabel . '_merged_report_' . $randLabel . '.pdf';
    }

    /**
     * @param $pathCollection
     */
    private function deleteTemporaryFiles($pathCollection)
    {
        /** @var string $fileName */
        foreach ($pathCollection as $fileName) {
            $file = $this->kernel->getRootDir() . '/../web' . $fileName;
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }
}
