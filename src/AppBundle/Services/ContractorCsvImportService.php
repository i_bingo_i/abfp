<?php

namespace AppBundle\Services;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorType;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ContractorTypeRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use League\Csv\Reader;

class ContractorCsvImportService
{
    private CONST CSV_NEW_CONTRACTOR_NAME_ID=0;
    private CONST CSV_OLD_CONTRACTOR_ID=0;
    private CONST CSV_OLD_CONTRACTOR_NEW_NAME_ID=2;
    /** @var ContainerInterface */
    private $container;
    /** @var EntityManager */
    private $objectManager;
    /** @var ContractorRepository  */
    private $contractorRepository;
    /** @var ContractorTypeRepository */
    private $contractorTypeRepository;
    /** @var array */
    private $csvData = [];
    /**
     * ContractorCsvImportService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->objectManager = $this->container->get('app.entity_manager');
        $this->contractorRepository = $this->objectManager->getRepository("AppBundle:Contractor");
        $this->contractorTypeRepository = $this->objectManager->getRepository("AppBundle:ContractorType");
    }

    /**
     * @param $fileName
     */
    public function parsingData($fileName)
    {
        $reader = Reader::createFromPath(__DIR__."/../../../CsvImport/{$fileName}");
        $csvReadData = $reader->fetchAll();

        $this->csvData = [];
        for ($i = 0; $i < count($csvReadData); $i++) {
            $this->csvData[] = $csvReadData[$i];
        }
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function importNewContractors()
    {
        /** @var ContractorType $competitorType */
        $competitorType = $this->contractorTypeRepository->findOneBy(['alias' => 'competitor']);

        foreach ($this->csvData as $fileLine) {
            $duplicateContractor = $this->contractorRepository->findBy(["name" => $fileLine[self::CSV_NEW_CONTRACTOR_NAME_ID]]);
            if (!$duplicateContractor) {
                $newContractor = new Contractor();
                $newContractor->setName($fileLine[self::CSV_NEW_CONTRACTOR_NAME_ID]);
                $newContractor->setType($competitorType);
                $this->objectManager->persist($newContractor);
            }
        }

        $this->objectManager->flush();
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function importUpdateContractors()
    {
        foreach ($this->csvData as $fileLine) {
            /** @var Contractor $oldContractor */
            $oldContractor = $this->contractorRepository->find($fileLine[self::CSV_OLD_CONTRACTOR_ID]);
            if ($oldContractor) {
                $oldContractor->setName($fileLine[self::CSV_OLD_CONTRACTOR_NEW_NAME_ID]);
                $this->objectManager->persist($oldContractor);
            }
        }
        $this->objectManager->flush();
    }
}