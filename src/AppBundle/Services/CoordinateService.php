<?php

namespace AppBundle\Services;

use AppBundle\Entity\Coordinate;
use AppBundle\Entity\Repository\CoordinateRepository;
use Doctrine\ORM\EntityManager;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CoordinateService
{
    /** @var EntityManager */
    private $entityManager;
    /** @var ContainerInterface */
    private $container;
    /** @var CoordinateRepository */
    private $coordinateRepository;

    /**
     * CoordinateService constructor.
     * @param EntityManager $entityManager
     * @param ContainerInterface $container
     */
    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->coordinateRepository = $this->entityManager->getRepository('AppBundle:Coordinate');
    }

    /**
     * @param string $latitude
     * @param string $longitude
     * @return Coordinate
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function creating($latitude, $longitude)
    {
        $coordinate = new Coordinate();
        $coordinate->setLatitude($latitude);
        $coordinate->setLongitude($longitude);

        $this->save($coordinate);

        return $coordinate;
    }

    /**
     * @param Coordinate $coordinate
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Coordinate $coordinate)
    {
        $this->entityManager->persist($coordinate);
        $this->entityManager->flush();
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return Coordinate|null|object
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getCoordinate($latitude = 0, $longitude = 0)
    {
        $coordinate = $this->coordinateRepository->findOneBy(['latitude' => $latitude, 'longitude' => $longitude]);

        if (empty($coordinate)) {
            $coordinate = $this->creating($latitude, $longitude);
        }

        return $coordinate;
    }
}