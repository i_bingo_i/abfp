<?php

namespace AppBundle\Services;

use AppBundle\Entity\Event;
use AppBundle\Entity\Repository\EventRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\WorkorderDeviceInfo as WorkorderDeviceInfoRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\User;
use AppBundle\Entity\WorkorderDeviceInfo;
use Doctrine\Common\Persistence\ObjectManager;
use Fungio\OutlookCalendarBundle\Service\OutlookCalendar;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class TechnicianAssignmentSyncService
{
    /** @var ContainerInterface */
    private $container;
    /** @var OutlookCalendar */
    private $outlookCalendar;
    /** @var Session */
    private $session;
    /** @var ObjectManager */
    private $objectManager;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var object|TokenStorage */
    private $tokenStorage;
    /** @var Device\DynamicFieldsService */
    private $dynamicFieldsService;
    /** @var WorkorderDeviceInfoRepository */
    private $workorderDeviceInfoRepository;

    /**
     * TechnicianAssignmentSyncService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->outlookCalendar = $this->container->get('fungio.outlook_calendar');
        $this->session = $this->container->get('session');
        $this->objectManager = $this->container->get('app.entity_manager');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->tokenStorage = $this->container->get('security.token_storage');
        $this->dynamicFieldsService = $this->container->get('app.dynamic_fields.service');
        $this->workorderDeviceInfoRepository = $this->objectManager->getRepository('AppBundle:WorkorderDeviceInfo');
    }

    /**
     * @param string $accessToken
     * @param \DateTime $dateTime
     * @param string|null $calendarId
     * @return array|mixed
     */
    public function getEvents(string $accessToken, \DateTime $dateTime, ?string $calendarId = null)
    {
        $events = $this->outlookCalendar->getEventsForDate($accessToken, $dateTime, $calendarId);
        return $events;
    }

    /**
     * @param string $accessToken
     * @param Event $event
     * @return array|mixed
     * @throws \Exception
     */
    public function setEvent(string $accessToken, Event $event)
    {
        $messageClauses = $this->formOutlookCalendarMessage($event);

        //subject argument
        $subject = $messageClauses['subject'];

        //content argument
        $content = $messageClauses['content'];

        //start time and end time arguments
        $dateFormat = 'Y-m-d\TH:i:s\Z';
        $fromDate = $event->getFromDate();
        $toDate = $event->getToDate();
        $dateTimeZone = new \DateTimeZone('America/Chicago');
        $startTime = \DateTime::createFromFormat($dateFormat, $fromDate->format($dateFormat), $dateTimeZone);
        $endTime = \DateTime::createFromFormat($dateFormat, $toDate->format($dateFormat), $dateTimeZone);

        //location argument
        $location = $messageClauses['location'];

        //add Event to Microsoft Outlook Calendar
        $eventId = $this->outlookCalendar->addEventToCalendar(
            $accessToken,
            $subject,
            $content,
            $startTime,
            $endTime,
            '',
            $location
        );

        return $eventId;
    }

    /**
     * @param string $accessToken
     * @param string $ownerEmail
     * @return string
     */
    public function getMicrosoftCalendarIDByOwnerEmail(string $accessToken, string $ownerEmail)
    {
        $calendars = $this->outlookCalendar->getCalendars($accessToken);
        $calendarId = '';
        foreach ($calendars['value'] as $calendar) {
            if ($calendar['Owner']['Address'] == $ownerEmail) {
                $calendarId .= $calendar['Id'];
            }
        }

        return $calendarId;
    }

    public function addEventToAnotherCalendar(
        $access_token,
        $subject,
        $content,
        \DateTime $startTime,
        \DateTime $endTime,
        $attendeeString = "",
        $location = "",
        $allDay = false,
        $calendarId = ''
    ) {
        $startTime->setTimeZone(new \DateTimeZone("UTC"));
        $endTime->setTimeZone(new \DateTimeZone("UTC"));
        if ($allDay) {
            $startTime = clone $startTime;
            $endTime->setTime(0, 0, 0);

            $endTime = clone $startTime;
            $endTime->modify('+1 day');
        }

        $tz = $startTime->getTimezone();
        // Generate the JSON payload
        $event = [
            "Subject" => $subject,
            "Start"   => [
                "DateTime" => $startTime->format('Y-m-d\TH:i:s\Z'),
                "TimeZone" => $tz->getName()
            ],
            "End"     => [
                "DateTime" => $endTime->format('Y-m-d\TH:i:s\Z'),
                "TimeZone" => $tz->getName()
            ],
            "Body"    => [
                "ContentType" => "HTML",
                "Content"     => $content
            ]
        ];
        if ($location != "") {
            $event['Location'] = [
                "DisplayName" => $location
            ];
        }

        $attendeeAddresses = $attendeeString;
        if (!is_array($attendeeAddresses)) {
            $attendeeAddresses = array_filter(explode(';', $attendeeString));
        }
        if (count($attendeeAddresses)) {
            $attendees = [];
            foreach ($attendeeAddresses as $address) {
                if ($address != "") {
                    $attendee = [
                        "EmailAddress" => [
                            "Address" => $address
                        ],
                        "Type"         => "Required"
                    ];

                    $attendees[] = $attendee;
                }
            }

            $event["Attendees"] = $attendees;
        }

        $eventPayload = json_encode($event);

        $createEventUrl ="https://outlook.office.com/api/v2.0/me/".((empty($calendarId)) ? 'events' : ('calendars/'.$calendarId.'/events'));

        $response = $this->outlookCalendar->makeApiCall($access_token, "POST", $createEventUrl, $eventPayload);

        if (isset($response['Id'])) {
            return $response['Id'];
        } else {
            return $response;
        }
    }

    public function setAnotherCalendarEvent(string $accessToken, string $calendarId, Event $event)
    {
        $messageClauses = $this->formOutlookCalendarMessage($event);

        //subject argument
        $subject = $messageClauses['subject'];

        //content argument
        $content = $messageClauses['content'];

        //start time and end time arguments
        $dateFormat = 'Y-m-d\TH:i:s\Z';
        $fromDate = $event->getFromDate();
        $toDate = $event->getToDate();
        $dateTimeZone = new \DateTimeZone('America/Chicago');
        $startTime = \DateTime::createFromFormat($dateFormat, $fromDate->format($dateFormat), $dateTimeZone);
        $endTime = \DateTime::createFromFormat($dateFormat, $toDate->format($dateFormat), $dateTimeZone);

        //location argument
        $location = $messageClauses['location'];

        //add Event to Microsoft Outlook Calendar
        $eventId = $this->addEventToAnotherCalendar(
            $accessToken,
            $subject,
            $content,
            $startTime,
            $endTime,
            '',
            $location,
            false,
            $calendarId
        );

        return $eventId;
    }

    /**
     * @param string $accessToken
     * @param string $eventId
     * @return array|mixed
     * @throws \Exception
     */
    public function deleteEvent(string $accessToken, string $eventId)
    {
        $result = $this->outlookCalendar->makeApiCall(
            $accessToken,
            'DELETE',
            'https://outlook.office.com/api/v2.0/me/events/'.$eventId
        );

        if (!empty($result['errorNumber'])) {
            throw new \Exception('Error #'.$result['errorNumber'].' '.$result['error']);
        }

        return $result;
    }

    /**
     * Pushes Event to MS Outlook Calendar deleted from it before, but NOT deleted from Project database
     *
     * @param string $accessToken
     * @param \DateTime $date
     * @param string|null $calendarId
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function pushEventsToMSOutlookCalendar(string $accessToken, \DateTime $date, ?string $calendarId = null)
    {
        //select events from DB of Project
        /** @var EventRepository $eventRepository */
        $eventRepository = $this->objectManager->getRepository('AppBundle:Event');
        $eventsFromDB = $eventRepository->getEventsOnDate($date);

        //select Events from Microsoft Outlook Calendar
        $eventsFromMSOutlookCalendar = $this->getEvents($accessToken, $date, $calendarId);
        $msOutlookCalendarEventIds = [];
        foreach ($eventsFromMSOutlookCalendar['value'] as $key => $eventFromMSOutlookCalendar) {
            $msOutlookCalendarEventIds[$key] = $eventFromMSOutlookCalendar['Id'];
        }

        /** @var Event $eventFromDB */
        foreach ($eventsFromDB as $eventFromDB) {
            if (!in_array($eventFromDB->getMsOutlookCalendarEventId(), $msOutlookCalendarEventIds)) {
                $resetEventId = (empty($calendarId)) ?
                    $this->setEvent($accessToken, $eventFromDB) :
                    $this->setAnotherCalendarEvent($accessToken, $calendarId, $eventFromDB);
                $eventFromDB->setMsOutlookCalendarEventId($resetEventId);
                $this->objectManager->persist($eventFromDB);
            }
        }
        $this->objectManager->flush();
    }

    public function logout()
    {
        //$session = new Session();
        if ($this->session->has('fungio_outlook_calendar_access_token')) {
            $this->session->remove('fungio_outlook_calendar_access_token');
        }
        $this->session->save();
    }

    public function getOutlookCalendarInstance()
    {
        return $this->outlookCalendar;
    }

    /**
     * @param Event $event
     * @return array
     */
    private function formOutlookCalendarMessage(Event $event)
    {
        //retrieve needed fields
        $workOrder = $event->getWorkorder(); //Workorder from Event
        $workOrderId = $workOrder->getId(); //Workorder ID
        $account = $workOrder->getAccount(); //Account from Workorder
        $accountName = $account->getName(); //name of Account (will be used more, than 1 time)
        $accountAddress = $account->getAddress(); //Address entity related to Account
        $address = $accountAddress->getAddress(); //address field (will be used more, than 1 time)
        $city = $accountAddress->getCity(); //city field (will be used more, than 1 time)
        $addressState = $accountAddress->getState(); //State entity related to Address
        $state = $addressState->getCode(); //code field of State entity (will be used more, than 1 time)
        $zip = $accountAddress->getZip(); //zip field of Address entity (will be used more, than 1 time)
        $strAddress = $address.', '.$city.', '.$state.', '.$zip; //address as a string (will be used more, than 1 time)
        $addressForGoogle = str_replace(' ', '+', $strAddress);
        $googleMapsHref = 'https://www.google.com/maps/place/'.$addressForGoogle;
        $woServices = $workOrder->getServices();
        $woAdditionalComments = $workOrder->getAdditionalComments();
        $woInternalComments = $workOrder->getInternalComment();
//        $woServicesAndDevices = $this->serviceRepository->getWorkorderServicesAndDevices($workOrder);
        /** @var User $userLoggedIn */
        $userLoggedIn = $this->tokenStorage->getToken()->getUser();
        $woDirections = $workOrder->getDirections();
        //Event scheduled time
        $fromDate = $event->getFromDate();
        $toDate = $event->getToDate();
        $strFromTime = ($fromDate->format('i') == '00') ? $fromDate->format('g') : $fromDate->format('g:i');
        $strToTime = ($toDate->format('i') == '00') ?  $toDate->format('g') : $toDate->format('g:i');
        $scheduledWOTime = $strFromTime.'-'.$strToTime;
        $timeCriticalSigns = ($event->getIsTimeCritical()) ? '! ' : '';

        //subject
        $subject = $timeCriticalSigns.$scheduledWOTime.' - '.$city.' - '.$accountName.' - '.$workOrder->getDivision()->getName()." WO #".$workOrderId;

        //location
        $location = $strAddress;

        //content
        $content = 'Workorder #'.$workOrderId."<br><br>";
        $content .= 'Account Name: '.$accountName.' (Account ID: '.$account->getId().")<br>";
        $content .= 'Account Address: '.$strAddress."<br><br>";

        if (!$woServices->isEmpty()) {
            $result = [];
            $content .= "Included services:<br>";
            /** @var Service $service */
            foreach ($woServices as $service) {
                $woDeviceInfo = $this->workorderDeviceInfoRepository->getOneWithCommentByDeviceAndWo(
                    $workOrder,
                    $service->getDevice()
                );
                $dynamicFields = $this->dynamicFieldsService->getDynamicFields($service->getDevice());
                if (empty($dynamicFields)) {
                    $result[$service->getDevice()->getId()]['device'] = $service->getDevice()->getTitle() . '<br>';

                } else {
                    $result[$service->getDevice()->getId()]['device'] = $service->getDevice()->getTitle() .
                        ' - ' . $dynamicFields . '<br>';
                }

                if(!empty($service->getDevice()->getNoteToTester())) {
                    $result[$service->getDevice()->getId()]['device'] .= "<span style='color:#d04437'>Test Notes: {$service->getDevice()->getNoteToTester()}</span><br><br>";
                }

                if (!isset($result[$service->getDevice()->getId()]['repairComment']) and $woDeviceInfo instanceof WorkorderDeviceInfo) {
                    $result[$service->getDevice()->getId()]['repairComment'] = $woDeviceInfo->getRepairComments();
                }

                $result[$service->getDevice()->getId()]['deviceComment'] = $service->getDevice()->getComments();

                if ($service->getNamed()->getFrequency()) {
                    $result[$service->getDevice()->getId()]['services'][] = $service->getNamed()->getName() . '<br>';

                } elseif ($service->getNamed()->getDeficiency()) {
                    $result[$service->getDevice()->getId()]['services'][] = $service->getNamed()->getName() . ' - '
                        . $service->getNamed()->getDeficiency() .'<br>';
                }
            }

            foreach ($result as $item) {
                $content .= $item['device'];

                foreach ($item['services'] as $oneService) {
                    $content .= $oneService;
                }

                if (!empty($item['repairComment'])) {
                    $content .= "General repair - Please see comments" . '<br>';
                    $content .= "Repair comment: " . $item['repairComment'];
                }

                $content .= '<br>';
            }
        }

        $content .= "Map: <a href='$googleMapsHref'>$googleMapsHref</a><br><br>";
        $content .= 'Scheduled by: '.$userLoggedIn->getFirstName().' '.$userLoggedIn->getLastName()."<br><br>";
        $content .= (!empty($woDirections)) ? 'WO Directions: '.$woDirections . "<br><br>" : '';
        $content .= ($woAdditionalComments) ? 'WO Additional Comments: ' . $woAdditionalComments . "<br><br>" : '';
        $content .= ($woInternalComments) ? 'WO Internal Comments: ' . $woInternalComments : '';

        return [
            'subject' => $subject,
            'content' => $content,
            'location' => $location,
        ];
    }
}
