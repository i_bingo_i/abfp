<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Images
{
    private $rootPath = __DIR__ . "/../../../web";

    /**
     * Images constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param UploadedFile $file
     * @param null $prefix
     * @return string
     */
    public function getRandomFileName(UploadedFile $file, $prefix = null)
    {
        $extension = $file->guessExtension();
        if (!$extension) {
            $extension = 'bin';
        }

        if ($prefix) {
            return $prefix . '_' . time() . '_' . rand(1, 99999) . '.' . $extension;
        }

        return time() . '_' . rand(1, 99999) . '.' . $extension;
    }
    /**
     * @param $imagePath
     * @param $imageName
     *
     * @return BinaryFileResponse
     */
    public function responseImage($imagePath, $imageName)
    {
        $response = new BinaryFileResponse($imagePath);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $imageName,
            iconv('UTF-8', 'ASCII//TRANSLIT', $imageName)
        );

        return $response;
    }

    /**
     * @param UploadedFile $image
     * @param $savePath
     *
     * @return bool|string
     */
    public function saveUploadImage(UploadedFile $image, $savePath)
    {
        if ($image) {
            $fullSavePath = $this->rootPath . $savePath;
            $randomFileName = $this->getRandomFileName($image);
            $image->move($fullSavePath, $randomFileName);
            chmod($fullSavePath."/".$randomFileName, 0777);

            return $randomFileName;
        }

        return false;
    }

    /**
     * @param UploadedFile $image
     * @param string $savePath
     * @param string $imageName
     * @return string
     */
    public function saveUploadCustomInvoice(UploadedFile $image, string $savePath, string $imageName)
    {
        $fullSavePath = $this->rootPath . $savePath;
        $image->move($fullSavePath, $imageName);
        chmod($fullSavePath."/".$imageName, 0777);

        return $imageName;
    }

    /**
     * @param UploadedFile $file
     * @param $savePath
     * @return bool|string
     */
    public function saveUploadFile(UploadedFile $file, $savePath)
    {
        if(!is_dir($this->rootPath .$savePath)) mkdir($this->rootPath .$savePath);

        return $this->saveUploadImage($file, $savePath);
    }
}
