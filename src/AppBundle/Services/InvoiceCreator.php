<?php

namespace AppBundle\Services;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Manager\InvoiceManager;
use AppBundle\Entity\Workorder;
use AppBundle\Factories\InvoiceFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InvoiceCreator
{
    /** @var ContainerInterface  */
    private $container;
    /** @var InvoiceManager */
    private $invoiceManager;

    /**
     * InvoiceCreator constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->invoiceManager = $this->container->get('invoice.manager');
    }

    /**
     * @param Workorder $workorder
     * @param string $file
     * @return Invoices
     */
    public function create(Workorder $workorder, string $file)
    {
        $invoice = InvoiceFactory::made($workorder, $file);
        $this->invoiceManager->save($invoice);

        return $invoice;
    }
}
