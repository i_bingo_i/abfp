<?php

namespace AppBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class Helper
{
    /** @var ObjectManager  */
    private $om;
    /** @var ContainerInterface  */
    private $container;
    /** @var object|Session  */
    private $session;

    /**
     * Helper constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;

        $this->session = $this->container->get('session');
    }

    /**
     * @param string $sessionName
     */
    public function removeSessionByCondition(string $sessionName)
    {
        if ($this->session->has($sessionName)) {
            $this->session->remove($sessionName);
        }
    }

    /**
     * @param array $delimiters
     * @param string $string
     * @return array
     */
    public function multiExplode(array $delimiters, string $string)
    {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
}
