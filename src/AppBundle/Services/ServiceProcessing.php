<?php

namespace AppBundle\Services;

use AppBundle\Entity\EntityManager\ProcessingStatsManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\ServiceStatusRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceStatus;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ServiceProcessing
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var ServiceStatusRepository */
    private $serviceStatusRepository;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var ProcessingStatsManager */
    private $processingStatsManager;
    /** @var ServiceManager */
    private $serviceManager;

    /**
     * ServiceProcessing constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->serviceStatusRepository = $this->objectManager->getRepository('AppBundle:ServiceStatus');
        $this->serviceRepository = $this->objectManager->getRepository('AppBundle:Service');
        $this->processingStatsManager = $this->container->get('app.processing_stats.manager');
        $this->serviceManager = $this->container->get('app.service.manager');
    }

    /**
     *
     */
    public function retestOpportunity()
    {
        $statuses = [];

        $statuses[] = $this->serviceStatusRepository->findOneBy(
            [ 'alias' => $this->serviceManager::STATUS_RETEST_OPPORTUNITY ]
        )->getId();
        $statuses[] = $this->serviceStatusRepository->findOneBy(
            ['alias' => $this->serviceManager::STATUS_UNDER_RETEST_NOTICE]
        )->getId();

        $services = $this->serviceRepository->getRetestOpportunity($statuses);

        if (!empty($services)) {
            $success = 0;
            /** @var Service $proposal */
            foreach ($services as $service) {
                $this->serviceManager->setStatus($service, $this->serviceManager::STATUS_RETEST_OPPORTUNITY);
                $this->serviceManager->update($service);
                $success++;
            }
            $this->processingStatsManager->createMessageByAlias(
                "Success finish in Retest Opportunity check. Count of Services: " .
                count($services).". Success processing: " . $success
            );
        } else {
            $this->processingStatsManager
                ->createMessageByAlias("Nothing to do in Retest Opportunity check. No Services for processing.");
        }
    }
}