<?php

namespace AppBundle\Services\AccountContactPersonHistory;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\AddressHistory;
use AppBundle\Entity\ContactPersonHistory;
use AppBundle\Entity\EntityManager\AccountContactPersonHistoryManager;
use AppBundle\Entity\User;
use AppBundle\Factories\AccountContactPersonHistoryFactory;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Repository\AddressHistoryRepository;
use AppBundle\Entity\Repository\ContactPersonHistoryRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Creator
{
    /** @var ContainerInterface */
    private $container;
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContactPersonHistoryRepository */
    private $contactPersonHistoryRepository;
    /** @var AddressHistoryRepository */
    private $addressHistoryRepository;
    /** @var TokenStorage */
    private $tokenStorage;
    /** @var AccountContactPersonHistoryManager */
    private $accountContactPersonHistoryManager;

    /**
     * Creator constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     * @param TokenStorage $tokenStorage
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container, TokenStorage $tokenStorage)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;

        $this->contactPersonHistoryRepository = $this->objectManager->getRepository('AppBundle:ContactPersonHistory');
        $this->addressHistoryRepository = $this->objectManager->getRepository('AppBundle:AddressHistory');
        $this->accountContactPersonHistoryManager = $this->container->get('app.account_contact_person_history.manager');
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @param int $flushFlag
     */
    public function create(AccountContactPerson $accountContactPerson, $flushFlag = 1)
    {
        $token = $this->tokenStorage->getToken();
        $user = null;
        if (!empty($token) and gettype($token->getUser()) != "string") {
            /** @var User $user */
            $user = $token->getUser();
        }
        /** @var ContactPersonHistory $contactPersonHistory */
        $contactPersonHistory = $this->contactPersonHistoryRepository->findOneBy(
            ['ownerEntity' => $accountContactPerson->getContactPerson()],
            ['id' => 'DESC']
        );

        $lastAddressHistory = null;
        if ($accountContactPerson->getSendingAddress()) {
            /** @var AddressHistory $lastAddressHistory */
            $lastAddressHistory =
                $this->addressHistoryRepository->getLastRecord($accountContactPerson->getSendingAddress());
        }

        $acpHistory = AccountContactPersonHistoryFactory::make(
            $accountContactPerson,
            $contactPersonHistory,
            $lastAddressHistory,
            $user
        );

        $this->objectManager->persist($acpHistory);
        if ($flushFlag) {
            $this->accountContactPersonHistoryManager->save($acpHistory);
        }
    }
}
