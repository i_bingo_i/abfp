<?php

namespace AppBundle\Services;

use AppBundle\Entity\ContractorCalendar;
use AppBundle\Entity\Repository\ContractorCalendarRepository;
use AppBundle\Entity\Workorder;
use Doctrine\Common\Persistence\ObjectManager;
use Fungio\OutlookCalendarBundle\Service\OutlookCalendar;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;
use \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class MSCalendarToken
{
    /** @var ContainerInterface */
    private $container;
    /** @var OutlookCalendar */
    private $outlookCalendar;
    /** @var ObjectManager */
    private $objectManager;
    /** @var object|TokenStorage */
    private $tokenStorage;
    /** @var Router */
    private $router;
    /** @var Session */
    private $session;
    /** @var ContractorCalendarRepository */
    private $contractorCalendarRepository;

    /**
     * MSCalendarToken constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->outlookCalendar = $this->container->get('fungio.outlook_calendar');
        $this->objectManager = $this->container->get('app.entity_manager');
        $this->tokenStorage = $this->container->get('security.token_storage');
        $this->router = $this->container->get('router');
        $this->session = $this->container->get('session');

        $this->contractorCalendarRepository = $this->objectManager->getRepository('AppBundle:ContractorCalendar');
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        $redirectUri = $this->router->generate('admin_ms_calendar_sign_in', [], UrlGeneratorInterface::ABSOLUTE_URL);

        return $this->outlookCalendar->getLoginUrl($redirectUri);
    }

    /**
     * @param string $accessToken
     * @return bool
     */
    public function checkAccessToken(string $accessToken)
    {
        return $this->outlookCalendar->isConnected($accessToken);
    }

    /**
     * @param null $code
     * @return bool|mixed
     */
    public function refresh($code = null)
    {
        /** @var ContractorCalendar $contractorCalendar */
        $contractorCalendar = $this->findMyCompanyCalendar();
        $redirectUri = $this->router->generate('admin_ms_calendar_sign_in', [], UrlGeneratorInterface::ABSOLUTE_URL);

        if ($code) {
            return $this->refreshByCode($contractorCalendar, $code, $redirectUri);
        }

        if (!empty($contractorCalendar->getRefreshToken())) {
            try {
                return $this->refreshByRefreshToken($contractorCalendar, $redirectUri);
            } catch (\Exception $exception) {
                return false;
            }
        }

        return false;
    }

    /**
     * @param $redirectUri
     * @return bool|mixed|string
     */
    public function getAccessToken($redirectUri)
    {
        /** @var ContractorCalendar $contractorCalendar */
        $contractorCalendar = $this->findMyCompanyCalendar();

        $accessToken = $contractorCalendar->getAccessToken();
        if (!$accessToken or !$this->checkAccessToken($accessToken)) {
            $this->session->set('redirect_uri', $redirectUri);
            $accessToken = $this->refresh();
        }

        return $accessToken;
    }

    /**
     * @param Workorder $workorder
     * @return bool|mixed|string
     */
    public function getOrRefreshAccessTokenForWo(Workorder $workorder)
    {
        $redirectUri = $this->router->generate(
            'admin_assign_technician_workorder',
            ['workorder' => $workorder->getId()]
        );
        /** @var ContractorCalendar $contractorCalendar */
        $contractorCalendar = $this->findMyCompanyCalendar();

        $accessToken = $contractorCalendar->getAccessToken();
        if (!$accessToken or !$this->checkAccessToken($accessToken)) {
            $this->session->set('redirect_uri', $redirectUri);
            $accessToken = $this->refresh();
        }

        return $accessToken;
    }

    /**
     * @param ContractorCalendar $contractorCalendar
     * @param $code
     * @param $redirectUri
     * @return mixed
     */
    private function refreshByCode(ContractorCalendar $contractorCalendar, $code, $redirectUri)
    {
        $token = $this->outlookCalendar->getTokenFromAuthCode($code, $redirectUri);
        $this->updateToken($contractorCalendar, $token);

        return $token['access_token'];
    }

    /**
     * @param ContractorCalendar $contractorCalendar
     * @param $redirectUri
     * @return mixed
     */
    private function refreshByRefreshToken(ContractorCalendar $contractorCalendar, $redirectUri)
    {
        $token = $this->outlookCalendar->getTokenFromRefreshToken($contractorCalendar->getRefreshToken(), $redirectUri);
        $this->updateToken($contractorCalendar, $token);

        return $token['access_token'];
    }

    /**
     * @param ContractorCalendar $contractorCalendar
     * @param $token
     */
    private function updateToken(ContractorCalendar $contractorCalendar, $token)
    {
        $contractorCalendar->setAccessToken($token['access_token']);
        $contractorCalendar->setRefreshToken($token['refresh_token']);
        $this->objectManager->persist($contractorCalendar);
        $this->objectManager->flush();
    }

    /**
     * @return ContractorCalendar
     */
    private function findMyCompanyCalendar()
    {
        /** @var ContractorCalendar $contractorCalendar */
        $contractorCalendar = $this->contractorCalendarRepository->findForMyCompany();

        return $contractorCalendar;
    }
}
