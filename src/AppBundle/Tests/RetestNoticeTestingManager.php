<?php

namespace AppBundle\Tests;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Bundle\FrameworkBundle\Client;

class RetestNoticeTestingManager extends FunctionalTestCase
{
    /**
     * Adds ContactPerson to selected Account to be able to create Retest Notice automatically
     *
     * @param Crawler $crawler
     * @param Client $client
     * @return Crawler
     */
    protected function addContactPersonToSelectedAccount(Crawler $crawler, Client $client) : Crawler
    {
        //go to View and manage all Account contacts page
        $viewAccountContactsLink = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a')->link();
        $crawler = $client->click($viewAccountContactsLink);

        //select "Add Contact Person" link and click it
        $addContactPersonLink = $crawler->selectLink('Add Contact Person')->link();
        $crawler = $client->click($addContactPersonLink);

        //check, that we are on the page of adding ContactPerson
        $mainPageTitleDivObject = $crawler->filter('.app-main__page-title.app-wrapper');
        $this->assertContains(
            'Link Contact person to',
            $mainPageTitleDivObject->filter('div')->eq(1)->text()
        );

        //select ContactPerson search form, input search phrase and submit it
        $searchContactPersonForm = $crawler->filter('#searchForm')->form();
        $searchContactPersonForm['searchPhrase'] = 'Petyr Bealish';
        $crawler = $client->submit($searchContactPersonForm);

        //get search result from table of search results
        $searchResultRowObject = $crawler->filter('.app-data .app-data__table tbody tr')->last();

        //get ContactPerson name to check, that we will be on the requested page further
        $selectedAccountContactPersonName = $searchResultRowObject->filter('td')->eq(2)->filter('a')->text();

        //select link for selecting ContactPerson to add to an Account and click it
        $selectContactPersonLink = $searchResultRowObject->filter('td')->last()->filter('a')->link();
        $crawler = $client->click($selectContactPersonLink);

        //check, that we are on the page for changing roles of the selected ContactPerson
        $changeRolesMainPageTitleNameDivObject = $crawler->filter('.app-main__page-title-name');
        $this->assertContains(
            $selectedAccountContactPersonName,
            $changeRolesMainPageTitleNameDivObject->text()
        );

        //select form for changing ContactPerson roles and submit it
        $changeContactPersonRolesForm = $crawler->filter('#roleForm')->form();
        $client->submit($changeContactPersonRolesForm);

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //return Crawler, which crawls by requested page
        return $crawler;
    }

    /**
     * Adds Service to Device to be able to create Opportunity automatically
     *
     * @param Crawler $crawler
     * @param Client $client
     * @return Crawler
     */
    protected function addServiceToDevice(Crawler $crawler, Client $client) : Crawler
    {
        //select add service link and click it
        $addServiceLink = $crawler->selectLink('Add Service')->link();
        $crawler = $client->click($addServiceLink);

        //assert, that we are on the page for adding new service
        $this->assertContains(
            'Service Information',
            $crawler->filter('.app-form-header')->text()
        );

        //select form for adding service, fullize required fields and "Service Fixed Fee" field and click it
        $addServiceForm = $crawler->selectButton('CREATE')->form();
        $addServiceForm['admin_bundle_service[named]'] = '2';
        $addServiceForm['admin_bundle_service[inspectionDue]'] = date('m/d/Y');
        $addServiceForm['admin_bundle_service[fixedPrice]'] = '$160';
        $client->submit($addServiceForm);
        $crawler = $client->followRedirect();

        //go to the page of Account linked to Device
        $accountLink = $crawler->filter('.app-device-styles__after-title-block_styling-link a')->link();
        $accountLinkText = $accountLink->getNode()->textContent;
        $crawler = $client->click($accountLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            $accountLinkText,
            $crawler->filter('.app-detailed__info__data-title')->text()
        );

        //return Crawler, which crawls by the Account page
        return $crawler;
    }
}
