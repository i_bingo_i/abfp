<?php

namespace AppBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Link;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Tests\TestTrait\AuthorizedClientTrait;

class BehaviorTestCase extends WebTestCase
{
    use AuthorizedClientTrait;

    /** @var Client */
    private $client;
    /** @var Crawler */
    private $crawler;

    /**
     * @param $email
     */
    protected function logIn($email)
    {
        $this->client = $this->createAuthorizedClient($email);
    }

    /**
     * @param $url
     */
    protected function goToPage($url)
    {
        $this->crawler = $this->client->request('GET', $url);
    }

    /**
     * @param $text
     */
    protected function see($text)
    {
        /** @var Response $response */
        $response = $this->client->getResponse();
        $this->assertContains($text, $response->getContent());
    }

    /**
     * @param $text
     */
    protected function seeLinkButton($text)
    {
        $appWrapper = $this->crawler->filter('.app-wrapper')->eq(0);
        $buttons = $appWrapper->filter('.btn');
        $buttonsText = $buttons->each(function (Crawler $node, $i) {
            return '[' . $node->text() . ']';
        });
        $buttonsText = 'Buttons: ' . implode(', ', $buttonsText);

        $this->assertContains($text, $buttonsText);
    }

    /**
     * @param $buttonText
     */
    protected function clickLinkButton($buttonText)
    {
        /** @var Link $link */
        $link = $this->crawler
            ->filter('.btn:contains("' . $buttonText . '")')
            ->eq(0)
            ->link()
        ;

        $this->crawler = $this->client->click($link);

        $attributeDataToggle = $link->getNode()->getAttribute('data-toggle');
        if ($attributeDataToggle === 'modal') {
            $this->showPopup($link);
        }
    }

    /**
     * @param $text
     */
    protected function seeConfirmPopup($text)
    {
        $popup = $this->crawler
            ->filter('.modal.fade.in')
            ->eq(0)
        ;
        $popupText = trim($popup->filter('.modal-body p')->eq(0)->text());
        $this->assertContains($text, $popupText);
    }

    protected function clickYesInConfirmPopup()
    {
        $popup = $this->crawler
            ->filter('.modal.fade.in')
            ->eq(0)
        ;

        $yesButtonLink = $popup
            ->filter('a[data-oneclick="Yes"]')
            ->eq(0)
            ->link()
        ;

        $this->crawler = $this->client->click($yesButtonLink);
    }

    protected function clickNoInConfirmPopup()
    {
        $this->hidePopup();
    }

    /**
     * @param $text
     */
    protected function seeLabel($text)
    {
        $mainInfoPanel = $this->crawler
            ->filter('.app-detailed__wrapper')
            ->eq(0)
        ;

        $labels = $mainInfoPanel->filter('.label');
        $labelsText = $labels->each(function (Crawler $node, $i) {
            return '[' . $node->text() . ']';
        });
        $labelsText = 'Labels: ' . implode(', ', $labelsText);

        $this->assertContains($text, $labelsText);
    }

    /**
     * @param $formTitle
     */
    protected function seeForm($formTitle)
    {
        /** @var Crawler $formContainer */
        $formContainer = $this->crawler->filter('.app-form')->eq(0);
        /** @var Crawler $formHeader */
        $formHeader = $formContainer->filter('.app-form-header')->eq(0);
        $this->assertContains($formTitle, $formHeader->text());
    }

    /**
     * @param Link $link
     */
    private function showPopup($link)
    {
        $attributeDataTarget = $link->getNode()->getAttribute('data-target');
        $popup = $this->crawler
            ->filter($attributeDataTarget)
            ->eq(0)
        ;

        $popupNode = $popup->getNode(0);
        $popupNode->setAttribute('class', 'modal fade in');
        $popupNode->setAttribute('style', 'display: block;');
    }

    private function hidePopup()
    {
        $popup = $this->crawler
            ->filter('.modal.fade.in')
            ->eq(0)
        ;

        $popupNode = $popup->getNode(0);
        $popupNode->setAttribute('class', 'modal fade');
        $popupNode->removeAttribute('style');
    }
}
