-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: erp
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `type` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `client_type_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `company` int(11) DEFAULT NULL,
  `municipality` int(11) DEFAULT NULL,
  `oldest_opportunity_date` date DEFAULT NULL,
  `oldest_opportunity_backflow_date` date DEFAULT NULL,
  `oldest_opportunity_fire_date` date DEFAULT NULL,
  `oldest_opportunity_plumbing_date` date DEFAULT NULL,
  `oldest_opportunity_alarm_date` date DEFAULT NULL,
  `oldest_proposal_date` date DEFAULT NULL,
  `oldest_proposal_backflow_date` date DEFAULT NULL,
  `oldest_proposal_fire_date` date DEFAULT NULL,
  `oldest_proposal_plumbing_date` date DEFAULT NULL,
  `oldest_proposal_alarm_date` date DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_system_mas_level_address_id` int(11) DEFAULT NULL,
  `old_account_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `building_type` int(11) DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `is_messages_processed` tinyint(1) DEFAULT '0',
  `old_link_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_send_to` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coordinate_id` int(11) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `special_discount` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7D3656A4F5B7AF75` (`address_id`),
  UNIQUE KEY `UNIQ_7D3656A4E5DE27B0` (`old_system_mas_level_address_id`),
  KEY `IDX_7D3656A48CDE5729` (`type`),
  KEY `IDX_7D3656A4727ACA70` (`parent_id`),
  KEY `IDX_7D3656A49771C8EE` (`client_type_id`),
  KEY `IDX_7D3656A44FBF094F` (`company`),
  KEY `IDX_7D3656A4C6F56628` (`municipality`),
  KEY `IDX_7D3656A42AFA207D` (`building_type`),
  KEY `IDX_7D3656A498BBE953` (`coordinate_id`),
  KEY `IDX_7D3656A417653B16` (`payment_term_id`),
  CONSTRAINT `FK_7D3656A417653B16` FOREIGN KEY (`payment_term_id`) REFERENCES `payment_term` (`id`),
  CONSTRAINT `FK_7D3656A42AFA207D` FOREIGN KEY (`building_type`) REFERENCES `account_building_type` (`id`),
  CONSTRAINT `FK_7D3656A44FBF094F` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_7D3656A4727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_7D3656A48CDE5729` FOREIGN KEY (`type`) REFERENCES `account_type` (`id`),
  CONSTRAINT `FK_7D3656A49771C8EE` FOREIGN KEY (`client_type_id`) REFERENCES `client_type` (`id`),
  CONSTRAINT `FK_7D3656A498BBE953` FOREIGN KEY (`coordinate_id`) REFERENCES `coordinate` (`id`),
  CONSTRAINT `FK_7D3656A4C6F56628` FOREIGN KEY (`municipality`) REFERENCES `municipality` (`id`),
  CONSTRAINT `FK_7D3656A4E5DE27B0` FOREIGN KEY (`old_system_mas_level_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_7D3656A4F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'Account one','Note: test','2018-06-19 10:59:58','2018-06-19 10:59:58',1,17,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(2,'Group account one','Note: test','2018-06-19 10:59:58','2018-06-19 10:59:58',2,18,NULL,2,0,NULL,2,'2018-05-19','2018-08-19','2018-05-19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(3,'Account two','Note: test','2018-06-19 10:59:58','2018-06-19 10:59:58',1,19,2,2,0,NULL,2,'2018-07-19',NULL,'2018-07-19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(4,'Account three','Note: test','2018-06-19 10:59:58','2018-06-19 10:59:58',1,20,2,2,0,NULL,2,'2018-05-19','2018-08-19','2018-05-19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(5,'Account five','Some notes','2018-06-19 10:59:58','2018-06-19 10:59:58',2,21,NULL,3,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(6,'McDonalds','','2018-06-19 10:59:58','2018-06-19 10:59:58',2,22,NULL,2,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'http://corporate.mcdonalds.com/mcd.html',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(7,'/\\ccount##1','','2018-06-19 10:59:58','2018-06-19 10:59:58',1,23,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(8,'Account six','qwerty!@#$&*()_','2018-06-19 10:59:58','2018-06-19 10:59:58',1,24,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(9,'Account seven','Illinois','2018-06-19 10:59:59','2018-06-19 10:59:59',1,25,NULL,1,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(10,'Test','2342342','2018-06-19 10:59:59','2018-06-19 10:59:59',2,26,NULL,2,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(11,'Account 10','Test Note _ ( ) ^ dfmsisugvj dewqjk ehferwh hfekr		','2018-06-19 10:59:59','2018-06-19 10:59:59',2,27,NULL,2,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(12,'Account frue89','qwerty!@#$&*()_','2018-06-19 10:59:59','2018-06-19 10:59:59',2,28,NULL,1,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(13,'Account nine','Illinois','2018-06-19 10:59:59','2018-06-19 10:59:59',1,29,NULL,1,0,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(14,'Acc','defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ','2018-06-19 10:59:59','2018-06-19 10:59:59',1,30,NULL,1,0,NULL,2,'2018-06-19','2018-06-19','2018-06-19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(15,'BFH','defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ','2018-06-19 10:59:59','2018-06-19 10:59:59',1,31,NULL,1,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(16,'Deleted Account','Some notes for account some notes for account some notes for account some notes for account','2018-06-19 10:59:59','2018-06-19 10:59:59',1,32,NULL,1,1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(17,'Another Deleted Account','Some notes for account some notes for account some notes for account some notes for account','2018-06-19 10:59:59','2018-06-19 10:59:59',2,33,NULL,1,1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0),(18,'Account for changing ContactPersonRoles testing','account for changing contact person roles testing','2018-06-19 10:59:59','2018-06-19 10:59:59',1,34,NULL,1,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_building_type`
--

DROP TABLE IF EXISTS `account_building_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_building_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_building_type`
--

LOCK TABLES `account_building_type` WRITE;
/*!40000 ALTER TABLE `account_building_type` DISABLE KEYS */;
INSERT INTO `account_building_type` VALUES (1,'Residential','residential','2018-06-19 10:59:58','2018-06-19 10:59:58'),(2,'Commercial','commercial','2018-06-19 10:59:58','2018-06-19 10:59:58');
/*!40000 ALTER TABLE `account_building_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_contact_person`
--

DROP TABLE IF EXISTS `account_contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_person` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `authorizer` tinyint(1) DEFAULT '0',
  `access` tinyint(1) DEFAULT '0',
  `access_primary` tinyint(1) DEFAULT '0',
  `payment` tinyint(1) DEFAULT '0',
  `payment_primary` tinyint(1) DEFAULT '0',
  `sending_address_id` int(11) DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `notes` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_F6B922FDA44EE6F7` (`contact_person`),
  KEY `IDX_F6B922FD7D3656A4` (`account`),
  KEY `IDX_F6B922FD6510ABA8` (`sending_address_id`),
  CONSTRAINT `FK_F6B922FD6510ABA8` FOREIGN KEY (`sending_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_F6B922FD7D3656A4` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_F6B922FDA44EE6F7` FOREIGN KEY (`contact_person`) REFERENCES `contact_person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_contact_person`
--

LOCK TABLES `account_contact_person` WRITE;
/*!40000 ALTER TABLE `account_contact_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_contact_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_contact_person_history`
--

DROP TABLE IF EXISTS `account_contact_person_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_contact_person_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `contact_person` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `authorizer` tinyint(1) DEFAULT '0',
  `access` tinyint(1) DEFAULT '0',
  `access_primary` tinyint(1) DEFAULT '0',
  `payment` tinyint(1) DEFAULT '0',
  `payment_primary` tinyint(1) DEFAULT '0',
  `sending_address_id` int(11) DEFAULT NULL,
  `sending_address_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_950F5F58835E0EEE` (`owner_entity_id`),
  KEY `IDX_950F5F58BDAFD8C8` (`author`),
  KEY `IDX_950F5F58A44EE6F7` (`contact_person`),
  KEY `IDX_950F5F587D3656A4` (`account`),
  KEY `IDX_950F5F586510ABA8` (`sending_address_id`),
  CONSTRAINT `FK_950F5F586510ABA8` FOREIGN KEY (`sending_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_950F5F587D3656A4` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_950F5F58835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_950F5F58A44EE6F7` FOREIGN KEY (`contact_person`) REFERENCES `contact_person_history` (`id`),
  CONSTRAINT `FK_950F5F58BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_contact_person_history`
--

LOCK TABLES `account_contact_person_history` WRITE;
/*!40000 ALTER TABLE `account_contact_person_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_contact_person_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_contact_persons_responsibilities`
--

DROP TABLE IF EXISTS `account_contact_persons_responsibilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_contact_persons_responsibilities` (
  `account_contact_person_id` int(11) NOT NULL,
  `device_category_id` int(11) NOT NULL,
  PRIMARY KEY (`account_contact_person_id`,`device_category_id`),
  KEY `IDX_199EC2CECE52684D` (`account_contact_person_id`),
  KEY `IDX_199EC2CE60C6C924` (`device_category_id`),
  CONSTRAINT `FK_199EC2CE60C6C924` FOREIGN KEY (`device_category_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_199EC2CECE52684D` FOREIGN KEY (`account_contact_person_id`) REFERENCES `account_contact_person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_contact_persons_responsibilities`
--

LOCK TABLES `account_contact_persons_responsibilities` WRITE;
/*!40000 ALTER TABLE `account_contact_persons_responsibilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_contact_persons_responsibilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_history`
--

DROP TABLE IF EXISTS `account_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `client_type_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `date_save` datetime NOT NULL,
  `company` int(11) DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address_id` int(11) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `special_discount` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_EE9164038CDE5729` (`type`),
  KEY `IDX_EE916403727ACA70` (`parent_id`),
  KEY `IDX_EE9164039771C8EE` (`client_type_id`),
  KEY `IDX_EE916403835E0EEE` (`owner_entity_id`),
  KEY `IDX_EE916403BDAFD8C8` (`author`),
  KEY `IDX_EE9164034FBF094F` (`company`),
  KEY `IDX_EE916403F5B7AF75` (`address_id`),
  KEY `IDX_EE91640379D0C0E4` (`billing_address_id`),
  KEY `IDX_EE91640317653B16` (`payment_term_id`),
  CONSTRAINT `FK_EE91640317653B16` FOREIGN KEY (`payment_term_id`) REFERENCES `payment_term` (`id`),
  CONSTRAINT `FK_EE9164034FBF094F` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_EE916403727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_EE91640379D0C0E4` FOREIGN KEY (`billing_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_EE916403835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_EE9164038CDE5729` FOREIGN KEY (`type`) REFERENCES `account_type` (`id`),
  CONSTRAINT `FK_EE9164039771C8EE` FOREIGN KEY (`client_type_id`) REFERENCES `client_type` (`id`),
  CONSTRAINT `FK_EE916403BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_EE916403F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_history`
--

LOCK TABLES `account_history` WRITE;
/*!40000 ALTER TABLE `account_history` DISABLE KEYS */;
INSERT INTO `account_history` VALUES (1,1,17,NULL,1,1,NULL,'Account one','Note: test','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(2,2,18,NULL,2,2,NULL,'Group account one','Note: test','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(3,1,19,2,2,3,NULL,'Account two','Note: test','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(4,1,20,2,2,4,NULL,'Account three','Note: test','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(5,2,21,NULL,3,5,NULL,'Account five','Some notes','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(6,2,22,NULL,2,6,NULL,'McDonalds','','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,'http://corporate.mcdonalds.com/mcd.html',NULL,NULL,0),(7,1,23,NULL,1,7,NULL,'/\\ccount##1','','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(8,1,24,NULL,1,8,NULL,'Account six','qwerty!@#$&*()_','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(9,1,25,NULL,1,9,NULL,'Account seven','Illinois','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(10,2,26,NULL,2,10,NULL,'Test','2342342','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(11,2,27,NULL,2,11,NULL,'Account 10','Test Note _ ( ) ^ dfmsisugvj dewqjk ehferwh hfekr		','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(12,2,28,NULL,1,12,NULL,'Account frue89','qwerty!@#$&*()_','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(13,1,29,NULL,1,13,NULL,'Account nine','Illinois','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(14,1,30,NULL,1,14,NULL,'Acc','defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(15,1,31,NULL,1,15,NULL,'BFH','defwikoiojgip90e oefrgojg er3g iojegr jeor jergp eorgupergp jer gp ker gioegrt rtg78 uw7e4ftw3f34r348r78 t4rjgio ejg po epkg opekrgop ero gopergjp ergoier p geprgkprekgop erpgioj58uyy59 4eog4590gu uY*^&(*^&*%$&%&^rfdeihlerwvf eiruwv heilrmv o;dfso9vue89  ','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(16,1,32,NULL,1,16,NULL,'Deleted Account','Some notes for account some notes for account some notes for account some notes for account','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(17,2,33,NULL,1,17,NULL,'Another Deleted Account','Some notes for account some notes for account some notes for account some notes for account','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0),(18,1,34,NULL,1,18,NULL,'Account for changing ContactPersonRoles testing','account for changing contact person roles testing','2018-06-19 10:59:59','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `account_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_type`
--

DROP TABLE IF EXISTS `account_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_type`
--

LOCK TABLES `account_type` WRITE;
/*!40000 ALTER TABLE `account_type` DISABLE KEYS */;
INSERT INTO `account_type` VALUES (1,'Account','account','2018-06-19 10:59:58','2018-06-19 10:59:58'),(2,'Group account','group account','2018-06-19 10:59:58','2018-06-19 10:59:58');
/*!40000 ALTER TABLE `account_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_messages`
--

DROP TABLE IF EXISTS `accounts_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_messages` (
  `account_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  PRIMARY KEY (`account_id`,`message_id`),
  KEY `IDX_82D960F39B6B5FBA` (`account_id`),
  KEY `IDX_82D960F3537A1329` (`message_id`),
  CONSTRAINT `FK_82D960F3537A1329` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  CONSTRAINT `FK_82D960F39B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_messages`
--

LOCK TABLES `accounts_messages` WRITE;
/*!40000 ALTER TABLE `accounts_messages` DISABLE KEYS */;
INSERT INTO `accounts_messages` VALUES (2,1),(14,1);
/*!40000 ALTER TABLE `accounts_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `address_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D4E6F815D83CC1` (`state_id`),
  KEY `IDX_D4E6F819EA97B0B` (`address_type_id`),
  CONSTRAINT `FK_D4E6F815D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_D4E6F819EA97B0B` FOREIGN KEY (`address_type_id`) REFERENCES `address_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,32,'Old system address 111','New York','23345','2018-06-19 10:59:58','2018-06-19 10:59:58',7),(2,32,'Some address for addresses street One building 1','Kiev','0005','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(3,32,'Some address for addresses street One building 1','Kiev','0005','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(4,32,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(5,2,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(6,37,'Street and Building and other info','Lviv','121212','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(7,31,'Just info about address','Paris','54454','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(8,28,'fdfdseeewefafsdfsdfs sfsdfsdfv sefef34 dddd 33','Kiev','567890','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(9,2,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(10,2,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(11,2,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(12,2,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(13,2,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(14,2,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(15,13,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(16,13,NULL,NULL,NULL,'2018-06-19 10:59:58','2018-06-19 10:59:58',1),(17,21,'Test address','Boston','09000','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(18,21,'some Test address','Boston','09000','2018-06-19 10:59:58','2018-06-19 10:59:58',3),(19,21,'Some address','Boston','09000','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(20,21,'Test address for account','New York','09000','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(21,1,'Test address for account','Kiev','354792','2018-06-19 10:59:58','2018-06-19 10:59:58',3),(22,1,'some test address lines','Odessa','354792','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(23,13,'test address','some city','0','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(24,5,'test address','City','999999','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(25,2,'Str # 1234!','Some city','111111','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(26,1,'efuwygierwhfiheiwh','City','324234','2018-06-19 10:59:58','2018-06-19 10:59:58',3),(27,1,'Tets Ndegw','defwikoiojgip90e','3424234','2018-06-19 10:59:58','2018-06-19 10:59:58',3),(28,5,'Tets Ndegw','City','999999','2018-06-19 10:59:58','2018-06-19 10:59:58',3),(29,2,'Str # 1234!','Some city fhs','111111','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(30,5,'Some address','City of fdskh','999999','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(31,5,'Str Alefwhfefrrnbndfv','Some city','111111','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(32,2,'Some address info for deleted account 11111','Kiev','121212','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(33,2,'Some address info for deleted account 22222','Lviv','3333333','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(34,1,'17A, Kharkivske Way','Kyiv','01999','2018-06-19 10:59:58','2018-06-19 10:59:58',NULL),(35,6,'Some address for Company One street One building 1','Kiev','00005','2018-06-19 10:59:58','2018-06-19 10:59:58',4),(36,32,'Another address for Company Two street  street 1','New York','232323','2018-06-19 10:59:58','2018-06-19 10:59:58',4),(37,17,'Another address for Company Three','Lviv','222222','2018-06-19 10:59:58','2018-06-19 10:59:58',4),(38,29,'12 address for Company Four','Odessa','688900','2018-06-19 10:59:58','2018-06-19 10:59:58',4),(39,29,'Lyskivska, str. 7-a, app, 149','Berezan','1991','2018-06-19 10:59:58','2018-06-19 10:59:58',4),(40,13,'1540 N Old Rand Rd','Chicago','60084','2018-06-19 10:59:58','2018-06-19 10:59:58',5),(41,13,'600 East Broadway','Chicago','65201','2018-06-19 10:59:58','2018-06-19 10:59:58',NULL),(42,13,'601 East Broadway','Chicago','65201','2018-06-19 10:59:58','2018-06-19 10:59:58',5),(43,13,'602 East Broadway','Chicago','65201','2018-06-19 10:59:58','2018-06-19 10:59:58',5),(44,13,'36 N. Apple','Chicago','60052','2018-06-19 10:59:58','2018-06-19 10:59:58',5),(45,13,'36 N. Apple','Chicago','60052','2018-06-19 10:59:58','2018-06-19 10:59:58',NULL),(46,13,'36 N. Apple','Chicago','60052','2018-06-19 10:59:58','2018-06-19 10:59:58',NULL),(47,13,'36 N. Apple','Chicago','60052','2018-06-19 10:59:58','2018-06-19 10:59:58',NULL),(48,13,'Street 1','Chicago','1234','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(49,13,'Street 1','Chicago','1234','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(50,13,'Street 2','Chicago','1234','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(51,13,'Street 3','Chicago','1234','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(52,13,'Street 4','Chicago','1234','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(53,13,'Street 1','Lviv','222222','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(54,2,'Street or no street 23','Kiev','23456','2018-06-19 10:59:58','2018-06-19 10:59:58',1),(55,32,'Build','Dnepr','88998','2018-06-19 10:59:58','2018-06-19 10:59:58',1);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_type`
--

DROP TABLE IF EXISTS `address_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_type`
--

LOCK TABLES `address_type` WRITE;
/*!40000 ALTER TABLE `address_type` DISABLE KEYS */;
INSERT INTO `address_type` VALUES (1,'Personal Address','personal','2018-06-19 10:59:38','2018-06-19 10:59:38',3),(2,'Site Address','site address','2018-06-19 10:59:38','2018-06-19 10:59:38',2),(3,'Group Account Address','group account address','2018-06-19 10:59:38','2018-06-19 10:59:38',2),(4,'Company Address','company address','2018-06-19 10:59:38','2018-06-19 10:59:38',1),(5,'Contractor Address','contractor address','2018-06-19 10:59:38','2018-06-19 10:59:38',4),(6,'Contact Address','contact address','2018-06-19 10:59:38','2018-06-19 10:59:38',5),(7,'Old System Master Address','old system master address','2018-06-19 10:59:38','2018-06-19 10:59:38',6),(8,'Municipality Address','municipality','2018-06-19 10:59:38','2018-06-19 10:59:38',5);
/*!40000 ALTER TABLE `address_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent`
--

LOCK TABLES `agent` WRITE;
/*!40000 ALTER TABLE `agent` DISABLE KEYS */;
INSERT INTO `agent` VALUES (1,'Apple','http://www.google.com','1111111111'),(2,'Bell Labs','http://www.google.com','3112111111'),(3,'AT&T','https://www.att.com/','4113111111');
/*!40000 ALTER TABLE `agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_channel`
--

DROP TABLE IF EXISTS `agent_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named` int(11) DEFAULT NULL,
  `agent` int(11) DEFAULT NULL,
  `device_category` int(11) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_70AAECED71E0CC87` (`named`),
  KEY `IDX_70AAECED268B9C9D` (`agent`),
  KEY `IDX_70AAECED887840E1` (`device_category`),
  CONSTRAINT `FK_70AAECED268B9C9D` FOREIGN KEY (`agent`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_70AAECED71E0CC87` FOREIGN KEY (`named`) REFERENCES `channel_named` (`id`),
  CONSTRAINT `FK_70AAECED887840E1` FOREIGN KEY (`device_category`) REFERENCES `device_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_channel`
--

LOCK TABLES `agent_channel` WRITE;
/*!40000 ALTER TABLE `agent_channel` DISABLE KEYS */;
INSERT INTO `agent_channel` VALUES (1,1,1,1,1),(2,2,1,1,0),(3,3,1,2,1),(4,1,2,1,1),(5,4,1,2,0),(6,4,1,1,0),(7,4,3,2,1);
/*!40000 ALTER TABLE `agent_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_channel_dynamic_field_value`
--

DROP TABLE IF EXISTS `agent_channel_dynamic_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_channel_dynamic_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent_channel` int(11) DEFAULT NULL,
  `field` int(11) DEFAULT NULL,
  `entity_value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EAC5B2F870AAECED` (`agent_channel`),
  KEY `IDX_EAC5B2F85BF54558` (`field`),
  KEY `IDX_EAC5B2F85BD407E2` (`entity_value_id`),
  CONSTRAINT `FK_EAC5B2F85BD407E2` FOREIGN KEY (`entity_value_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_EAC5B2F85BF54558` FOREIGN KEY (`field`) REFERENCES `channel_dynamic_field` (`id`),
  CONSTRAINT `FK_EAC5B2F870AAECED` FOREIGN KEY (`agent_channel`) REFERENCES `agent_channel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_channel_dynamic_field_value`
--

LOCK TABLES `agent_channel_dynamic_field_value` WRITE;
/*!40000 ALTER TABLE `agent_channel_dynamic_field_value` DISABLE KEYS */;
INSERT INTO `agent_channel_dynamic_field_value` VALUES (1,'google@gmail.com',1,1,NULL),(2,NULL,2,2,53),(3,'1112223333',3,3,NULL),(4,'google@gmail.com',4,1,NULL),(5,'google.com',5,4,NULL),(6,'yahoo.com',6,4,NULL),(7,'yahoo.com',7,4,NULL);
/*!40000 ALTER TABLE `agent_channel_dynamic_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents_contacts`
--

DROP TABLE IF EXISTS `agents_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents_contacts` (
  `agent_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`agent_id`,`contact_id`),
  KEY `IDX_BFB535093414710B` (`agent_id`),
  KEY `IDX_BFB53509E7A1254A` (`contact_id`),
  CONSTRAINT `FK_BFB535093414710B` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_BFB53509E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents_contacts`
--

LOCK TABLES `agents_contacts` WRITE;
/*!40000 ALTER TABLE `agents_contacts` DISABLE KEYS */;
INSERT INTO `agents_contacts` VALUES (1,15),(2,9),(3,10),(3,11);
/*!40000 ALTER TABLE `agents_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` tinyint(1) NOT NULL DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DADD4A251E27F6BF` (`question_id`),
  KEY `IDX_DADD4A254BD2A4C0` (`report_id`),
  CONSTRAINT `FK_DADD4A251E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `FK_DADD4A254BD2A4C0` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_dropbox_choices`
--

DROP TABLE IF EXISTS `channel_dropbox_choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_dropbox_choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `option_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `select_default` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D7781A17443707B0` (`field_id`),
  CONSTRAINT `FK_D7781A17443707B0` FOREIGN KEY (`field_id`) REFERENCES `channel_dynamic_field` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_dropbox_choices`
--

LOCK TABLES `channel_dropbox_choices` WRITE;
/*!40000 ALTER TABLE `channel_dropbox_choices` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel_dropbox_choices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_dynamic_field`
--

DROP TABLE IF EXISTS `channel_dynamic_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_dynamic_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `validation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5D1EFF9772F5A1AA` (`channel_id`),
  KEY `IDX_5D1EFF97C54C8C93` (`type_id`),
  KEY `IDX_5D1EFF97A2274850` (`validation_id`),
  CONSTRAINT `FK_5D1EFF9772F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `channel_named` (`id`),
  CONSTRAINT `FK_5D1EFF97A2274850` FOREIGN KEY (`validation_id`) REFERENCES `dynamic_field_validation` (`id`),
  CONSTRAINT `FK_5D1EFF97C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `dynamic_field_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_dynamic_field`
--

LOCK TABLES `channel_dynamic_field` WRITE;
/*!40000 ALTER TABLE `channel_dynamic_field` DISABLE KEYS */;
INSERT INTO `channel_dynamic_field` VALUES (1,1,1,'Send to','send to','2018-06-19 10:59:58','2018-06-19 10:59:58',6),(2,2,3,'Send to','send to','2018-06-19 10:59:58','2018-06-19 10:59:58',NULL),(3,3,1,'Send to','send to','2018-06-19 10:59:58','2018-06-19 10:59:58',4),(4,4,1,'Send to','send to','2018-06-19 10:59:58','2018-06-19 10:59:58',10);
/*!40000 ALTER TABLE `channel_dynamic_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_named`
--

DROP TABLE IF EXISTS `channel_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_named`
--

LOCK TABLES `channel_named` WRITE;
/*!40000 ALTER TABLE `channel_named` DISABLE KEYS */;
INSERT INTO `channel_named` VALUES (1,'Email','email','2018-06-19 10:59:58','2018-06-19 10:59:58'),(2,'Mail','mail','2018-06-19 10:59:58','2018-06-19 10:59:58'),(3,'Fax','fax','2018-06-19 10:59:58','2018-06-19 10:59:58'),(4,'Upload','upload','2018-06-19 10:59:58','2018-06-19 10:59:58');
/*!40000 ALTER TABLE `channel_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_type`
--

DROP TABLE IF EXISTS `client_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_type`
--

LOCK TABLES `client_type` WRITE;
/*!40000 ALTER TABLE `client_type` DISABLE KEYS */;
INSERT INTO `client_type` VALUES (1,'Active','active','2018-06-19 10:59:57','2018-06-19 10:59:57'),(2,'Inactive','inactive','2018-06-19 10:59:57','2018-06-19 10:59:57'),(3,'Potential','potential','2018-06-19 10:59:57','2018-06-19 10:59:57');
/*!40000 ALTER TABLE `client_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `old_company_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `source_entity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_4FBF094FF5B7AF75` (`address_id`),
  KEY `IDX_4FBF094F727ACA70` (`parent_id`),
  CONSTRAINT `FK_4FBF094F727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_4FBF094FF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,35,'Company One','google.com','Notes for Company one loren ipsum notes for Company one loren ipsum',0,'2018-06-19 10:59:58','2018-06-19 10:59:58',NULL,0,NULL,NULL),(2,36,'Another Company Two','yandex.com','Notes for Company two notes notes notes notes notes notes notes notes notes notes',0,'2018-06-19 10:59:58','2018-06-19 10:59:58',NULL,0,NULL,NULL),(3,37,'Very Good Company Three','mail.ru','Company Three notes notes notes notes notes ',0,'2018-06-19 10:59:58','2018-06-19 10:59:58',NULL,0,NULL,NULL),(4,38,'Grate Company Four','yahoo.com','notes notes notes notes notes notes notes notes notes notes notes notes notes notes',0,'2018-06-19 10:59:58','2018-06-19 10:59:58',NULL,0,NULL,NULL),(5,39,'McDonalds','https://www.mcdonalds.ua/ua.html','Cool company',0,'2018-06-19 10:59:58','2018-06-19 10:59:58',NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_history`
--

DROP TABLE IF EXISTS `company_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7E86A9C835E0EEE` (`owner_entity_id`),
  KEY `IDX_7E86A9CBDAFD8C8` (`author`),
  KEY `IDX_7E86A9CF5B7AF75` (`address_id`),
  CONSTRAINT `FK_7E86A9C835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_7E86A9CBDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_7E86A9CF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_history`
--

LOCK TABLES `company_history` WRITE;
/*!40000 ALTER TABLE `company_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4C62E638F5B7AF75` (`address_id`),
  KEY `IDX_4C62E6388CDE5729` (`type`),
  CONSTRAINT `FK_4C62E6388CDE5729` FOREIGN KEY (`type`) REFERENCES `contact_type` (`id`),
  CONSTRAINT `FK_4C62E638F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,3,'John','Johnson','Title for Contact 1','0905671221','0312121211','003','0434346767','contact1@gmail.com',3),(2,NULL,'Jam','Smith','Title for Contact 2','0905671222','0312121212','004','0434346768','contact2@gmail.com',3),(3,6,'William','Johnson','Title for Contact 3','0905671223','0312121213','005','0434346769','contact3@gmail.com',3),(4,NULL,'John','Snow',NULL,NULL,NULL,NULL,NULL,NULL,3),(5,41,'James','Bond','Title 1 for Contact for Municipality','0915671221','0322121211','007','0514346760','contact4@gmail.com',1),(6,17,'William','Taylor','Title 2 for Contact for Municipality','0915671222','0322121212','008','0514346761','contact5@gmail.com',1),(7,20,'Jack','Sparrow','Title 3 for Contact for Municipality','0915671223','0322121213','009','0514346762','contact6@gmail.com',1),(8,42,'Bill','Smith','Title 4 for Contact for Municipality','0915671224','0322121214','010','0514346763','contact7@gmail.com',1),(9,43,'Ralph','Gleason','Title 1 for Contact for Agent','0925671221','0332121211','011','0714346761','contact8@gmail.com',2),(10,44,'Paul','Mccartney','Title 2 for Contact for Agent','0925671222','0332121212','012','0714346762','contact9@gmail.com',2),(11,50,'Linda','Smith','Title 3 for Contact for Agent','0925671223','0332121213','013','0714346763','contact10@gmail.com',2),(12,51,'Jann','Wenner','Title 4 for Contact for Agent','0925671224','0332121214','014','0714346764','contact11@gmail.com',2),(13,NULL,'Jann','Winner','Title 5 for Contact for Department','0925671224',NULL,'014','0514345554',NULL,3),(14,51,'Jon','Doe','Some Department\'s title','0925671224',NULL,'014','0514345554',NULL,3),(15,51,'Harry','Potter','Some Agents\'s title','0925671224',NULL,'014','0514345554','contact14@gmail.com',2),(16,51,'Bruce','Willis','Some department\'s title','0925671224',NULL,'014','0514345554','contact14@gmail.com',3),(17,51,'Keira','Knightley','some title','0234571224',NULL,'014','0412345554','contact16@gmail.com',3),(18,51,'John','Depp','Some Agents\'s title','0925671224',NULL,'014','0514345554','contact17@gmail.com',3);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_person`
--

DROP TABLE IF EXISTS `contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cod` tinyint(1) DEFAULT '0',
  `notes` longtext COLLATE utf8_unicode_ci,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `company` int(11) DEFAULT NULL,
  `num_contact` int(11) DEFAULT NULL,
  `source_entity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_address` int(11) DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_A44EE6F74FBF094F` (`company`),
  KEY `IDX_A44EE6F78E6966AA` (`personal_address`),
  CONSTRAINT `FK_A44EE6F74FBF094F` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_A44EE6F78E6966AA` FOREIGN KEY (`personal_address`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_person`
--

LOCK TABLES `contact_person` WRITE;
/*!40000 ALTER TABLE `contact_person` DISABLE KEYS */;
INSERT INTO `contact_person` VALUES (1,'Eddard','Stark','Title for Contact Person','8905671221','003','1212121211','3434346767',1,'sdsd sdsdsee rerhrrhrtrt kldfkjjgdlfhgu fgf fgderew vccx','2018-06-19 10:59:59','2018-06-19 10:59:59','eddard.stark@gmail.com',0,NULL,NULL,NULL,NULL,3,0),(2,'Robb','Stark','Contact Persons title','4547765555','123','3434346767','5643433423',1,'wwwwww wwwww www w ww w w wwww','2018-06-19 10:59:59','2018-06-19 10:59:59','robb.stark@gmail.com',0,NULL,NULL,NULL,NULL,4,0),(3,'Tyrion','Lannister','Contact Persons title test','3435671234','333','5672440987','3437774444',0,'sdsdsd rrt','2018-06-19 10:59:59','2018-06-19 10:59:59','tyrion.lannister@gmail.com',0,2,NULL,NULL,NULL,5,0),(4,'Petyr','Bealish','Contact test Persons title','5671215657','123','3432222323','2137779765',0,'wwwwww wwwww www w ww w w wwww','2018-06-19 10:59:59','2018-06-19 10:59:59','petyr.bealish@gmail.com',0,NULL,NULL,NULL,NULL,6,0),(5,'Tony','Stark','Contact Persons title','3432140345','123','4562347890','5563455677',0,'wwwwww 1212 sdsd test dsdsd','2018-06-19 10:59:59','2018-06-19 10:59:59','tony.stark@gmail.com',0,NULL,NULL,NULL,NULL,7,0),(6,'Valentin','Strykhalo','Contact Persons deleted 1','4454324567','159','4215670000','4547873245',0,'some notes for deleted contact person 1','2018-06-19 10:59:59','2018-06-19 10:59:59','valentin.strykhalo@test.com',1,NULL,NULL,NULL,NULL,8,0),(7,'Nikola','Tesla','Contact Persons deleted 2','3330964321','741','1126531278','6782346789',1,'some notes for deleted contact person 2 some notes for deleted contact person 2 some notes for deleted contact person 2','2018-06-19 10:59:59','2018-06-19 10:59:59','nikola.tesla@test.com',1,NULL,NULL,NULL,NULL,9,0),(8,'Mailing Address Conflict!',NULL,NULL,NULL,NULL,NULL,NULL,0,'Some of the sites in the old system under this Group Account (formerly a Master) were using Master Address as mailing address to send retest notices.','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,0,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `contact_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_person_history`
--

DROP TABLE IF EXISTS `contact_person_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_person_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cod` tinyint(1) DEFAULT '0',
  `notes` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `company` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_76D94D02835E0EEE` (`owner_entity_id`),
  KEY `IDX_76D94D02BDAFD8C8` (`author`),
  KEY `IDX_76D94D024FBF094F` (`company`),
  CONSTRAINT `FK_76D94D024FBF094F` FOREIGN KEY (`company`) REFERENCES `company` (`id`),
  CONSTRAINT `FK_76D94D02835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `contact_person` (`id`),
  CONSTRAINT `FK_76D94D02BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_person_history`
--

LOCK TABLES `contact_person_history` WRITE;
/*!40000 ALTER TABLE `contact_person_history` DISABLE KEYS */;
INSERT INTO `contact_person_history` VALUES (1,1,NULL,'Eddard','Stark','Title for Contact Person','contact1@gmail.com','8905671221','003','1212121211','3434346767',1,'sdsd sdsdsee rerhrrhrtrt kldfkjjgdlfhgu fgf fgderew vccx','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL),(2,2,NULL,'Robb','Stark','Contact Persons title','robb.stark@gmail.com','4547765555','123','3434346767','5643433423',1,'wwwwww wwwww www w ww w w wwww','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL),(3,3,NULL,'Tyrion','Lannister','Contact Persons title test','tyrion.lannister@gmail.com','3435671234','333','5672440987','3437774444',0,'sdsdsd rrt','2018-06-19 10:59:59','2018-06-19 10:59:59',2),(4,4,NULL,'Petyr','Bealish','Contact test Persons title','petyr.bealish@gmail.com','5671215657','123','3432222323','2137779765',0,'wwwwww wwwww www w ww w w wwww','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL),(5,5,NULL,'Tony','Stark','Contact Persons title','tony.stark@gmail.com','3432140345','123','4562347890','5563455677',0,'wwwwww 1212 sdsd test dsdsd','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL),(6,6,NULL,'Valentin','Strykhalo','Contact Persons deleted 1','valentin.strykhalo@test.com','4454324567','159','4215670000','4547873245',0,'some notes for deleted contact person 1','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL),(7,7,NULL,'Nikola','Tesla','Contact Persons deleted 2','nikola.tesla@test.com','3330964321','741','1126531278','6782346789',1,'some notes for deleted contact person 2 some notes for deleted contact person 2 some notes for deleted contact person 2','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL),(8,8,NULL,'Mailing Address Conflict!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'Some of the sites in the old system under this Group Account (formerly a Master) were using Master Address as mailing address to send retest notices.','2018-06-19 10:59:59','2018-06-19 10:59:59',NULL);
/*!40000 ALTER TABLE `contact_person_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_type`
--

DROP TABLE IF EXISTS `contact_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_type`
--

LOCK TABLES `contact_type` WRITE;
/*!40000 ALTER TABLE `contact_type` DISABLE KEYS */;
INSERT INTO `contact_type` VALUES (1,'Municipality contact','municipality_contact','municipality_contact.svg'),(2,'Agent contact','agent_contact','agent_contact.svg'),(3,'Department contact','department_contact','municipality_contact.svg');
/*!40000 ALTER TABLE `contact_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_divisions`
--

DROP TABLE IF EXISTS `contacts_divisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_divisions` (
  `contact_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`contact_id`,`division_id`),
  KEY `IDX_767E9ABE7A1254A` (`contact_id`),
  KEY `IDX_767E9AB41859289` (`division_id`),
  CONSTRAINT `FK_767E9AB41859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_767E9ABE7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_divisions`
--

LOCK TABLES `contacts_divisions` WRITE;
/*!40000 ALTER TABLE `contacts_divisions` DISABLE KEYS */;
INSERT INTO `contacts_divisions` VALUES (1,1),(2,2),(3,2),(4,1),(5,2),(6,2),(7,1),(8,1),(9,1),(10,1),(10,2),(11,1),(12,2),(13,2),(14,2),(15,2),(16,2),(17,1),(18,1);
/*!40000 ALTER TABLE `contacts_divisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor`
--

DROP TABLE IF EXISTS `contractor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_license_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state_license_expiration_date` datetime NOT NULL,
  `coi_expiration` datetime DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_sistem_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_437BD2EFF5B7AF75` (`address_id`),
  KEY `IDX_437BD2EF8CDE5729` (`type`),
  CONSTRAINT `FK_437BD2EF8CDE5729` FOREIGN KEY (`type`) REFERENCES `contractor_type` (`id`),
  CONSTRAINT `FK_437BD2EFF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor`
--

LOCK TABLES `contractor` WRITE;
/*!40000 ALTER TABLE `contractor` DISABLE KEYS */;
INSERT INTO `contractor` VALUES (1,'Monarch','https://monarch.com','123321','2018-06-19 10:59:59','2018-06-19 10:59:59','/uploads/logo/company_a.png','2018-06-19 10:59:59','2018-06-19 10:59:59',44,1,'1234567890',NULL,NULL,NULL,NULL,0),(2,'Contractor2','https://www.yahoo.com/','123321','2018-06-19 10:59:59','2018-06-19 10:59:59','/uploads/logo/company_b.png','2018-06-19 10:59:59','2018-06-19 10:59:59',42,2,'0987654321',NULL,NULL,NULL,NULL,0),(3,'American Backflow & Fire Prevention','www.americanbackflowprevention.com','123321','2018-06-19 10:59:59','2018-06-19 10:59:59','/uploads/logo/abfp_logo_png.jpg','2018-06-19 10:59:59','2018-06-19 10:59:59',40,3,'7778888888',NULL,NULL,NULL,NULL,0),(4,'Contractor no coi date','http://www.google.com','123321','2018-06-19 10:59:59',NULL,NULL,'2018-06-19 10:59:59','2018-06-19 10:59:59',43,1,NULL,NULL,NULL,NULL,NULL,0),(5,'IDAP Group','http://idapgroup.com','123456','2018-06-19 10:59:59',NULL,NULL,'2018-06-19 10:59:59','2018-06-19 10:59:59',34,1,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `contractor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_calendar`
--

DROP TABLE IF EXISTS `contractor_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractor_id` int(11) DEFAULT NULL,
  `access_token` longtext COLLATE utf8_unicode_ci,
  `refresh_token` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_867D276EB0265DC7` (`contractor_id`),
  CONSTRAINT `FK_867D276EB0265DC7` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_calendar`
--

LOCK TABLES `contractor_calendar` WRITE;
/*!40000 ALTER TABLE `contractor_calendar` DISABLE KEYS */;
INSERT INTO `contractor_calendar` VALUES (1,3,NULL,NULL);
/*!40000 ALTER TABLE `contractor_calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_service`
--

DROP TABLE IF EXISTS `contractor_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fixed_price` double DEFAULT NULL,
  `hours_price` double DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `named_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `device_category` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `estimation_time` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8DCF8195457EB27E` (`named_id`),
  KEY `IDX_8DCF8195B0265DC7` (`contractor_id`),
  KEY `IDX_8DCF8195887840E1` (`device_category`),
  KEY `IDX_8DCF81955D83CC1` (`state_id`),
  CONSTRAINT `FK_8DCF8195457EB27E` FOREIGN KEY (`named_id`) REFERENCES `service_named` (`id`),
  CONSTRAINT `FK_8DCF81955D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_8DCF8195887840E1` FOREIGN KEY (`device_category`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_8DCF8195B0265DC7` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_service`
--

LOCK TABLES `contractor_service` WRITE;
/*!40000 ALTER TABLE `contractor_service` DISABLE KEYS */;
INSERT INTO `contractor_service` VALUES (1,NULL,NULL,NULL,NULL,1,3,2,13,0),(2,NULL,NULL,NULL,NULL,2,3,2,13,0),(3,NULL,NULL,NULL,NULL,3,3,2,13,0),(4,NULL,NULL,NULL,NULL,4,3,2,13,0),(5,NULL,NULL,NULL,NULL,5,3,3,13,0),(6,NULL,NULL,NULL,NULL,6,3,3,13,0),(7,NULL,NULL,NULL,NULL,7,3,3,13,0),(8,NULL,NULL,NULL,NULL,8,3,2,13,0),(9,NULL,NULL,NULL,NULL,9,3,2,13,0),(10,NULL,NULL,NULL,NULL,10,3,2,13,0),(11,NULL,NULL,NULL,NULL,11,3,2,13,0),(12,NULL,NULL,NULL,NULL,12,3,2,13,0),(13,NULL,NULL,NULL,NULL,13,3,2,13,0),(14,NULL,NULL,NULL,NULL,14,3,2,13,0),(15,NULL,NULL,NULL,NULL,15,3,2,13,0),(16,NULL,NULL,NULL,NULL,16,3,2,13,0),(17,NULL,NULL,NULL,NULL,17,3,2,13,0),(18,NULL,NULL,NULL,NULL,18,3,2,13,0),(19,NULL,NULL,NULL,NULL,19,3,2,13,0),(20,50,50,'NFPA 25',NULL,20,3,2,13,0.5),(21,NULL,NULL,NULL,NULL,21,3,2,13,0),(22,NULL,NULL,NULL,NULL,22,3,2,13,0),(23,NULL,NULL,NULL,NULL,23,3,2,13,0),(24,NULL,NULL,NULL,NULL,24,3,2,13,0),(25,NULL,NULL,NULL,NULL,25,3,2,13,0),(26,NULL,NULL,NULL,NULL,26,3,2,13,0),(27,NULL,NULL,NULL,NULL,27,3,2,13,0),(28,NULL,NULL,NULL,NULL,28,3,2,13,0),(29,NULL,NULL,NULL,NULL,29,3,3,13,0),(30,NULL,NULL,NULL,NULL,30,3,3,13,0),(31,NULL,NULL,NULL,NULL,31,3,3,13,0),(32,50,50,'NFPA 25',NULL,32,3,2,13,0.5),(33,NULL,NULL,NULL,NULL,33,3,2,13,0),(34,NULL,NULL,NULL,NULL,34,3,2,13,0),(35,NULL,NULL,NULL,NULL,35,3,2,13,0),(36,NULL,NULL,NULL,NULL,36,3,2,13,0),(37,NULL,NULL,NULL,NULL,37,3,2,13,0),(38,NULL,NULL,NULL,NULL,38,3,2,13,0),(39,NULL,NULL,NULL,NULL,39,3,2,13,0),(40,NULL,NULL,NULL,NULL,40,3,2,13,0),(41,NULL,NULL,NULL,NULL,41,3,2,13,0),(42,NULL,NULL,NULL,NULL,42,3,2,13,0),(43,NULL,NULL,NULL,NULL,43,3,2,13,0),(44,NULL,NULL,NULL,NULL,44,3,2,13,0),(45,50,50,'NFPA 25',NULL,45,3,3,13,0),(46,50,50,'NFPA 25',NULL,46,3,3,13,0),(47,NULL,NULL,NULL,NULL,47,3,2,13,0),(48,NULL,NULL,NULL,NULL,48,3,2,13,0),(49,NULL,NULL,NULL,NULL,49,3,2,13,0),(50,NULL,NULL,NULL,NULL,50,3,2,13,0),(51,NULL,NULL,NULL,NULL,51,3,2,13,0),(52,NULL,NULL,NULL,NULL,52,3,2,13,0),(53,NULL,NULL,NULL,NULL,53,3,2,13,0),(54,NULL,NULL,NULL,NULL,54,3,2,13,0),(55,NULL,NULL,NULL,NULL,55,3,2,13,0),(56,NULL,NULL,NULL,NULL,56,3,2,13,0),(57,NULL,NULL,NULL,NULL,57,3,2,13,0),(58,NULL,NULL,NULL,NULL,58,3,2,13,0),(59,NULL,NULL,NULL,NULL,59,3,2,13,0),(60,NULL,NULL,NULL,NULL,60,3,2,13,0),(61,NULL,NULL,NULL,NULL,61,3,2,13,0),(62,NULL,NULL,NULL,NULL,62,3,2,13,0),(63,NULL,NULL,NULL,NULL,63,3,2,13,0),(64,NULL,NULL,NULL,NULL,64,3,2,13,0),(65,NULL,NULL,NULL,NULL,65,3,2,13,0),(66,NULL,NULL,NULL,NULL,66,3,2,13,0),(67,NULL,NULL,NULL,NULL,67,3,2,13,0),(68,NULL,NULL,NULL,NULL,68,3,2,13,0),(69,NULL,NULL,NULL,NULL,69,3,2,13,0),(70,NULL,NULL,NULL,NULL,70,3,2,13,0),(71,NULL,NULL,NULL,NULL,71,3,2,13,0),(72,NULL,NULL,NULL,NULL,72,3,2,13,0),(73,NULL,NULL,NULL,NULL,73,3,2,13,0),(74,NULL,NULL,NULL,NULL,74,3,2,13,0),(75,NULL,NULL,NULL,NULL,75,3,2,13,0),(76,NULL,NULL,NULL,NULL,76,3,2,13,0),(77,NULL,NULL,NULL,NULL,77,3,2,13,0),(78,NULL,NULL,NULL,NULL,78,3,2,13,0),(79,NULL,NULL,NULL,NULL,79,3,2,13,0),(80,NULL,NULL,NULL,NULL,80,3,2,13,0),(81,NULL,NULL,NULL,NULL,81,3,2,13,0),(82,NULL,NULL,NULL,NULL,82,3,2,13,0),(83,NULL,NULL,NULL,NULL,83,3,2,13,0),(84,NULL,NULL,NULL,NULL,84,3,2,13,0),(85,NULL,NULL,NULL,NULL,85,3,2,13,0),(86,NULL,NULL,NULL,NULL,86,3,2,13,0),(87,NULL,NULL,NULL,NULL,87,3,2,13,0),(88,NULL,NULL,NULL,NULL,88,3,2,13,0),(89,NULL,NULL,NULL,NULL,89,3,2,13,0),(90,NULL,NULL,NULL,NULL,90,3,2,13,0),(91,NULL,NULL,NULL,NULL,91,3,2,13,0),(92,NULL,NULL,NULL,NULL,92,3,2,13,0),(93,NULL,NULL,NULL,NULL,93,3,2,13,0),(94,NULL,NULL,NULL,NULL,94,3,2,13,0),(95,NULL,NULL,NULL,NULL,95,3,2,13,0),(96,NULL,NULL,NULL,NULL,96,3,2,13,0),(97,NULL,NULL,NULL,NULL,97,3,2,13,0),(98,NULL,NULL,NULL,NULL,98,3,2,13,0),(99,NULL,NULL,NULL,NULL,99,3,2,13,0),(100,NULL,NULL,NULL,NULL,100,3,2,13,0),(101,NULL,NULL,NULL,NULL,101,3,2,13,0),(102,NULL,NULL,NULL,NULL,102,3,2,13,0),(103,NULL,NULL,NULL,NULL,103,3,2,13,0),(104,NULL,NULL,NULL,NULL,104,3,2,13,0),(105,NULL,NULL,NULL,NULL,105,3,2,13,0),(106,NULL,NULL,NULL,NULL,106,3,2,13,0),(107,NULL,NULL,NULL,NULL,107,3,2,13,0),(108,NULL,NULL,NULL,NULL,108,3,2,13,0),(109,NULL,NULL,NULL,NULL,109,3,2,13,0),(110,NULL,NULL,NULL,NULL,110,3,2,13,0),(111,NULL,NULL,NULL,NULL,111,3,2,13,0),(112,NULL,NULL,NULL,NULL,112,3,2,13,0),(113,NULL,NULL,NULL,NULL,113,3,2,13,0),(114,NULL,NULL,NULL,NULL,114,3,2,13,0),(115,NULL,NULL,NULL,NULL,115,3,2,13,0),(116,NULL,NULL,NULL,NULL,116,3,2,13,0),(117,NULL,NULL,NULL,NULL,117,3,2,13,0),(118,NULL,NULL,NULL,NULL,118,3,2,13,0),(119,NULL,NULL,NULL,NULL,119,3,2,13,0),(120,NULL,NULL,NULL,NULL,120,3,2,13,0),(121,NULL,NULL,NULL,NULL,121,3,2,13,0),(122,NULL,NULL,NULL,NULL,122,3,2,13,0),(123,NULL,NULL,NULL,NULL,123,3,2,13,0),(124,NULL,NULL,NULL,NULL,124,3,2,13,0),(125,600,600,'NFPA 20',NULL,125,3,2,13,4),(126,300,300,'NFPA 20',NULL,126,3,2,13,2),(127,200,200,'NFPA 20',NULL,127,3,2,13,1),(128,285,285,'NFPA 25',NULL,128,3,2,13,0.5),(129,275,275,'NFPA 25',NULL,129,3,2,13,1),(130,100,100,'35 Ill. Adm. Code 607.104(a) and (b)','153.000',130,3,1,13,0.25),(131,700,700,NULL,NULL,131,3,1,13,3),(132,80,80,NULL,NULL,132,3,1,13,0.5),(133,300,300,'NFPA 72',NULL,133,3,2,13,3),(134,225,225,'NFPA 72',NULL,134,3,2,13,3),(135,200,200,'NFPA 72',NULL,135,3,2,13,2),(136,30,30,'NFPA 10',NULL,136,3,2,13,0),(137,30,30,'NFPA 10',NULL,137,3,2,13,0),(138,60,60,'NFPA 10',NULL,138,3,2,13,0),(139,600,600,'NFPA 25',NULL,139,3,2,13,4),(140,275,275,'NFPA 25',NULL,140,3,2,13,2),(141,225,225,'NFPA 25',NULL,141,3,2,13,2),(142,225,225,'NFPA 25',NULL,142,3,2,13,2),(143,300,300,'NFPA 291',NULL,143,3,2,13,0),(144,200,200,'NFPA 25',NULL,144,3,2,13,1),(145,325,325,'NFPA 13',NULL,145,3,2,13,2),(146,NULL,NULL,NULL,NULL,146,3,2,13,0),(147,NULL,NULL,NULL,NULL,147,3,2,13,0),(148,NULL,NULL,NULL,NULL,148,3,2,13,0),(149,NULL,NULL,NULL,NULL,149,3,2,13,0),(150,NULL,NULL,NULL,NULL,150,3,2,13,0),(151,NULL,NULL,NULL,NULL,151,3,2,13,0),(152,NULL,NULL,NULL,NULL,152,3,2,13,0),(153,NULL,NULL,NULL,NULL,153,3,2,13,0),(154,50,50,'NFPA 25',NULL,154,3,3,13,0),(155,50,50,'NFPA 25',NULL,155,3,3,13,0),(156,95,95,'NFPA 96',NULL,156,3,2,13,1),(157,95,95,'NFPA 96',NULL,157,3,2,13,1),(158,95,95,'NFPA 96',NULL,158,3,2,13,1),(159,NULL,NULL,NULL,NULL,159,3,2,13,0),(160,NULL,NULL,NULL,NULL,160,3,2,13,0),(161,NULL,NULL,NULL,NULL,161,3,2,13,0),(162,NULL,NULL,NULL,NULL,162,3,2,13,0),(163,NULL,NULL,NULL,NULL,163,3,2,13,0),(164,NULL,NULL,NULL,NULL,164,3,2,13,0),(165,NULL,NULL,NULL,NULL,165,3,2,13,0),(166,NULL,NULL,NULL,NULL,166,3,2,13,0),(167,NULL,NULL,NULL,NULL,167,3,2,13,0),(168,NULL,NULL,NULL,NULL,168,3,2,13,0),(169,NULL,NULL,NULL,NULL,169,3,2,13,0),(170,NULL,NULL,NULL,NULL,170,3,2,13,0),(171,NULL,NULL,NULL,NULL,171,3,1,13,0),(172,NULL,NULL,NULL,NULL,172,3,1,13,0),(173,NULL,NULL,NULL,NULL,173,3,1,13,0),(174,NULL,NULL,NULL,NULL,174,3,1,13,0),(175,NULL,NULL,NULL,NULL,175,3,1,13,0),(176,NULL,NULL,NULL,NULL,176,3,1,13,0),(177,NULL,NULL,NULL,NULL,177,3,1,13,0),(178,NULL,NULL,NULL,NULL,178,3,1,13,0),(179,NULL,NULL,NULL,NULL,179,3,1,13,0),(180,NULL,NULL,NULL,NULL,180,3,1,13,0),(181,NULL,NULL,NULL,NULL,181,3,1,13,0),(182,NULL,NULL,NULL,NULL,182,3,1,13,0),(183,NULL,NULL,NULL,NULL,183,3,1,13,0),(184,NULL,NULL,NULL,NULL,184,3,1,13,0),(185,NULL,NULL,NULL,NULL,185,3,1,13,0),(186,NULL,NULL,NULL,NULL,186,3,1,13,0),(187,NULL,NULL,NULL,NULL,187,3,1,13,0),(188,NULL,NULL,NULL,NULL,188,3,1,13,0),(189,NULL,NULL,NULL,NULL,189,3,1,13,0),(190,NULL,NULL,NULL,NULL,190,3,1,13,0),(191,NULL,NULL,NULL,NULL,191,3,1,13,0),(192,NULL,NULL,NULL,NULL,192,3,1,13,0),(193,NULL,NULL,NULL,NULL,193,3,1,13,0),(194,NULL,NULL,NULL,NULL,194,3,1,13,0),(195,NULL,NULL,NULL,NULL,195,3,1,13,0),(196,NULL,NULL,NULL,NULL,196,3,1,13,0),(197,NULL,NULL,NULL,NULL,197,3,1,13,0),(198,NULL,NULL,NULL,NULL,198,3,1,13,0),(199,NULL,NULL,NULL,NULL,199,3,1,13,0),(200,NULL,NULL,NULL,NULL,200,3,1,13,0),(201,NULL,NULL,NULL,NULL,201,3,1,13,0),(202,NULL,NULL,NULL,NULL,202,3,1,13,0),(203,NULL,NULL,NULL,NULL,203,3,1,13,0),(204,NULL,NULL,NULL,NULL,204,3,1,13,0),(205,NULL,NULL,NULL,NULL,205,3,1,13,0),(206,NULL,NULL,NULL,NULL,206,3,1,13,0),(207,NULL,NULL,NULL,NULL,207,3,1,13,0),(208,NULL,NULL,NULL,NULL,208,3,1,13,0),(209,NULL,NULL,NULL,NULL,209,3,1,13,0),(210,NULL,NULL,NULL,NULL,210,3,1,13,0),(211,NULL,NULL,NULL,NULL,211,3,1,13,0),(212,NULL,NULL,NULL,NULL,212,3,1,13,0),(213,NULL,NULL,NULL,NULL,213,3,1,13,0),(214,NULL,NULL,NULL,NULL,214,3,1,13,0),(215,NULL,NULL,NULL,NULL,215,3,1,13,0),(216,NULL,NULL,NULL,NULL,216,3,1,13,0),(217,NULL,NULL,NULL,NULL,217,3,1,13,0),(218,NULL,NULL,NULL,NULL,218,3,1,13,0),(219,NULL,NULL,NULL,NULL,219,3,1,13,0),(220,NULL,NULL,NULL,NULL,220,3,1,13,0),(221,NULL,NULL,NULL,NULL,221,3,1,13,0),(222,NULL,NULL,NULL,NULL,222,3,1,13,0),(223,NULL,NULL,NULL,NULL,223,3,1,13,0),(224,NULL,NULL,NULL,NULL,224,3,1,13,0),(225,NULL,NULL,NULL,NULL,225,3,1,13,0),(226,NULL,NULL,NULL,NULL,226,3,1,13,0),(227,NULL,NULL,NULL,NULL,227,3,1,13,0),(228,NULL,NULL,NULL,NULL,228,3,1,13,0),(229,NULL,NULL,NULL,NULL,229,3,1,13,0),(230,NULL,NULL,NULL,NULL,230,3,1,13,0),(231,NULL,NULL,NULL,NULL,231,3,1,13,0),(232,NULL,NULL,NULL,NULL,232,3,1,13,0),(233,NULL,NULL,NULL,NULL,233,3,1,13,0),(234,NULL,NULL,NULL,NULL,234,3,1,13,0),(235,NULL,NULL,NULL,NULL,235,3,1,13,0),(236,NULL,NULL,NULL,NULL,236,3,1,13,0),(237,NULL,NULL,NULL,NULL,237,3,1,13,0),(238,NULL,NULL,NULL,NULL,238,3,1,13,0),(239,NULL,NULL,NULL,NULL,239,3,1,13,0),(240,NULL,NULL,NULL,NULL,240,3,1,13,0),(241,NULL,NULL,NULL,NULL,241,3,1,13,0),(242,NULL,NULL,NULL,NULL,242,3,1,13,0),(243,NULL,NULL,NULL,NULL,243,3,1,13,0),(244,NULL,NULL,NULL,NULL,244,3,1,13,0),(245,NULL,NULL,NULL,NULL,245,3,1,13,0),(246,NULL,NULL,NULL,NULL,246,3,1,13,0),(247,NULL,NULL,NULL,NULL,247,3,1,13,0),(248,NULL,NULL,NULL,NULL,248,3,1,13,0),(249,NULL,NULL,NULL,NULL,249,3,1,13,0),(250,NULL,NULL,NULL,NULL,250,3,2,13,0),(251,NULL,NULL,NULL,NULL,251,3,2,13,0),(252,NULL,NULL,NULL,NULL,252,3,2,13,0),(253,NULL,NULL,NULL,NULL,253,3,2,13,0),(254,NULL,NULL,NULL,NULL,254,3,2,13,0),(255,NULL,NULL,NULL,NULL,255,3,2,13,0),(256,NULL,NULL,NULL,NULL,256,3,2,13,0),(257,NULL,NULL,NULL,NULL,257,3,2,13,0),(258,NULL,NULL,NULL,NULL,258,3,2,13,0),(259,NULL,NULL,NULL,NULL,259,3,2,13,0),(260,NULL,NULL,NULL,NULL,260,3,2,13,0),(261,NULL,NULL,NULL,NULL,261,3,2,13,0),(262,NULL,NULL,NULL,NULL,262,3,2,13,0),(263,NULL,NULL,NULL,NULL,263,3,2,13,0),(264,NULL,NULL,NULL,NULL,264,3,2,13,0),(265,NULL,NULL,NULL,NULL,265,3,2,13,0),(266,NULL,NULL,NULL,NULL,266,3,2,13,0),(267,75,75,'NFPA 25',NULL,267,3,2,13,1),(268,150,150,'NFPA 25',NULL,268,3,2,13,2);
/*!40000 ALTER TABLE `contractor_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_type`
--

DROP TABLE IF EXISTS `contractor_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_type`
--

LOCK TABLES `contractor_type` WRITE;
/*!40000 ALTER TABLE `contractor_type` DISABLE KEYS */;
INSERT INTO `contractor_type` VALUES (1,'Partner','partner','2018-06-19 10:59:58','2018-06-19 10:59:58',2),(2,'Competitor','competitor','2018-06-19 10:59:58','2018-06-19 10:59:58',3),(3,'My Company','my_company','2018-06-19 10:59:58','2018-06-19 10:59:58',1);
/*!40000 ALTER TABLE `contractor_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_user`
--

DROP TABLE IF EXISTS `contractor_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contractor` int(11) DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `ms_refresh_token` longtext COLLATE utf8_unicode_ci,
  `ms_calendar_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_759F1C39F5B7AF75` (`address_id`),
  KEY `IDX_759F1C398D93D649` (`user`),
  KEY `IDX_759F1C39437BD2EF` (`contractor`),
  CONSTRAINT `FK_759F1C39437BD2EF` FOREIGN KEY (`contractor`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_759F1C398D93D649` FOREIGN KEY (`user`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_759F1C39F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_user`
--

LOCK TABLES `contractor_user` WRITE;
/*!40000 ALTER TABLE `contractor_user` DISABLE KEYS */;
INSERT INTO `contractor_user` VALUES (1,NULL,1,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(2,NULL,2,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(3,NULL,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(4,NULL,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(5,NULL,3,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(6,NULL,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(7,NULL,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(8,NULL,15,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(9,NULL,22,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(10,NULL,23,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(11,NULL,24,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(12,NULL,34,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(13,NULL,35,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,0,NULL,NULL),(14,NULL,9,NULL,NULL,1,'CEO (Superadmin)',NULL,NULL,NULL,NULL,3,0,NULL,NULL),(15,NULL,10,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,3,0,NULL,NULL),(16,NULL,29,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,3,0,NULL,NULL),(17,NULL,30,NULL,NULL,1,'Big Boss',NULL,NULL,NULL,NULL,3,0,NULL,NULL),(18,NULL,31,NULL,NULL,1,'Super Super Admin',NULL,NULL,NULL,NULL,3,0,NULL,NULL),(19,NULL,28,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,3,0,NULL,NULL),(20,NULL,32,NULL,NULL,1,'Tester',NULL,NULL,NULL,NULL,3,0,NULL,NULL),(21,NULL,33,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,3,0,NULL,NULL),(22,44,17,NULL,NULL,1,'Senior Manager','john_doe@email.com','1112222222','1112222222','1112222222',1,0,NULL,NULL),(23,45,18,NULL,NULL,1,'Manager','john_bishop@email.com','1112222222','1112222222','1112222222',1,0,NULL,NULL),(24,46,19,'back-flow-testing.jpg','1500386316.png',1,'Tester','james_hatfield@email.com','1112222222','1112222222','1112222222',1,0,NULL,NULL),(25,47,20,'thmb_tester3.jpg','1500386336.png',1,'Tester','kirk_hammet@email.com','1112222222','1112222222','1112222222',1,0,NULL,NULL);
/*!40000 ALTER TABLE `contractor_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor_user_history`
--

DROP TABLE IF EXISTS `contractor_user_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor_user_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `contractor` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_save` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B40CE4F3F5B7AF75` (`address_id`),
  KEY `IDX_B40CE4F38D93D649` (`user`),
  KEY `IDX_B40CE4F3437BD2EF` (`contractor`),
  KEY `IDX_B40CE4F3BDAFD8C8` (`author`),
  KEY `IDX_B40CE4F3835E0EEE` (`owner_entity_id`),
  CONSTRAINT `FK_B40CE4F3437BD2EF` FOREIGN KEY (`contractor`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_B40CE4F3835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_B40CE4F38D93D649` FOREIGN KEY (`user`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_B40CE4F3BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_B40CE4F3F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor_user_history`
--

LOCK TABLES `contractor_user_history` WRITE;
/*!40000 ALTER TABLE `contractor_user_history` DISABLE KEYS */;
INSERT INTO `contractor_user_history` VALUES (1,NULL,1,5,NULL,1,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(2,NULL,2,5,NULL,2,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(3,NULL,7,5,NULL,3,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(4,NULL,5,5,NULL,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(5,NULL,3,5,NULL,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(6,NULL,6,5,NULL,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(7,NULL,4,5,NULL,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(8,NULL,15,5,NULL,8,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(9,NULL,22,5,NULL,9,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(10,NULL,23,5,NULL,10,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(11,NULL,24,5,NULL,11,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(12,NULL,34,5,NULL,12,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(13,NULL,35,5,NULL,13,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(14,NULL,9,3,NULL,14,NULL,NULL,1,'CEO (Superadmin)',NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(15,NULL,10,3,NULL,15,NULL,NULL,1,'Super Admin',NULL,NULL,NULL,NULL,'2018-06-19 11:00:00'),(16,44,17,1,NULL,22,NULL,NULL,1,'Senior Manager','john_doe@email.com','(111)222-2222','(111)222-2222','(111)222-2222','2018-06-19 11:00:00'),(17,45,18,1,NULL,23,NULL,NULL,1,'Manager','john_bishop@email.com','(111)222-2222','(111)222-2222','(111)222-2222','2018-06-19 11:00:00'),(18,46,19,1,NULL,24,'back-flow-testing.jpg','1500386316.png',1,'Tester','james_hatfield@email.com','(111)222-2222','(111)222-2222','(111)222-2222','2018-06-19 11:00:00'),(19,47,20,1,NULL,25,'thmb_tester3.jpg','1500386336.png',1,'Tester','kirk_hammet@email.com','(111)222-2222','(111)222-2222','(111)222-2222','2018-06-19 11:00:00');
/*!40000 ALTER TABLE `contractor_user_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coordinate`
--

DROP TABLE IF EXISTS `coordinate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coordinate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coordinate`
--

LOCK TABLES `coordinate` WRITE;
/*!40000 ALTER TABLE `coordinate` DISABLE KEYS */;
INSERT INTO `coordinate` VALUES (1,40.7127837,-74.0059413),(2,34.0522342,-118.2436849);
/*!40000 ALTER TABLE `coordinate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `municipality_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CD1DE18AAE6F181C` (`municipality_id`),
  KEY `IDX_CD1DE18A41859289` (`division_id`),
  KEY `IDX_CD1DE18A3414710B` (`agent_id`),
  CONSTRAINT `FK_CD1DE18A3414710B` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_CD1DE18A41859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_CD1DE18AAE6F181C` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,2,1,'Chicago Water Supply Department',NULL),(2,2,2,'Chicago Fire Protection Department',NULL),(3,1,1,'New York Water Supply Department',NULL),(4,1,2,'New York Fire Protection Department',NULL),(5,3,1,'Portland Water Supply Department',NULL),(6,3,2,'Portland Fire Protection Department',NULL);
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_channel`
--

DROP TABLE IF EXISTS `department_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `upload_fee` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `processing_fee` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `owner_agent_id` int(11) DEFAULT NULL,
  `fees_basis_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `devision_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_1F5066A4457EB27E` (`named_id`),
  KEY `IDX_1F5066A422F4C8C8` (`owner_agent_id`),
  KEY `IDX_1F5066A4C6944BCB` (`fees_basis_id`),
  KEY `IDX_1F5066A4AE80F5DF` (`department_id`),
  KEY `IDX_1F5066A45BB97207` (`devision_id`),
  KEY `IDX_1F5066A47E3C61F9` (`owner_id`),
  CONSTRAINT `FK_1F5066A422F4C8C8` FOREIGN KEY (`owner_agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_1F5066A4457EB27E` FOREIGN KEY (`named_id`) REFERENCES `channel_named` (`id`),
  CONSTRAINT `FK_1F5066A45BB97207` FOREIGN KEY (`devision_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_1F5066A47E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `agent_channel` (`id`),
  CONSTRAINT `FK_1F5066A4AE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_1F5066A4C6944BCB` FOREIGN KEY (`fees_basis_id`) REFERENCES `fees_basis` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_channel`
--

LOCK TABLES `department_channel` WRITE;
/*!40000 ALTER TABLE `department_channel` DISABLE KEYS */;
INSERT INTO `department_channel` VALUES (1,1,'0','0',0,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,1,1,NULL,1),(2,2,'0','0',0,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,1,1,NULL,1),(3,3,'0','0',0,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,1,1,NULL,1),(4,4,'2','15',1,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,1,1,1,NULL,1),(5,1,'0','0',0,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,2,2,NULL,1),(6,2,'0','0',0,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,2,2,NULL,1),(7,3,'0','0',0,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,NULL,2,2,NULL,1),(8,4,'2','15',1,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,1,2,2,NULL,1),(9,4,'3','7',1,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,1,5,1,NULL,1),(10,1,'0','0',1,0,'2018-06-19 10:59:59','2018-06-19 10:59:59',NULL,1,6,2,NULL,1);
/*!40000 ALTER TABLE `department_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_channel_dynamic_field_value`
--

DROP TABLE IF EXISTS `department_channel_dynamic_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_channel_dynamic_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  `entity_value_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `department_chanel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_64EFA3443707B0` (`field_id`),
  KEY `IDX_64EFA3D957CA06` (`option_value_id`),
  KEY `IDX_64EFA35BD407E2` (`entity_value_id`),
  KEY `IDX_64EFA3FD76E0D2` (`department_chanel_id`),
  CONSTRAINT `FK_64EFA3443707B0` FOREIGN KEY (`field_id`) REFERENCES `channel_dynamic_field` (`id`),
  CONSTRAINT `FK_64EFA35BD407E2` FOREIGN KEY (`entity_value_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_64EFA3D957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `channel_dropbox_choices` (`id`),
  CONSTRAINT `FK_64EFA3FD76E0D2` FOREIGN KEY (`department_chanel_id`) REFERENCES `department_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_channel_dynamic_field_value`
--

LOCK TABLES `department_channel_dynamic_field_value` WRITE;
/*!40000 ALTER TABLE `department_channel_dynamic_field_value` DISABLE KEYS */;
INSERT INTO `department_channel_dynamic_field_value` VALUES (1,1,NULL,NULL,'mail@mail.com','2018-06-19 10:59:59','2018-06-19 10:59:59',1),(2,2,NULL,48,NULL,'2018-06-19 10:59:59','2018-06-19 10:59:59',2),(3,3,NULL,NULL,'1234567890','2018-06-19 10:59:59','2018-06-19 10:59:59',3),(4,4,NULL,NULL,'http://web.cc.us/upload','2018-06-19 10:59:59','2018-06-19 10:59:59',4),(5,1,NULL,NULL,'mail-2@mail2.com','2018-06-19 10:59:59','2018-06-19 10:59:59',5),(6,2,NULL,48,NULL,'2018-06-19 10:59:59','2018-06-19 10:59:59',6),(7,3,NULL,NULL,'1234567890','2018-06-19 10:59:59','2018-06-19 10:59:59',7),(8,4,NULL,NULL,'http://web-2.cc.us/upload2','2018-06-19 10:59:59','2018-06-19 10:59:59',8),(9,4,NULL,NULL,'http://web.cc.us/upload','2018-06-19 10:59:59','2018-06-19 10:59:59',9),(10,1,NULL,NULL,'mail@mail.com','2018-06-19 10:59:59','2018-06-19 10:59:59',10);
/*!40000 ALTER TABLE `department_channel_dynamic_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_channel_history`
--

DROP TABLE IF EXISTS `department_channel_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_channel_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `owner_agent_id` int(11) DEFAULT NULL,
  `fees_basis_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `devision_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `upload_fee` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `processing_fee` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7BAD093B457EB27E` (`named_id`),
  KEY `IDX_7BAD093B22F4C8C8` (`owner_agent_id`),
  KEY `IDX_7BAD093BC6944BCB` (`fees_basis_id`),
  KEY `IDX_7BAD093BAE80F5DF` (`department_id`),
  KEY `IDX_7BAD093B5BB97207` (`devision_id`),
  KEY `IDX_7BAD093B7E3C61F9` (`owner_id`),
  KEY `IDX_7BAD093BBDAFD8C8` (`author`),
  KEY `IDX_7BAD093B835E0EEE` (`owner_entity_id`),
  CONSTRAINT `FK_7BAD093B22F4C8C8` FOREIGN KEY (`owner_agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_7BAD093B457EB27E` FOREIGN KEY (`named_id`) REFERENCES `channel_named` (`id`),
  CONSTRAINT `FK_7BAD093B5BB97207` FOREIGN KEY (`devision_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_7BAD093B7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `agent_channel` (`id`),
  CONSTRAINT `FK_7BAD093B835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `department_channel` (`id`),
  CONSTRAINT `FK_7BAD093BAE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_7BAD093BBDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_7BAD093BC6944BCB` FOREIGN KEY (`fees_basis_id`) REFERENCES `fees_basis` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_channel_history`
--

LOCK TABLES `department_channel_history` WRITE;
/*!40000 ALTER TABLE `department_channel_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `department_channel_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_history`
--

DROP TABLE IF EXISTS `department_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `municipality_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_save` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9A1398A8AE6F181C` (`municipality_id`),
  KEY `IDX_9A1398A841859289` (`division_id`),
  KEY `IDX_9A1398A83414710B` (`agent_id`),
  KEY `IDX_9A1398A8BDAFD8C8` (`author`),
  KEY `IDX_9A1398A8835E0EEE` (`owner_entity_id`),
  CONSTRAINT `FK_9A1398A83414710B` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`),
  CONSTRAINT `FK_9A1398A841859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_9A1398A8835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_9A1398A8AE6F181C` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`),
  CONSTRAINT `FK_9A1398A8BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_history`
--

LOCK TABLES `department_history` WRITE;
/*!40000 ALTER TABLE `department_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `department_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments_contacts`
--

DROP TABLE IF EXISTS `departments_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments_contacts` (
  `department_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`department_id`,`contact_id`),
  KEY `IDX_9363C481AE80F5DF` (`department_id`),
  KEY `IDX_9363C481E7A1254A` (`contact_id`),
  CONSTRAINT `FK_9363C481AE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_9363C481E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments_contacts`
--

LOCK TABLES `departments_contacts` WRITE;
/*!40000 ALTER TABLE `departments_contacts` DISABLE KEYS */;
INSERT INTO `departments_contacts` VALUES (1,1),(1,4),(2,2),(2,3),(3,17),(3,18),(4,13),(4,16);
/*!40000 ALTER TABLE `departments_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `comments` longtext COLLATE utf8_unicode_ci,
  `note_to_tester` longtext COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `old_device_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `alarm_panel_id` int(11) DEFAULT NULL,
  `source_entity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_editor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_92FB68E457EB27E` (`named_id`),
  KEY `IDX_92FB68E6BF700BD` (`status_id`),
  KEY `IDX_92FB68E9B6B5FBA` (`account_id`),
  KEY `IDX_92FB68E727ACA70` (`parent_id`),
  KEY `IDX_92FB68EC226AA40` (`alarm_panel_id`),
  KEY `IDX_92FB68E7E5A734A` (`last_editor_id`),
  CONSTRAINT `FK_92FB68E457EB27E` FOREIGN KEY (`named_id`) REFERENCES `device_named` (`id`),
  CONSTRAINT `FK_92FB68E6BF700BD` FOREIGN KEY (`status_id`) REFERENCES `device_status` (`id`),
  CONSTRAINT `FK_92FB68E727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_92FB68E7E5A734A` FOREIGN KEY (`last_editor_id`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_92FB68E9B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_92FB68EC226AA40` FOREIGN KEY (`alarm_panel_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_category`
--

DROP TABLE IF EXISTS `device_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_category`
--

LOCK TABLES `device_category` WRITE;
/*!40000 ALTER TABLE `device_category` DISABLE KEYS */;
INSERT INTO `device_category` VALUES (1,'Backflow','backflow','2018-06-19 10:59:58','2018-06-19 10:59:58','backflow.svg',4),(2,'Fire','fire','2018-06-19 10:59:58','2018-06-19 10:59:58','fire.svg',3),(3,'Plumbing','plumbing','2018-06-19 10:59:58','2018-06-19 10:59:58','plumbing.svg',2),(4,'Alarm','alarm','2018-06-19 10:59:58','2018-06-19 10:59:58','alarm.svg',1);
/*!40000 ALTER TABLE `device_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_history`
--

DROP TABLE IF EXISTS `device_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` longtext COLLATE utf8_unicode_ci,
  `note_to_tester` longtext COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AD0CA9D6457EB27E` (`named_id`),
  KEY `IDX_AD0CA9D66BF700BD` (`status_id`),
  KEY `IDX_AD0CA9D69B6B5FBA` (`account_id`),
  KEY `IDX_AD0CA9D6835E0EEE` (`owner_entity_id`),
  KEY `IDX_AD0CA9D6BDAFD8C8` (`author`),
  KEY `IDX_AD0CA9D6727ACA70` (`parent_id`),
  CONSTRAINT `FK_AD0CA9D6457EB27E` FOREIGN KEY (`named_id`) REFERENCES `device_named` (`id`),
  CONSTRAINT `FK_AD0CA9D66BF700BD` FOREIGN KEY (`status_id`) REFERENCES `device_status` (`id`),
  CONSTRAINT `FK_AD0CA9D6727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_AD0CA9D6835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_AD0CA9D69B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_AD0CA9D6BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_history`
--

LOCK TABLES `device_history` WRITE;
/*!40000 ALTER TABLE `device_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_named`
--

DROP TABLE IF EXISTS `device_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_B7652CE012469DE2` (`category_id`),
  KEY `IDX_B7652CE0727ACA70` (`parent_id`),
  CONSTRAINT `FK_B7652CE012469DE2` FOREIGN KEY (`category_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_B7652CE0727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `device_named` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_named`
--

LOCK TABLES `device_named` WRITE;
/*!40000 ALTER TABLE `device_named` DISABLE KEYS */;
INSERT INTO `device_named` VALUES (1,3,'Water Heater','water_heater','2018-06-19 10:59:58','2018-06-19 10:59:58',NULL,0,0),(2,3,'Lift Station','lift_station','2018-06-19 10:59:58','2018-06-19 10:59:58',NULL,0,0),(3,1,'Backflow Device','backflow_device','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0),(4,2,'Fire Extinguisher(s)','fire_extinguishers','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,1,0),(5,2,'Fire Sprinkler System','fire_sprinkler_system','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,3,0),(6,2,'Fire Pump','fire_pump','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,2,0),(7,2,'Antifreeze System','anti_freeze_system','2018-06-19 11:00:01','2018-06-19 11:00:01',5,0,1),(8,2,'Dry Valve System','dry_valve_system','2018-06-19 11:00:01','2018-06-19 11:00:01',5,0,1),(9,2,'Pre-Engineered Fire Suppression System','pre_engineered_fire_suppression_system','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0),(10,2,'Deluge System','deluge_system','2018-06-19 11:00:01','2018-06-19 11:00:01',5,0,1),(11,2,'Pre-Action System','pre_action_system','2018-06-19 11:00:01','2018-06-19 11:00:01',5,0,1),(12,2,'Standpipe System','standpipe_system','2018-06-19 11:00:01','2018-06-19 11:00:01',5,0,1),(13,2,'Fire Alarm Control Panel (FACP)','fire_alarm_control_panel','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0),(14,2,'Fire Extinguisher','fire_extinguisher','2018-06-19 11:00:01','2018-06-19 11:00:01',4,1,0),(15,2,'Riser','riser','2018-06-19 11:00:01','2018-06-19 11:00:01',5,0,1),(16,2,'Section','section','2018-06-19 11:00:01','2018-06-19 11:00:01',5,0,1),(17,2,'FDC Check Valve','fdc_check_valve','2018-06-19 11:00:01','2018-06-19 11:00:01',5,0,1),(18,2,'Air Compressor','air_compressor','2018-06-19 11:00:01','2018-06-19 11:00:01',8,0,2),(19,2,'Low Point','low_point','2018-06-19 11:00:01','2018-06-19 11:00:01',8,0,2);
/*!40000 ALTER TABLE `device_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_status`
--

DROP TABLE IF EXISTS `device_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_status`
--

LOCK TABLES `device_status` WRITE;
/*!40000 ALTER TABLE `device_status` DISABLE KEYS */;
INSERT INTO `device_status` VALUES (1,'Active','active','2018-06-19 11:00:00','2018-06-19 11:00:00'),(2,'Inactive','inactive','2018-06-19 11:00:00','2018-06-19 11:00:00');
/*!40000 ALTER TABLE `device_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field`
--

DROP TABLE IF EXISTS `dynamic_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `validation_id` int(11) DEFAULT NULL,
  `use_label` tinyint(1) DEFAULT '0',
  `label_after` tinyint(1) DEFAULT '0',
  `label_description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_show` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_FC831C2394A4C7D4` (`device_id`),
  KEY `IDX_FC831C23C54C8C93` (`type_id`),
  KEY `IDX_FC831C23A2274850` (`validation_id`),
  CONSTRAINT `FK_FC831C2394A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device_named` (`id`),
  CONSTRAINT `FK_FC831C23A2274850` FOREIGN KEY (`validation_id`) REFERENCES `dynamic_field_validation` (`id`),
  CONSTRAINT `FK_FC831C23C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `dynamic_field_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field`
--

LOCK TABLES `dynamic_field` WRITE;
/*!40000 ALTER TABLE `dynamic_field` DISABLE KEYS */;
INSERT INTO `dynamic_field` VALUES (1,3,2,'Size','backflow_size','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(2,3,2,'Make','backflow_make','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(3,3,1,'Model','model','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(4,3,2,'Type','type','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,0),(5,3,1,'Serial Number','serial_number','2018-06-19 11:00:00','2018-06-19 11:00:00',1,0,0,NULL,1),(6,3,2,'Hazard','hazard','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(7,3,2,'Orientation','orientation','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(8,3,2,'Shut Off Valve(s)','shut_off_valve','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(9,3,2,'Approved Installation','approved_installation','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(10,4,1,'Number of Extinguishers','number_of_extinguishers','2018-06-19 11:00:00','2018-06-19 11:00:00',11,1,1,'extinguisher(s)',1),(11,5,2,'Size','fire_system_size','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(12,5,1,'Number of Risers','number_of_risers','2018-06-19 11:00:00','2018-06-19 11:00:00',3,1,1,'Riser',1),(13,5,1,'Number of Sections','number_of_sections','2018-06-19 11:00:00','2018-06-19 11:00:00',3,1,1,'Sections',1),(14,5,2,'Water Supply Source','water_supply_source','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(15,5,1,'Number of heads','number_of_heads','2018-06-19 11:00:00','2018-06-19 11:00:00',11,1,1,'Heads',1),(16,6,2,'Shaft','shaft','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(17,6,2,'Make','fire_pump_make','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(18,6,1,'Model','model','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(19,6,1,'Serial Number','serial_number','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(20,6,2,'Rated GPM','rated_gpm','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(21,6,1,'Rated PSI','rated_psi','2018-06-19 11:00:00','2018-06-19 11:00:00',11,0,0,NULL,1),(22,6,2,'Pump Listed','pump_listed','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(23,6,2,'Pump Approved','pump_approved','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(24,6,2,'Pump Suction','pump_suction','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(25,6,1,'Impeller Diameter','impeller_diameter','2018-06-19 11:00:00','2018-06-19 11:00:00',13,0,0,NULL,1),(26,6,2,'Discharge Pipe','discharge_pipe','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(27,6,2,'Suction Pipe','suction_pipe','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(28,6,2,'Rotation','rotation','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(29,6,1,'Test Header Location','test_header_location','2018-06-19 11:00:00','2018-06-19 11:00:00',NULL,0,0,NULL,1),(30,7,2,'Size','anti_freeze_size','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(31,7,2,'Anti-Freeze System Type','anti_freeze_system_type','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(32,7,1,'Number of Heads','number_of_heads','2018-06-19 11:00:01','2018-06-19 11:00:01',3,0,0,NULL,1),(33,7,2,'Water Supply Source','water_supply_source','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(34,8,2,'Size','dry_valve_size','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(35,8,1,'Make','dry_valve_make','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(36,8,1,'Model','model','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(37,8,1,'Serial Number','serial_number','2018-06-19 11:00:01','2018-06-19 11:00:01',1,0,0,NULL,1),(38,8,1,'Number of Low Points','number_of_low_points','2018-06-19 11:00:01','2018-06-19 11:00:01',3,0,0,NULL,1),(39,8,2,'Water Supply Source','water_supply_source','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(40,8,1,'ITV Location','itv_location','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,1,0,'ITV Location: ',1),(41,13,2,'FACP Type','facp_type','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(42,13,2,'Make','fire_alarm_panel_make','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(43,13,1,'Model','model','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(44,13,1,'Alarm Monitoring Company','alarm_monitoring_company','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(45,13,1,'Passcode','passcode','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(46,13,1,'Position Number','position_number','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(47,13,1,'Phone Number','phone_number','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(48,14,2,'Fire Extinguisher Size','fire_extinguisher_size','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(49,14,2,'Fire Extinguisher Type','fire_extinguisher_type','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(50,15,1,'Riser Number','riser_number','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(51,15,2,'Size','riser_size','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(52,15,1,'ITV Location','itv_location','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(53,15,2,'Gauge Size IPS','gauge_size_ips','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(54,16,1,'Section Number','section_number','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(55,16,2,'Size','section_size','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(56,16,1,'ITV Location','itv_location','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(57,16,2,'Gauge Size IPS','gauge_size_ips','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(58,17,2,'Size','fdc_check_valve_size','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(59,17,1,'Make','make','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(60,17,2,'Connection','connection','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(61,17,2,'Drain Down','drain_down','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(62,18,1,'Make','make','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(63,18,1,'Model','model','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(64,18,1,'Serial Number','serial_number','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1),(65,18,2,'Volts','volts','2018-06-19 11:00:01','2018-06-19 11:00:01',NULL,0,0,NULL,1);
/*!40000 ALTER TABLE `dynamic_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_dropbox_value`
--

DROP TABLE IF EXISTS `dynamic_field_dropbox_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_dropbox_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `option_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `select_default` tinyint(1) DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1A16D482443707B0` (`field_id`),
  CONSTRAINT `FK_1A16D482443707B0` FOREIGN KEY (`field_id`) REFERENCES `dynamic_field` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=331 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_dropbox_value`
--

LOCK TABLES `dynamic_field_dropbox_value` WRITE;
/*!40000 ALTER TABLE `dynamic_field_dropbox_value` DISABLE KEYS */;
INSERT INTO `dynamic_field_dropbox_value` VALUES (1,1,'1/4\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_1_slash_4'),(2,1,'1/2\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_1_slash_2'),(3,1,'3/4\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_3_slash_4'),(4,1,'1\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_1'),(5,1,'1.25\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_1_dot_25'),(6,1,'1.5\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_1_dot_5'),(7,1,'2\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_2'),(8,1,'2.5\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_2_dot_5'),(9,1,'3\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_3'),(10,1,'4\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_4'),(11,1,'6\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_6'),(12,1,'8\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_8'),(13,1,'10\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_10'),(14,1,'12\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_size_12'),(15,2,'Ames','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_ames'),(16,2,'Febco','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_febco'),(17,2,'Watts','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_watts'),(18,2,'Wilkins','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_wilkins'),(19,2,'Rainbird','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_rainbird'),(20,2,'Conbraco','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_conbraco'),(21,2,'Apollo','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_apollo'),(22,2,'Colt','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_colt'),(23,2,'Maxim','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_maxim'),(24,2,'Beeco','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_beeco'),(25,2,'Backflow Direct','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_backflow_direct'),(26,2,'Flomatic','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_flomatic'),(27,2,'Swing CHK','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_swing_chk'),(28,2,'Zurn','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_zurn'),(29,2,'Verify','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_verify'),(30,2,'Cash Acme','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_cash_acme'),(31,2,'Globe','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_globe'),(32,2,'Cla-Val','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_cla_val'),(33,2,'Buckner','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_buckner'),(34,2,'Hersey','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_hersey'),(35,2,'A.R.I','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_ari'),(36,2,'Other','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_make_other'),(37,4,'RPZ','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_type_rpz'),(38,4,'RPDA','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_type_rpda'),(39,4,'DCDA','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_type_dcda'),(40,4,'DCV','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_type_dcv'),(41,4,'SVB','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_type_svb'),(42,4,'PVB','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_type_pvb'),(43,4,'Unapproved Device','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_type_unapproved_device'),(44,6,'Back Wash Supply','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_back_wash_supply'),(45,6,'Baptism Fountain','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_baptism_fountain'),(46,6,'Boiler Feed','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_boiler_feed'),(47,6,'Brine Tank','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_brine_tank'),(48,6,'Car Wash','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_car_wash'),(49,6,'Chemical Feeder','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_chemical_feeder'),(50,6,'Chiller Make Up','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_chiller_make_up'),(51,6,'Wok Range','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_wok_range'),(52,6,'Chlorine Line','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_chlorine_line'),(53,6,'Coffee Machine','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_coffee_machine'),(54,6,'Cold Feed Pedi Chair','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_cold_feed_pedi_chair'),(55,6,'Cold Water Supply','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_cold_water_supply'),(56,6,'Commercial Feed','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_commercial_feed'),(57,6,'Compressor','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_compressor'),(58,6,'Cooling Tower','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_cooling_tower'),(59,6,'Dental Equipment','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_dental_equipment'),(60,6,'Dialysis','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_dialysis'),(61,6,'Dishwasher','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_dishwasher'),(62,6,'Domestic','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_domestic'),(63,6,'Domestic By-Pass','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_domestic_by_pass'),(64,6,'Drinking Fountain','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_drinking_fountain'),(65,6,'Equipment Make-Up','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_equipment_make_up'),(66,6,'Film Processor','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_film_processor'),(67,6,'Filter System','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_filter_system'),(68,6,'Filter Water','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_filter_water'),(69,6,'Fire Anti-Freeze','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_fire_anti_freeze'),(70,6,'Fire By-Pass','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_fire_by_pass'),(71,6,'Fire Protection','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_fire_protection'),(72,6,'Radiant Heat','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_radiant_heat'),(73,6,'Fountain','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_fountain'),(74,6,'Garbage Disposal','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_garbage_disposal'),(75,6,'Generator','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_second_generator'),(76,6,'Hoodwash','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hoodwash'),(77,6,'Hose Bibb','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hose_bibb'),(78,6,'Hose Connection','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hose_connection'),(79,6,'Hose Reel','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hose_reel'),(80,6,'Hot Feed Pedi Chair','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hot_feed_pedi_chair'),(81,6,'Hot Tub','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hot_tub'),(82,6,'Hot Water Supply','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hot_water_supply'),(83,6,'Humidfier','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_humidfier'),(84,6,'HVAC Equip.','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hvac_equip'),(85,6,'Hydrant','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_hydrant'),(86,6,'Ice Machine','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_ice_machine'),(87,6,'Irrigation','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_irrigation'),(88,6,'Jockey Pump','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_jockey_pump'),(89,6,'Laboratory','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_laboratory'),(90,6,'Laundry Cold','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_laundry_cold'),(91,6,'Laundry Hot','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_laundry_hot'),(92,6,'Make Up Water','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_make_up_water'),(93,6,'Melt Tank','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_melt_tank'),(94,6,'Misters','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_misters'),(95,6,'MRI','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_mri'),(96,6,'Outside Pond','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_outside_pond'),(97,6,'Photo Developer','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_photo_developer'),(98,6,'Plaster Trimmer','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_plaster_trimmer'),(99,6,'Pond Feed','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_pond_feed'),(100,6,'Pool Feed','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_pool_feed'),(101,6,'Portable Hydrant','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_portable_hydrant'),(102,6,'Potable Water','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_potable_water'),(103,6,'Pressure Water','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_pressure_water'),(104,6,'Pressure Washer','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_pressure_washer'),(105,6,'Process Water','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_process_water'),(106,6,'Pressure Rectifier','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_pressure_rectifier'),(107,6,'Pressure Reducer','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_pressure_reducer'),(108,6,'RO System','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_ro_system'),(109,6,'SAA','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_saa'),(110,6,'Sanitation Flush','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_sanitation_flush'),(111,6,'Sillcock','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_sillcock'),(112,6,'Soap Dispenser','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_soap_dispenser'),(113,6,'Soda Equip','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_soda_equip'),(114,6,'Steamer','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_steamer'),(115,6,'Sterilizer','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_sterilizer'),(116,6,'Tank Fill','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_tank_fill'),(117,6,'Tank Washer','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_tank_washer'),(118,6,'Trash Compactor','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_trash_compactor'),(119,6,'Trash Shoot','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_trash_shoot'),(120,6,'Truck Fill','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_truck_fill'),(121,6,'Utility Sink','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_utility_sink'),(122,6,'Vaccum Pump','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_vaccum_pump'),(123,6,'Wall Hydrant','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_wall_hydrant'),(124,6,'Wash Hose','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_wash_hose'),(125,6,'Washroom','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_washroom'),(126,6,'Waste','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_waste'),(127,6,'Water Softner','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_water_softner'),(128,6,'X-Ray','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_x_ray'),(129,6,'Yard Hydrant','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_yard_hydrant'),(130,6,'Floor Heat','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_floor_heat'),(131,6,'Swim Lift','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_swim_lift'),(132,6,'Other','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_hazard_other'),(133,7,'Horizontal','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_orientation_horizontal'),(134,7,'Vertical','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_orientation_vertical'),(135,8,'Ball','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_shut_off_valve_ball'),(136,8,'Gate','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_shut_off_valve_gate'),(137,8,'OS&Y','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_shut_off_valve_os&y'),(138,8,'NRS','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_shut_off_valve_nrs'),(139,8,'Butterfly','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_shut_off_valve_butterfly'),(140,8,'Waffer','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_shut_off_valve_waffer'),(141,8,'Integrated','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_shut_off_valve_integrated'),(142,9,'Yes','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_approved_installation_yes'),(143,9,'No','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'backflow_device_approved_installation_no'),(144,11,'1\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_1'),(145,11,'1.25\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_1_dot_25'),(146,11,'1.5\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_1_dot_5'),(147,11,'2\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_2'),(148,11,'2.5\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_2_dot_5'),(149,11,'3\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_3'),(150,11,'4\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_4'),(151,11,'6\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_6'),(152,11,'8\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_8'),(153,11,'10\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_10'),(154,11,'12\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_size_12'),(155,14,'City','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_water_supply_source_city'),(156,14,'Tank','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_water_supply_source_tank'),(157,14,'Pump','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_water_supply_source_pump'),(158,14,'Pond','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_water_supply_source_pond'),(159,14,'Well','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_water_supply_source_well'),(160,14,'Other','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_system_water_supply_source_other'),(161,16,'Horizontal Split Case','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_shaft_horizontal_split_case'),(162,16,'Vertical In-Line','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_shaft_vertical_in_line'),(163,16,'End Suction','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_shaft_end_suction'),(164,17,'Allis Chalmers','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_allis_chalmers'),(165,17,'Aurora','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_aurora'),(166,17,'Chicago','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_chicago'),(167,17,'Fairbanks Morse','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_fairbanks_morse'),(168,17,'Le Courtney','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_le_courtney'),(169,17,'Patterson','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_patterson'),(170,17,'Peerless','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_peerless'),(171,17,'Redi-Buffalo','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_redi_buffalo'),(172,17,'SPP','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_spp'),(173,17,'Vertical','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_vertical'),(174,17,'Marathon','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_marathon'),(175,17,'1500gpm','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_make_1500gpm'),(176,20,'25','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_25'),(177,20,'50','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_50'),(178,20,'75','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_75'),(179,20,'100','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_100'),(180,20,'150','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_150'),(181,20,'200','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_200'),(182,20,'250','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_250'),(183,20,'300','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_300'),(184,20,'400','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_400'),(185,20,'450','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_450'),(186,20,'500','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_500'),(187,20,'750','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_750'),(188,20,'1000','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_1000'),(189,20,'1250','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_1250'),(190,20,'1500','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_1500'),(191,20,'2000','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_2000'),(192,20,'2500','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_2500'),(193,20,'3000','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_3000'),(194,20,'3500','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_3500'),(195,20,'4000','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_4000'),(196,20,'4500','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_4500'),(197,20,'5000','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rated_gpm_5000'),(198,22,'FM','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_listed_fm'),(199,22,'UL','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_listed_ul'),(200,22,'ULC','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_listed_ulc'),(201,22,'None','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_listed_none'),(202,23,'YES','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_approved_yes'),(203,23,'NO','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_approved_no'),(204,24,'City','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_suction_city'),(205,24,'Tank','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_suction_tank'),(206,24,'Pond','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_suction_pond'),(207,24,'Other','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_pump_suction_other'),(208,26,'1.5\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_1_dot_5'),(209,26,'2\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_2'),(210,26,'2.5\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_2_dot_5'),(211,26,'3\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_3'),(212,26,'4\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_4'),(213,26,'6\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_6'),(214,26,'8\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_8'),(215,26,'10\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_10'),(216,26,'12\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_discharge_pipe_12'),(217,27,'1.5\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_1_dot_5'),(218,27,'2\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_2'),(219,27,'2.5\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_2_dot_5'),(220,27,'3\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_3'),(221,27,'4\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_4'),(222,27,'6\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_6'),(223,27,'8\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_8'),(224,27,'10\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_10'),(225,27,'12\"','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_suction_pipe_12'),(226,28,'Clockwise','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rotation_clockwise'),(227,28,'CC Clockwise','2018-06-19 11:00:00','2018-06-19 11:00:00',0,'fire_pump_rotation_cc_clockwise'),(228,30,'1\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_1'),(229,30,'1.25\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_1_dot_25'),(230,30,'1.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_1_dot_5'),(231,30,'2\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_2'),(232,30,'2.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_2_dot_5'),(233,30,'3\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_3'),(234,30,'4\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_4'),(235,30,'6\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_6'),(236,30,'8\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_8'),(237,30,'10\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_10'),(238,30,'12\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_size_12'),(239,31,'Propylene Glycol','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_type_propylene_glycol'),(240,31,'Glycerin','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_type_glycerin'),(241,33,'City','2018-06-19 11:00:01','2018-06-19 11:00:01',1,'anti_freeze_system_water_supply_source_city'),(242,33,'Tank','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_water_supply_source_tank'),(243,33,'Well','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_water_supply_source_well'),(244,33,'Pond','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_water_supply_source_pond'),(245,33,'Other','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'anti_freeze_system_water_supply_source_other'),(246,34,'1\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_size_1'),(247,34,'1.25\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_size_125'),(248,34,'2\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_size_2'),(249,34,'2.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_size_25'),(250,34,'3\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_size_3'),(251,34,'4\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_size_4'),(252,34,'6\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_size_6'),(253,34,'8\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_size_8'),(254,39,'City','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_water_supply_source_city'),(255,39,'Tank','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_water_supply_source_tank'),(256,39,'Pump','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_water_supply_source_pump'),(257,39,'Pond','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_water_supply_source_pond'),(258,39,'Other','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'dry_valve_water_supply_source_other'),(259,41,'Addressable','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'addressable'),(260,41,'Conventional','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'conventional'),(261,42,'Silent Knight','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_silent_knight'),(262,42,'Fire-Lite','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_fire_lite'),(263,42,'Simplex','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_simplex'),(264,42,'Edwards','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_edwards'),(265,42,'Notifier','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_notifier'),(266,42,'Kiddie','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_kiddie'),(267,42,'Siemens','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_siemens'),(268,42,'ADT','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_adt'),(269,42,'Deelat','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_deelat'),(270,42,'Ademco','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_ademco'),(271,42,'Napco','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_napco'),(272,42,'Potter','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_potter'),(273,42,'Other','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'make_other'),(274,48,'1 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_1_lb'),(275,48,'2 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_2_lb'),(276,48,'2.5 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_2_dot_5_lb'),(277,48,'4 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_4_lb'),(278,48,'5 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_5_lb'),(279,48,'6 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_6_lb'),(280,48,'10 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_10_lb'),(281,48,'20 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_20_lb'),(282,48,'30 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_30_lb'),(283,48,'50 LB','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_size_50_lb'),(284,49,'Class A','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_type_class_a'),(285,49,'Class B','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_type_class_b'),(286,49,'Class C','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_type_class_c'),(287,49,'Class D','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_type_class_d'),(288,49,'Class K','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fire_extinguisher_fire_extinguisher_type_class_k'),(289,51,'1\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_1'),(290,51,'1.25\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_1_dot_25'),(291,51,'1.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_1_dot_5'),(292,51,'2\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_2'),(293,51,'2.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_2_dot_5'),(294,51,'3\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_3'),(295,51,'4\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_4'),(296,51,'6\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_6'),(297,51,'8\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_8'),(298,51,'10\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_10'),(299,51,'12\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_size_12'),(300,53,'1/8\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'riser_gauge_size_ips_1_slash_8'),(301,53,'1/4\"','2018-06-19 11:00:01','2018-06-19 11:00:01',1,'riser_gauge_size_ips_1_slash_4'),(302,55,'1\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_1'),(303,55,'1.25\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_1_dot_25'),(304,55,'1.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_1_dot_5'),(305,55,'2\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_2'),(306,55,'2.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_2_dot_5'),(307,55,'3\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_3'),(308,55,'4\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_4'),(309,55,'6\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_6'),(310,55,'8\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_8'),(311,55,'10\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_10'),(312,55,'12\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_size_12'),(313,57,'1/8\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'section_gauge_size_ips_1_slash_8'),(314,57,'1/4\"','2018-06-19 11:00:01','2018-06-19 11:00:01',1,'section_gauge_size_ips_1_slash_4'),(315,58,'1.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_size_1_dot_5'),(316,58,'2\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_size_2'),(317,58,'2.5\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_size_2_dot_5'),(318,58,'3\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_size_3'),(319,58,'4\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_size_4'),(320,58,'6\"','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_size_6'),(321,60,'Flanged','2018-06-19 11:00:01','2018-06-19 11:00:01',1,'fdc_check_valve_connection_flanged'),(322,60,'Threaded','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_connection_threaded'),(323,60,'Groove','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_connection_groove'),(324,60,'Other','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_connection_other'),(325,61,'Ball Drip','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_drain_down_ball_drip'),(326,61,'Out FDC','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_drain_down_out_fdc'),(327,61,'Other','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'fdc_check_valve_drain_down_other'),(328,65,'120V','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'air_compressor_volts_120'),(329,65,'240V','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'air_compressor_volts_240'),(330,65,'480V','2018-06-19 11:00:01','2018-06-19 11:00:01',0,'air_compressor_volts_480');
/*!40000 ALTER TABLE `dynamic_field_dropbox_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_type`
--

DROP TABLE IF EXISTS `dynamic_field_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_type`
--

LOCK TABLES `dynamic_field_type` WRITE;
/*!40000 ALTER TABLE `dynamic_field_type` DISABLE KEYS */;
INSERT INTO `dynamic_field_type` VALUES (1,'Text','text','2018-06-19 10:59:57','2018-06-19 10:59:57'),(2,'Dropdown','dropdown','2018-06-19 10:59:57','2018-06-19 10:59:57'),(3,'Entity','entity','2018-06-19 10:59:57','2018-06-19 10:59:57');
/*!40000 ALTER TABLE `dynamic_field_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_validation`
--

DROP TABLE IF EXISTS `dynamic_field_validation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_validation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `validation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_validation`
--

LOCK TABLES `dynamic_field_validation` WRITE;
/*!40000 ALTER TABLE `dynamic_field_validation` DISABLE KEYS */;
INSERT INTO `dynamic_field_validation` VALUES (1,'app-font--uppercase','app-font--uppercase',NULL),(2,'app-validation-digits','app-validation-digits',NULL),(3,'app-limit-digits','app-limit-digits',NULL),(4,'app-validation-phone','app-validation-phone',NULL),(5,'app-validation-currency','app-validation-currency',NULL),(6,'app-validation-email','app-validation-email',NULL),(7,'app-validation-maxLength100','app-validation-maxlength100',NULL),(8,'app-validation-dynamicZip','app-validation-dynamiczip',NULL),(9,'app-validation-maxLength100','app-validation-maxlength100',NULL),(10,'app-validation-maxLength100','app-validation-maxlength100',NULL),(11,'app-limit-digits4','app-limit-digits4',NULL),(12,'app-limit-digits3','app-limit-digits3',NULL),(13,'app-limit-number','app-limit-number',NULL);
/*!40000 ALTER TABLE `dynamic_field_validation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_value`
--

DROP TABLE IF EXISTS `dynamic_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `device_id` int(11) DEFAULT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F0552AD2443707B0` (`field_id`),
  KEY `IDX_F0552AD294A4C7D4` (`device_id`),
  KEY `IDX_F0552AD2D957CA06` (`option_value_id`),
  CONSTRAINT `FK_F0552AD2443707B0` FOREIGN KEY (`field_id`) REFERENCES `dynamic_field` (`id`),
  CONSTRAINT `FK_F0552AD294A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_F0552AD2D957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `dynamic_field_dropbox_value` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_value`
--

LOCK TABLES `dynamic_field_value` WRITE;
/*!40000 ALTER TABLE `dynamic_field_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `dynamic_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_field_value_history`
--

DROP TABLE IF EXISTS `dynamic_field_value_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_field_value_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `device_id` int(11) DEFAULT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_68704F9C443707B0` (`field_id`),
  KEY `IDX_68704F9C835E0EEE` (`owner_entity_id`),
  KEY `IDX_68704F9C94A4C7D4` (`device_id`),
  KEY `IDX_68704F9CD957CA06` (`option_value_id`),
  CONSTRAINT `FK_68704F9C443707B0` FOREIGN KEY (`field_id`) REFERENCES `dynamic_field` (`id`),
  CONSTRAINT `FK_68704F9C835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `dynamic_field_value` (`id`),
  CONSTRAINT `FK_68704F9C94A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device_history` (`id`),
  CONSTRAINT `FK_68704F9CD957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `dynamic_field_dropbox_value` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_field_value_history`
--

LOCK TABLES `dynamic_field_value_history` WRITE;
/*!40000 ALTER TABLE `dynamic_field_value_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `dynamic_field_value_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `make` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_calibrated_date` datetime DEFAULT NULL,
  `next_calibrated_date` datetime DEFAULT NULL,
  `company_supplied_gauge` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `contractor_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D338D583C54C8C93` (`type_id`),
  KEY `IDX_D338D583CB3CC8D2` (`contractor_user_id`),
  CONSTRAINT `FK_D338D583C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `equipment_type` (`id`),
  CONSTRAINT `FK_D338D583CB3CC8D2` FOREIGN KEY (`contractor_user_id`) REFERENCES `contractor_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` VALUES (1,1,'Gauge','Turbo','23232232323','2014-12-10 00:00:00','2015-12-10 00:00:00',1,'2018-06-19 11:00:00','2018-06-19 11:00:00',14),(2,1,'Gauge 1','Super','ER244234334','2016-12-10 00:00:00','2017-12-10 00:00:00',1,'2018-06-19 11:00:00','2018-06-19 11:00:00',22),(3,1,'New Gauge','Lightsaber','86654542323','2016-02-19 00:00:00','2017-02-19 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',22),(4,1,'Good Gauge 2','Death Star','45434534535','2017-07-22 00:00:00','2018-07-22 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',NULL),(5,1,'Not Bad Gauge','Millennium Falcon','TP56734334','2017-03-01 00:00:00','2018-03-01 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',23),(6,1,'Very Good Gauge','R2D2','7754543453','2016-12-12 00:00:00','2017-12-12 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',24),(7,1,'Super Good Gauge','R2D2','4545457876','2017-12-12 00:00:00','2018-12-12 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',20),(8,1,'Strange Device. Don\'t touch this','C-3PO','54654645','2018-02-02 00:00:00','2019-02-02 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',19),(9,1,'Super Device for everyone','RoboCop','5667689890','2018-03-03 00:00:00','2019-03-03 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',18),(10,1,'Main Gauge','T-1000','8785544398','2018-04-04 00:00:00','2019-04-04 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',17),(11,1,'Main Gauge for SuperAdmin','T-1000','AA0001AA','2018-05-05 00:00:00','2019-05-05 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',14),(12,1,'Main Gauge for SuperAdmin','T-800','AA0987OI','2018-03-02 00:00:00','2019-03-02 00:00:00',0,'2018-06-19 11:00:00','2018-06-19 11:00:00',16);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment_type`
--

DROP TABLE IF EXISTS `equipment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment_type`
--

LOCK TABLES `equipment_type` WRITE;
/*!40000 ALTER TABLE `equipment_type` DISABLE KEYS */;
INSERT INTO `equipment_type` VALUES (1,'Backflow Equipment','backflow equipment','2018-06-19 10:59:38','2018-06-19 10:59:38');
/*!40000 ALTER TABLE `equipment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `coordinate_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `is_final` tinyint(1) NOT NULL DEFAULT '0',
  `can_finish_wo` tinyint(1) DEFAULT '0',
  `ms_outlook_calendar_event_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3BAE0AA7A76ED395` (`user_id`),
  KEY `IDX_3BAE0AA798BBE953` (`coordinate_id`),
  KEY `IDX_3BAE0AA7C54C8C93` (`type_id`),
  KEY `IDX_3BAE0AA72C1C3467` (`workorder_id`),
  CONSTRAINT `FK_3BAE0AA72C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_3BAE0AA798BBE953` FOREIGN KEY (`coordinate_id`) REFERENCES `coordinate` (`id`),
  CONSTRAINT `FK_3BAE0AA7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_3BAE0AA7C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `event_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_type`
--

LOCK TABLES `event_type` WRITE;
/*!40000 ALTER TABLE `event_type` DISABLE KEYS */;
INSERT INTO `event_type` VALUES (1,'Workorder','workorder'),(2,'Time off','time_off');
/*!40000 ALTER TABLE `event_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees_basis`
--

DROP TABLE IF EXISTS `fees_basis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fees_basis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees_basis`
--

LOCK TABLES `fees_basis` WRITE;
/*!40000 ALTER TABLE `fees_basis` DISABLE KEYS */;
INSERT INTO `fees_basis` VALUES (1,'per Device','per device','2018-06-19 10:59:57','2018-06-19 10:59:57'),(2,'per Site','per site','2018-06-19 10:59:57','2018-06-19 10:59:57');
/*!40000 ALTER TABLE `fees_basis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user`
--

LOCK TABLES `fos_user` WRITE;
/*!40000 ALTER TABLE `fos_user` DISABLE KEYS */;
INSERT INTO `fos_user` VALUES (1,'schur.maksim@gmail.com','schur.maksim@gmail.com','schur.maksim@gmail.com','schur.maksim@gmail.com',1,NULL,'$2y$13$akcq7B7JdEl15AhC/zZ/u.IRrB0ZM.yQ5SSciTkwkM0NFcPNM0ecm',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Maksim','Schur','6159fef895ef6c2737531585fd50c92d'),(2,'oleksandr.korchak@idapgroup.com','oleksandr.korchak@idapgroup.com','oleksandr.korchak@idapgroup.com','oleksandr.korchak@idapgroup.com',1,NULL,'$2y$13$2JhkxBiPbSFbfV6X3SPIqOcDmyfaWF6fjhikZTyUt5E4DYmR7Sldi',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Oleksandr','Korchak','1e4a05736fd3bd3004585c9fa1bbad58'),(3,'daria.novikova@idapgroup.com','daria.novikova@idapgroup.com','daria.novikova@idapgroup.com','daria.novikova@idapgroup.com',1,NULL,'$2y$13$iQynSrbhlY3KvBp69aJUP.CUP/mkPQhZx6UN71kwKxJoQ0KUkf4ta',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Daria','Novikova','be74a06e9ba78a733b87c4a76516a351'),(4,'taras.turbovets@idapgroup.com','taras.turbovets@idapgroup.com','taras.turbovets@idapgroup.com','taras.turbovets@idapgroup.com',1,NULL,'$2y$13$Wr3eDOHQy.0AHsj9pQn8FeJDJeXYdpbdK/pz6i.4Oy6Id1iXDpeFa',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Taras','Turbovets','c0c343c38e4473f4bbff2ce7ad41185c'),(5,'kocmohabty@yandex.ru','kocmohabty@yandex.ru','kocmohabty@yandex.ru','kocmohabty@yandex.ru',1,NULL,'$2y$13$yU63rixnx1.KFrknXsIwTeYOtNOLt.O1gVKXOqddFmIRFFNUMcSwK',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Igor','Yaitskikh','9a90399f7d64bfb18140ce86cf14234a'),(6,'mykola.shevchuk@idapgroup.com','mykola.shevchuk@idapgroup.com','mykola.shevchuk@idapgroup.com','mykola.shevchuk@idapgroup.com',1,NULL,'$2y$13$esPDbBY7YJhxl9QeGvTw6eRA9CZ6SeOv.d3LJAqEdLDNpwDKoGmPO',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Mykola','Shevchuk','29add41d3d7b0fbb048fe163c385429a'),(7,'pavlo.pashchevskyi@idapgroup.com','pavlo.pashchevskyi@idapgroup.com','pavlo.pashchevskyi@idapgroup.com','pavlo.pashchevskyi@idapgroup.com',1,NULL,'$2y$13$c/292qX5rIkeZvkUXDg/T..PKgat.e1PHx79L8SWw4ldl2yGU9KAi',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Pavlo','Pashchevskyi','2521095d21763432cada21256dbab1f9'),(8,'anastasiya.mashoshyna@idapgroup.com','anastasiya.mashoshyna@idapgroup.com','anastasiya.mashoshyna@idapgroup.com','anastasiya.mashoshyna@idapgroup.com',1,NULL,'$2y$13$amVzkb94khcxKfyrm5oPBuNvUm4jNxe5kqCsuq9TJsJMhDFNQTmEm',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Anastasiya','Mashoshyna','55b628843d4b52234eb99445daea2a6c'),(9,'dharbut@gmail.com','dharbut@gmail.com','dharbut@gmail.com','dharbut@gmail.com',1,NULL,'$2y$13$ZwtwT77QG0KP2TR/XIOjy.yHjgAlYR9jeDxWdmIFMnJqEdHW3cEyS',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Dan','Harbut','f30067ae473f9e88a912940510a045f8'),(10,'superadmin@americanbackflowprevention.com','superadmin@americanbackflowprevention.com','superadmin@americanbackflowprevention.com','superadmin@americanbackflowprevention.com',1,NULL,'$2y$13$yBDth5OwQ/nLXLRKM9Ns8ej8UuT2WNeg9h/Plk7MQR2FSuuVz3g8e',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','ABFP','SuperAdmin','8422edf8e3021d1035240ac1f6ce4040'),(11,'api@test.com','api@test.com','api@test.com','api@test.com',1,NULL,'$2y$13$AI4lPhE7DNAxgi260eJBveeS7ZkQiw7WwcG04mofk4875hsOrP362',NULL,NULL,NULL,'a:1:{i:0;s:8:\"ROLE_API\";}','Maksim','Schur','51f6887a677a03f5609c7dd0bbc0560c'),(12,'test_api1@test.com','test_api1@test.com','test_api1@test.com','test_api1@test.com',1,NULL,'$2y$13$2EyqIEUoCtDsWpWY4D1ZpuGI5OiOZN5kp1esypw8VlNcKpfOWua.W',NULL,NULL,NULL,'a:1:{i:0;s:8:\"ROLE_API\";}','Test1','Test1','bb4889594523023953b143dae4fafebe'),(13,'test_api2@test.com','test_api2@test.com','test_api2@test.com','test_api2@test.com',1,NULL,'$2y$13$ekjnHMEjRAI4lw365y7b3.w0O0e1zOrWxzZO.UY25WpwETzU8Qfsi',NULL,NULL,NULL,'a:1:{i:0;s:8:\"ROLE_API\";}','Test2','Test2','f5900c218e40d62c9c61ea73360c7d16'),(14,'api2@test.com','api2@test.com','api2@test.com','api2@test.com',1,NULL,'$2y$13$ciXweotBpgftTUxN8WbsauDIw5qywPy.p6kb0GTk/iZfXxkyg9Pri',NULL,NULL,NULL,'a:1:{i:0;s:8:\"ROLE_API\";}','Igor','Yaitskikh','da4b365d321394e3d9bbc8bf9d477e1c'),(15,'ibabich88i@gmail.com','ibabich88i@gmail.com','ibabich88i@gmail.com','ibabich88i@gmail.com',1,NULL,'$2y$13$Nb2Pj1a7OU5BPVBqd1aeC.YXeuuiEd1JBvcvnqbInc5X8m7dmVN4W',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Alex','Babich','cbb766a70d8b4f989480be92d5f5c765'),(16,'john.doe@example.com','john.doe@example.com','john.doe@example.com','john.doe@example.com',1,NULL,'$2y$13$4FswrfU4qIzahi5XVndsde2iDckFAkYp0BaMyXlHt3KKcglwaF5Z6',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','John','Doe','b75c17dc79be4b50a08a805c38fd6da4'),(17,'john_doe_admin@email.com','john_doe_admin@email.com','john_doe_admin@email.com','john_doe_admin@email.com',1,NULL,'$2y$13$YbQfwoOzNSfHfucVmmR7.uFFWAkVq9XwHUxGy8k50bcVAYTCtYqDS',NULL,NULL,NULL,'a:2:{i:0;s:10:\"ROLE_ADMIN\";i:1;s:12:\"ROLE_CONTACT\";}','John','Doe','bb0bbbff6ab0f7e6c7e88d70f9f7df94'),(18,'john_bishop@email.com','john_bishop@email.com','john_bishop@email.com','john_bishop@email.com',1,NULL,'$2y$13$M/Cyd5ZL8VYyf4SAuugpBelL9hZSBCilY0cDUEzKcpsemUwIieSra',NULL,NULL,NULL,'a:1:{i:0;s:12:\"ROLE_CONTACT\";}','John','Bishop','38ab470677c572183b932a2590a550b2'),(19,'james_hatfield@email.com','james_hatfield@email.com','james_hatfield@email.com','james_hatfield@email.com',1,NULL,'$2y$13$YOVtWAOYHRDz7BhRe26/Q.E9/ymBNxkiziaJuQJFDqSwWYrVlY9W6',NULL,NULL,NULL,'a:1:{i:0;s:11:\"ROLE_TESTER\";}','James','Hatfield','4f6e10232a562f45b96fce30929b5407'),(20,'kirk_hammet@email.com','kirk_hammet@email.com','kirk_hammet@email.com','kirk_hammet@email.com',1,NULL,'$2y$13$B4eHjsq0MwTXiBITDoXh..PDVe5teJ1cbiLstHL7r/Lxu6/QnSYPa',NULL,NULL,NULL,'a:1:{i:0;s:11:\"ROLE_TESTER\";}','Kirk','Hammet','daf6068e4ffca52334d355b73c920e2c'),(21,'loes5307@comcast.net','loes5307@comcast.net','loes5307@comcast.net','loes5307@comcast.net',1,NULL,'$2y$13$/kxjRQce3RN26dJ7zoC1QuWfG8JZ9rqTdWtNS0AiZSj5Q1XUgDJty',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','David','Loes','d3a8d5ec41ce6fd2cb84f2f9de324923'),(22,'svetlana.kovalchuk@idapgroup.com','svetlana.kovalchuk@idapgroup.com','svetlana.kovalchuk@idapgroup.com','svetlana.kovalchuk@idapgroup.com',1,NULL,'$2y$13$ozniwA1CD5RC6A2k2gjoguXZYqvEP.kmf78NxE4EFk1p6LLfZaQaa',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Svetlana','Kovalchuk','1811c133ff4dbccdf09c76239f5dc8c7'),(23,'yurii@test.com','yurii@test.com','yurii@test.com','yurii@test.com',1,NULL,'$2y$13$T8z52AxiM7F/DRNgOrOT.OI940lHiJutV4C/UDF1439Mz/YTl0lt6',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Yurii','Trokhymchuk','a3d32a15a19c32ce8a12d57419faac68'),(24,'vlad@test.com','vlad@test.com','vlad@test.com','vlad@test.com',1,NULL,'$2y$13$tKqyfm4/MlN6VSktcakcEu2ffEJ.P1yabc/4N8KZTBZbL0ag/K6pa',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Vlad','Emets','e19f247184b096c75f661bb3686cb927'),(25,'nosik@test.com','nosik@test.com','nosik@test.com','nosik@test.com',1,NULL,'$2y$13$sJ8nXdyA2rPAtrZHBZqfMOYtLIb0RhBVYR.Kjy768RNYwWfYnjkHC',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',NULL,NULL,'e8ab7bfb0fc3dd9a76eb007bdd5a98af'),(26,'nikityuk@test.com','nikityuk@test.com','nikityuk@test.com','nikityuk@test.com',1,NULL,'$2y$13$hkkzrUTd2.hfT2LfFe2MKuNr6agLhr5DZvh48E.Ah3dZuVBKH2LfO',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',NULL,NULL,'b462904e68d86c373db32e7f32f09ad4'),(27,'leesogonich@test.com','leesogonich@test.com','leesogonich@test.com','leesogonich@test.com',1,NULL,'$2y$13$poAZyIxT5kB1j.rRPlBdqutNhUd91RtHL04.KHH6HPPdfisShQ2KK',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}',NULL,NULL,'90594df7e4dda39881db4e1449d0092f'),(28,'markzuck@gmail.com','markzuck@gmail.com','markzuck@gmail.com','markzuck@gmail.com',1,NULL,'$2y$13$N0AlBRxaKL6uMXqjA.MCEu2JVzsL6wBc0BbIqiyrnPwY/rpsocN0u',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Mark','Zuckerberg','a6645cd0eb6a8faef0af073a035a2a5b'),(29,'elonmusk@gmail.com','elonmusk@gmail.com','elonmusk@gmail.com','elonmusk@gmail.com',1,NULL,'$2y$13$nvheQ7YI/JhZQkm4yyRKCOcLZDFCOVmwWkWJUmqzaxfwPN0p/ChtC',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Mark','Elon','4adb50e4e0e82ae2e2cfea76ee6410cc'),(30,'billgates@gmail.com','billgates@gmail.com','billgates@gmail.com','billgates@gmail.com',1,NULL,'$2y$13$lPsFPQ9qUwGvtWWtcq8ee.g9/C79A190QP3o/4cUFV/Br.fTPu9ji',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Bill','Gates','81897f7f083a6ae08292d63c95b5bee0'),(31,'sergeybrin@gmail.com','sergeybrin@gmail.com','sergeybrin@gmail.com','sergeybrin@gmail.com',1,NULL,'$2y$13$CbxdpMzm6XzppidLfU3KreboPmoBDagPjTMvtr5c4Nnjg.RPPDYka',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Sergey','Brin','f3d4a893ca39e093342a01255d26794d'),(32,'gabenewell@gmail.com','gabenewell@gmail.com','gabenewell@gmail.com','gabenewell@gmail.com',1,NULL,'$2y$13$nYiTHVxqXUjlwitzz0/p1u6XYeBAUym/yFbUUp2.98MUDHVc8j54O',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Gabe','Newell','bc426d343e78426633432477c8bb7941'),(33,'jakfresko@gmail.com','jakfresko@gmail.com','jakfresko@gmail.com','jakfresko@gmail.com',1,NULL,'$2y$13$4uTKVfK.wiUjDOzoAQUubefYPfwsNkdf.9fa.ciowpxtQfVuDW5q2',NULL,NULL,NULL,'a:3:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";i:2;s:12:\"ROLE_CONTACT\";}','Jak','Fresko','7785b6206af5c29f085ebdc295f9692b'),(34,'vdovychenko.dmytro@gmail.com','vdovychenko.dmytro@gmail.com','vdovychenko.dmytro@gmail.com','vdovychenko.dmytro@gmail.com',1,NULL,'$2y$13$5ll9pU/ijwjBasX6FAVzqu0GHY4gR.MIx2QFbCln3vZTu5rLDbMRG',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Dmytro','Vdovychenko','743971ce148a9630dccfc9a2a48680a8'),(35,'maksym.makartsov@idapgroup.com','maksym.makartsov@idapgroup.com','maksym.makartsov@idapgroup.com','maksym.makartsov@idapgroup.com',1,NULL,'$2y$13$L//XAaVHMKu8umDVxnAvtOqIDYh3aTWEhwR.CCNx9vpJV0hNcRzaC',NULL,NULL,NULL,'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}','Maksym','Makartsov','69788cc2fcad0dc5d9cf325398b72717');
/*!40000 ALTER TABLE `fos_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frozen_proposal`
--

DROP TABLE IF EXISTS `frozen_proposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frozen_proposal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C26948E59B6B5FBA` (`account_id`),
  KEY `IDX_C26948E5E92F8F78` (`recipient_id`),
  KEY `IDX_C26948E561220EA6` (`creator_id`),
  CONSTRAINT `FK_C26948E561220EA6` FOREIGN KEY (`creator_id`) REFERENCES `contractor_user_history` (`id`),
  CONSTRAINT `FK_C26948E59B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account_history` (`id`),
  CONSTRAINT `FK_C26948E5E92F8F78` FOREIGN KEY (`recipient_id`) REFERENCES `account_contact_person_history` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frozen_proposal`
--

LOCK TABLES `frozen_proposal` WRITE;
/*!40000 ALTER TABLE `frozen_proposal` DISABLE KEYS */;
/*!40000 ALTER TABLE `frozen_proposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frozen_service_for_proposal`
--

DROP TABLE IF EXISTS `frozen_service_for_proposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frozen_service_for_proposal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_history_id` int(11) DEFAULT NULL,
  `frozen_proposal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C57D54678854BDE6` (`service_history_id`),
  KEY `IDX_C57D5467C5A6CB48` (`frozen_proposal_id`),
  CONSTRAINT `FK_C57D54678854BDE6` FOREIGN KEY (`service_history_id`) REFERENCES `service_history` (`id`),
  CONSTRAINT `FK_C57D5467C5A6CB48` FOREIGN KEY (`frozen_proposal_id`) REFERENCES `frozen_proposal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frozen_service_for_proposal`
--

LOCK TABLES `frozen_service_for_proposal` WRITE;
/*!40000 ALTER TABLE `frozen_service_for_proposal` DISABLE KEYS */;
/*!40000 ALTER TABLE `frozen_service_for_proposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_error_messages`
--

DROP TABLE IF EXISTS `import_error_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_error_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_error_messages`
--

LOCK TABLES `import_error_messages` WRITE;
/*!40000 ALTER TABLE `import_error_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_error_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workorder_id` int(11) DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6A2F2F952C1C3467` (`workorder_id`),
  CONSTRAINT `FK_6A2F2F952C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `letter`
--

DROP TABLE IF EXISTS `letter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `letter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `emails` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8E02EE0AF4792058` (`proposal_id`),
  KEY `IDX_8E02EE0A78CED90B` (`from_id`),
  KEY `IDX_8E02EE0A30354A65` (`to_id`),
  CONSTRAINT `FK_8E02EE0A30354A65` FOREIGN KEY (`to_id`) REFERENCES `account_contact_person_history` (`id`),
  CONSTRAINT `FK_8E02EE0A78CED90B` FOREIGN KEY (`from_id`) REFERENCES `contractor_user_history` (`id`),
  CONSTRAINT `FK_8E02EE0AF4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `letter`
--

LOCK TABLES `letter` WRITE;
/*!40000 ALTER TABLE `letter` DISABLE KEYS */;
/*!40000 ALTER TABLE `letter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses`
--

DROP TABLE IF EXISTS `licenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `renewal_date` datetime DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `contractor_user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_7F320F3F457EB27E` (`named_id`),
  KEY `IDX_7F320F3F5D83CC1` (`state_id`),
  KEY `IDX_7F320F3FCB3CC8D2` (`contractor_user_id`),
  CONSTRAINT `FK_7F320F3F457EB27E` FOREIGN KEY (`named_id`) REFERENCES `licenses_named` (`id`),
  CONSTRAINT `FK_7F320F3F5D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_7F320F3FCB3CC8D2` FOREIGN KEY (`contractor_user_id`) REFERENCES `contractor_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses`
--

LOCK TABLES `licenses` WRITE;
/*!40000 ALTER TABLE `licenses` DISABLE KEYS */;
INSERT INTO `licenses` VALUES (1,1,1,'863650921346','2018-09-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',24,0),(2,2,NULL,'761355732338','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',25,0),(3,3,6,'863650921346','2018-08-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',24,0),(4,4,NULL,'521355789531','2018-10-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',25,0),(5,5,2,'345673289519','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',24,0),(6,2,NULL,'764355232345','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',19,0),(7,1,NULL,'784325537345','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',16,0),(8,1,NULL,'712325537115','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',21,0),(9,2,NULL,'712325537115','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',21,0),(10,1,NULL,'2342344323112','2018-10-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',17,0),(11,2,NULL,'612327767115','2018-11-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',17,0),(12,1,NULL,'21212124356','2018-10-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',18,0),(13,2,NULL,'12544565990','2018-11-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',18,0),(14,1,NULL,'99000876552','2018-10-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',19,0),(15,1,NULL,'99000876552','2018-10-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',20,0),(16,2,NULL,'345345343212','2018-11-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',20,0),(17,3,NULL,'712325537115','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',21,0),(18,4,NULL,'712325537115','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',21,0),(19,3,NULL,'784325537345','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',16,0),(20,4,NULL,'784325537345','2018-07-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',16,0),(21,1,NULL,'2324354435455','2018-10-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',14,0),(22,2,NULL,'3324565677231','2018-08-19 11:00:01','2018-06-19 11:00:01','2018-06-19 11:00:01',14,0);
/*!40000 ALTER TABLE `licenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_category`
--

DROP TABLE IF EXISTS `licenses_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_category`
--

LOCK TABLES `licenses_category` WRITE;
/*!40000 ALTER TABLE `licenses_category` DISABLE KEYS */;
INSERT INTO `licenses_category` VALUES (1,'Backflow','backflow','backflow_blue.svg','2018-06-19 11:00:00','2018-06-19 11:00:00'),(2,'Fire','fire','fire_blue.svg','2018-06-19 11:00:00','2018-06-19 11:00:00'),(3,'Plumbing','plumbing','tool_blue.svg','2018-06-19 11:00:00','2018-06-19 11:00:00'),(4,'Alarm','alarm','alarm_blue.svg','2018-06-19 11:00:00','2018-06-19 11:00:00'),(5,'Driver','driver','circle.svg','2018-06-19 11:00:00','2018-06-19 11:00:00');
/*!40000 ALTER TABLE `licenses_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_category_relations`
--

DROP TABLE IF EXISTS `licenses_category_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_category_relations` (
  `licensed_named_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`licensed_named_id`,`category_id`),
  KEY `IDX_4FAEBA5D43EC7B4F` (`licensed_named_id`),
  KEY `IDX_4FAEBA5D12469DE2` (`category_id`),
  CONSTRAINT `FK_4FAEBA5D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `licenses_category` (`id`),
  CONSTRAINT `FK_4FAEBA5D43EC7B4F` FOREIGN KEY (`licensed_named_id`) REFERENCES `licenses_named` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_category_relations`
--

LOCK TABLES `licenses_category_relations` WRITE;
/*!40000 ALTER TABLE `licenses_category_relations` DISABLE KEYS */;
INSERT INTO `licenses_category_relations` VALUES (1,1),(2,2),(3,3),(4,2),(4,4),(5,5);
/*!40000 ALTER TABLE `licenses_category_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_dropbox_choices`
--

DROP TABLE IF EXISTS `licenses_dropbox_choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_dropbox_choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `option_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `select_default` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EF4F4CF4443707B0` (`field_id`),
  CONSTRAINT `FK_EF4F4CF4443707B0` FOREIGN KEY (`field_id`) REFERENCES `licenses_dynamic_field` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_dropbox_choices`
--

LOCK TABLES `licenses_dropbox_choices` WRITE;
/*!40000 ALTER TABLE `licenses_dropbox_choices` DISABLE KEYS */;
INSERT INTO `licenses_dropbox_choices` VALUES (1,1,'Fire Alarms',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(2,1,'Inspection and Testing alarms',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(3,1,'Special Hazards',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(4,1,'Inspection and Testing WBS',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(5,1,'Water Based Systems Layout',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(6,2,'I',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(7,2,'II',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(8,2,'III',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(9,2,'IV',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(10,3,'Portable Fire Extinguishers',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(11,3,'Pre-Engineered Kitchen Fire Extinguishing Systems',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(12,3,'Pre-Engineered Industrial Fire Extinguishing Systems',0,'2018-06-19 11:00:01','2018-06-19 11:00:01'),(13,3,'Engineered Fire Suppression Systems',0,'2018-06-19 11:00:01','2018-06-19 11:00:01');
/*!40000 ALTER TABLE `licenses_dropbox_choices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_dynamic_field`
--

DROP TABLE IF EXISTS `licenses_dynamic_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_dynamic_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `license_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `is_show` tinyint(1) DEFAULT '1',
  `use_label` tinyint(1) DEFAULT '0',
  `label_after` tinyint(1) DEFAULT '0',
  `label_description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1C2977D4460F904B` (`license_id`),
  KEY `IDX_1C2977D4C54C8C93` (`type_id`),
  CONSTRAINT `FK_1C2977D4460F904B` FOREIGN KEY (`license_id`) REFERENCES `licenses_named` (`id`),
  CONSTRAINT `FK_1C2977D4C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `dynamic_field_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_dynamic_field`
--

LOCK TABLES `licenses_dynamic_field` WRITE;
/*!40000 ALTER TABLE `licenses_dynamic_field` DISABLE KEYS */;
INSERT INTO `licenses_dynamic_field` VALUES (1,4,2,'NICET Certification','nicet certification','2018-06-19 11:00:01','2018-06-19 11:00:01',1,0,0,NULL),(2,4,2,'NICET Level','nicet level','2018-06-19 11:00:01','2018-06-19 11:00:01',1,1,0,'Level: '),(3,2,2,'NAFED Certification','nafed certification','2018-06-19 11:00:01','2018-06-19 11:00:01',1,0,0,NULL);
/*!40000 ALTER TABLE `licenses_dynamic_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_dynamic_field_value`
--

DROP TABLE IF EXISTS `licenses_dynamic_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_dynamic_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `license_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9D146CEE443707B0` (`field_id`),
  KEY `IDX_9D146CEE460F904B` (`license_id`),
  KEY `IDX_9D146CEED957CA06` (`option_value_id`),
  CONSTRAINT `FK_9D146CEE443707B0` FOREIGN KEY (`field_id`) REFERENCES `licenses_dynamic_field` (`id`),
  CONSTRAINT `FK_9D146CEE460F904B` FOREIGN KEY (`license_id`) REFERENCES `licenses` (`id`),
  CONSTRAINT `FK_9D146CEED957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `licenses_dropbox_choices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_dynamic_field_value`
--

LOCK TABLES `licenses_dynamic_field_value` WRITE;
/*!40000 ALTER TABLE `licenses_dynamic_field_value` DISABLE KEYS */;
INSERT INTO `licenses_dynamic_field_value` VALUES (1,1,4,NULL,'2018-06-19 11:00:04','2018-06-19 11:00:04',2),(2,2,4,NULL,'2018-06-19 11:00:04','2018-06-19 11:00:04',9),(3,3,2,NULL,'2018-06-19 11:00:04','2018-06-19 11:00:04',11);
/*!40000 ALTER TABLE `licenses_dynamic_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenses_named`
--

DROP TABLE IF EXISTS `licenses_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenses_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenses_named`
--

LOCK TABLES `licenses_named` WRITE;
/*!40000 ALTER TABLE `licenses_named` DISABLE KEYS */;
INSERT INTO `licenses_named` VALUES (1,'Backflow License','backflow license','2018-06-19 11:00:00','2018-06-19 11:00:00'),(2,'NAFED License','nafed license','2018-06-19 11:00:00','2018-06-19 11:00:00'),(3,'Plumbers License','plumbers license','2018-06-19 11:00:00','2018-06-19 11:00:00'),(4,'NICET License','nicet license','2018-06-19 11:00:00','2018-06-19 11:00:00'),(5,'Drivers License','drivers license','2018-06-19 11:00:00','2018-06-19 11:00:00');
/*!40000 ALTER TABLE `licenses_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__gallery`
--

DROP TABLE IF EXISTS `media__gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__gallery`
--

LOCK TABLES `media__gallery` WRITE;
/*!40000 ALTER TABLE `media__gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `media__gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__gallery_media`
--

DROP TABLE IF EXISTS `media__gallery_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  KEY `IDX_80D4C541EA9FDD75` (`media_id`),
  CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__gallery_media`
--

LOCK TABLES `media__gallery_media` WRITE;
/*!40000 ALTER TABLE `media__gallery_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media__gallery_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__media`
--

DROP TABLE IF EXISTS `media__media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media__media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_identifier` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__media`
--

LOCK TABLES `media__media` WRITE;
/*!40000 ALTER TABLE `media__media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media__media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6BD307FC54C8C93` (`type_id`),
  CONSTRAINT `FK_B6BD307FC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `message_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,1,'warning.svg','no_autorizer','Authorizer for some divisions under this account is missing'),(2,1,'warning.svg','authorizer_invalid_address','Some Authorizers under this Account have invalid mailing address.'),(3,1,'warning.svg','no_municipality_compliance_channel_backflow','Information about upload fees is missing for some Divisions under this Account (some Municipalities related to this Account have no Compliance Channels for some divisions under this account).'),(4,2,'warning.svg','site_has_own_authorizer','This Site Account has its own Authoriser for some Divisions even thought it is under a Group Account.'),(5,1,'warning.svg','service_no_fee','Service Fee information for some of the Services under this Account is missing.');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_type`
--

DROP TABLE IF EXISTS `message_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_type`
--

LOCK TABLES `message_type` WRITE;
/*!40000 ALTER TABLE `message_type` DISABLE KEYS */;
INSERT INTO `message_type` VALUES (1,'Error','error'),(2,'Warning','warning');
/*!40000 ALTER TABLE `message_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('0001_Create_User_Entity'),('0002_Add_Field_Access_Token_For_User_Entity'),('0003_Create_Account_Entity'),('0004_Create_Account_Type_Entity'),('0005_Create_State_Entity'),('0006_Add_Field_Parent_For_Account_Entity'),('0007_Change_Not_Required_Field_Notes_For_Account'),('0008_Create_Client_Type_Entity'),('0009_Add_Deleted_Flag_To_Account_Entity'),('0010_Add_AccountHistory_Entity'),('0011_Update_AccountHistory_Entity_Author_Nullable_True'),('0012_Add_Code_To_State_Entity'),('0013_Create_Contact_Person_Entity'),('0014_Add_Field_Email_Contact_Person_Entity'),('0015_Add_Address_Entity'),('0016_Add_Field_Addreses_For_Contact_Person_Entity'),('0017_Add_AddressType_Entity'),('0018_Update_Address_Entity_Add_AddressType'),('0019_Null_Default_Field_Notes_For_ContactPerson'),('0020_Add_Null_Default_Value_For_Fields_ContactPerson'),('0021_Add_Null_Default_Value_For_Fields_Address'),('0022_Add_Null_Default_Value_For_Fields_Phone_And_Fax_Address'),('0023_Create_ContactPersonHistory_Entity'),('0024_Updated_ContactPerson_Entity_Change_Notes_Type'),('0025_Update_ContactPerson_Entity_Add_Deleted_Property'),('0026_Create_Company_Entity'),('0027_Create_CompanyHistory_Entity'),('0028_Update_Company_Entity_Change_City_Size'),('0029_Update_Company_Entity_Set_Website_Nullable_True'),('0030_Update_CompanyHistory_Entity_Set_Website_Nullable_True'),('0031_Update_Company_Entity_Set_Address_City_Zip_Nullable_True'),('0032_Update_Company_History_Entity_Set_Address_City_Zip_Nullable_True'),('0035_Add_Field_Company_For_Account_Entity'),('0036_Add_Field_Company_For_ContactPerson_Entity'),('0037_Add_Field_Company_For_ContactPersonHistory_Entity'),('0038_Add_Field_Company_For_AccountHistory_Entity'),('0039_Create_Role_Entity'),('0040_Create_Responsibility_Entity'),('0041_Create_Account_Contact_Person_Entity'),('0042_Create_Account_Contact_Person_History_Entity'),('0043_Create_DeviceCategory_Entity'),('0044_Create_DynamicFieldType_Entity'),('0045_Create_DiveceNamed_Entity_With_Relation_To_DeviceCategory_Entity'),('0046_Create_DeviceStatus_Entity'),('0047_Create_DynamicField_Add_Relation_To_DeviceNamed_And_DynamicField_Type'),('0048_Create_DynamicFieldValue_Entity_And_Relation_To_DynamicField'),('0049_Create_Device_Entity_And_Relate_To_All'),('0050_Update_Device_Entity_Add_Comment_And_NoteToTester'),('0051_Add_Icon_To_DeviceCategory_Entity'),('0052_Add_Deleted_Property_To_Device_Entity'),('0053_Create_DynamicFieldValueHistory_Entity'),('0054_Create_DeviceHistory_Entity_And_Relate_With_DynamicFieldHistory'),('0055_Create_DynamicFieldDropboxValue_And_Linked_To_DynamicField'),('0056_Add_NumContact_sourceEntity_SourceID_To_ContactPerson_Entity'),('0056_Linked_DynamicFieldValue_To_DynamicFieldDropboxChoices'),('0057_Linked_DynamicFieldValueHistory_To_DynamicFieldDropboxValue'),('0058_Create_DynamicFieldValidation_Entity'),('0059_Added_Parent_And_Children_Relation_To_Device_Entity'),('0060_Added_Parent_And_Children_Relation_To_DeviceHistory_Entity'),('0061_Add_Parent_Relation_To_DeviceNamed_Entity'),('0062_Add_Validation_To_DynamicField_Entity'),('0063_Add_SelectDefault_To_DynamicFieldDropboxValue_Entity'),('0064_Adde_UseLabel_LabelAfter_LabelDescription_To_DynamicField_Entity'),('0065_Add_IsShow_To_DynamicField_Entity'),('0065_Create_Contractor_Entity'),('0066_Add_Field_Address_For_Contractor'),('0067_Create_ContractorType_Entity'),('0068_Add_Field_Type_For_Contractor'),('0069_Create_ServiceFrequency_Entity'),('0070_Create_Service_Named_Entity'),('0071_Add_Relationship_To_Service_Named'),('0072_Create_ContractorService_Entity'),('0073_Add_Relationship_To_ContractorService'),('0074_Create_Service_Entity'),('0075_Add_Relationship_To_Service'),('0076_Add_ContactPersonAdresses_Table_And_Relations_To_ContactPerson'),('0077_Drop_Relation_From_Adress_To_ContactPerson_Entities'),('0078_Refactor_Account_Entity_Add_Address_Relation'),('0079_Add_Field_Sort_For_ContractorType'),('0080_Refactor_Company_Entity_Add_Address_Entity_Relation'),('0081_Refactored_AccountHistory_Entity'),('0082_Refactored_CompanyHistory_Entity'),('0083_Update_Contractor_Entity_Coi_Expiration_Nullable_True'),('0084_Add_Field_Comment_For_Service_Entity'),('0085_Update_CompanyHistory_Change_Address_Relation'),('0086_Update_AccountHistory_Change_Address_Relation'),('0087_Change_Fixed_And_Hourly_Price_And_LastTest_For_Service'),('0088_Add_ServiceHistory_Entity'),('0089_Update_ContractorService_Entity_Change_Price_Fields_Nullable_True_Add_Field_State_Many_To_One_Relation'),('0090_Set_Logo_And_Website_Nullable_For_Contractor_Entity'),('0091_Create_ContractorUser_Entity'),('0092_Add_Contractor_Field_To_ContractorUser_Entity_With_Relation_Many_To_One_To_Contractor_Entity'),('0093_Create_Municipality_Entity'),('0094_Create_LicensesNamed_Entity'),('0095_Create_Licenses_Entity_And_Relate_To_LicensesNamed_And_State'),('0096_Relate_Licenses_And_ContractorUser_Entities'),('0097_Add_Deleted_To_Licenses_Entity'),('0098_Create_Media_Gallery_GalleryMedia_Entities'),('0099_Create_LicensesCategory_Entity'),('0100_Relate_LicensesNamed_To_LicensesCategory_Entity'),('0101_Create_LicensesDynamicField_Entity_And_Relate'),('0102_Create_LicensesDynamicFieldValue_Entity_And_Relate'),('0103_Create_LicensesDropboxChoices_Entity'),('0104_Relate_LicensesDynamicFieldValue_To_LicensesDropboxChoices'),('0105_Update_LicenseNamed_Entity_Realion_To_Category'),('0106_Add_Field_Phone_For_Contractor_Entity'),('0107_Create_ContractorUserHistory_Entity'),('0108_Create_MunicipalityHistory_Entity'),('0109_Remove_Icon_From_LicensesNamed_Entity'),('0110_Add_Label_To_LicensesDynamicField'),('0111_Create_Agent_Entity'),('0112_Create_Department_Entity'),('0113_Create_ChannelNamed_Entity'),('0114_Create_ChannelDynamicField_Entity_And_Relate'),('0115_Create_ChannelDropboxChoices_And_Relate_ChannelDynamicField'),('0116_Create_FeesBasis_Entity'),('0117_Create_DepartmentChannelDynamicFieldValue_Entity_And_Relate'),('0118_Create_DepartmentChannel_Entity'),('0119_Relate_DepartmentChannelDynamicFieldValue_DepartmentChanel'),('0120_Create_Contact_Entity'),('0121_Refactor_DepartmentChannel_Entity'),('0122_Create_AgentChannel_Entity'),('0123_Create_AgentChannelDynamicFieldValue_Entity'),('0124_Create_Relation_Between_Department_And_Agent_Entities'),('0125_Add_Relations_For_AgentChannel_Entity'),('0126_Add_Relations_For_AgentChannelDynamicFieldValue_Entity'),('0127_Create_DepartmentHistory_Entity'),('0128_Updated_DepartmentChannel_Entity'),('0129_Update_AgentChannel_Entity_Field_Default'),('0130_Create_Many_To_Many_Relation_Between_Department_And_Contact_Entities_With_Department_Master'),('0131_Create_Many_To_Many_Relation_Between_Municipality_And_Contact_Entities_With_Municipality_Master'),('0132_Create_ContactType_Entity'),('0133_Create_Many_To_One_Relation_Between_Contact_And_ContactType_Entities_With_Contact_Master'),('0134_Create_Many_To_Many_Relation_Between_Agent_And_Contact_Entities_With_Agent_Master'),('0135_Change_Relation_Between_Contact_And_DeviceCategory_Entities_From_Many_To_One_To_Many_To_Many'),('0136_Add_Owner_Field_To_DepartmentChannel_Entity_Related_To_AgentChannel_Entity_As_Many_To_One'),('0137_Add_Validation_To_ChannelDynamicField_Entity'),('0138_Add_ForiginKey_Cascade_DepartmentChannelDynamicFieldValue_Entity'),('0139_Add_entityValue_Field_To_AgentChannelDynamicFieldValue_Entity'),('0140_Set_Nullable_TRUE_For_Value_Field_For_AgentChannelDynamicFieldValue_Entity'),('0141_Set_NULLABLE_TRUE_For_isDefault_Field_AgentChannel_Entity'),('0142_Added_Municipality_Field_To_Account_Entity_And_Relationship'),('0143_Create_EquipmentType_Entity'),('0144_Create_Equipment_Entity_And_Relate_EquipmentType'),('0145_Relate_Equipment_And_ContractorUser_Entities'),('0146_Create_Opportunity_Entity'),('0147_Create_Opportunity_Type_And_Status_Entities'),('0148_Add_Relationship_Opportunity_Entity'),('0149_Add_Filds_And_Relationship_Service_And_Opportunity_Entities'),('0150_Add_Nullable_Price_Field_Opportunity_Entity'),('0151_Add_OldestOpportunityDate_Field_Account_Entity'),('0152_Create_ProposalType_Entity'),('0153_Create_Proposal_Entity'),('0154_Add_Proposal_Many_To_One_Relation_To_Service_Entity'),('0155_Create_ProposalStatus_Entity'),('0156_Add_ProposalStatus_Many_To_One_Relation_To_Proposal_Entity'),('0157_Add_ParentAccount_Field_Opportunity_Entity'),('0158_Add_Oldest_Opp_Divicion_Fields_Account_Entity'),('0159_Add_Some_Fields_Proposal_Entity'),('0160_Add_Fields_DateCreate_And_DateUpdate_For_Proposal_Entity'),('0161_Add_OldestProposalDate_Fields_Account_Entity'),('0162_Add_Recipient_Field_Proposal_Entity'),('0163_Add_DepartmentChannel_Field_Service_Entity'),('0164_Create_MessageType_Entity'),('0165_Create_Message_Entity'),('0166_Add_SendingAddress_To_Proposal_Entity'),('0167_Add_Messages_Many_To_Many_Relation_To_Account_Entity'),('0168_Add_SendingAddressDescription_To_Proposal_Entity'),('0169_Create_ProposalLogType_Entity'),('0170_Create_ProposalAction_Entity'),('0171_Create_ProposalLog_Entity'),('0172_Add_Actions_Field_To_ProposalLog_Entity_For_Many_To_Many_Relation_To_ProposalAction_Entity'),('0173_Change_Relations_To_History_In_Proposal_Entity'),('0174_Add_Fields_Authorizer_Access_AccessPrimary_Payment_PaymentPrimary'),('0175_Add_ManyToMany_Field_Responsibilities_To_DeviceCategory_For_AccountContactPerson_Entity'),('0176_Delete_Fields_Role_And_Responsibility_For_AccountContactPerson_Entity'),('0177_Add_Fields_Authorizer_Access_AccessPrimary_Payment_PaymentPrimary_For_History'),('0178_Delete_ContactPersonRole_Entity'),('0179_Delete_ContactPersonResponsibility_Entity'),('0180_Add_Field_Weight_For_DeviceCategory_Entity'),('0181_Add_Field_DateCreate_ServiceHistory_Entity'),('0182_Change_ContactPerson_On_AccountContactPerson_Proposal_Entity'),('0183_Remove_SendingAdress_And_SendingAdressDescription_From_Proposal_Entity'),('0184_Add_SendingAddress_And_SendingAddressDescription_To_AccountContactPerson_Entity'),('0185_Add_SendingAddress_And_SendingAddressDescription_To_AccountContactPersonHistory_Entity'),('0186_Add_Field_Active_For_DepartmentChanel_Entity'),('0187_Add_SendingAddress_And_SendingAddressDescription_To_Proposal_Entity'),('0188_Reemove_SendingAddressDescription_From_AccountContactPerson_Entity'),('0189_Add_Field_Report_For_Proposal_Entity'),('0190_Delete_Field_Addresses_For_ContactPerson_Entity'),('0191_Add_Sort_Field_To_AddressType_Entity'),('0192_Add_Field_PersonalAddress_To_ContactPerson_Entity'),('0193_Create_Letter_Entity'),('0194_Add_Website_Field_To_Account_Entity'),('0195_Add_Website_Field_To_AccountHistory_Entity'),('0196_Add_FirstSendingDate_Field_To_Proposal_Entity'),('0197_Change_From_To_Fields_Letter_Entity'),('0198_Create_ProposalHistory_Entity'),('0199_Create_ProcessingStats_Entity'),('0200_Create_ProcessingStatsType'),('0201_Relate_ProcessingStats_And_ProcessingStatsType_Entities'),('0202_Add_Priority_Integer_Field_To_ProposalStatus_Entity'),('0203_Add_Letter_Field_To_ProposalLog_Entity'),('0204_Add_Message_String_Field_To_ProposalLog_Entity'),('0205_Create_ServiceStatus_Entity'),('0206_Add_Status_Field_To_Service_Entity_For_ServiceStatus_Many_To_One_Relation'),('0207_Remove_Message_From_ProposalLogType_Entity'),('0208_Align_Service_And_ServiceHistory_Entities'),('0209_Delete_Relation_Opportunity_To_ServiceHistory_Entity'),('0210_Create_WorkorderStatus_Entity'),('0211_Create_WorkorderLog_Entity'),('0212_Create_Workorder_Entity'),('0213_Add_Workorder_Field_To_WorkorderLog_Entity_For_Workorder_Many_To_One_Relation'),('0214_Add_Workorder_Field_To_ServiceHistory_Entity_For_Workorder_Many_To_One_Relation'),('0217_Add_Opportunity_Field_To_ServiceHistory_Entity_For_Opportunity_Many_To_One_Relation'),('0218_Create_DepartmentChannelHistory_Entity'),('0219_Field_Attachment_Became_Nullable_Letter_Entity'),('0220_Create_PaymentMethod_Entity'),('0221_Changed_Field_Contact_Person_To_Contact_Person_History_AccountContactPersonHistory_Entity'),('0222_Update_Workorder_Service_And_ServiceHistory_Entity'),('0223_Add_InternalComment_Property_To_WorkOrder_Entity'),('0227_Add_Field_Billing_Address_To_Account_AccountHistory_Entities'),('0228_Add_Authorizer_Property_To_Workorder_Entity'),('0229_Add_PaymentMethod_Property_To_Workorder_Entity'),('0230_Create_FrozenServiceForProposal_Entity'),('0231_Delete_Service_Field_From_FrozenServiceForProposal_Entity'),('0232_Add_Cod_Boolean_Field_To_Account_Entity'),('0233_Add_Cod_Boolean_Field_To_AccountHistory_Entity'),('0234_Add_DateScheduled_Field_To_WorkOrder_Entity'),('0235_Create_Coordinate_Entity'),('0236_Create_WorkorderLogType_Entity'),('0237_Add_Status_Property_To_WorkorderLog_Entity'),('0238_Change_Account_Field_Type_In_Workorder_Entity_From_AccountHistory_To_Account'),('0239_Change_Workorder_Entity_Add_StartScheduledDate_And_FinishScheduledDate'),('0240_Refactor_Workorder_Entity_Add_ScheduledFrom_And_ScheduledTo_Properties'),('0241_Add_Field_Invoice_To_Workorder_Entity'),('0242_Add_FrozenServiceForWorkorder_Entity_And_Linked_To_Other_Entities'),('0243_Related_Workorder_And_FrozenServiceForWorkorder_Entities'),('0244_Refactor_Relate_Workorder_And_FrozenServiceForWorkorder_Entities'),('0245_Create_EventType_Entity'),('0246_Create_Event_Entity'),('0247_Create_WorkorderFrozenContent_Entity'),('0248_Create_Invoices_Entity'),('0249_ReRelate_WorkorderFrozenContent_Entity_To_DeviceHistory_Entity'),('0250_Added_IsFrozen_To_Workorder_Entity'),('0251_Add_Default_Zero_Value_To_Upload_And_Processing_Fees_Of_DepartmentChannel_Entity'),('0252_Add_Default_Zero_Value_To_Upload_And_Processing_Fees_Of_DepartmentChannelHistory_Entity'),('0253_Add_AccountAuthorizer_To_Workorder_Entity'),('0254_Add_OldCmpanyId_To_Company_Entity'),('0255_Add_OldAccountId_To_Account_Entity'),('0257_Create_ImportErrorMessages_Entity'),('0258_Create_AccountBuildingType_Entity'),('0259_Add_IsHistoryProcessed_To_Company_Entity'),('0260_Add_IsHistoryProcessed_And_IsMessagesProcessed_To_Account_Entity'),('0261_Add_IsHistoryProcessed_To_ContactPerson_Entity'),('0262_Add_IsHistoryProcessed_To_AccountContactPerson_Entity'),('0263_Add_OldLinkId_To_Account_Entity'),('0264_Add_SiteSendTo_To_Account_Entity'),('0265_Add_Address_MunicipalityPhone_MunicipalityFax_MunicipalityOldId_To_Municipality_Entity'),('0266_Add_OldSistemId_Comment_Fax_Email_To_Contractor_Entity'),('0267_Make_Website_And_Phone_For_Agent_Entity_Nullable_True'),('0268_Change_BillingAddress_To_OldSystemMasLevelAddress_Account_Entity'),('0269_Make_LastName_For_Contact_Entity_Nullable_True'),('0270_Add_Field_OldDeviceId_To_Device_Entity'),('0271_Update_MunicipalityHistory_Entity_To_Actual_State'),('0272_Add_IsHistoryProcessing_To_Device_Municipality_Contractor_ContractorUser_Entities'),('0273_Add_OldBackflowwDepartmentId_OldFireDepartmentId_IsMixedMunicipality_To_Municipality_Entity'),('0274_Add_Alias_To_DynamicFieldDropboxChoices_Entity'),('0279_Add_AlarmPanel_Field_To_Device_Entity'),('0280_Add_Coordinate_To_Account_Entity'),('0281_Add_SourceEntity_Parent_To_Company_Entity'),('0282_Add_ReportStatus_Entity'),('0283_Add_Report_Entity'),('0284_Add_IsNeedRepair_For_Report_Entity'),('0285_Add_LastReport_Relation_To_Service_And_ServiceHistory_Entity'),('0286_Add_QuestionType_Entity'),('0287_Add_Question_Entity'),('0288_Add_Relation_Question_To_QuestionType'),('0289_Add_Answer_Entity'),('0290_Add_Relation_Answer_To_Question'),('0291_Add_Relation_Answer_To_Report'),('0292_Add_RangeQuestionStatus_Entity'),('0293_Add_RangeQuestion_Entity'),('0294_Add_positiveStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0295_Add_lowerNegativeStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0296_Add_upperNegativeStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0297_Add_failAnywayStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0298_Add_ServicePatern_Entity'),('0299_Add_servicePatern_Field_To_RangeQuestion_Entity_To_Relate_It_To_ServicePatern_Entity_As_Many_To_One'),('0300_Add_RangeResult_Entity'),('0301_Add_rangeQuestion_Field_To_RangeResult_Entity_To_Relate_It_To_RangeQuestion_Entity_As_Many_To_One'),('0302_Add_rangeQuestionStatus_Field_To_RangeResult_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One'),('0303_Add_report_Field_To_RangeResult_Entity_To_Relate_It_To_Report_Entity_As_Many_To_One'),('0304_Add_deficiency_Field_To_Service_Entity'),('0305_Add_IsFrozen_Boolean_Field_To_Proposal_Entity'),('0306_Add_Nullable_Default_For_Fields_ServicesCount_DevicesCount_SitesCount_To_Proposal_Entity'),('0307_Add_Nullable_Default_For_Fields_ServicesCount_DevicesCount_SitesCount_To_Proposal_Entity'),('0310_Add_Clone_Field_To_Proposal_Entity_To_Relate_It_To_Itself_As_Many_To_One'),('0311_Add_Cloned_Field_To_Proposal_Entity_To_Relate_It_To_Itself_As_Many_To_One'),('0312_Create_NotIncluded_Entity'),('0313_Create_SpecialNotification_Entity'),('0314_Add_division_Field_To_Relate_It_To_DeviceCategory_Entity_As_Many_To_One_Add_proposals_Field_To_Relate_It_To_Proposal_Entity_As_Many_To_Many'),('0315_Add_specialNotification_Field_To_Relate_It_To_Proposal_Entity_And_SpecialNotification_Entity_As_Many_To_Many'),('0316_Add_Clone_Field_To_ProposalHistory_Entity_To_Relate_It_To_Proposal_Entity_As_Many_To_One'),('0317_Add_Cloned_Field_To_ProposalHistory_Entity_To_Relate_It_To_Proposal_Entity_As_Many_To_One'),('0318_Create_PaymentTerm_Entity'),('0319_Add_PaymentTerm_Field_To_Account_Entity_To_Relate_It_To_PaymentTerm_Entity_As_Many_To_One'),('0320_Add_PaymentTerm_Field_To_Workorder_Entity_To_Relate_It_To_PaymentTerm_Entity_As_Many_To_One'),('0321_Add_CssClass_Nullable_Field_To_ServiceStatus_Entity'),('0321_Delete_Deficiency_To_Service_Add_To_ServiceName_Entity'),('0322_Add_IsNeedSendMunicipalityReport_To_ServiceNamed_Entity'),('0323_Add_Some_Fields_To_Proposal_Entity'),('0324_Fix_ServiceHistory_Entity'),('0325_Change_Relationship_SpecialNotification_And_Proposal_Entities'),('0326_Create_RepairDeviceInfo_Entity'),('0327_Drop_Field_RepairDeviceInfo_Entity'),('0328_Add_Properties_Field_To_Proposal_Entity'),('0329_Add_CssClass_Field_To_WorkorderStatus_Entity'),('0330_Add_CssClass_Field_To_ProposalStatus_Entity'),('0331_Add_PaymentTerms_To_AccountHistory_Entity'),('0332_Change_Relations_In_Proposal_Entity_From_History_To_Actual'),('0334_Drop_TypeId_Field_From_ProposalLog_Entity'),('0335_Drop_ProposalAction_Entity_Drop_ProposalLogsActionTable'),('0336_Create_ProposalLogsActions_Table'),('0337_Add_Link_Field_Add_IsActive_Field_Drop_Route_Field_To_ProposalLoaAction_Entity'),('0338_Drop_ProposalLogType_Table'),('0339_Create_Proposal_Log_Action_Named_Table'),('0340_Add_Named_Field_To_Relate_It_To_ProposalLogAction_Entity_As_Many_To_One'),('0341_Add_Title_Field_To_Device_Entity'),('0342_Add_SubtotalMunicAndProcFee_DeviceTotal_RepairDeviceInfo_Entity'),('0343_Add_Sort_Field_to_DeviceNamed_Entity'),('0344_Create_FrozenProposal_Entity'),('0345_Replace_Proposal_Many_To_One_Relation_With_FrozenProposal_Many_To_One_Relation_In_FrozenServiceForProposal_Entity'),('0346_Add_Frozen_Field_To_Proposal_Entity_To_Relate_It_To_FrozenProposal_Entity_As_Many_To_One'),('0347_Change_History_Relations_To_Actual_Relations_In_ProposalHistory_Entity'),('0348_Add_EstimationTime_Field_To_ContactorService_Entity'),('0349_Add_IsNeedMunicipalityFee_Field_To_ServiceNamed_Entity'),('0350_Create_ProposalLifeCycle_Entity'),('0351_Add_lifeCycle_Field_To_Proposal_Entity_To_Relate_It_To_ProposalLifeCycle_Entity_As_Many_To_One'),('0352_Add_AllTotalMunicAndProcFee_AllTotalRepairsPrice_AllGrandTotalPrice_Fields_Proposal_Entity'),('0353_Add_Relationship_Between_Proposal_And_NotIncluded_Entities'),('0354_Add_CanFinishWO_To_Event_Entity'),('0355_Add_Field_ContractorService_To_Service_Entity'),('0356_Add_Field_ContractorService_To_ServiceHistory_Entity'),('0357_Changing_Relation_Between_Workorder_And_Proposal_Entities'),('0358_Add_LastEditor_Property_To_Device_Entity'),('0359_Add_Relationship_Proposal_WorkorderLog_Entities'),('0360_Create_WorkorderDeviceInfo_Entity'),('0361_Add_MounthFrequency_To_ServiceFriquency_Entity'),('0362_Remove_Cod_Filed_From_Account_Entity'),('0363_Add_SpecialDiscount_Field_To_Account_Entity'),('0364_Remove_Cod_Filed_From_AccountHistory_Entity'),('0365_Add_SpecialDiscount_Field_To_AccountHistory_Entity'),('0366_Add_LastTester_Field_To_Service_Entity'),('0367_Add_Field_Level_To_DeviceNamed_Entity'),('0368_Change_Link_Proposal_To_ProposalHistory_For_ServiceHistory_Entity'),('0369_Add_Field_TotalEstimationTime_To_Proposal_Entity'),('0370_Add_Field_TotalEstimationTime_To_ProposalHistory_Entity'),('0371_Add_Fields_TotalEstimationTimeInpections_And_Total_Estimation_Time_Repairs_To_WorkOrder_Entity'),('0372_Add_MSOutlookCalendarEventId_Field_To_Event_Entity'),('0373_Add_FinishTime_Field_To_Report_Entity'),('0374_Add_AdditionalComment_Field_To_Workorder_Entity'),('0375_Add_AfterHoursWorkCost_Field_To_Workorder_Entity'),('0376_Add_AdditionalServicesCost_Field_To_Workorder_Entity'),('0377_Add_Discount_Field_To_Workorder_Entity'),('0378_Add_Signature_Location_FinishTime_Fields_Workorder_Entity'),('0379_Add_ManyToMany_Field_notIncludedItems_To_WorkOrderEntity'),('0380_Add_TotalInspectionsFee_Field_To_Workorder_Entity'),('0381_Add_TotalRepairsPrice_Field_To_Workorder_Entity'),('0382_Add_TotalMunicAndProcFee_Field_To_Workorder_Entity'),('0383_Create_WorkorderType_Entity'),('0384_Remove_TotalInspectionsFee_Field_To_Workorder_Entity'),('0385_Add_TotalInspectionsCount_Field_To_Workorder_Entity'),('0386_Add_TotalRepairsCount_Field_To_Workorder_Entity'),('0387_Add_TotalPrice_Field_To_Workorder_Entity'),('0389_Add_DateCreate_And_DateUpdate_To_Service_Entity'),('0390_Add_DateUpdate_To_ServiceHistory_Entity'),('0391_Add_Notes_Field_To_AccountContactPerson_Entity'),('0392_Add_Notes_Field_To_AccountContactPersonHistory_Entity'),('0396_Add_CreatedBy_Field_To_Workorder_Entity'),('0397_Add_MsOutlookEmail_Field_To_ContractorUser_Entity'),('0398_Add_PrintTemplate_Property_To_Report_Entity'),('0399_Add_Finisher_Property_To_Workorder_Entity'),('0400_Change_AfterHoursWorkCost_Field_Proposal_Entity_From_String_To_Float'),('0401_Change_Discount_Field_Proposal_Entity_From_String_To_Float'),('0402_Change_AfterHoursWorkCost_Field_ProposalHistory_Entity_From_String_To_Float'),('0403_Change_Discount_Field_ProposalHistory_Entity_From_String_To_Float'),('0404_Add_AdditionalCost_Field_To_Proposal_Entity'),('0405_Add_AdditionalCost_Field_To_ProposalHistory_Entity'),('0406_Add_Emails_Field_To_Letter_Entity'),('0407_Add_MsRefreshToken_Field_To_ContractorUser_Entity'),('0408_Add_SignatureOwner_To_Workorder_Entity'),('0409_Change_msOutlookEmail_Field_To_msCalendarId_Field_In_ContractorUser_Entity'),('0410_Change_Size_Field_msOutlookCalendarEventId'),('0411_Add_Field_TextReport_To_Report_Entity'),('0412_Add_Field_HtmlReport_To_Report_Entity'),('0413_Create_ContractorCalendar_Entity');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipalities_contacts`
--

DROP TABLE IF EXISTS `municipalities_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `municipalities_contacts` (
  `municipality_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`municipality_id`,`contact_id`),
  KEY `IDX_20E5CE8CAE6F181C` (`municipality_id`),
  KEY `IDX_20E5CE8CE7A1254A` (`contact_id`),
  CONSTRAINT `FK_20E5CE8CAE6F181C` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`id`),
  CONSTRAINT `FK_20E5CE8CE7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipalities_contacts`
--

LOCK TABLES `municipalities_contacts` WRITE;
/*!40000 ALTER TABLE `municipalities_contacts` DISABLE KEYS */;
INSERT INTO `municipalities_contacts` VALUES (1,6),(1,7),(2,5);
/*!40000 ALTER TABLE `municipalities_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipality`
--

DROP TABLE IF EXISTS `municipality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `municipality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `municipality_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `municipality_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_municipality_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_history_processed` tinyint(1) DEFAULT '0',
  `old_backflow_department_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_fire_department_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_mixed_municipality` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_C6F56628F5B7AF75` (`address_id`),
  CONSTRAINT `FK_C6F56628F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipality`
--

LOCK TABLES `municipality` WRITE;
/*!40000 ALTER TABLE `municipality` DISABLE KEYS */;
INSERT INTO `municipality` VALUES (1,'City of New York','http://www1.nyc.gov/',NULL,NULL,NULL,NULL,0,NULL,NULL,0),(2,'City of Kyiv','https://kyivcity.gov.ua/',NULL,NULL,NULL,NULL,0,NULL,NULL,0),(3,'City of Portland','http://www.cityofportland.org',NULL,NULL,NULL,NULL,0,NULL,NULL,0);
/*!40000 ALTER TABLE `municipality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipality_history`
--

DROP TABLE IF EXISTS `municipality_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `municipality_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_save` datetime NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `municipality_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `municipality_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B20D04DBBDAFD8C8` (`author`),
  KEY `IDX_B20D04DB835E0EEE` (`owner_entity_id`),
  KEY `IDX_B20D04DBF5B7AF75` (`address_id`),
  CONSTRAINT `FK_B20D04DB835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `municipality` (`id`),
  CONSTRAINT `FK_B20D04DBBDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_B20D04DBF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipality_history`
--

LOCK TABLES `municipality_history` WRITE;
/*!40000 ALTER TABLE `municipality_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `municipality_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notIncludedItems_proposals`
--

DROP TABLE IF EXISTS `notIncludedItems_proposals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notIncludedItems_proposals` (
  `proposal_id` int(11) NOT NULL,
  `not_included_id` int(11) NOT NULL,
  PRIMARY KEY (`proposal_id`,`not_included_id`),
  KEY `IDX_1C5646E9F4792058` (`proposal_id`),
  KEY `IDX_1C5646E9C1CB14A1` (`not_included_id`),
  CONSTRAINT `FK_1C5646E9C1CB14A1` FOREIGN KEY (`not_included_id`) REFERENCES `not_included` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1C5646E9F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notIncludedItems_proposals`
--

LOCK TABLES `notIncludedItems_proposals` WRITE;
/*!40000 ALTER TABLE `notIncludedItems_proposals` DISABLE KEYS */;
/*!40000 ALTER TABLE `notIncludedItems_proposals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notIncludedItems_workorders`
--

DROP TABLE IF EXISTS `notIncludedItems_workorders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notIncludedItems_workorders` (
  `workorder_id` int(11) NOT NULL,
  `not_included_id` int(11) NOT NULL,
  PRIMARY KEY (`workorder_id`,`not_included_id`),
  KEY `IDX_E384424C2C1C3467` (`workorder_id`),
  KEY `IDX_E384424CC1CB14A1` (`not_included_id`),
  CONSTRAINT `FK_E384424C2C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E384424CC1CB14A1` FOREIGN KEY (`not_included_id`) REFERENCES `not_included` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notIncludedItems_workorders`
--

LOCK TABLES `notIncludedItems_workorders` WRITE;
/*!40000 ALTER TABLE `notIncludedItems_workorders` DISABLE KEYS */;
/*!40000 ALTER TABLE `notIncludedItems_workorders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `not_included`
--

DROP TABLE IF EXISTS `not_included`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `not_included` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AC36D2A341859289` (`division_id`),
  CONSTRAINT `FK_AC36D2A341859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `not_included`
--

LOCK TABLES `not_included` WRITE;
/*!40000 ALTER TABLE `not_included` DISABLE KEYS */;
INSERT INTO `not_included` VALUES (1,1,'Correcting service valves issues if any','correcting_service_valves_issues_if_any','2018-06-19 10:59:58','2018-06-19 10:59:58'),(2,1,'Permits, Permit Fees or Drawings','permits_permit_fees_or_drawings','2018-06-19 10:59:58','2018-06-19 10:59:58'),(3,1,'Hards Parts (if needed)','hards_parts_if_needed','2018-06-19 10:59:58','2018-06-19 10:59:58'),(4,1,'Additional work not mentioned in this proposal','additional_work_not_mentioned_in_this_proposal','2018-06-19 10:59:58','2018-06-19 10:59:58'),(5,1,'Hydraulic Calulations','hydraulic_calulations','2018-06-19 10:59:58','2018-06-19 10:59:58'),(6,1,'Any Patching or Painting services','any_patching_or_painting_services','2018-06-19 10:59:58','2018-06-19 10:59:58'),(7,1,'Electrical or alarm work if needed is extra','electrical_or_alarm_work_if_needed_is_extra','2018-06-19 10:59:58','2018-06-19 10:59:58'),(8,2,'Any Notifications to Building Occupants','any_notifications_to_building_occupants','2018-06-19 10:59:58','2018-06-19 10:59:58'),(9,2,'Permits, Permit Fees or Drawings','permits_permit_fees_or_drawings_fire','2018-06-19 10:59:58','2018-06-19 10:59:58'),(10,2,'Additional work not mentioned in this proposal','additional_work_not_mentioned_in_this_proposal_fire','2018-06-19 10:59:58','2018-06-19 10:59:58'),(11,2,'Hydraulic Calculations','hydraulic_calculations','2018-06-19 10:59:58','2018-06-19 10:59:58'),(12,2,'Any Patching or Painting services','any_patching_or_painting_services_fire','2018-06-19 10:59:58','2018-06-19 10:59:58'),(13,2,'Any tamper switches','any_tamper_switches','2018-06-19 10:59:58','2018-06-19 10:59:58'),(14,2,'Insulation','insulation','2018-06-19 10:59:58','2018-06-19 10:59:58'),(15,2,'Lift equipment','lift_equipment_fire','2018-06-19 10:59:58','2018-06-19 10:59:58'),(16,2,'Any additional programming to Alarm Panel','any_additional_programming_to_alarm_panel_fire','2018-06-19 10:59:58','2018-06-19 10:59:58'),(17,3,'Any Patching, Painting or Tile services','any_patching_painting_or_tile_services_plumbing','2018-06-19 10:59:58','2018-06-19 10:59:58'),(18,3,'Permits. Permit Fees, or Drawings','permits_permit_fees_or_drawings_plumbing','2018-06-19 10:59:58','2018-06-19 10:59:58'),(19,3,'Not responsible For Cutting Utilities in the Floor','not_responsible_for_cutting_utilities_in_the_floor_plumbing','2018-06-19 10:59:58','2018-06-19 10:59:58'),(20,3,'Correcting service valves issues if any','correcting_service_valves_issues_if_any_plumbing','2018-06-19 10:59:58','2018-06-19 10:59:58'),(21,3,'Insulation','insulation_plumbing','2018-06-19 10:59:58','2018-06-19 10:59:58'),(22,3,'Lift equipment','lift_equipment_plumbing','2018-06-19 10:59:58','2018-06-19 10:59:58');
/*!40000 ALTER TABLE `not_included` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunity`
--

DROP TABLE IF EXISTS `opportunity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `services_count` int(11) NOT NULL,
  `devices_count` int(11) NOT NULL,
  `sites_count` int(11) NOT NULL,
  `price` double DEFAULT NULL,
  `execution_services_date` date NOT NULL,
  `division` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `parent_account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8389C3D710174714` (`division`),
  KEY `IDX_8389C3D78CDE5729` (`type`),
  KEY `IDX_8389C3D77B00651C` (`status`),
  KEY `IDX_8389C3D77D3656A4` (`account`),
  KEY `IDX_8389C3D7F7E22E2` (`parent_account`),
  CONSTRAINT `FK_8389C3D710174714` FOREIGN KEY (`division`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_8389C3D77B00651C` FOREIGN KEY (`status`) REFERENCES `opportunity_status` (`id`),
  CONSTRAINT `FK_8389C3D77D3656A4` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_8389C3D78CDE5729` FOREIGN KEY (`type`) REFERENCES `opportunity_type` (`id`),
  CONSTRAINT `FK_8389C3D7F7E22E2` FOREIGN KEY (`parent_account`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunity`
--

LOCK TABLES `opportunity` WRITE;
/*!40000 ALTER TABLE `opportunity` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunity_status`
--

DROP TABLE IF EXISTS `opportunity_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunity_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alisa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunity_status`
--

LOCK TABLES `opportunity_status` WRITE;
/*!40000 ALTER TABLE `opportunity_status` DISABLE KEYS */;
INSERT INTO `opportunity_status` VALUES (1,'Past Due','past_due',1),(2,'RN To Be Created','rn_to_be_created',2);
/*!40000 ALTER TABLE `opportunity_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunity_type`
--

DROP TABLE IF EXISTS `opportunity_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunity_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alisa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunity_type`
--

LOCK TABLES `opportunity_type` WRITE;
/*!40000 ALTER TABLE `opportunity_type` DISABLE KEYS */;
INSERT INTO `opportunity_type` VALUES (1,'Retest','retest'),(2,'Repair','repair');
/*!40000 ALTER TABLE `opportunity_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_method`
--

DROP TABLE IF EXISTS `payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_method`
--

LOCK TABLES `payment_method` WRITE;
/*!40000 ALTER TABLE `payment_method` DISABLE KEYS */;
INSERT INTO `payment_method` VALUES (1,'Bill','bill',1),(2,'Cash','cash',0),(3,'Check','check',0),(4,'Credit Card','credit_card',0);
/*!40000 ALTER TABLE `payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_term`
--

DROP TABLE IF EXISTS `payment_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_term`
--

LOCK TABLES `payment_term` WRITE;
/*!40000 ALTER TABLE `payment_term` DISABLE KEYS */;
INSERT INTO `payment_term` VALUES (1,'Net 15 days. Our price is firm for 30 days.','net_fifteen_days'),(2,'Net 30 days. Our price is firm for 30 days.','net_thirty_days'),(3,'COD','cod'),(4,'Paid in full before work can be started.','full_paid_before_started'),(5,'50% Deposit Die Before Work Can Start.','fifty_percent_deposit');
/*!40000 ALTER TABLE `payment_term` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processing_stats`
--

DROP TABLE IF EXISTS `processing_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processing_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2935D2C7C54C8C93` (`type_id`),
  CONSTRAINT `FK_2935D2C7C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `processing_stats_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processing_stats`
--

LOCK TABLES `processing_stats` WRITE;
/*!40000 ALTER TABLE `processing_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `processing_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processing_stats_type`
--

DROP TABLE IF EXISTS `processing_stats_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processing_stats_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processing_stats_type`
--

LOCK TABLES `processing_stats_type` WRITE;
/*!40000 ALTER TABLE `processing_stats_type` DISABLE KEYS */;
INSERT INTO `processing_stats_type` VALUES (1,'Info','info'),(2,'Warning','warning'),(3,'Error','error');
/*!40000 ALTER TABLE `processing_stats_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal`
--

DROP TABLE IF EXISTS `proposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `date_of_proposal` date DEFAULT NULL,
  `grand_total` double DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `reference_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_count` int(11) DEFAULT NULL,
  `devices_count` int(11) DEFAULT NULL,
  `sites_count` int(11) DEFAULT NULL,
  `earliest_due_date` date DEFAULT NULL,
  `service_total_fee` double DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `sending_address_id` int(11) DEFAULT NULL,
  `sending_address_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_sending_date` datetime DEFAULT NULL,
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0',
  `clone_id` int(11) DEFAULT NULL,
  `cloned_id` int(11) DEFAULT NULL,
  `after_hours_work_cost` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `additional_comment` longtext COLLATE utf8_unicode_ci,
  `frozen_id` int(11) DEFAULT NULL,
  `life_cycle_id` int(11) DEFAULT NULL,
  `all_total_munic_and_proc_fee` double DEFAULT NULL,
  `all_total_repairs_price` double DEFAULT NULL,
  `all_grand_total_price` double DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `total_estimation_time` double DEFAULT NULL,
  `additional_cost` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BFE5947241859289` (`division_id`),
  KEY `IDX_BFE594729B6B5FBA` (`account_id`),
  KEY `IDX_BFE59472C54C8C93` (`type_id`),
  KEY `IDX_BFE5947261220EA6` (`creator_id`),
  KEY `IDX_BFE594726BF700BD` (`status_id`),
  KEY `IDX_BFE594724F8A983C` (`contact_person_id`),
  KEY `IDX_BFE594726510ABA8` (`sending_address_id`),
  KEY `IDX_BFE594723D083110` (`clone_id`),
  KEY `IDX_BFE594723735CA56` (`cloned_id`),
  KEY `IDX_BFE59472E3690CE9` (`frozen_id`),
  KEY `IDX_BFE594726C4CA05D` (`life_cycle_id`),
  KEY `IDX_BFE594722C1C3467` (`workorder_id`),
  CONSTRAINT `FK_BFE594722C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_BFE594723735CA56` FOREIGN KEY (`cloned_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_BFE594723D083110` FOREIGN KEY (`clone_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_BFE5947241859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_BFE594724F8A983C` FOREIGN KEY (`contact_person_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_BFE5947261220EA6` FOREIGN KEY (`creator_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_BFE594726510ABA8` FOREIGN KEY (`sending_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_BFE594726BF700BD` FOREIGN KEY (`status_id`) REFERENCES `proposal_status` (`id`),
  CONSTRAINT `FK_BFE594726C4CA05D` FOREIGN KEY (`life_cycle_id`) REFERENCES `proposal_life_cycle` (`id`),
  CONSTRAINT `FK_BFE594729B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_BFE59472C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `proposal_type` (`id`),
  CONSTRAINT `FK_BFE59472E3690CE9` FOREIGN KEY (`frozen_id`) REFERENCES `frozen_proposal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal`
--

LOCK TABLES `proposal` WRITE;
/*!40000 ALTER TABLE `proposal` DISABLE KEYS */;
/*!40000 ALTER TABLE `proposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_history`
--

DROP TABLE IF EXISTS `proposal_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `sending_address_id` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `reference_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_count` int(11) DEFAULT NULL,
  `devices_count` int(11) DEFAULT NULL,
  `sites_count` int(11) DEFAULT NULL,
  `date_of_proposal` date DEFAULT NULL,
  `earliest_due_date` date DEFAULT NULL,
  `grand_total` double DEFAULT NULL,
  `service_total_fee` double DEFAULT NULL,
  `sending_address_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `clone_id` int(11) DEFAULT NULL,
  `cloned_id` int(11) DEFAULT NULL,
  `after_hours_work_cost` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `additional_comment` longtext COLLATE utf8_unicode_ci,
  `total_estimation_time` double DEFAULT NULL,
  `additional_cost` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_721E400041859289` (`division_id`),
  KEY `IDX_721E40009B6B5FBA` (`account_id`),
  KEY `IDX_721E4000C54C8C93` (`type_id`),
  KEY `IDX_721E400061220EA6` (`creator_id`),
  KEY `IDX_721E40006BF700BD` (`status_id`),
  KEY `IDX_721E40004F8A983C` (`contact_person_id`),
  KEY `IDX_721E40006510ABA8` (`sending_address_id`),
  KEY `IDX_721E4000835E0EEE` (`owner_entity_id`),
  KEY `IDX_721E4000BDAFD8C8` (`author`),
  KEY `IDX_721E40003D083110` (`clone_id`),
  KEY `IDX_721E40003735CA56` (`cloned_id`),
  CONSTRAINT `FK_721E40003735CA56` FOREIGN KEY (`cloned_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_721E40003D083110` FOREIGN KEY (`clone_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_721E400041859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_721E40004F8A983C` FOREIGN KEY (`contact_person_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_721E400061220EA6` FOREIGN KEY (`creator_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_721E40006510ABA8` FOREIGN KEY (`sending_address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_721E40006BF700BD` FOREIGN KEY (`status_id`) REFERENCES `proposal_status` (`id`),
  CONSTRAINT `FK_721E4000835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_721E40009B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_721E4000BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_721E4000C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `proposal_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_history`
--

LOCK TABLES `proposal_history` WRITE;
/*!40000 ALTER TABLE `proposal_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `proposal_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_life_cycle`
--

DROP TABLE IF EXISTS `proposal_life_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_life_cycle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_life_cycle`
--

LOCK TABLES `proposal_life_cycle` WRITE;
/*!40000 ALTER TABLE `proposal_life_cycle` DISABLE KEYS */;
INSERT INTO `proposal_life_cycle` VALUES (1,'Proposal Life Cycle','proposal_life_cycle'),(2,'Retest Notice Life Cycle','retest_notice_life_cycle');
/*!40000 ALTER TABLE `proposal_life_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_log`
--

DROP TABLE IF EXISTS `proposal_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `letter_id` int(11) DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_81AFA0B0F4792058` (`proposal_id`),
  KEY `IDX_81AFA0B0F675F31B` (`author_id`),
  KEY `IDX_81AFA0B04525FF26` (`letter_id`),
  CONSTRAINT `FK_81AFA0B04525FF26` FOREIGN KEY (`letter_id`) REFERENCES `letter` (`id`),
  CONSTRAINT `FK_81AFA0B0F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_81AFA0B0F675F31B` FOREIGN KEY (`author_id`) REFERENCES `contractor_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_log`
--

LOCK TABLES `proposal_log` WRITE;
/*!40000 ALTER TABLE `proposal_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `proposal_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_log_action`
--

DROP TABLE IF EXISTS `proposal_log_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_log_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `named_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2004EC46457EB27E` (`named_id`),
  CONSTRAINT `FK_2004EC46457EB27E` FOREIGN KEY (`named_id`) REFERENCES `proposal_log_action_named` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_log_action`
--

LOCK TABLES `proposal_log_action` WRITE;
/*!40000 ALTER TABLE `proposal_log_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `proposal_log_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_log_action_named`
--

DROP TABLE IF EXISTS `proposal_log_action_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_log_action_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_for_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_log_action_named`
--

LOCK TABLES `proposal_log_action_named` WRITE;
/*!40000 ALTER TABLE `proposal_log_action_named` DISABLE KEYS */;
INSERT INTO `proposal_log_action_named` VALUES (1,'Send via email','send_via_email','admin_letter_proposal',1),(2,'Notice was sent via Mail','notice_was_sent_via_mail','admin_letter_mail_proposal',1),(3,'Client was called','client_was_called','admin_proposal_client_was_called',1),(4,'Reset Services to Next Time','reset_services_to_next_time','admin_proposal_reset_services_next_time',1),(5,'Send Email Reminder','send_email_reminder','admin_letter_email_reminder',1),(6,'Link','link',NULL,1);
/*!40000 ALTER TABLE `proposal_log_action_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_logs_actions`
--

DROP TABLE IF EXISTS `proposal_logs_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_logs_actions` (
  `proposal_log_id` int(11) NOT NULL,
  `proposal_action_id` int(11) NOT NULL,
  PRIMARY KEY (`proposal_log_id`,`proposal_action_id`),
  KEY `IDX_BD2367AE4B713931` (`proposal_log_id`),
  KEY `IDX_BD2367AEA86603F3` (`proposal_action_id`),
  CONSTRAINT `FK_BD2367AE4B713931` FOREIGN KEY (`proposal_log_id`) REFERENCES `proposal_log` (`id`),
  CONSTRAINT `FK_BD2367AEA86603F3` FOREIGN KEY (`proposal_action_id`) REFERENCES `proposal_log_action` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_logs_actions`
--

LOCK TABLES `proposal_logs_actions` WRITE;
/*!40000 ALTER TABLE `proposal_logs_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `proposal_logs_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_status`
--

DROP TABLE IF EXISTS `proposal_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_status`
--

LOCK TABLES `proposal_status` WRITE;
/*!40000 ALTER TABLE `proposal_status` DISABLE KEYS */;
INSERT INTO `proposal_status` VALUES (1,'Draft','draft',0,'app-status-label--yellow'),(2,'To Be Sent','to_be_sent',1,'app-status-label--yellow'),(3,'Replacing','replacing',1,'app-status-label--yellow'),(4,'Sent via Mail','sent_via_mail',2,'app-status-label--green'),(5,'Sent via Email','sent_via_email',2,'app-status-label--green'),(6,'Sent via Mail and Email','sent_via_mail_and_email',3,'app-status-label--green'),(7,'Call Needed','call_needed',4,'app-status-label--red'),(8,'Client Was Called','client_was_called',5,'app-status-label--green'),(9,'Send Email Reminder','send_email_reminder',6,'app-status-label--red'),(10,'Email Reminder Sent','email_reminder_sent',7,'app-status-label--green'),(11,'Past Due','past_due',8,'app-status-label--red'),(12,'Sent Past Due Email','send_past_due_email',9,'app-status-label--red'),(13,'Rejected By Client','rejected_by_client',11,'app-status-label--red'),(14,'No Response','no_response',10,'app-status-label--red'),(15,'Deleted','deleted',12,'app-status-label--gray'),(16,'Services Were Reset','reset_services_to_next_time',12,'app-status-label--gray'),(17,'Workorder created','workorder_created',12,'app-status-label--gray'),(18,'Services Discarded','services_discarded',12,'app-status-label--gray'),(19,'Replaced','replaced',12,'app-status-label--gray');
/*!40000 ALTER TABLE `proposal_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposal_type`
--

DROP TABLE IF EXISTS `proposal_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proposal_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposal_type`
--

LOCK TABLES `proposal_type` WRITE;
/*!40000 ALTER TABLE `proposal_type` DISABLE KEYS */;
INSERT INTO `proposal_type` VALUES (1,'Retest','retest'),(2,'Repair','repair'),(3,'Custom','custom');
/*!40000 ALTER TABLE `proposal_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isSelectByDefault` tinyint(1) NOT NULL DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `question_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6F7494ECB90598E` (`question_type_id`),
  CONSTRAINT `FK_B6F7494ECB90598E` FOREIGN KEY (`question_type_id`) REFERENCES `question_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_type`
--

DROP TABLE IF EXISTS `question_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_type`
--

LOCK TABLES `question_type` WRITE;
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
INSERT INTO `question_type` VALUES (1,'Green','green','2018-06-19 10:59:38','2018-06-19 10:59:38'),(2,'Blue','blue','2018-06-19 10:59:38','2018-06-19 10:59:38'),(3,'Not Testable','not_testable','2018-06-19 10:59:38','2018-06-19 10:59:38');
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `range_question`
--

DROP TABLE IF EXISTS `range_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `range_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_value` int(11) DEFAULT '0',
  `max_value` int(11) DEFAULT '0',
  `lower_bound` int(11) DEFAULT '0',
  `upper_bound` int(11) DEFAULT '0',
  `sort` int(11) DEFAULT '0',
  `date_cretea` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `positive_status_id` int(11) DEFAULT NULL,
  `lower_negative_status_id` int(11) DEFAULT NULL,
  `upper_negative_status_id` int(11) DEFAULT NULL,
  `fail_anyway_status_id` int(11) DEFAULT NULL,
  `service_patern_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F8A3E6ED8C4651E9` (`positive_status_id`),
  KEY `IDX_F8A3E6EDDAA3558D` (`lower_negative_status_id`),
  KEY `IDX_F8A3E6EDE44B10AD` (`upper_negative_status_id`),
  KEY `IDX_F8A3E6ED14B8E846` (`fail_anyway_status_id`),
  KEY `IDX_F8A3E6EDD38F0878` (`service_patern_id`),
  CONSTRAINT `FK_F8A3E6ED14B8E846` FOREIGN KEY (`fail_anyway_status_id`) REFERENCES `range_question_status` (`id`),
  CONSTRAINT `FK_F8A3E6ED8C4651E9` FOREIGN KEY (`positive_status_id`) REFERENCES `range_question_status` (`id`),
  CONSTRAINT `FK_F8A3E6EDD38F0878` FOREIGN KEY (`service_patern_id`) REFERENCES `service_patern` (`id`),
  CONSTRAINT `FK_F8A3E6EDDAA3558D` FOREIGN KEY (`lower_negative_status_id`) REFERENCES `range_question_status` (`id`),
  CONSTRAINT `FK_F8A3E6EDE44B10AD` FOREIGN KEY (`upper_negative_status_id`) REFERENCES `range_question_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `range_question`
--

LOCK TABLES `range_question` WRITE;
/*!40000 ALTER TABLE `range_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `range_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `range_question_status`
--

DROP TABLE IF EXISTS `range_question_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `range_question_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_fail` tinyint(1) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `range_question_status`
--

LOCK TABLES `range_question_status` WRITE;
/*!40000 ALTER TABLE `range_question_status` DISABLE KEYS */;
INSERT INTO `range_question_status` VALUES (1,'Closed Tight/Held','closed_tight_held',0,'2018-06-19 10:59:38','2018-06-19 10:59:38'),(2,'Leaked','leaked',1,'2018-06-19 10:59:38','2018-06-19 10:59:38'),(3,'Open','open',0,'2018-06-19 10:59:38','2018-06-19 10:59:38'),(4,'Did Not Open','did_not_open',1,'2018-06-19 10:59:38','2018-06-19 10:59:38');
/*!40000 ALTER TABLE `range_question_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `range_result`
--

DROP TABLE IF EXISTS `range_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `range_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `result_value` int(11) DEFAULT '0',
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `range_question_id` int(11) DEFAULT NULL,
  `range_question_status_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3AEC1F6380802003` (`range_question_id`),
  KEY `IDX_3AEC1F63DE084BD1` (`range_question_status_id`),
  KEY `IDX_3AEC1F634BD2A4C0` (`report_id`),
  CONSTRAINT `FK_3AEC1F634BD2A4C0` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FK_3AEC1F6380802003` FOREIGN KEY (`range_question_id`) REFERENCES `range_question` (`id`),
  CONSTRAINT `FK_3AEC1F63DE084BD1` FOREIGN KEY (`range_question_status_id`) REFERENCES `range_question_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `range_result`
--

LOCK TABLES `range_result` WRITE;
/*!40000 ALTER TABLE `range_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `range_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repair_device_info`
--

DROP TABLE IF EXISTS `repair_device_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repair_device_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `special_notification_id` int(11) DEFAULT NULL,
  `proposal_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estimation_time` double NOT NULL,
  `price` double NOT NULL,
  `subtotal_munic_and_proc_fee` double NOT NULL,
  `device_total` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_97B48F4CA2485031` (`special_notification_id`),
  KEY `IDX_97B48F4CF4792058` (`proposal_id`),
  KEY `IDX_97B48F4C94A4C7D4` (`device_id`),
  CONSTRAINT `FK_97B48F4C94A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_97B48F4CA2485031` FOREIGN KEY (`special_notification_id`) REFERENCES `special_notification` (`id`),
  CONSTRAINT `FK_97B48F4CF4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repair_device_info`
--

LOCK TABLES `repair_device_info` WRITE;
/*!40000 ALTER TABLE `repair_device_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `repair_device_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `tester_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_report` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `is_need_repair` tinyint(1) DEFAULT '0',
  `finish_time` datetime DEFAULT NULL,
  `print_template` longtext COLLATE utf8_unicode_ci,
  `text_report` longtext COLLATE utf8_unicode_ci,
  `html_report` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_C42F77846BF700BD` (`status_id`),
  KEY `IDX_C42F7784ED5CA9E6` (`service_id`),
  KEY `IDX_C42F7784B0265DC7` (`contractor_id`),
  KEY `IDX_C42F7784979A21C1` (`tester_id`),
  KEY `IDX_C42F77842C1C3467` (`workorder_id`),
  CONSTRAINT `FK_C42F77842C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_C42F77846BF700BD` FOREIGN KEY (`status_id`) REFERENCES `report_status` (`id`),
  CONSTRAINT `FK_C42F7784979A21C1` FOREIGN KEY (`tester_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_C42F7784B0265DC7` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_C42F7784ED5CA9E6` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_status`
--

DROP TABLE IF EXISTS `report_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_status`
--

LOCK TABLES `report_status` WRITE;
/*!40000 ALTER TABLE `report_status` DISABLE KEYS */;
INSERT INTO `report_status` VALUES (1,'Pass','pass'),(2,'Fail','fail'),(3,'Not Testable','not_testable'),(4,'Completed','completed');
/*!40000 ALTER TABLE `report_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fixed_price` double DEFAULT NULL,
  `hourly_price` double DEFAULT NULL,
  `last_tested` datetime DEFAULT NULL,
  `inspection_due` datetime NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `named` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `company_last_tested` int(11) DEFAULT NULL,
  `comment` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opportunity` int(11) DEFAULT NULL,
  `test_date` date DEFAULT NULL,
  `department_channel` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `proposal_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `last_report_id` int(11) DEFAULT NULL,
  `contractor_service_id` int(11) DEFAULT NULL,
  `last_tester_id` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E19D9AD271E0CC87` (`named`),
  KEY `IDX_E19D9AD294A4C7D4` (`device_id`),
  KEY `IDX_E19D9AD29B6B5FBA` (`account_id`),
  KEY `IDX_E19D9AD29B52D956` (`company_last_tested`),
  KEY `IDX_E19D9AD28389C3D7` (`opportunity`),
  KEY `IDX_E19D9AD21F5066A4` (`department_channel`),
  KEY `IDX_E19D9AD26BF700BD` (`status_id`),
  KEY `IDX_E19D9AD2F4792058` (`proposal_id`),
  KEY `IDX_E19D9AD22C1C3467` (`workorder_id`),
  KEY `IDX_E19D9AD25C1D7BC6` (`last_report_id`),
  KEY `IDX_E19D9AD2B4DAC98D` (`contractor_service_id`),
  KEY `IDX_E19D9AD28055FEC7` (`last_tester_id`),
  CONSTRAINT `FK_E19D9AD21F5066A4` FOREIGN KEY (`department_channel`) REFERENCES `department_channel` (`id`),
  CONSTRAINT `FK_E19D9AD22C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_E19D9AD25C1D7BC6` FOREIGN KEY (`last_report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FK_E19D9AD26BF700BD` FOREIGN KEY (`status_id`) REFERENCES `service_status` (`id`),
  CONSTRAINT `FK_E19D9AD271E0CC87` FOREIGN KEY (`named`) REFERENCES `service_named` (`id`),
  CONSTRAINT `FK_E19D9AD28055FEC7` FOREIGN KEY (`last_tester_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_E19D9AD28389C3D7` FOREIGN KEY (`opportunity`) REFERENCES `opportunity` (`id`),
  CONSTRAINT `FK_E19D9AD294A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_E19D9AD29B52D956` FOREIGN KEY (`company_last_tested`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_E19D9AD29B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_E19D9AD2B4DAC98D` FOREIGN KEY (`contractor_service_id`) REFERENCES `contractor_service` (`id`),
  CONSTRAINT `FK_E19D9AD2F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_frequency`
--

DROP TABLE IF EXISTS `service_frequency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_frequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mounth_frequency` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_frequency`
--

LOCK TABLES `service_frequency` WRITE;
/*!40000 ALTER TABLE `service_frequency` DISABLE KEYS */;
INSERT INTO `service_frequency` VALUES (1,'1 month','1_month','1'),(2,'3 months','3_months','3'),(3,'6 months','6_months','6'),(4,'1 year','1_year','12'),(5,'2 years','2_years','24'),(6,'5 years','5_years','60'),(7,'6 years','6_years','72'),(8,'10 years','10_years','120'),(9,'non-repeatable','non_repeatable','0'),(10,'12 years','12_years','144'),(11,'3 years','3_years','36');
/*!40000 ALTER TABLE `service_frequency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_history`
--

DROP TABLE IF EXISTS `service_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `named` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `company_last_tested` int(11) DEFAULT NULL,
  `owner_entity_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `fixed_price` double DEFAULT NULL,
  `hourly_price` double DEFAULT NULL,
  `last_tested` datetime DEFAULT NULL,
  `inspection_due` datetime NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `comment` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `proposal_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `test_date` date DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `last_report_id` int(11) DEFAULT NULL,
  `opportunity_id` int(11) DEFAULT NULL,
  `department_channel_id` int(11) DEFAULT NULL,
  `contractor_service_id` int(11) DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E83E22D771E0CC87` (`named`),
  KEY `IDX_E83E22D794A4C7D4` (`device_id`),
  KEY `IDX_E83E22D79B6B5FBA` (`account_id`),
  KEY `IDX_E83E22D79B52D956` (`company_last_tested`),
  KEY `IDX_E83E22D7835E0EEE` (`owner_entity_id`),
  KEY `IDX_E83E22D7BDAFD8C8` (`author`),
  KEY `IDX_E83E22D7F4792058` (`proposal_id`),
  KEY `IDX_E83E22D76BF700BD` (`status_id`),
  KEY `IDX_E83E22D72C1C3467` (`workorder_id`),
  KEY `IDX_E83E22D75C1D7BC6` (`last_report_id`),
  KEY `IDX_E83E22D79A34590F` (`opportunity_id`),
  KEY `IDX_E83E22D7E0FCAD46` (`department_channel_id`),
  KEY `IDX_E83E22D7B4DAC98D` (`contractor_service_id`),
  CONSTRAINT `FK_E83E22D72C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_E83E22D75C1D7BC6` FOREIGN KEY (`last_report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FK_E83E22D76BF700BD` FOREIGN KEY (`status_id`) REFERENCES `service_status` (`id`),
  CONSTRAINT `FK_E83E22D771E0CC87` FOREIGN KEY (`named`) REFERENCES `service_named` (`id`),
  CONSTRAINT `FK_E83E22D7835E0EEE` FOREIGN KEY (`owner_entity_id`) REFERENCES `service` (`id`),
  CONSTRAINT `FK_E83E22D794A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FK_E83E22D79A34590F` FOREIGN KEY (`opportunity_id`) REFERENCES `opportunity` (`id`),
  CONSTRAINT `FK_E83E22D79B52D956` FOREIGN KEY (`company_last_tested`) REFERENCES `contractor` (`id`),
  CONSTRAINT `FK_E83E22D79B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_E83E22D7B4DAC98D` FOREIGN KEY (`contractor_service_id`) REFERENCES `contractor_service` (`id`),
  CONSTRAINT `FK_E83E22D7BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_E83E22D7E0FCAD46` FOREIGN KEY (`department_channel_id`) REFERENCES `department_channel` (`id`),
  CONSTRAINT `FK_E83E22D7F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal_history` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_history`
--

LOCK TABLES `service_history` WRITE;
/*!40000 ALTER TABLE `service_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_named`
--

DROP TABLE IF EXISTS `service_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_named` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `for_account` tinyint(1) NOT NULL DEFAULT '0',
  `frequency_id` int(11) DEFAULT NULL,
  `deficiency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_need_send_municipality_report` tinyint(1) NOT NULL DEFAULT '0',
  `is_need_municipality_fee` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_D892216094879022` (`frequency_id`),
  CONSTRAINT `FK_D892216094879022` FOREIGN KEY (`frequency_id`) REFERENCES `service_frequency` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_named`
--

LOCK TABLES `service_named` WRITE;
/*!40000 ALTER TABLE `service_named` DISABLE KEYS */;
INSERT INTO `service_named` VALUES (1,'General repair',0,NULL,'Please see comments',0,0),(2,'General repair',0,NULL,'Unidentified issue',0,0),(3,'Equipment installation',0,NULL,'Please see comments',0,0),(4,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(5,'Lift Station non-frequency service 1',0,NULL,'Lift Station deficiency 1',1,1),(6,'Lift Station non-frequency service 2',0,NULL,'Lift Station deficiency 2',1,1),(7,'Lift Station non-frequency service 3',0,NULL,'Lift Station deficiency 3',1,1),(8,'General repair',0,NULL,'Please see comments',0,0),(9,'General repair',0,NULL,'Unidentified issue',0,0),(10,'Equipment installation',0,NULL,'Please see comments',0,0),(11,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(12,'General repair',0,NULL,'Please see comments',0,0),(13,'General repair',0,NULL,'Unidentified issue',0,0),(14,'Equipment installation',0,NULL,'Please see comments',0,0),(15,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(16,'General repair',0,NULL,'Please see comments',0,0),(17,'General repair',0,NULL,'Unidentified issue',0,0),(18,'Equipment installation',0,NULL,'Please see comments',0,0),(19,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(20,'5 Year Section Gauge Replacement',0,6,NULL,1,0),(21,'General repair',0,NULL,'Please see comments',0,0),(22,'General repair',0,NULL,'Unidentified issue',0,0),(23,'Equipment installation',0,NULL,'Please see comments',0,0),(24,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(25,'General repair',0,NULL,'Please see comments',0,0),(26,'General repair',0,NULL,'Unidentified issue',0,0),(27,'Equipment installation',0,NULL,'Please see comments',0,0),(28,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(29,'Water Heater non-frequency service 1',0,NULL,'Water Heater deficiency 1',1,1),(30,'Water Heater non-frequency service 2',0,NULL,'Water Heater deficiency 2',1,1),(31,'Water Heater non-frequency service 3',0,NULL,'Water Heater deficiency 3',1,1),(32,'5 Year Riser Gauge Replacement',0,6,NULL,1,0),(33,'General repair',0,NULL,'Please see comments',0,0),(34,'General repair',0,NULL,'Unidentified issue',0,0),(35,'Equipment installation',0,NULL,'Please see comments',0,0),(36,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(37,'Rebuild Fire Pump',0,NULL,'Pump is not operational',0,0),(38,'Repack Fire Pump',0,NULL,'Pump is leaking more than specs allow',0,0),(39,'Replace Fire Pump',0,NULL,'Pump is not Operational/Needs to be replaced',0,0),(40,'Unplug sensing line',0,NULL,'Sensing line plugged',0,0),(41,'Adjust Fire Pump Cut In/Off Pressure',0,NULL,'Fire Pump On/Off pressure not to spec',0,0),(42,'Replace jockey pump',0,NULL,'Jockey pump Inoperative',0,0),(43,'Adjust jockey pump on/off settings',0,NULL,'Jockey pump settings incorrect',0,0),(44,'Replace mercoid switch',0,NULL,'Mercoid switch Inop',0,0),(45,'Test One Water Heater Device',0,4,NULL,1,1),(46,'Test Two Water Heater Device',0,4,NULL,1,1),(47,'General repair',0,NULL,'Please see comments',0,0),(48,'General repair',0,NULL,'Unidentified issue',0,0),(49,'Equipment installation',0,NULL,'Please see comments',0,0),(50,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(51,'Replace heater with new',0,NULL,'Sprinkler room heater inop',0,0),(52,'Install new tamper on control valve',0,NULL,'Missing tamper switch',0,0),(53,'Replace flow switch',0,NULL,'Defective flow switch',0,0),(54,'Replace sprinkler head',0,NULL,'Sprinkler head leaking',0,0),(55,'Replace sprinkler head',0,NULL,'Sprinkler head painted',0,0),(56,'Replace sprinkler head',0,NULL,'Sprinkler head corroded',0,0),(57,'Replace sprinkler head',0,NULL,'Sprinkler head has physical damage',0,0),(58,'Replace sprinkler pipe',0,NULL,'Sprinkler pipe is deteriorated/corroded',0,0),(59,'Install new sprinkler head and pipe',0,NULL,'Missing sprinkler head',0,0),(60,'Repack valve packing',0,NULL,'Valve packing leaking',0,0),(61,'Replace gauge',0,NULL,'Broken pressure gauge',0,0),(62,'Replace gauge',0,NULL,'Outdated pressure gauge',0,0),(63,'Replace Control valve',0,NULL,'Control Valve inoperative',0,0),(64,'Install Test Header Hose Valve(s)',0,NULL,'Missing Test Header Hose Valve(s)',0,0),(65,'Install Test Header Hose Valve(s)',0,NULL,'Damaged Test Header Hose Valve(s)',0,0),(66,'Install new flow switch',0,NULL,'Flow switch inoperative',0,0),(67,'Install new flow switch',0,NULL,'Flow switch seal leaking',0,0),(68,'Relocate/Move Sprinkler Head(s)',0,NULL,'Sprinkler Head(s) Needs Relocating',0,0),(69,'Locate leak and repair',0,NULL,'Unknown leak in fire system piping',0,0),(70,'Pipe main drain to outside',0,NULL,'Main drain not piped to outside',0,0),(71,'Pipe main drain to floor drain',0,NULL,'Main drain not piped to floor drain',0,0),(72,'Replace sprinkler heads with new',0,NULL,'Obsolete sprinkler heads',0,0),(73,'Replace/repair trim ring',0,NULL,'Missing/damage trim on sprinkler head',0,0),(74,'Send to lab for testing',0,NULL,'Outdated sprinkler heads',0,0),(75,'Replace all fire sprinkler heads outdated',0,NULL,'Outdated sprinkler heads',0,0),(76,'Replace all recalled sprinkler heads',0,NULL,'Recalled sprinkler heads',0,0),(77,'Correct sprinkler head orientation',0,NULL,'Sprinkler head orientation incorrect',0,0),(78,'Repair leaks on sensing line',0,NULL,'Air pressure sensing line leaking',0,0),(79,'Drain riser and replace shut-off valve',0,NULL,'Gauge shut-off valve inoperative',0,0),(80,'Repair hydrant',0,NULL,'Fire hydrant inoperative',0,0),(81,'Repair hydrant',0,NULL,'Fire hydrant leaking',0,0),(82,'Replace hydrant',0,NULL,'Fire hydrant damaged',0,0),(83,'System Flush',0,NULL,'Excessive Debris in system',0,0),(84,'Hydrostatic Test',0,NULL,'System needs Hydrostatic Testing',0,0),(85,'Rewire tamper switch',0,NULL,'Tamper switch not wired correctly',0,0),(86,'Replace tamper switch',0,NULL,'Tamper switch not working correctly',0,0),(87,'Install tamper correctly',0,NULL,'Tamper switch not installed correctly',0,0),(88,'Install tamper switch',0,NULL,'Tamper switch missing',0,0),(89,'Sync Strobe Lights',0,NULL,'Strobe Lights out of Sync',0,0),(90,'Add Tamper Switches to System',0,NULL,'Tamper Switches not Connected',0,0),(91,'Replace horn',0,NULL,'Horn not working',0,0),(92,'Replace strobe',0,NULL,'Strobe not working',0,0),(93,'General repair',0,NULL,'Please see comments',0,0),(94,'General repair',0,NULL,'Unidentified issue',0,0),(95,'Equipment installation',0,NULL,'Please see comments',0,0),(96,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(97,'Replace exterior strobe',0,NULL,'Exterior strobe not working',0,0),(98,'Replace local fire bell',0,NULL,'Local fire bell not working',0,0),(99,'Replace smoke detector',0,NULL,'Smoke detector not working',0,0),(100,'Replace heat detector',0,NULL,'Heat detector not working',0,0),(101,'Replace battery for secondary power',0,NULL,'Battery for secondary power supply failed',0,0),(102,'Add/Replace Low Air Alarm Switch',0,NULL,'Low Air Switch Missing or Damaged',0,0),(103,'Install New Strobe Light',0,NULL,'Strobe Light Missing or Needed',0,0),(104,'Connect Supervisory Switch',0,NULL,'Switch not working or missing',0,0),(105,'Inspect and bring on-line Fire Alarm Panel',0,NULL,'Panel not working at this time',0,0),(106,'Connect Flow Switch to Alarm Panel',0,NULL,'Flow Switch not monitored by FACP',0,0),(107,'Investigate Alarm Panel/Device',0,NULL,'Device not operating correctly',0,0),(108,'Replace Transceiver Batteries',0,NULL,'Transceiver Batteries did not pass testing',0,0),(109,'Replace Fire Pull Station',0,NULL,'Pull Station Inoperative',0,0),(110,'Install New Fire Panel',0,NULL,'Panel Inoperative and needs to be replaced',0,0),(111,'Recalibrate Flow Switches',0,NULL,'Flow Switches not sending Signal',0,0),(112,'Recalibrate Tamper Switches',0,NULL,'Tamper Switches not sending Signal',0,0),(113,'Rewire tamper switch',0,NULL,'Tamper switch not wired correctly',0,0),(114,'Replace tamper switch',0,NULL,'Tamper switch not working correctly',0,0),(115,'Install tamper correctly',0,NULL,'Tamper switch not installed correctly',0,0),(116,'Install tamper switch',0,NULL,'Tamper switch missing',0,0),(117,'Sync Strobe Lights',0,NULL,'Strobe Lights out of Sync',0,0),(118,'Add Tamper Switches to System',0,NULL,'Tamper Switches not Connected',0,0),(119,'Replace horn',0,NULL,'Horn not working',0,0),(120,'Replace strobe',0,NULL,'Strobe not working',0,0),(121,'General repair',0,NULL,'Please see comments',0,0),(122,'General repair',0,NULL,'Unidentified issue',0,0),(123,'Equipment installation',0,NULL,'Please see comments',0,0),(124,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(125,'Annual Fire Pump Inspection',0,4,NULL,1,1),(126,'Quarterly Fire Pump Inspection',0,2,NULL,1,1),(127,'Monthly Churn Test',0,1,NULL,1,1),(128,'Annual Air Compressor Maintenance',0,4,NULL,1,0),(129,'Annual Antifreeze System Test & Inspection',0,4,NULL,1,1),(130,'Annual Backflow Inspection',0,4,NULL,1,1),(131,'Annual Fire Line Flush',0,4,NULL,1,0),(132,'Annual Y-strainer Cleaning',0,4,NULL,1,0),(133,'Annual Fire Alarm System Inspection',0,4,NULL,1,1),(134,'Bi-Annual Fire Alarm System Inspection',0,3,NULL,1,1),(135,'Quarterly Fire Alarm System Inspection',0,2,NULL,1,1),(136,'6 Year Hydrotest',0,7,NULL,1,0),(137,'12 Year Hydrotest',0,10,NULL,1,0),(138,'Annual Fire Extinguisher(s) Inspection',0,4,NULL,1,0),(139,'5 Year Obstruction Investigation with Check Valve and Pipe Inspection',0,6,NULL,1,1),(140,'Annual Fire Sprinkler System Inspection',0,4,NULL,1,1),(141,'Bi-Annual Fire Sprinkler System Inspection',0,3,NULL,1,1),(142,'Quarterly Fire Sprinkler System Inspection',0,2,NULL,1,1),(143,'Annual Fire Hydrant Inspection',0,4,NULL,1,1),(144,'Annual Standpipe System Inspection',0,4,NULL,1,1),(145,'Bi-Annual Pre-Action System Inspection',0,3,NULL,1,1),(146,'General repair',0,NULL,'Please see comments',0,0),(147,'General repair',0,NULL,'Unidentified issue',0,0),(148,'Equipment installation',0,NULL,'Please see comments',0,0),(149,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(150,'Replace Extinguisher',0,NULL,'Extinguisher Outdated',0,0),(151,'Replace Extinguisher',0,NULL,'Extinguisher Discharged',0,0),(152,'Replace Extinguisher',0,NULL,'Extinguisher Dented/Damage to body',0,0),(153,'Recharge Extinguisher',0,NULL,'Extinguisher Discharged',0,0),(154,'Test One Lift Station Device',0,4,NULL,1,1),(155,'Test Two Lift Station Device',0,4,NULL,1,1),(156,'Annual Pre-Engineered System Inspection',0,4,NULL,1,1),(157,'Bi-Annual Pre-Engineered System Inspection',0,3,NULL,1,1),(158,'Quarterly Pre-Engineered System Inspection',0,2,NULL,1,1),(159,'General repair',0,NULL,'Please see comments',0,0),(160,'General repair',0,NULL,'Unidentified issue',0,0),(161,'Equipment installation',0,NULL,'Please see comments',0,0),(162,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(163,'Replace air compressor',0,NULL,'Air compressor inoperative',0,0),(164,'Replace check valve',0,NULL,'Air check valve leaking back',0,0),(165,'Relocate air compressor',0,NULL,'Air compressor needs relocation',0,0),(166,'General repair',0,NULL,'Please see comments',0,0),(167,'General repair',0,NULL,'Unidentified issue',0,0),(168,'Equipment installation',0,NULL,'Please see comments',0,0),(169,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(170,'Recharge System',0,NULL,'Antifreeze Deteriorated',0,0),(171,'Rebuild check valve',0,NULL,'Check valve #1 is leaking/failing',0,0),(172,'Rebuild check valve',0,NULL,'Check valve #2 is leaking/failing',0,0),(173,'Replace shut-off valve',0,NULL,'Shut-off valve #1 Inoperative',0,0),(174,'Replace shut-off valve',0,NULL,'Shut-off valve #2 Inoperative',0,0),(175,'Repack valve packing',0,NULL,'Shut-off valve #1 packing leaking',0,0),(176,'Repack valve packing',0,NULL,'Shut-off valve #2 packing leaking',0,0),(177,'Rebuild relief valve',0,NULL,'Air inlet is leaking',0,0),(178,'Unplug/rebuild relief valve',0,NULL,'Air inlet is plugged',0,0),(179,'Total HARD parts rebuild',0,NULL,'Both check valves and relief valve are leaking/failing',0,0),(180,'Total SOFT parts rebuild',0,NULL,'Both check valves and relief valve are leaking/failing',0,0),(181,'Total HARD parts rebuild',0,NULL,'Both check valves are leaking/failing',0,0),(182,'Total SOFT parts rebuild',0,NULL,'Both check valves are leaking/failing',0,0),(183,'Replace poppet assembly',0,NULL,'Broken #1 poppet assembly',0,0),(184,'Replace poppet assembly',0,NULL,'Broken #2 poppet assembly',0,0),(185,'Install new device',0,NULL,'By-Pass device missing',0,0),(186,'Install new device',0,NULL,'By-Pass device frozen/damaged',0,0),(187,'Install new sensing line',0,NULL,'Damaged sensing line',0,0),(188,'Install new test cock #1',0,NULL,'Damaged test cock #1',0,0),(189,'Install new test cock #2',0,NULL,'Damaged test cock #2',0,0),(190,'Install new test cock #3',0,NULL,'Damaged test cock #3',0,0),(191,'Install new test cock #4',0,NULL,'Damaged test cock #4',0,0),(192,'Repair valve body',0,NULL,'Device leaking from valve body',0,0),(193,'Replace device',0,NULL,'Device leaking from valve body',0,0),(194,'Install new device',0,NULL,'Device Missing',0,0),(195,'Replace O-Rings with new',0,NULL,'Device O-Rings missing or damaged',0,0),(196,'Replace device',0,NULL,'Freeze damage',0,0),(197,'Install correctly',0,NULL,'Improper Installation',0,0),(198,'Total HARD parts rebuild',0,NULL,'Low test results',0,0),(199,'Total SOFT parts rebuild',0,NULL,'Low test results',0,0),(200,'Install new test cock #1',0,NULL,'Missing test cock #1',0,0),(201,'Install new test cock #2',0,NULL,'Missing test cock #2',0,0),(202,'Install new test cock #3',0,NULL,'Missing test cock #3',0,0),(203,'Install new test cock #4',0,NULL,'Missing test cock #4',0,0),(204,'Replace spring assembly',0,NULL,'Missing/broken spring on #1 check valve',0,0),(205,'Replace spring assembly',0,NULL,'Missing/broken spring on #2 check valve',0,0),(206,'Replace spring assembly',0,NULL,'Missing/broken spring on relief valve',0,0),(207,'Install air-gap',0,NULL,'No air-gap',0,0),(208,'Install drain line',0,NULL,'No drain line',0,0),(209,'Rod floor drain to clear debris',0,NULL,'Floor drain backing up',0,0),(210,'Add Y-Strainer',0,NULL,'No Y-Strainer, debris entering device',0,0),(211,'Install new sensing line',0,NULL,'Plugged sensing line',0,0),(212,'Rebuild relief valve',0,NULL,'Relief valve is leaking/failing',0,0),(213,'Rebuild relief valve',0,NULL,'Relief valve is not opening',0,0),(214,'Replace with new',0,NULL,'Warped check covers',0,0),(215,'Replace with new',0,NULL,'Y-Strainer damaged internally',0,0),(216,'Replace with new',0,NULL,'Y-Strainer missing screen',0,0),(217,'Install new by-pass device and meter',0,NULL,'By-Pass meter and device have freeze damage',0,0),(218,'Install new meter',0,NULL,'By-Pass meter has freeze damage',0,0),(219,'Install new approved device',0,NULL,'Repair parts are no longer made',0,0),(220,'Install new approved device',0,NULL,'Currently no backflow preventer installed',0,0),(221,'Install pressure rectifier',0,NULL,'Major pressure fluctuation',0,0),(222,'Replace relief lid',0,NULL,'Relief lid is pitted and leaking',0,0),(223,'Install new approved device',0,NULL,'Brass seats are pitted and chipped',0,0),(224,'Clean and service Y-Strainer',0,NULL,'Y-strainer is plugged',0,0),(225,'Install hammer arrestor',0,NULL,'Water hammer is damaging device',0,0),(226,'Total HARD parts rebuild',0,NULL,'Broken hard parts',0,0),(227,'Replace with new',0,NULL,'Broken fitting',0,0),(228,'Replace bulk head fitting (special order)',0,NULL,'Broken bulk head',0,0),(229,'Repair as needed',0,NULL,'Device is leaking and not from relief valve',0,0),(230,'Rebuild relief valve (hard parts)',0,NULL,'Relief valve VT broken/warped',0,0),(231,'File decommission report',0,NULL,'Device no longer in use',0,0),(232,'Install vacuum breaker',0,NULL,'Site needs a vacuum breaker Installed',0,0),(233,'Install new shut off valve',0,NULL,'Shut off valve #1 missing',0,0),(234,'Install new shut off valve',0,NULL,'Shut off valve #2 missing',0,0),(235,'Clean inter valve body ports',0,NULL,'Supply line plugged',0,0),(236,'Soft parts rebuild and valve body cleaning',0,NULL,'Hydraulic moan coming from device',0,0),(237,'Replace/rebuild check valve',0,NULL,'Booster pump check failing',0,0),(238,'Repair leaking pipe upstream',0,NULL,'Main supply leaking',0,0),(239,'Repair leaking pipe downstream',0,NULL,'Piping downstream of backflow leaking',0,0),(240,'Install new approved device',0,NULL,'Obsolete device',0,0),(241,'Install new approved device',0,NULL,'Device is not repairable',0,0),(242,'Post-repair Backflow Inspection',0,NULL,'Device requires post-repair test',1,1),(243,'General repair',0,NULL,'Please see comments',0,0),(244,'General repair',0,NULL,'Unidentified issue',0,0),(245,'Equipment installation',0,NULL,'Please see comments',0,0),(246,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(247,'Replace #1 cam check assemlby',0,NULL,'#1 Cam Check is leaking/failing',0,0),(248,'Replace #2 cam check assemlby',0,NULL,'#2 Cam Check is leaking/failing',0,0),(249,'Replace both Cam Checks',0,NULL,'Both Cam Checks are leaking/failing',0,0),(250,'General repair',0,NULL,'Please see comments',0,0),(251,'General repair',0,NULL,'Unidentified issue',0,0),(252,'Equipment installation',0,NULL,'Please see comments',0,0),(253,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(254,'General repair',0,NULL,'Please see comments',0,0),(255,'General repair',0,NULL,'Unidentified issue',0,0),(256,'Equipment installation',0,NULL,'Please see comments',0,0),(257,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(258,'General repair',0,NULL,'Please see comments',0,0),(259,'General repair',0,NULL,'Unidentified issue',0,0),(260,'Equipment installation',0,NULL,'Please see comments',0,0),(261,'Inspection Notice to be sent',0,NULL,'Some inspections for device past due or incomplete',0,0),(262,'Replace FDC caps',0,NULL,'FDC caps missing/damaged',0,0),(263,'Replace FDC caps',0,NULL,'FDC caps painted shut',0,0),(264,'Replace ball drip',0,NULL,'Ball drip on FDC leaking',0,0),(265,'Install ball drip',0,NULL,'Ball drip missing on FDC',0,0),(266,'Install new FDC sign',0,NULL,'FDC sign missing',0,0),(267,'Annual Dry Valve System Partial Trip Test',0,11,NULL,1,1),(268,'Annual Dry Valve System Full Trip Test',0,11,NULL,1,1);
/*!40000 ALTER TABLE `service_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_named_device_named`
--

DROP TABLE IF EXISTS `service_named_device_named`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_named_device_named` (
  `service_named_id` int(11) NOT NULL,
  `device_named_id` int(11) NOT NULL,
  PRIMARY KEY (`service_named_id`,`device_named_id`),
  KEY `IDX_ADE484A8BC08E943` (`service_named_id`),
  KEY `IDX_ADE484A8CB4AEB5E` (`device_named_id`),
  CONSTRAINT `FK_ADE484A8BC08E943` FOREIGN KEY (`service_named_id`) REFERENCES `service_named` (`id`),
  CONSTRAINT `FK_ADE484A8CB4AEB5E` FOREIGN KEY (`device_named_id`) REFERENCES `device_named` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_named_device_named`
--

LOCK TABLES `service_named_device_named` WRITE;
/*!40000 ALTER TABLE `service_named_device_named` DISABLE KEYS */;
INSERT INTO `service_named_device_named` VALUES (1,15),(2,15),(3,15),(4,15),(5,2),(6,2),(7,2),(8,19),(9,19),(10,19),(11,19),(12,11),(13,11),(14,11),(15,11),(16,9),(17,9),(18,9),(19,9),(20,5),(20,7),(20,8),(20,10),(20,11),(20,12),(20,15),(20,16),(20,17),(20,18),(20,19),(21,16),(22,16),(23,16),(24,16),(25,12),(26,12),(27,12),(28,12),(29,1),(30,1),(31,1),(32,5),(32,7),(32,8),(32,10),(32,11),(32,12),(32,15),(32,16),(32,17),(32,18),(32,19),(33,6),(34,6),(35,6),(36,6),(37,6),(38,6),(39,6),(40,6),(41,6),(42,6),(43,6),(44,6),(45,1),(46,1),(47,5),(48,5),(49,5),(50,5),(51,5),(52,5),(53,5),(54,5),(55,5),(56,5),(57,5),(58,5),(59,5),(60,5),(61,5),(62,5),(63,5),(64,5),(65,5),(66,5),(67,5),(68,5),(69,5),(70,5),(71,5),(72,5),(73,5),(74,5),(75,5),(76,5),(77,5),(78,5),(79,5),(80,5),(81,5),(82,5),(83,5),(84,5),(85,5),(86,5),(87,5),(88,5),(89,5),(90,5),(91,5),(92,5),(93,13),(94,13),(95,13),(96,13),(97,13),(98,13),(99,13),(100,13),(101,13),(102,13),(103,13),(104,13),(105,13),(106,13),(107,13),(108,13),(109,13),(110,13),(111,13),(112,13),(113,13),(114,13),(115,13),(116,13),(117,13),(118,13),(119,13),(120,13),(121,4),(122,4),(123,4),(124,4),(125,6),(126,6),(127,6),(128,5),(128,7),(128,8),(128,10),(128,11),(128,12),(128,15),(128,16),(128,17),(128,18),(128,19),(129,5),(129,7),(129,8),(129,10),(129,11),(129,12),(129,15),(129,16),(129,17),(129,18),(129,19),(130,3),(131,3),(132,3),(133,13),(134,13),(135,13),(136,4),(136,14),(137,4),(137,14),(138,4),(138,14),(139,5),(139,7),(139,8),(139,10),(139,11),(139,12),(139,15),(139,16),(139,17),(139,18),(139,19),(140,5),(140,7),(140,8),(140,10),(140,11),(140,12),(140,15),(140,16),(140,17),(140,18),(140,19),(141,5),(141,7),(141,8),(141,10),(141,11),(141,12),(141,15),(141,16),(141,17),(141,18),(141,19),(142,5),(142,7),(142,8),(142,10),(142,11),(142,12),(142,15),(142,16),(142,17),(142,18),(142,19),(143,5),(143,7),(143,8),(143,10),(143,11),(143,12),(143,15),(143,16),(143,17),(143,18),(143,19),(144,5),(144,7),(144,8),(144,10),(144,11),(144,12),(144,15),(144,16),(144,17),(144,18),(144,19),(145,5),(145,7),(145,8),(145,10),(145,11),(145,12),(145,15),(145,16),(145,17),(145,18),(145,19),(146,14),(147,14),(148,14),(149,14),(150,14),(151,14),(152,14),(153,14),(154,2),(155,2),(156,9),(157,9),(158,9),(159,18),(160,18),(161,18),(162,18),(163,18),(164,18),(165,18),(166,7),(167,7),(168,7),(169,7),(170,7),(171,3),(172,3),(173,3),(174,3),(175,3),(176,3),(177,3),(178,3),(179,3),(180,3),(181,3),(182,3),(183,3),(184,3),(185,3),(186,3),(187,3),(188,3),(189,3),(190,3),(191,3),(192,3),(193,3),(194,3),(195,3),(196,3),(197,3),(198,3),(199,3),(200,3),(201,3),(202,3),(203,3),(204,3),(205,3),(206,3),(207,3),(208,3),(209,3),(210,3),(211,3),(212,3),(213,3),(214,3),(215,3),(216,3),(217,3),(218,3),(219,3),(220,3),(221,3),(222,3),(223,3),(224,3),(225,3),(226,3),(227,3),(228,3),(229,3),(230,3),(231,3),(232,3),(233,3),(234,3),(235,3),(236,3),(237,3),(238,3),(239,3),(240,3),(241,3),(242,3),(243,3),(244,3),(245,3),(246,3),(247,3),(248,3),(249,3),(250,10),(251,10),(252,10),(253,10),(254,8),(255,8),(256,8),(257,8),(258,17),(259,17),(260,17),(261,17),(262,17),(263,17),(264,17),(265,17),(266,17),(267,5),(267,7),(267,8),(267,10),(267,11),(267,12),(267,15),(267,16),(267,17),(267,18),(267,19),(268,5),(268,7),(268,8),(268,10),(268,11),(268,12),(268,15),(268,16),(268,17),(268,18),(268,19);
/*!40000 ALTER TABLE `service_named_device_named` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_patern`
--

DROP TABLE IF EXISTS `service_patern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_patern` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_patern`
--

LOCK TABLES `service_patern` WRITE;
/*!40000 ALTER TABLE `service_patern` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_patern` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_status`
--

DROP TABLE IF EXISTS `service_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_status`
--

LOCK TABLES `service_status` WRITE;
/*!40000 ALTER TABLE `service_status` DISABLE KEYS */;
INSERT INTO `service_status` VALUES (1,'RETEST OPPORTUNITY','under_retest_opportunity','app-status-label--yellow'),(2,'UNDER RETEST NOTICE','under_retest_notice','app-status-label--gray'),(3,'UNDER WORKORDER','under_workorder','app-status-label--gray'),(4,'DELETED','deleted','app-status-label--red'),(5,'REPAIR OPPORTUNITY','repair_opportunity','app-status-label--yellow'),(6,'UNDER PROPOSAL','under_proposal','app-status-label--gray'),(7,'RESOLVED','resolved','app-status-label--gray'),(8,'CANCELED','canceled','app-status-label--gray'),(9,'DISCARDED','discarded','app-status-label--gray'),(10,'OMITTED','omitted','app-status-label--gray');
/*!40000 ALTER TABLE `service_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `special_notification`
--

DROP TABLE IF EXISTS `special_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `special_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `division_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6792D8C341859289` (`division_id`),
  CONSTRAINT `FK_6792D8C341859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `special_notification`
--

LOCK TABLES `special_notification` WRITE;
/*!40000 ALTER TABLE `special_notification` DISABLE KEYS */;
INSERT INTO `special_notification` VALUES (1,'Domestic Water to be off for 1 hour','backflow_domestic_water_to_be_off_for_1_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(2,'Domestic Water to be off for 2 Hrs','backflow_domestic_water_to_be_off_for_2_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(3,'Domestic Water to be off for 4 Hrs','backflow_domestic_water_to_be_off_for_4_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(4,'Domestic Water will be off for 6 Hrs','backflow_domestic_water_will_be_off_for_6_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(5,'Domestic Water to be off for 8 Hrs','backflow_domestic_water_to_be_off_for_8_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(6,'Domestic Water to be off for 24 Hrs','backflow_domestic_water_to_be_off_for_24_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(7,'Domestic Water Will Not be Affected','backflow_domestic_water_will_not_be_affected','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(8,'Fire Protection Supply Will Be Off for 1 Hour','backflow_fire_protection_supply_will_be_off_for_1_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(9,'Fire Protection Supply Will Be Off for 2 Hrs','backflow_fire_protection_supply_will_be_off_for_2_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(10,'Fire Protection Supply Will Be Off for 4 Hrs','backflow_fire_protection_supply_will_be_off_for_4_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(11,'Fire Protection Supply Will Be Off for 8 Hrs','backflow_fire_protection_supply_will_be_off_for_8_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(12,'Fire Protection Supply Will Be Off for 24 Hrs','backflow_fire_protection_supply_will_be_off_for_24_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(13,'Water Supply to Equipment will be off for 1 Hour','backflow_fire_supply_to_equipment_will_be_off_for_1_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(14,'Water Supply to Equipment will be off for 2 Hour','backflow_fire_supply_to_equipment_will_be_off_for_2_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',1),(15,'Fire Protection Will Be Off for 1 Hour','fire_fire_protection_will_be_off_for_1_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',2),(16,'Fire Protection Will Be Off for 2 Hrs','fire_fire_protection_will_be_off_for_2_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',2),(17,'Fire Protection Will Be Off for 4 Hrs','fire_fire_protection_will_be_off_for_4_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',2),(18,'Fire Protection Will Be Off for 6 Hours','fire_fire_protection_will_be_off_for_6_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',2),(19,'Fire Protection Will Be Off for 8 Hrs','fire_fire_protection_will_be_off_for_8_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',2),(20,'Fire Protection Will Be Off for 24 Hrs','fire_fire_protection_will_be_off_for_24_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',2),(21,'Fire Protection Will Not be Affected','fire_fire_protection_will_not_be_affected','2018-06-19 11:00:00','2018-06-19 11:00:00',2),(22,'Alarms May Sound During Repairs','fire_alarms_may_sound_during_repairs','2018-06-19 11:00:00','2018-06-19 11:00:00',2),(23,'Domestic Water to be off for 1 hour','plumbing_domestic_water_to_be_off_for_1_hour','2018-06-19 11:00:00','2018-06-19 11:00:00',3),(24,'Domestic Water to be off for 2 Hrs','plumbing_domestic_water_to_be_off_for_2_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',3),(25,'Domestic Water to be off for 4 Hrs','plumbing_domestic_water_to_be_off_for_4_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',3),(26,'Domestic Water to be off for 6 Hrs','plumbing_domestic_water_to_be_off_for_6_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',3),(27,'Domestic Water to be off for 8 Hrs','plumbing_domestic_water_to_be_off_for_8_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',3),(28,'Domestic Water to be off for 24 Hrs','plumbing_domestic_water_to_be_off_for_24_hours','2018-06-19 11:00:00','2018-06-19 11:00:00',3),(29,'Domestic Water Will Not be Affected','plumbing_domestic_water_will_not_be_affected','2018-06-19 11:00:00','2018-06-19 11:00:00',3);
/*!40000 ALTER TABLE `special_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Alabama','alabama','2018-06-19 10:59:38','2018-06-19 10:59:38','AL'),(2,'Alaska','alaska','2018-06-19 10:59:38','2018-06-19 10:59:38','AK'),(3,'Arizona','arizona','2018-06-19 10:59:38','2018-06-19 10:59:38','AZ'),(4,'Arkansas','arkansas','2018-06-19 10:59:38','2018-06-19 10:59:38','AR'),(5,'California','california','2018-06-19 10:59:38','2018-06-19 10:59:38','CA'),(6,'Colorado','colorado','2018-06-19 10:59:38','2018-06-19 10:59:38','CO'),(7,'Connecticut','connecticut','2018-06-19 10:59:38','2018-06-19 10:59:38','CT'),(8,'Delaware','delaware','2018-06-19 10:59:38','2018-06-19 10:59:38','DE'),(9,'Florida','florida','2018-06-19 10:59:38','2018-06-19 10:59:38','FL'),(10,'Georgia','georgia','2018-06-19 10:59:38','2018-06-19 10:59:38','GA'),(11,'Hawaii','hawaii','2018-06-19 10:59:38','2018-06-19 10:59:38','HI'),(12,'Idaho','idaho','2018-06-19 10:59:38','2018-06-19 10:59:38','ID'),(13,'Illinois','illinois','2018-06-19 10:59:38','2018-06-19 10:59:38','IL'),(14,'Indiana','indiana','2018-06-19 10:59:38','2018-06-19 10:59:38','IN'),(15,'Iowa','iowa','2018-06-19 10:59:38','2018-06-19 10:59:38','IA'),(16,'Kansas','kansas','2018-06-19 10:59:38','2018-06-19 10:59:38','KS'),(17,'Kentucky','kentucky','2018-06-19 10:59:38','2018-06-19 10:59:38','KY'),(18,'Louisiana','louisiana','2018-06-19 10:59:38','2018-06-19 10:59:38','LA'),(19,'Maine','maine','2018-06-19 10:59:38','2018-06-19 10:59:38','ME'),(20,'Maryland','maryland','2018-06-19 10:59:38','2018-06-19 10:59:38','MD'),(21,'Massachusetts','massachusetts','2018-06-19 10:59:38','2018-06-19 10:59:38','MA'),(22,'Michigan','michigan','2018-06-19 10:59:38','2018-06-19 10:59:38','MI'),(23,'Minnesota','minnesota','2018-06-19 10:59:38','2018-06-19 10:59:38','MN'),(24,'Mississippi','mississippi','2018-06-19 10:59:38','2018-06-19 10:59:38','MS'),(25,'Missouri','missouri','2018-06-19 10:59:38','2018-06-19 10:59:38','MO'),(26,'Montana','montana','2018-06-19 10:59:38','2018-06-19 10:59:38','MT'),(27,'Nebraska','nebraska','2018-06-19 10:59:38','2018-06-19 10:59:38','NE'),(28,'Nevada','nevada','2018-06-19 10:59:38','2018-06-19 10:59:38','NV'),(29,'New Hampshire','new hampshire','2018-06-19 10:59:38','2018-06-19 10:59:38','NH'),(30,'New Jersey','new jersey','2018-06-19 10:59:38','2018-06-19 10:59:38','NJ'),(31,'New Mexico','new mexico','2018-06-19 10:59:38','2018-06-19 10:59:38','NM'),(32,'New York','new york','2018-06-19 10:59:38','2018-06-19 10:59:38','NY'),(33,'North Carolina','north carolina','2018-06-19 10:59:38','2018-06-19 10:59:38','NC'),(34,'North Dakota','north dakota','2018-06-19 10:59:38','2018-06-19 10:59:38','ND'),(35,'Ohio','ohio','2018-06-19 10:59:38','2018-06-19 10:59:38','OH'),(36,'Oklahoma','oklahoma','2018-06-19 10:59:38','2018-06-19 10:59:38','OK'),(37,'Oregon','oregon','2018-06-19 10:59:38','2018-06-19 10:59:38','OR'),(38,'Pennsylvania','pennsylvania','2018-06-19 10:59:38','2018-06-19 10:59:38','PA'),(39,'Rhode Island','rhode island','2018-06-19 10:59:38','2018-06-19 10:59:38','RI'),(40,'South Carolina','south carolina','2018-06-19 10:59:38','2018-06-19 10:59:38','SC'),(41,'South Dakota','south dakota','2018-06-19 10:59:38','2018-06-19 10:59:38','SD'),(42,'Tennessee','tennessee','2018-06-19 10:59:38','2018-06-19 10:59:38','TN'),(43,'Texas','texas','2018-06-19 10:59:38','2018-06-19 10:59:38','TX'),(44,'Utah','utah','2018-06-19 10:59:38','2018-06-19 10:59:38','UT'),(45,'Vermont','vermont','2018-06-19 10:59:38','2018-06-19 10:59:38','VT'),(46,'Virginia','virginia','2018-06-19 10:59:38','2018-06-19 10:59:38','VA'),(47,'Washington','washington','2018-06-19 10:59:38','2018-06-19 10:59:38','WA'),(48,'West Virginia','west virginia','2018-06-19 10:59:38','2018-06-19 10:59:38','WV'),(49,'Wisconsin','wisconsin','2018-06-19 10:59:38','2018-06-19 10:59:38','WI'),(50,'Wyoming','wyoming','2018-06-19 10:59:38','2018-06-19 10:59:38','WY');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder`
--

DROP TABLE IF EXISTS `workorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `grand_total` double DEFAULT NULL,
  `total_service_fee` double DEFAULT NULL,
  `directions` longtext COLLATE utf8_unicode_ci,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `internal_comment` longtext COLLATE utf8_unicode_ci,
  `authorizer_id` int(11) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `scheduled_from` datetime DEFAULT NULL,
  `scheduled_to` datetime DEFAULT NULL,
  `is_frozen` tinyint(1) DEFAULT '0',
  `account_authorizer_id` int(11) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `total_estimated_time_inspections` double DEFAULT NULL,
  `total_estimated_time_repairs` double DEFAULT NULL,
  `additional_comments` longtext COLLATE utf8_unicode_ci,
  `after_hours_work_cost` double DEFAULT NULL,
  `additional_services_cost` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `total_repairs_price` double DEFAULT NULL,
  `total_munic_and_proc_fee` double DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_inspections_count` int(11) DEFAULT NULL,
  `total_repairs_count` int(11) DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `finisher_id` int(11) DEFAULT NULL,
  `signature_owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_51CF52BB9B6B5FBA` (`account_id`),
  KEY `IDX_51CF52BB6BF700BD` (`status_id`),
  KEY `IDX_51CF52BB41859289` (`division_id`),
  KEY `IDX_51CF52BB3CEFA549` (`authorizer_id`),
  KEY `IDX_51CF52BB5AA1164F` (`payment_method_id`),
  KEY `IDX_51CF52BBA3C999FA` (`account_authorizer_id`),
  KEY `IDX_51CF52BB17653B16` (`payment_term_id`),
  KEY `IDX_51CF52BB64D218E` (`location_id`),
  KEY `IDX_51CF52BBC54C8C93` (`type_id`),
  KEY `IDX_51CF52BBB03A8386` (`created_by_id`),
  KEY `IDX_51CF52BB95D2E802` (`finisher_id`),
  KEY `IDX_51CF52BBCD50E748` (`signature_owner_id`),
  CONSTRAINT `FK_51CF52BB17653B16` FOREIGN KEY (`payment_term_id`) REFERENCES `payment_term` (`id`),
  CONSTRAINT `FK_51CF52BB3CEFA549` FOREIGN KEY (`authorizer_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_51CF52BB41859289` FOREIGN KEY (`division_id`) REFERENCES `device_category` (`id`),
  CONSTRAINT `FK_51CF52BB5AA1164F` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`),
  CONSTRAINT `FK_51CF52BB64D218E` FOREIGN KEY (`location_id`) REFERENCES `coordinate` (`id`),
  CONSTRAINT `FK_51CF52BB6BF700BD` FOREIGN KEY (`status_id`) REFERENCES `workorder_status` (`id`),
  CONSTRAINT `FK_51CF52BB95D2E802` FOREIGN KEY (`finisher_id`) REFERENCES `contractor_user` (`id`),
  CONSTRAINT `FK_51CF52BB9B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_51CF52BBA3C999FA` FOREIGN KEY (`account_authorizer_id`) REFERENCES `account_contact_person` (`id`),
  CONSTRAINT `FK_51CF52BBB03A8386` FOREIGN KEY (`created_by_id`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_51CF52BBC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `workorder_type` (`id`),
  CONSTRAINT `FK_51CF52BBCD50E748` FOREIGN KEY (`signature_owner_id`) REFERENCES `account_contact_person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder`
--

LOCK TABLES `workorder` WRITE;
/*!40000 ALTER TABLE `workorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `workorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_device_info`
--

DROP TABLE IF EXISTS `workorder_device_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_device_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repair_special_notification_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `repair_comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_estimation_time` double DEFAULT NULL,
  `inspection_estimation_time` double DEFAULT NULL,
  `subtotal_inspections_fee` double DEFAULT NULL,
  `repair_parts_and_labour_price` double DEFAULT NULL,
  `subtotal_munic_and_proc_fee` double DEFAULT NULL,
  `device_total` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D53ECF8555272D6` (`repair_special_notification_id`),
  KEY `IDX_D53ECF852C1C3467` (`workorder_id`),
  KEY `IDX_D53ECF8594A4C7D4` (`device_id`),
  CONSTRAINT `FK_D53ECF852C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_D53ECF8555272D6` FOREIGN KEY (`repair_special_notification_id`) REFERENCES `special_notification` (`id`),
  CONSTRAINT `FK_D53ECF8594A4C7D4` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_device_info`
--

LOCK TABLES `workorder_device_info` WRITE;
/*!40000 ALTER TABLE `workorder_device_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `workorder_device_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_frozen_content`
--

DROP TABLE IF EXISTS `workorder_frozen_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_frozen_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_history_id` int(11) DEFAULT NULL,
  `device_history_id` int(11) DEFAULT NULL,
  `service_history_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `municipality_fee` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `processing_fee` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AE06C4E7E29E76F6` (`account_history_id`),
  KEY `IDX_AE06C4E79936C794` (`device_history_id`),
  KEY `IDX_AE06C4E78854BDE6` (`service_history_id`),
  KEY `IDX_AE06C4E72C1C3467` (`workorder_id`),
  CONSTRAINT `FK_AE06C4E72C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_AE06C4E78854BDE6` FOREIGN KEY (`service_history_id`) REFERENCES `service_history` (`id`),
  CONSTRAINT `FK_AE06C4E79936C794` FOREIGN KEY (`device_history_id`) REFERENCES `device_history` (`id`),
  CONSTRAINT `FK_AE06C4E7E29E76F6` FOREIGN KEY (`account_history_id`) REFERENCES `account_history` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_frozen_content`
--

LOCK TABLES `workorder_frozen_content` WRITE;
/*!40000 ALTER TABLE `workorder_frozen_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `workorder_frozen_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_log`
--

DROP TABLE IF EXISTS `workorder_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `proposal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3711E899F675F31B` (`author_id`),
  KEY `IDX_3711E8992C1C3467` (`workorder_id`),
  KEY `IDX_3711E899C54C8C93` (`type_id`),
  KEY `IDX_3711E8992989F1FD` (`invoice_id`),
  KEY `IDX_3711E899F4792058` (`proposal_id`),
  CONSTRAINT `FK_3711E8992989F1FD` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  CONSTRAINT `FK_3711E8992C1C3467` FOREIGN KEY (`workorder_id`) REFERENCES `workorder` (`id`),
  CONSTRAINT `FK_3711E899C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `workorder_log_type` (`id`),
  CONSTRAINT `FK_3711E899F4792058` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`id`),
  CONSTRAINT `FK_3711E899F675F31B` FOREIGN KEY (`author_id`) REFERENCES `contractor_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_log`
--

LOCK TABLES `workorder_log` WRITE;
/*!40000 ALTER TABLE `workorder_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `workorder_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_log_type`
--

DROP TABLE IF EXISTS `workorder_log_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_log_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_log_type`
--

LOCK TABLES `workorder_log_type` WRITE;
/*!40000 ALTER TABLE `workorder_log_type` DISABLE KEYS */;
INSERT INTO `workorder_log_type` VALUES (1,'WORKORDER WAS CREATED','workorder_was_created'),(2,'WORKORDER WAS SCHEDULED','workorder_was_scheduled'),(3,'Reports sent','reports_sent'),(4,'Payment received','payment_received'),(5,'Bill Sent / Invoice Created','bill_sent_invoice_created'),(6,'Scheduled date has come','scheduled_date_has_come'),(7,'Scheduled date has passed','scheduled_date_has_passed'),(8,'DONE','done');
/*!40000 ALTER TABLE `workorder_log_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_status`
--

DROP TABLE IF EXISTS `workorder_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `css_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_status`
--

LOCK TABLES `workorder_status` WRITE;
/*!40000 ALTER TABLE `workorder_status` DISABLE KEYS */;
INSERT INTO `workorder_status` VALUES (1,'To Be Scheduled','to_be_scheduled',1,'app-status-label--red'),(2,'Scheduled','scheduled',2,'app-status-label--green'),(3,'TO BE DONE TODAY','to_be_done_today',3,'app-status-label--yellow'),(4,'NOT COMPLETED','not_completed',4,'app-status-label--red'),(5,'PENDING PAYMENT','pending_payment',6,'app-status-label--green'),(6,'Send Reports','send_reports',7,'app-status-label--red'),(7,'Send Bill/Create Invoice','send_bill_create_invoice',5,'app-status-label--red'),(8,'DONE','done',8,'app-status-label--gray');
/*!40000 ALTER TABLE `workorder_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workorder_type`
--

DROP TABLE IF EXISTS `workorder_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workorder_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workorder_type`
--

LOCK TABLES `workorder_type` WRITE;
/*!40000 ALTER TABLE `workorder_type` DISABLE KEYS */;
INSERT INTO `workorder_type` VALUES (1,'Generic Workorder','generic_workorder'),(2,'Workorder','simple_workorder');
/*!40000 ALTER TABLE `workorder_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-19 11:00:48
