<?php

namespace AppBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Tests\TestTrait\AuthorizedClientTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FunctionalTestCase extends WebTestCase
{
    use AuthorizedClientTrait;

    /** @var ContainerInterface $container */
    private $container;

    /**
     * @param $serviceName
     * @return object
     */
    protected function get($serviceName)
    {
        $this->container = static::$kernel->getContainer();

        return $this->container->get($serviceName);
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route         The name of the route
     * @param mixed  $parameters    An array of parameters
     * @param int    $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    protected function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->get('router')->generate($route, $parameters, $referenceType);
    }
}
