<?php

namespace AppBundle\ActionManagers;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Services\Address\AddressProvider;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\NonUniqueResultException;
use InvoiceBundle\Repository\CustomerRepository;

class AccountContactPersonActionManger
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var CustomerRepository */
    private $customerRepository;

    /**
     * AccountContactPersonActionManger constructor.
     * @param ObjectManager $objectManager
     * @param CustomerRepository $customerRepository
     */
    public function __construct(
        ObjectManager $objectManager,
        CustomerRepository $customerRepository
    ) {
        $this->objectManager = $objectManager;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     */
    public function resetCustomerToContactPerson(AccountContactPerson $accountContactPerson)
    {
        try {
            $customer = $this->customerRepository->findByContactPerson($accountContactPerson->getContactPerson());

        } catch (NonUniqueResultException $exception) {
            $customer = null;
        }

        $accountContactPerson->setCustomer($customer);
        AddressProvider::setInNullAddress($accountContactPerson->getCustomBillingAddress());

        $this->objectManager->flush();
    }
}