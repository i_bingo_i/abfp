<?php

namespace AppBundle\ActionManagers;

use AppBundle\DTO\Requests\CreateCompanyAndLinkedToAccountDTO;
use AppBundle\Entity\Account;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\DTO\Requests\SaveCompanyRequest;
use InvoiceBundle\ActionManagers\CustomerActionManager;
use InvoiceBundle\DataTransportObject\Requests\CreateCustomerDTO;
use AppBundle\DTO\Requests\CreateCompanyAndLinkedToCPDTO;
use AppBundle\Entity\ContactPerson;
use InvoiceBundle\DataTransportObject\Requests\UpdateCustomerDTO;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Repository\CustomerRepository;
use AppBundle\Events\CompanyCreatedEvent;
use AppBundle\Events\CompanyUpdatedEvent;
use AppBundle\Events\ContactPersonUpdatedEvent;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CompanyActionManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var CustomerActionManager */
    private $customerActionManager;

    /**
     * CompanyActionManager constructor.
     *
     * @param ObjectManager $objectManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        ObjectManager $objectManager,
        EventDispatcherInterface $dispatcher,
        CustomerActionManager $customerActionManager
    ) {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;
        $this->customerActionManager = $customerActionManager;
    }

    /**
     * @param SaveCompanyRequest $createCompanyDTO
     *
     * @return Company
     */
    public function create(SaveCompanyRequest $createCompanyDTO)
    {
        /** @var Company $company */
        $company = $createCompanyDTO->getCompany();
        $this->objectManager->persist($company);
        $this->objectManager->flush();

        $this->createCustomerForCompany($company, $createCompanyDTO->getBillingAddress());

        /** @var CompanyCreatedEvent $event */
        $event = new CompanyCreatedEvent($company);
        $this->dispatcher->dispatch('company.created', $event);

        return $company;
    }

    /**
     * @param SaveCompanyRequest $createCompanyDTO
     * @return Company
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function update(SaveCompanyRequest $createCompanyDTO)
    {
        /** @var Company $company */
        $company = $createCompanyDTO->getCompany();
        $this->objectManager->persist($company);

        /** @var CustomerRepository $customerRepository */
        $customerRepository = $this->objectManager->getRepository('InvoiceBundle:Customer');
        /** @var Customer $customer */
        $customer = $customerRepository->findByCompany($company);
        $customerName = $company->getName();
        $updateCustomerDTO = new UpdateCustomerDTO();
        $updateCustomerDTO
            ->setCustomer($customer)
            ->setBillingAddress($createCompanyDTO->getBillingAddress())
            ->setName($customerName);
        $this->customerActionManager->refreshSequence($customer);
        $this->customerActionManager->update($updateCustomerDTO);

        /** @var Address $billingAddress */
        $billingAddress = $createCompanyDTO->getBillingAddress();
        $this->objectManager->persist($billingAddress);

        $this->objectManager->flush();

        /** @var CompanyUpdatedEvent $event */
        $event = new CompanyUpdatedEvent($company);
        $this->dispatcher->dispatch('company.updated', $event);

        return $company;
    }

    /**
     * @param CreateCompanyAndLinkedToCPDTO $createAndLinkedToCpRequest
     *
     * @return Company
     */
    public function createAndLinkedCP(CreateCompanyAndLinkedToCPDTO $createAndLinkedToCpRequest)
    {
        /** @var Company $company */
        $company = $createAndLinkedToCpRequest->getCompany();
        $this->objectManager->persist($company);

        /** @var ContactPerson $contactPerson */
        $contactPerson = $createAndLinkedToCpRequest->getContactPerson();
        $contactPerson->setCompany($company);
        $this->objectManager->persist($contactPerson);

        $this->objectManager->flush();
        $this->createCustomerForCompany($company, $createAndLinkedToCpRequest->getBillingAddress());

        /** @var CompanyCreatedEvent $eventCreateCompany */
        $eventCreateCompany = new CompanyCreatedEvent($company);
        $this->dispatcher->dispatch('company.created', $eventCreateCompany);
        /** @var ContactPersonUpdatedEvent $eventUpdateCp */
        $eventUpdateCp = new ContactPersonUpdatedEvent($contactPerson);
        $this->dispatcher->dispatch('contact_person.updated', $eventUpdateCp);

        return $company;
    }

    /**
     * @param CreateCompanyAndLinkedToAccountDTO $createAndLinkedToAccountRequest
     *
     * @return Company
     */
    public function createAndLinkedToAccount(CreateCompanyAndLinkedToAccountDTO $createAndLinkedToAccountRequest)
    {
        /** @var Company $company */
        $company = $createAndLinkedToAccountRequest->getCompany();
        $this->objectManager->persist($company);

        /** @var Account $account */
        $account = $createAndLinkedToAccountRequest->getAccount();
        $account->setCompany($company);
        $this->objectManager->persist($account);

        $this->objectManager->flush();
        $this->createCustomerForCompany($company, $createAndLinkedToAccountRequest->getBillingAddress());

        /** @var CompanyCreatedEvent $eventCreateCompany */
        $eventCreateCompany = new CompanyCreatedEvent($company);
        $this->dispatcher->dispatch('company.created', $eventCreateCompany);

        return $company;
    }

    /**
     * @param Company $company
     *
     * @param Address $billingAddress
     */
    private function createCustomerForCompany(Company $company, Address $billingAddress)
    {
        /** @var CreateCustomerDTO $createCustomerDTO */
        $createCustomerDTO = new CreateCustomerDTO();
        $createCustomerDTO->setEntity($company);
        $createCustomerDTO->setBillingAddress($billingAddress);

        $this->customerActionManager->create($createCustomerDTO);
    }
}
