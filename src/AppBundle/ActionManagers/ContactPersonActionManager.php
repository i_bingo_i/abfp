<?php

namespace AppBundle\ActionManagers;

use AppBundle\DTO\Requests\SaveContactPersonRequest;
use InvoiceBundle\ActionManagers\CustomerActionManager;
use InvoiceBundle\DataTransportObject\Requests\UpdateCustomerDTO;
use InvoiceBundle\DataTransportObject\Requests\CreateCustomerDTO;
use AppBundle\Entity\Address;
use AppBundle\Entity\ContactPerson;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Repository\CustomerRepository;
use AppBundle\Events\ContactPersonCreatedEvent;
use AppBundle\Events\ContactPersonUpdatedEvent;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ContactPersonActionManager
{
    /** @var Customer */
    public $customer;
    /** @var  ObjectManager */
    private $objectManager;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var CustomerActionManager */
    private $customerActionManager;

    /**
     * CompanyActionManager constructor.
     *
     * @param ObjectManager $objectManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        ObjectManager $objectManager,
        EventDispatcherInterface $dispatcher,
        CustomerActionManager $customerActionManager
    ) {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;
        $this->customerActionManager = $customerActionManager;
    }

    /**
     * @param SaveContactPersonRequest $saveContactPersonDTO
     *
     * @return ContactPerson
     */
    public function create(SaveContactPersonRequest $saveContactPersonDTO)
    {
        /** @var ContactPerson $contactPerson */
        $contactPerson = $saveContactPersonDTO->getContactPerson();
        $this->objectManager->persist($contactPerson);
        $this->objectManager->flush();

        /** @var CreateCustomerDTO $createCustomerDTO */
        $createCustomerDTO = new CreateCustomerDTO();
        $createCustomerDTO->setEntity($contactPerson);
        $createCustomerDTO->setBillingAddress($saveContactPersonDTO->getBillingAddress());
        $this->customer = $this->customerActionManager->create($createCustomerDTO);

        /** @var ContactPersonCreatedEvent $event */
        $event = new ContactPersonCreatedEvent($contactPerson);
        $this->dispatcher->dispatch('contact_person.created', $event);

        return $contactPerson;
    }

    /**
     * @param SaveContactPersonRequest $saveContactPersonDTO
     * @return ContactPerson
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(SaveContactPersonRequest $saveContactPersonDTO)
    {
        /** @var ContactPerson $contactPerson */
        $contactPerson = $saveContactPersonDTO->getContactPerson();
        $this->objectManager->persist($contactPerson);
        /** @var Address $billingAddress */
        $billingAddress = $saveContactPersonDTO->getBillingAddress();
        $this->objectManager->persist($billingAddress);
        $this->objectManager->flush();

        /** @var CustomerRepository $customerRepository */
        $customerRepository = $this->objectManager->getRepository('InvoiceBundle:Customer');
        /** @var Customer $customer */
        $customer = $customerRepository->findByContactPerson($contactPerson);
        $updateCustomerDTO = new UpdateCustomerDTO();
        $customerName = $contactPerson->getFirstName() . ' ' . $contactPerson->getLastName();
        $updateCustomerDTO
            ->setCustomer($customer)
            ->setBillingAddress($saveContactPersonDTO->getBillingAddress())
            ->setName($customerName);
        $this->customerActionManager->refreshSequence($customer);
        $this->customerActionManager->update($updateCustomerDTO);

        /** @var ContactPersonUpdatedEvent $event */
        $event = new ContactPersonUpdatedEvent($contactPerson);
        $this->dispatcher->dispatch('contact_person.updated', $event);

        return $contactPerson;
    }
}
