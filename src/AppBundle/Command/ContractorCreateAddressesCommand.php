<?php

namespace AppBundle\Command;

use AppBundle\Entity\Address;
use AppBundle\Entity\AddressHistory;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\Contractor;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\ContractorRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\Common\Persistence\ObjectManager;

class ContractorCreateAddressesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('contractor:create:addresses')
            ->setHelp('Create Addresses')
            ->setDescription('Create Addresses')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Addresses creating...');

        $startTime = microtime(true);

        /** @var ObjectManager $objectManager */
        $objectManager = $this->getContainer()->get('app.entity_manager');
        /** @var ContractorRepository $contractorRepository */
        $contractorRepository = $objectManager->getRepository('AppBundle:Contractor');
        /** @var AddressTypeRepository $addressTypeRepository */
        $addressTypeRepository = $objectManager->getRepository('AppBundle:AddressType');

        /** @var AddressType $addresTypeContractor */
        $addresTypeContractor = $addressTypeRepository->findOneBy(['alias' => 'contractor address']);
        /** @var Contractor[] $contractors */
        $contractors = $contractorRepository->findBy(['address' => null]);

        /** @var Contractor $contractor */
        foreach ($contractors as $contractor) {
            $newAddress = new Address();
            $newAddress->setAddressType($addresTypeContractor);
            $objectManager->persist($newAddress);

            $addressHistory = new AddressHistory();
            $addressHistory->setAddressType($newAddress->getAddressType());
            $addressHistory->setOwnerEntity($newAddress);
            $addressHistory->setDateCreate($newAddress->getDateCreate());
            $addressHistory->setDateUpdate($newAddress->getDateUpdate());
            $objectManager->persist($addressHistory);

            $contractor->setAddress($newAddress);
        }
        $objectManager->flush();

        $endTime = microtime(true);
        $executionTime = $endTime - $startTime;

        $io->success(
            'Contractor create addresses had been being executed for '.$executionTime.' sec. and finished successfully!'
        );
    }
}
