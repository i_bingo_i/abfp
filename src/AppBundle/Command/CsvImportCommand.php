<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;

class CsvImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('csv:import')
            ->addArgument('filename', InputArgument::REQUIRED, 'The file name.')
            ->setHelp('Imports data from CSV file to database tables of the Project')
            ->setDescription('Imports data from CSV file to database tables of the Project')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Data importing from CSV-file...');

        $csvImportService = $this->getContainer()->get('app.csv_import.service');

        $startTime = microtime(true);

        $csvImportService->parsingData($input->getArgument('filename'));

        $csvImportService->importDataFromCSVFile();

        $endTime = microtime(true);
        $executionTime = $endTime - $startTime;

        $io->success(
            'Data import from CSV file had been being executed for '.$executionTime.' sec. and finished successfully!'
        );
    }
}
