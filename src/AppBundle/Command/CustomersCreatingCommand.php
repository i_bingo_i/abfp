<?php

namespace AppBundle\Command;

use AppBundle\Creators\AddressCreator;
use InvoiceBundle\Creators\CustomerCreator;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Repository\CustomerRepository;
use Doctrine\Common\Persistence\ObjectManager;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CustomersCreatingCommand extends ContainerAwareCommand
{
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var AddressCreator */
    private $addressCreator;
    /** @var  ObjectManager */
    private $objectManager;
    /** @var OrgLevelToCompanyService $importCompaniesService */
    private $importCompaniesService;
    /** @var CustomerCreator */
    private $customerCreator;
    /** @var CustomerRepository */
    private $customerRepository;

    /**
     * CustomersCreatingCommand constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->customerRepository = $objectManager->getRepository('InvoiceBundle:Customer');

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('customers:create')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');
        $this->customerCreator = $this->getContainer()->get('customer.creator');
        $this->dispatcher = $this->getContainer()->get('event_dispatcher');
        $this->addressCreator = $this->getContainer()->get('address.creator');

        $companyRepository = $this->objectManager->getRepository('AppBundle:Company');
        /** @var Company[] $companies */
        $companies = $companyRepository->findAllWithoutCustomer();
        if (count($companies)) {
            $this->creatingForCompany($companies, $output);
        }

        $contactPersonRepository = $this->objectManager->getRepository('AppBundle:ContactPerson');
        /** @var ContactPerson[] $contactPersons */
        $contactPersons = $contactPersonRepository->findAllWithoutCustomer();
        if (count($contactPersons)) {
            $this->creatingForContactPerson($contactPersons, $output);
        }

        $accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
        $accountContactPersons = $accountContactPersonRepository->findAll();
        if (count($accountContactPersons)) {
            $this->creatingForAccountContactPerson($accountContactPersons, $output);
        }

        if (!count($companies) and !count($contactPersons) and !count($accountContactPersons)) {
            $output->writeln('No processed elements');
        }
    }

    /**
     * @param array $companies
     * @param OutputInterface $output
     */
    private function creatingForCompany(array $companies, OutputInterface $output)
    {
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Creating customers for company was started');

        $output = new ConsoleOutput();
        $progress = new ProgressBar($output, count($companies));
        $progress->start();

        $i = 0;
        /** @var Company $company */
        foreach ($companies as $company) {
            /** @var Address $billingAddress */
            $billingAddress = $this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE);
            /** @var Customer $customer */
            $customer = $this->customerCreator->make($company, $billingAddress);
            $this->objectManager->persist($customer);

            if ($i % 1000 == 0 and $i!=0) {
                $this->objectManager->flush();
            }
            $i++;

            $progress->advance();
        }

        $this->objectManager->flush();

        /** @var \DateTime $finishTime */
        $finishTime = new \DateTime('now');
        $timeInterval = $this->importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln('');
        $output->writeln($finishTime->format("m/d/Y H:i:s").' Creating customers for company was started completed!');
        $output->writeln('');
        $output->writeln($timeInterval);
    }

    /**
     * @param array $contactPersons
     * @param OutputInterface $output
     */
    private function creatingForContactPerson(array $contactPersons, OutputInterface $output)
    {
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Creating customers for contact person was started');

        $output = new ConsoleOutput();
        $progress = new ProgressBar($output, count($contactPersons));
        $progress->start();

        $i = 0;
        /** @var ContactPerson $person */
        foreach ($contactPersons as $person) {
            /** @var Address $billingAddress */
            $billingAddress = $this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE);
            /** @var Customer $customer */
            $customer = $this->customerCreator->make($person, $billingAddress);
            $this->objectManager->persist($customer);

            if ($i % 1000 == 0 and $i!=0) {
                $this->objectManager->flush();
            }
            $i++;

            $progress->advance();
        }

        $this->objectManager->flush();

        /** @var \DateTime $finishTime */
        $finishTime = new \DateTime('now');
        $timeInterval = $this->importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln('');
        $output->writeln(
            $finishTime->format("m/d/Y H:i:s").' Creating customers for contact person was started completed!'
        );
        $output->writeln('');
        $output->writeln($timeInterval);
    }

    /**
     * @param array $accountContactPersons
     * @param OutputInterface $output
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function creatingForAccountContactPerson(array $accountContactPersons, OutputInterface $output)
    {
        $startTime = new \DateTime('now');
        $output->writeln(
            $startTime->format("m/d/Y H:i:s").' Creating customers for account contact person was started'
        );

        $output = new ConsoleOutput();
        $progress = new ProgressBar($output, count($accountContactPersons));
        $progress->start();

        $i = 0;
        /** @var AccountContactPerson $acp */
        foreach ($accountContactPersons as $acp) {
            /** @var Customer $customer */
            $customer = $this->customerRepository->findByContactPerson($acp->getContactPerson());
            $acp->setCustomer($customer);
            $this->objectManager->persist($acp);

            if ($i % 1000 == 0 and $i!=0) {
                $this->objectManager->flush();
            }
            $i++;

            $progress->advance();
        }

        $this->objectManager->flush();

        /** @var \DateTime $finishTime */
        $finishTime = new \DateTime('now');
        $timeInterval = $this->importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln('');
        $output->writeln(
            $finishTime->format("m/d/Y H:i:s").' Creating customers for account contact person was started completed!'
        );
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('');

        $progress->finish();
    }
}
