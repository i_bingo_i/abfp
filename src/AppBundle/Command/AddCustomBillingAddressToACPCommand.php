<?php

namespace AppBundle\Command;

use AppBundle\Creators\AddressCreator;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\AddressHistory;
use AppBundle\Entity\AddressType;
//use AppBundle\Entity\ContactPersonHistory;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\ContactPersonHistoryRepository;
//use AppBundle\Factories\AccountContactPersonHistoryFactory;
use AppBundle\Services\AccountContactPersonHistory\Creator;
use DateTime;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;
use AppBundle\Factories\AddressHistoryFactory;

class AddCustomBillingAddressToACPCommand extends ContainerAwareCommand
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var AddressCreator */
    private $addressCreator;
    /** @var OrgLevelToCompanyService $importCompaniesService */
    private $importCompaniesService;
//    /** @var array */
//    private $addresses = [];
    /** @var ContactPersonHistoryRepository */
    private $contactPersonHistoryRepository;
    /** @var Creator */
    private $accountContactPersonHistoryCreator;

    /**
     * CustomersCreatingCommand constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct();

        $this->objectManager = $objectManager;
        $this->accountContactPersonRepository = $objectManager->getRepository('AppBundle:AccountContactPerson');
        $this->contactPersonHistoryRepository = $this->objectManager
            ->getRepository('AppBundle:ContactPersonHistory');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('acp:updating')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->addressCreator = $this->getContainer()->get('address.creator');
        $this->importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');
        $this->accountContactPersonHistoryCreator = $this->getContainer()
            ->get('app.account_contact_person_history_creator.service');

        $accountContactPersons = $this->accountContactPersonRepository->findBy(['customBillingAddress' => null]);
//        $quantity = count($accountContactPersons);

//        $addressesQuantity = $this->createAddresses($quantity, $output);
        $ACPQuantity = $this->updateACP($accountContactPersons, $output);

        if (!$addressesQuantity and !$ACPQuantity) {
            $output->writeln('No processed elements');
        }
    }

//    /**
//     * @param integer $quantity
//     * @param OutputInterface $output
//     * @return int
//     * @throws \Exception
//     */
//    private function createAddresses($quantity, OutputInterface $output)
//    {
//        $startTime = new DateTime('now');
//        $output->writeln($startTime->format("m/d/Y H:i:s").' Creating empty addresses was started');
//
//        $output = new ConsoleOutput();
//        $progress = new ProgressBar($output, $quantity);
//        $progress->start();
//
//        for ($i = 0; $i < $quantity; $i++) {
//            $address = $this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE);
//
//            $this->objectManager->persist($address);
//            $this->addresses[] = $address;
//
//            if ($i % 1000 == 0 and $i!=0) {
//                $this->objectManager->flush();
//            }
//
//            $progress->advance();
//        }
//
//        $this->objectManager->flush();
//
//
//        /** @var \DateTime $finishTime */
//        $finishTime = new DateTime('now');
//        $timeInterval = $this->importCompaniesService->getTimedifference($startTime, $finishTime);
//        $output->writeln('');
//        $output->writeln($finishTime->format("m/d/Y H:i:s").' Creating empty addresses was completed!');
//        $output->writeln('');
//        $output->writeln($timeInterval);
//
//        return $i;
//    }

    /**
     * @param array $accountContactPersons
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    private function updateACP(array $accountContactPersons, OutputInterface $output)
    {
        $startTime = new DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Updating AccountContactPerson entity field customBillingAddress was started');

        $output = new ConsoleOutput();
//        $progress = new ProgressBar($output, count($this->addresses));
        $progress = new ProgressBar($output, count($accountContactPersons));
        $progress->start();

//        $i = 0;
        $counter = 0;

        /** @var AccountContactPerson $contactPerson */
        foreach ($accountContactPersons as $contactPerson) {
//            $address = array_pop($this->addresses);

            /** @var Address $address */
            $address = $this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE);
            $this->objectManager->persist($address);

            /** @var AddressHistory $addressHistory */
            $addressHistory = AddressHistoryFactory::make($address);
            $this->objectManager->persist($addressHistory);

            $contactPerson->setCustomBillingAddress($address);
            $this->accountContactPersonHistoryCreator->create($contactPerson);

//            $i++;
            $counter++;
//            if ($i % 1000 == 0 and $i!=0) {
            if (($counter % 1000) == 0 and $counter != 0) {
                $this->objectManager->flush();
            }

            $progress->advance();
        }
        $this->objectManager->flush();

        /** @var \DateTime $finishTime */
        $finishTime = new DateTime('now');
        $timeInterval = $this->importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln('');
        $output->writeln($finishTime->format("m/d/Y H:i:s").' Updating AccountContactPerson entity field customBillingAddress was completed!');
        $output->writeln('');
        $output->writeln($timeInterval);

        return $i;
    }
}
