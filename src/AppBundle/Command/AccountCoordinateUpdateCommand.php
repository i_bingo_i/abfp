<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Coordinates\Helper;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AccountCoordinateUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('account:coordinate:update')
            ->setHelp('Retrieves all Workorders later, than today, gets Account from each of them and updates Coordinate of the Account according to its Address')
            ->setDescription('Retrieves all Workorders later, than today, gets Account from each of them and updates Coordinate of the Account according to its Address')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Updating of Coordinate, according to Account Address...');

        $container = $this->getContainer();

        /** @var ObjectManager $objectManager */
        $objectManager = $container->get('app.entity_manager');
        /** @var Helper $coordinateHelper */
        $coordinateHelper = $container->get('app.coordinates_helper.service');
        /** @var WorkorderRepository $workOrderRepository */
        $workOrderRepository = $objectManager->getRepository('AppBundle:Workorder');

        $startTime = microtime(true);

        $workorders = $workOrderRepository->getScheduledFromCurrentDate();

        $io->progressStart(count($workorders));

        /** @var Workorder $workorder */
        foreach ($workorders as $workorder) {
            /** @var Account $account */
            $account = $workorder->getAccount();
            $coordinateHelper->refreshByAccount($account);

            $io->progressAdvance();
        }
        $objectManager->flush();

        $io->progressFinish();

        $endTime = microtime(true);
        $executionTime = $endTime - $startTime;
        $io->success(
            'Updating of Coordinate, according to Account Address had been being executed for '.$executionTime.' sec.'
            .' and finished successfully!'
        );
    }
}
