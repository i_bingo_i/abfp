<?php

namespace AppBundle\Command;

use AppBundle\Services\TechnicianAssignmentSyncService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class MicrosoftOutlookCalendarEventPushCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('calendar:event:push')
            ->setDescription('This command would running to push Event to Microsoft Outlook Calendar')
            ->setHelp('If you have assigned technician to Workorder and would like to push this event to Microsoft Outlook Calendar, use this command')
            ->addOption(
                'access-token',
                'token',
                InputOption::VALUE_OPTIONAL,
                'Token got from Microsoft to access to Microsoft Outlook Calendar via REST API'

            )
            ->addOption(
                'date',
                'd',
                InputOption::VALUE_REQUIRED,
                'Date of Event(s) you would like to get'
            )
            ->addOption(
                'ms-outlook-email',
                'moe',
                InputOption::VALUE_OPTIONAL,
                'Microsoft Outlook E-mail of ContractorUser, which ID of calendar will be found by'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ContainerInterface $container */
        $container = $this->getContainer();
        /** @var TechnicianAssignmentSyncService $technicianAssignmentSyncService */
        $technicianAssignmentSyncService = $container->get('app.technician_assignment_sync.service');
        /** @var Session $session */
        $session = $container->get('session');

        //get access token
        if ($input->hasOption('access-token') && $input->getOption('access-token')) {
            $accessToken = $input->getOption('access-token');
            $session->set('fungio_outlook_calendar_access_token', $accessToken);
        } else {
            if ($session->has('fungio_outlook_calendar_access_token') && $session->get('fungio_outlook_calendar_access_token')) {
                $accessToken = $session->get('fungio_outlook_calendar_access_token');
            } else {
                throw new \Exception('Unable to get access token either from command line option, or from the session! The program has been terminated!');
            }
        }

        $date = \DateTime::createFromFormat('m/d/Y', $input->getOption('date'));
        if ($date && $date->format('m/d/Y') != $input->getOption('date')) {
            throw new \Exception('Date you have inputed has incorrect format! Program has been terminated!');
        }

        $calendarId = null;
        if ($input->hasOption('ms-outlook-email') && $input->getOption('ms-outlook-email')) {
            $msOutlookEmail = $input->getOption('ms-outlook-email');
            $calendarId = $technicianAssignmentSyncService->getMicrosoftCalendarIDByOwnerEmail($accessToken, $msOutlookEmail);
        }

        $technicianAssignmentSyncService->pushEventsToMSOutlookCalendar($accessToken, $date, $calendarId);

        $output->writeln('Events deleted from Microsoft Outlook Calendar have been successfully recovered!');
    }
}
