<?php

namespace AppBundle\Command;

use AppBundle\Entity\AccountContactPerson;
use DateTime;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Repository\CustomerRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AddCustomerToACPCommand extends ContainerAwareCommand
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var CustomerRepository */
    private $customerRepository;
    /** @var OrgLevelToCompanyService */
    private $importCompaniesService;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:add_customer_to_acpcommand')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->objectManager = $this->getContainer()->get('app.entity_manager');
        $this->customerRepository = $this->objectManager->getRepository('InvoiceBundle:Customer');
        $this->importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');

        $accountContactPersonRepository = $this->objectManager->getRepository('AppBundle:AccountContactPerson');
        $accountContactPersons = $accountContactPersonRepository->findBy(['customer' => null]);
        if (count($accountContactPersons)) {
            $this->creatingForAccountContactPerson($accountContactPersons, $output);
        }

        if (!count($accountContactPersons)) {
            $output->writeln('No processed elements');
        }
    }

    /**
     * @param array $accountContactPersons
     * @param OutputInterface $output
     * @throws \Exception
     */
    private function creatingForAccountContactPerson(array $accountContactPersons, OutputInterface $output)
    {
        $startTime = new DateTime('now');
        $output->writeln(
            $startTime->format("m/d/Y H:i:s").' Creating customers for account contact person was started'
        );

        $output = new ConsoleOutput();
        $progress = new ProgressBar($output, count($accountContactPersons));
        $progress->start();

        $i = 0;
        /** @var AccountContactPerson $acp */
        foreach ($accountContactPersons as $acp) {
            /** @var Customer $customer */
            $customer = $this->customerRepository->findByContactPerson($acp->getContactPerson());
            $acp->setCustomer($customer);
            $this->objectManager->persist($acp);

            if ($i % 1000 == 0 and $i!=0) {
                $this->objectManager->flush();
            }
            $i++;

            $progress->advance();
        }

        $this->objectManager->flush();

        /** @var DateTime $finishTime */
        $finishTime = new DateTime('now');
        $timeInterval = $this->importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln('');
        $output->writeln(
            $finishTime->format("m/d/Y H:i:s").' Creating customers for account contact person was started completed!'
        );
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('');

        $progress->finish();
    }
}
