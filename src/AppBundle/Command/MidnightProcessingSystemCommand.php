<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\ProposalsProcessing;
use Throwable;

class MidnightProcessingSystemCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('proposals:need:call')
            ->setDescription("It command would running with changing some application data depending on conditions")
            ->setHelp("It command would running with changing some application data depending on conditions");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $processingStatsManager = $this->getContainer()->get('app.processing_stats.manager');

        try {
            /** @var ProposalsProcessing $proposalProcessingService */
            $proposalProcessingService = $this->getContainer()->get('app.proposals_processing.service');
            $opportunityProcessingService = $this->getContainer()->get('app.opportunity_processing.service');

            $processingStatsManager->createMessageByAlias("Start Processing");

            $proposalProcessingService->checkRetestNoticesForCallNeeding();
            $proposalProcessingService->checkProposalsForCallNeeding();
            $proposalProcessingService->sendEmailReminder();
            $proposalProcessingService->pastDue();
            $proposalProcessingService->sendEmailPastDue();
            $proposalProcessingService->noResponse();

            $opportunityProcessingService->pastDue();

            //$serviceProcessingService->retestOpportunity();

            $processingStatsManager->createMessageByAlias("Finish Processing");
        } catch (Throwable $e) {
            $processingStatsManager->createMessageByAlias(
                "Error during proposals processing. Message: ".$e->getMessage(),
                "error"
            );
        }
    }
}
