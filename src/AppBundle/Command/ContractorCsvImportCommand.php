<?php

namespace AppBundle\Command;

use AppBundle\Services\ContractorCsvImportService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;

class ContractorCsvImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('contractor:csv:import')
//            ->addArgument('filename', InputArgument::REQUIRED, 'The file name.')
            ->setHelp('Imports data from CSV file to database tables of the Project')
            ->setDescription('Imports data from CSV file to database tables of the Project')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Data importing from CSV-file...');

        /** @var ContractorCsvImportService $contractorCsvImportService */
        $contractorCsvImportService = $this->getContainer()->get('app.contractor.csv.import.service');

        $startTime = microtime(true);

//        $contractorCsvImportService->parsingData($input->getArgument('filename'));
        $contractorCsvImportService->parsingData("contractors_for_add.csv");
        $contractorCsvImportService->importNewContractors();
//        $contractorCsvImportService->parsingData("contractors_for_add.csv");
//        $contractorCsvImportService->importUpdateContractors();
        $endTime = microtime(true);
        $executionTime = $endTime - $startTime;

        $io->success(
            'Contractor import from CSV file had been being executed for '.$executionTime.' sec. and finished successfully!'
        );
    }
}
