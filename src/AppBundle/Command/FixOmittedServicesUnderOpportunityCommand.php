<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Message;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

class FixOmittedServicesUnderOpportunityCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:fix_omitted_services')
            ->setDescription('Fix omitted services under opportunity');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        /** @var ObjectManager $objectManager */
        $objectManager = $container->get('app.entity_manager');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $objectManager->getRepository("AppBundle:Service");
        /** @var ServiceManager $serviceManager */
        $serviceManager = $container->get("app.service.manager");

        $servicesForProcessing = $serviceRepository->getOmittedServiceUnderOpportunity();

        $output = new ConsoleOutput();
        $progress = new ProgressBar($output, count($servicesForProcessing));
        $progress->start();

        /** @var Service $service */
        foreach ($servicesForProcessing as $service) {

            $serviceManager->resetOpportunity($service);

            $progress->advance();
        }
        $output->writeln('');
        $objectManager->flush();
        $progress->finish();
    }
}
