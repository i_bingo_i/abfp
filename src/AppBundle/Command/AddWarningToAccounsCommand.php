<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\Message;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\MessageRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddWarningToAccounsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:add_warning_to_accouns')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        /** @var ObjectManager $objectManager */
        $objectManager = $container->get('app.entity_manager');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');
        /** @var MessageRepository $messegRepository */
        $messegRepository = $objectManager->getRepository('AppBundle:Message');
        /** @var Message $messegNoAutorizer */
        $messegNoAutorizer = $messegRepository->findOneBy(['alias' => 'no_autorizer']);
        /** @var AccountContactPersonManager $accountContactPersonManager */
        $accountContactPersonManager = $container->get('app.account_contact_person.manager');
        /** @var AccountManager $accountManager */
        $accountManager = $container->get('app.account.manager');
        /** @var DeviceCategory $division */
        $division = $objectManager->getRepository('AppBundle:DeviceCategory')->findOneBy(['alias' => 'fire']);
        /** @var array $accounts */
        $accounts = $accountRepository->getAccountsWithPreEngineeredFireSuppressionSystemDevices();

        $output = new ConsoleOutput();
        $progress = new ProgressBar($output, count($accounts));
        $progress->start();

        $addMessageOne = function(Account $account) use ($messegNoAutorizer, $objectManager) {
            $account->addMessage($messegNoAutorizer);

            $objectManager->persist($account);
        };

        /** @var Account $account */
        foreach ($accounts as $account) {
            if ($account->getParent()) {
                if (!$accountContactPersonManager->isAuthorize($account->getParent(), $division)) {
                    $addMessageOne($account->getParent());
                }

            } else {
                if (!$accountContactPersonManager->isAuthorize($account, $division)) {
                    $addMessageOne($account);
                }
            }

            $accountManager->checkServiceFee($account);
            $accountManager->checkMessageAuthorizerAddress($account);
            $accountManager->checkDefaultDepartmentChanel($account);

            $progress->advance();
        }

        $objectManager->flush();
        $progress->finish();
    }
}
