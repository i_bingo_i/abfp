<?php

namespace AppBundle\Validators;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserValidator
{
    /** @var ValidatorInterface */
    private $validator;
    /** @var ObjectManager */
    private $objectManager;
    /** @var UserRepository */
    private $userRepository;

    /**
     * AddressValidator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator, ObjectManager $objectManager)
    {
        $this->validator = $validator;
        $this->objectManager = $objectManager;

        $this->userRepository = $this->objectManager->getRepository('AppBundle:User');
    }

    /**
     * @param AccountContactPerson $acp
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function update(User $user)
    {
        return $this->validator->validate(
            $user,
            null,
            ['update']
        );
    }

    /**
     * @param User $user
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function create(User $user)
    {
        return $this->validator->validate(
            $user,
            null,
            ['create']
        );
    }

    /**
     * @param User $user
     * @return bool|string
     */
    public function checkNotUniqueEmail(User $user)
    {
        $users = $this->userRepository->findUniqueByEmail($user);

        if ($users) {
            $message = "Value " . $user->getEmail() . " not unique for field email";

            return $message;
        }

        return false;
    }
}
