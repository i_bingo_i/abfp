<?php

namespace AppBundle\Events;

use AppBundle\Entity\ContactPerson;
use Symfony\Component\EventDispatcher\Event;

class ContactPersonCreatedEvent extends Event
{
    const NAME = 'contact_person.created';

    /** @var  ContactPerson */
    protected $contactPerson;

    /**
     * ContactPersonCreatedEvent constructor.
     * @param ContactPerson $contactPerson
     */
    public function __construct(ContactPerson $contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return ContactPerson
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }
}
