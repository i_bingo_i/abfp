<?php

namespace AppBundle\Events;

use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use Symfony\Component\EventDispatcher\Event;

class ContactPersonSetCompanyEvent extends Event
{
    const NAME = 'contact_person.set_company';

    /** @var  ContactPerson */
    protected $contactPerson;
    /** @var Company */
    protected $company;

    /**
     * ContactPersonSetCompanyEvent constructor.
     * @param ContactPerson $contactPerson
     * @param Company $company
     */
    public function __construct(ContactPerson $contactPerson, Company $company)
    {
        $this->contactPerson = $contactPerson;
        $this->company = $company;
    }

    /**
     * @return ContactPerson
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
