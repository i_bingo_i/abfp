<?php

namespace AppBundle\Events;

use AppBundle\Entity\Proposal;
use Symfony\Component\EventDispatcher\Event;

class ProposalCloneEvent extends Event
{
    const NAME = 'proposal.clone';

    /** @var Proposal */
    protected $proposal;
    /** @var Proposal */
    protected $clonedProposal;

    protected $comment;

    /**
     * ProposalCloneEvent constructor.
     * @param Proposal $proposal
     * @param Proposal $clonedProposal
     * @param null $comment
     */
    public function __construct(Proposal $proposal, Proposal $clonedProposal, $comment = null)
    {
        $this->proposal = $proposal;
        $this->clonedProposal = $clonedProposal;
        $this->comment = $comment;
    }

    /**
     * @return Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * @return Proposal
     */
    public function getClonedProposal()
    {
        return $this->clonedProposal;
    }

    /**
     * @return null
     */
    public function getUserComment()
    {
        return $this->comment;
    }
}
