<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Municipality;

class MunicipalityCreatedEvent extends Event
{
    const NAME = 'municipality.created';

    /** @var Municipality */
    protected $municipality;

    /**
     * MunicipalityCreatedEvent constructor.
     * @param Municipality $municipality
     */
    public function __construct(Municipality $municipality)
    {
        $this->municipality = $municipality;
    }

    /**
     * @return Municipality
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }
}
