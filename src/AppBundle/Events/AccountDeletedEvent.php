<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Account;

class AccountDeletedEvent extends Event
{
    const NAME = 'account.deleted';

    /** @var Account */
    protected $account;

    /**
     * AccountCreatedEvent constructor.
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }
}