<?php

namespace AppBundle\Events;

use AppBundle\Entity\ContractorUser;
use Symfony\Component\EventDispatcher\Event;

class ContractorUserDeleteEvent extends Event
{
    const NAME = 'contractor_user.deleted';
    /** @var ContractorUser  */
    protected $contractorUser;

    /**
     * ContractorUserCreatedEvent constructor.
     * @param ContractorUser $contractorUser
     */
    public function __construct(ContractorUser $contractorUser)
    {
        $this->contractorUser = $contractorUser;
    }

    /**
     * @return ContractorUser
     */
    public function getContractorUser()
    {
        return $this->contractorUser;
    }
}
