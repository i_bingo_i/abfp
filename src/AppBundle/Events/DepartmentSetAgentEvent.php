<?php

namespace AppBundle\Events;

use AppBundle\Entity\Agent;
use AppBundle\Entity\Department;
use Symfony\Component\EventDispatcher\Event;

class DepartmentSetAgentEvent extends Event
{
    const NAME = 'department.set_agent';

    /** @var Agent */
    protected $agent;
    /** @var Agent */
    protected $department;

    /**
     * DepartmentSetAgentEvent constructor.
     * @param Agent $agent
     * @param Department $department
     */
    public function __construct(Agent $agent, Department $department)
    {
        $this->agent = $agent;
        $this->department = $department;
    }

    /**
     * @return Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @return Agent
     */
    public function getDepartment()
    {
        return $this->department;
    }
}
