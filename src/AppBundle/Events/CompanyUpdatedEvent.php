<?php

namespace AppBundle\Events;

use AppBundle\Entity\Company;
use Symfony\Component\EventDispatcher\Event;

class CompanyUpdatedEvent extends Event
{
    const NAME = 'company.updated';

    /** @var Company */
    protected $company;

    /**
     * CompanyUpdatedEvent constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
