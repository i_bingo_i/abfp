<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Department;

class DepartmentCreatedEvent extends Event
{
    const NAME = 'department.created';

    /** @var Department */
    protected $department;

    /**
     * DepartmentCreatedEvent constructor.
     * @param Department $department
     */
    public function __construct(Department $department)
    {
        $this->department = $department;
    }

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }
}
