<?php

namespace AppBundle\Events;

use AppBundle\Entity\Company;
use Symfony\Component\EventDispatcher\Event;

class CompanyDeletedEvent extends Event
{
    const NAME = 'company.deleted';

    /** @var Company */
    protected $company;

    /**
     * CompanyDeletedEvent constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
