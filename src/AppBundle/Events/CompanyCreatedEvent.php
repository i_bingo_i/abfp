<?php

namespace AppBundle\Events;

use AppBundle\Entity\Company;
use Symfony\Component\EventDispatcher\Event;

class CompanyCreatedEvent extends Event
{
    const NAME = 'company.created';

    /** @var Company */
    protected $company;

    /**
     * CompanyCreatedEvent constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
