<?php

namespace AppBundle\Events;

use AppBundle\Entity\Company;
use Symfony\Component\EventDispatcher\Event;

class CompanyUnlinkedEvent extends Event
{
    const NAME = 'company.unlinked';

    /** @var Company */
    protected $company;

    /**
     * CompanyUnlinkedEvent constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
