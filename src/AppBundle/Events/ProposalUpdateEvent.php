<?php

namespace AppBundle\Events;

use AppBundle\Entity\Proposal;
use Symfony\Component\EventDispatcher\Event;

class ProposalUpdateEvent extends Event
{
    const NAME = 'proposal.update';

    protected $proposal;

    /**
     * ProposalUpdateEvent constructor.
     * @param Proposal $proposal
     */
    public function __construct(Proposal $proposal)
    {
        $this->proposal = $proposal;
    }

    /**
     * @return Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }
}