<?php

namespace AppBundle\Events;

use AppBundle\Entity\Company;
use Symfony\Component\EventDispatcher\Event;

class CompanyLinkedEvent extends Event
{
    const NAME = 'company.linked';

    /** @var Company */
    protected $company;

    /**
     * CompanyLinkedEvent constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
