<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\AddressHistory;

class AddressHistoryItemCreateEvent extends Event
{
    const NAME = 'address.history.item.create';

    /** @var AddressHistory */
    protected $addressHistory;

    /**
     * AddressHistoryItemCreatedEvent constructor.
     * @param AddressHistory $addressHistory
     */
    public function __construct(AddressHistory $addressHistory)
    {
        $this->addressHistory = $addressHistory;
    }

    /**
     * @return AddressHistory
     */
    public function getAddressHistory()
    {
        return $this->addressHistory;
    }
}