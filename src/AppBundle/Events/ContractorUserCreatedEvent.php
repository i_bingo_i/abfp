<?php

namespace AppBundle\Events;

use AppBundle\Entity\ContractorUser;
use Symfony\Component\EventDispatcher\Event;

class ContractorUserCreatedEvent extends Event
{
    const NAME = 'contractor_user.created';
    /** @var ContractorUser  */
    protected $contractorUser;

    /**
     * ContractorUserCreatedEvent constructor.
     * @param ContractorUser $contractorUser
     */
    public function __construct(ContractorUser $contractorUser)
    {
        $this->contractorUser = $contractorUser;
    }

    /**
     * @return ContractorUser
     */
    public function getContractorUser()
    {
        return $this->contractorUser;
    }
}
