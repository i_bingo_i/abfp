<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Service;

class ServiceDeletedEvent extends Event
{
    const NAME = 'service.deleted';

    /** @var Service */
    private $service;

    /**
     * ServiceDeletedEvent constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }
}
