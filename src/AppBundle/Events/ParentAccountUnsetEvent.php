<?php

namespace AppBundle\Events;

use AppBundle\Entity\Account;
use Symfony\Component\EventDispatcher\Event;

class ParentAccountUnsetEvent extends Event
{
    const NAME = 'account.unset_parent';

    /** @var Account */
    protected $account;
    /** @var Account */
    protected $parent;

    /**
     * ParentAccountUnsetEvent constructor.
     * @param Account $account
     * @param Account $parent
     */
    public function __construct(Account $account, Account $parent)
    {
        $this->account = $account;
        $this->parent = $parent;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return Account
     */
    public function getParent()
    {
        return $this->parent;
    }
}
