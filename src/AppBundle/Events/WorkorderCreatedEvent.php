<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Workorder;

class WorkorderCreatedEvent extends Event
{
    const NAME = 'workorder.created';

    /** @var Workorder */
    protected $workorder;

    /**
     * WorkorderCreatedEvent constructor.
     * @param Workorder $workorder
     */
    public function __construct(Workorder $workorder)
    {
        $this->workorder = $workorder;

    }

    /**
     * @return Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }
}
