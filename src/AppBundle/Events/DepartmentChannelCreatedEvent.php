<?php

namespace AppBundle\Events;

use AppBundle\Entity\DepartmentChannel;
use Symfony\Component\EventDispatcher\Event;

class DepartmentChannelCreatedEvent extends Event
{
    const NAME = 'department_channel.created';

    /** @var DepartmentChannel */
    protected $departmentChannel;

    /**
     * DepartmentChannelCreatedEvent constructor.
     * @param DepartmentChannel $departmentChannel
     */
    public function __construct(DepartmentChannel $departmentChannel)
    {
        $this->departmentChannel = $departmentChannel;
    }

    /**
     * @return DepartmentChannel
     */
    public function getDepartmentChannel()
    {
        return $this->departmentChannel;
    }
}
