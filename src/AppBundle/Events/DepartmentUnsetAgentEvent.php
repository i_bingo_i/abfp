<?php

namespace AppBundle\Events;

use AppBundle\Entity\Agent;
use AppBundle\Entity\Department;
use Symfony\Component\EventDispatcher\Event;

class DepartmentUnsetAgentEvent extends Event
{
    const NAME = 'department.unset_agent';

    /** @var Agent */
    protected $agent;
    /** @var Department */
    protected $department;

    /**
     * DepartmentUnsetAgentEvent constructor.
     * @param Agent $agent
     * @param Department $department
     */
    public function __construct(Agent $agent, Department $department)
    {
        $this->agent = $agent;
        $this->department = $department;
    }

    /**
     * @return Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }
}
