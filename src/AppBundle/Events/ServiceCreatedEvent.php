<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Service;

class ServiceCreatedEvent extends Event
{
    const NAME = 'service.created';

    /** @var Service */
    protected $service;

    /**
     * ServiceCreatedEvent constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }
}
