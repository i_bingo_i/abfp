<?php

namespace AppBundle\Events;

use AppBundle\Entity\Opportunity;
use Symfony\Component\EventDispatcher\Event;

class OpportunityUpdateEvent extends Event
{
    const NAME = 'opportunity.update';

    /** @var Opportunity  */
    private $opportunity;

    /**
     * OpportunityUpdateEvent constructor.
     * @param Opportunity $opportunity
     */
    public function __construct(Opportunity $opportunity)
    {
        $this->opportunity = $opportunity;
    }

    /**
     * @return Opportunity
     */
    public function getOpportunity(): Opportunity
    {
        return $this->opportunity;
    }
}
