<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Service;

class ServiceUpdatedEvent extends Event
{
    const NAME = 'service.updated';

    /** @var  Service */
    protected $service;

    /**
     * ServiceUpdatedEvent constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }
}
