<?php

namespace AppBundle\Events;

use AppBundle\Entity\Account;
use Symfony\Component\EventDispatcher\Event;

class ParentAccountSetEvent extends Event
{
    const NAME = 'account.set_parent';

    /** @var Account */
    protected $account;
    /** @var Account */
    protected $parent;

    /**
     * ParentAccountSetEvent constructor.
     * @param Account $account
     * @param Account $parent
     */
    public function __construct(Account $account, Account $parent)
    {
        $this->account = $account;
        $this->parent = $parent;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return Account
     */
    public function getParent()
    {
        return $this->parent;
    }
}
