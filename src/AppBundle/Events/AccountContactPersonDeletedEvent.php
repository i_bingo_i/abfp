<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\AccountContactPerson;

class AccountContactPersonDeletedEvent extends Event
{
    const NAME = 'contact.deleted';

    /** @var AccountContactPerson */
    protected $contact;

    /**
     * AccountContactPersonDeletedEvent constructor.
     * @param AccountContactPerson $contact
     */
    public function __construct(AccountContactPerson $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return AccountContactPerson
     */
    public function getContact()
    {
        return $this->contact;
    }
}
