<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Device;

class DeviceCreatedEvent extends Event
{
    const NAME = 'device.created';

    /** @var Device */
    protected $device;

    /**
     * DeviceCreatedEvent constructor.
     * @param Device $device
     */
    public function __construct(Device $device)
    {
        $this->device = $device;
    }

    /**
     * @return Device
     */
    public function getDevice()
    {
        return $this->device;
    }
}
