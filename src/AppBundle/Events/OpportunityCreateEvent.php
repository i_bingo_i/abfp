<?php

namespace AppBundle\Events;

use AppBundle\Entity\Opportunity;
use Symfony\Component\EventDispatcher\Event;

class OpportunityCreateEvent extends Event
{
    const NAME = 'opportunity.create';

    /** @var Opportunity */
    private $opportunity;

    /**
     * OpportunityCreateEvent constructor.
     * @param Opportunity $opportunity
     */
    public function __construct(Opportunity $opportunity)
    {
        $this->opportunity = $opportunity;
    }

    /**
     * @return Opportunity
     */
    public function getOpportunity(): Opportunity
    {
        return $this->opportunity;
    }
}
