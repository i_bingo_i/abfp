<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\AccountContactPerson;

class AccountContactPersonSetRolesEvent extends Event
{
    const NAME = 'contact.set_roles';

    /** @var AccountContactPerson */
    protected $contact;

    /**
     * AccountContactPersonSetRolesEvent constructor.
     * @param AccountContactPerson $contact
     */
    public function __construct(AccountContactPerson $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return AccountContactPerson
     */
    public function getContact()
    {
        return $this->contact;
    }
}
