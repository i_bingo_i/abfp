<?php

namespace AppBundle\Events;

use AppBundle\Entity\ContactPerson;
use Symfony\Component\EventDispatcher\Event;

class ContactPersonDeletedEvent extends Event
{
    const NAME = 'contact_person.deleted';

    /** @var  ContactPerson */
    protected $contactPerson;

    /**
     * ContactPersonSetCompanyEvent constructor.
     * @param ContactPerson $contactPerson
     */
    public function __construct(ContactPerson $contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return ContactPerson
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }
}
