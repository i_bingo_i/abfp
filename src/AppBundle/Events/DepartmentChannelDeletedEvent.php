<?php

namespace AppBundle\Events;

use AppBundle\Entity\DepartmentChannel;
use Symfony\Component\EventDispatcher\Event;

class DepartmentChannelDeletedEvent extends Event
{
    const NAME = 'department_channel.deleted';

    /** @var DepartmentChannel */
    protected $departmentChannel;

    /**
     * DepartmentChannelDeletedEvent constructor.
     * @param DepartmentChannel $departmentChannel
     */
    public function __construct(DepartmentChannel $departmentChannel)
    {
        $this->departmentChannel = $departmentChannel;
    }

    /**
     * @return DepartmentChannel
     */
    public function getDepartmentChannel()
    {
        return $this->departmentChannel;
    }
}
