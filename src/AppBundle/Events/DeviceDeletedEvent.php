<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Device;

class DeviceDeletedEvent extends Event
{
    const NAME = 'device.deleted';

    /** @var Device */
    protected $device;

    /**
     * DeviceDeletedEvent constructor.
     * @param Device $device
     */
    public function __construct(Device $device)
    {
        $this->device = $device;
    }

    /**
     * @return Device
     */
    public function getDevice()
    {
        return $this->device;
    }
}
