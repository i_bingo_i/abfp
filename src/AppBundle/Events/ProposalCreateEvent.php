<?php

namespace AppBundle\Events;

use AppBundle\Entity\Proposal;
use Symfony\Component\EventDispatcher\Event;

class ProposalCreateEvent extends Event
{
    const NAME = 'proposal.create';

    protected $proposal;

    /**
     * ProposalCreateEvent constructor.
     * @param Proposal $proposal
     */
    public function __construct(Proposal $proposal)
    {
        $this->proposal = $proposal;
    }

    /**
     * @return Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }
}