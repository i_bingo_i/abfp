<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Account;

class AccountUpdatedEvent extends Event
{
    const NAME = 'account.update';

    /** @var Account */
    protected $account;

    /**
     * AccountCreatedEvent constructor.
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }
}
