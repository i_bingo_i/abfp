<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Municipality;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Services\Search;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;

class MunicipalityRestController extends FOSRestController
{
    /**
     * Search municipality by Name
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "name" : (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Municipality",
     *   tags={
     *      "not tested method" = "#dc9000"
     *   },
     *   resource = true,
     *   description = "Search municipality by Name",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="phrase", nullable=false, strict=true, description="Name for search")
     *
     * @Rest\Post("/municipality/search-by-name")
     * @View(serializerGroups={"short"})
     * @return array|mixed
     */
    public function postSearchMunicipalityAction(ParamFetcher $paramFetcher)
    {
        /** @var Search $searchService */
        $searchService = $this->get('app.search');
        $phrase = $paramFetcher->get('phrase');

        /** @var Municipality[] $searchResult */
        $searchResult = $searchService->searchMunicipalityForAccount($phrase);

        return $this->view($searchResult, Response::HTTP_OK);
    }
}
