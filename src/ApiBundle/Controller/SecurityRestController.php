<?php

namespace ApiBundle\Controller;

use ApiBundle\Services\TechnicianRestService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use AppBundle\Services\UserAccessToken;

use AppBundle\Entity\User;
use AppBundle\Entity\Repository\UserRepository;

class SecurityRestController extends FOSRestController
{
    /**
     * Sing In
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "email": (string)"",
     *          "firstName": (string)"",
     *          "lastName": (string)"",
     *          "accessToken": (string)"",
     *     }
     *
     * ### Response FAIL user not found ###
     *      {
     *          "code": 404,
     *          "message": "User not found. Authentication Failed.",
     *      }
     *
     * ### Response FAIL wrong password ###
     *      {
     *          "code": 477,
     *          "message": "Invalid password. Authentication Failed.",
     *      }
     *
     * @ApiDoc(
     *   section = "User Security",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Sing In",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "User not found. Authentication Failed.",
     *     477 = "Invalid password. Authentication Failed."
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="email", nullable=false, strict=true, description="User email")
     * @RequestParam(name="password", nullable=false,  strict=true, description="User password")
     *
     * @Rest\Post("/login")
     * @return View
     */
    public function postLoginAction(ParamFetcher $paramFetcher)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var Serializer $serializer */
        $serializer = $this->get('jms_serializer');
        /** @var UserRepository $userRepository */
        $userRepository = $objectManager->getRepository('AppBundle:User');
        /** @var TechnicianRestService $technicianRestService */
        $technicianRestService = $this->get('api.technician.rest.service');

        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => $paramFetcher->get('email'), 'deleted' => 0]);
        if (!$user) {
            throw new HttpException(Response::HTTP_NOT_FOUND, "User not found. Authentication Failed.");
        }

        $encoderService = $this->get('security.encoder_factory');
        $encoder = $encoderService->getEncoder($user);

        if ($encoder->isPasswordValid($user->getPassword(), $paramFetcher->get('password'), $user->getSalt())) {

            $response = $technicianRestService->prepareResponse($user);

            $response = $serializer->toArray(
                $response,
                SerializationContext::create()->setGroups(array('login'))->enableMaxDepthChecks()
            );

            return new View(
                $response,
                Response::HTTP_OK
            );
        }

        throw new HttpException(477, "Invalid password. Authentication Failed.");
    }

    /**
     * Check user's accessToken
     *
     * ### Response OK ###
     *
     *     {
     *          "tokenIsValid": 1
     *     }
     *
     * ### Response FAIL 401 ###
     *
     *     API Key "<access token>" does not exist.
     *
     * @ApiDoc(
     *   section = "User Security",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Check user's accessToken",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "Unauthorized"
     *   }
     * )
     *
     * @Rest\Get("/check-access-token")
     *
     * @return array
     */
    public function getCheckAccessTokenAction()
    {
        return [
            "tokenIsValid" => 1
        ];
    }
}
