<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Repository\ContractorServiceRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ServiceContractorRestController extends FOSRestController
{
    /**
     * Get ServiceContractor
     *
     * ### Response OK ###
     *     {
     *          'fixedPrice' => (integer)
     *     }
     * ### Response FAIL Validation error ###
     *      {
     *          "code": 400,
     *          "message": "Parameters service and contractor required.",
     *      }
     *
     * @ApiDoc(
     *   section = "ServiceContractor",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Get ServiceContractor",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters={
     *      {"name"="service", "dataType"="integer", "required"=true, "description"="service id"},
     *      {"name"="contractor", "dataType"="integer", "required"=true, "description"="contractor id"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameters service and contractor required."
     *   }
     * )
     *
     * @Rest\Get("/contractor-service/get-price")
     * @param Request $request
     * @return View
     */
    public function getPriceAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ContractorServiceRepository $contractorServiceRepository */
        $contractorServiceRepository = $objectManager->getRepository('AppBundle:ContractorService');

        $serviceId = $request->get('service');
        $contractorId = $request->get('contractor');

        if (!$serviceId || !$contractorId) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                "Parameters service and contractor required."
            );
        }

        $serviceContractor = $contractorServiceRepository->findOneBy([
            'named' => $serviceId,
            'contractor' => $contractorId
        ]);

        return new View([
            'fixedPrice' => $serviceContractor->getFixedPrice()
        ], Response::HTTP_OK);
    }
}
