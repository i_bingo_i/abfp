<?php

namespace ApiBundle\Controller;

use ApiBundle\Services\ServiceRestService;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use AppBundle\Services\Search;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Persistence\ObjectManager;
use ServiceBundle\Services\ServiceCreatorService;
use ServiceBundle\Services\ServiceUpdater;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class ServiceRestController extends FOSRestController
{
    /**
     * Create service
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "updatedAt": (int) 1524573209
     *     }
     *
     * ### Request example ###
     *      {
     *          "deviceID": "1",
     *          "serviceNameID": "1",
     *          "fixedPrice": "67",
     *          "inspectionDue": 1524573209,
     *          "comment": "this is my comment"
     *      }
     *
     * @ApiDoc(
     *   section = "Service",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Create inspection service",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="deviceID", "dataType"="string", "required"=true, "description"="Device id"},
     *       {"name"="serviceNameID", "dataType"="string", "required"=true, "description"="Service name id"},
     *       {"name"="inspectionDue", "dataType"="int", "required"=true, "description"="Inspection Due date"},
     *       {"name"="lastTested", "dataType"="int", "required"=false, "description"="Last Tested date"},
     *       {"name"="lastTester", "dataType"="string", "required"=false, "description"="Company last Tested id"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found",
     *     500 = "Something was wrong"
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Post("/service/inspection")
     * @return View
     */
    public function postServiceCreateAction(Request $request)
    {
        /** @var ServiceCreatorService $serviceCreatorService */
        $serviceCreatorService = $this->get("service.creator.service");
        $allData = $request->request->all();
        $route = $request->getRequestUri();
        $allData["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");

        $result = $serviceCreatorService->createInspectionByParams($allData, $route);

        if (!$result instanceof Service) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Service can not be added'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'updatedAt' => $result->getDateUpdate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }

    
    /**
     * Update service
     *
     * ### Response OK ###
     *     {
     *          "id": (string) "12"
     *          "updatedAt": (int) 15668543
     *     }
     *
     * @ApiDoc(
     *   section = "Service",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Update inspection service",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="serviceID", "dataType"="string", "required"=true, "description"="Service id"},
     *       {"name"="deviceID", "dataType"="string", "required"=false, "description"="Device id"},
     *       {"name"="inspectionDue", "dataType"="int", "required"=false, "description"="Inspection Due date"},
     *       {"name"="lastTested", "dataType"="int", "required"=false, "description"="Last Tested date"},
     *       {"name"="lastTester", "dataType"="string", "required"=false, "description"="Last Tester id"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found"
     *   }
     * )
     *
     *
     * @param Request $request
     *
     * @Rest\Put("/service/inspection")
     * @return View
     */
    public function putServiceUpdateAction(Request $request)
    {
        /** @var ServiceUpdater $serviceUpdater */
        $serviceUpdater = $this->get("service.updater.service");

        $allData = $request->request->all();
        $route = $request->getRequestUri();
        $allData["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");

        $result = $serviceUpdater->updateByParams($allData, $route);

        if (!$result instanceof Service) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Service can not be edited'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'updatedAt' => $result->getDateUpdate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Update service fee
     *
     * ### Response OK ###
     *     {
     *          "result": (bool) true
     *     }
     *
     * ### Response FAIL service not found ###
     *      {
     *          "code": 404,
     *          "message": "Service not found.",
     *      }
     *
     * @ApiDoc(
     *   section = "Service",
     *   tags={
     *      "not tested method" = "#dc9000"
     *   },
     *   resource = true,
     *   description = "Update service fee",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "Unauthorized",
     *     404 = "Service not found"
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="id", nullable=false, requirements="[0-9]+", strict=true, description="Service id")
     * @RequestParam(name="value", nullable=false, requirements="[0-9]*[.,]?[0-9]+", strict=true, description="Service Fee")
     *
     * @Rest\Put("/service/fee")
     * @return View
     */
    public function putServiceFeeUpdateAction(ParamFetcher $paramFetcher)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $objectManager->getRepository('AppBundle:Service');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');

        $data = $paramFetcher->all();
        /** @var Service $service */
        $service = $serviceRepository->findOneBy(['id' => $data['id']]);
        if (!$service) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Service not found');
        }

        try {
            $service->setFixedPrice($data['value']);
            $serviceManager->update($service);
        } catch (Throwable $e) {
            return new View(
                [
                    'cod' => $e->getCode(),
                    'message' => $e->getMessage()
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return new View(
            [
                'result' => true
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Search Non frequency Service
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "name" : (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Service",
     *   tags={
     *      "not tested method" = "#dc9000"
     *   },
     *   resource = true,
     *   description = "Search Non frequency Service",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="deviceAlias", nullable=false, strict=true, description="Device alias")
     * @RequestParam(name="phrase", nullable=false, strict=true, description="Non frequency Service for search")
     *
     * @Rest\Post("/service/non-frequency")
     * @Rest\View(serializerGroups={"all"})
     * @return array|mixed
     */
    public function postNonFrequencySearchByNameAction(ParamFetcher $paramFetcher)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ServiceNamedRepository $serviceNameRepository */
        $serviceNameRepository = $objectManager->getRepository('AppBundle:ServiceNamed');
        /** @var Search $searchService */
        $searchService = $this->get('app.search');

        $params = [];
        $params['phrase'] = $paramFetcher->get('phrase');
        $params['deviceAlias'] = $paramFetcher->get('deviceAlias');

        /** @var Service[] $searchResult */
        $searchResult = $searchService->searchServiceNameNooFrequency($params);

        return $this->view($searchResult, Response::HTTP_OK);
    }

}
