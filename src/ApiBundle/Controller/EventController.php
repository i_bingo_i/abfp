<?php

namespace ApiBundle\Controller;

use ApiBundle\Services\TechnicianRestService;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Workorder;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations\FileParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations\View as Ser;

class EventController extends FOSRestController
{
    /**
     * Event Start
     *
     * ### Response OK ###
     *     {}
     *
     *
     * @ApiDoc(
     *   section = "Events",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Event start",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="eventID", nullable=false, strict=true, description="Event ID")
     * @RequestParam(name="time", nullable=false,  strict=true, description="Technician's time of start")
     * @RequestParam(name="lat", nullable=true,  strict=true, description="Technician's location at moment of starting")
     * @RequestParam(name="lon", nullable=true,  strict=true, description="Technician's location at moment of starting")
     *
     * @Rest\Post("/event/start")
     * @return View
     */
    public function postEventStartAction(ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        return new View([],Response::HTTP_OK);
    }

    /**
     * Event Leave
     *
     * ### Response OK ###
     *     {}
     *
     *
     * @ApiDoc(
     *   section = "Events",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Event leave",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="eventID", nullable=false, strict=true, description="Event ID")
     * @RequestParam(name="time", nullable=false,  strict=true, description="Technician's time of start")
     * @RequestParam(name="lat", nullable=true,  strict=true, description="Technician's location at moment of starting")
     * @RequestParam(name="lon", nullable=true,  strict=true, description="Technician's location at moment of starting")
     *
     * @Rest\Post("/event/leave")
     * @return View
     */
    public function postEventLeaveAction(ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        return new View([],Response::HTTP_OK);
    }

    /**
     * Create event
     *
     * ### Response OK ###
     *     {
     *          "pathImage": (string)"",
     *          "timestamp": (string)""
     *     }
     *
     * ### 400 Bad Request ###
     *     {
     *          "code": 400,
     *          "message": "Parameter 'orderId' of value 'Some no valid value' violated a constraint 'Parameter 'orderId' value, does not match requirements '[0-9]+''"
     *     }
     * ### 404 Not found ###
     *     {
     *          "code" : 404,
     *          "message" : "Workorder by id did not found"
     *     }
     *
     * @ApiDoc(
     *   section = "Events",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "A",
     *   headers = {
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Validation errors",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Object not found"
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="orderId", nullable=false, allowBlank=false, requirements="[0-9]+", strict=true, description="Workoeder ID")
     * @RequestParam(name="user", nullable=false, allowBlank=false, requirements="[0-9]+", strict=true, description="ContractorUser ID")
     * @RequestParam(name="startDate", nullable=false, allowBlank=false, strict=true, description="Start schedule date")
     * @RequestParam(name="endDate", nullable=false, allowBlank=false, strict=true, description="End schedule date")
     *
     * @Rest\Post("/event/create")
     * @Ser(serializerGroups={"full"})
     * @return View
     * @throws \Exception
     */
    public function postEventAssignAction(ParamFetcher $paramFetcher)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->get('app.entity_manager');
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $entityManager->getRepository('AppBundle:Workorder');
        /** @var ContractorUserRepository $contractorUserRepository */
        $contractorUserRepository = $entityManager->getRepository('AppBundle:ContractorUser');
        /** @var TechnicianRestService $technicianRestService */
        $technicianRestService = $this->get('api.technician.rest.service');
        /** @var array $data */
        $data = $paramFetcher->all();
        /** @var Workorder $workorder */
        $workorder = $workorderRepository->find($data['orderId']);
        /** @var ContractorUser $contractorUser */
        $data['user'] = $contractorUserRepository->find($data['user']);

        if (empty($workorder)) {
            throw $this->createNotFoundException('Workorder by id did not found');
        }

        if (empty($data['user'])) {
            throw $this->createNotFoundException('ContractorUser by id did not found');
        }

        $event = $technicianRestService->assignTechnician($workorder, $data);

        return $this->view($event, Response::HTTP_OK);
    }
}