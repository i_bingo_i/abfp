<?php

namespace ApiBundle\Controller;

use AppBundle\Services\Device\AlarmPanelsAttacherService;
use AppBundle\Services\Device\DeviceFileAttacherService;
use ApiBundle\Services\DeviceRestService;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Services\Device\DeviceUpdaterService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DeviceRestController extends FOSRestController
{
    /**
     * Create device
     *
     * ### Response OK ###
     *     {
     *          "id": (string) "3",
     *          "location": (string) "LOCATION fkgdgk",
     *          "comments": (string) "Comment comment comment",
     *          "noteToTester": (string) "TESTER you are tester",
     *          "deleted": (boolean) false,
     *          "createdAt": (int) 1524497561,
     *          "updatedAt": (int) 1524497561,
     *          "title": (string) "RPZ",
     *          "named": (dictionary){
     *                  "inspectionsPatterns": (array)[
     *                  (dictionary){
     *                                  "id": (string) "1",
     *                                  "name": (string) "Annual Backflow Inspection",
     *                                  "frequency": (dictionary){
     *                                          (string) "id": "4",
     *                                          (string) "name": "1 year",
     *                                          (string) "alias": "1_year"
     *                                  },
     *                                  "deficiency": (string) "Deficiency1",
     *                                  "isNeedSendMunicipalityReport": (boolean) true,
     *                                  "isNeedMunicipalityFee": (boolean) true
     *                          }
     *                          ...
     *                  ],
     *                  "repairsPatterns": (array)[
     *                     (dictionary){
     *                                  "id": (string) "2",
     *                                  "name": (string) "Backflow Device Repair (When converted from Proposal)",
     *                                  "deficiency": (string) "Deficiency2",
     *                                  "isNeedSendMunicipalityReport": (boolean) false,
     *                                  "isNeedMunicipalityFee": (boolean) true
     *                          },
     *                          ...
     *                  ],
     *                  "id": (string) "4",
     *                  "name": (string) "Backflow Device",
     *                  "alias": (string) "backflow_device",
     *                  "createdAt": (int) 1524239941,
     *                  "updatedAt": (int) 1524239941,
     *                  "fields": (array)[
     *                       (dictionary){
     *                                  "id": (string) "1",
     *                                  "name": (string) "Size",
     *                                  "alias": (string) "size",
     *                                  "isShow": (boolean) true,
     *                                  "useLabel": (boolean) false,
     *                                  "labelAfter": (boolean) false,
     *                                  "selectOptions": (array) [
     *                                    (dictionary){
     *                                                  "id": (string) "1",
     *                                                  "optionValue": (string) "1/4\"",
     *                                                  "selectDefault": (boolean) false
     *                                          },
     *                                          ...
     *                                 ],
     *                          },
     *                          ...
     *                  ],
     *                  "division": (dictionary){
     *                          "id": (string) "1",
     *                          "name": (string) "Backflow",
     *                          "alias": (string) "backflow"
     *                  }
     *          },
     *          "fields": (array)[
     *                  (dictionary){
     *                          "id": (string) "19",
     *                          "description": (string) "Size",
     *                          "description_id": (string) "1",
     *                          "description_alias": (string) "size",
     *                          "value": (string) "1.5\"",
     *                          "isOptionValue": (boolean) true,
     *                          "optionValueId": (string) "6"
     *                  },
     *                  ...
     *          ]
     *     }
     *
     * ### Request example ###
     *      {
     *          "accountID": 1,
     *          "devicePatternID": 4,
     *          "location": "LOCATION fkgdgk",
     *          "comment": "Comment comment comment",
     *          "note": "TESTER for you",
     *          "fields": {
     *                  "1": "1.5\"",
     *                  "2": "Rainbird",
     *                  "3": "sdsdfs",
     *                  "4": "DCDA",
     *                  "5": "345436476",
     *                  "6": "Cold Water Supply"
     *          }
     *      }
     *
     * @ApiDoc(
     *   section = "Device",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Create device",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="accountID", "dataType"="string", "required"=true, "description"="Account id"},
     *       {"name"="devicePatternID", "dataType"="string", "required"=true, "description"="Device pattern id"},
     *       {"name"="parentID", "dataType"="string", "required"=false, "description"="Device parent id"},
     *       {"name"="location", "dataType"="string", "required"=false, "description"="Location device"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"},
     *       {"name"="note", "dataType"="string", "required"=false, "description"="Notes to tester"},
     *       {"name"="fields", "dataType"="dictionary", "required"=false, "description"="Dynamic fields value"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "Invalid credentials.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Post("/device")
     * @return View
     */
    public function postDeviceCreateAction(Request $request)
    {
        /** @var DeviceRestService $deviceRestService */
        $deviceRestService = $this->get('api.device.rest.service');
        $allData = $request->request->all();

        $result = $deviceRestService->addDeviceToAccount($allData);

        if ($result["code"]) {
            return $this->view(["message" => $result["message"], "code" => 404], 404);
        }

        return new View(
            $result["message"],
            Response::HTTP_OK
        );
    }

    /**
     * Update device
     *
     * ### Response OK ###
     *     {
     *          "id": (string) "3"
     *          "updatedAt": (int) 1524573209
     *     }
     *
     * ### Request example ###
     *      {
     *          "deviceID": "1",
     *          "location": "LOCATION new",
     *          "comment": "Comment comment comment",
     *          "note": "TESTER for you",
     *          "fields": {
     *                  "19": "2.5\"",
     *                  "20": "Rainbird",
     *                  "21": "sdsdfs",
     *                  "22": "DCDA",
     *                  "23": "345436476",
     *                  "24": "Cold Water Supply"
     *          }
     *      }
     *
     * @ApiDoc(
     *   section = "Device",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Update device",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="deviceID", "dataType"="string", "required"=true, "description"="Device id"},
     *       {"name"="location", "dataType"="string", "required"=false, "description"="Location device"},
     *       {"name"="statusAlias", "dataType"="string", "required"=false, "description"="Status alias"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"},
     *       {"name"="note", "dataType"="string", "required"=false, "description"="Notes to tester"},
     *       {"name"="fields", "dataType"="dictionary", "required"=false, "description"="Dynamic fields value"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "Invalid credentials.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Put("/device")
     * @return View
     */
    public function putDeviceUpdateAction(Request $request)
    {
        /** @var DeviceUpdaterService $deviceUpdaterService */
        $deviceUpdaterService = $this->get("app.device.updater.service");

        $allData = $request->request->all();
        $route = $request->getRequestUri();
        $allData["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");
        $result = $deviceUpdaterService->createByParams($allData, $route);


        if (!$result instanceof Device) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Device can not be added'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'updatedAt' => $result->getDateUpdate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Connect/Disconnect Alarm
     *
     * ### Response OK ###
     *     {
     *          "updatedAt": (int) 1524573209
     *     }
     *
     * @ApiDoc(
     *   section = "Connect Alarm",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Connect/Disconnect Alarm",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="deviceID", "dataType"="string", "required"=true, "description"="Device id"},
     *       {"name"="alarmID", "dataType"="string", "required"=false, "description"="Alarm panel id"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'deviceID' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "No device found for id ",
     *     404 = "No alarm panel found for id ",
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Put("/alarm")
     * @return View
     */
    public function putConnectAlarmUpdateAction(Request $request)
    {

        /** @var AlarmPanelsAttacherService $alarmPanelsAttacherService */
        $alarmPanelsAttacherService = $this->get('app.alarm.panel.attacher.service');
        $params = $request->request->all();
        $route = $request->getRequestUri();
        $params["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");

        $result = $alarmPanelsAttacherService->attachById($params, $route);

        if (!$result instanceof Device) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'FACP can not be linked/unlinked to device'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'updatedAt' => $result->getDateCreate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }


    /**
     * Upload device photo
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "pathImage": (string)"",
     *          "updatedAt": (int) 15006986
     *     }
     *
     * @ApiDoc(
     *   section = "Device",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Upload device photo",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   requirements={
     *      {
     *          "name"="deviceID",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Device id"
     *      }
     *   },
     *   parameters={
     *     {"name"="image", "dataType"="file", "required"=true, "description"="Upload device photo"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameters media and deviceId required.",
     *     401 = "API Key 'token key' does not exist.",
     *   }
     * )
     *
     * @param Request $request
     * @Rest\Post("/device/upload")
     * @return View
     */
    public function postUploadDeviceImageAction(Request $request)
    {
        /** @var DeviceFileAttacherService $reportCreatorService */
        $deviceFileAttachService = $this->get("app.device.file.attacher.rest.service");

        $file = $request->files->get("image");
        $deviceId = $request->request->get("deviceID");
        $route = $request->getRequestUri();
        $accessToken = $request->headers->get("X-ACCESS-TOKEN");

        /** @var Device $device */
        $device = $deviceFileAttachService->attachById($deviceId, $file, $route, $accessToken);

        if (!$device instanceof Device) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Device can not be edited'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$device->getId(),
                'pathImage' => $device->getPhoto(),
                'updatedAt' => $device->getDateCreate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Get device
     *
     * ### Response OK ###
     *     {
     *          "id": (string) "3",
     *          "location": (string) "LOCATION fkgdgk",
     *          "comments": (string) "Comment comment comment",
     *          "noteToTester": (string) "TESTER you are tester",
     *          "deleted": (boolean) false,
     *          "createdAt": (int) 1524497561,
     *          "updatedAt": (int) 1524497561,
     *          "title": (string) "RPZ",
     *          "named": (dictionary){
     *                  "inspectionsPatterns": (array)[
     *                  (dictionary){
     *                                  "id": (string) "1",
     *                                  "name": (string) "Annual Backflow Inspection",
     *                                  "frequency": (dictionary){
     *                                          (string) "id": "4",
     *                                          (string) "name": "1 year",
     *                                          (string) "alias": "1_year"
     *                                  },
     *                                  "deficiency": (string) "Deficiency1",
     *                                  "isNeedSendMunicipalityReport": (boolean) true,
     *                                  "isNeedMunicipalityFee": (boolean) true
     *                          }
     *                          ...
     *                  ],
     *                  "repairsPatterns": (array)[
     *                     (dictionary){
     *                                  "id": (string) "2",
     *                                  "name": (string) "Backflow Device Repair (When converted from Proposal)",
     *                                  "deficiency": (string) "Deficiency2",
     *                                  "isNeedSendMunicipalityReport": (boolean) false,
     *                                  "isNeedMunicipalityFee": (boolean) true
     *                          },
     *                          ...
     *                  ],
     *                  "id": (string) "4",
     *                  "name": (string) "Backflow Device",
     *                  "alias": (string) "backflow_device",
     *                  "createdAt": (int) 1524239941,
     *                  "updatedAt": (int) 1524239941,
     *                  "fields": (array)[
     *                       (dictionary){
     *                                  "id": (string) "1",
     *                                  "name": (string) "Size",
     *                                  "alias": (string) "size",
     *                                  "isShow": (boolean) true,
     *                                  "useLabel": (boolean) false,
     *                                  "labelAfter": (boolean) false,
     *                                  "selectOptions": (array) [
     *                                    (dictionary){
     *                                                  "id": (string) "1",
     *                                                  "optionValue": (string) "1/4\"",
     *                                                  "selectDefault": (boolean) false
     *                                          },
     *                                          ...
     *                                 ],
     *                          },
     *                          ...
     *                  ],
     *                  "division": (dictionary){
     *                          "id": (string) "1",
     *                          "name": (string) "Backflow",
     *                          "alias": (string) "backflow"
     *                  }
     *          },
     *          "fields": (array)[
     *                  (dictionary){
     *                          "id": (string) "19",
     *                          "description": (string) "Size",
     *                          "description_id": (string) "1",
     *                          "description_alias": (string) "size",
     *                          "value": (string) "1.5\"",
     *                          "isOptionValue": (boolean) true,
     *                          "optionValueId": (string) "6"
     *                  },
     *                  ...
     *          ]
     *     }
     *
     * ### Request example ###
     *      {
     *          "accountID": 1,
     *          "devicePatternID": 4,
     *          "location": "LOCATION fkgdgk",
     *          "comment": "Comment comment comment",
     *          "note": "TESTER for you",
     *          "fields": {
     *                  "1": "1.5\"",
     *                  "2": "Rainbird",
     *                  "3": "sdsdfs",
     *                  "4": "DCDA",
     *                  "5": "345436476",
     *                  "6": "Cold Water Supply"
     *          }
     *      }
     *
     * @ApiDoc(
     *   section = "Device",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Get device",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="deviceID", "dataType"="string", "required"=true, "description"="Device id"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "Invalid credentials.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param $deviceID
     *
     * @Rest\Get("/device/{deviceID}")
     * @return View
     */
    public function getDeviceAction($deviceID)
    {
        /** @var DeviceRestService $deviceRestService */
        $deviceRestService = $this->get('api.device.rest.service');

        $result = $deviceRestService->getDeviceById($deviceID);

        if ($result["code"]) {
            return $this->view(["message" => $result["message"], "code" => 404], 404);
        }

        return new View(
            $result["message"],
            Response::HTTP_OK
        );
    }


    /**
     * Get device patterns
     *
     * ### Response OK ###
     *     {
     *           [
     *                {
     *                  "inspectionsPatterns": (array)[
     *                  (dictionary){
     *                                  "id": (string) "1",
     *                                  "name": (string) "Annual Backflow Inspection",
     *                                  "frequency": (dictionary){
     *                                          (string) "id": "4",
     *                                          (string) "name": "1 year",
     *                                          (string) "alias": "1_year"
     *                                  },
     *                                  "deficiency": (string) "Deficiency1",
     *                                  "isNeedSendMunicipalityReport": (boolean) true,
     *                                  "isNeedMunicipalityFee": (boolean) true
     *                          }
     *                          ...
     *                  ],
     *                  "repairsPatterns": (array)[
     *                     (dictionary){
     *                                  "id": (string) "2",
     *                                  "name": (string) "Backflow Device Repair (When converted from Proposal)",
     *                                  "deficiency": (string) "Deficiency2",
     *                                  "isNeedSendMunicipalityReport": (boolean) false,
     *                                  "isNeedMunicipalityFee": (boolean) true
     *                          },
     *                          ...
     *                  ],
     *                  "id": (string) "4",
     *                  "name": (string) "Backflow Device",
     *                  "alias": (string) "backflow_device",
     *                  "createdAt": (int) 1524239941,
     *                  "updatedAt": (int) 1524239941,
     *                  "fields": (array)[
     *                       (dictionary){
     *                                  "id": (string) "1",
     *                                  "name": (string) "Size",
     *                                  "alias": (string) "size",
     *                                  "isShow": (boolean) true,
     *                                  "useLabel": (boolean) false,
     *                                  "labelAfter": (boolean) false,
     *                                  "selectOptions": (array) [
     *                                    (dictionary){
     *                                                  "id": (string) "1",
     *                                                  "optionValue": (string) "1/4\"",
     *                                                  "selectDefault": (boolean) false
     *                                          },
     *                                          ...
     *                                 ],
     *                          },
     *                          ...
     *                  ],
     *                  "division": (dictionary){
     *                          "id": (string) "1",
     *                          "name": (string) "Backflow",
     *                          "alias": (string) "backflow"
     *                  }
     *              },
     *              ...
     *          ]
     *     }
     *
     * @ApiDoc(
     *   section = "Device",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Get device patterns",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "Invalid credentials.",
     *     404 = "Something not found"
     *   }
     * )
     *
     *
     * @Rest\Get("/device-patterns/")
     * @return View
     */
    public function getDevicePatternsAction()
    {
        /** @var DeviceRestService $deviceRestService */
        $deviceRestService = $this->get('api.device.rest.service');

        $result = $deviceRestService->getDevicePatterns();

        if ($result["code"]) {
            return $this->view(["message" => $result["message"], "code" => 404], 404);
        }

        return new View(
            $result["message"],
            Response::HTTP_OK
        );
    }


}