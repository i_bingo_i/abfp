<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use AppBundle\Services\UserAccessToken;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserRestController extends FOSRestController
{
    /**
     * Restore password
     *
     * ### Response OK ###
     *     {
     *          "result": (string)"success"
     *     }
     *
     * ### Response FAIL user not found ###
     *      {
     *          "code": 404,
     *          "message": "User not found.",
     *      }
     *
     *
     * @ApiDoc(
     *   section = "Restore password",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Sing In",
     *   parameters={
     *     {"name"="email", "dataType"="string", "required"=true, "description"="User email"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "User not found",
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Get("/forgotpassword")
     * @return View
     */
    public function postRestoreAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var UserRepository $userRepository */
        $userRepository = $objectManager->getRepository('AppBundle:User');

        if (!$request->get('email')) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                "Parameter email required."
            );
        }

        $user = $userRepository->findOneBy(['email' => $request->get('email')]);

        if (!$user) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                "User not found."
            );
        }

        return new View(
            [
                'result' => 'success'
            ],
            Response::HTTP_OK
        );
    }

}