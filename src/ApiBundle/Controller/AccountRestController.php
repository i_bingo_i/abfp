<?php

namespace ApiBundle\Controller;

use ApiBundle\Services\AccountRestService;
use AppBundle\Entity\Account;
use AppBundle\Entity\EntityManager\AccountHistoryManager;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\AccountTypeRepository;
use AppBundle\Entity\Repository\ClientTypeRepository;
use AppBundle\Entity\Repository\StateRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountRestController extends FOSRestController
{
    private $coordinates = [ [40.7127837,-74.0059413], [34.0522342,-118.2436849], [41.8781136,-87.6297982],
        [29.7604267,-95.3698028], [39.9525839,-75.1652215], [33.4483771,-112.0740373], [29.4241219,-98.49362819999999],
        [32.715738,-117.1610838], [32.7766642,-96.79698789999999], [37.3382082,-121.8863286]
    ];

    /**
     * Create account
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "lat" : (float)"",
     *          "lng" : (float)"",
     *          "updatedAt": (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Account",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Create account",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="name", nullable=false, strict=true, description="Account email")
     * @RequestParam(name="address", nullable=false,  strict=true, description="Account address")
     * @RequestParam(name="city", nullable=false,  strict=true, description="Account city")
     * @RequestParam(name="state", nullable=false,  strict=true, description="Account state")
     * @RequestParam(name="zip", nullable=false,  strict=true, description="Account zip")
     * @RequestParam(name="notes", nullable=true,  strict=true, description="Account notes")
     *
     * @Rest\Post("/account")
     * @return View
     */
    public function postAccountCreateAction(ParamFetcher $paramFetcher)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountTypeRepository $accountTypeRepository */
        $accountTypeRepository = $objectManager->getRepository('AppBundle:AccountType');
        /** @var StateRepository $stateRepository */
        $stateRepository = $objectManager->getRepository('AppBundle:State');
        /** @var ClientTypeRepository $clientTypeRepository */
        $clientTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var AccountHistoryManager $accountHistoryManager */
        $accountHistoryManager = $this->get("app.account_history.manager");

//        $newAccount = new Account();
//        $accountTypeSite = $accountTypeRepository->findOneBy(['alias' => 'account']);
//        $state = $stateRepository->findOneBy(['alias' => $paramFetcher->get('state')]);
//        $clientType = $clientTypeRepository->findOneBy(['alias' => 'active']);
//
//        $newAccount->setName($paramFetcher->get('name'));
//        $newAccount->setAddress($paramFetcher->get('address'));
//        $newAccount->setCity($paramFetcher->get('city'));
//        $newAccount->setState($state);
//        $newAccount->setZip($paramFetcher->get('zip'));
//        $newAccount->setNotes($paramFetcher->get('notes'));
//        $newAccount->setClientType($clientType);
//        $newAccount->setType($accountTypeSite);
//
//        $objectManager->persist($newAccount);
//        $objectManager->flush();
//        $accountHistoryManager->createAccountHistoryItem($newAccount, $this->getUser());
        // TODO after should will delete
        $randValue = rand(0,9);
        return new View(
            [
                'id' => strval(rand(0,9)),//$newAccount->getId(),
                'lat' => $this->coordinates[$randValue][0],
                'lng' => $this->coordinates[$randValue][1],
                'updatedAt' => new \DateTime()//$newAccount->getDateCreate()
            ],
            200
        );
    }

    /**
     * Update account
     *
     * ### Response OK ###
     *     {
     *          "lat" : (float)"",
     *          "lng" : (float)"",
     *          "updatedAt": (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Account",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Update account",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="id", nullable=false, strict=true, description="Account id")
     * @RequestParam(name="name", nullable=false, strict=true, description="Account email")
     * @RequestParam(name="address", nullable=false,  strict=true, description="Account address")
     * @RequestParam(name="city", nullable=false,  strict=true, description="Account city")
     * @RequestParam(name="state", nullable=false,  strict=true, description="Account state")
     * @RequestParam(name="zip", nullable=false,  strict=true, description="Account zip")
     * @RequestParam(name="notes", nullable=true,  strict=true, description="Account notes")
     *
     * @Rest\Put("/account")
     * @return View
     */
    public function putAccountUpdateAction(ParamFetcher $paramFetcher)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');
        /** @var AccountTypeRepository $accountTypeRepository */
        $accountTypeRepository = $objectManager->getRepository('AppBundle:AccountType');
        /** @var StateRepository $stateRepository */
        $stateRepository = $objectManager->getRepository('AppBundle:State');
        /** @var ClientTypeRepository $clientTypeRepository */
        $clientTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var AccountHistoryManager $accountHistoryManager */
        $accountHistoryManager = $this->get("app.account_history.manager");

        /** @var Account $account */
//        $account = $accountRepository->findOneBy(['id' => $paramFetcher->get('id')]);
//        $accountTypeSite = $accountTypeRepository->findOneBy(['alias' => 'account']);
//        $state = $stateRepository->findOneBy(['alias' => $paramFetcher->get('state')]);
//        $clientType = $clientTypeRepository->findOneBy(['alias' => 'active']);

//        $account->setName($paramFetcher->get('name'));
//        $account->setAddress($paramFetcher->get('address'));
//        $account->setCity($paramFetcher->get('city'));
//        $account->setState($state);
//        $account->setZip($paramFetcher->get('zip'));
//        $account->setNotes($paramFetcher->get('notes'));
//        $account->setClientType($clientType);
//        $account->setType($accountTypeSite);
//
//        $objectManager->persist($account);
//        $objectManager->flush();
//        $accountHistoryManager->createAccountHistoryItem($account, $this->getUser());
        // TODO after should will delete
        $randValue = rand(0,9);
        return new View(
            [
                'lat' => $this->coordinates[$randValue][0],
                'lng' => $this->coordinates[$randValue][1],
                'updatedAt' => new \DateTime()//$account->getDateUpdate()
            ],
            200
        );
    }

    /**
     * Get account by ID
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "lat" : (float)"",
     *          "lng" : (float)"",
     *          "updatedAt": (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Account",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Get account",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="accountId", "dataType"="string", "required"=false, "description"="Account id"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     * @param $accountId
     * @Rest\Get("/account/{accountId}")
     * @return View
     */
    public function getAccountAction($accountId)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');
        /** @var Serializer $serializer */
        $serializer = $this->get('jms_serializer');

        $account = $accountRepository->find($accountId);

        if(!$account)
        {
            return new View(
                [
                    'message' => "Account not found",
                    'code' => 404
                ],
                404
            );
        }

        $serializedAccount = $serializer->toArray(
            $account,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

//        $serializedAccount = $accountRestService->composeAccountContactPersonsForAccount($account, $serializedAccount);
//
//        print_r($serializedAccount); die();

        return new View(
            [$serializedAccount],
            200
        );
    }

    /**
     * Quick Create Account Contact Person
     *
     * ### Response OK ###
     *     {
     *          "id" : (string) "12"
     *          "firstName" : (string) "Test"
     *          "firstLast" : (string) "Test2"
     *          "phone" : (string) "45455"
     *          "cell" : (string) "564645"
     *          "email" : (string) "65656"
     *          "position" : (string)""
     *          "authorizer" : (boolean) true
     *          "isAccess" : (boolean) true
     *          "access" : (boolean) true
     *          "accessPrimary" : (boolean) true
     *          "payment" : (boolean) true
     *          "paymentPrimary" : (boolean) true
     *          "deleted" : (boolean) false
     *          "address": (string) "Street of Kharkiv building 666, Kiev, ID 04343",
     *          "divisions" : (array)[
     *                      (string) "backflow",
     *                      (string) "fire"
     *                  ]
     *          "timestamp" : (int) 1567324
     *     }
     *
     * ### Request Example ###
     *     {
     *          "accountID": 1,
     *          "name": "Test",
     *          "lastName": "Test2",
     *          "contactTitle": "Mr",
     *          "email": "test@test.com",
     *          "phone": "65465464654",
     *          "cell": "23232234",
     *          "ext": "111",
     *          "fax": "232323",
     *          "notes": "This is test note for API created technician",
     *          "address": "Street of Kharkiv building 666",
     *          "city": "Kiev",
     *          "zip": "04343",
     *          "stateID": "12",
     *          "contactAuthorizer": true,
     *          "contactAccess": true,
     *          "contactAccessPrimary": true,
     *          "contactResponsibility": [1, 3]
     *      }
     *
     * @ApiDoc(
     *   section = "Account",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Quick Create Account Contact Person",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="accountID", "dataType"="string", "required"=true, "description"="Account ID"},
     *       {"name"="name", "dataType"="string", "required"=true, "description"="New Account Contact Person First Name"},
     *       {"name"="lastName", "dataType"="string", "required"=false, "description"="New Account Contact Person Last Name"},
     *       {"name"="contactTitle", "dataType"="string", "required"=false, "description"="New Account Contact Person Title"},
     *       {"name"="email", "dataType"="string", "required"=false, "description"="New Account Contact Person Email"},
     *       {"name"="phone", "dataType"="string", "required"=false, "description"="New Account Contact Person Phone"},
     *       {"name"="cell", "dataType"="string", "required"=false, "description"="New Account Contact Person Cell"},
     *       {"name"="ext", "dataType"="string", "required"=false, "description"="New Account Contact Person Extension"},
     *       {"name"="fax", "dataType"="string", "required"=false, "description"="New Account Contact Person Fax"},
     *       {"name"="notes", "dataType"="string", "required"=false, "description"="New Account Contact Person Notes"},
     *       {"name"="contactCodOnly", "dataType"="boolean", "required"=false, "description"="New Account Contact Person COD Only flag"},
     *       {"name"="address", "dataType"="string", "required"=false, "description"="New Account Contact Person Personal personal address line"},
     *       {"name"="city", "dataType"="string", "required"=false, "description"="New Account Contact Person Personal personal address city"},
     *       {"name"="zip", "dataType"="string", "required"=false, "description"="New Account Contact Person Personal personal address zip"},
     *       {"name"="stateID", "dataType"="string", "required"=false, "description"="New Account Contact Person Personal personal address state ID"},
     *       {"name"="contactAuthorizer", "dataType"="boolean", "required"=false, "description"="New Account Contact Person Personal is Authorizer"},
     *       {"name"="contactAccess", "dataType"="boolean", "required"=false, "description"="New Account Contact Person Personal is Access"},
     *       {"name"="contactAccessPrimary", "dataType"="boolean", "required"=false, "description"="New Account Contact Person Personal is Primary Access"},
     *       {"name"="contactPayment", "dataType"="boolean", "required"=false, "description"="New Account Contact Person Personal is Payment"},
     *       {"name"="contactPaymentPrimary", "dataType"="boolean", "required"=false, "description"="New Account Contact Person Personal is Primary Payment"},
     *       {"name"="contactResponsibility", "dataType"="array", "required"=false, "description"="New Account Contact Person responsibilities"}
     *
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     * @param Request $request
     * @Rest\Post("/contact-person")
     * @return View
     */
    public function createAccountContactPersonAction(Request $request)
    {
        /** @var AccountRestService $accountRestService */
        $accountRestService = $this->get("api.account.rest.service");

        $params = $request->request->all();

        $result = $accountRestService->createACP($params);

        if ($result["code"]) {
            return $this->view(["message" => $result["message"], "code" => 404], 404);
        }

        return new View(
            $result["message"],
            Response::HTTP_OK
        );
    }
}
