<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class InspectionRestController extends FOSRestController
{
    /**
     * Relink inspection to another Device
     *
     * ### Response OK ###
     *     {
     *          "updatedAt": (string)""
     *     }
     * ### Response FAIL bad request. Parameter should not be blank ###
     *     {
     *          "code": 400,
     *          "message": "Parameter 'parameter' of value '' violated a constraint 'This value should not be blank.'"
     *     }
     * ### Response FAIL service not found ###
     *     {
     *          "code": 404,
     *          "message": "No service found for id "
     *     }
     *
     * @ApiDoc(
     *   section = "Inspection",
     *   tags={
     *      "not tested method" = "#dc9000"
     *   },
     *   resource = true,
     *   description = "Relink inspection to another Device",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "No found",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="serviceID", requirements="\d+", nullable=false, allowBlank=false, strict=true, description="Service ID")
     * @RequestParam(name="deviceID", requirements="\d+", nullable=false, allowBlank=false, strict=true, description="Device ID")
     *
     * @Rest\Put("/inspection/relink")
     * @return array|mixed
     */
    public function putRelinkInspectionToAnotherDeviceAction(ParamFetcher $paramFetcher)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $objectManager->getRepository('AppBundle:Service');
        /** @var DeviceRepository $deviceRepository */
        $deviceRepository = $objectManager->getRepository('AppBundle:Device');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var Service $service */
        $service = $serviceRepository->find($paramFetcher->get('serviceID'));
        /** @var Device $device */
        $device = $deviceRepository->find($paramFetcher->get('deviceID'));

        if (!$service) {
            throw $this->createNotFoundException("No service found for id {$service->getId()}");
        }

        if (!$device) {
            throw $this->createNotFoundException("No device found for id {$device->getId()}");
        }

        if ($service->getAccount()->getId() != $device->getAccount()->getId()
            || $service->getDepartmentChannel()->getDevision()->getId() != $device->getNamed()->getCategory()->getId()
        ) {
            throw new BadRequestHttpException('This service cannot relink to this device');
        }

        $service->setDevice($device);
        $serviceManager->checkAndCreateContractorService($service);
        $serviceManager->update($service);

        // TODO It is strings will need be on event
        if (!empty($service->getOpportunity())) {
            $serviceManager->resetOpportunity($service);
        }
        $serviceManager->attachOpportunity($service);

        return new View(
            [
                'updatedAt' => $service->getTestDate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Complete Inspection
     *
     * ### Response OK ###
     *     {
     *          "serviceID": (string)"",
     *          "comment" : (string)"",
     *          "result" : (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Inspection",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Complete Inspection",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="serviceID", nullable=false, strict=true, description="Service id")
     * @RequestParam(name="comment", nullable=true, strict=true, description="Comment")
     * @RequestParam(name="result", nullable=false, strict=true, description="One of predefined success, failed, notTestable")
     *
     * @Rest\Post("/completeInspection")
     * @return array|mixed
     */
    public function postCompleteInspectionAction(ParamFetcher $paramFetcher)
    {
        $data = $paramFetcher->all();

        return new View(
            [
                'id' => strval(rand(0, 9)),
                'updatedAt' => new \DateTime()
            ],
            Response::HTTP_OK
        );
    }
}