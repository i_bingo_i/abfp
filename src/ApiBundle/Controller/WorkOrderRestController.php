<?php

namespace ApiBundle\Controller;

use ApiBundle\Services\WorkorderLogRestService;
use ApiBundle\Services\WorkOrderRestService;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Repository\WorkorderCommentRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderComment;
use AppBundle\Entity\WorkorderLog;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\RestBundle\Controller\Annotations\FileParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WorkorderBundle\Services\CommentService;
use WorkorderBundle\Services\SignatureAttacherService;
use WorkorderBundle\Services\WorkorderService;

class WorkOrderRestController extends FOSRestController
{
    /**
     * Add comment
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "timestamp": (string)""
     *     }
     *
     * ### Response FAIL Workorder not found ###
     *      {
     *          "code": 404,
     *          "message": "Workorder not found."
     *      }
     *
     * @ApiDoc(
     *   section = "Comment",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Create comment",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'orderId OR comment' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Workorder not found",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="orderID", nullable=false, strict=true, description="Workorder ID")
     * @RequestParam(name="comment", nullable=false,  strict=true, description="Text of comment")
     *
     * @Rest\Post("/comment")
     * @return View
     */
    public function postCommentCreateAction(ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        return new View(
            [
                'id' => (string) rand(1, 10),
                'timestamp' => (int)(new \DateTime())->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Edit comment
     *
     * ### Response OK ###
     *     {
     *          "timestamp": (string)""
     *     }
     *
     * ### Response FAIL Comment not found ###
     *      {
     *          "code": 404,
     *          "message": "Comment not found."
     *      }
     *
     * @ApiDoc(
     *   section = "Comment",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Edit comment",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'commentID OR comment' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Comment not found",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="commentID", nullable=false, strict=true, description="Comment ID to edit")
     * @RequestParam(name="comment", nullable=false,  strict=true, description="Text of comment")
     *
     * @Rest\Put("/comment")
     * @return View
     */
    public function putCommentUpdateAction(ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        return new View(
            [
                'timestamp' => new \DateTime()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Workorder create
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "timestamp": (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Create Workorder",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'accountID OR ect..' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="accountID", nullable=false, strict=true, description="Account ID")
     * @RequestParam(name="serviceID", nullable=false,  strict=true, description="Service ID")
     * @RequestParam(name="authorizerID", nullable=false,  strict=true, description="Authorizer ID")
     * @RequestParam(name="paymentType", nullable=false,  strict=true, description="Payment Type")
     * @RequestParam(name="directions", nullable=true,  strict=true, description="Directions")
     * @RequestParam(name="comments", nullable=true,  strict=true, description="Comments")
     *
     * @Rest\Post("/order")
     * @return View
     */
    public function postWorkorderCreateAction(ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        return new View(
            [
                'id' => (string) rand(1, 10),
                'timestamp' => new \DateTime()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Workorder media
     *
     * ### Response OK ###
     *     {
     *          "pathImage": (string)"",
     *          "timestamp": (string)""
     *     }
     *
     * ### 400 Bad Request ###
     *     {
     *          "code": 400,
     *          "message": "Parameter 'orderId' of value 'Some no valid value' violated a constraint 'Parameter 'orderId' value, does not match requirements '[0-9]+''"
     *     }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Add image to closed workorder",
     *   headers = {
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters = {
     *          {"name"="image", "dataType"="file", "required"=true, "description"="Upload media"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Validation errors",
     *     401 = "API Key 'token key' does not exist.",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="orderId", nullable=false, allowBlank=false, requirements="[0-9]+", strict=true, description="Workoeder ID")
     * @FileParam(name="image", requirements={"mimeTypes"="image/jpeg"}, image=true)
     *
     * @Rest\Post("/media-order")
     * @return View
     */
    public function postMediaAddAction(ParamFetcher $paramFetcher)
    {
        /** @var WorkOrderRestService $workorderRestService */
        $workorderRestService = $this->get('api.workorder.rest.service');
        $orderId = $paramFetcher->get('orderId');
        $image = $paramFetcher->get('image');

        $imagePath = $workorderRestService->uploadSignature($orderId, $image);

        return $this->view(
            [
                'pathImage' => $imagePath,
                'timestamp' => new \DateTime()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Add Signature to Workorder
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "pathImage": (string)"",
     *          "updatedAt": (int) 15006986
     *     }
     * ### 400 Bad Request ###
     *     {
     *          "code": 400,
     *          "message": "Parameter 'orderId' of value 'Some no valid value' violated a constraint 'Parameter 'orderId' value, does not match requirements '[0-9]+''"
     *     }
     * ### 404 Not found error ###
     *      {
     *          "code": 404,
     *          "message": "Workorder id does not found"
     *      }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Add signature to order",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters={
     *       {"name"="orderID", "dataType"="string", "required"=true, "description"="Order ID"},
     *       {"name"="contactID", "dataType"="string", "required"=false, "description"="Contact person ID"},
     *       {"name"="image", "dataType"="file", "required"=true, "description"="Signature"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Validation errors",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Not found error"
     *   }
     * )
     *
     *
     * @param Request $request
     *
     * @Rest\Post("/order/signature")
     * @return View
     */
    public function postWorkorderSignatureAction(Request $request)
    {
        /** @var SignatureAttacherService $signatureAttacherService */
        $signatureAttacherService = $this->get("workorder.signature.attacher.service");

        $params = $request->request->all();
        $params["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");
        $file = $request->files->get("image");
        $route = $request->getRequestUri();

        $result = $signatureAttacherService->attachByParams($params, $file, $route);

        if (!$result instanceof Workorder) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Workorder signature can not be uploaded'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'pathImage' => $result->getSignature(),
                'updatedAt' => $result->getDateUpdate()->getTimestamp()
            ],
            Response::HTTP_OK
        );

//        /** @var WorkOrderRestService $workorderRestService */
//        $workorderRestService = $this->get('api.workorder.rest.service');
//
//        $params = $request->request->all();
//        $file = $request->files->get("image");
//
//        $result = $workorderRestService->addSignatureToWO($params, $file);
//
//        if ($result["code"]) {
//            return $this->view(["message" => $result["message"], "code" => 404], 404);
//        }
//
//        return new View(
//            [
//                "id" => $result["message"]["id"],
//                "updatedAt" => $result["message"]["updatedAt"],
//                "pathImage" => $result["message"]["pathImage"]
//            ],
//            Response::HTTP_OK
//        );
    }


    /**
     * Get Workorders List
     *
     * ### Response OK ###
     *     {
     *          "currentDate": (int) 1523970512,
     *          "orders": (array)[
     *
     *          ],
     *          "devicePattern": (array)[
     *
     *          ],
     *          "companies": (array)[
     *
     *          ],
     *          "municipalities": (array)[
     *
     *          ],
     *          "states": (array)[
     *
     *          ],
     *          "status": (array)[
     *              "reportStatus": (array)[
     *                  (dictionary){
     *                      "id": (string) "",
     *                      "name": (string) "",
     *                      "alias": (string) "",
     *                  }
     *              ]
     *          ]
     *     }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Get orders list",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="techID", "dataType"="string", "required"=true, "description"="Techitian ID"},
     *       {"name"="currentTimeStamp", "dataType"="string", "required"=false, "description"="Timestamp"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'orderID OR signerID' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param $techID
     * @param $currentTimeStamp
     *
     * @Rest\Get("/orders/list/{techID}/{currentTimeStamp}", defaults={"currentTimeStamp" = ""})
     * @return View
     */
    public function getWorkordersListAction($techID = 0, $currentTimeStamp = '')
    {
        /** @var WorkOrderRestService $workorderRestService */
        $workorderRestService = $this->get('api.workorder.rest.service');

        if (!$currentTimeStamp) {
            $currentTimeStamp = time();
        }
        $workorderList = $workorderRestService->getWorkordersList($techID, $currentTimeStamp);

        if (!$workorderList or empty($workorderList)) {
            return new View(
                [
                    "message" => "Workorders not found",
                    "code" => 404
                ],
                404
            );
        }

        return new View(
            $workorderList,
            Response::HTTP_OK
        );
    }

    /**
     * Get Workorders Info for map
     *
     * @ApiDoc(
     *   section = "Development",
     *   tags={
     *      "not tested method" = "#dc9000"
     *   },
     *   resource = true,
     *   description = "Get orders list",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters={
     *       {"name"="workorderID", "dataType"="string", "required"=true, "description"="Division ID"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'orderID OR signerID' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param $workorderID
     *
     * @Rest\Get("/orders/map/{workorderID}", defaults={"workorderID" = ""})
     * @return View
     */
    public function getWorkordersInfoByDivisionAction($workorderID)
    {
        /** @var WorkOrderRestService $workorderRestService */
        $workorderRestService = $this->get('api.workorder.rest.service');

        $result = $workorderRestService->getWorkordersInfoByDivision($workorderID);

        if ($result["code"]) {
            return $this->view(["message" => $result["message"], "code" => 404], 404);
        }

        return new View(
            $result["message"],
            Response::HTTP_OK
        );
    }

    /**
     * Add Service to Workorder
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "updatedAt": (int) 15006986
     *     }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Add Service to Workorder",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters={
     *       {"name"="orderID", "dataType"="string", "required"=true, "description"="Workorder ID"},
     *       {"name"="serviceID", "dataType"="string", "required"=true, "description"="Service ID"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'workorderID OR serviceID' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param $request Request
     *
     * @Rest\Put("/order/service")
     * @return View
     * @throws \Exception
     */
    public function addServiceToWorkorderAction(Request $request)
    {
        /** @var WorkOrderRestService $workorderRestService */
        $workorderRestService = $this->get('api.workorder.rest.service');
        $params = $request->request->all();
        $params["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");
        $params["route"] = $request->getRequestUri();

        $result = $workorderRestService->addServiceToWorkorder($params);

        if ($result["code"]) {
            return $this->view(["message" => $result["message"], "statusCode" => "601"], Response::HTTP_BAD_REQUEST);
        }

        return new View(
            $result["message"],
            Response::HTTP_OK
        );
    }

    /**
     * Re-Schedule Workorder
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "updatedAt": (int) 15006986
     *     }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Re-Schedule Workorder",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters={
     *       {"name"="orderID", "dataType"="string", "required"=true, "description"="Workorder ID"},
     *       {"name"="rescheduleTime", "dataType"="int", "required"=false, "description"="Re-schedule time"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'workorderID OR serviceID' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param $request Request
     *
     * @Rest\Put("/order/reschedule")
     * @return View
     * @throws \Exception
     */
    public function reScheduledWorkorderAction(Request $request)
    {
        /** @var WorkorderService $workorderService */
        $workorderService = $this->get("workorder.workorder_service.service");

        $params = $request->request->all();
        $params["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");
        $route = $request->getRequestUri();

        $result = $workorderService->rescheduleByParams($params, $route);

        if (!$result instanceof Workorder) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Workorder can not receive reschedule request'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'updatedAt' => $result->getDateUpdate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
//        /** @var WorkOrderRestService $workorderRestService */
//        $workorderRestService = $this->get('api.workorder.rest.service');
//        $params = $request->request->all();
//
//        $result = $workorderRestService->reScheduleWorkorder($params);
//
//        if ($result["code"]) {
//            return $this->view(["message" => $result["message"], "code" => 404], 404);
//        }
//
//        return new View(
//            $result["message"],
//            Response::HTTP_OK
//        );
    }

    /**
     * Create media comment for workorder
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "orderID": (string)"",
     *          "updatedAt": (int) 15006986
     *     }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Create media comment for workorder",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters={
     *       {"name"="orderID", "dataType"="string", "required"=true, "description"="Workorder ID"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment for workorder"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Parameter 'orderID OR serviceID' of value 'NULL' violated a constraint 'This value should not be null.'",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param $request Request
     *
     * @Rest\Post("/order/comment/create")
     * @return View
     */
    public function workorderCommentCreateAction(Request $request)
    {
        /** @var CommentService $workorderCommentService */
        $workorderCommentService =$this->get("workorder.comment.service");

        $params = $request->request->all();
        $params["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");
        $route = $request->getRequestUri();

        $result = $workorderCommentService->createByParams($params, $route);

        if (!$result instanceof WorkorderComment) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Workorder comment can not be added'],
                Response::HTTP_BAD_REQUEST
            );
         }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'updatedAt' => $result->getDateUpdate()->getTimestamp()
            ],
            Response::HTTP_OK
        );

//        /** @var WorkOrderRestService $workorderRestService */
//        $workorderRestService = $this->get('api.workorder.rest.service');
//        $params = $request->request->all();
//        $params["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");
//
//        $result = $workorderRestService->addWorkordersComment($params);
//
//        if ($result["code"]) {
//            return $this->view(["message" => $result["message"], "code" => 404], 404);
//        }
//
//        return new View(
//            $result["message"],
//            Response::HTTP_OK
//        );
    }

    /**
     * Add Workorder's comment's file
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "pathImage": (string)"",
     *          "updatedAt": (int) 15006986
     *     }
     * ### 400 Bad Request ###
     *     {
     *          "code": 400,
     *          "message": "Something not found"
     *     }
     * ### 404 Not found error ###
     *      {
     *          "code": 404,
     *          "message": "Something not found"
     *      }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Add Workorder's comment's file",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters={
     *       {"name"="commentID", "dataType"="string", "required"=true, "description"="Comment ID"},
     *       {"name"="image", "dataType"="file", "required"=true, "description"="File for comment"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Validation errors",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Not found error"
     *   }
     * )
     *
     *
     * @param Request $request
     *
     * @Rest\Post("/order/comment/media")
     * @return View
     */
    public function postAddWorkordersCommentFileAction(Request $request)
    {
        /** @var CommentService $workorderCommentService */
        $workorderCommentService = $this->get("workorder.comment.service");

        $file = $request->files->get("image");
        $commentID = $request->request->get("commentID");
        $accessToken = $request->headers->get("X-ACCESS-TOKEN");
        $route = $request->getRequestUri();

        $result = $workorderCommentService->attachFileByParams($commentID, $file, $route, $accessToken);

        if (!$result instanceof WorkorderComment) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Workorder attachment image can not be uploaded'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'pathImage' => $result->getPhoto(),
                'updatedAt' => $result->getDateUpdate()->getTimestamp()
            ],
            Response::HTTP_OK
        );


//        /** @var WorkOrderRestService $workorderRestService */
//        $workorderRestService = $this->get('api.workorder.rest.service');
//
//        $params = $request->request->all();
//        $file = $request->files->get("image");
//
//        $result = $workorderRestService->addWorkordersCommentFile($params, $file);
//
//        if ($result["code"]) {
//            return $this->view(["message" => $result["message"], "code" => 404], 404);
//        }
//
//        return new View(
//            [
//                "id" => $result["message"]["id"],
//                "updatedAt" => $result["message"]["updatedAt"],
//                "pathImage" => $result["message"]["pathImage"]
//            ],
//            Response::HTTP_OK
//        );
    }

    /**
     * Get workorder comments by ID
     *
     * ### Response OK ###
     *     {
     *          "id": (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Get workorders comments",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="orderID", "dataType"="string", "required"=true, "description"="Workorder id"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     * @param $orderID
     * @param Request $request
     *
     * @Rest\Get("/order/comment/{orderID}")
     * @return View
     */
    public function getWorkordersCommentsAction($orderID, Request $request)
    {
        /** @var CommentService $workorderCommentService */
        $workorderCommentService = $this->get("workorder.comment.service");

        $route = $request->getRequestUri();

        $result = $workorderCommentService->getById($orderID, $route);

        if (is_bool($result) and $result === false) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Workorder not found'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            $result,
            Response::HTTP_OK
        );
    }

    /**
     * Workorder Log comment
     *
     * ### Response OK ###
     *     {
     *          "id" : 4 (string),
     *          "date" : 1531321554 (integer),
     *          "message" : "Elon Mark was assigned to the workorder to 07/21/2020, 07:00 AM - 10:30 AM" (string)
     *     }
     *
     * ### Response FAIL Workorder not found ###
     *      {
     *          "code": 404,
     *          "message": "Workorder not found."
     *      }
     *
     * @ApiDoc(
     *   section = "Workorder",
     *   tags={
     *      "not tested method" = "#dc9000"
     *   },
     *   resource = true,
     *   description = "Get log comment by Workorder",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Workorder id"
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Workorder not found",
     *   }
     * )
     * @param Request $request
     * @Rest\Get("/workorder-log-comment")
     * @Rest\View(serializerGroups={"full"})
     * @return mixed
     */
    public function getWorkorderLogCommentAction(Request $request)
    {
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $this->get('app.entity_manager')->getRepository('AppBundle:Workorder');
        /** @var Workorder|null $workorder */
        $workorder = $workorderRepository->find($request->get('id'));

        if (empty($workorder)) {
            throw $this->createNotFoundException("Workorder by ({$request->get('id')}) ID not found");
        }

        $workorderLogComment = $workorder->getLogs()->filter(function (WorkorderLog $log){
            return !empty($log->getChangingStatus());
        })->first();

        return new View($workorderLogComment,Response::HTTP_OK);
    }

}