<?php

namespace ApiBundle\Controller;

use ApiBundle\Services\TechnicianRestService;
use AppBundle\Entity\Contractor;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Services\Search;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;


class ContractorRestController extends FOSRestController
{
    /**
     * Search contractors by Name
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "name" : (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Contractor",
     *   tags={
     *      "not tested method" = "#dc9000"
     *   },
     *   resource = true,
     *   description = "Search contractors by Name",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="phrase", nullable=false, strict=true, description="Name for search")
     *
     * @Rest\Post("/contractor/search-by-name")
     * @return array|mixed
     */
    public function postSearchByNameAction(ParamFetcher $paramFetcher)
    {
        /** @var Search $searchService */
        $searchService = $this->get('app.search');
        $phrase = $paramFetcher->get('phrase');

        /** @var Contractor[] $searchResult */
        $searchResult = $searchService->searchContractor($phrase);

        return $this->view($searchResult, Response::HTTP_OK);
    }

    /**
     * Get device
     *
     * ### Response OK ###
     *     {
     *          "id": (string) "3",
     *          "location": (string) "LOCATION fkgdgk"
     *     }
     *
     *
     * @ApiDoc(
     *   section = "Technician",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Get contractor profile",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="contractorID", "dataType"="string", "required"=true, "description"="Technician id"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "Invalid credentials.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param $contractorUserID
     *
     * @Rest\Get("/technician/{contractorUserID}")
     * @return View
     */
    public function getContractorUserByIdAction($contractorUserID)
    {

        /** @var TechnicianRestService $contractorRestService */
        $contractorRestService = $this->get("api.technician.rest.service");

        $result = $contractorRestService->getTechnicianProfile($contractorUserID);

        if ($result["code"]) {
            return $this->view(["message" => $result["message"], "code" => 404], 404);
        }

        return new View(
            $result["message"],
            Response::HTTP_OK
        );
    }
}
