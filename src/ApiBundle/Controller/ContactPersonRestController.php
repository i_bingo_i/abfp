<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\EntityManager\ContactPersonHistoryManager;
use AppBundle\Entity\Repository\ContactPersonRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;

class ContactPersonRestController extends FOSRestController
{
    /**
     * Update contact person
     *
     * ### Response OK ###
     *     {
     *          "updatedAt": (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Contact person",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Update contact person",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="id", nullable=false, strict=true, description="Contact person id")
     * @RequestParam(name="title", nullable=false, strict=true, description="Contact person title")
     * @RequestParam(name="phone", nullable=false,  strict=true, description="Contact person phone")
     * @RequestParam(name="ext", nullable=false,  strict=true, description="Contact person ext")
     * @RequestParam(name="cell", nullable=false,  strict=true, description="Contact person cell")
     * @RequestParam(name="email", nullable=false,  strict=true, description="Contact person email")
     *
     * @Rest\Put("/contact-person")
     * @return View
     */
    public function putUpdateContactPersonAction(ParamFetcher $paramFetcher)
    {
        /** @var ObjectManager $objectManager */
//        $objectManager = $this->get('app.entity_manager');
        /** @var ContactPersonHistoryManager $contactPersonHistoryManager */
//        $contactPersonHistoryManager = $this->get("app.contact_person_history.manager");
        /** @var ContactPersonRepository $contactPersonRepository */
//        $contactPersonRepository = $objectManager->getRepository("AppBundle:ContactPerson");
        /** @var  ContactPerson $contactPersonForUpdate */
//        $contactPersonForUpdate = $contactPersonRepository->find($paramFetcher->get('id'));


//        $contactPersonForUpdate->setTitle($paramFetcher->get('title'));
//        $contactPersonForUpdate->setPhone($paramFetcher->get('phone'));
//        $contactPersonForUpdate->setExt($paramFetcher->get('ext'));
//        $contactPersonForUpdate->setCell($paramFetcher->get('cell'));
//        $contactPersonForUpdate->setEmail($paramFetcher->get('email'));
//
//        $objectManager->persist($contactPersonForUpdate);
//        $objectManager->flush();
//        $contactPersonHistoryManager->createContactPersonHistoryItem($contactPersonForUpdate, $this->getUser());

        return new View(
            [
                'updatedAt' => new \DateTime()//$contactPersonForUpdate->getDateUpdate()
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Create contact person
     *
     * ### Response OK ###
     *     {
     *          "id" : (string)""
     *          "updatedAt" : (string)""
     *     }
     *
     * @ApiDoc(
     *   section = "Contact person",
     *   tags={
     *      "mock up method" = "#985151"
     *   },
     *   resource = true,
     *   description = "Create contact person",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     *
     * @param ParamFetcher $paramFetcher
     *
     * @RequestParam(name="accountID", nullable=false, strict=true, description="Account Id")
     * @RequestParam(name="name", nullable=false, strict=true, description="Contact name")
     * @RequestParam(name="lastName", nullable=true, strict=true, description="Contact person last name")
     * @RequestParam(name="phone", nullable=true,  strict=true, description="Contact person phone")
     * @RequestParam(name="ext", nullable=true,  strict=true, description="Contact person ext")
     * @RequestParam(name="cell", nullable=true,  strict=true, description="Contact person cell")
     * @RequestParam(name="email", nullable=true,  strict=true, description="Contact person email")
     * @RequestParam(name="fax", nullable=true,  strict=true, description="Contact person fax")
     * @RequestParam(name="notes", nullable=true,  strict=true, description="Contact person notes")
     * @RequestParam(name="address", nullable=true,  strict=true, description="Personal address of contact")
     * @RequestParam(name="sity", nullable=true,  strict=true, description="Contact person sity")
     * @RequestParam(name="stateID", nullable=true,  strict=true, description="Contact person email")
     * @RequestParam(name="zip", nullable=true,  strict=true, description="Contact person email")
     *
     * @Rest\Post("/contact-person-old")
     * @return View
     */
    public function postContactPersonCreateAction(ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        return new View(
            [
                'id' => (string) rand(1, 10),
                'updatedAt' => new \DateTime()
            ],
            Response::HTTP_OK
        );
    }
}
