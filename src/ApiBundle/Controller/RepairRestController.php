<?php

namespace ApiBundle\Controller;

use ApiBundle\Services\ServiceRestService;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use ServiceBundle\Services\RepairDiscardService;
use ServiceBundle\Services\ServiceUpdater;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RepairRestController extends FOSRestController
{
    /**
     * Discard Repair Service for Device
     *
     * ### Response OK ###
     *     {
     *          "updatedAt": (int) 153421123,
     *     }
     * ### Response FAIL Validation error ###
     *     {
     *          "code": 400,
     *          "message": "Parameter 'serviceID' of value '' violated a constraint 'This value should not be blank.'",
     *     }
     * ### Response FAIL No found error ###
     *     {
     *          "code": 404,
     *          "message": "No service found for id ",
     *     }
     *
     * @ApiDoc(
     *   section = "Repair",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Discard Repair",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *   },
     *   parameters={
     *       {"name"="serviceID", "dataType"="string", "required"=true, "description"="Service id"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad request",
     *     404 = "No found",
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Put("/repair/discard")
     * @return View
     */
    public function putRepairDiscardAction(Request $request)
    {
        /** @var RepairDiscardService $repairDiscardService */
        $repairDiscardService = $this->get("service.repair.discard.service");
        $allData = $request->request->all();
        $route = $request->getRequestUri();
        $allData["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");

        $result = $repairDiscardService->discardByParams($allData, $route);


        if (!$result instanceof Service) {
            return $this->view(
                ['statusCode' => '601', 'message' => 'Non-frequency service can not be discarded'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'updatedAt' => $result->getDateCreate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }


    /**
     * Create repair service
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "updatedAt": (int) 1524573209
     *     }
     *
     * ### Request example ###
     *      {
     *          "deviceID": "1",
     *          "serviceNameID": "2",
     *          "comment": "this is my comment"
     *      }
     *
     * @ApiDoc(
     *   section = "Repair",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Create repair service",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="deviceID", "dataType"="string", "required"=true, "description"="Device id"},
     *       {"name"="serviceNameID", "dataType"="string", "required"=true, "description"="Array of Service's name id"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found"
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Post("/service/repair")
     * @return View
     */
    public function postRepairServiceToDeviceAction(Request $request)
    {
        /** @var ServiceRestService $serviceResService */
        $serviceResService = $this->get("api.service.rest.service");
        $allData = $request->request->all();
        $route = $request->getRequestUri();
        $allData["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");

        $result = $serviceResService->addRepairServiceToDevice($allData, $route);

        if ($result["code"]) {
            return $this->view(
                ['statusCode' => '601', 'message' => $result["message"]],
                Response::HTTP_BAD_REQUEST
            );
        }

        return new View(
            $result["message"],
            Response::HTTP_OK
        );
    }

    /**
     * Update repair service
     *
     * ### Response OK ###
     *     {
     *          "id": (string) "12"
     *          "updatedAt": (int) 15668543
     *     }
     *
     * @ApiDoc(
     *   section = "Repair",
     *   tags={
     *      "real work method" = "#4890da",
     *      "601 answer uses method" = "#54b50a"
     *   },
     *   resource = true,
     *   description = "Update repair service",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="serviceID", "dataType"="string", "required"=true, "description"="Service id"},
     *       {"name"="deviceID", "dataType"="string", "required"=true, "description"="Device id"},
     *       {"name"="comment", "dataType"="string", "required"=false, "description"="Comment"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     401 = "API Key 'token key' does not exist.",
     *     404 = "Something not found"
     *   }
     * )
     *
     *
     * @param Request $request
     *
     * @Rest\Put("/service/repair")
     * @return View
     */
    public function putRepairServiceUpdateAction(Request $request)
    {
        /** @var ServiceUpdater $serviceUpdater */
        $serviceUpdater = $this->get("service.updater.service");

        $allData = $request->request->all();
        $allData["accessToken"] = $request->headers->get("X-ACCESS-TOKEN");
        $route = $request->getRequestUri();

        $result = $serviceUpdater->updateRepairServiceByParams($allData, $route);

        if (!$result instanceof Service) {
            return $this->view(
                ['statusCode' => '601', 'message' => $result],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            [
                'id' => (string)$result->getId(),
                'updatedAt' => $result->getDateUpdate()->getTimestamp()
            ],
            Response::HTTP_OK
        );
    }
}
