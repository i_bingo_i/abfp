<?php

namespace ApiBundle\Services;

use AppBundle\Entity\EntityManager\EventManager;
use AppBundle\Entity\EntityManager\WorkorderLogManager;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Event;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Event\Creator;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Services\UserAccessToken;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use WorkorderBundle\Services\LogCreator;
use WorkorderBundle\Services\LogMessage;

class TechnicianRestService
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var UserAccessToken */
    private $userAccessTokenService;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;
    /** @var WorkorderManager */
    private $workorderManager;
    /** @var WorkorderLogManager */
    private $workorderLogManager;
    /** @var EventManager */
    private $eventManager;
    /** @var Serializer */
    private $serializer;
    /** @var Creator */
    private $eventCreatorService;

    /**
     * TechnicianRestService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;

        $this->userAccessTokenService = $this->container->get('app.user_token');
        $this->workorderManager = $this->container->get('app.work_order.manager');
        $this->serializer = $container->get("jms_serializer");
        $this->workorderLogManager = $this->container->get('app.workorder_log.manager');
        $this->eventManager = $this->container->get('app.event.manager');
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');
        $this->eventCreatorService = $this->container->get('app.event_creator.service');
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function prepareResponse(User $user)
    {
        $this->setAccessToken($user);

        $res["currentDate"] = time();
        $res["accessToken"] = $user->getAccessToken();
        $res["technician"] = $this->getContractorUser($user);

        return $res;
    }

    /**
     * @param Workorder $workorder
     * @param $getParams
     * @return \AppBundle\Entity\Event
     * @throws \Exception
     */
    public function assignTechnician(Workorder $workorder, $getParams)
    {
        $getParams = $this->serializeParameters($getParams);
        $isChangedWorkorderStatus = $this->workorderManager->setScheduledDate($workorder, $getParams);
        $this->workorderManager->update($workorder);
        /** @var Event $event */
        $event = $this->eventCreatorService->create($workorder, $getParams);

        // Create log record in WO log
        /** @var LogMessage $logMessage */
        $logMessage = $this->container->get('workorder.log_message');
        /** @var LogCreator $woLogCreator */
        $woLogCreator = $this->container->get('workorder.log_creator');
        $woLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_WORKORDER_WAS_SCHEDULED,
            'message' => $logMessage->makeByCreateEvent($event),
            'changingStatus' => $isChangedWorkorderStatus
        ]);

        return $event;
    }


    /**
     * @param $contractorUserID
     * @return array
     */
    public function getTechnicianProfile($contractorUserID)
    {
        $contractorUser = $this->contractorUserRepository->find($contractorUserID);

        if (!$contractorUser) {
            return ["code" => 1, "message" => "There is no contractor user with such id ".$contractorUserID];
        }

        $serializedContractorUser = $this->serializer->toArray(
            $contractorUser,
            SerializationContext::create()->setGroups(array('workorder'))
        );

        return ["code" => 0, "message" => $serializedContractorUser];
    }

    /**
     * @param User $user
     */
    private function setAccessToken(User $user)
    {
        $userAccessToken = $this->userAccessTokenService->createUserAccessToken($user);
        $user->setAccessToken($userAccessToken);

        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }

    /**
     * @param User $user
     * @return \AppBundle\Entity\ContractorUser|null|object
     */
    private function getContractorUser(User $user)
    {
        return $this->contractorUserRepository->findOneBy(['user' => $user->getId()]);
    }

    /**
     * @param $getParams
     * @return mixed
     */
    private function serializeParameters($getParams)
    {
        $startDate = $getParams['startDate'];
        $getParams['startDate'] = (new \DateTime())->setTimestamp($startDate)->format('m/d/Y');
        $getParams['startTime'] = (new \DateTime())->setTimestamp($startDate)->format('H:i');
        $getParams['endTime'] = (new \DateTime())->setTimestamp($getParams['endDate'])->format('H:i');

        return $getParams;
    }

}