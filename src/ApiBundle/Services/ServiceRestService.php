<?php

namespace ApiBundle\Services;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorType;
use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\EntityManager\OpportunityManager;
use AppBundle\Entity\EntityManager\ReportManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Report;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\ContractorTypeRepository;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\ReportRepository;
use AppBundle\Entity\Repository\ReportStatusRepository;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceNamed;
use AppBundle\Twig\AppExtension;
use Doctrine\ORM\Mapping\Entity;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use ServiceBundle\Services\ServiceCreatorService;
use ServiceBundle\Services\ServiceUpdater;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use AppBundle\Services\TrashRequests\Creator;

class ServiceRestService
{
    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;
    /** @var ReportStatusRepository */
    private $reportStatusRepository;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var ReportRepository */
    private $reportRepository;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var ContractorRepository */
    private $contractorRepository;
    /** @var ContractorTypeRepository */
    private $contractorTypeRepository;
    /** @var DeviceManager */
    private $deviceManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ContractorType */
    private $myCompanyType;
    /** @var Contractor */
    private $mainCompany;
    /** @var ContractorServiceRepository */
    private $contractorServiceRepository;
    /** @var DepartmentChannelManager */
    private $departmentChannelManager;
    /** @var OpportunityManager */
    private $opportunityManager;
    /** @var Serializer */
    private $serializer;
    /** @var ReportManager */
    private $reportManager;
    /** @var ServiceUpdater */
    private $serviceUpdater;
    /** @var Creator */
    private $trashRequestCreator;
    /** @var ServiceCreatorService */
    private $serviceCreator;

    /**
     * ServiceRestService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
        $this->deviceManager = $this->container->get("app.device.manager");
        $this->reportManager = $this->container->get("app.report.manager");
        $this->serializer = $container->get("jms_serializer");
        $this->serviceManager = $this->container->get("app.service.manager");
        $this->opportunityManager = $this->container->get("app.opportunity.manager");
        $this->departmentChannelManager = $this->container->get("app.department_channel.manager");
        $this->contractorUserRepository = $this->om->getRepository("AppBundle:ContractorUser");
        $this->reportStatusRepository = $this->om->getRepository("AppBundle:ReportStatus");
        $this->serviceRepository = $this->om->getRepository("AppBundle:Service");
        $this->reportRepository = $this->om->getRepository("AppBundle:Report");
        $this->deviceRepository = $this->om->getRepository("AppBundle:Device");
        $this->serviceNamedRepository = $this->om->getRepository("AppBundle:ServiceNamed");
        $this->contractorRepository = $this->om->getRepository("AppBundle:Contractor");
        $this->contractorTypeRepository = $this->om->getRepository("AppBundle:ContractorType");
        $this->contractorServiceRepository = $this->om->getRepository("AppBundle:ContractorService");
        $this->myCompanyType = $this->contractorTypeRepository->findOneBy(['alias' => 'my_company']);
        $this->mainCompany = $this->contractorRepository->findOneBy(['type' => $this->myCompanyType]);
        $this->serviceUpdater = $this->container->get("service.updater.service");
        $this->trashRequestCreator = $this->container->get('app.trash_requests.creator.service');
        $this->serviceCreator = $this->container->get('service.creator.service');
    }

    /**
     * @param $entity
     */
    public function save($entity)
    {
        $this->om->persist($entity);
        $this->om->flush();
    }

    /**
     * @param $params
     * @return array
     */
    public function addInspectionServiceToDevice($params)
    {
        $newInspection = new Service();
        /** @var Device $device */
        $device = $this->deviceRepository->find($params["deviceID"]);
        if (!$device) {
            return ["code" => 1, "message" => "Device not found"];
        }
        $account = $device->getAccount();

        $checkServicePattern= $this->serviceNamedRepository->find($params["serviceNameID"]);

        if (!$checkServicePattern) {
            return ["code" => 1, "message" => "There is no ServicePattern with such ID"];
        }

        $serviceNamed =
            $this->serviceNamedRepository->getServiceNamedByIdAndDevice($params["serviceNameID"], $device->getNamed());

        if (!$serviceNamed) {
            return ["code" => 1, "message" => "This ServicePattern can not be used for current device"];
        }

        $inspectionDue = new \DateTime();
        $inspectionDue->setTimestamp($params["inspectionDue"]);
        $newInspection->setNamed($serviceNamed);
        $newInspection->setAccount($account);
        $newInspection->setDevice($device);
        $newInspection->setInspectionDue($inspectionDue);

        if (isset($params["lastTested"])) {
            $lastTested = new \DateTime();
            $lastTested->setTimestamp($params["lastTested"]);
            $newInspection->setLastTested($lastTested);
        }

        if (isset($params["lastTester"])) {
            $contractor = $this->contractorRepository->find($params["lastTester"]);
            $newInspection->setCompanyLastTested($contractor);
        }

//        if (isset($params["fixedPrice"])) {$newInspection->setFixedPrice($params["fixedPrice"]);}
        if (isset($params["comment"])) {$newInspection->setComment($params["comment"]);}
        $this->serviceManager->checkAndCreateContractorService($newInspection);
        $newInspection->setDepartmentChannel($this->departmentChannelManager->getChannelByDivision($newInspection));

        $contractorService = $this->contractorServiceRepository->findOneBy([
            'named' => $newInspection->getNamed(),
            'contractor' => $this->mainCompany
        ]);

        $newInspection->setContractorService($contractorService);
        $this->serviceManager->create($newInspection);
        $this->serviceManager->attachOpportunity($newInspection);

        $serializeInspection = $this->serializer->toArray(
            $newInspection,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        return ["code" => 0, "message" => $serializeInspection];
    }


    /**
     * @param $params
     * @return array
     */
    public function updateInspectionService($params)
    {
        /** @var Service $serviceInspection */
        $serviceInspection = $this->serviceRepository->find($params["serviceID"]);
        if (!$serviceInspection) {
            return ["code" => 1, "message" => "There is no Inspection with such ID"];
        }

        if (($serviceInspection->getProposal()
                && $serviceInspection->getStatus()->getAlias() === $this->serviceManager::STATUS_UNDER_RETEST_NOTICE) ||
            ($serviceInspection->getWorkorder()
                && $serviceInspection->getStatus()->getAlias() === $this->serviceManager::STATUS_UNDER_WORKORDER)
        ) {
            return ["code" => 1, "message" => "You can't edit this Inspection"];
        }

        /** For Change Device */
        if (isset($params["deviceID"])) {
            /** @var Device $device */
            $device = $this->deviceRepository->find($params["deviceID"]);

            if (!$device) {
                return ["code" => 1, "message" => "There is no Device with such ID"];
            }

            $serviceNamed = $this->serviceNamedRepository->getServiceNamedByIdAndDevice(
                $serviceInspection->getNamed()->getId(),
                $device->getNamed()
            );

            if (!$serviceNamed) {
                return ["code" => 1, "message" => "You can't add this service to this device"];
            }
            $serviceInspection->setDevice($device);
        }


        if (isset($params["inspectionDue"])) {
            $inspectionDue = new \DateTime();
            $inspectionDue->setTimestamp($params["inspectionDue"]);
            $serviceInspection->setInspectionDue($inspectionDue);
        }

        if (isset($params["lastTester"])) {
            $contractor = $this->contractorRepository->find($params["lastTester"]);
            $serviceInspection->setCompanyLastTested($contractor);
        }

        if (isset($params["lastTested"])) {
            $lastTested = new \DateTime();
            $lastTested->setTimestamp($params["lastTested"]);
            $serviceInspection->setLastTested($lastTested);
        }

        if (isset($params["comment"])) {
            $serviceInspection->setComment($params["comment"]);
        }

        $this->serviceManager->checkAndCreateContractorService($serviceInspection);

        $this->serviceManager->update($serviceInspection);

        if (!empty($serviceInspection->getOpportunity())) {
            $this->serviceManager->resetOpportunity($serviceInspection);
        }
        $this->serviceManager->attachOpportunity($serviceInspection);

        $message = [
            "id" => (string)$serviceInspection->getId(),
            "updatedAt" => $serviceInspection->getTestDate()->getTimeStamp()
        ];
        
        return ["code" => 0, "message" => $message];
    }


    /**
     * @param $params
     * @param $route
     * @return array
     */
    public function addRepairServiceToDevice($params, $route)
    {
        $comment = isset($params["comment"]) ? $params["comment"] : null;
        /** @var Device $device */
        $device = $this->deviceRepository->find($params["deviceID"]);

        $checkOnError = $this->checkingForCreateRepairService($params, $route, $device);
        if ($checkOnError["code"]) {
            return $checkOnError;
        }

        /** @var ServiceNamed $serviceNamed */
        $serviceNamed = $this->serviceNamedRepository->getServiceNamedByIdAndDevice(
            $params["serviceNameID"],
            $device->getNamed(),
            true
        );

        /** @var Service $service */
        $service = $this->serviceManager->getDiscardAndResolvedService($device, $serviceNamed);

        if (empty($service)) {
            $service = $this->serviceCreator->createNonFrequencyByServiceName($device, $serviceNamed);
            $this->serviceManager->attachOpportunity($service, $this->opportunityManager::TYPE_REPAIR);
        } else {
            $this->serviceManager->restore($service);
        }

        $this->serviceUpdater->updateRepairServiceComment($service, $comment);

        $serializedServiceRepairs = $this->serializer->toArray(
            $service,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        return ["code" => 0, "message" => $serializedServiceRepairs];
    }

    /**
     * @param $reportId
     * @param UploadedFile $file
     * @return array
     */
    public function addFileToServicesReport($reportId, ?UploadedFile $file)
    {
        $report = $this->reportRepository->find($reportId);
        if (!$report) {
            return ["code" => 1, "message" => "Report not found"];
        }

        if (!$file) {
            return ["code" => 1, "message" => "Image not found. Please, select image and try again"];
        }

        $saveImagePath = "/uploads/reports";
        $filePath = $this->container->get('app.images')->saveUploadFile($file, $saveImagePath);

        $report->setFileReport($saveImagePath."/".$filePath);
        $this->reportManager->save($report);

        $message = [
            "id" => (string)$report->getId(),
            "updatedAt" => $report->getDateUpdate()->getTimestamp(),
            "pathFile" => $saveImagePath."/".$filePath
        ];

        return ["code" => 0, "message" => $message];
    }

    /**
     * @param $params
     * @param $route
     * @param $device
     * @return array
     */
    private function checkingForCreateRepairService($params, $route, $device)
    {
        if (!$device instanceof Device) {
            $message[] = "<b>Non-frequency service can not be create</b>.<br> There is no Device with such ID";
            $this->trashRequestCreator->create($route, json_encode($params), $message, null, $params["accessToken"]);

            return ["code" => 1, "message" => reset($message)];
        }

        /** @var ServiceNamed $checkServicePattern */
        $checkServicePattern = $this->serviceNamedRepository->find($params["serviceNameID"]);
        if (!$checkServicePattern instanceof ServiceNamed) {
            $message[] =
                "<b>Non-frequency service can not be create</b>.<br> There is no RepairServicePattern with such ID";
            $this->trashRequestCreator->create($route, json_encode($params), $message, null, $params["accessToken"]);

            return ["code" => 1, "message" => reset($message)];
        }

        /** @var ServiceNamed $serviceNamed */
        $serviceNamed = $this->serviceNamedRepository->getServiceNamedByIdAndDevice(
            $params["serviceNameID"],
            $device->getNamed(),
            true
        );
        if (!$serviceNamed instanceof ServiceNamed) {
            $message[] = "<b>Non-frequency service can not be create</b>.<br>
                        This RepairServicePattern can not be used for current device";
            $this->trashRequestCreator->create($route, json_encode($params), $message);

            return ["code" => 1, "message" => reset($message)];
        }

        /** @var Service $service */
        $service = $this->serviceRepository->findOneBy(["device" => $device, "named" => $serviceNamed]);
        if ($service instanceof Service) {
            $serviceStatusAlias = $service->getStatus()->getAlias();

            $message = null;

            if ($serviceStatusAlias == $this->serviceManager::STATUS_REPAIR_OPPORTUNITY) {
                $message[] =
                    "<b>Non-frequency service can not be create</b>.<br> This Repair Service under opportunity";
            }
            if ($serviceStatusAlias == $this->serviceManager::STATUS_UNDER_WORKORDER) {
                $message[] =
                    "<b>Non-frequency service can not be create</b>.<br> This Repair Service under workorder";
            }
            if ($serviceStatusAlias == $this->serviceManager::STATUS_UNDER_PROPOSAL) {
                $message[] =
                    "<b>Non-frequency service can not be create</b>.<br> This Repair Service under proposal";
            }

            if ($message) {
                $this->trashRequestCreator->create(
                    $route,
                    json_encode($params),
                    $message,
                    null,
                    $params["accessToken"]
                );
                return ["code" => 1, "message" => reset($message)];
            }
        }

        return ["code" => 0];
    }
}
