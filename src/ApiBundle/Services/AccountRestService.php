<?php

namespace ApiBundle\Services;

use InvoiceBundle\Creators\CustomerCreator;
use AppBundle\Creators\AddressCreator;
use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\ContactPersonHistoryManager;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Services\AccountContactPersonHistory\Creator;
use AppBundle\Twig\AppExtension;
use InvoiceBundle\Entity\Customer;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\AddressType;

class AccountRestService
{

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var AccountRepository */
    private $accountRepository;
    /** @var AppExtension */
    private $twigFilter;
    /** @var Serializer */
    private $serializer;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var StateRepository */
    private $stateRepository;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var Creator */
    private $accountContactPersonHistoryCreatorService;
    /** @var \AppBundle\Services\Address\Creator */
    private $addressCreatorService;
    /** @var ContactPersonHistoryManager */
    private $contactPersonHistoryManager;
    /** @var AddressCreator */
    private $addressCreator;
    /** @var CustomerCreator */
    private $customerCreator;

    /**
     * AccountRestService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
        $this->twigFilter = $container->get("app.twig_extension");
        $this->serializer = $container->get("jms_serializer");
        $this->accountContactPersonHistoryCreatorService =
            $this->container->get('app.account_contact_person_history_creator.service');
        $this->addressCreatorService = $this->container->get('app.address_creator.service');
        $this->contactPersonHistoryManager = $this->container->get('app.contact_person_history.manager');
        $this->addressCreator = $this->container->get('address.creator');
        $this->customerCreator = $this->container->get('customer.creator');

        $this->accountRepository = $this->om->getRepository("AppBundle:Account");
        $this->deviceRepository = $this->om->getRepository("AppBundle:Device");
        $this->stateRepository = $this->om->getRepository("AppBundle:State");
        $this->deviceCategoryRepository = $this->om->getRepository("AppBundle:DeviceCategory");
    }


    /**
     * @param $params
     * @return array
     */
    public function createACP($params)
    {

        if (!isset($params["accountID"])) {
            return ["code" => 1, "message" => "No account ID in request"];
        }
        /** @var Account $account */
        $account = $this->accountRepository->find($params["accountID"]);

        if (!$account) {
            return ["code" => 1, "message" => "There is no Account with such ID"];
        }

        if (!isset($params["name"])) {
            return ["code" => 1, "message" => "Please, enter new Acoount Contact Person First Name"];
        }
        /** @var ContactPerson $contactPerson */
        $contactPerson = $this->quickCreateContactPerson($params);
        /** @var Customer $customer */
        $customer = $this->quickCreateCustomer($contactPerson);

        $newACP = new AccountContactPerson();
        $newACP->setAccount($account);
        $newACP->setContactPerson($contactPerson);
        $newACP->setCustomer($customer);

        $newACP = $this->setRolesAndResposibilityForACP($params, $newACP);
        $newACP->setSendingAddress($contactPerson->getPersonalAddress());

        $this->om->persist($newACP);
        $this->om->flush();

        /** It is  SPIKE */
        $this->accountContactPersonHistoryCreatorService->create($newACP);
        $serializedACP = $this->serializer->toArray(
            $newACP,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        return ["code" => 0, "message" => $serializedACP];
    }


    /**
     * @param $params
     * @return ContactPerson
     */
    private function quickCreateContactPerson($params)
    {
        $newContactPerson = new ContactPerson();

        $newContactPerson->setFirstName($params["name"]);
        if (isset($params["lastName"])) {
            $newContactPerson->setLastName($params["lastName"]);
        }
        if (isset($params["contactTitle"])) {
            $newContactPerson->setTitle($params["contactTitle"]);
        }
        if (isset($params["email"])) {
            $newContactPerson->setEmail($params["email"]);
        }
        if (isset($params["phone"])) {
            $newContactPerson->setPhone($params["phone"]);
        }
        if (isset($params["cell"])) {
            $newContactPerson->setCell($params["cell"]);
        }
        if (isset($params["ext"])) {
            $newContactPerson->setExt($params["ext"]);
        }
        if (isset($params["fax"])) {
            $newContactPerson->setFax($params["fax"]);
        }
        if (isset($params["notes"])) {
            $newContactPerson->setNotes($params["notes"]);
        }
        if (isset($params["contactCodOnly"])) {
            $newContactPerson->setCod($params["contactCodOnly"]);
        }

//        if (isset($params["address"]) or
//            isset($params["city"]) or
//            isset($params["zip"]) or
//            isset($params["stateID"])
//        ) {
        $cpAddress = $this->addContactPersonAddress($params);
        $newContactPerson->setPersonalAddress($cpAddress);
//        }

        $this->om->persist($newContactPerson);

        $this->contactPersonHistoryManager->createContactPersonHistoryItem($newContactPerson, false);

        return $newContactPerson;
    }

    /**
     * @param $contactPerson
     * @return Customer
     */
    private function quickCreateCustomer($contactPerson)
    {
        /** @var Customer $customer */
        $customer = $this->customerCreator->make($contactPerson, $this->addContactPersonBillingAddress());
        $this->om->persist($customer);

        return $customer;
    }

    /**
     * @param $params
     * @return Address
     */
    private function addContactPersonAddress($params)
    {
        $cpAddress = $this->addressCreatorService->createWithPersonalType();

        if (isset($params["address"])) {
            $cpAddress->setAddress($params["address"]);
        }
        if (isset($params["city"])) {
            $cpAddress->setCity($params["city"]);
        }
        if (isset($params["zip"])) {
            $cpAddress->setZip($params["zip"]);
        }
        if (isset($params["stateID"])) {
            $state = $this->stateRepository->find($params["stateID"]);
            $cpAddress->setState($state);
        }

        $this->om->persist($cpAddress);

        $this->om->flush();

        return $cpAddress;
    }

    /**
     * @return Address
     */
    private function addContactPersonBillingAddress()
    {
        $cpBillingAddress = $this->addressCreator->make(AddressType::BILLING_ADDRESS_TYPE);

        $this->om->persist($cpBillingAddress);

        $this->om->flush();

        return $cpBillingAddress;
    }


    /**
     * @param $params
     * @param AccountContactPerson $accountContactPerson
     * @return AccountContactPerson
     */
    private function setRolesAndResposibilityForACP($params, AccountContactPerson $accountContactPerson)
    {
        if (isset($params["contactAuthorizer"])) {
            $accountContactPerson->setAuthorizer($params["contactAuthorizer"]);
        }
        if (isset($params["contactAccess"])) {
            $accountContactPerson->setAccess($params["contactAccess"]);
        }
        if (isset($params["contactAccessPrimary"])) {
            $accountContactPerson->setAccessPrimary($params["contactAccessPrimary"]);
        }
        if (isset($params["contactPayment"])) {
            $accountContactPerson->setPayment($params["contactPayment"]);
        }
        if (isset($params["contactPaymentPrimary"])) {
            $accountContactPerson->setPaymentPrimary($params["contactPaymentPrimary"]);
        }

        if (isset($params["contactResponsibility"]) and !empty($params["contactResponsibility"])) {
            foreach ($params["contactResponsibility"] as $responsibilityID) {
                /** @var DeviceCategory $responsibility */
                $responsibility = $this->deviceCategoryRepository->find($responsibilityID);
                if ($responsibility) {
                    $accountContactPerson->addResponsibility($responsibility);
                }
            }
        } else {
            $accountContactPerson->setAuthorizer(true);
        }

        return $accountContactPerson;
    }
}
