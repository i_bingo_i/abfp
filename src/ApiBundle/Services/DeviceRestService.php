<?php

namespace ApiBundle\Services;

use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DeviceStatus;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Report;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\DeviceStatusRepository;
use AppBundle\Entity\Repository\DynamicFieldDropboxChoicesRepository;
use AppBundle\Entity\Repository\DynamicFieldValueRepository;
use AppBundle\Entity\Repository\ReportRepository;
use AppBundle\Entity\Repository\ReportStatusRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Services\Images;
use AppBundle\Twig\AppExtension;
use Doctrine\ORM\Mapping\Entity;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;


class DeviceRestService
{

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var Serializer */
    private $serializer;
    /** @var Images */
    private $imageService;
    /** @var AccountRepository */
    private $accountRepository;
    /** @var DeviceNamedRepository */
    private $deviceNamedRepository;
    /** @var DynamicFieldDropboxChoicesRepository */
    private $dynamicFieldDropboxChoicesRepositry;
    /** @var DeviceManager */
    private $deviceManager;
    /** @var DynamicFieldValueRepository */
    private $dynamicFieldValueRepository;
    /** @var DeviceStatus */
    private $deviceStatusActive;
    /** @var DeviceStatusRepository */
    private $deviceStatusRepository;

//    /** @var MediaManager */
//    private $sonataMediaManager;


    /**
     * WorkorderRestService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
        $this->serializer = $container->get("jms_serializer");
        $this->deviceManager = $this->container->get("app.device.manager");
        $this->deviceRepository = $this->om->getRepository("AppBundle:Device");
        $this->deviceStatusRepository = $this->om->getRepository("AppBundle:DeviceStatus");
        $this->accountRepository = $this->om->getRepository("AppBundle:Account");
        $this->deviceNamedRepository = $this->om->getRepository("AppBundle:DeviceNamed");
        $this->dynamicFieldValueRepository = $this->om->getRepository("AppBundle:DynamicFieldValue");
        $this->dynamicFieldDropboxChoicesRepositry = $this->om->getRepository("AppBundle:DynamicFieldDropboxChoices");
//        $this->sonataMediaManager = $this->container->get("sonata.media.manager.media");
        $this->imageService = $this->container->get('app.images');
        $this->deviceStatusActive = $this->om->getRepository("AppBundle:DeviceStatus")->findOneBy([
            "alias" => "active"
        ]);
    }

    /**
     * @param $entity
     */
    public function save($entity)
    {
        $this->om->persist($entity);
        $this->om->flush();
    }


    /**
     * @param $deviceId
     * @param null|UploadedFile $file
     * @return array
     */
    public function uploadImageForDevice($deviceId, ?UploadedFile $file)
    {

        $device = $this->deviceRepository->find($deviceId);
        if (!$device) {
            return ["code" => 1, "message" => "Device with ID ".$deviceId." not found"];
        }

        if (!$file) {
            return ["code" => 1, "message" => "Image not found. Please, select image and try again"];
        }

        $saveImagePath = "/uploads/devices";
        $filePath = $this->imageService->saveUploadFile($file, $saveImagePath);

        $device->setPhoto($saveImagePath."/".$filePath);
        $this->deviceManager->save($device);

        $message = [
            "id" => (string)$device->getId(),
            "pathImage" => $saveImagePath."/".$filePath,
            "updatedAt" => $device->getDateUpdate()->getTimestamp()
        ];

        return ["code" => 0, "message" => $message];
    }


    /**
     * @param $params
     * @return array
     */
    public function addDeviceToAccount($params)
    {
        /** @var Device $device */
        $device = new Device();

        $account = $this->accountRepository->find($params["accountID"]);
        if (!$account) {
            return ["code" => 1, "message" => "Account not found"];
        }

        $deviceNamed = $this->deviceNamedRepository->find($params["devicePatternID"]);
        if (!$deviceNamed) {
            return ["code" => 1, "message" => "Device pattern not found"];
        }

        if (isset($params["parentID"]))
        {
            $parent = $this->deviceRepository->find($params["parentID"]);
            $device->setParent($parent);
        }

        $device->setStatus($this->deviceStatusActive);
        $device->setNamed($deviceNamed);
        $device->setAccount($account);
        if (isset($params["location"])) {$device->setLocation($params["location"]);}
        if (isset($params["comment"])) {$device->setComments($params["comment"]);}
        if (isset($params["note"])) {$device->setNoteToTester($params["note"]);}

        if (isset($params["fields"]) and !empty($params["fields"])) {
            $device = $this->addFieldsToDevice($params["fields"], $device);
        }

        $device->setTitle($this->deviceManager->getTitle($device));
        
        $this->deviceManager->create($device);

        $serializeDevice = $this->serializer->toArray(
            $device,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        return ["code" => 0, "message" => $serializeDevice];
    }


    /**
     * @param $params
     * @return array
     */
    public function updateDevice($params)
    {
        $deviceForUpdate = $this->deviceRepository->find($params["deviceID"]);

        if (!$deviceForUpdate) {
            return ["code" => 1, "message" => "Device not found"];
        }

        if (isset($params["location"])) {$deviceForUpdate->setLocation($params["location"]);}
        if (isset($params["comment"])) {$deviceForUpdate->setComments($params["comment"]);}
        if (isset($params["note"])) {$deviceForUpdate->setNoteToTester($params["note"]);}

        if (isset($params["fields"]) and !empty($params["fields"])) {
            $deviceForUpdate = $this->updateDevicesFields($params["fields"], $deviceForUpdate);
        }

        if (isset($params["statusAlias"])) {
            $status = $this->deviceStatusRepository->findOneBy(["alias" => $params["statusAlias"]]);
            $deviceForUpdate->setStatus($status);
        }
        
        $deviceForUpdate->setTitle($this->deviceManager->getTitle($deviceForUpdate));

        $this->deviceManager->save($deviceForUpdate);

        $message = [
            "id" => (string)$deviceForUpdate->getId(),
            "updatedAt" => $deviceForUpdate->getDateUpdate()->getTimestamp()
        ];

        return ["code" => 0, "message" => $message];
    }


    /**
     * @param $fieldsArray
     * @param Device $deviceForUpdate
     * @return Device
     */
    private function updateDevicesFields($fieldsArray, Device $deviceForUpdate)
    {
        foreach ($fieldsArray as $key => $value) {
            /** @var DynamicFieldValue $fieldValue */
            $fieldValue = $this->dynamicFieldValueRepository->find($key);
            /** @var DynamicField $dynamicField */
            $dynamicField = $fieldValue->getField();
            $fieldValue = $this->setFieldValue($dynamicField, $fieldValue, $value);
            $this->save($fieldValue);
        }

        return $deviceForUpdate;
    }

    /**
     * @param $fieldsArray
     * @param Device $device
     * @return Device
     */
    private function addFieldsToDevice($fieldsArray, Device $device)
    {
        /** @var DynamicField $fields */
        $fields = $device->getNamed()->getFields();
        foreach ($fields as $field) {
            $newValue = new DynamicFieldValue();
            $newValue->setDevice($device);
            $newValue->setField($field);
            $newValue = $this->addFieldValue($fieldsArray, $field, $newValue);
            $device->addField($newValue);
            $this->om->persist($newValue);
        }


        return $device;
    }


    /**
     * @param $fieldsArray
     * @param DynamicField $field
     * @param DynamicFieldValue $newValue
     * @return DynamicFieldValue
     */
    private function addFieldValue($fieldsArray, DynamicField $field, DynamicFieldValue $newValue)
    {
        if (isset($fieldsArray[$field->getId()])) {
            $newValue = $this->setFieldValue($field, $newValue, $fieldsArray[$field->getId()]);
        }

        return $newValue;
    }

    /**
     * @param DynamicField $field
     * @param DynamicFieldValue $newValue
     * @param $value
     * @return DynamicFieldValue
     */
    private function setFieldValue(DynamicField $field, DynamicFieldValue $newValue, $value)
    {
        if ($field->getType()->getAlias() == "text") {
            $newValue->setValue($value);
        } elseif ($field->getType()->getAlias() == "dropdown") {
            /** @var DynamicFieldDropboxChoices $dropboxChoice */
            $dropboxChoice = $this->dynamicFieldDropboxChoicesRepositry->findOneBy(
                [
                    "optionValue" => $value,
                    "field" => $field->getId()
                ]
            );
            if ($dropboxChoice) {
                $newValue->setOptionValue($dropboxChoice);
            }
        }

        return $newValue;
    }

    /**
     * @param $params
     * @return array
     */
    public function connectOrDisconnectAlarmPanel($params)
    {
        $device = $this->deviceRepository->find($params["deviceID"]);

        if (!$device) {
            return ["code" => 1, "message" => "No device found for id {$params["deviceID"]}"];
        }

        if (isset($params["alarmID"])) {
            /** @var DeviceNamed $deviceNameAlarmPanel */
            $deviceNameAlarmPanel = $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']);
            /** @var Device $alarmPanelId */
            $alarmPanel = $this->deviceRepository->findOneBy([
                "id" => $params["alarmID"],
                "named" => $deviceNameAlarmPanel
            ]);

            if (!$alarmPanel) {
                return ["code" => 1, "message" => "No alarm panel found with id {$params["alarmID"]}"];
            }

            $device->setAlarmPanel($alarmPanel);

        } else {
            $device->setAlarmPanel();
        }

        $this->deviceManager->save($device);


        return ["code" => 0, "message" => $device->getDateUpdate()->getTimestamp()];
    }


    /**
     * @param $deviceID
     * @return array
     */
    public function getDeviceById($deviceID)
    {
        $device = $this->deviceRepository->find($deviceID);
        if (!$device) {
            return ["code" => 1, "message" => "Device with ID ".$deviceID." not found"];
        }

        $serializeDevice = $this->serializer->toArray(
            $device,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        return ["code" => 0, "message" => $serializeDevice];
    }

    /**
     * @return array
     */
    public function getDevicePatterns()
    {
        $patterns = $this->deviceNamedRepository->findAll();
        if (!$patterns) {
            return ["code" => 1, "message" => "Device pattern not found"];
        }

        $serializeDevicePatterns = $this->serializer->toArray(
            $patterns,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        return ["code" => 0, "message" => $serializeDevicePatterns];

    }

}