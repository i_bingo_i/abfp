<?php

namespace ApiBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\Address;
use AppBundle\Entity\Contractor;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\DeviceStatus;
use AppBundle\Entity\DynamicFieldValidation;
use AppBundle\Entity\EntityManager\RepairDeviceInfoManager;
use AppBundle\Entity\EntityManager\WorkorderLogManager;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Event;
use AppBundle\Entity\PaymentMethod;
use AppBundle\Entity\PaymentTerm;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\ReportStatus;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\EventRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\Repository\WorkorderCommentRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceStatus;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderComment;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Entity\WorkorderStatus;
use AppBundle\Entity\WorkorderType;
use AppBundle\Services\Account\Authorizer;
use AppBundle\Services\CoordinateService;
use WorkorderBundle\Services\Creator;
use WorkorderBundle\Services\IncludeService;
use WorkorderBundle\Services\CommentService;
use WorkorderBundle\Services\WorkorderService;
use AppBundle\Twig\AppExtension;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Services\TrashRequests\Creator as TrashRequestsCreator;
use WorkorderBundle\Services\LogCreator;
use WorkorderBundle\Services\LogMessage;

class WorkOrderRestService
{
    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var WorkorderRepository */
    private $workorderRepository;
    /** @var AppExtension */
    private $twigFilter;
    /** @var Serializer */
    private $serializer;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;
    /** @var StateRepository */
    private $stateRepository;
    /** @var MunicipalityRepository */
    private $municipalityRepository;
    /** @var CompanyRepository */
    private $companyRepository;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var DeviceNamedRepository */
    private $deviceNamedRepository;
    /** @var DeviceStatus */
    private $deviceStatus;
    /** @var ServiceStatus */
    private $serviceStatus;
    /** @var ReportStatus */
    private $reportStatus;
    /** @var EventRepository */
    private $eventRepository;
    /** @var WorkorderStatus */
    private $workorderStatus;
    /** @var PaymentMethod */
    private $paymentMethod;
    /** @var PaymentTerm */
    private $paymentTerm;
    /** @var DynamicFieldValidation */
    private $validation;
    /** @var WorkorderManager */
    private $workorderManager;
    /** @var CoordinateService */
    private $coordinateService;
    /** @var Contractor */
    private $contractors;
    /** @var WorkorderType */
    private $workorderTypes;
    /** @var AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var Authorizer */
    private $accountAuthorizerService;
    /** @var MessageRepository */
    private $messageRepository;
    /** @var Creator */
    private $woCreatorService;
    /** @var RepairDeviceInfoManager */
    private $repairDeviceInfoManager;
    /** @var WorkorderLogManager  */
    private $workorderLogManager;
    /** @var UserRepository */
    private $userRepository;
    /** @var IncludeService */
    private $woIncludeService;
    /** @var CommentService */
    private $workorderCommentService;
    /** @var WorkorderCommentRepository */
    private $workorderCommentRepository;
    /** @var WorkorderService */
    private $workorderService;
    /** @var TrashRequestsCreator */
    private $trashRequestCreator;
    /** @var \AppBundle\Services\RepairDeviceInfo\Creator */
    private $repairDeviceInfoCreatorService;

    private $errorMessages = [];
    /**
     * WorkorderRestService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
        $this->twigFilter = $container->get("app.twig_extension");
        $this->serializer = $container->get("jms_serializer");
        $this->workorderManager = $this->container->get('app.work_order.manager');
        $this->coordinateService = $this->container->get('app.coordinate.service');
        $this->workorderRepository = $this->om->getRepository("AppBundle:Workorder");
        $this->deviceRepository = $this->om->getRepository("AppBundle:Device");
        $this->contractorUserRepository = $this->om->getRepository("AppBundle:ContractorUser");
        $this->stateRepository = $this->om->getRepository("AppBundle:State");
        $this->municipalityRepository = $this->om->getRepository("AppBundle:Municipality");
        $this->companyRepository = $this->om->getRepository("AppBundle:Company");
        $this->serviceNamedRepository = $this->om->getRepository("AppBundle:ServiceNamed");
        $this->serviceRepository = $this->om->getRepository("AppBundle:Service");
        $this->deviceNamedRepository = $this->om->getRepository("AppBundle:DeviceNamed");
        $this->accountContactPersonRepository = $this->om->getRepository("AppBundle:AccountContactPerson");
        $this->eventRepository = $this->om->getRepository("AppBundle:Event");
        $this->deviceStatus = $this->om->getRepository("AppBundle:DeviceStatus")->findAll();
        $this->serviceStatus = $this->om->getRepository("AppBundle:ServiceStatus")->findAll();
        $this->reportStatus = $this->om->getRepository("AppBundle:ReportStatus")->findAll();
        $this->workorderStatus = $this->om->getRepository("AppBundle:WorkorderStatus")->findAll();
        $this->paymentMethod = $this->om->getRepository("AppBundle:PaymentMethod")->findAll();
        $this->paymentTerm = $this->om->getRepository("AppBundle:PaymentTerm")->findAll();
        $this->contractors = $this->om->getRepository("AppBundle:Contractor")->findAll();
        $this->validation = $this->om->getRepository("AppBundle:DynamicFieldValidation")->findAll();
        $this->workorderTypes = $this->om->getRepository("AppBundle:WorkorderType")->findAll();
        $this->deviceCategoryRepository = $this->om->getRepository("AppBundle:DeviceCategory");
        $this->messageRepository = $this->om->getRepository("AppBundle:Message");
        $this->userRepository = $this->om->getRepository("AppBundle:User");
        $this->workorderCommentRepository = $this->om->getRepository("AppBundle:WorkorderComment");
        $this->accountAuthorizerService = $this->container->get('app.account_authorizer.service');
        $this->woCreatorService = $this->container->get('workorder.creator.service');
        $this->workorderLogManager = $this->container->get("app.workorder_log.manager");
        $this->woIncludeService = $this->container->get("workorder.include_service.service");
        $this->repairDeviceInfoManager = $this->container->get("app.repair_device_info.manager");
        $this->workorderCommentService = $this->container->get('workorder.comment.service');
        $this->workorderService = $this->container->get('workorder.workorder_service.service');
        $this->trashRequestCreator = $this->container->get('app.trash_requests.creator.service');
        $this->serviceHelper = $this->container->get('service.service_helper.service');
        $this->repairDeviceInfoCreatorService = $this->container->get('app.device_info_creator.service');
    }


    /**
     * @param $technicianId
     * @param $currentTimeStamp
     * @return array|bool|mixed
     */
    public function getWorkordersList($technicianId, $currentTimeStamp)
    {
        $workordersList = [];
        $workordersList["currentDate"] = time();
        $technician = $this->contractorUserRepository->find($technicianId);

        if (!$technician) {
            return false;
        } else {
            $workordersListAll = $this->workorderRepository->getWorkordersByTechnician($technician, $currentTimeStamp);
        }

        $serializedWorkorderListSerialize = $this->serializer->toArray(
            $workordersListAll,
            SerializationContext::create()->setGroups(array('workorder'))
        );

        $serializedWorkorderListSerialize = $this->sortingOutWorkorders($serializedWorkorderListSerialize);
        $workordersList["timezone"] = date_default_timezone_get();
        $workordersList = $this->addDevicePatternToWorkorderList($workordersList);
        $workordersList = $this->addContractorsToWorkorderList($workordersList);
        $workordersList = $this->addMunicipalitiesToWorkorderList($workordersList);
        $workordersList = $this->addStatesToWorkorderList($workordersList);
        $workordersList = $this->addReportStatusesToWorkorderList($workordersList);

        $serializedWorkorderList = $this->serializer->toArray(
            $workordersList,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        $serializedWorkorderList["orders"] = $this->filteringArray($serializedWorkorderListSerialize);

        return $serializedWorkorderList;
    }

    /**
     * @param $workorderId
     * @return array
     */
    public function getWorkorderEvents($workorderId)
    {
        /** @var Workorder $workorder */
        $workorder = $this->workorderRepository->find($workorderId);
        if (!$workorder) {
            return ["code" => 1, "message" => "Workorder not found"];
        }

        $events = $this->eventRepository->getEventsByWorkorder($workorder);
        if (!$events) {
            return ["code" => 1, "message" => "Events not found"];
        }

        $serializeEvents = $this->serializer->toArray(
            $events,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );

        return ["code" => 0, "message" => $serializeEvents];
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function uploadSignature(UploadedFile $file)
    {
        $saveImagePath = "/uploads/signatures";
        $filePath = $this->container->get('app.images')->saveUploadFile($file, $saveImagePath);

        return $saveImagePath.'/'.$filePath;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function uploadCommentFile(UploadedFile $file)
    {
        $saveImagePath = "/uploads/workorder_comments";
        $filePath = $this->container->get('app.images')->saveUploadFile($file, $saveImagePath);

        return $saveImagePath.'/'.$filePath;
    }

    /**
     * @param $params
     * @param null|UploadedFile $file
     * @return array
     */
    public function addSignatureToWO($params, ?UploadedFile $file)
    {
        /** @var Workorder $workorder */
        $workorder = $this->workorderRepository->find($params["orderID"]);

        if (!$workorder) {
            return ["code" => 1, "message" => "Workorder with id {$params["orderID"]} does not found"];
        }

        if (!$file) {
            return ["code" => 1, "message" => "Image not found. Please, select image and try again"];
        }

        if (isset($params["contactID"])) {
            $contactPerson = $this->accountContactPersonRepository->find($params["contactID"]);
            $workorder->setSignatureOwner($contactPerson);
        }

        $filePath = $this->uploadSignature($file);

        $workorder->setSignature($filePath);

        $this->workorderManager->save($workorder);

        $message = [
            "id" => $workorder->getId(),
            "pathImage" => $filePath,
            "updatedAt" => $workorder->getDateUpdate()->getTimestamp()
        ];

        return ["code" => 0, "message" => $message];
    }


    /**
     * @param $workorderID
     * @return array|mixed
     */
    public function getWorkordersInfoByDivision($workorderID)
    {
        /** @var Workorder $currentWorkorder */
        $currentWorkorder = $this->workorderRepository->find($workorderID);

        if (!$currentWorkorder) {
            return ["code" => 1, "message" => "Workorder with id {$workorderID} not found"];
        }
        /** @var DeviceCategory $division */
        $division = $this->deviceCategoryRepository->find($currentWorkorder->getDivision()->getId());

        $workordersList = $this->workorderRepository->getWorkordersForMap($division, $workorderID);

        $resultArray = [];
        /** @var Workorder $workorder */
        foreach ($workordersList as $workorder) {
            if (!empty($workorder->getAccount()->getCoordinate())) {
                $eventArray = [];
                $current = ((int)$workorder->getId() == (int)$workorderID) ? true : false;
//                $current = true;
                $events = $workorder->getEvents();
                if (!empty($events)) {
                    /** @var Event $event */
                    foreach ($events as $event) {
                        $eventArray[] = [
                            "userName" => $event->getUser()->getUser()->getFirstName()." ".($event->getUser()->getUser()->getLastName() ?? ""),
                            "dateAs" => $event->getFromDate()->format("m/d/Y"),
                            "timeFrom" => $event->getFromDate()->format("h:i A"),
                            "timeTo" => $event->getToDate()->format("h:i A")
                        ];
                    }
                }

                $resultArray[] = [
                    "id" => $workorder->getId(),
                    "current" => $current,
                    "events" => $eventArray,
                    "coordinates" => $workorder->getAccount()->getCoordinate(),
                    "accountAddress" => $this->addressFilter($workorder->getAccount()->getAddress())
                ];
            }
        }

        $serializedWorkorderList = $this->serializer->toArray(
            $resultArray,
            SerializationContext::create()->setGroups(array('full'))->enableMaxDepthChecks()
        );
        
        return [
            "code" => 0,
            "message" =>$serializedWorkorderList
        ];

    }

    /**
     * @param Address|null $address
     * @return string
     */
    public function addressFilter(Address $address = null)
    {
        $formatedAddress = '';
        if ($address) {
            if (!is_null($address->getAddress())) {
                $formatedAddress .= $address->getAddress();
                $formatedAddress .= ($address->getCity() or $address->getState() or $address->getZip()) ? ', ' : ' ';
            }

            if (!is_null($address->getCity())) {
                $formatedAddress .= $address->getCity();
                $formatedAddress .= ($address->getState() or $address->getZip()) ? ', ' : ' ';
            }

            $formatedAddress .= !is_null($address->getState()) ? $address->getState()->getCode() . ' ' : '';
            $formatedAddress .= !is_null($address->getZip()) ? $address->getZip() . ' ' : '';
        }
        return trim($formatedAddress);
    }


    /**
     * @param $params
     * @return array
     * @throws \Exception
     */
    public function reScheduleWorkorder($params)
    {
        /** @var Workorder $workorder */
        $workorder = $this->workorderRepository->find($params["orderID"]);

        if (!$workorder) {
            return ["code" => 1, "message" => "Workorder not found"];
        }

        $this->workorderManager->reschedule($workorder, $params['comment'], true);
        
        $this->om->persist($workorder);
        $this->om->flush();

        return ["code" => 0, "message" =>
            ["id" => (string)$workorder->getId(), "updatedAt" => $workorder->getDateUpdate()->getTimestamp()]];
    }

    /**
     * @param $params
     * @return array
     * @throws \Exception
     */
    public function addServiceToWorkorder($params)
    {
        /** @var Service $service */
        $service = $this->serviceRepository->find($params["serviceID"]);

        if (!$service) {
            $this->trashRequestCreator->create(
                $params["route"],
                json_encode($params),
                ["Service with id {$params["serviceID"]} not found"],
                null,
                $params["accessToken"]
            );

            return ["code" => 1, "message" => "Service with id {$params["serviceID"]} not found"];
        }

        /** @var Workorder $workorder */
        $workorder = $this->workorderRepository->find($params["orderID"]);

        if (!$workorder) {
            $this->trashRequestCreator->create(
                $params["route"],
                json_encode($params),
                ["Workorder with id {$params["orderID"]} not found"],
                null,
                $params["accessToken"]
            );

            return ["code" => 1, "message" => "Workorder with id {$params["orderID"]} not found"];
        }

        $comment = (isset($params["comment"])) ? $params["comment"] : '';

        /** @var Device $device */
        $device = $service->getDevice();
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $device->getNamed()->getCategory();
        /** @var Account $account */
        $account = $device->getAccount();
        /** @var Account $ownerAccount */
        $ownerAccount = $this->accountAuthorizerService->getAccountWithAuthorizerForDivision($account, $deviceCategory);
        $serviceStatusAlias = $service->getStatus()->getAlias();
        $correctServiceStatusesForAdd = ["under_retest_opportunity", "repair_opportunity", "omitted"];

        if (!in_array($serviceStatusAlias, $correctServiceStatusesForAdd)) {
            $this->trashRequestCreator->create(
                $params["route"],
                json_encode($params),
                [
                    "Service can not be added to Workorder. Target Service has incorrect status 
                    (most likely has been added to another document)
                    "
                ],
                null,
                $params["accessToken"]
            );

            return [
                "code" => 1,
                "message" => "Service can not be added to Workorder. Target Service has incorrect status 
                (most likely has been added to another document)"
            ];
        }

        $token = $params["accessToken"];

        if ($workorder->getIsFrozen()) {
            $this->trashRequestCreator->create(
                $params["route"],
                json_encode($params),
                ["Current workorder is not available to receive a new service"],
                null,
                $params["accessToken"]
            );

            return ["code" => 1, "message" => "Current workorder is not available to receive a new service"];
        }

        if ($workorder->getAccount()->getId() != $ownerAccount->getId()) {
            $this->trashRequestCreator->create(
                $params["route"],
                json_encode($params),
                ["Service can not be added to Workorder. Target Workorder is not available."],
                null,
                $params["accessToken"]
            );

            return [
                "code" => 1,
                "message" => "Service can not be added to Workorder. Target Workorder is not available."
            ];
        }

        // we must recreate the history of the service to update departmentChannel price
        $this->serviceHelper->updateServicesAndHistory([$service]);

        /** @var RepairDeviceInfo $repairDeviceInfo */
        $repairDeviceInfo = $this->repairDeviceInfoCreatorService->createInfoForInspectionsWithoutProposal(
            $device,
            [$service]
        );
        $workorder = $this->woIncludeService->addServices($device, [$service], $repairDeviceInfo, $workorder, true);

        /** @var LogMessage $logMessage */
        $logMessage = $this->container->get('workorder.log_message');
        /** Add Inspection  to Workorder*/
        if ($service->getNamed()->getFrequency()) {
            $message = $logMessage->makeByService($service);
            $this->repairDeviceInfoManager->remove($repairDeviceInfo);
        } else {
            $message = $logMessage->makeByRepairs([$service]);
        }

        // Create log record in WO log
        /** @var LogCreator $woLogCreator */
        $woLogCreator = $this->container->get('workorder.log_creator');
        $woLogCreator->createRecord($workorder, [
            'type' => WorkorderLogType::TYPE_SERVICE_WERE_ADDED,
            'message' => $message,
            'comment' => $comment,
            'token' => $token
        ]);

        $this->om->persist($service);
        $this->om->persist($workorder);
        $this->om->flush();

        return ["code" => 0, "message" =>
            [
                "id" => (string)$workorder->getId(),
                "updatedAt" => $workorder->getDateUpdate()->getTimestamp()
            ]
        ];
    }

    /**
     * @param $params
     * @return array
     */
    public function addWorkordersComment($params)
    {
        /** @var Workorder $workorder */
        $workorder = $this->workorderRepository->find($params["orderID"]);

        if (!$workorder) {
            return ["code" => 1, "message" => "Workorder not found"];
        }

        if (isset($params["comment"])) {
            /** @var WorkorderComment $workorderComment */
            $workorderComment = new WorkorderComment();
            $workorderComment->setComment($params["comment"]);
            $workorderComment = $this->workorderCommentService->createCommentFromIOS(
                $workorderComment,
                $workorder,
                $this->getUserByAccessToken($params["accessToken"])
            );
        }

        return ["code" => 0, "message" =>
            [
                "id" => (string)$workorderComment->getId(),
                "orderID" => (string)$workorder->getId(),
                "updatedAt" => $workorderComment->getDateUpdate()->getTimestamp()
            ]
        ];
    }


    /**
     * @param $params
     * @param UploadedFile $file
     * @return array
     */
    public function addWorkordersCommentFile($params, UploadedFile $file)
    {
        /** @var Workorder $workorder */
        $workorderComment = $this->workorderCommentRepository->find($params["commentID"]);

        if (!$workorderComment) {
            return ["code" => 1, "message" => "Workorder's comment not found"];
        }
        if (!$file) {
            return ["code" => 1, "message" => "Workorder's comment's file not found"];
        }

        $file = $this->uploadCommentFile($file);
        $workorderComment->setPhoto($file);

        $this->om->persist($workorderComment);
        $this->om->flush();

        return ["code" => 0, "message" =>
            [
                "id" => (string)$workorderComment->getId(),
                "pathImage" => $file,
                "updatedAt" => $workorderComment->getDateUpdate()->getTimestamp()
            ]
        ];
    }

    /** ****************************************************************************/

    /**
     * @param $workordersList
     * @return mixed
     */
    private function addStatesToWorkorderList($workordersList)
    {
        $workordersList["states"] = $this->stateRepository->findAll();

        return $workordersList;
    }

    /**
     * @param $workordersList
     * @return mixed
     */
    private function addMunicipalitiesToWorkorderList($workordersList)
    {
        $workordersList["municipalities"] = $this->municipalityRepository->findAll();

        return $workordersList;
    }

    /**
     * @param $workordersList
     * @return mixed
     */
    private function addContractorsToWorkorderList($workordersList)
    {
        $workordersList["contractors"] = $this->contractors;

        return $workordersList;
    }


    private function addDevicePatternToWorkorderList($workordersList)
    {
        $workordersList["devicePattern"]["deviceNamed"] = $this->deviceNamedRepository->findAll();
        $workordersList["devicePattern"]["status"] = $this->deviceStatus;

        return $workordersList;
    }

    /**
     * @param $workordersList
     * @return mixed
     */
    private function addReportStatusesToWorkorderList($workordersList)
    {
        $workordersList["status"]["reportStatus"] = $this->reportStatus;
        $workordersList["status"]["serviceStatuses"] = $this->serviceStatus;;
        $workordersList["status"]["workorderPaymentMethod"] = $this->paymentMethod;
        $workordersList["status"]["workorderStatus"] = $this->workorderStatus;
        $workordersList["status"]["paymentType"] = $this->paymentMethod;
        $workordersList["status"]["validation"] = $this->validation;
        $workordersList["status"]["workorderTypes"] = $this->workorderTypes;

        return $workordersList;
    }


    /** This is fucking KOSTIL. Don't beat me please */
    /**
     * @param $parentArray
     * @return mixed
     */
    private function filteringArray(&$parentArray)
    {
        foreach ($parentArray as $key => &$value)
        {
            if (gettype($value) == "array")
            {
                if ($this->checkDeleted($value)) {
                    /** TODO: remove this before next release */
//                    $parentArray=$this->customDeleteElement($parentArray, $key);
//                    unset($value);
//                    unset($parentArray[$key]);
                } else {
                    $value = $this->filteringArray($value);
//                    }
                }
            }
        }

        return $parentArray;
    }

    /**
     * @param $array
     * @return bool
     */
    private function checkDeleted($array)
    {
        foreach ($array as $cKey => $cValue) {
            if ($cKey === "deleted" and $cValue === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $accessToken
     * @return User|null|object
     */
    private function getUserByAccessToken($accessToken)
    {
        return $this->userRepository->findOneBy(["accessToken" => $accessToken]);
    }


    /**
     * @param $workordersArray
     * @return mixed
     */
    private function sortingOutWorkorders($workordersArray)
    {
        foreach ($workordersArray as &$workorder)
        {
            if (isset($workorder["comments"]) and !empty($workorder["comments"]))
            {
                $workorder = $this->filteringComments($workorder);
            }
        }

        return $workordersArray;
    }

    /**
     * @param array $workorder
     * @return array
     */
    private function filteringComments(array $workorder)
    {

        foreach($workorder["comments"] as $key => &$comment) {
            if((empty($comment["commentFile"]) and empty($comment["comment"])) or $comment["forAdminUsersOnly"]){
                $workorder["comments"][$key];
                unset($comment);
                unset($workorder["comments"][$key]);
                $workorder["comments"] = array_values($workorder["comments"]);
            }
        }

        return $workorder;
    }

    // TODO: DEPRECATED
    /**
     * @param $data
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function parseDataForFinishWO($data)
    {
        $parsedData = [];

        $parsedData["workorder"] = null;
        if (isset($data["orderID"])) {
            $parsedData["workorder"] = $this->workorderRepository->find($data["orderID"]);
        }

        $parsedData["finisher"] = null;
        if (isset($data["technicianID"])) {
            $parsedData["finisher"] = $this->contractorUserRepository->find($data["technicianID"]);
        }

        $parsedData["comment"] = null;
        if (isset($data["comment"])) {
            $parsedData["comment"] = $data["comment"];
        }

        $parsedData["lat"] = isset($data['lat']) ? trim($data['lat']) : 0;
        $parsedData["lng"] = isset($data['lng']) ? trim($data['lng']) : 0;
        $parsedData["coordinates"] = $this->coordinateService->getCoordinate(trim($parsedData['lat']), trim($parsedData['lng']));

        $parsedData["finishTime"] = new \DateTime();
        if (isset($data['finishTime'])) {
            $parsedData["finishTime"] = (new \DateTime())->setTimestamp($data['finishTime']);
        }

        return $parsedData;
    }

    /**
     * @param $parsedData
     */
    private function checkForFinishWO($parsedData)
    {
        $this->errorMessages = [];

        if (is_null($parsedData["workorder"])) {
            $this->errorMessages[] = "Workorder does not found";
        }

        if (is_null($parsedData["finisher"])) {
            $this->errorMessages[] = "Contractor User does not found. You can't finish workorder";
        }

        if (!empty($this->errorMessages)) {
            $this->errorMessages = array_merge(["<b>Workorder can not be finished</b>.<br>"], $this->errorMessages);
        }
    }
}
