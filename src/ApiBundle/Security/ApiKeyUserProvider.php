<?php

namespace ApiBundle\Security;

use AppBundle\Entity\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class ApiKeyUserProvider implements UserProviderInterface
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->userRepository = $objectManager->getRepository('AppBundle:User');
    }

    public function getUsernameForApiKey($apiKey)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['accessToken' => $apiKey]);

        if ($user) {
            return $user->getUsername();
        }

        return false;
    }

    public function loadUserByUsername($username)
    {
        return $this->userRepository->findOneBy([
            'username' => $username
        ]);
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
