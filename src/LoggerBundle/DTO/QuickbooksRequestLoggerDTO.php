<?php

namespace LoggerBundle\DTO;

use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\Job;

class QuickbooksRequestLoggerDTO
{
    private $entity;
    private $request;
    private $response;
    /** @var Workorder */
    private $workorder;
    private $user;
    /** @var bool */
    private $needToPositiveFlashMessage = true;
    /** @var bool  */
    private $needToNegativeFlashMessage = true;
    /** @var bool  */
    private $needToWritePositiveRecord = true;
    /** @var bool  */
    private $needToWriteNegativeRecord = true;

    /**
     * @param string $entity
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return isset($this->entity) ? $this->entity->getId() : 0;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        if ($this->getEntity() instanceof Invoices) {
            return 'invoice';
        }

        if ($this->getEntity() instanceof Workorder) {
            return 'workorder';
        }

        if ($this->getEntity() instanceof Company) {
            return 'company';
        }

        if ($this->getEntity() instanceof ContactPerson) {
            return 'contact_person';
        }

        if ($this->getEntity() instanceof Customer) {
            return 'customer';
        }

        if ($this->getEntity() instanceof Job) {
            return 'job';
        }

        return 'invoice';
    }

    /**
     * @param $request
     * @return $this
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param $response
     * @return $this
     */
    public function setResponse($response)
    {
        $json = json_encode($response);
        $this->response = json_decode($json, true);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Workorder|null $workorder
     * @return $this
     */
    public function setWorkorder(Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * @return Workorder|null
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @param $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param bool $isNeedFlash
     * @return $this
     */
    public function setNeedToPositiveFlashMessage($isNeedFlash = true)
    {
        $this->needToPositiveFlashMessage = $isNeedFlash;

        return $this;
    }

    /**
     * @return bool
     */
    public function getNeedToPositiveFlashMessage()
    {
        return $this->needToPositiveFlashMessage;
    }

    /**
     * @param bool $isNeedFlash
     * @return $this
     */
    public function setNeedToNegativeFlashMessage($isNeedFlash = true)
    {
        $this->needToNegativeFlashMessage = $isNeedFlash;

        return $this;
    }

    /**
     * @return bool
     */
    public function getNeedToNegativeFlashMessage()
    {
        return $this->needToNegativeFlashMessage;
    }

    /**
     * @param bool $isNeedToWrite
     * @return $this
     */
    public function setNeedToWritePositiveRecord($isNeedToWrite = true)
    {
        $this->needToWritePositiveRecord = $isNeedToWrite;

        return $this;
    }

    /**
     * @return bool
     */
    public function getNeedToWritePositiveRecord()
    {
        return $this->needToWritePositiveRecord;
    }

    /**
     * @param bool $isNeedToWrite
     * @return $this
     */
    public function setNeedToWriteNegativeRecord($isNeedToWrite = true)
    {
        $this->needToWriteNegativeRecord = $isNeedToWrite;

        return $this;
    }

    /**
     * @return bool
     */
    public function getNeedToWriteNegativeRecord()
    {
        return $this->needToWriteNegativeRecord;
    }
}
