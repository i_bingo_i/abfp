<?php

namespace LoggerBundle\Handlers;

use AppBundle\Entity\Logger;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\AbstractProcessingHandler;

class MonologDBHandler extends AbstractProcessingHandler
{
    private $entityManager;

    /**
     * MonologDBHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $record
     */
    protected function write(array $record)
    {
        $logEntry = new Logger();
        $logEntry->setMessage($record['message']);
        $logEntry->setLevel($record['level']);
        $logEntry->setLevelName($record['level_name']);
        $logEntry->setExtra($record['extra']);
        $logEntry->setContext($record['context']);
        $logEntry->setEntityId($this->getEntityIdFromContext($record));
        $logEntry->setEntityName($this->getEntityNameFromContext($record));
        $logEntry->setAction($this->getActionFromContext($record));
        $logEntry->setUserId($this->getUserIdFromContext($record));

        $this->entityManager->persist($logEntry);
        $this->entityManager->flush();
    }

    /**
     * @param array $record
     * @return null
     */
    protected function getEntityNameFromContext(array $record)
    {
        if (isset($record['context']['entity_name'])) {
            return $record['context']['entity_name'];
        }

        return null;
    }

    /**
     * @param array $record
     * @return null
     */
    protected function getEntityIdFromContext(array $record)
    {
        if (isset($record['context']['entity_id'])) {
            return $record['context']['entity_id'];
        }

        return null;
    }

    /**
     * @param array $record
     * @return null
     */
    protected function getActionFromContext(array $record)
    {
        if (isset($record['context']['action'])) {
            return $record['context']['action'];
        }

        return null;
    }

    /**
     * @param array $record
     * @return null
     */
    protected function getUserIdFromContext(array $record)
    {
        if (isset($record['context']['user_id'])) {
            return $record['context']['user_id'];
        }

        return null;
    }
}
