<?php

namespace LoggerBundle\Controller;

use AppBundle\Entity\EntityManager\LoggerManager;
use AppBundle\Entity\Repository\LoggerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class LoggerController extends Controller
{
    /**
     * @return Response
     */
    public function listAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var LoggerManager $loggerManager */
        $loggerManager = $this->get('app.logger.manager');
        /** @var LoggerRepository $loggerRepository */
        $loggerRepository = $objectManager->getRepository('AppBundle:Logger');
        $filters = $request->query->all();
        $page = $request->query->get('page');

        $logResults = $loggerRepository->findByFilterWithPagination($filters, $page);

        return $this->render('@Logger/list.html.twig', [
            'logResults' => $logResults,
            'entityNames' => $loggerManager::ENTITY_NAMES,
            'filters' => $filters
        ]);
    }

    /**
     * @return Response
     */
    public function quickbooksRequestListAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var LoggerRepository $loggerRepository */
        $loggerRepository = $objectManager->getRepository('AppBundle:Logger');
        $filters = $request->query->all();
        $page = $request->query->get('page');

        $logResults = $loggerRepository->findByFilterWithPaginationForQuickbooks($filters, $page);

        return $this->render('@Logger/quickbooks_request_list.html.twig', [
            'logResults' => $logResults,
            'filters' => $filters
        ]);
    }
}
