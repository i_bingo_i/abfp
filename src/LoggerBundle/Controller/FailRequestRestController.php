<?php

namespace LoggerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use LoggerBundle\Services\FailRequestLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class FailRequestRestController extends FOSRestController
{
    /**
     * Create log for fail request
     *
     * ### Response OK ###
     *     {
     *          "id": (string)"",
     *          "updatedAt": (int) 156676766
     *     }
     *
     * ### Response FAIL ###
     *     {
     *          "statusCode": (string) "601",
     *          "message": (string) "Log has not created"
     *     }
     *
     * @ApiDoc(
     *   section = "Logger",
     *   tags={
     *      "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Create log for fail request",
     *   headers={
     *      {
     *          "name"="X-ACCESS-TOKEN",
     *          "description"="Access token",
     *          "required"=true
     *      }
     *     },
     *   parameters={
     *       {"name"="failRequestData", "dataType"="text", "required"=true, "description"="Fail request data"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     401 = "API Key 'token key' does not exist."
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Post("/post-fail-request")
     * @return View
     */
    public function postFailRequestAction(Request $request)
    {
        /** @var FailRequestLogger $failRequestLogger */
        $failRequestLogger = $this->get('monolog.fail_request_logger.service');

        $params = $request->request->all();
        $failRequestData = $params['failRequestData'];
        $failRequestLogger->create($failRequestData);

        return $this->view(
            [
                'id' => (string) rand(1, 10),
                'updatedAt' => new \DateTime()
            ],
            Response::HTTP_OK
        );
    }
}
