<?php

namespace LoggerBundle\Services;

use AppBundle\Entity\Event;
use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class EventLogger
{
    const CREATE_EVENT = 'create_wo_event';
    const REMOVE_EVENT = 'remove_wo_event';

    const CREATE_MESSAGE = 'event has been added for';
    const REMOVE_MESSAGE = 'event has been removed for';

    /** @var ContainerInterface */
    private $container;
    /** @var Logger */
    private $monologLogger;
    /** @var TokenStorageInterface */
    private $securityTokenStorage;


    /**
     * EventLogger constructor.
     * @param ContainerInterface $container
     * @param TokenStorageInterface $securityTokenStorage
     */
    public function __construct(ContainerInterface $container, TokenStorageInterface $securityTokenStorage)
    {
        $this->container = $container;

        $this->monologLogger = $this->container->get('monolog.logger.db');
        $this->securityTokenStorage = $securityTokenStorage;
    }

    /**
     * @param Event $event
     * @param $response
     */
    public function create(Event $event, $response)
    {
        $message = $this->generateMessage($event, self::CREATE_MESSAGE);
        $this->writeRecord($event, self::CREATE_EVENT, $message, $response);
    }

    /**
     * @param Event $event
     * @param $response
     */
    public function remove(Event $event, $response = 'removed')
    {
        $message = $this->generateMessage($event, self::REMOVE_MESSAGE);
        $this->writeRecord($event, self::REMOVE_EVENT, $message, $response);
    }

    /**
     * @param Event $event
     * @param $action
     * @param $message
     * @param $response
     */
    private function writeRecord(Event $event, $action, $message, $response)
    {
        /** @var User $user */
        $user = $this->securityTokenStorage->getToken()->getUser();
        $workorderId = $event->getWorkorder()->getId();

        $this->monologLogger->info($message, [
            'entity_name' => 'workorder',
            'entity_id' => $workorderId,
            'action' => $action,
            'action_entity' => 'event',
            'action_entity_id' => $event->getId(),
            'user_id' => $user->getId(),
            'user_first_name' => $user->getFirstName(),
            'user_last_name' => $user->getLastName(),
            'response' => $response
        ]);
    }

    /**
     * @param Event $event
     * @param $message
     * @return string
     */
    private function generateMessage(Event $event, $message)
    {
        /** @var User $user */
        $user = $event->getUser()->getUser();
        $eventFromDate = $event->getFromDate()->format("m/d/Y");
        $eventFromTime = $event->getFromDate()->format("h:i A");
        $eventToTime = $event->getToDate()->format("h:i A");
        $message = $message . " " . $user->getFirstName() . " " . $user->getLastName() . "(#" . $user->getId() . ")";
        $message = $message . '<br>' . "for " . $eventFromDate . '<br>' . $eventFromTime . " - " . $eventToTime;

        return $message;
    }
}
