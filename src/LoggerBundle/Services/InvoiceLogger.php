<?php

namespace LoggerBundle\Services;

use InvoiceBundle\Entity\Invoices;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class InvoiceLogger extends BaseLogger
{
    private const ENTITY_NAME = 'invoices';

    /**
     * InvoiceLogger constructor.
     * @param Logger $logger
     * @param TokenStorageInterface $securityTokenStorage
     */
    public function __construct(Logger $logger, TokenStorageInterface $securityTokenStorage)
    {
        parent::__construct($logger, $securityTokenStorage);
    }

    /**
     * @param Invoices $entity
     * @param string $createMessage
     * @return string
     */
    protected function generateMessage($entity, $createMessage): string
    {
        $eventFromDate = $entity->getDateUpdate()->format("m/d/Y");
        $eventFromTime = $entity->getDateUpdate()->format("h:i A");
        $eventToTime = $event->getToDate()->format("h:i A");
        $message = $message . " " . $user->getFirstName() . " " . $user->getLastName() . "(#" . $user->getId() . ")";
        $message = $message . '<br>' . "for " . $eventFromDate . '<br>' . $eventFromTime . " - " . $eventToTime;

        return $message;
    }

    /**
     * @param Invoices $entity
     * @param string $action
     * @param string $message
     * @param $response
     */
    protected function writeRecord($entity, $action, $message, $response)
    {
        $this->monologLogger->info($message, [
            'entity_name' => self::ENTITY_NAME,
            'entity_id' => $entity->getId(),
            'action' => $action,
            'action_entity' => self::ENTITY_NAME,
            'action_entity_id' => $entity->getId(),
            'user_id' => $this->user->getId(),
            'user_first_name' => $this->user->getFirstName(),
            'user_last_name' => $this->user->getLastName(),
            'response' => $response
        ]);
    }
}