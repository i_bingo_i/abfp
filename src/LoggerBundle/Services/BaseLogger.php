<?php

namespace LoggerBundle\Services;

use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

abstract class BaseLogger
{
    /** @var Logger */
    protected $monologLogger;
    /** @var TokenStorageInterface */
    protected $securityTokenStorage;
    /** @var string */
    protected $createMessage;
    /** @var string */
    protected $createEvent;
    /** @var User */
    protected $user;

    /**
     * BaseLogger constructor.
     * @param Logger $logger
     * @param TokenStorageInterface $securityTokenStorage
     */
    public function __construct(Logger $logger, TokenStorageInterface $securityTokenStorage)
    {
        $this->monologLogger = $logger;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->user = $this->securityTokenStorage->getToken()->getUser();
    }

    /**
     * @param object $entity
     * @param $response
     */
    public function create($entity, $response)
    {
        $message = $this->generateMessage($entity, $this->createMessage);
        $this->writeRecord($entity, $this->createEvent, $message, $response);
    }

    /**
     * @param object $entity
     * @param $response
     */
    public function remove($entity, $response = 'removed')
    {
        $message = $this->generateMessage($entity, $this->createMessage);
        $this->writeRecord($entity, $this->createEvent, $message, $response);
    }

    /**
     * @param object $entity
     * @param string $createMessage
     * @return string
     */
    abstract protected function generateMessage($entity, $createMessage) : string;

    /**
     * @param object $entity
     * @param string $action
     * @param string $message
     * @param $response
     * @return mixed
     */
    abstract protected function writeRecord($entity, $action, $message, $response);

}