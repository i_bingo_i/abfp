<?php

namespace LoggerBundle\Services;

use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class FailRequestLogger
{
    const ENTITY_NAME = 'fail_request';
    const ENTITY_ID = 0;
    const ACTION = 'create_fail_request';

    /** @var ContainerInterface */
    private $container;
    /** @var Logger */
    private $monologLogger;
    /** @var TokenStorageInterface */
    private $securityTokenStorage;
    /** @var Session */
    private $session;

    /**
     * FailRequestLogger constructor.
     * @param ContainerInterface $container
     * @param TokenStorageInterface $securityTokenStorage
     */
    public function __construct(ContainerInterface $container, TokenStorageInterface $securityTokenStorage)
    {
        $this->container = $container;

        $this->monologLogger = $this->container->get('monolog.logger.db');
        $this->securityTokenStorage = $securityTokenStorage;
    }

    /**
     * @param $response
     */
    public function create($response)
    {
        $this->writeRecord($response);
    }

    /**
     * @param $response
     */
    private function writeRecord($response)
    {
        $user = null;

        $this->monologLogger->info($response, [
            'entity_name' => self::ENTITY_NAME,
            'entity_id' => self::ENTITY_ID,
            'action' => self::ACTION,
            'user_id' => ($user instanceof User) ? $user->getId() : null,
            'user_first_name' => ($user instanceof User) ? $user->getFirstName() : null,
            'user_last_name' => ($user instanceof User) ? $user->getLastName() : null
        ]);
    }
}
