<?php

namespace LoggerBundle\Services;

use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class QuickbooksRequestLogger
{
    public const SYNCHRONIZE_INVOICE = [
        'log' => 'QB Items synchronization',
        'positive_result' => 'QB Items names were synchronized',
        'negative_result' => 'QB Items names were not synchronized'
    ];
    public const UPDATE_COMPANY_CUSTOMER_QB = [
        'log' => 'Company Customer record update',
        'positive_result' => 'Customer record was updated in Quickbooks',
        'negative_result' => 'Customer record was NOT updated in Quickbooks'
    ];
    public const UPDATE_CONTACT_PERSON_CUSTOMER_QB = [
        'log' => 'Contact Person Customer record update.',
        'positive_result' => 'Customer record was updated in Quickbooks',
        'negative_result' => 'Customer record was NOT updated in Quickbooks'
    ];
    public const ADD_CUSTOMER_IN_QB = [
        'log' => 'Adding Contact Person Customer record to QB.',
        'positive_result' => 'Customer record was added to Quickbooks',
        'negative_result' => 'Customer record was NOT added to Quickbooks - BREAK SEQUENCE'
    ];
    public const CREATE_JOB_IN_QB = [
        'log' => 'Adding Job record to QB',
        'positive_result' => 'Job record was added to Quickbooks',
        'negative_result' => 'Job record was NOT added to Quickbooks - BREAK SEQUENCE'
    ];
    public const UPDATE_JOB_IN_QB = [
        'log' => 'QB Job record update.',
        'positive_result' => 'Job record was updated in Quickbooks',
        'negative_result' => 'Job record was NOT updated in Quickbooks - BREAK SEQUENCE'
    ];
    public const VOID_INVOICE_IN_QB = [
        'log' => 'Invoice voided in QB.',
        'positive_result' => 'Previous Workorder Invoice was voided in Quickbooks',
        'negative_result' => 'Previous Workorder Invoice was NOT voided in Quickbooks - BREAK SEQUENCE'
    ];
    public const SUBMIT_NEW_INVOICE_TO_QB = [
        'log' => 'New Invoice submitted to QB.',
        'positive_result' => 'Invoice was submitted to Quickbooks - END OF SEQUENCE',
        'negative_result' => 'Invoice was NOT submitted to Quickbooks - END OF SEQUENCE'
    ];
    public const SUBMIT_DRAFT_INVOICE_TO_QB = [
        'log' => 'Invoice Draft submitted to QB.',
        'positive_result' => 'Invoice was submitted to Quickbooks - END OF SEQUENCE',
        'negative_result' => 'Invoice was NOT submitted to Quickbooks - END OF SEQUENCE'
    ];
    public const REFRESH_INVOICE_IN_QB = [
        'log' => 'Refresh Invoice data from QB (manual request).',
        'positive_result' => 'Invoice information was refreshed from Quickbooks',
        'negative_result' => 'Invoice information was NOT refreshed from Quickbooks.'
    ];
    public const CHECK_PAYMENTS_IN_QB_BY_AHJ = [
        'log' => 'Check payments applied for Invoice in QB (automatic request).',
        'positive_result' => 'Invoice information was refreshed from Quickbooks',
        'negative_result' => 'Invoice information was NOT refreshed from Quickbooks.'
    ];
    public const CHECK_PAYMENTS_IN_QB_MANUALLY = [
        'log' => 'Check payments applied for Invoice in QB (manual request).',
        'positive_result' => 'Invoice information was refreshed from Quickbooks',
        'negative_result' => 'Invoice information was NOT refreshed from Quickbooks.'
    ];
    public const CHECK_PAYMENTS_IN_QB_MIDNIGHT_CHECK = [
        'log' => 'Check payments applied for Invoice in QB (midnight check request).',
        'positive_result' => 'Invoice information was refreshed from Quickbooks',
        'negative_result' => 'Invoice information was NOT refreshed from Quickbooks.'
    ];
    public const REFRESH_SEQUENCE_FOR_CUSTOMER = [
        'log' => 'Customer sequence was not updated',
        'positive_result' => 'Customer sequence was updated',
        'negative_result' => 'Customer sequence was not updated'
    ];
    public const REFRESH_SEQUENCE_FOR_JOB = [
        'log' => 'Job sequence was not updated',
        'positive_result' => 'Job sequence was updated',
        'negative_result' => 'Job sequence was not updated'
    ];

    public const WARNING_MESSAGE = 'Already up to date.';

    /** @var ContainerInterface */
    private $container;
    /** @var Logger */
    private $monologLogger;
    /** @var TokenStorageInterface */
    private $securityTokenStorage;
    /** @var Session */
    private $session;

    /**
     * EventLogger constructor.
     * @param ContainerInterface $container
     * @param TokenStorageInterface $securityTokenStorage
     */
    public function __construct(ContainerInterface $container, TokenStorageInterface $securityTokenStorage)
    {
        $this->container = $container;

        $this->monologLogger = $this->container->get('monolog.logger.db');
        $this->securityTokenStorage = $securityTokenStorage;
        $this->session = $this->container->get('session');
    }

    /**
     * @param QuickbooksRequestLoggerDTO $loggerDTO
     * @param $action
     */
    public function createPositiveRecord(QuickbooksRequestLoggerDTO $loggerDTO, $action)
    {
        $message = $action['log'];
        $positiveResult = $action['positive_result'];

        if ($loggerDTO->getNeedToWritePositiveRecord()) {
            $this->writeRecord($loggerDTO, $message, $positiveResult);
        }

        if ($loggerDTO->getNeedToPositiveFlashMessage()) {
            $this->session->getFlashBag()->add('success', $positiveResult);
        }
    }

    /**
     * @param QuickbooksRequestLoggerDTO $loggerDTO
     * @param $action
     */
    public function createNegativeRecord(QuickbooksRequestLoggerDTO $loggerDTO, $action)
    {
        $message = $action['log'];
        $negativeResult = $action['negative_result'];

        if ($loggerDTO->getNeedToWriteNegativeRecord()) {
            $this->writeRecord($loggerDTO, $message, null, $negativeResult);
        }

        if ($loggerDTO->getNeedToNegativeFlashMessage()) {
            $this->session->getFlashBag()->add('failed', $negativeResult);
        }
    }

    /**
     * @param QuickbooksRequestLoggerDTO $loggerDTO
     * @param $action
     */
    public function createWarningRecord(QuickbooksRequestLoggerDTO $loggerDTO, $action)
    {
        $message = $action['log'];

        $this->writeRecord($loggerDTO, $message, null, self::WARNING_MESSAGE);

        if ($loggerDTO->getNeedToNegativeFlashMessage()) {
            $this->session->getFlashBag()->add('warning', self::WARNING_MESSAGE);
        }
    }

    /**
     * @param QuickbooksRequestLoggerDTO $loggerDTO
     * @param $message
     * @param null $positiveResult
     * @param null $negativeResult
     */
    private function writeRecord(
        QuickbooksRequestLoggerDTO $loggerDTO,
        $message,
        $positiveResult = null,
        $negativeResult = null
    ) {
        /** @var User $user */
        $user = $loggerDTO->getUser();

        if (!$user instanceof User) {
            /** @var User $user */
            $user = $this->securityTokenStorage->getToken()->getUser();
        }

        if ($user instanceof User) {
            $userInfo = $user->getFirstName() . " " . $user->getLastName() . "(#" . $user->getId() . ")";
        } else {
            $userInfo = 'anon.';
        }

        $workorderId = $loggerDTO->getWorkorder() instanceof Workorder ? $loggerDTO->getWorkorder()->getId() : null;

        $this->monologLogger->info($message, [
            'user_info' => $userInfo,
            'wo_id' => $workorderId,
            'entity_name' => $loggerDTO->getEntityName(),
            'entity_id' => $loggerDTO->getEntityId(),
            'action' => $message,
            'action_entity' => $loggerDTO->getEntityName(),
            'action_entity_id' => $loggerDTO->getEntityId(),
            'user_id' => ($user instanceof User) ? $user->getId() : 1,
            'user_first_name' => ($user instanceof User) ? $user->getFirstName() : '-',
            'user_last_name' => ($user instanceof User) ? $user->getLastName() : '-',
            'response' => $loggerDTO->getResponse(),
            'request' => $loggerDTO->getRequest(),
            'negative_result' => $negativeResult,
            'positive_result' => $positiveResult,
            'quickbooks_request_logger' => true
        ]);
    }
}
