<?php

namespace InvoiceBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Services\Address\AddressProvider;
use InvoiceBundle\ActionManagers\CustomerActionManager;
use InvoiceBundle\ActionManagers\InvoiceActionManager;
use InvoiceBundle\ActionManagers\JobActionManager;
use InvoiceBundle\Creators\InvoiceCreator;
use InvoiceBundle\DataTransportObject\Requests\CreateInvoiceDTO;
use InvoiceBundle\Entity\Invoices;
use AppBundle\Entity\Workorder;
use InvoiceBundle\Parsers\Invoice\Request\CreateInvoiceParser;
use InvoiceBundle\Parsers\Invoice\Request\EditPaymentOverrideParser;
use InvoiceBundle\Services\Invoice\InvoiceProvider;
use InvoiceBundle\Services\Invoice\InvoiceSender;
use InvoiceBundle\Services\PaymentTerm\PaymentTermProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class InvoiceController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return Response|static
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createAction(Workorder $workorder, Request $request)
    {
        /** @var InvoiceCreator $invoiceCreator */
        $invoiceCreator = $this->get('invoice.creator');
        /** @var TokenStorage $tokenStorage */
        $tokenStorage = $this->get('security.token_storage');
        $invoice = $invoiceCreator->makeByWorkorder($workorder);
        $billAddressACP = AddressProvider::getBillToAddressByACP($invoice->getAccountContactPerson());

        if ($request->isMethod($request::METHOD_POST)) {
            /** @var InvoiceActionManager $invoiceActionManager */
            $invoiceActionManager = $this->get('invoice.invoice.action.manager');
            /** @var CreateInvoiceParser $invoiceParser */
            $invoiceParser = $this->get('invoice.create.parser');

            if ($invoiceActionManager->voidInvoice($workorder)) {
                /** @var CreateInvoiceDTO $createInvoiceDTO */
                $createInvoiceDTO = $invoiceParser->parse($request);

                $invoiceActionManager->save($createInvoiceDTO);
            }

            $redirectRoute = $request->getHost() . $this->generateUrl('admin_workorder_view', [
                'workorder' => $workorder->getId()
            ]);

            return Response::create($redirectRoute, Response::HTTP_OK);
        }

        /** @var PaymentTermProvider $paymentTermProvider */
        $paymentTermProvider = $this->get('payment-term.provider');
        /** @var InvoiceProvider $invoiceProvider */
        $invoiceProvider = $this->get('invoice.provider');
        /** @var array $paymentTerms */
        $paymentTerms = $paymentTermProvider->getAll();

        return $this->render('InvoiceBundle::create.html.twig', [
            'invoice' => $invoiceProvider->serializeInJSONInvoice($invoice),
            'terms' => $paymentTermProvider->serializeInJson($paymentTerms),
            'billAddressACP' => $billAddressACP,
            'accessToken' => $tokenStorage->getToken()->getUser()->getAccessToken()
        ]);
    }


    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return Response|static
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendDraftAction(Workorder $workorder, Request $request)
    {
        /** @var InvoiceActionManager $invoiceActionManager */
        $invoiceActionManager = $this->get('invoice.invoice.action.manager');
        /** @var TokenStorage $tokenStorage */
        $tokenStorage = $this->get('security.token_storage');
        /** @var Invoices $invoice */
        $invoice = $invoiceActionManager->getCurrentDraftInvoice($workorder);

        if ($request->isMethod($request::METHOD_POST)) {
            /** @var CreateInvoiceParser $invoiceParser */
            $invoiceParser = $this->get('invoice.create.parser');

            if ($invoiceActionManager->voidInvoice($workorder)) {
                /** @var CreateInvoiceDTO $createInvoiceDTO */
                $createInvoiceDTO = $invoiceParser->parse($request);

                $invoiceActionManager->save($createInvoiceDTO);
            }

            $redirectRoute = $request->getHost() . $this->generateUrl('admin_workorder_view', [
                    'workorder' => $workorder->getId()
                ]);

            return Response::create($redirectRoute, Response::HTTP_OK);
        }

        /** @var PaymentTermProvider $paymentTermProvider */
        $paymentTermProvider = $this->get('payment-term.provider');
        /** @var InvoiceProvider $invoiceProvider */
        $invoiceProvider = $this->get('invoice.provider');
        /** @var array $paymentTerms */
        $paymentTerms = $paymentTermProvider->getAll();
        /** @var Address $billAddressACP */
        $billAddressACP = AddressProvider::getBillToAddressByACP($invoice->getAccountContactPerson());

        return $this->render('InvoiceBundle::create.html.twig', [
            'invoice' => $invoiceProvider->serializeInJSONInvoice($invoice),
            'terms' => $paymentTermProvider->serializeInJson($paymentTerms),
            'billAddressACP' => $billAddressACP,
            'accessToken' => $tokenStorage->getToken()->getUser()->getAccessToken()
        ]);
    }

    /**
     * @param Invoices $invoice
     * @return Response
     */
    public function viewAction(Invoices $invoices)
    {
        /** @var InvoiceProvider $invoiceProvider */
        $invoiceProvider = $this->get('invoice.provider');

        $invoiceJson = $invoiceProvider->serializeInJSONInvoice($invoices);

        return $this->render('InvoiceBundle::view.html.twig', [
            'invoice' => $invoiceJson,
        ]);
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     *
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function saveAndSendToAccountingSystemAction(Workorder $workorder, Request $request)
    {
        /** @var CreateInvoiceParser $invoiceParser */
        $invoiceParser = $this->get('invoice.create.parser');
        /** @var InvoiceActionManager $invoiceActionManager */
        $invoiceActionManager = $this->get('invoice.invoice.action.manager');

        /** @var CreateInvoiceDTO $createInvoiceDTO */
        $createInvoiceDTO = $invoiceParser->parse($request);

        if ($invoiceActionManager->voidInvoice($workorder)) {
            $invoiceActionManager->saveAndSend($createInvoiceDTO);
        }

        /** @var string $redirectRoute */
        $redirectRoute = $request->getHost() . $this->generateUrl('admin_workorder_view', [
            'workorder' => $workorder->getId()
        ]);

        return Response::create($redirectRoute, Response::HTTP_OK);
    }

    /**
     * @param Invoices $invoice
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendToAccountingSystemAction(Invoices $invoice)
    {
        $this->sendingInvoiceProcess($invoice);

        return $this->redirectToRoute('invoice_view', ['invoice' => $invoice->getId()]);
    }

    /**
     * @param Workorder $workorder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function replaceAction(Workorder $workorder)
    {
        return $this->redirectToRoute('invoice_create', ['workorder' => $workorder->getId()]);
    }

    /**
     * @param Invoices $invoices
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function editPaymentsOverrideAction(Invoices $invoices, Request $request)
    {
        /** @var EditPaymentOverrideParser $invoiceParser */
        $invoiceParser = $this->get('invoice.edit_payment_override.parser');
        $editPaymentInvoiceDTO = $invoiceParser->parse($request);

        /** @var InvoiceActionManager $invoiceActionManager */
        $invoiceActionManager = $this->get('invoice.invoice.action.manager');
        $invoiceActionManager->editOverridePayment($editPaymentInvoiceDTO);

        return $this->redirectToRoute('invoice_view', ['invoices' => $invoices->getId()]);
    }

    /**
     * @param Invoices $invoice
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendingInvoiceProcess(Invoices $invoice)
    {
        /** @var CustomerActionManager $customerAccountingManager */
        $customerAccountingManager = $this->get('invoice.customer.action.manager');
        /** @var JobActionManager $jobAccountingManager */
        $jobAccountingManager = $this->get('invoice.job.action.manager');
        /** @var InvoiceSender $invoiceSender */
        $invoiceSender = $this->get('invoice.sender');

        $customerAccountingManager->sendToAccountingSystem($invoice->getJob()->getCustomer());

        $jobAccountingManager->sendJobToAccountingSystem($invoice);

        $invoiceSender->sendToAccountingSystem($invoice);
    }
}
