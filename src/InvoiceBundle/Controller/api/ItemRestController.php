<?php

namespace InvoiceBundle\Controller\api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use InvoiceBundle\Services\ItemProvider;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use QuickbooksBundle\Services\App\Item\Synchronizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Request;

class ItemRestController extends FOSRestController
{
    /**
     * Get Items
     *
     * ### Response OK ###
     *     {
     *           "id": "", (string)
     *           "name": "", (string)
     *           "description": "", (string)
     *           "type": { (object)
     *              "name": "" (string)
     *           },
     *           "rate": 0, (integer)
     *           "rateType": { (object)
     *              "name": "" (string)
     *           }
     *     },
     *     {
     *           "id": "", (string)
     *           "name": "", (string)
     *           "description": "", (string)
     *           "type": { (object)
     *              "name": "" (string)
     *           },
     *           "rate": 0, (integer)
     *           "rateType": { (object)
     *              "name": "" (string)
     *           }
     *     }
     *
     * @ApiDoc(
     *   section = "Item",
     *   tags={
     *     "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Get Items",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @param Request $request
     *
     * @Rest\Get("/item")
     * @Rest\View(serializerGroups={"full"})
     * @return View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItemsAction(Request $request)
    {
        /** @var ItemProvider $itemProvider */
        $itemProvider = $this->get('invoice.item_provider');
        /** @var Synchronizer $synchronizer */
        $synchronizer = $this->get('quickbooks.item.synchronizer');
        $accessToken = $request->headers->get("accesstoken");

        $synchronizedItems = $synchronizer->synchronizeItems($accessToken);

        return $this->view(
            $itemProvider->getAllItems(),
            $synchronizedItems ? Response::HTTP_OK : Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}
