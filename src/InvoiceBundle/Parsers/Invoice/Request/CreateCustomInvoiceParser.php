<?php

namespace InvoiceBundle\Parsers\Invoice\Request;

use AppBundle\Entity\Workorder;
use InvoiceBundle\DataTransportObject\Requests\CreateCustomInvoiceDTO;
use Symfony\Component\HttpFoundation\Request;

class CreateCustomInvoiceParser
{
    /**
     * @param Request $request
     * @return CreateCustomInvoiceDTO
     */
    public function parse(Request $request)
    {
        /** @var Workorder $workorder */
        $workorder = $request->attributes->get('workorder');

        $invoicePdf = null;
        if (isset($request->files->get('form')['image'])) {
            $invoicePdf = $request->files->get('form')['image'];
        }

        $comment = null;
        if (isset($request->request->get('form')['comment'])) {
            $comment = isset($request->request->get('form')['comment']);
        }

        /** @var CreateCustomInvoiceDTO $customInvoiceDto */
        $customInvoiceDto = new CreateCustomInvoiceDTO();
        $customInvoiceDto->setWorkorder($workorder);
        $customInvoiceDto->setFile($invoicePdf);
        $customInvoiceDto->setComment($comment);

        return $customInvoiceDto;
    }
}
