<?php

namespace InvoiceBundle\Parsers\Invoice\Request;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\PaymentTerm;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\PaymentTermRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\State;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use InvoiceBundle\DataTransportObject\Requests\CreateInvoiceDTO;
use InvoiceBundle\Entity\InvoiceLine;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Repository\InvoiceLineRepository;
use InvoiceBundle\Repository\InvoicesRepository;
use InvoiceBundle\Services\Invoice\InvoiceProvider;
use InvoiceBundle\Services\InvoiceLine\InvoiceLineManager;
use InvoiceBundle\Services\InvoiceStatus\InvoiceStatusProvider;
use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Repository\ItemRepository;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\AccountContactPerson\Authorizer;
use InvoiceBundle\Provider\JobProvider;
use AppBundle\Creators\AddressCreator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CreateInvoiceParser
{
    /** @var InvoiceProvider */
    private $invoiceProvider;
    /** @var StateRepository */
    private $stateRepository;
    /** @var PaymentTermRepository */
    private $paymentTermRepository;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var ItemRepository */
    private $itemRepository;
    /** @var InvoiceStatusProvider */
    private $invoiceStatusProvider;
    /** @var Authorizer */
    private $authorizer;
    /** @var JobProvider */
    private $jobProvider;
    /** @var AddressCreator */
    private $addressCreator;
    /** @var User */
    private $currentUser;
    /** @var InvoicesRepository */
    private $invoiceRepository;
    /** @var InvoiceLineRepository */
    private $invoiceLineRepository;
    /** @var InvoiceLineManager */
    private $invoiceLineManager;

    /**
     * CreateInvoiceParser constructor.
     * @param InvoiceProvider $invoiceProvider
     * @param StateRepository $stateRepository
     * @param PaymentTermRepository $paymentTermRepository
     * @param AddressTypeRepository $addressTypeRepository
     * @param ItemRepository $itemRepository
     * @param InvoiceStatusProvider $invoiceStatusProvider
     * @param Authorizer $authorizer
     * @param JobProvider $jobProvider
     * @param AddressCreator $addressCreator
     * @param TokenStorageInterface $tokenStorage
     * @param InvoicesRepository $invoicesRepository
     * @param InvoiceLineRepository $invoiceLineRepository
     * @param InvoiceLineManager $invoiceLineManager
     */
    public function __construct(
        InvoiceProvider $invoiceProvider,
        StateRepository $stateRepository,
        PaymentTermRepository $paymentTermRepository,
        AddressTypeRepository $addressTypeRepository,
        ItemRepository $itemRepository,
        InvoiceStatusProvider $invoiceStatusProvider,
        Authorizer $authorizer,
        JobProvider $jobProvider,
        AddressCreator $addressCreator,
        TokenStorageInterface $tokenStorage,
        InvoicesRepository $invoicesRepository,
        InvoiceLineRepository $invoiceLineRepository,
        InvoiceLineManager $invoiceLineManager
    ) {
        $this->invoiceProvider = $invoiceProvider;
        $this->stateRepository = $stateRepository;
        $this->paymentTermRepository = $paymentTermRepository;
        $this->addressTypeRepository = $addressTypeRepository;
        $this->itemRepository = $itemRepository;
        $this->invoiceStatusProvider = $invoiceStatusProvider;
        $this->invoiceRepository = $invoicesRepository;
        $this->invoiceLineRepository = $invoiceLineRepository;
        $this->invoiceLineManager = $invoiceLineManager;

        $this->authorizer = $authorizer;
        $this->jobProvider = $jobProvider;
        $this->addressCreator = $addressCreator;

        $systemToken = $tokenStorage->getToken();
        if ($systemToken) {
            $this->currentUser = $systemToken->getUser();
        }
    }

    /**
     * @param Request $request
     *
     * @return CreateInvoiceDTO
     * @throws \Exception
     */
    public function parse(Request $request)
    {
        /** @var string $invoiceJson */
        $invoiceJson = $request->getContent();

        $invoice = $this->invoiceProvider->deserializing($invoiceJson);
        $invoice->setStatus($this->invoiceStatusProvider->getDraft());

        $this->parseBillingAddress($invoice);
        $this->parsePaymentTerm($invoice);

        $pono = $invoice->getWorkorder()->getPoNo() ?? null;
        /** @var Workorder $workorder */
        $workorder = $request->get('workorder');

        if ($workorder instanceof Workorder) {
            $workorder->setPoNo($pono);
            $invoice->setWorkorder($workorder);

            /** @var Account $account */
            $account = $workorder->getAccount();
            /** @var Address $shipToAddress */
            $shipToAddress = $this->addressCreator->makeShipToByAccount($account);
            $invoice->setShipToAddress($shipToAddress);
        }

        /** @var AccountContactPerson $acp */
        $acp = $this->authorizer->getPrimaryPaymentByAccountForDivisions($workorder);
        if (!empty($acp)) {
            $invoice->setAccountContactPerson($acp);
            $invoice->setCustomer($acp->getCustomer());
        }

        if (!empty($acp) and $workorder instanceof Workorder) {
            /** @var Job $job */
            $job = $this->jobProvider->getJob($workorder->getAccount(), $acp->getCustomer(), $workorder->getDivision());
            $invoice->setJob($job);
        }

        if (empty($invoice->getInvoiceDate())) {
            $invoice->setInvoiceDate(new \DateTime());
        }

        if ($this->currentUser) {
            $invoice->setUser($this->currentUser);
        }

        if ($request->get('id')) {
            /** @var Invoices $existingInvoice */
            $existingInvoice = $this->invoiceRepository->find($request->get('id'));

            /** @var Invoices $invoice */
            $invoice = $this->updateExitingInvoice($existingInvoice, $invoice);
        }

        $useCustomBillAddress = $request->get('useCustomBillAddress');
        $invoice->setUseCustomBillAddress($useCustomBillAddress);

        $this->parseInvoiceLines($invoice);

        $createInvoiceDTO = new CreateInvoiceDTO();
        $createInvoiceDTO
            ->setWorkorder($workorder)
            ->setInvoice($invoice);

        return $createInvoiceDTO;
    }

    /**
     * @param Invoices $deserializedRequest
     */
    private function parseBillingAddress(Invoices $deserializedRequest)
    {
        if ($deserializedRequest->getBillToAddress()) {
            /** @var State $state */
            $state = $this->stateRepository->findOneBy(
                ['code' => $deserializedRequest->getBillToAddress()->getState()->getCode()]
            );
            /** @var AddressType $addressType */
            $addressType = $this->addressTypeRepository->findOneBy(['alias' => AddressType::BILLING_ADDRESS_TYPE]);

            $deserializedRequest->getBillToAddress()->setAddressType($addressType);
            $deserializedRequest->getBillToAddress()->setState($state);
        }
    }

    /**
     * @param Invoices $deserializedRequest
     */
    private function parsePaymentTerm(Invoices $deserializedRequest)
    {
        if ($deserializedRequest->getPaymentTerm()) {
            /** @var PaymentTerm $paymentTerm */
            $paymentTerm = $this->paymentTermRepository->findOneBy(
                ['alias' => $deserializedRequest->getPaymentTerm()->getAlias()]
            );

            $deserializedRequest->setPaymentTerm($paymentTerm);
        }
    }

    /**
     * @param Invoices $deserializedRequest
     */
    private function parseInvoiceLines(Invoices $deserializedRequest)
    {
        if ($deserializedRequest->getLines()) {
            /** @var InvoiceLine $line */
            foreach ($deserializedRequest->getLines() as $line) {
                /** @var Item $item */
                $item = $this->itemRepository->findOneBy(['name' => $line->getItem()->getName()]);

                $line->setItem($item);
                $line->setInvoice($deserializedRequest);

                if ($line->getItem()->getType()->getAlias() == ItemType::TYPE_DISCOUNT) {
                    $line->setQuantity();
                }
            }
        }
    }

    /**
     * @param Invoices $existingInvoice
     * @param Invoices $deserializedInvoice
     * @return Invoices
     */
    private function updateExitingInvoice(Invoices $existingInvoice, Invoices $deserializedInvoice)
    {
        $existingInvoice->setBillToAddress($deserializedInvoice->getBillToAddress());
        $existingInvoice->setPaymentTerm($deserializedInvoice->getPaymentTerm());
        $existingInvoice->setMemo($deserializedInvoice->getMemo());
        $existingInvoice->setTotal($deserializedInvoice->getTotal());
        $existingInvoice->setPaymentApplied($deserializedInvoice->getPaymentApplied());
        $existingInvoice->setOverridePayment($deserializedInvoice->getOverridePayment());
        $existingInvoice->setBalanceDue($deserializedInvoice->getBalanceDue());
        $existingInvoice->setInspector($deserializedInvoice->getInspector());
        $existingInvoice->setInvoiceDate($deserializedInvoice->getInvoiceDate());

        $this->invoiceLineManager->deleteAllLinesInInvoice($existingInvoice);
        $existingInvoice->setLines($deserializedInvoice->getLines());

        return $existingInvoice;
    }
}
