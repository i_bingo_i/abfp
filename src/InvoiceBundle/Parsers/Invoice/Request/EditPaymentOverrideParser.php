<?php

namespace InvoiceBundle\Parsers\Invoice\Request;

use InvoiceBundle\DataTransportObject\Requests\EditPaymentInvoiceDTO;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Services\Invoice\InvoiceProvider;
use Symfony\Component\HttpFoundation\Request;

class EditPaymentOverrideParser
{
    const OVERRIDE_PAYMENT_FRONTEND_NAME = 'newPayment';

    /** @var InvoiceProvider */
    private $invoiceProvider;

    /**
     * EditPaymentOverrideParser constructor.
     * @param InvoiceProvider $invoiceProvider
     */
    public function __construct(InvoiceProvider $invoiceProvider)
    {
        $this->invoiceProvider = $invoiceProvider;
    }

    /**
     * @param Request $request
     *
     * @return EditPaymentInvoiceDTO
     */
    public function parse(Request $request)
    {
        /** @var EditPaymentInvoiceDTO $editPaymentInvoiceDTO */
        $editPaymentInvoiceDTO = new EditPaymentInvoiceDTO();

        $overridePayment = $request->get(self::OVERRIDE_PAYMENT_FRONTEND_NAME);
        $editPaymentInvoiceDTO->setOverridePayment($overridePayment);

        /** @var Invoices $invoice */
        $invoice = $request->get('invoices');
        $editPaymentInvoiceDTO->setInvoice($invoice);

        return $editPaymentInvoiceDTO;
    }
}
