<?php

namespace InvoiceBundle\Creators;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use InvoiceBundle\Builders\InvoiceBuilder;
use InvoiceBundle\Entity\Invoices;
use AppBundle\Services\AccountContactPerson\Authorizer;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Provider\JobProvider;
use InvoiceBundle\Services\ItemTransformers\ItemTransformer;
use AppBundle\Creators\AddressCreator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class InvoiceCreator
{
    /** @var Authorizer */
    private $authorizer;
    /** @var JobProvider */
    private $jobProvider;
    /** @var AddressCreator */
    private $addressCreator;
    /** @var User */
    private $currentUser;
    /** @var ItemTransformer */
    private $itemTransformer;

    public function __construct(
        Authorizer $authorizer,
        JobProvider $jobProvider,
        AddressCreator $addressCreator,
        TokenStorageInterface $tokenStorage,
        ItemTransformer $itemTransformer
    ) {
        $this->authorizer = $authorizer;
        $this->jobProvider = $jobProvider;
        $this->addressCreator = $addressCreator;
        $this->itemTransformer = $itemTransformer;

        $systemToken = $tokenStorage->getToken();
        if ($systemToken) {
            $this->currentUser = $systemToken->getUser();
        }
    }

    /**
     * @param Workorder $workorder
     *
     * @return Invoices
     */
    public function makeByWorkorder(Workorder $workorder)
    {
        $invoiceLines = $this->itemTransformer->transform($workorder);

        /** @var InvoiceBuilder $invoiceBuilder */
        $invoiceBuilder = new InvoiceBuilder();
        $invoiceBuilder->setWorkorder($workorder);
        $invoiceBuilder->setInspectorByWo();

        if ($this->currentUser) {
            $invoiceBuilder->setUser($this->currentUser);
        }

        /** @var AccountContactPerson $acp */
        $acp = $this->authorizer->getPrimaryPaymentByAccountForDivisions($workorder);
        if (!empty($acp)) {
            $invoiceBuilder->setAcp($acp);
            $invoiceBuilder->setBillToAddressByAcp();
        }

        /** @var Account $account */
        $account = $workorder->getAccount();
        if (!empty($acp)) {
            /** @var Job $job */
            $job = $this->jobProvider->getJob($account, $acp->getCustomer(), $workorder->getDivision());
            $invoiceBuilder->setJob($job);
        }

        if ($invoiceLines) {
            $invoiceBuilder->setLines($invoiceLines);
        }

        /** @var Address $shipToAddress */
        $shipToAddress = $this->addressCreator->makeShipToByAccount($account);
        $invoiceBuilder->setShipToAddress($shipToAddress);

        return $invoiceBuilder->getInvoice();
    }
}
