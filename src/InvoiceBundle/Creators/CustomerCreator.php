<?php

namespace InvoiceBundle\Creators;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\EntityType;
use InvoiceBundle\Repository\EntityTypeRepository;
use Doctrine\ORM\EntityManager;
use Interfaces\IdInterface;

class CustomerCreator
{
    /** @var EntityManager */
    private $entityManager;
    /** @var EntityTypeRepository */
    private $entityTypeRepository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityTypeRepository = $entityManager->getRepository('InvoiceBundle:EntityType');
    }

    /**
     * @param IdInterface $entity
     * @param Address $billingAddress
     *
     * @return Customer
     */
    public function make(IdInterface $entity, Address $billingAddress)
    {
        /** @var Customer $customer */
        $customer = new Customer();
        $customer->setBillingAddress($billingAddress);
        $customer->setEntityId($entity->getId());

        /** @var Company $entity */
        if ($entity instanceof Company) {
            /** @var EntityType $typeCompany */
            $typeCompany = $this->entityTypeRepository->findOneBy(['alias' => EntityType::TYPE_COMPANY]);
            $customer->setEntityType($typeCompany);
            $customer->setName($entity->getName());
        }

        /** @var ContactPerson $entity */
        if ($entity instanceof ContactPerson) {
            /** @var EntityType $typeContactPerson */
            $typeContactPerson = $this->entityTypeRepository->findOneBy(['alias' => EntityType::TYPE_CONTACT_PERSON]);
            $customer->setEntityType($typeContactPerson);
            $customer->setName($entity->getFirstName() . ' ' . $entity->getLastName());
        }

        return $customer;
    }
}
