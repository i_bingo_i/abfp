<?php

namespace InvoiceBundle\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use QuickbooksBundle\Entity\ItemRateType;

class LoadItemRateTypeData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $percent = new ItemRateType();
        $percent->setName('Percent');
        $percent->setAlias('percent');

        $manager->persist($percent);

        $float = new ItemRateType();
        $float->setName('Float');
        $float->setAlias('float');

        $manager->persist($float);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}