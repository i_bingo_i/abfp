<?php

namespace InvoiceBundle\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use InvoiceBundle\Entity\InvoiceStatus;

class LoadInvoiceStatusData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $draft = new InvoiceStatus();
        $draft->setName('Draft');
        $draft->setAlias('draft');

        $manager->persist($draft);

        $inQB = new InvoiceStatus();
        $inQB->setName('In QB');
        $inQB->setAlias('in_qb');

        $manager->persist($inQB);

        $custom = new InvoiceStatus();
        $custom->setName('Custom');
        $custom->setAlias('custom');

        $manager->persist($custom);

        $voided = new InvoiceStatus();
        $voided->setName('Voided');
        $voided->setAlias('voided');

        $manager->persist($voided);

        $deleted = new InvoiceStatus();
        $deleted->setName('Deleted');
        $deleted->setAlias('deleted');

        $manager->persist($deleted);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}