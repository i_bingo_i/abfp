<?php

namespace InvoiceBundle\Provider;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Factories\JobFactory;
use InvoiceBundle\Repository\JobRepository;
use InvoiceBundle\Entity\Job;

class JobProvider
{
    /** @var JobRepository */
    private $jobRepository;

    /**
     * JobProvider constructor.
     *
     * @param JobRepository $jobRepository
     */
    public function __construct(JobRepository $jobRepository)
    {
        $this->jobRepository = $jobRepository;
    }

    /**
     * @param Account $account
     * @param Customer $customer
     * @param DeviceCategory $division
     *
     * @return Job
     */
    public function getJob(Account $account, Customer $customer, DeviceCategory $division): Job
    {
        /** @var Job|null $job */
        $job = $this->jobRepository->findOneBy([
            'division' => $division,
            'customer' => $customer,
            'account' => $account
        ]);

        if (!$job instanceof Job) {
            /** @var Job $job */
            $job = JobFactory::make($account, $customer, $division);
        }

        return $job;
    }
}
