<?php

namespace InvoiceBundle\Repository;

use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use Doctrine\ORM\EntityRepository;
use InvoiceBundle\Entity\EntityType;

class CustomerRepository extends EntityRepository
{
    /**
     * @param ContactPerson $contactPerson
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByContactPerson(ContactPerson $contactPerson)
    {
        $qb = $this->createQueryBuilder("c");
        $qb
            ->leftJoin('c.entityType', 'ce')
            ->where('c.entityId = :contactPerson')
            ->andWhere('ce.alias =:type')
            ->setParameters([
                'contactPerson' => $contactPerson,
                'type' => EntityType::TYPE_CONTACT_PERSON
            ]);

        $customer = $qb->getQuery()->getOneOrNullResult();

        return $customer;
    }

    /**
     * @param Company $company
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByCompany(Company $company)
    {
        $qb = $this->createQueryBuilder("c");
        $qb
            ->leftJoin('c.entityType', 'ce')
            ->where('c.entityId = :company')
            ->andWhere('ce.alias =:type')
            ->setParameters([
                'company' => $company,
                'type' => EntityType::TYPE_COMPANY
            ]);

        $customer = $qb->getQuery()->getOneOrNullResult();

        return $customer;
    }
}
