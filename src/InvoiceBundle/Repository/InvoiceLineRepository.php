<?php

namespace InvoiceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\Entity\Item;

class InvoiceLineRepository extends EntityRepository
{
    /**
     * @param Invoices $invoice
     * @param Item $item
     * @return bool
     */
    public function getOneByInvoiceAndItem(Invoices $invoice, Item $item)
    {
        $query = $this->createQueryBuilder("il");
        $query
            ->where('il.invoice = :invoice')
            ->andWhere('il.item = :item')
            ->setParameters(
                [
                    'invoice' => $invoice,
                    'item' => $item
                ]
            );

        return $query->getQuery()->getResult() ? $query->getQuery()->getResult()[0] : false;
    }
}
