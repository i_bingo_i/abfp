<?php

namespace InvoiceBundle\Event;

use InvoiceBundle\Entity\Invoices;
use Symfony\Component\EventDispatcher\Event;

class EditInvoicePaymentOverrideEvent extends Event
{
    public const PAYMENT_OVERRIDE_EDIT = 'payment_override.edit';
    
    /** @var Invoices */
    private $invoice;

    /**
     * EditInvoicePaymentOverrideEvent constructor.
     * @param Invoices $invoice
     */
    public function __construct(Invoices $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return Invoices
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}