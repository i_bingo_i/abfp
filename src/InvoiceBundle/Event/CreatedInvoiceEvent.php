<?php

namespace InvoiceBundle\Event;

use InvoiceBundle\Entity\Invoices;
use Symfony\Component\EventDispatcher\Event;

class CreatedInvoiceEvent extends Event
{
    public const CREATED_INVOICE = 'invoice.created';

    /** @var Invoices */
    private $invoice;

    /**
     * EditInvoicePaymentOverrideEvent constructor.
     * @param Invoices $invoice
     */
    public function __construct(Invoices $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return Invoices
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}
