<?php

namespace InvoiceBundle\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WorkorderBundle\Services\WorkorderLogHandler;

class EditInvoicePaymentOverrideSubscriber implements EventSubscriberInterface
{
    /** @var WorkorderLogHandler */
    private $workorderLogHandler;

    /**
     * EditInvoicePaymentOverrideSubscriber constructor.
     * @param WorkorderLogHandler $workorderLogHandler
     */
    public function __construct(WorkorderLogHandler $workorderLogHandler)
    {
        $this->workorderLogHandler = $workorderLogHandler;
    }

    /**
     * @param EditInvoicePaymentOverrideEvent $event
     * @throws \WorkorderBundle\Services\Exeptions\LogRecordTypeCantBeNull
     */
    public function editPaymentOverride(EditInvoicePaymentOverrideEvent $event)
    {
        $invoice = $event->getInvoice();
        $workorder = $invoice->getWorkorder();

        $this->workorderLogHandler->invoicePaymentsOverrideWasEdited($workorder, $invoice);
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            EditInvoicePaymentOverrideEvent::PAYMENT_OVERRIDE_EDIT => [
                'editPaymentOverride',
                1
            ]
        ];
    }
}