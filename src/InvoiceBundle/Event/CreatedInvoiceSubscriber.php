<?php

namespace InvoiceBundle\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WorkorderBundle\Services\WorkorderLogHandler;

class CreatedInvoiceSubscriber implements EventSubscriberInterface
{
    /** @var WorkorderLogHandler */
    private $workorderLogHandler;

    /**
     * CreatedInvoiceSubscriber constructor.
     * @param WorkorderLogHandler $workorderLogHandler
     */
    public function __construct(WorkorderLogHandler $workorderLogHandler)
    {
        $this->workorderLogHandler = $workorderLogHandler;
    }

    /**
     * @param CreatedInvoiceEvent $event
     * @throws \Exception
     */
    public function onCreated(CreatedInvoiceEvent $event)
    {
        $invoice = $event->getInvoice();
        $workorder = $event->getInvoice()->getWorkorder();

        $this->workorderLogHandler->invoiceDraftWasSave($workorder, $invoice);
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            CreatedInvoiceEvent::CREATED_INVOICE => ['onCreated']
        ];
    }
}