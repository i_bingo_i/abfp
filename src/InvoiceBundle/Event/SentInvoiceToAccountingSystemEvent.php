<?php

namespace InvoiceBundle\Event;

use InvoiceBundle\Entity\Invoices;
use Symfony\Component\EventDispatcher\Event;

class SentInvoiceToAccountingSystemEvent extends Event
{
    public const SENT_INVOICE_TO_AS = 'invoice_to_as.sent';

    /** @var Invoices */
    private $invoice;

    /**
     * EditInvoicePaymentOverrideEvent constructor.
     * @param Invoices $invoice
     */
    public function __construct(Invoices $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return Invoices
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}