<?php

namespace InvoiceBundle\Event;

use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Repository\WorkorderStatusRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use WorkorderBundle\Services\WorkorderLogHandler;
use WorkorderBundle\Validators\AvailableStatus;
use AppBundle\Entity\WorkorderStatus;

class SentInvoiceToAccountingSystemSubscriber implements EventSubscriberInterface
{
    /** @var WorkorderLogHandler */
    private $workorderLogHandler;
    /** @var WorkorderManager */
    private $workorderManager;
    /** @var AvailableStatus */
    private $availableStatusValidator;
    /** @var WorkorderStatusRepository */
    private $workorderStatusRepository;

    /**
     * SentInvoiceToAccountingSystemSubscriber constructor.
     * @param WorkorderLogHandler $workorderLogHandler
     * @param WorkorderManager $workorderManager
     * @param AvailableStatus $availableStatusValidator
     * @param WorkorderStatusRepository $workorderStatusRepository
     */
    public function __construct(
        WorkorderLogHandler $workorderLogHandler,
        WorkorderManager $workorderManager,
        AvailableStatus $availableStatusValidator,
        WorkorderStatusRepository $workorderStatusRepository
    ) {
        $this->workorderLogHandler = $workorderLogHandler;
        $this->workorderManager = $workorderManager;
        $this->availableStatusValidator = $availableStatusValidator;
        $this->workorderStatusRepository = $workorderStatusRepository;
    }

    /**
     * @param SentInvoiceToAccountingSystemEvent $event
     * @throws \Exception
     */
    public function onSent(SentInvoiceToAccountingSystemEvent $event)
    {
        $invoice = $event->getInvoice();
        $workorder = $invoice->getWorkorder();

        /** @var WorkorderStatus $woSendInvoiceStatus */
        $woSendInvoiceStatus = $this->workorderStatusRepository->findOneBy([
            'alias' => WorkorderStatus::STATUS_SEND_INVOICE
        ]);
        if ($this->availableStatusValidator->canChangeStatus($workorder->getStatus(), $woSendInvoiceStatus)) {
            $workorder->setStatus($woSendInvoiceStatus);
        }

        $this->workorderLogHandler->invoiceSubmittedToAccountingSystem($workorder, $invoice);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            SentInvoiceToAccountingSystemEvent::SENT_INVOICE_TO_AS => 'onSent'
        ];
    }
}
