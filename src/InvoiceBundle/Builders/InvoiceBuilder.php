<?php

namespace InvoiceBundle\Builders;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\Event;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Address\AddressProvider;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\Job;

class InvoiceBuilder
{
    /** @var Invoices */
    private $invoice;

    public function __construct()
    {
        $this->invoice = new Invoices();
    }

    /**
     * @param Invoices $invoice
     */
    public function initByInvoice(Invoices $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @param AccountContactPerson $acp
     *
     * @return InvoiceBuilder
     */
    public function setAcp(AccountContactPerson $acp): InvoiceBuilder
    {
        $this->invoice->setAccountContactPerson($acp);
        $this->invoice->setCustomer($acp->getCustomer());

        return $this;
    }

    /**
     * @return InvoiceBuilder
     */
    public function setBillToAddressByAcp(): InvoiceBuilder
    {
        /** @var AccountContactPerson $acp */
        $acp = $this->invoice->getAccountContactPerson();

        if (!empty($acp)) {
            $billToAddress = AddressProvider::getBillToAddressByACP($acp);

            $this->invoice->setBillToAddress($billToAddress);
        }

        return $this;
    }

    /**
     * @param Address $address
     *
     * @return InvoiceBuilder
     */
    public function setBillToAddress(Address $address): InvoiceBuilder
    {
        $this->invoice->setBillToAddress($address);

        return $this;
    }

    /**
     * @param Workorder $workorder
     *
     * @return InvoiceBuilder
     */
    public function setWorkorder(Workorder $workorder): InvoiceBuilder
    {
        $this->invoice->setWorkorder($workorder);

        return $this;
    }

    /**
     * @return InvoiceBuilder
     */
    public function setInspectorByWo(): InvoiceBuilder
    {
        /** @var Workorder $workorder */
        $workorder = $this->invoice->getWorkorder();

        $inspector = [];
        /** @var Event $event */
        foreach ($workorder->getEvents() as $event) {
            $inspector[$event->getUser()->getId()] = $event->getUser()->getUser()->getFirstName() . ' '
                . $event->getUser()->getUser()->getLastName();
        }
        $inspector = implode(', ', $inspector);
        $this->invoice->setInspector($inspector);

        return $this;
    }

    /**
     * @param $inspector
     *
     * @return InvoiceBuilder
     */
    public function setInspector($inspector): InvoiceBuilder
    {
        $this->invoice->setInspector($inspector);

        return $this;
    }

    /**
     * @param Job $job
     *
     * @return InvoiceBuilder
     */
    public function setJob(Job $job): InvoiceBuilder
    {
        $this->invoice->setJob($job);

        return $this;
    }

    /**
     * @param Address $address
     *
     * @return InvoiceBuilder
     */
    public function setShipToAddress(Address $address): InvoiceBuilder
    {
        $this->invoice->setShipToAddress($address);

        return $this;
    }

    /**
     * @param User $user
     *
     * @return InvoiceBuilder
     */
    public function setUser(User $user): InvoiceBuilder
    {
        $this->invoice->setUser($user);

        return $this;
    }

    /**
     * @param \DateTime $date
     *
     * @return InvoiceBuilder
     */
    public function setInvoiceDate(\DateTime $date): InvoiceBuilder
    {
        $this->invoice->setInvoiceDate($date);

        return $this;
    }

    /**
     * @return Invoices
     */
    public function getInvoice(): Invoices
    {
        return $this->invoice;
    }

    /**
     * @param array $invoiceLines
     */
    public function setLines(array $invoiceLines)
    {
        $this->invoice->setLines($invoiceLines);
    }
}
