<?php

namespace InvoiceBundle\ActionManagers;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Services\Job\JobEditor;
use QuickbooksBundle\Exceptions\FailIQuickbooksAction;
use QuickbooksBundle\Managers\JobManager;
use QuickbooksBundle\Services\Quickbooks\Job\CreatorJob;
use QuickbooksBundle\Services\Quickbooks\Job\ModifierJob;
use Doctrine\Common\Persistence\ObjectManager;
use \QuickbooksBundle\Repository\QBRepository\Job as JobRepository;

class JobActionManager
{
    /** @var JobEditor */
    private $jobEditor;
    /** @var CreatorJob */
    private $creatorJobInQB;
    /** @var ModifierJob */
    private $modifierJob;
    /** @var  ObjectManager */
    private $objectManager;
    /** @var JobManager */
    private $jobManager;
    /** @var JobRepository  */
    private $jobRepository;

    /**
     * JobActionManager constructor.
     * @param JobEditor $jobEditor
     * @param CreatorJob $creatorJobInQB
     * @param ModifierJob $modifierJob
     * @param ObjectManager $objectManager
     * @param JobManager $jobManager
     * @param JobRepository $jobRepository
     */
    public function __construct(
        JobEditor $jobEditor,
        CreatorJob $creatorJobInQB,
        ModifierJob $modifierJob,
        ObjectManager $objectManager,
        JobManager $jobManager,
        JobRepository $jobRepository
    ) {
        $this->jobEditor = $jobEditor;
        $this->creatorJobInQB = $creatorJobInQB;
        $this->modifierJob = $modifierJob;
        $this->objectManager = $objectManager;
        $this->jobManager = $jobManager;
        $this->jobRepository = $jobRepository;
    }

    /**
     * @param Invoices $invoice
     * @throws FailIQuickbooksAction
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendJobToAccountingSystem(Invoices $invoice)
    {
        /** @var Job $job */
        $job = $invoice->getJob();

        try {
            if (empty($job->getAccountingSystemId())) {
                $response = $this->jobManager->addJob($invoice);
            } else {
                $response = $this->jobManager->modJob($invoice);
            }
        } catch (FailIQuickbooksAction $exception) {
            throw new FailIQuickbooksAction($exception->getMessage());
        }

        $job->setAccountingSystemId($response->get('ListID'));
        $job->setEditSequence($response->get('EditSequence'));

        $this->objectManager->persist($job);
        $this->objectManager->flush();
    }

    /**
     * @param Job $job
     * @throws FailIQuickbooksAction
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function refreshSequence(Job $job)
    {
        if ($job->getAccountingSystemId()) {
            $response = $this->jobRepository->find($job);

            $job->setEditSequence($response->get('EditSequence'));
            $this->objectManager->flush();
        }
    }
}
