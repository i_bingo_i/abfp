<?php

namespace InvoiceBundle\ActionManagers;

use AppBundle\Entity\Repository\WorkorderStatusRepository;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Entity\WorkorderStatus;
use AppBundle\Services\PdfManagers\PDFInvoiceManager;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Workorder;
use InvoiceBundle\Creators\InvoiceCreator;
use InvoiceBundle\DataTransportObject\Requests\CreateCustomInvoiceDTO;
use InvoiceBundle\DataTransportObject\Requests\CreateInvoiceDTO;
use InvoiceBundle\DataTransportObject\Requests\EditPaymentInvoiceDTO;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\InvoiceStatus;
use InvoiceBundle\Event\CreatedInvoiceEvent;
use InvoiceBundle\Event\EditInvoicePaymentOverrideEvent;
use InvoiceBundle\Event\SentInvoiceToAccountingSystemEvent;
use InvoiceBundle\Services\Invoice\InvoiceSender;
use InvoiceBundle\Repository\InvoicesRepository;
use InvoiceBundle\Repository\InvoiceStatusRepository;
use QuickbooksBundle\Exceptions\FailIQuickbooksAction;
use QuickbooksBundle\Managers\InvoiceManager;
use QuickbooksBundle\Response\Response;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Session\Session;
use InvoiceBundle\Services\InvoiceFile\Uploader;
use WorkorderBundle\Services\Exeptions\LogRecordTypeCantBeNull;
use WorkorderBundle\Validators\AvailableStatus;
use WorkorderBundle\Services\LogCreator;
use WorkorderBundle\Services\LogMessage;
use QuickbooksBundle\Managers\InvoiceManager as QuickbooksInvoiceManager;

class InvoiceActionManager
{
    /** @var Invoices */
    private $invoice;
    /** @var ObjectManager */
    private $objectManager;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var CustomerActionManager $customerAccountingManager */
    private $customerAccountingManager;
    /** @var JobActionManager $jobAccountingManager */
    private $jobAccountingManager;
    /** @var InvoiceSender $invoiceSender */
    private $invoiceSender;
    /** @var Session */
    private $session;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var InvoiceStatusRepository */
    private $invoiceStatusRepository;
    /** @var InvoicesRepository */
    private $invoiceRepository;
    /** @var InvoiceCreator */
    private $invoiceCreator;
    /** @var PDFInvoiceManager */
    private $pdfInvoiceManager;
    /** @var InvoiceManager  */
    private $invoiceManager;
    /** @var Uploader */
    private $invoiceFileUploader;
    /** @var AvailableStatus */
    private $availableStatusService;
    /** @var WorkorderStatusRepository */
    private $woStatusRepository;
    /** @var LogCreator */
    private $woLogCreator;
    /** @var LogMessage $logMessage */
    private $logMessage;
    /** @var QuickbooksInvoiceManager  */
    private $quickbooksInvoiceManager;

    /**
     * InvoiceActionManager constructor.
     * @param ObjectManager $objectManager
     * @param EventDispatcherInterface $dispatcher
     * @param InvoiceCreator $invoiceCreator
     * @param CustomerActionManager $customerAccountingManager
     * @param JobActionManager $jobAccountingManager
     * @param InvoiceSender $invoiceSender
     * @param Session $session
     * @param PDFInvoiceManager $pdfInvoiceManager
     * @param QuickbooksInvoiceManager $invoiceManager
     * @param Uploader $uploader
     * @param AvailableStatus $availableStatus
     * @param LogCreator $logCreator
     * @param LogMessage $logMessage
     * @param QuickbooksInvoiceManager $quickbooksInvoiceManager
     */
    public function __construct(
        ObjectManager $objectManager,
        EventDispatcherInterface $dispatcher,
        InvoiceCreator $invoiceCreator,
        CustomerActionManager $customerAccountingManager,
        JobActionManager $jobAccountingManager,
        InvoiceSender $invoiceSender,
        Session $session,
        PDFInvoiceManager $pdfInvoiceManager,
        InvoiceManager $invoiceManager,
        Uploader $uploader,
        AvailableStatus $availableStatus,
        LogCreator $logCreator,
        LogMessage $logMessage,
        QuickbooksInvoiceManager $quickbooksInvoiceManager
    ) {
        $this->objectManager = $objectManager;
        $this->dispatcher = $dispatcher;
        $this->invoiceCreator = $invoiceCreator;

        $this->addressTypeRepository = $this->objectManager->getRepository("AppBundle:AddressType");
        $this->invoiceStatusRepository = $this->objectManager->getRepository('InvoiceBundle:InvoiceStatus');
        $this->invoiceRepository = $this->objectManager->getRepository('InvoiceBundle:Invoices');
        $this->woStatusRepository = $this->objectManager->getRepository('AppBundle:WorkorderStatus');

        $this->customerAccountingManager = $customerAccountingManager;
        $this->jobAccountingManager = $jobAccountingManager;
        $this->invoiceSender = $invoiceSender;
        $this->session = $session;
        $this->pdfInvoiceManager = $pdfInvoiceManager;
        $this->invoiceManager = $invoiceManager;
        $this->invoiceFileUploader = $uploader;
        $this->availableStatusService = $availableStatus;
        $this->woLogCreator = $logCreator;
        $this->logMessage = $logMessage;
        $this->quickbooksInvoiceManager = $quickbooksInvoiceManager;
    }

    /**
     * @param CreateCustomInvoiceDTO $customInvoiceDTO
     * @throws GuzzleException
     */
    public function submitCustom(CreateCustomInvoiceDTO $customInvoiceDTO)
    {
        /** @var Workorder $workorder */
        $workorder = $customInvoiceDTO->getWorkorder();

        $invoiceFileName = "WO_" . $workorder->getId() . "_Invoice_" .
            $customInvoiceDTO->getFile()->getClientOriginalName();
        $this->invoiceFileUploader->uploadInvoice($customInvoiceDTO->getFile(), $invoiceFileName);

        /** @var WorkorderStatus $submittedInvoiceStatus */
        $submittedInvoiceStatus =
            $this->woStatusRepository->findOneBy(['alias' => WorkorderStatus::SUBMITTED_INVOICE_STATUS]);
        if ($this->availableStatusService->canChangeStatus($workorder->getStatus(), $submittedInvoiceStatus)) {
            $workorder->setStatus($submittedInvoiceStatus);
        }

        $this->deleteDraftInvoice($workorder);
        $this->voidInvoice($workorder);

        /** @var InvoiceStatus $invoiceCustomStatus */
        $invoiceCustomStatus = $this->invoiceStatusRepository->findOneBy(['alias' => InvoiceStatus::CUSTOM]);
        /** @var Invoices $invoice */
        $invoice = new Invoices();
        $invoice->setWorkorder($workorder);
        $invoice->setStatus($invoiceCustomStatus);
        $invoice->setFile($invoiceFileName);
        $this->objectManager->persist($invoice);

        $workorder->addInvoice($invoice);
        $this->objectManager->flush();

        try {
            $this->woLogCreator->createRecord($workorder, [
                'type' => WorkorderLogType::TYPE_USE_CUSTOM_PDF,
                'message' => $this->logMessage->makeByLogType(WorkorderLogType::TYPE_USE_CUSTOM_PDF),
                'changingStatus' => true,
                'comment' => $customInvoiceDTO->getComment(),
                'invoice' => $invoice
            ]);
        } catch (LogRecordTypeCantBeNull $exception) {
            $this->session->getFlashBag()->add('error', $exception->getMessage());
        }
    }


    /**
     * @param Workorder $workorder
     * @return Invoices
     */
    public function getCurrentDraftInvoice(Workorder $workorder)
    {
        /** @var Invoices $lastDraftInvoice */
        $lastDraftInvoice =
            $this->invoiceRepository->getLastForWorkorderByStatusAlias($workorder, InvoiceStatus::STATUS_DRAFT);

        if ($lastDraftInvoice instanceof Invoices) {
            return $lastDraftInvoice;
        }

        /** @var Invoices $invoice */
        return $this->invoiceCreator->makeByWorkorder($workorder);
    }

    /**
     * @param CreateInvoiceDTO $createInvoiceDTO
     *
     * @return Invoices
     */
    public function save(CreateInvoiceDTO $createInvoiceDTO)
    {
        /** @var Invoices $invoice */
        $invoice = $createInvoiceDTO->invoice();

        $invoiceId = $invoice->getId();

        $this->objectManager->persist($invoice);
        $this->objectManager->flush();

        if (!$invoiceId) {
            $event = new CreatedInvoiceEvent($invoice);
            $this->dispatcher->dispatch($event::CREATED_INVOICE, $event);
        }

        return $invoice;
    }

    // TODO: тут викликається два флеша потрібно відрефакторити

    /**
     * @param CreateInvoiceDTO $createInvoiceDTO
     * @throws GuzzleException
     */
    public function saveAndSend(CreateInvoiceDTO $createInvoiceDTO)
    {
        /** @var Invoices $invoice */
        $invoice = $createInvoiceDTO->invoice();
        $isNewInvoice = $invoice->getId() ? false : true;

        $this->objectManager->persist($invoice);
        $this->objectManager->flush();

        $isSend = $this->send($invoice, $isNewInvoice);
        if ($isSend) {
            $invoicePdfName = $this->pdfInvoiceManager->save($invoice);
            $invoice->setFile($invoicePdfName);

            $this->objectManager->persist($invoice);
            $this->objectManager->flush();
        }
    }

    /**
     * @param Invoices $invoice
     * @param bool $isNewInvoice
     * @return bool
     * @throws GuzzleException
     */
    public function send(Invoices $invoice, $isNewInvoice = false)
    {
        try {
            $this->customerAccountingManager->refreshSequence($invoice->getJob()->getCustomer());
            $this->customerAccountingManager->sendToAccountingSystem($invoice);
            $this->jobAccountingManager->refreshSequence($invoice->getJob());
            $this->jobAccountingManager->sendJobToAccountingSystem($invoice);
            /** @var Response $response */
            $response = $this->quickbooksInvoiceManager->createInvoice($invoice, $isNewInvoice);
        } catch (FailIQuickbooksAction $exception) {
            return false;
        }

        $inQBInvoiceStatus = $this->invoiceStatusRepository->findOneBy(['alias' => InvoiceStatus::STATUS_IN_QB]);
        $invoice->setAccountingSystemId($response->get('TxnID'));
        $invoice->setEditSequence($response->get('EditSequence'));
        $invoice->setStatus($inQBInvoiceStatus);

        $event = new SentInvoiceToAccountingSystemEvent($invoice);
        $this->dispatcher->dispatch($event::SENT_INVOICE_TO_AS, $event);

        return true;
    }

    // TODO: не очевидно коли сам процес Override Payment відбувається десь у обробнику $event
    /**
     * @param EditPaymentInvoiceDTO $editPaymentInvoiceDTO
     */
    public function editOverridePayment(EditPaymentInvoiceDTO $editPaymentInvoiceDTO)
    {
        $overridePayment = $editPaymentInvoiceDTO->overridePayment();
        /** @var Invoices $invoice */
        $invoice = $editPaymentInvoiceDTO->invoice();

        if ($invoice->getOverridePayment() != $overridePayment) {
            $invoice->setOverridePayment($overridePayment);

            if ($invoice->getStatus()->getAlias() == InvoiceStatus::STATUS_IN_QB) {
                $invoicePdfName = $this->pdfInvoiceManager->save($invoice);
                $invoice->setFile($invoicePdfName);
            }

            $event = new EditInvoicePaymentOverrideEvent($invoice);
            $this->dispatcher->dispatch($event::PAYMENT_OVERRIDE_EDIT, $event);
        }
    }

    /**
     * @param Workorder $workorder
     * @return bool
     * @throws GuzzleException
     */
    public function voidInvoice(Workorder $workorder)
    {
        $lastWoInvoice = $this->invoiceRepository->getLastInvoiceForWorkorder($workorder);

        if ($lastWoInvoice instanceof Invoices) {
            /** @var InvoiceStatus $invoiceStatus */
            $invoiceStatus = $lastWoInvoice->getStatus();

            if ($invoiceStatus->getAlias() == InvoiceStatus::CUSTOM) {
                $this->setVoidedStatus($lastWoInvoice);
            }

            if ($invoiceStatus->getAlias() == InvoiceStatus::STATUS_IN_QB) {
                try {
                    $this->invoiceManager->voidInvoice($lastWoInvoice);
                } catch (FailIQuickbooksAction $exception) {
                    return false;
                }

                $this->setVoidedStatus($lastWoInvoice);
            }
        }

        return true;
    }

    /**
     * @param Invoices $invoice
     */
    private function setVoidedStatus(Invoices $invoice)
    {
        /** @var InvoiceStatus $voidedInvoiceStatus */
        $voidedInvoiceStatus = $this->invoiceStatusRepository->findOneBy(['alias' => InvoiceStatus::VOIDED]);

        $invoice->setStatus($voidedInvoiceStatus);
        $this->objectManager->flush();
    }

    /**
     * @param Workorder $workorder
     */
    private function deleteDraftInvoice(Workorder $workorder)
    {
        /** @var Invoices $draftInvoice */
        $draftInvoice =
            $this->invoiceRepository->getLastForWorkorderByStatusAlias($workorder, InvoiceStatus::STATUS_DRAFT);
        if ($draftInvoice instanceof Invoices) {
            /** @var InvoiceStatus $deleteInvoiceStatus */
            $deleteInvoiceStatus = $this->invoiceStatusRepository->findOneBy(['alias' => InvoiceStatus::DELETED]);

            $draftInvoice->setStatus($deleteInvoiceStatus);
        }
    }
}
