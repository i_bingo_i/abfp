<?php

namespace InvoiceBundle\ActionManagers;

use InvoiceBundle\Creators\CustomerCreator;
use AppBundle\Entity\Address;
use InvoiceBundle\DataTransportObject\Requests\UpdateCustomerDTO;
use InvoiceBundle\DataTransportObject\Requests\CreateCustomerDTO;
use InvoiceBundle\Entity\Customer;
use Doctrine\Common\Persistence\ObjectManager;
use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\Exceptions\FailIQuickbooksAction;
use QuickbooksBundle\Managers\CustomerManager;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Services\Quickbooks\Customer\CreatorCustomer;
use QuickbooksBundle\Services\Quickbooks\Customer\ModifierCustomer;
use QuickbooksBundle\Repository\QBRepository\Customer as CustomerRepository;

class CustomerActionManager
{
    /** @var  ObjectManager */
    private $objectManager;
    /** @var CustomerCreator */
    private $customerCreator;
    /** @var ModifierCustomer */
    private $modifierCustomer;
    /** @var CreatorCustomer */
    private $creatorCustomerAccountingSystem;
    /** @var CustomerManager */
    private $customerManager;
    /** @var CustomerRepository */
    private $customerRepository;

    /**
     * CustomerActionManager constructor.
     * @param ObjectManager $objectManager
     * @param CustomerCreator $customerCreator
     * @param ModifierCustomer $modifierCustomer
     * @param CreatorCustomer $creatorCustomerAccountingSystem
     * @param CustomerManager $customerManager
     * @param CustomerRepository $customerRepository
     */
    public function __construct(
        ObjectManager $objectManager,
        CustomerCreator $customerCreator,
        ModifierCustomer $modifierCustomer,
        CreatorCustomer $creatorCustomerAccountingSystem,
        CustomerManager $customerManager,
        CustomerRepository $customerRepository
    ) {
        $this->objectManager = $objectManager;
        $this->customerCreator = $customerCreator;
        $this->modifierCustomer = $modifierCustomer;
        $this->creatorCustomerAccountingSystem = $creatorCustomerAccountingSystem;
        $this->customerManager = $customerManager;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param CreateCustomerDTO $createCustomerDTO
     * @return Customer
     */
    public function create(CreateCustomerDTO $createCustomerDTO)
    {
        $entity = $createCustomerDTO->getEntity();
        /** @var Address $billingAddress */
        $billingAddress = $createCustomerDTO->getBillingAddress();
        /** @var Customer $customer */
        $customer = $this->customerCreator->make($entity, $billingAddress);

        $this->objectManager->persist($customer);
        $this->objectManager->flush();

        return $customer;
    }

    /**
     * @param UpdateCustomerDTO $updateCustomerDTO
     * @return bool|Customer
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(UpdateCustomerDTO $updateCustomerDTO)
    {
        /** @var Customer $customer */
        $customer = $updateCustomerDTO->getCustomer();
        $customer->setBillingAddress($updateCustomerDTO->getBillingAddress());
        $customer->setName($updateCustomerDTO->getName());

        if (!empty($customer->getAccountingSystemId()) && !empty($customer->getEditSequence())) {
            try {
                /** @var Response $response */
                $response = $this->customerManager->modCustomer($customer);
            } catch (FailIQuickbooksAction $exception) {
                return false;
            }

            $customer->setAccountingSystemId($response->get('ListID'));
            $customer->setEditSequence($response->get('EditSequence'));
        }

        $this->objectManager->persist($customer);
        $this->objectManager->flush();

        return $customer;
    }

    /**
     * @param Invoices $invoice
     * @throws FailIQuickbooksAction
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendToAccountingSystem(Invoices $invoice)
    {
        /** @var Customer $customer */
        $customer = $invoice->getJob()->getCustomer();
        if (empty($customer->getAccountingSystemId())) {
            try {
                $response = $this->customerManager->addCustomer($invoice);
            } catch (FailIQuickbooksAction $exception) {
                throw new FailIQuickbooksAction($exception->getMessage());
            }

            $customer->setAccountingSystemId($response->get('ListID'));
            $customer->setEditSequence($response->get('EditSequence'));
            $this->objectManager->flush();
        }
    }

    /**
     * @param Customer $customer
     * @throws FailIQuickbooksAction
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function refreshSequence(Customer $customer)
    {
        if ($customer->getAccountingSystemId()) {
            $response = $this->customerRepository->find($customer);

            $customer->setEditSequence($response->get('EditSequence'));
            $this->objectManager->flush();
        }
    }
}
