<?php

namespace InvoiceBundle\Services;

use JMS\Serializer\Serializer;
use QuickbooksBundle\Repository\ItemRepository;

class ItemProvider
{
    /** @var ItemRepository */
    private $itemRepository;
    /** @var Serializer */
    private $serializer;

    public function __construct(ItemRepository $repository, Serializer $serializer)
    {
        $this->itemRepository = $repository;
        $this->serializer = $serializer;
    }

    /**
     * @return array
     */
    public function getAllItems()
    {
        return $this->itemRepository->findBy(['isActive' => true]);
    }

    /**
     * @return mixed|string
     */
    public function getAllItemsInJson()
    {
        return $this->serializer->serialize($this->getAllItems(), 'json');
    }
}