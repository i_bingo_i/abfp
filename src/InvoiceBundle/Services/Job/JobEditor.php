<?php

namespace InvoiceBundle\Services\Job;

use InvoiceBundle\Entity\Job;

class JobEditor
{
    /** @var JobManager */
    private $jobManager;

    /**
     * JobEditor constructor.
     * @param JobManager $jobManager
     */
    public function __construct(JobManager $jobManager)
    {
        $this->jobManager = $jobManager;
    }

    /**
     * @param Job $job
     * @param Job $jobFromAccountingSystem
     */
    public function addAccountingSystemId(Job $job, Job $jobFromAccountingSystem)
    {
        $job->setAccountingSystemId($jobFromAccountingSystem->getAccountingSystemId());
        $job->setEditSequence($jobFromAccountingSystem->getEditSequence());
    }
}