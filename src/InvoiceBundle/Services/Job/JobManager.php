<?php

namespace InvoiceBundle\Services\Job;

use Doctrine\Common\Persistence\ObjectManager;
use InvoiceBundle\Entity\Job;

class JobManager
{
    /** @var ObjectManager */
    private $objectManager;

    /**
     * JobManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param Job $job
     */
    public function save(Job $job)
    {
        $this->objectManager->persist($job);
        $this->objectManager->flush();
    }
}