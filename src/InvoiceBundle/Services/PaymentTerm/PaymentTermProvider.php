<?php

namespace InvoiceBundle\Services\PaymentTerm;

use AppBundle\Entity\PaymentTerm;
use AppBundle\Entity\Repository\PaymentTermRepository;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class PaymentTermProvider
{
    /** @var PaymentTermRepository */
    private $paymentTermRepository;
    /** @var Serializer */
    private $serializer;

    /**
     * PaymentTermProvider constructor.
     * @param PaymentTermRepository $paymentTermRepository
     * @param Serializer $serializer
     */
    public function __construct(
        PaymentTermRepository $paymentTermRepository,
        Serializer $serializer
    ) {
        $this->paymentTermRepository = $paymentTermRepository;
        $this->serializer = $serializer;
    }

    /**
     * @return \AppBundle\Entity\PaymentTerm[]|array
     */
    public function getAll()
    {
        return $this->paymentTermRepository->findAll();
    }

    /**
     * @param PaymentTermProvider|array $paymentTerm
     * @param string $group
     * @return mixed|string
     */
    public function serializeInJson($paymentTerm, $group = 'full')
    {
        return $this->serializer->serialize(
            $paymentTerm,
            'json',
            SerializationContext::create()->setGroups([$group])
        );
    }

    /**
     * @param string $getAlias
     * @return PaymentTerm|null
     */
    public function getByAlias($getAlias)
    {
        /** @var PaymentTerm|null $paymentTerm */
        $paymentTerm = $this->paymentTermRepository->findOneBy(['alias' => $getAlias]);

        return $paymentTerm;
    }
}