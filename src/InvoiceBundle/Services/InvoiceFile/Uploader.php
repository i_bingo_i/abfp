<?php

namespace InvoiceBundle\Services\InvoiceFile;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Uploader
{
    private $fullSavePath = __DIR__ . "/../../../../web/uploads/invoices/";

    /**
     * @param UploadedFile $file
     * @param string $fileName
     */
    public function uploadInvoice(UploadedFile $file, string $fileName)
    {
        $file->move($this->fullSavePath, $fileName);
        chmod($this->fullSavePath."/".$fileName, 0777);
    }
}
