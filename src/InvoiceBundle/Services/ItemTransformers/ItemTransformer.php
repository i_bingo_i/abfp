<?php

namespace InvoiceBundle\Services\ItemTransformers;

use AppBundle\Entity\Account;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\Device;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceHistory;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\WorkorderDeviceInfo;
use AppBundle\Services\Tree\Grouping;
use InvoiceBundle\Entity\InvoiceLine;
use InvoiceBundle\Services\Sorter\InvoiceLineSorter;
use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Repository\ItemRepository;

class ItemTransformer
{
    private const ITEM_ADDRESS_NAME = 'Address';
    private const ITEM_INSPECTION_NAME = 'Inspection';
    private const ITEM_REPAIR_NAME = 'Repair';
    private const ITEM_MUN_FEE_NAME = 'Mun. fee.';
    private const ITEM_OTHER_NAME = 'Other';
    private const ITEM_DISCOUNT_NAME = 'Discount';

    private const ITEM_MUN_FEE_DESCRIPTION = "Fees for submitting reports to %s for necessary inspection(s)";
    private const ITEM_AFTER_HOURS_WORK_COST_DESCRIPTION = 'After Hours Work Cost';
    private const ITEM_ADDITIONAL_COST_DESCRIPTION = "Additional Cost - Inspect %s Device that leaking on site to 
        create a proposal";

    /** @var Grouping */
    private $grouping;
    /** @var ItemRepository */
    private $itemRepository;
    /** @var int */
    private $serialNumber;
    /** @var Item */
    private $itemAddress;
    /** @var Item */
    private $itemInspection;
    /** @var Item */
    private $itemRepair;
    /** @var Item */
    private $itemMunFee;
    /** @var Item */
    private $itemOther;
    /** @var array */
    private $invoiceLinesDirty;
    /** @var DepartmentChannel */
    private $departmentChannels;
    /** @var int */
    private $inspectionQuantity;
    /** @var Workorder */
    private $workorder;
    /** @var Account */
    private $account;
    /** @var WorkorderDeviceInfo */
    private $workorderDeviceInfo;
    /** @var array */
    private $repairInvoiceLinesStorage;

    /**
     * ItemTransformer constructor.
     * @param Grouping $grouping
     * @param ItemRepository $itemRepository
     */
    public function __construct(Grouping $grouping, ItemRepository $itemRepository)
    {
        $this->grouping = $grouping;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param Workorder $workorder
     * @return array
     */
    public function transform(Workorder $workorder)
    {
        $invoiceLineSorter = new InvoiceLineSorter();
        $this->workorder = $workorder;
        $this->serialNumber = 1;
        $this->departmentChannels = [];
        $accountTree = $this->grouping->getTreeForWorkorder($workorder);

        if (is_array($accountTree) && !empty($accountTree)) {
            $this->accountProcessing($accountTree);

            $this->departmentChannelProcessing($this->departmentChannels);

            if ($this->workorder->getAfterHoursWorkCost()) {
                $this->invoiceLinesDirty[] = $this->transformAfterHoursWorkCostToOther($this->workorder);
            }

            if ($this->workorder->getAdditionalServicesCost()) {
                $this->invoiceLinesDirty[] = $this->transformAdditionalCostToOther($this->workorder);
            }

            if ($this->workorder->getDiscount()) {
                $this->invoiceLinesDirty[] = $this->transformDiscount($this->workorder);
            }
        }

        return $invoiceLineSorter->sort($this->invoiceLinesDirty);
    }

    /**
     * @param array $accountTree
     */
    private function accountProcessing(array $accountTree)
    {
        foreach ($accountTree as $key => $node) {
            /** @var Account $account */
            $this->account = $node['account'];
            $this->repairInvoiceLinesStorage = [];

            $this->invoiceLinesDirty[$this->account->getId()][] = $this->transformSiteToAddress($this->account);

            $this->devicesProcessing($node['devices']);
        }
    }

    /**
     * @param array $devices
     */
    private function devicesProcessing(array $devices)
    {
        foreach ($devices as $oneDeviceNode) {
            /** @var Device $device */
            $device = $oneDeviceNode['device'];
            /** @var WorkorderDeviceInfo $workorderDeviceInfo */
            $this->workorderDeviceInfo = $this->workorder->getWorkorderDeviceInfo()
                ->filter(function (WorkorderDeviceInfo $workorderDeviceInfo) use ($device) {
                    return $workorderDeviceInfo->getDevice()->getId() == $device->getId();
                })->first();

            $this->serviceProcessing($oneDeviceNode["services"]);

            /** Add serial number to repair invoice line */
            /** @var InvoiceLine $invoiceLine */
            foreach ($this->repairInvoiceLinesStorage as $invoiceLine) {
                $invoiceLine->setSerialNumber($this->serialNumber++);
            }

            if (!empty($oneDeviceNode['subDevices'])) {
                $this->devicesProcessing($oneDeviceNode['subDevices']);
            }
        }
    }

    /**
     * @param array $services
     */
    private function serviceProcessing(array $services)
    {
        /** @var Service | ServiceHistory $service */
        foreach ($services as $service) {
            if ($service instanceof Service || $service instanceof ServiceHistory) {
                if ($service->getNamed()->getFrequency()) {
                    $this->inspectionProcessing($service);

                } else {
                    $this->repairProcessing($service);
                }

                $this->addDepartmentChanel($service);
            }
        }
    }

    /**
     * @param Service | ServiceHistory $service
     */
    private function inspectionProcessing($service)
    {
        /** @var int $accountId */
        $accountId = $service->getAccount()->getId();
        /** @var string $serviceName */
        $serviceName = $service->getNamed()->getName();
        /** @var int $serviceFixedPrice */
        $serviceFixedPrice = $service->getFixedPrice();
        /** @var InvoiceLine $invoiceLine */
        $invoiceLine = $this->invoiceLinesDirty[$accountId][$serviceName][$serviceFixedPrice] ?? null;

        if (empty($invoiceLine)) {
            $invoiceLine = $this->transformInspection($service);
        } else {
            $invoiceLine->setQuantity($invoiceLine->getQuantity() + 1);
            $invoiceLine->setAmount($invoiceLine->getAmount() + $service->getFixedPrice());
        }

        $this->invoiceLinesDirty[$accountId][$serviceName][$serviceFixedPrice] = $invoiceLine;
    }

    /**
     * @param Service | ServiceHistory $service
     */
    private function repairProcessing($service)
    {
        /** @var int $accountId */
        $accountId = $service->getAccount()->getId();
        /** @var string $serviceNameworkorderDeviceInfoId */
        $serviceNameWorkorderDeviceInfoId = $service->getDevice()->getTitle()
            . $this->workorderDeviceInfo->getId();
        /** @var InvoiceLine $invoiceLine */
        $invoiceLine = $this->invoiceLinesDirty[$accountId][$serviceNameWorkorderDeviceInfoId] ?? null;

        if (empty($invoiceLine)) {
            $invoiceLine = $this->transformRepair($service);
            $invoiceLine->setRate($this->workorderDeviceInfo->getRepairPartsAndLabourPrice());
            $invoiceLine->setAmount($this->workorderDeviceInfo->getRepairPartsAndLabourPrice());
        } else {
            $invoiceLine->setDescription(
                $invoiceLine->getDescription() . ', ' . $service->getNamed()->getName()
            );
        }

        $this->invoiceLinesDirty[$accountId][$serviceNameWorkorderDeviceInfoId] = $invoiceLine;
        $this->repairInvoiceLinesStorage[] = $invoiceLine;
    }

    /**
     * @param array $departmentChannels
     */
    private function departmentChannelProcessing(array $departmentChannels)
    {
        if (!empty($departmentChannels)) {
            foreach ($this->departmentChannels as $departmentChannel) {
                $this->invoiceLinesDirty[] = $this->transformMunicipality($departmentChannel);
            }
        }
    }

    /**
     * @param Account $account
     * @return InvoiceLine
     */
    private function transformSiteToAddress(Account $account)
    {
        if (empty($this->itemAddress)) {
            $this->itemAddress = $this->itemRepository->findOneBy(['name' => self::ITEM_ADDRESS_NAME]);
        }

        $description = $account->getAddress()->getAddress()
            . ', ' . $account->getAddress()->getCity()
            . ', ' . $account->getAddress()->getState()->getCode()
            . ' ' . $account->getAddress()->getZip();

        $invoiceLineSite = new InvoiceLine();
        $invoiceLineSite->setItem($this->itemAddress);
        $invoiceLineSite->setDescription($description);
        $invoiceLineSite->setSerialNumber($this->serialNumber++);

        return $invoiceLineSite;
    }

    /**
     * @param Service | ServiceHistory $inspection
     * @return InvoiceLine
     */
    private function transformInspection($inspection)
    {
        if (empty($this->itemInspection)) {
            $this->itemInspection = $this->itemRepository->findOneBy(['name' => self::ITEM_INSPECTION_NAME]);
        }

        $invoiceLineInspection = new InvoiceLine();
        $invoiceLineInspection->setItem($this->itemInspection);
        $invoiceLineInspection->setDescription($inspection->getNamed()->getName());
        $invoiceLineInspection->setQuantity(1);
        $invoiceLineInspection->setRate($inspection->getFixedPrice());
        $invoiceLineInspection->setAmount($inspection->getFixedPrice());
        $invoiceLineInspection->setSerialNumber($this->serialNumber++);

        return $invoiceLineInspection;
    }

    /**
     * @param Service | ServiceHistory $repair
     * @return InvoiceLine
     */
    private function transformRepair($repair)
    {
        if (empty($this->itemRepair)) {
            $this->itemRepair = $this->itemRepository->findOneBy(['name' => self::ITEM_REPAIR_NAME]);
        }

        $invoiceLineRepair = new InvoiceLine();
        $invoiceLineRepair->setItem($this->itemRepair);
        $invoiceLineRepair->setDescription(
            $repair->getDevice()->getTitle() . ' - ' . $repair->getNamed()->getName()
        );
        $invoiceLineRepair->setQuantity(1);

        return $invoiceLineRepair;
    }

    /**
     * @param DepartmentChannel $chanel
     * @return InvoiceLine
     */
    private function transformMunicipality(DepartmentChannel $chanel)
    {
        $rate = $this->getMunFee($chanel);

        if (empty($this->itemMunFee)) {
            $this->itemMunFee = $this->itemRepository->findOneBy(['name' => self::ITEM_MUN_FEE_NAME]);
        }

        $municipality = $chanel->getDepartment()->getMunicipality()->getName();
        $inspectionQuantity = $this->inspectionQuantity[$chanel->getNamed()->getAlias() . $chanel->getId()] ?? 0;

        $invoiceLineMunicipality = new InvoiceLine();
        $invoiceLineMunicipality->setItem($this->itemMunFee);
        $invoiceLineMunicipality->setDescription(sprintf(self::ITEM_MUN_FEE_DESCRIPTION, $municipality));
        $invoiceLineMunicipality->setQuantity($inspectionQuantity);
        $invoiceLineMunicipality->setRate($rate);
        $invoiceLineMunicipality->setAmount($rate * $inspectionQuantity);
        $invoiceLineMunicipality->setSerialNumber($this->serialNumber++);

        return $invoiceLineMunicipality;
    }

    /**
     * @param Workorder $workorder
     * @return InvoiceLine
     */
    private function transformAfterHoursWorkCostToOther(Workorder $workorder)
    {
        if (empty($this->itemOther)) {
            $this->itemOther = $this->itemRepository->findOneBy(['name' => self::ITEM_OTHER_NAME]);
        }

        $invoiceLineOther = new InvoiceLine();
        $invoiceLineOther->setItem($this->itemOther);
        $invoiceLineOther->setDescription(self::ITEM_AFTER_HOURS_WORK_COST_DESCRIPTION);
        $invoiceLineOther->setQuantity(1);
        $invoiceLineOther->setRate($workorder->getAfterHoursWorkCost());
        $invoiceLineOther->setAmount($workorder->getAfterHoursWorkCost());
        $invoiceLineOther->setSerialNumber($this->serialNumber++);
        
        return $invoiceLineOther;
    }

    /**
     * @param Workorder $workorder
     * @return InvoiceLine
     */
    private function transformAdditionalCostToOther(Workorder $workorder)
    {
        if (empty($this->itemOther)) {
            $this->itemOther = $this->itemRepository->findOneBy(['name' => self::ITEM_OTHER_NAME]);
        }

        $description = 'Additional Cost';

        if (!empty($workorder->getAdditionalCostComment())) {
            $description .= ' ' . '-' . ' ' . $workorder->getAdditionalCostComment();
        }

        $invoiceLineOther = new InvoiceLine();
        $invoiceLineOther->setItem($this->itemOther);
        $invoiceLineOther->setDescription(
            sprintf(self::ITEM_ADDITIONAL_COST_DESCRIPTION, $workorder->getDivision()->getName())
        );
        $invoiceLineOther->setQuantity(1);
        $invoiceLineOther->setRate($workorder->getAdditionalServicesCost());
        $invoiceLineOther->setAmount($workorder->getAdditionalServicesCost());
        $invoiceLineOther->setDescription($description);
        $invoiceLineOther->setSerialNumber($this->serialNumber++);

        return $invoiceLineOther;
    }

    /**
     * @param Workorder $workorder
     * @return InvoiceLine
     */
    private function transformDiscount(Workorder $workorder)
    {
        $itemDiscount = $this->itemRepository->findOneBy(['name' => self::ITEM_DISCOUNT_NAME]);

        $invoiceLineDiscount = new InvoiceLine();
        $invoiceLineDiscount->setItem($itemDiscount);
        $invoiceLineDiscount->setRate($workorder->getDiscount());
        $invoiceLineDiscount->setAmount($workorder->getDiscount());
        $invoiceLineDiscount->setSerialNumber($this->serialNumber++);

        return $invoiceLineDiscount;
    }

    /**
     * @param DepartmentChannel $chanel
     * @return int
     */
    private function getMunFee(DepartmentChannel $chanel)
    {
        $res = 0;
        $sum = $chanel ? $chanel->getUploadFee() + $chanel->getProcessingFee() : 0;

        if ($chanel && $sum && $chanel->getActive() && $chanel->getIsDefault()
            && !$chanel->getDeleted()  && !$chanel->getFeesWillVary()
        ) {
            $res = $sum;
        }

        return (int) $res;
    }

    /**
     * @param Service | ServiceHistory $service
     */
    private function addDepartmentChanel($service)
    {
        /** @var DepartmentChannel $departmentChannel */
        $departmentChannel = $service->getDepartmentChannel();
        /** @var string $departmentChannelIndex */
        $departmentChannelIndex = $departmentChannel->getNamed()->getAlias()
            . $departmentChannel->getId();

        if ($this->getMunFee($departmentChannel) && $service->getNamed()->getIsNeedMunicipalityFee()) {
            $this->departmentChannels[$departmentChannelIndex] = $departmentChannel;

            if (!empty($this->inspectionQuantity[$departmentChannelIndex])) {
                $this->inspectionQuantity[$departmentChannelIndex] ++;
            } else {
                $this->inspectionQuantity[$departmentChannelIndex] = 1;
            }
        }
    }
}