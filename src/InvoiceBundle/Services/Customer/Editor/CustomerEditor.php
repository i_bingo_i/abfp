<?php

namespace InvoiceBundle\Services\Customer\Editor;

use AppBundle\Entity\Address;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Services\Customer\CustomerManager;
use QuickbooksBundle\Services\Quickbooks\Customer\ModifierCustomer;

// TODO: DEPRECATED CLASS
class CustomerEditor
{
    /** @var CustomerManager */
    private $customerManager;
    /** @var ModifierCustomer */
    private $modifierCustomer;

    /**
     * CustomerEditor constructor.
     * @param CustomerManager $customerManager
     * @param ModifierCustomer $modifierCustomer
     */
    public function __construct(CustomerManager $customerManager, ModifierCustomer $modifierCustomer)
    {
        $this->customerManager = $customerManager;
        $this->modifierCustomer = $modifierCustomer;
    }

    /**
     * @deprecated This method is Deprecated use ModifierCustomer -> modifyCustomer method
     *
     * @param Customer $customer
     * @param Address $address
     * @param $name
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function modify(Customer $customer, Address $address, $name)
    {
        if (!empty($customer->getAccountingSystemId()) && !empty($customer->getEditSequence())) {
            $this->editBillingAddressAndName($customer, $address, $name);
            $accountingSystemCustomer = $this->modifierCustomer->modifyCustomer($customer);
            $this->setAccountingSystemId($customer, $accountingSystemCustomer);

        } else {
            $this->editBillingAddressAndName($customer, $address, $name);

        }
    }

    /**
     * @param Customer $customer
     * @param Address $address
     * @param string $name
     */
    private function editBillingAddressAndName(Customer $customer, Address $address, $name)
    {
        $customer->setBillingAddress($address);
        $customer->setName($name);
    }

    /**
     * @deprecated This method is Deprecated
     *
     * @param Customer $customer
     * @param Customer $customerFromAccountingSystem
     */
    public function setAccountingSystemId(Customer $customer, Customer $customerFromAccountingSystem)
    {
        $customer->setAccountingSystemId($customerFromAccountingSystem->getAccountingSystemId());
        $customer->setEditSequence($customerFromAccountingSystem->getEditSequence());
    }
}
