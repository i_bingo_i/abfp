<?php

namespace InvoiceBundle\Services\Customer\Provider;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Company;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Services\Helper;

class CustomerACPProvider
{
    /** @var Customer */
    public $otherCompanyCustomer;
    /** @var CustomerCompanyProvider */
    private $customerCompanyProvider;
    /** @var CustomerContactPersonProvider */
    private $customerContactPersonProvider;
    /** @var Customer */
    private $contactPersonCustomer;
    /** @var Customer */
    private $contactPersonCompanyCustomer;
    /** @var Customer */
    private $accountCompanyCustomer;

    /**
     * CustomerACPProvider constructor.
     * @param CustomerCompanyProvider $customerCompanyProvider
     * @param CustomerContactPersonProvider $customerContactPersonProvider
     */
    public function __construct(
        CustomerCompanyProvider $customerCompanyProvider,
        CustomerContactPersonProvider $customerContactPersonProvider
    ) {
        $this->customerCompanyProvider = $customerCompanyProvider;
        $this->customerContactPersonProvider = $customerContactPersonProvider;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @param Company $otherCompany
     * @return array
     */
    public function getCustomers(AccountContactPerson $accountContactPerson, Company $otherCompany = null)
    {
        $res = [
            'Account Person' => [],
            'Person Company' => (new class()
            {

            }),
            'Account Company' => (new class()
            {

            }),
            'Other Company' => (new class()
            {

            })
        ];

        $this->getCustomersByACP($accountContactPerson, $otherCompany);

        if (!empty($accountContactPerson->getContactPerson())) {
            Helper::addToArrayIfNotNull(
                $this->contactPersonCustomer,
                $res,
                'Account Person'
            );

            if (!empty($accountContactPerson->getContactPerson()->getCompany())) {
                Helper::addToArrayIfNotNull(
                    $this->contactPersonCompanyCustomer,
                    $res,
                    'Person Company'
                );
            }
        }

        if (!empty($accountContactPerson->getAccount()->getCompany())) {
            Helper::addToArrayIfNotNull(
                $this->accountCompanyCustomer,
                $res,
                'Account Company'
            );
        }

        if (!empty($this->otherCompanyCustomer)) {
            Helper::addToArrayIfNotNull(
                $this->otherCompanyCustomer,
                $res,
                'Other Company'
            );
        }

        return $res;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @param Company $otherCompany
     */
    private function getCustomersByACP(AccountContactPerson $accountContactPerson, Company $otherCompany = null)
    {
        if ($accountContactPerson->getContactPerson()) {
            $this->contactPersonCustomer = $this->customerContactPersonProvider->getCustomer(
                $accountContactPerson->getContactPerson()
            );
        }

        if ($accountContactPerson->getContactPerson()->getCompany()) {
            $this->contactPersonCompanyCustomer = $this->customerCompanyProvider->getCustomer(
                $accountContactPerson->getContactPerson()->getCompany()
            );
        }

        if ($accountContactPerson->getAccount()->getCompany()) {
            $this->accountCompanyCustomer = $this->customerCompanyProvider->getCustomer(
                $accountContactPerson->getAccount()->getCompany()
            );
        }
        
        if ($otherCompany) {
            $this->otherCompanyCustomer = $this->customerCompanyProvider->getCustomer($otherCompany);

        } elseif ($this->isACPCustomerThisOtherCompany($accountContactPerson)) {
            $this->otherCompanyCustomer = $accountContactPerson->getCustomer();
        }
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @return bool
     */
    private function isACPCustomerThisOtherCompany(AccountContactPerson $accountContactPerson)
    {
        $res = true;

        if (!empty($accountContactPerson->getCustomer())) {
            if (!empty($this->contactPersonCustomer)
                &&  $this->contactPersonCustomer->getId() == $accountContactPerson->getCustomer()->getId()
            ) {
                $res = false;
            }

            if (!empty($this->contactPersonCompanyCustomer)
                && $this->contactPersonCompanyCustomer->getId() == $accountContactPerson->getCustomer()->getId()
            ) {
                $res = false;
            }

            if (!empty($this->accountCompanyCustomer)
                && $this->accountCompanyCustomer->getId() == $accountContactPerson->getCustomer()->getId()
            ) {
                $res = false;
            }
        }

        return $res;
    }
}
