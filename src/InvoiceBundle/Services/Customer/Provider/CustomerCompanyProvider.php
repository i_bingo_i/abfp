<?php

namespace InvoiceBundle\Services\Customer\Provider;

use AppBundle\Entity\Company;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\EntityType;
use InvoiceBundle\Repository\CustomerRepository;
use InvoiceBundle\Repository\EntityTypeRepository;

class CustomerCompanyProvider extends BaseCustomerProvider
{
    /**
     * CustomerCompanyProvider constructor.
     * @param CustomerRepository $customerRepository
     * @param EntityTypeRepository $entityTypeRepository
     */
    public function __construct(CustomerRepository $customerRepository, EntityTypeRepository $entityTypeRepository)
    {
        parent::__construct($customerRepository, $entityTypeRepository);

        $this->entityType = $this->entityTypeRepository->findOneBy(['alias' => EntityType::TYPE_COMPANY]);
    }

    /**
     * @param Company $company
     * @return null|Customer
     */
    public function getCustomer(Company $company)
    {
        /** @var Customer $customer */
        $customer = parent::getByEntityId($company->getId());

        return $customer;
    }
}