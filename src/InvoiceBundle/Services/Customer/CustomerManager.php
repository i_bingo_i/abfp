<?php

namespace InvoiceBundle\Services\Customer;

use InvoiceBundle\Entity\Customer;
use Doctrine\Common\Persistence\ObjectManager;

class CustomerManager
{
    /** @var ObjectManager */
    private $objectManager;

    /**
     * CustomerManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param Customer $customer
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Customer $customer)
    {
        $this->objectManager->persist($customer);
        $this->objectManager->flush();
    }

    public function flush()
    {
        $this->objectManager->flush();
    }
}