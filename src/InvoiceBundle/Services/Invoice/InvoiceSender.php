<?php

namespace InvoiceBundle\Services\Invoice;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Event\SentInvoiceToAccountingSystemEvent;
use InvoiceBundle\Manager\InvoiceManager;
use InvoiceBundle\Services\InvoiceLine\InvoiceLineHandler;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;
use QuickbooksBundle\Managers\InvoiceManager as QuickbooksInvoiceManager;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Response\Status;
use QuickbooksBundle\Services\Quickbooks\Invoice\CreatorInvoice;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use QuickbooksBundle\Validators\Request\Quickbook as QuickbookRequestValidator;
use LoggerBundle\Services\QuickbooksRequestLogger;

class InvoiceSender
{
    /** @var CreatorInvoice */
    private $creatorInvoice;
    /** @var InvoiceEditor */
    private $invoiceEditor;
    /** @var InvoiceManager */
    private $invoiceManager;
    /** @var InvoiceLineHandler */
    private $invoiceLineHandler;
    /** @var QuickbookRequestValidator */
    private $quickbookValidator;
    /** @var QuickbooksRequestLogger  */
    private $requestLogger;
    /** @var QuickbooksInvoiceManager  */
    private $quickbooksInvoiceManager;

    /**
     * InvoiceSender constructor.
     * @param CreatorInvoice $creatorInvoice
     * @param InvoiceEditor $invoiceEditor
     * @param InvoiceManager $invoiceManger
     * @param InvoiceLineHandler $invoiceLineHandler
     * @param EventDispatcherInterface $dispatcher
     * @param QuickbooksInvoiceManager $quickbooksInvoiceManager
     * @param QuickbookRequestValidator $quickbookValidator
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(
        CreatorInvoice $creatorInvoice,
        InvoiceEditor $invoiceEditor,
        InvoiceManager $invoiceManger,
        InvoiceLineHandler $invoiceLineHandler,
        EventDispatcherInterface $dispatcher,
        QuickbooksInvoiceManager $quickbooksInvoiceManager,
        QuickbookRequestValidator $quickbookValidator,
        QuickbooksRequestLogger $requestLogger
    ) {
        $this->creatorInvoice = $creatorInvoice;
        $this->invoiceEditor = $invoiceEditor;
        $this->invoiceManager = $invoiceManger;
        $this->invoiceLineHandler = $invoiceLineHandler;
        $this->dispatcher = $dispatcher;
        $this->quickbooksInvoiceManager = $quickbooksInvoiceManager;
        $this->quickbookValidator = $quickbookValidator;
        $this->requestLogger = $requestLogger;
    }

    /**
     * @param Invoices $invoice
     * @param bool $isNewInvoice
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendToAccountingSystem(Invoices $invoice, $isNewInvoice = false)
    {
        /** @var Response $response */
        $response = $this->quickbooksInvoiceManager->createInvoice($invoice);

        $requestLoggerDto = new QuickbooksRequestLoggerDTO();
        $requestLoggerDto->setEntity($invoice);
        $requestLoggerDto->setWorkorder($invoice->getWorkorder());
        $requestLoggerDto->setRequest($this->quickbooksInvoiceManager->getSourceBody());

        $logAction = $isNewInvoice ? QuickbooksRequestLogger::SUBMIT_NEW_INVOICE_TO_QB :
            QuickbooksRequestLogger::SUBMIT_DRAFT_INVOICE_TO_QB;

        /** @var Status $responseStatus */
        $responseStatus = $response->status();
        if ($responseStatus->code() != 0
            and $responseStatus->message() != 'A query request did not find a matching object in QuickBooks'
        ) {
            $invoice->setAccountingSystemId($response->get('TxnID'));
            $invoice->setEditSequence($response->get('EditSequence'));

            $event = new SentInvoiceToAccountingSystemEvent($invoice);
            $this->dispatcher->dispatch($event::SENT_INVOICE_TO_AS, $event);

            $requestLoggerDto->setResponse($this->quickbooksInvoiceManager->getResponseAttributes());
            $this->requestLogger->createPositiveRecord($requestLoggerDto, $logAction);
        } else {
            $requestLoggerDto->setResponse($responseStatus->message());
            $this->requestLogger->createNegativeRecord($requestLoggerDto, $logAction);
        }
    }
}
