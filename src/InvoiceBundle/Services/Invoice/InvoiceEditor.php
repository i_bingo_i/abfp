<?php

namespace InvoiceBundle\Services\Invoice;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Event\EditInvoicePaymentOverrideEvent;
use InvoiceBundle\Services\InvoiceStatus\InvoiceStatusProvider;
use InvoiceBundle\Services\PaymentTerm\PaymentTermProvider;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class InvoiceEditor
{
    /** @var InvoiceStatusProvider */
    private $invoiceStatusProvider;

    /**
     * InvoiceEditor constructor.
     * @param InvoiceStatusProvider $invoiceStatusProvider
     */
    public function __construct(
        InvoiceStatusProvider $invoiceStatusProvider
    ) {
        $this->invoiceStatusProvider = $invoiceStatusProvider;
    }

    /**
     * @param Invoices $invoices
     * @param Invoices $invFromAccSystem
     */
    public function modifyAfterSend(Invoices $invoices, Invoices $invFromAccSystem)
    {
        $invoices->setAccountingSystemId($invFromAccSystem->getAccountingSystemId());
        $invoices->setEditSequence($invFromAccSystem->getEditSequence());
        $invoices->setStatus($this->invoiceStatusProvider->getInQB());
        $invoices->setRefNumber($invFromAccSystem->getRefNumber());
    }
}