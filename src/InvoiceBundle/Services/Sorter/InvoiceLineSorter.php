<?php

namespace InvoiceBundle\Services\Sorter;

use InvoiceBundle\Entity\InvoiceLine;

class InvoiceLineSorter
{
    /** @var array */
    private $result = [];

    /**
     * @param array $invoiceLines
     * @return array
     */
    public function sort(array $invoiceLines)
    {
        foreach ($invoiceLines as $firstLevel) {
            $this->oneLevelProcessing($firstLevel);
        }

        return $this->result;
    }

    /**
     * @param $level
     */
    private function oneLevelProcessing($level)
    {
        if ($level instanceof InvoiceLine) {
            $this->oneLineProcessing($level);

        } elseif (is_array($level)) {
            foreach ($level as $item) {
                if ($item instanceof InvoiceLine) {
                    $this->oneLineProcessing($item);

                } elseif (is_array($level)) {
                    $this->oneLevelProcessing($item);
                }
            }
        }
    }

    /**
     * @param InvoiceLine $invoiceLine
     */
    public function oneLineProcessing(InvoiceLine $invoiceLine)
    {
        $this->result[] = $invoiceLine;

        uasort($this->result, function (InvoiceLine $a, InvoiceLine $b) {
            return $a->getSerialNumber() <=> $b->getSerialNumber();
        });
    }
}