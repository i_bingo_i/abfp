<?php

namespace InvoiceBundle\DataTransportObject\Requests;

use AppBundle\Entity\Workorder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CreateCustomInvoiceDTO
{
    /** @var Workorder */
    private $workorder;
    /** @var UploadedFile|null */
    private $file;
    /** @var string|null */
    private $comment;

    /**
     * @return Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @return null|UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return null|string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param Workorder $workorder
     * @return $this
     */
    public function setWorkorder(Workorder $workorder)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * @param UploadedFile|null $file
     * @return $this
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @param null $comment
     * @return $this
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }
}
