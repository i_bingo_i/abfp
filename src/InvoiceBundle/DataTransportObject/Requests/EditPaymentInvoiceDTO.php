<?php

namespace InvoiceBundle\DataTransportObject\Requests;

use AppBundle\Entity\Workorder;
use InvoiceBundle\Entity\Invoices;

class EditPaymentInvoiceDTO
{
    /** @var Invoices */
    private $invoice;
    /** @var double */
    private $overridePayment;

    /**
     * @param Invoices $invoice
     *
     * @return EditPaymentInvoiceDTO
     */
    public function setInvoice(Invoices $invoice): EditPaymentInvoiceDTO
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * @return Invoices
     */
    public function invoice()
    {
        return $this->invoice;
    }

    /**
     * @param double $overridePayment
     *
     * @return EditPaymentInvoiceDTO
     */
    public function setOverridePayment($overridePayment): EditPaymentInvoiceDTO
    {
        $this->overridePayment = $overridePayment;

        return $this;
    }

    /**
     * @return double
     */
    public function overridePayment()
    {
        return $this->overridePayment;
    }
}
