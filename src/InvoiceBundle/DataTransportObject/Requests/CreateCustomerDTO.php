<?php

namespace InvoiceBundle\DataTransportObject\Requests;

use AppBundle\Entity\Address;

class CreateCustomerDTO
{
    private $entity;
    /** @var Address */
    private $billingAddress;

    /**
     * @param $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param Address $billingAddress
     */
    public function setBillingAddress(Address $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }
}
