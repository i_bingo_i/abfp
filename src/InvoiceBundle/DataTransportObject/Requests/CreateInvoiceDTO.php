<?php

namespace InvoiceBundle\DataTransportObject\Requests;

use AppBundle\Entity\Workorder;
use InvoiceBundle\Entity\Invoices;

class CreateInvoiceDTO
{
    /** @var Invoices */
    private $invoice;
    /** @var Workorder */
    private $workorder;

    /**
     * @param Invoices $invoice
     *
     * @return CreateInvoiceDTO
     */
    public function setInvoice(Invoices $invoice): CreateInvoiceDTO
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * @return Invoices
     */
    public function invoice()
    {
        return $this->invoice;
    }

    /**
     * @param Workorder $workorder
     *
     * @return CreateInvoiceDTO
     */
    public function setWorkorder(Workorder $workorder): CreateInvoiceDTO
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * @return Workorder
     */
    public function workorder()
    {
        return $this->workorder;
    }
}
