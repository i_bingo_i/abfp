<?php

namespace InvoiceBundle\DataTransportObject;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use Exception;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Entity\InvoiceStatus;

class InvoiceDTO
{
    /** @var Workorder */
    private $workorder;
    /** @var AccountContactPerson */
    private $accountContactPerson;
    /** @var Address */
    private $billToAddress;
    /** @var InvoiceStatus */
    private $status;
    /** @var Address */
    private $shipToAddress;
    /** @var User */
    private $user;
    /** @var Job */
    private $job;
    /** @var string */
    private $inspector;


    /**
     * @return Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @return AccountContactPerson
     */
    public function getAccountContactPerson()
    {
        return $this->accountContactPerson;
    }

    /**
     * @return Address
     */
    public function getBillToAddress()
    {
        return $this->billToAddress;
    }

    /**
     * @return InvoiceStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return Address
     */
    public function getShipToAddress()
    {
        return $this->shipToAddress;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Workorder $workorder
     * @return $this
     */
    public function setWorkorder($workorder)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @return $this
     * @throws Exception
     */
    public function setAccountContactPerson(AccountContactPerson $accountContactPerson)
    {
        $this->accountContactPerson = $accountContactPerson;

        return $this;
    }

    /**
     * @param InvoiceStatus $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param Job $job
     * @return $this
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * @param Address $address
     * @return $this
     */
    public function setShipToAddress(Address $address)
    {
        $this->shipToAddress = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getInspector()
    {
        return $this->inspector;
    }

    /**
     * @param $inspector
     * @return $this
     */
    public function setInspector($inspector)
    {
        $this->inspector = $inspector;

        return $this;
    }

    /**
     * @param Address $address
     * @return $this
     */
    public function setBillingAddress(Address $address)
    {
        $this->billToAddress = $address;

        return $this;
    }
}