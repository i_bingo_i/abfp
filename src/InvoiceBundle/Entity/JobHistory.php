<?php

namespace InvoiceBundle\Entity;

/**
 * JobHistory
 */
class JobHistory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $accountingSystemId;

    /**
     * @var string
     */
    private $editSequence;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var \InvoiceBundle\Entity\Job
     */
    private $ownerEntity;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountingSystemId
     *
     * @param string $accountingSystemId
     *
     * @return JobHistory
     */
    public function setAccountingSystemId($accountingSystemId)
    {
        $this->accountingSystemId = $accountingSystemId;

        return $this;
    }

    /**
     * Get accountingSystemId
     *
     * @return string
     */
    public function getAccountingSystemId()
    {
        return $this->accountingSystemId;
    }

    /**
     * Set editSequence
     *
     * @param string $editSequence
     *
     * @return JobHistory
     */
    public function setEditSequence($editSequence)
    {
        $this->editSequence = $editSequence;

        return $this;
    }

    /**
     * Get editSequence
     *
     * @return string
     */
    public function getEditSequence()
    {
        return $this->editSequence;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return JobHistory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return JobHistory
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set ownerEntity
     *
     * @param \InvoiceBundle\Entity\Job $ownerEntity
     *
     * @return JobHistory
     */
    public function setOwnerEntity(\InvoiceBundle\Entity\Job $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \InvoiceBundle\Entity\Job
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }
}
