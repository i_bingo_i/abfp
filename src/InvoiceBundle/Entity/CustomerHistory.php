<?php

namespace InvoiceBundle\Entity;

use InvoiceBundle\Entity\EntityTrait\SetModificationDateTrait;

/**
 * CustomerHistory
 */
class CustomerHistory
{
    use SetModificationDateTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $accountingSystemId;

    /**
     * @var string
     */
    private $entityId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $editSequence;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \InvoiceBundle\Entity\EntityType
     */
    private $entityType;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $billingAddress;

    /**
     * @var \InvoiceBundle\Entity\Customer
     */
    private $ownerEntity;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountingSystemId
     *
     * @param string $accountingSystemId
     *
     * @return CustomerHistory
     */
    public function setAccountingSystemId($accountingSystemId)
    {
        $this->accountingSystemId = $accountingSystemId;

        return $this;
    }

    /**
     * Get accountingSystemId
     *
     * @return string
     */
    public function getAccountingSystemId()
    {
        return $this->accountingSystemId;
    }

    /**
     * Set entityId
     *
     * @param string $entityId
     *
     * @return CustomerHistory
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId
     *
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CustomerHistory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set editSequence
     *
     * @param string $editSequence
     *
     * @return CustomerHistory
     */
    public function setEditSequence($editSequence)
    {
        $this->editSequence = $editSequence;

        return $this;
    }

    /**
     * Get editSequence
     *
     * @return string
     */
    public function getEditSequence()
    {
        return $this->editSequence;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     *
     * @return CustomerHistory
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Set entityType
     *
     * @param \InvoiceBundle\Entity\EntityType $entityType
     *
     * @return CustomerHistory
     */
    public function setEntityType(\InvoiceBundle\Entity\EntityType $entityType = null)
    {
        $this->entityType = $entityType;

        return $this;
    }

    /**
     * Get entityType
     *
     * @return \InvoiceBundle\Entity\EntityType
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * Set billingAddress
     *
     * @param \AppBundle\Entity\Address $billingAddress
     *
     * @return CustomerHistory
     */
    public function setBillingAddress(\AppBundle\Entity\Address $billingAddress = null)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set ownerEntity
     *
     * @param \InvoiceBundle\Entity\Customer $ownerEntity
     *
     * @return CustomerHistory
     */
    public function setOwnerEntity(\InvoiceBundle\Entity\Customer $ownerEntity = null)
    {
        $this->ownerEntity = $ownerEntity;

        return $this;
    }

    /**
     * Get ownerEntity
     *
     * @return \InvoiceBundle\Entity\Customer
     */
    public function getOwnerEntity()
    {
        return $this->ownerEntity;
    }
}
