<?php

namespace InvoiceBundle\Entity;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use AppBundle\Entity\PaymentTerm;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Invoices
 */
class Invoices
{
    use DateTimeControlTrait;

    /** @var integer */
    private $id;
    /** @var string */
    private $file;
    /** @var DateTime */
    private $dateCreate;
    /** @var DateTime */
    private $dateUpdate;
    /** @var ArrayCollection  */
    private $logs;
    /** @var Workorder */
    private $workorder;
    /** @var InvoiceStatus */
    private $status;
    /** @var AccountContactPerson */
    private $accountContactPerson;
    /** @var PaymentTerm */
    private $paymentTerm;
    /** @var User */
    private $user;
    /** @var Address */
    private $billToAddress;
    /** @var InvoiceLine */
    private $lines;
    /** @var float */
    private $total;
    /** @var float */
    private $paymentApplied;
    /** @var float */
    private $balanceDue;
    /** @var float */
    private $overridePayment;
    /** @var DateTime */
    private $save;
    /** @var DateTime */
    private $refresh;
    /** @var int */
    private $editSequence;
    /** @var string */
    private $accountingSystemId;
    /** @var string */
    private $memo;
    /** @var Address */
    private $shipToAddress;
    /** @var Job */
    private $job;
    /** @var string */
    private $inspector;
    /** @var DateTime */
    private $invoiceDate;
    /** @var string */
    private $refNumber;
    /** @var Customer */
    private $customer;
    /** @var boolean */
    private $useCustomBillAddress = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->logs = new ArrayCollection();
        $this->lines = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Invoices
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set dateCreate
     *
     * @param DateTime $dateCreate
     *
     * @return Invoices
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param DateTime $dateUpdate
     *
     * @return Invoices
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Add log
     *
     * @param \AppBundle\Entity\WorkorderLog $log
     *
     * @return Invoices
     */
    public function addLog(\AppBundle\Entity\WorkorderLog $log)
    {
        $this->logs[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \AppBundle\Entity\WorkorderLog $log
     */
    public function removeLog(\AppBundle\Entity\WorkorderLog $log)
    {
        $this->logs->removeElement($log);
    }

    /**
     * Get logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Set workorder
     *
     * @param \AppBundle\Entity\Workorder $workorder
     *
     * @return Invoices
     */
    public function setWorkorder(\AppBundle\Entity\Workorder $workorder = null)
    {
        $this->workorder = $workorder;

        return $this;
    }

    /**
     * Get workorder
     *
     * @return \AppBundle\Entity\Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @return InvoiceStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param InvoiceStatus $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return AccountContactPerson
     */
    public function getAccountContactPerson()
    {
        return $this->accountContactPerson;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     */
    public function setAccountContactPerson($accountContactPerson)
    {
        $this->accountContactPerson = $accountContactPerson;
    }

    /**
     * @return PaymentTerm
     */
    public function getPaymentTerm()
    {
        return $this->paymentTerm;
    }

    /**
     * @param PaymentTerm $paymentTerm
     */
    public function setPaymentTerm($paymentTerm)
    {
        $this->paymentTerm = $paymentTerm;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param Address $address
     * @return $this
     */
    public function setBillToAddress(Address $address = null)
    {
        $this->billToAddress = $address;
    }

    /**
     * @return Address
     */
    public function getBillToAddress()
    {
        return $this->billToAddress;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @param InvoiceLine $lines
     */
    public function addLine($lines)
    {
        $this->lines->add($lines);
        $lines->setInvoice($this);
    }

    /**
     * @param InvoiceLine $line
     * @return $this
     */
    public function removeLine(InvoiceLine $line)
    {
        $this->lines->removeElement($line);

        return $this;
    }

    /**
     * @param ArrayCollection | array $lines
     */
    public function setLines($lines)
    {
        foreach ($lines as $line) {
            $this->addLine($line);
        }
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getPaymentApplied()
    {
        return $this->paymentApplied;
    }

    /**
     * @param float $paymentApplied
     */
    public function setPaymentApplied($paymentApplied)
    {
        $this->paymentApplied = $paymentApplied;
    }

    /**
     * @return float
     */
    public function getBalanceDue()
    {
        return $this->balanceDue;
    }

    /**
     * @param float $balanceDue
     */
    public function setBalanceDue($balanceDue)
    {
        $this->balanceDue = $balanceDue;
    }

    /**
     * @return float
     */
    public function getOverridePayment()
    {
        return $this->overridePayment;
    }

    /**
     * @param float $overridePayment
     */
    public function setOverridePayment($overridePayment)
    {
        $this->overridePayment = $overridePayment;
    }

    /**
     * @return DateTime
     */
    public function getSave()
    {
        return $this->save;
    }

    /**
     * @param DateTime $save
     */
    public function setSave($save)
    {
        $this->save = $save;
    }

    /**
     * @return DateTime
     */
    public function getRefresh()
    {
        return $this->refresh;
    }

    /**
     * @param DateTime $refresh
     */
    public function setRefresh($refresh)
    {
        $this->refresh = $refresh;
    }

    /**
     * @return int
     */
    public function getEditSequence()
    {
        return $this->editSequence;
    }

    /**
     * @param int $editSequence
     */
    public function setEditSequence($editSequence)
    {
        $this->editSequence = $editSequence;
    }

    /**
     * @return string
     */
    public function getAccountingSystemId()
    {
        return $this->accountingSystemId;
    }

    /**
     * @param string $accountingSystemId
     */
    public function setAccountingSystemId($accountingSystemId)
    {
        $this->accountingSystemId = $accountingSystemId;
    }

    /**
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * @param string $memo
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    }

    /**
     * @param Address $shipToAddress
     * @return $this
     */
    public function setShipToAddress(Address $shipToAddress = null)
    {
        $this->shipToAddress = $shipToAddress;
    }

    /**
     * @return Address
     */
    public function getShipToAddress()
    {
        return $this->shipToAddress;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Job $job
     */
    public function setJob(Job $job = null)
    {
        $this->job = $job;
    }

    /**
     * @return string
     */
    public function getInspector()
    {
        return $this->inspector;
    }

    /**
     * @param string|null $inspector
     */
    public function setInspector($inspector = null)
    {
        $this->inspector = $inspector;
    }

    /**
     * @return DateTime
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * @param DateTime $invoiceDate
     */
    public function setInvoiceDate(DateTime $invoiceDate = null)
    {
        $this->invoiceDate = $invoiceDate;
    }

    /**
     * @return string
     */
    public function getRefNumber()
    {
        return $this->refNumber;
    }

    /**
     * @param string $refNumber
     */
    public function setRefNumber($refNumber)
    {
        $this->refNumber = $refNumber;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;
    }

    /**
     * Set useCustomBillAddress
     *
     * @param boolean $useCustomBillAddress
     *
     * @return Invoices
     */
    public function setUseCustomBillAddress($useCustomBillAddress)
    {
        $this->useCustomBillAddress = $useCustomBillAddress;

        return $this;
    }

    /**
     * Get useCustomBillAddress
     *
     * @return boolean
     */
    public function getUseCustomBillAddress()
    {
        return $this->useCustomBillAddress;
    }
}
