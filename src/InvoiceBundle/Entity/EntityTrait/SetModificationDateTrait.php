<?php

namespace InvoiceBundle\Entity\EntityTrait;

trait SetModificationDateTrait
{
    /**
     * @return $this
     * @throws \Exception
     */
    public function setModificationDateValue()
    {
        $this->modificationDate = new \DateTime();

        return $this;
    }
}
