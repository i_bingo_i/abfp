<?php

namespace InvoiceBundle\Entity;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityTrait\DateTimeControlTrait;
use DateTime;

class Job
{
    use DateTimeControlTrait;

    /** @var integer */
    private $id;
    /** @var string */
    private $accountingSystemId;
    /** @var string */
    private $editSequence;
    /** @var Customer */
    private $customer;
    /** @var Account */
    private $account;
    /** @var DeviceCategory */
    private $division;
    /** @var DateTime */
    private $dateCreate;
    /** @var DateTime */
    private $dateUpdate;
    /** @var string */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccountingSystemId()
    {
        return $this->accountingSystemId;
    }

    /**
     * @param string $accountingSystemId
     * @return $this
     */
    public function setAccountingSystemId($accountingSystemId)
    {
        $this->accountingSystemId = $accountingSystemId;

        return $this;
    }

    /**
     * @return string
     */
    public function getEditSequence()
    {
        return $this->editSequence;
    }

    /**
     * @param string $editSequence
     */
    public function setEditSequence($editSequence = null)
    {
        $this->editSequence = $editSequence;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return $this
     */
    public function setAccount(Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return DeviceCategory
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @param DeviceCategory $division
     * @return $this
     */
    public function setDivision(DeviceCategory $division = null)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * @param DateTime $dateCreate
     * @return $this
     */
    public function setDateCreate(DateTime $dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @param DateTime $dateUpdate
     * @return $this
     */
    public function setDateUpdate(DateTime $dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     * @return $this
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }
}
