<?php

namespace InvoiceBundle\Entity;

class InvoiceStatus
{
    public const STATUS_DRAFT = 'draft';
    public const STATUS_IN_QB = 'in_qb';
    public const VOIDED = 'voided';
    public const CUSTOM = 'custom';
    public const DELETED = 'deleted';

    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $alias;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }
}
