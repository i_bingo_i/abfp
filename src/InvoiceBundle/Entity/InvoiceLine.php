<?php

namespace InvoiceBundle\Entity;

use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\Entity\Item;

class InvoiceLine
{
    /** @var int */
    private $id;

    /** @var string */
    private $description;

    /** @var int */
    private $quantity;

    /** @var float */
    private $rate;

    /** @var float */
    private $amount;

    /** @var InvoiceLine */
    private $previous;

    /** @var InvoiceLine */
    private $next;

    /** @var Item */
    private $item;

    /** @var string */
    private $accountingSystemId;

    /** @var Invoices */
    private $invoice;
    /** @var integer */
    private $serialNumber;

    /**
     * @return string
     */
    public function getAccountingSystemId()
    {
        return $this->accountingSystemId;
    }

    /**
     * @param string $accountingSystemId
     */
    public function setAccountingSystemId($accountingSystemId)
    {
        $this->accountingSystemId = $accountingSystemId;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item = null)
    {
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->item->getName();
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description = null)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity = null)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     */
    public function setRate($rate = null)
    {
        $this->rate = $rate;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;
    }

    /**
     * @return InvoiceLine
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * @param InvoiceLine $previous
     */
    public function setPrevious($previous = null)
    {
        $this->previous = $previous;
    }

    /**
     * @return InvoiceLine
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param InvoiceLine $next
     */
    public function setNext($next = null)
    {
        $this->next = $next;
    }

    /**
     * @return Invoices
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param Invoices $invoice
     */
    public function setInvoice($invoice = null)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return int
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * @param int $serialNumber
     */
    public function setSerialNumber($serialNumber = null)
    {
        $this->serialNumber = $serialNumber;
    }
}
