<?php

namespace InvoiceBundle\Manager;

use AppBundle\Entity\Account;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Repository\JobRepository;
use Doctrine\Common\Persistence\ObjectManager;
use InvoiceBundle\Traits\JobName;

class JobManager
{
    use JobName;

    /** @var ObjectManager */
    private $objectManager;
    /** @var JobRepository */
    private $jobRepository;

    /**
     * JobManager constructor.
     *
     * @param ObjectManager $objectManager
     * @param JobRepository $jobRepository
     */
    public function __construct(ObjectManager $objectManager, JobRepository $jobRepository)
    {
        $this->objectManager = $objectManager;
        $this->jobRepository = $jobRepository;
    }

    /**
     * @param Account $account
     */
    public function refreshForAccount(Account $account)
    {
        /** @var Job[] $accountJobs */
        $accountJobs = $this->jobRepository->findBy(['account' => $account]);
        /** @var Job $job */
        foreach ($accountJobs as $job) {
            $newName = self::makeName($account, $job->getDivision());
            $job->setName($newName);
            $this->objectManager->persist($job);
        }

        $this->objectManager->flush();
    }
}
