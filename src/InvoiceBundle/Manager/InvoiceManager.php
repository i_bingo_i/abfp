<?php

namespace InvoiceBundle\Manager;

use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use InvoiceBundle\Entity\Invoices;
use AppBundle\Entity\Letter;
use AppBundle\Entity\Workorder;
use InvoiceBundle\Entity\InvoiceStatus;
use AppBundle\Services\Letter\LetterCreator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;

class InvoiceManager
{
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContainerInterface */
    private $container;
    /** @var InvoiceStatusRepository */
    private $invoiceStatusRepository;
    /** @var Session */
    private $session;

    /**
     * InvoiceManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->container = $container;
        $this->session = $this->container->get("session");
        $this->invoiceStatusRepository = $this->objectManager->getRepository('InvoiceBundle:InvoiceStatus');
    }

    /**
     * @param Workorder $workorder
     * @param string $file
     * @return Invoices
     */
    public function create(Workorder $workorder, string $file)
    {
        /** @var InvoiceStatus $invoiceStatusDraft */
        $invoiceStatusDraft = $this->invoiceStatusRepository->findOneBy(['alias' => InvoiceStatus::STATUS_DRAFT]);

        $invoice = new Invoices();
        $invoice->setFile($file);
        $invoice->setWorkorder($workorder);
        $invoice->setStatus($invoiceStatusDraft);

        $this->objectManager->persist($invoice);
        $this->objectManager->flush();

        return $invoice;
    }

    /**
     * @param Invoices $invoices
     */
    public function save(Invoices $invoices)
    {
        $this->objectManager->persist($invoices);
        $this->objectManager->flush();
    }

    /**
     * @param Invoices|null $invoices
     */
    public function flush(Invoices $invoices = null)
    {
        $this->objectManager->flush($invoices);
    }
}