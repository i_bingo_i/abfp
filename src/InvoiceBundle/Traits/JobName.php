<?php

namespace InvoiceBundle\Traits;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;

trait JobName
{
    /**
     * @param Account $account
     * @param DeviceCategory $category
     *
     * @return string
     */
    public static function makeName(Account $account, DeviceCategory $category): string
    {
        $firstCharName = substr($category->getName(), 0, 1);
        $jobNamePrefix = '['.$account->getId() .$firstCharName.']';
        $jobName = $jobNamePrefix . ' ' . $account->getName();

        return $jobName;
    }
}
