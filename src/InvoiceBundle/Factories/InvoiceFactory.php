<?php

namespace InvoiceBundle\Factories;

use InvoiceBundle\DataTransportObject\Requests\CreateInvoiceDTO;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\DataTransportObject\InvoiceDTO;

class InvoiceFactory
{
    /**
     * @param InvoiceDTO $invoiceDTO
     * @return Invoices
     */
    public static function make(InvoiceDTO $invoiceDTO)
    {
        $invoice = new Invoices();
        $invoice->setWorkorder($invoiceDTO->getWorkorder());
        $invoice->setUser($invoiceDTO->getUser());
        $invoice->setStatus($invoiceDTO->getStatus());
        $invoice->setShipToAddress($invoiceDTO->getShipToAddress());

        if (!empty($invoiceDTO->getAccountContactPerson())) {
            $invoice->setAccountContactPerson($invoiceDTO->getAccountContactPerson());
        }

        if (!empty($invoiceDTO->getBillToAddress())) {
            $invoice->setBillToAddress($invoiceDTO->getBillToAddress());
        }

        if (!empty($invoiceDTO->getJob())) {
            $invoice->setJob($invoiceDTO->getJob());
        }

        $invoice->setInspector($invoiceDTO->getInspector());

        return $invoice;
    }
}