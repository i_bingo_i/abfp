<?php

namespace InvoiceBundle\Factories;

use AppBundle\Entity\Address;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\EntityType;

class CustomerFactory
{
    /**
     * @param int $entityId
     * @param string $name
     * @param EntityType $entityType
     * @param Address $billingAddress
     *
     * @return mixed
     */
    public static function make($entityId, $name, EntityType $entityType, ?Address $billingAddress = null)
    {
        $customer = new Customer();

        $customer->setEntityId($entityId);
        $customer->setName($name);
        $customer->setEntityType($entityType);
        $customer->setBillingAddress($billingAddress);

        return $customer;
    }
}