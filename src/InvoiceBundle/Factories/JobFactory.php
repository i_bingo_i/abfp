<?php

namespace InvoiceBundle\Factories;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Traits\JobName;

class JobFactory
{
    use JobName;

    /**
     * @param Account $account
     * @param Customer $customer
     * @param DeviceCategory $category
     *
     * @return Job
     */
    public static function make(Account $account, Customer $customer, DeviceCategory $category)
    {
        $jobName = self::makeName($account, $category);

        /** @var Job $job */
        $job = new Job();
        $job->setAccount($account)
            ->setCustomer($customer)
            ->setDivision($category)
            ->setName($jobName);

        return $job;
    }
}
