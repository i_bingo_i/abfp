<?php

namespace InvoiceBundle\Factories;

use AppBundle\Entity\Account;
use AppBundle\Entity\Address;

class ShipToAddressFactory
{
    /**
     * @param Account $account
     * @return Address
     */
    public static function make(Account $account)
    {
        $typeAccount = $account->getType()->getAlias() == 'account' ? 'Site' : 'Group';
        $address = $account->getName() . PHP_EOL
            . $account->getAddress()->getAddress() . PHP_EOL
            . $typeAccount. ' Account ID - ' . $account->getId();

        $shipToAddress = new Address();
        $shipToAddress->setAddress($address);
        $shipToAddress->setZip($account->getAddress()->getZip());
        $shipToAddress->setState($account->getAddress()->getState());
        $shipToAddress->setCity($account->getAddress()->getCity());

        return $shipToAddress;
    }
}