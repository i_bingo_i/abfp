<?php

namespace InvoiceBundle\Factories;

use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\CustomerHistory;

class CustomerHistoryFactory
{
    /**
     * @param Customer $customer
     *
     * @return CustomerHistory
     */
    public static function make(Customer $customer)
    {
        /** @var CustomerHistory $customerHistory */
        $customerHistory = new CustomerHistory();

        $customerHistory->setOwnerEntity($customer);
        $customerHistory->setAccountingSystemId($customer->getAccountingSystemId());
        $customerHistory->setEditSequence($customer->getEditSequence());
        $customerHistory->setEntityId($customer->getEntityId());
        $customerHistory->setName($customer->getName());
        $customerHistory->setEntityType($customer->getEntityType());
        $customerHistory->setBillingAddress($customer->getBillingAddress());

        return $customerHistory;
    }
}
