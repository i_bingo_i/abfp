<?php

namespace InvoiceBundle\Factories;

use InvoiceBundle\Entity\Job;
use InvoiceBundle\Entity\JobHistory;

class JobHistoryFactory
{
    /**
     * @param Job $job
     *
     * @return JobHistory
     */
    public static function make(Job $job)
    {
        /** @var JobHistory $jobHistory */
        $jobHistory = new JobHistory();

        $jobHistory->setOwnerEntity($job);
        $jobHistory->setAccountingSystemId($job->getAccountingSystemId());
        $jobHistory->setEditSequence($job->getEditSequence());
        $jobHistory->setName($job->getName());
        $jobHistory->setDateUpdate($job->getDateUpdate());

        return $jobHistory;
    }
}
