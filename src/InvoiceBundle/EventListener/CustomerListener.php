<?php

namespace InvoiceBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PostFlushEventArgs;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\CustomerHistory;
use InvoiceBundle\Factories\CustomerHistoryFactory;

class CustomerListener
{
    private $customerHistories = [];

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var Customer $entity */
        $entity = $args->getObject();

        if ($entity instanceof Customer) {
            /** @var CustomerHistory $customerHistory */
            $customerHistory = CustomerHistoryFactory::make($entity);
            $entityManager = $args->getObjectManager();
            $entityManager->persist($customerHistory);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        /** @var Customer $entity */
        $entity = $args->getObject();

        if ($entity instanceof Customer) {
            $this->customerHistories[] = CustomerHistoryFactory::make($entity);
        }
    }

    /**
     * @param PostFlushEventArgs $args
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        if (! empty($this->customerHistories)) {
            /** @var EntityManager $entityManager */
            $entityManager = $args->getEntityManager();

            /** @var CustomerHistory $history */
            foreach ($this->customerHistories as $history) {
                $entityManager->persist($history);
            }

            $this->customerHistories = [];
            $entityManager->flush();
        }
    }
}
