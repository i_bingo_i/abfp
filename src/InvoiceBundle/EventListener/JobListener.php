<?php

namespace InvoiceBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PostFlushEventArgs;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Entity\JobHistory;
use InvoiceBundle\Factories\JobHistoryFactory;

class JobListener
{
    private $jobHistories = [];

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var Job $entity */
        $entity = $args->getObject();

        if ($entity instanceof Job) {
            /** @var JobHistory $jobHistory */
            $jobHistory = JobHistoryFactory::make($entity);
            $entityManager = $args->getObjectManager();
            $entityManager->persist($jobHistory);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        /** @var Job $entity */
        $entity = $args->getObject();

        if ($entity instanceof Job) {
            $this->jobHistories[] = JobHistoryFactory::make($entity);
        }
    }

    /**
     * @param PostFlushEventArgs $args
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        if (! empty($this->jobHistories)) {
            /** @var EntityManager $entityManager */
            $entityManager = $args->getEntityManager();

            /** @var JobHistory $history */
            foreach ($this->jobHistories as $history) {
                $entityManager->persist($history);
            }

            $this->jobHistories = [];
            $entityManager->flush();
        }
    }
}
