<?php

namespace Interfaces;

interface IdInterface
{
    public function getId();
}
