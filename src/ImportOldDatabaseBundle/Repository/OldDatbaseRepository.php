<?php

namespace ImportOldDatabaseBundle\Repository;

use Doctrine\ORM\EntityManager;
use Realestate\MssqlBundle\Driver\PDODblib\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OldDatbaseRepository
{
    /** @var EntityManager */
    protected $oldEm;
    /** @var  Connection */
    protected $oldConnection;
    /** @var ContainerInterface */
    private $container;
    /** @var EntityManager */
    private $entityManager;
    /** @var \Doctrine\DBAL\Connection */
    private $connection;


    /**
     * OldDatbaseRepository constructor.
     * @param EntityManager $oldEm
     * @param ContainerInterface $container
     */
    public function __construct($oldEm, ContainerInterface $container)
    {
        $this->oldEm = $oldEm;
        $this->oldConnection = $oldEm->getConnection();

        $this->container = $container;
        $this->entityManager = $this->container->get('app.entity_manager');
        $this->connection = $this->entityManager->getConnection();
    }

    /**
     * @return array
     */
    public function getAllDatabaseTables()
    {
        $getAllTablesQuery = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'";
        $oldStatement = $this->oldConnection->prepare($getAllTablesQuery);
        $oldStatement->execute();
        $oldDBTables = $oldStatement->fetchAll();

        return $oldDBTables;
    }


    /**
     * @param $tableName
     * @param string $conditions
     * @return array
     */
    public function getAllTablesRowsByTableName($tableName, $conditions = "")
    {
        $query = 'SELECT * FROM ' . $tableName . $conditions;
        $oldStatement = $this->oldConnection->prepare($query);
        $oldStatement->execute();
        $oldResults = $oldStatement->fetchAll();

        return $oldResults;
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function queryProcessContractorUserHistory()
    {
        $sql = 'INSERT INTO contractor_user_history(`active`, `title`, `personal_email`, `personal_phone`, `work_phone`,
                  `fax`, `user`, `contractor`, `owner_entity_id`, `date_save`)
                SELECT cu.active, cu.title, cu.personal_email, cu.personal_phone, cu.work_phone, cu.fax, cu.user, 
                  cu.contractor, cu.id, NOW()
                FROM contractor_user AS cu';

        return  $this->connection->executeUpdate($sql);
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeAllContractorUserHistory()
    {
        $sql = 'DELETE FROM contractor_user_history';

        return  $this->connection->executeUpdate($sql);
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function queryProcessContactPersonHistory()
    {
        $sql = 'INSERT INTO contact_person_history (`owner_entity_id`, `first_name`, `last_name`, `title`, `email`, 
                    `phone`, `ext`, `cell`, `fax`, `cod`, `notes`, `date_create`, `date_update`, `company`)
                  SELECT cp.id, cp.first_name, cp.last_name, cp.title, cp.email, cp.phone, cp.ext, cp.cell, cp.fax, 
                    cp.cod, cp.notes, cp.date_create, cp.date_update, cp.company
                  FROM contact_person as cp';

        return  $this->connection->executeUpdate($sql);
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeAllContactPersonHistory()
    {
        $sql = 'DELETE FROM contact_person_history';

        return  $this->connection->executeUpdate($sql);
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function queryProcessAccountHistory()
    {
        $sql = 'INSERT INTO account_history (`type`, `address_id`, `parent_id`, `client_type_id`, `owner_entity_id`,
                    `name`, `notes`, `date_create`, `date_update`, `company`, `website`, `date_save`, `special_discount`)
                  SELECT a.type, a.address_id, a.parent_id, a.client_type_id, a.id, a.name, a.notes, a.date_create,
                    a.date_update, a.company, a.website, a.date_create, a.special_discount
                  FROM account AS a';

        return  $this->connection->executeUpdate($sql);
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeAllAccountHistory()
    {
        $sql = 'DELETE FROM account_history';

        return  $this->connection->executeUpdate($sql);
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function queryProcessAccountContactPersonHistory()
    {
        $sql = 'INSERT INTO account_contact_person_history (`owner_entity_id`, `deleted`, `date_create`, `date_update`,
                    `access`, `access_primary`, `payment`, `payment_primary`, `authorizer`, `sending_address_id`,
                    `account`, `contact_person`)
                  SELECT acp.id, acp.deleted, acp.date_create, acp.date_update, acp.access, acp.access_primary,
                    acp.payment, acp.payment_primary, acp.authorizer, acp.sending_address_id, acp.account, 
                    (SELECT cph.id 
                      FROM contact_person_history as cph 
                      WHERE cph.owner_entity_id = acp.contact_person 
                      ORDER BY cph.date_update LIMIT 1)
                  FROM account_contact_person as acp';

        return  $this->connection->executeUpdate($sql);
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeAllAccountContactPersonHistory()
    {
        $sql = 'DELETE FROM account_contact_person_history';

        return  $this->connection->executeUpdate($sql);
    }

    /**
     * @param $tableName
     * @param $fields
     * @param $conditions
     * @return array
     */
    public function getDataByCondition($tableName, $fields = '*', $conditions = '')
    {
        $query = 'SELECT ' . $fields . ' FROM ' . $tableName . ' ' . $conditions;
        $oldStatement = $this->oldConnection->prepare($query);
        $oldStatement->execute();
        $oldResults = $oldStatement->fetchAll();

        return $oldResults;
    }


    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function changeImagesLinksForDevices()
    {
        $sql = "UPDATE device SET 
                photo = REPLACE(photo, 'https://service.americanbackflowprevention.com:10443/DeviceImg/', '/uploads/devices/')
                ";

        return $this->connection->executeUpdate($sql);
    }
}