<?php

namespace ImportOldDatabaseBundle\Managers;

use AppBundle\Entity\Company;
use AppBundle\Entity\ImportErrorMessages;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportErrorMessageManager
{
    private const ERROR_MESSAGE_MAS_LEVEL = 'Writing is ID %s in MasLevel table not correct. %s';
    private const ERROR_MESSAGE_SITE_LEVEL = 'Writing is ID %s in Main1 table not correct. %s';
    private const ERROR_MESSAGE_ORG_LEVEL = 'Writing is ID %s in OrgLevel table not correct. %s';
    private const ACCOUNT_NAME = 'Account name absent.';
    private const COMPANY_NAME = 'Company name absent.';
    private const EMPTY_COMPANY_ADDRESS = 'Empty company address.';
    private const ZIP_CODE = 'In zip code not only numbers.';
    private const PHONE_NUMBER = 'Incorrect contact person format phone number.';
    private const ABSENT_COMPANY_ADDRESS = 'Company Address was not found while linking Account Contact Person to the
        Account, Contact Person Personal Address is used by default.';

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;

    /**
     * ImportErrorMessageManager constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
    }

    /**
     * @param string $message
     */
    public function setErrorMessage(string $message)
    {
        $errorMessage = new ImportErrorMessages();
        $errorMessage->setMessage($message);

        $this->om->persist($errorMessage);
//        $this->om->flush();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function checkMasLevelData(array $data)
    {
        $error = false;
        if (empty($data['MasName'])) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_MAS_LEVEL, $data['MasID'], self::ACCOUNT_NAME));
            $error = true;
        }
        if (!preg_match('/^\d+$/', $data['MasZip'])) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_MAS_LEVEL, $data['MasID'], self::ZIP_CODE));
        }
        if ((!empty($data['MasConPhone1'])
                && !preg_match('/^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/', $data['MasConPhone1']))
            ||
            (!empty($data['MasConPhone2'])
                && !preg_match('/^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/', $data['MasConPhone2']))
        ) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_MAS_LEVEL, $data['MasID'], self::PHONE_NUMBER));
        }

        return $error;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function checkSiteData(array $data)
    {
        $error = false;
        if (empty($data['SiteName'])) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_SITE_LEVEL, $data['AccountID'], self::ACCOUNT_NAME));
            //$error = true;
        }
        if (!preg_match('/^\d+$/', $data['SiteZip'])) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_SITE_LEVEL, $data['AccountID'], self::ZIP_CODE));
        }
        if ((!empty($data['SiteConPhone'])
                && !preg_match('/^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/', $data['SiteConPhone']))
            ||
            (!empty($data['SiteConPhone2'])
                && !preg_match('/^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/', $data['SiteConPhone2']))
        ) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_SITE_LEVEL, $data['AccountID'], self::PHONE_NUMBER));
        }

        return $error;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function checkOrgData(array $data)
    {
        $error = false;
        if (empty($data['OrgName'])) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_ORG_LEVEL, $data['OrgID'], self::COMPANY_NAME));
            $error = true;
        }

        if ($this->checkEmptyOrgAddress($data)) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_ORG_LEVEL, $data['OrgID'], self::EMPTY_COMPANY_ADDRESS));
        }

        if (!preg_match('/^\d+$/', $data['OrgZip'])) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_ORG_LEVEL, $data['OrgID'], self::ZIP_CODE));
        }
        if ((!empty($data['OrgPhone'])
                && !preg_match('/^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/', $data['OrgPhone']))
            ||
            (!empty($data['OrgPhone2'])
                && !preg_match('/^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/', $data['OrgPhone2']))
        ) {
            $this->setErrorMessage(sprintf(self::ERROR_MESSAGE_ORG_LEVEL, $data['OrgID'], self::PHONE_NUMBER));
        }
        if (empty($data['OrgAddress1'])) {
            $this->setErrorMessage(
                sprintf(self::ERROR_MESSAGE_ORG_LEVEL, $data['OrgID'], self::ABSENT_COMPANY_ADDRESS)
            );
        }

        return $error;
    }

    /**
     * @param array $oldCompany
     * @return bool
     */
    public function checkEmptyOrgAddress(array $oldCompany)
    {
        if (empty($oldCompany['OrgCity']) and
            empty($oldCompany['OrgZip']) and
            empty($oldCompany['OrgAddress1']) and
            empty($oldCompany['OrgState'])
        ) {
            return true;
        }

        return false;
    }
}
