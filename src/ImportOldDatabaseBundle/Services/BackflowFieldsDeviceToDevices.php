<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output;
use AppBundle\Entity\Repository\DynamicFieldDropboxChoicesRepository;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;

class BackflowFieldsDeviceToDevices
{
    private const IMPORT_TABLE = 'abfp.dbo.Device';
    private const NEED_FLUSH_ON = 1000;

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;

    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var  OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var DynamicFieldDropboxChoicesRepository */
    private $choicesRepository;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var array */
    private $dynamicFields = [
        'size' => [],
        'make' => []
    ];

    /**
     * MasLevelToAccount constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;

        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->deviceRepository = $this->om->getRepository('AppBundle:Device');
        $this->choicesRepository = $this->om->getRepository('AppBundle:DynamicFieldDropboxChoices');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');

        $this->dynamicFields['size'] = [
            '1/4' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1_slash_4']),
            '1/2' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1_slash_2']),
            '3/4' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_3_slash_4']),
            '1' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1']),
            '1 1/4' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1_dot_25']),
            '1 1/2' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1_dot_5']),
            '2' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_2']),
            '2 1/2' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_2_dot_5']),
            '3' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_3']),
            '4' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_4']),
            '6' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_6']),
            '8' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_8']),
            '10' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_10']),
            '12' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_12'])
        ];

        $this->dynamicFields['make'] = [
            'Ames' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_ames']),
            'Febco' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_febco']),
            'Watts' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_watts']),
            'Wilkins' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_wilkins']),
            'Rainbird' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_rainbird']),
            'Conbraco' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_conbraco']),
            'Apollo' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_apollo']),
            'Colt' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_colt']),
            'Maxim' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_maxim']),
            'Beeco' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_beeco']),
            'Backflow Direct' => $this->choicesRepository->findOneBy(
                ['alias' => 'backflow_device_make_backflow_direct']
            ),
            'Flomatic' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_flomatic']),
            'Flowmatic' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_flomatic']),
            'Swing CHK' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_swing_chk']),
            'Zurn' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_zurn']),
            'Verify' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_verify']),
            'Cash Acme' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_cash_acme']),
            'Globe' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_globe']),
            'Cla-Val' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_cla_val']),
            'Buckner' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_buckner']),
            'Hersey' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_hersey']),
            'Other' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_other']),
        ];
    }

    /**
     * @return int
     */
    public function import()
    {
        $oldDevices = $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::IMPORT_TABLE);
        $countImportedDevices = 0;
        $output = new Output\ConsoleOutput();
        $progress = new ProgressBar($output, count($oldDevices));
        $progress->start();

        foreach ($oldDevices as $oldDevice) {
            try {
                /** @var Account $account */
                $device = $this->deviceRepository->findOneBy(['oldDeviceId' => $oldDevice['DevID']]);
                if (!$device) {
                    $this->importErrorMessageManager->setErrorMessage('[import devices error][OldDeviceId:' . $oldDevice['DevID'] . ' ]: dont find device with DevID: ' . $oldDevice['DevID']);
                    continue;
                }

                /** @var DynamicFieldValue $field */
                foreach ($device->getFields() as $field) {
                    $wasSet = false;

                    if ($field->getField()->getAlias() == 'backflow_size'
                        and ($oldDevice['DevSize'] != 'NA' or $oldDevice['DevSize'] != '(Blanks)')
                        and is_null($field->getOptionValue())
                    ) {
                        $size = $this->getSize($oldDevice);
                        $field->setOptionValue($size);
                        $wasSet = true;
                    }

                    if ($field->getField()->getAlias() == 'backflow_make'
                        and $oldDevice['DevMake'] != 'NA'
                        and is_null($field->getOptionValue())
                    ) {
                        /** @var DynamicFieldDropboxChoices $make */
                        $make = $this->getMake($oldDevice);
                        $field->setOptionValue($make);
                        $wasSet = true;
                    }

                    if ($wasSet) {
                        $this->om->persist($field);
                    }
                }

                $countImportedDevices++;

                $progress->advance();

                $this->needLoad($countImportedDevices);
                //                    if ($countImportedDevices > 10000) {
                //                        break;
                //                    }

            } catch (\Exception $exception) {
                $this->importErrorMessageManager->setErrorMessage('[import devices error][OldDeviceId:' . $oldDevice['DevID'] . ' ]: error: "' . $exception->getMessage() . '" AccountId: ' . $oldDevice['AccountID']);
                continue;
            }
        }

        unset($oldDevices);
        $progress->finish();
        $this->om->flush();

        return $countImportedDevices;
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getSize($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'DevSize', 'size');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getMake($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'DevMake', 'make');
    }

    /**
     * @param $oldDevice
     * @param $oldFieldName
     * @param $newFieldName
     * @return null
     */
    private function getDynamicField($oldDevice, $oldFieldName, $newFieldName)
    {
        $oldValue = $oldDevice[$oldFieldName];
        if (isset($this->dynamicFields[$newFieldName][$oldValue])) {
            return $this->dynamicFields[$newFieldName][$oldValue];
        }

        return null;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i != 0) {
            $this->om->flush();
        }
    }

    /**
     * @param \DateTime $datetime1
     * @param \DateTime $datetime2
     * @return string
     */
    public function getTimedifference(\DateTime $datetime1, \DateTime $datetime2)
    {
        $resultInterval = "Process took:";

        $interval = $datetime1->diff($datetime2);

        if ($interval->h > 0) {
            $resultInterval .= " " . $interval->h . " hours";
        }
        if ($interval->i > 0) {
            $resultInterval .= " " . $interval->i . " minutes";
        }
        if ($interval->s > 0) {
            $resultInterval .= " " . $interval->s . " seconds";
        }

        return $resultInterval;
    }
}
