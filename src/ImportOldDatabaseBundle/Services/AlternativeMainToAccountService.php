<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountBuildingType;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\Coordinate;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\PaymentTerm;
use AppBundle\Entity\Repository\AccountBuildingTypeRepository;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\AccountTypeRepository;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\ClientTypeRepository;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Entity\Repository\ContactPersonRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\PaymentTermRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\State;
use Doctrine\Common\Persistence\ObjectManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output;

class AlternativeMainToAccountService
{
    public const TABLE_MAIN1 = 'abfp.dbo.Main1';
    public const ACCOUNT_TYPE_ACCOUNT = 'account';
    public const ACCOUNT_CLIENT_TYPE_ACTIVE = 'active';
    private const ADDRESS_TYPE_PERSONAL = 'personal';
    public const ADDRESS_TYPE_SITE_ADDRESS = 'site address';
    public const DEFAULT_MUNICIPALITY = 'City of Chicago';
    public const TYPE_BUILDING_ACCOUNT_RESIDENTIAL = 'residential';
    public const TYPE_BUILDING_ACCOUNT_COMMERCIAL = 'commercial';
    private const NEED_FLUSH_ON = 1000;
    private const TABLE_MAS_LEVEL = 'abfp.dbo.MasLevel';

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var AccountTypeRepository */
    private $accountTypeRepository;
    /** @var StateRepository */
    private $stateRepository;
    /** @var CompanyRepository */
    private $companyRepository;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var AccountBuildingTypeRepository */
    private $accountBuildingTypeRepository;
    /** @var AccountManager */
    private $accountManager;
    /** @var ContactPersonManager */
    private $contactPersonManager;
    /** @var  AccountRepository */
    private $accountRepository;
    /** @var  ClientTypeRepository */
    private $clientTypeRepository;
    /** @var  MunicipalityRepository */
    private $municipalityRepository;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var MasLevelToAccountService */
    private $masLevelToAccountService;
    /** @var ContactPersonRepository */
    private $contactPersonRepository;
    /** @var array */
    private $groupAccounts = [];
    /** @var  MunicipalToMunicipalityService */
    private $municipalityImportService;
    /** @var AccountContactPerson */
    private $accountContactPerson1;
    /** @var AccountContactPerson */
    private $accountContactPerson2;
    /** @var PaymentTermRepository */
    private $paymentTermRepository;
    /** @var PaymentTerm */
    private $paymentTermDefault;
    /** @var array */
    private $undesirableNames = [
        'current homeowner', 'current homeowner:', 'current homeowners', 'current  homeowner',
        'property at', 'property at:', 'property  at'
    ];

    /**
     * MasLevelToAccount constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;

        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->accountTypeRepository = $this->om->getRepository('AppBundle:AccountType');
        $this->clientTypeRepository = $this->om->getRepository('AppBundle:ClientType');
        $this->stateRepository = $this->om->getRepository('AppBundle:State');
        $this->companyRepository = $this->om->getRepository('AppBundle:Company');
        $this->addressTypeRepository = $this->om->getRepository('AppBundle:AddressType');
        $this->accountRepository = $this->om->getRepository('AppBundle:Account');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
        $this->accountBuildingTypeRepository = $this->om->getRepository('AppBundle:AccountBuildingType');
        $this->municipalityRepository = $this->om->getRepository('AppBundle:Municipality');
        $this->accountManager = $this->container->get('app.account.manager');
        $this->contactPersonManager = $this->container->get('app.contact_person.manager');
        $this->municipalityImportService = $this->container->get('import.municipal_to_municipality.service');
        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->masLevelToAccountService = $this->container->get('import.mas_level_to_account.service');
        $this->contactPersonRepository = $this->om->getRepository('AppBundle:ContactPerson');
        $this->paymentTermRepository = $this->om->getRepository('AppBundle:PaymentTerm');
        $this->paymentTermDefault = $this->paymentTermRepository->findOneBy(["alias" => "net_fifteen_days"]);
    }


    public function testParseDate(){
        $masLevelData = $this->oldDatabaseRepository->getAllTablesRowsByTableName(
            self::TABLE_MAIN1
//            " WHERE SiteMunicipalityID <> FireMunicipalityID"
        );

        var_dump(count($masLevelData));
        die();
    }

    /**
     * @return int
     */
    public function parseData()
    {
        $masLevelData = $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::TABLE_MAIN1);
        $result = $this->dataImport($masLevelData);
        unset($masLevelData);

        return $result;
    }

    /**
     * @param array $oldData
     * @return int
     */
    private function dataImport(array $oldData)
    {
        $i = 0;
        $importedIds = $this->getImportedAccountOldIds();
        $output = new Output\ConsoleOutput();
        $progress = new ProgressBar($output, count($oldData));
        $progress->start();

        foreach ($oldData as $data) {
            try {
                $company = null;
                $municipality = null;
                if (!in_array($data['AccountID'], $importedIds) && strtolower(trim($data['OwnTitle'])) != 'del') {
                    $this->accountContactPerson1 = null;
                    $this->accountContactPerson2 = null;
                    $error = $this->importErrorMessageManager->checkSiteData($data);

    //                if ($error) {
    //                    continue;
    //                }

                    $municipality = $this->getMunicipalityForAccount($data);

                    if (!empty($data['MasID'])) {
                        $company = $this->companyRepository->findOneBy(
                            ['oldCompanyId' => $data['MasID'], 'sourceEntity' => 'Master']
                        );
                    }

                    if (!empty($data['OrgID']) && empty($company)) {
                        $company = $this->companyRepository->findOneBy(
                            ['oldCompanyId' => $data['OrgID'], 'sourceEntity' => 'Org']
                        );
                    }

                    /** @var Account $account */
                    $account = $this->setAccount($data, $company, $municipality);
                    if (!empty($data['SiteConLName']) ||
                        !empty($data['SiteConFName']) ||
                        !empty($data['SiteTitle']) ||
                        !empty($data['SiteConPhone']) ||
                        !empty($data['SitePhoneExt']) ||
                        !empty($data['SiteConCell']) ||
                        !empty($data['SiteConFax']) ||
                        !empty($data['SiteConEmail'])
                    ) {
                        /** @var ContactPerson $firstContactPerson */
                        $firstContactPerson = $this->setContactPerson($data, $company);
                        $this->accountContactPerson1 = $this->setAccountContactPerson($account, $firstContactPerson, true);
                    }
                    if (!empty($data['SiteConLName2']) ||
                        !empty($data['SiteConFName2']) ||
                        !empty($data['SiteTitle2']) ||
                        !empty($data['SiteConPhone2']) ||
                        !empty($data['SitePhoneExt2']) ||
                        !empty($data['SiteConCell2']) ||
                        !empty($data['SiteConFax2']) ||
                        !empty($data['SiteConEmail2'])
                    ) {
                        /** @var ContactPerson $secondContactPerson */
                        $secondContactPerson = $this->setContactPerson($data, $company, 2);
                        $this->accountContactPerson2 = $this->setAccountContactPerson($account, $secondContactPerson);
                    }

                    $this->setRoleAndMailingAddress($account, $data);

                    $i++;
                    $progress->advance();

                    $this->needLoad($i);
//                    if ($i == 10000) {
//                        break;
//                    }
                }
            } catch (\Exception $exception) {
                $this->importErrorMessageManager->setErrorMessage('[import Site accounts][OldAccountId:' . $data['AccountID'] . ' ]: department error AccountId: ' . $data['AccountID']);
                continue;
            }
        }

        unset($oldData);
        unset($importedIds);
        $progress->finish();
        $output->writeln('');

        $this->om->flush();

        return $i;
    }

    /**
     * @return array
     */
    public function getImportedAccountOldIds()
    {
        $importedIds =[];
        $importedAccountsArray = $this->accountRepository->getImportedAccount();
        foreach ($importedAccountsArray as $importedAccount) {
            $importedIds[] = $importedAccount["oldAccountId"];
        }

        return $importedIds;
    }

    /**
     * @param array $data
     * @param Company|null $company
     * @param Municipality|null $municipality
     * @return Account
     */
    private function setAccount(array $data, ?Company $company, ?Municipality $municipality)
    {
        $account = new Account();
        $account->setOldAccountId($data['AccountID']);
        $account->setOldLinkId($data['MasID']);
        $account->setName($data['SiteName'] ?? 'no name');
        $account->setAddress($this->setAddress($data));
        $account->setType($this->accountTypeRepository->findOneBy(['alias' => self::ACCOUNT_TYPE_ACCOUNT]));
        $account->setClientType($this->clientTypeRepository->findOneBy(['alias' =>self::ACCOUNT_CLIENT_TYPE_ACTIVE]));
        $account->setPaymentTerm($this->paymentTermDefault);
        $account->setBuildingType(
            $this->accountBuildingTypeRepository->findOneBy(['alias' => strtolower($data['AccountType'])])
        );
        $account->setCoordinate($this->setCoordinate($data));

        if ($company) {
            $account->setCompany($company);
        }

        if ($municipality) {
            $account->setMunicipality($municipality);
            $municipality->addAccount($account);
            $parentAcc = $account->getParent();
            if ($parentAcc) {
                $parentAcc->setMunicipality($municipality);
                $this->om->persist($parentAcc);
            }
            $this->om->persist($municipality);

        }

        $account->setSiteSendTo($data['SiteSendTo']);
        $account->setNotes($this->prepareCommentData($data, $account));
        $this->om->persist($account);

        return $account;
    }

    /**
     * @param array $data
     * @return Address
     */
    private function setAddress(array $data)
    {
        $state = $this->stateRepository->findOneBy(['code' => strtoupper($data['SiteState'])]);

        $address = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_SITE_ADDRESS]);

        if ($addressType) {
            $address->setAddressType($addressType);
        }
        $address->setAddress($data['SiteAddr1']);
        $address->setCity($data['SiteCity']);
        $address->setZip($data['SiteZip']);
        $address->setState($state);

        $this->om->persist($address);

        return $address;
    }

    /**
     * @param array $data
     * @param Company|null $company
     * @param string $serialNumber '' - first contact person 2 - second contact person in old database
     * @return ContactPerson
     */
    private function setContactPerson(array $data, ?Company $company, $serialNumber = '')
    {
        $contactPerson = new ContactPerson();
        $contactPerson->setFirstName($data['SiteConFName'.$serialNumber] ?? 'Property Owner');
        $contactPerson->setLastName($data['SiteConLName'.$serialNumber]);
        $contactPerson->setTitle($data['SiteTitle'.$serialNumber]);
        $contactPerson->setPhone($data['SiteConPhone'.$serialNumber]);
        $contactPerson->setExt($data['SitePhoneExt'.$serialNumber]);
        $contactPerson->setCell($data['SiteConCell'.$serialNumber]);
        $contactPerson->setFax($data['SiteConFax'.$serialNumber]);
        $contactPerson->setEmail($data['SiteConEmail'.$serialNumber]);
        $contactPerson->setSourceEntity("Site");
        $contactPerson->setSourceId($data['AccountID']);
        $contactPerson->setPersonalAddress($this->createEmptyAddressForCP());
        $contactPerson->setNumContact(empty($serialNumber) ? 1 : $serialNumber);
        if ($company) {
            $contactPerson->setCompany($company);
        }
        $this->om->persist($contactPerson);

        return $contactPerson;
    }

    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @param bool $accessPrimary
     * @return AccountContactPerson
     */
    private function setAccountContactPerson(Account $account, ContactPerson $contactPerson, $accessPrimary = null)
    {
        $accountContactPerson = new AccountContactPerson();
        $accountContactPerson->setContactPerson($contactPerson);
        $accountContactPerson->setAccount($account);
        $accountContactPerson->setAccess(true);
        $accountContactPerson->setAccessPrimary($accessPrimary);
        $accountContactPerson->setSendingAddress($this->checkAddressForACP($accountContactPerson));

        $this->om->persist($accountContactPerson);

        return $accountContactPerson;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }


    /**
     * @param AccountContactPerson $accountContactPerson
     * @return Address
     */
    private function checkAddressForACP(AccountContactPerson $accountContactPerson)
    {
        /** @var ContactPerson $acpContactPerson */
        $acpContactPerson = $accountContactPerson->getContactPerson();
        /** @var Company $acpCompany */
        $acpCompany = $acpContactPerson->getCompany();
        /** @var Account $acpAccount */
        $acpAccount = $accountContactPerson->getAccount();

        if (!empty($acpAccount) && !empty($acpAccount->getAddress())) {
                return $acpAccount->getAddress();

        } elseif (!empty($acpCompany) && !empty($acpCompany->getAddress())) {
            return $acpCompany->getAddress();
        }

        return $acpContactPerson->getPersonalAddress();
    }

    /**
     * @param array|null $masLevelData
     * @return Address
     */
    private function createEmptyAddressForCP($masLevelData = null)
    {
        $newAddress = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_PERSONAL]);
        $newAddress->setAddressType($addressType);

        if (!empty($masLevelData)) {
            $newAddress->setAddress($masLevelData['MasAddr'] ?? '');
            $newAddress->setCity($masLevelData['MasCity']);
            $newAddress->setZip($masLevelData['MasZip']);
            if (!empty($masLevelData['MasState'])) {
                /** @var State $state */
                $state = $this->stateRepository->findOneBy(['code' => strtoupper($masLevelData['MasState'])]);
                $newAddress->setState($state);
            }
        }

        $this->om->persist($newAddress);

        return $newAddress;
    }

    /**
     * @param Account $account
     * @param array $data
     */
    private function setAccountContactPersonRole(Account $account, $data)
    {
        if ($account->getSiteSendTo() === 'Site') {
            $this->siteSendToSite($account, $data);

        } elseif ($account->getSiteSendTo() === 'Org') {
            $this->siteSendToMasterLetterOrOrg($account, true);

        } elseif ($account->getSiteSendTo() === 'Master' || $account->getSiteSendTo() === 'Both') {
            if ($this->isCurrentHomeowner($data) || empty($account->getCompany())) {
                $this->siteSendToMasterCurrentHomeowner($account, $data);
            } else {
                $this->siteSendToMasterLetterOrOrg($account);
            }
        }
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     */
    private function setResponsibility(AccountContactPerson $accountContactPerson)
    {
        $allDeviceCategories = $this->deviceCategoryRepository->findAll();

        /** @var DeviceCategory $division */
        foreach ($allDeviceCategories as $division) {
            $accountContactPerson->addResponsibility($division);
        }
    }

    /**
     * @param Account $account
     * @param $data
     */
    private function setRoleAndMailingAddress(Account $account, $data)
    {
        if ($data['SiteLetter'] === 'Retest Notice') {
            $this->setAccountContactPersonRole($account, $data);
        }

        if ($data['SiteLetter'] === 'Org Letter') {
            $this->siteSendToMasterLetterOrOrg($account, true);
        }

        if ($data['SiteLetter'] === 'Master Letter') {
            if ($this->isCurrentHomeowner($data) || empty($account->getCompany())) {
                $this->siteSendToMasterCurrentHomeowner($account, $data);
            } else {
                $this->siteSendToMasterLetterOrOrg($account);
            }
        }
    }

    /**
     * @param $oldAccountData
     * @return Municipality|null|object
     */
    private function getMunicipalityForAccount($oldAccountData)
    {
        $backflowMunicipalityId = $oldAccountData["SiteMunicipalityID"];
        $fireMunicipalityId = $oldAccountData["FireMunicipalityID"];
        $finalMunicipalityId = null;

        if (!empty($backflowMunicipalityId) and
            !empty($fireMunicipalityId) and
            $backflowMunicipalityId == $fireMunicipalityId
        ) {
            $finalMunicipalityId = $backflowMunicipalityId;
        } elseif (!empty($backflowMunicipalityId) and empty($fireMunicipalityId)) {
            $finalMunicipalityId = $backflowMunicipalityId;
        } elseif (empty($backflowMunicipalityId) and !empty($fireMunicipalityId)) {
            $finalMunicipalityId = $fireMunicipalityId;
        } elseif (!empty($backflowMunicipalityId) and
            !empty($fireMunicipalityId) and
            $backflowMunicipalityId != $fireMunicipalityId
        ) {
            $finalMunicipality = $this->municipalityImportService->createAndGetMixedMunicipality(
                $backflowMunicipalityId,
                $fireMunicipalityId
            );

            return $finalMunicipality;
        }

        if (!empty($finalMunicipalityId)) {
            return $this->municipalityRepository->findOneBy(['oldMunicipalityId' => $finalMunicipalityId]);
        } else {
            return $this->municipalityRepository->find(1);
        }
    }

    /**
     * @param $data
     * @return Coordinate
     */
    private function setCoordinate($data)
    {
        $coordinate = new Coordinate();
        $coordinate->setLatitude(!empty($data['SLat']) ? $data['SLat'] : 0);
        $coordinate->setLongitude(!empty($data['SLog']) ? $data['SLog'] : 0);

        $this->om->persist($coordinate);

        return $coordinate;
    }

    /**
     * @param Account $account
     * @param array $data
     */
    private function siteSendToSite(Account $account, $data)
    {
        if (!empty($this->accountContactPerson1)) {
            $this->accountContactPerson1->setAuthorizer(true);
            $this->setResponsibility($this->accountContactPerson1);
            $this->accountContactPerson1->setSendingAddress($account->getAddress());

            $this->om->persist($this->accountContactPerson1);
        }
        if (!empty($this->accountContactPerson2)) {
            $this->accountContactPerson2->setAuthorizer(true);
            if (empty($this->accountContactPerson1)) {
                $this->setResponsibility($this->accountContactPerson2);
            }
            $this->accountContactPerson2->setSendingAddress($account->getAddress());

            $this->om->persist($this->accountContactPerson2);
        }
        if ($this->isNeedDummyContactPerson($data)
            && empty($this->accountContactPerson1)
            && empty($this->accountContactPerson2)
        ) {
            /** @var ContactPerson $fakeContactPerson */
            $fakeContactPerson = $this->createFakeContactPerson($data);
            /** @var AccountContactPerson $fakeAccountContactPerson */
            $fakeAccountContactPerson = $this->setAccountContactPerson($account, $fakeContactPerson, true);
            $fakeAccountContactPerson->setAuthorizer(true);
            $this->setResponsibility($fakeAccountContactPerson);
            $fakeAccountContactPerson->setSendingAddress($account->getAddress());
            $this->setAccountClientType($account);

            $this->om->persist($fakeAccountContactPerson);
        }
    }

    /**
     * @param Account $account
     * @param null $orgCompany
     */
    private function siteSendToMasterLetterOrOrg(Account $account, $orgCompany = null)
    {
        $contactPersons = null;
        $company = null;

        if ($orgCompany && $account->getCompany() && $account->getCompany()->getParent()) {
            $company = $account->getCompany()->getParent();
        } elseif (empty($orgCompany) && $account->getCompany()) {
            $company = $account->getCompany();
        }

        if (!empty($company) ) {
            $contactPersons = $this->contactPersonRepository->getByCompany($company);
        }

        if (!empty($contactPersons)) {
            $this->setACPAndSendingAddress($account, $company->getAddress(), $contactPersons);
        }
    }

    /**
     * @param Account $account
     * @param $data
     */
    private function siteSendToMasterCurrentHomeowner(Account $account, $data)
    {
        $contactPersons = null;

        if (!empty($data['MasID'])) {
            /** @var array $contactPersons */
            $contactPersons = $this->contactPersonRepository->getBySourceIdCompany($data['MasID']);
        }

        if (empty($contactPersons) && $this->isNeedDummyContactPerson($data)) {
            /** @var ContactPerson $contactPersons */
            $contactPersons = $this->createFakeContactPerson($data, true);
            $this->setAccountClientType($account);
        }

        if (!empty($contactPersons) && is_array($contactPersons)) {
            $address = $contactPersons[0]->getPersonalAddress();
            $this->setACPAndSendingAddress($account, $address, $contactPersons);

        } elseif (!empty($contactPersons) && is_object($contactPersons)) {
            $acp = $this->masLevelToAccountService->setAccountContactPerson($account, $contactPersons, true);
            $acp->setSendingAddress($contactPersons->getPersonalAddress());

            $this->om->persist($acp);
        }
    }

    /**
     * @param $data
     * @param bool $forMaster
     * @return ContactPerson|bool
     */
    private function createFakeContactPerson($data, $forMaster = false)
    {
        $masLevelData = null;
        $sourceEntity = 'Main1';
        $sourceId = $data['AccountID'];

        if ($forMaster) {
            $masLevelData = $this->oldDatabaseRepository->getAllTablesRowsByTableName(
                self::TABLE_MAS_LEVEL,
            " WHERE ".self::TABLE_MAS_LEVEL.".MasID = '{$data['MasID']}'"
            );
            $masLevelData = count($masLevelData) > 1 ? $masLevelData : $masLevelData[0];
            if (!in_array(strtolower(trim($masLevelData['MasName'])), $this->undesirableNames)) {
                return false;
            }
            $sourceEntity = 'MasLevel';
            $sourceId = $masLevelData['MasID'];
        }

        $contactPerson = new ContactPerson();
        $contactPerson->setFirstName('Current Homeowner');
        $contactPerson->setSourceEntity($sourceEntity);
        $contactPerson->setSourceId($sourceId);
        $contactPerson->setPersonalAddress($this->createEmptyAddressForCP($masLevelData));
        $contactPerson->setNumContact(1);

        $this->om->persist($contactPerson);

        return $contactPerson;
    }

    /**
     * @param Account $account
     * @param Address $address
     * @param $contactPersons
     */
    private function setACPAndSendingAddress(Account $account, Address $address, $contactPersons)
    {
        $quantityCP = count($contactPersons);
        /** @var ContactPerson $person */
        foreach ($contactPersons as $person) {
            $flag = ($person->getNumContact() == 1) || ($quantityCP == 1) ? true : false;

            $acp = $this->masLevelToAccountService->setAccountContactPerson($account, $person, $flag);

            $acp->setSendingAddress($address);

            $this->om->persist($acp);
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isCurrentHomeowner($data)
    {
        return in_array(strtolower(trim($data['SiteName'])), $this->undesirableNames);
    }

    /**
     * @param array $data
     * @param Account $account
     * @return string
     */
    private function prepareCommentData($data, Account $account)
    {
        $result = $account->getNotes() ?? '';
        $fields = [
            'SiteComments1', 'SiteComments2', 'SiteMunCom1',
            'SiteMunCom2', 'SiteMunCom3', 'SiteMunCom4', 'SiteMunCom5', 'OwnConCell1'
        ];
        $OwnConCell1 = 'BSI CCN: ';

        foreach ($fields as $field) {
            if (!empty($data[$field]) && $field == 'OwnConCell1') {
                $result .= empty($result)
                    ? $OwnConCell1 . $data[$field]
                    : (PHP_EOL . PHP_EOL . $OwnConCell1 . $data[$field]);
            } elseif (!empty($data[$field])) {
                $result .= empty($result) ? $data[$field] : (PHP_EOL . PHP_EOL . $data[$field]);
            }
        }

        return $result;
    }

    /**
     * @param Account $account
     */
    private function setAccountClientType(Account $account)
    {
        $clientType = $this->clientTypeRepository->findOneBy(['alias' => 'potential']);
        $account->setClientType($clientType);

        $this->om->persist($account);
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isNeedDummyContactPerson($data)
    {
        $isResidentialType = strtolower(trim($data['AccountType'])) == 'residential';
        $isPropertyAt = in_array(strtolower(trim($data['SiteName'])), ['property at', 'property at:', 'property  at']);
        $isCurrentHomeowner = $this->isCurrentHomeowner($data);

        if (!$isResidentialType && $isPropertyAt && $isCurrentHomeowner) {
            $isCurrentHomeowner = false;
        }

        return $isCurrentHomeowner;
    }
}
