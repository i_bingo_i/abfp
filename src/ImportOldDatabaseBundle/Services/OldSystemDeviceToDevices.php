<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorType;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DeviceStatus;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Opportunity;
use AppBundle\Entity\OpportunityType;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\ContractorTypeRepository;
use AppBundle\Entity\Repository\DeviceStatusRepository;
use AppBundle\Entity\Repository\DynamicFieldDropboxChoicesRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\OpportunityRepository;
use AppBundle\Entity\Repository\OpportunityTypeRepository;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use AppBundle\Entity\EntityManager\OpportunityManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output;

class OldSystemDeviceToDevices
{
    private const IMPORT_TABLE = 'abfp.dbo.Device';
    private const NEED_FLUSH_ON = 1000;

    private const CONTRACTOR_TYPE_COMPETITOR = 'competitor';

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    private $opportunityCache = [];

    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var  OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var  AccountRepository */
    private $accountRepository;
    /** @var DeviceNamedRepository */
    private $deviceNamedRepository;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var DeviceNamed */
    private $deviceNamedBackflow;
    /** @var ServiceNamed */
    private $serviceNamedAnnualBackflowInspection;
    /** @var DepartmentChannelManager $departmentChannelManager */
    private $departmentChannelManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var OpportunityManager */
    private $opportunityManager;
    /** @var OpportunityRepository */
    private $opportunityRepository;
    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var DynamicFieldDropboxChoicesRepository */
    private $choicesRepository;
    /** @var DeviceStatusRepository */
    private $deviceStatusRepository;
    /** @var  ContractorRepository */
    private $contractorRepository;
    /** @var  ContractorTypeRepository */
    private $contractorTypeRepository;
    /** @var ContractorType  */
    private $contractorTypeCompetitor;
    /** @var DeviceStatus */
    private $deviceStatusActive;
    /** @var DeviceStatus */
    private $deviceStatusInActive;
    private $dynamicFields = [
        'size' => [],
        'make' => [],
        'type' => [],
        'hazard' => [],
        'orientation' => [],
        'shut_off_valve' => []
    ];
    /** @var DynamicFieldDropboxChoices */
    private $choiceApprovedInstallationNo;
    /** @var array */
    private $contractorsCache;
    /** @var OpportunityTypeRepository */
    private $opportunityTypeRepository;
    /** @var OpportunityType */
    private $opportunityTypeRetest;
    /** @var DeviceManager */
    private $deviceManager;
    /** @var ContractorServiceRepository */
    private $contractorServiceRepository;

    /**
     * MasLevelToAccount constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
        $this->departmentChannelManager = $this->container->get('app.department_channel.manager');
        $this->serviceManager = $this->container->get('app.service.manager');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
        $this->opportunityManager = $this->container->get('app.opportunity.manager');

        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->opportunityTypeRepository = $this->om->getRepository('AppBundle:OpportunityType');
        $this->deviceStatusRepository = $this->om->getRepository('AppBundle:DeviceStatus');
        $this->accountRepository = $this->om->getRepository('AppBundle:Account');
        $this->deviceNamedRepository = $this->om->getRepository('AppBundle:DeviceNamed');
        $this->serviceNamedRepository = $this->om->getRepository('AppBundle:ServiceNamed');
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->opportunityRepository = $this->om->getRepository('AppBundle:Opportunity');
        $this->choicesRepository = $this->om->getRepository('AppBundle:DynamicFieldDropboxChoices');
        $this->deviceRepository = $this->om->getRepository('AppBundle:Device');
        $this->contractorRepository = $this->om->getRepository('AppBundle:Contractor');
        $this->contractorTypeRepository = $this->om->getRepository('AppBundle:ContractorType');
        $this->deviceManager = $this->container->get("app.device.manager");
        $this->contractorServiceRepository = $this->om->getRepository('AppBundle:ContractorService');

        $this->contractorTypeCompetitor = $this->contractorTypeRepository->findOneBy(['alias' => 'competitor']);
        $this->deviceNamedBackflow = $this->deviceNamedRepository->findOneBy(['alias' => 'backflow_device']);
        $this->deviceStatusActive = $this->deviceStatusRepository->findOneBy(['alias' => 'active']);
        $this->deviceStatusInActive = $this->deviceStatusRepository->findOneBy(['alias' => 'inactive']);
        $this->opportunityTypeRetest = $this->opportunityTypeRepository->findOneBy(['alisa' => 'retest']);
        $this->serviceNamedAnnualBackflowInspection = $this->serviceNamedRepository->findOneBy([
            'name' => 'Annual Backflow Inspection'
        ]);
        $this->choiceApprovedInstallationNo = $this->choicesRepository->findOneBy([
            'alias' => 'backflow_device_approved_installation_no'
        ]);

        $this->dynamicFields['type'] = [
            'RPZ' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_type_rpz']),
            'RPDA' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_type_rpda']),
            'DCDA' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_type_dcda']),
            'DCV' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_type_dcv']),
            'SVB' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_type_svb']),
            'PVB' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_type_pvb']),
            'Unapproved' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_type_unapproved_device']),
        ];

        $this->dynamicFields['size'] = [
            '1/4' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1_slash_4']),
            '1/2' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1_slash_2']),
            '3/4' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_3_slash_4']),
            '1' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1']),
            '1 1/4' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1_dot_25']),
            '1 1/2' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_1_dot_5']),
            '2' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_2']),
            '2 1/2' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_2_dot_5']),
            '3' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_3']),
            '4' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_4']),
            '6' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_6']),
            '8' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_8']),
            '10' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_10']),
            '12' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_size_12'])
        ];

        $this->dynamicFields['make'] = [
            'Ames' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_ames']),
            'Febco' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_febco']),
            'Watts' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_watts']),
            'Wilkins' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_wilkins']),
            'Rainbird' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_rainbird']),
            'Conbraco' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_conbraco']),
            'Apollo' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_apollo']),
            'Colt' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_colt']),
            'Maxim' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_maxim']),
            'Beeco' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_beeco']),
            'Backflow Direct' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_backflow_direct']),
            'Flomatic' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_flomatic']),
            'Flowmatic' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_flomatic']),
            'Swing CHK' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_swing_chk']),
            'Zurn' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_zurn']),
            'Verify' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_verify']),
            'Cash Acme' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_cash_acme']),
            'Globe' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_globe']),
            'Cla-Val' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_cla_val']),
            'Buckner' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_buckner']),
            'Hersey' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_hersey']),
            'Other' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_make_other']),
        ];

        $this->dynamicFields['hazard'] = [
            'Back Wash Supply' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_back_wash_supply']),
            'Baptism Fountain' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_baptism_fountain']),
            'Boiler' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_boiler_feed']),
            'Boiler Feed' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_boiler_feed']),
            'BoilerFeed' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_boiler_feed']),
            'Brine Tank' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_brine_tank']),
            'Car Wash' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_car_wash']),
            'Chemical Feeder' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_chemical_feeder']),
            'Chill Water' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_chiller_make_up']),
            'Chiller Make Up' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_chiller_make_up']),
            'Chinese Range' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_wok_range']),
            'Chlorine Line' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_chlorine_line']),
            'Coffee Machine' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_coffee_machine']),
            'Cold Feed Pedi Chair' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_cold_feed_pedi_chair']),
            'Cold pedi chair' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_cold_feed_pedi_chair']),
            'Cold Pedi' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_cold_feed_pedi_chair']),
            'Cold Water' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_cold_water_supply']),
            'Commercial' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_commercial_feed']),
            'Compressor' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_compressor']),
            'Cooling Tower' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_cooling_tower']),
            'Dental Chair' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_dental_equipment']),
            'Dental Equipment' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_dental_equipment']),
            'Dialysis' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_dialysis']),
            'Dishwasher' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_dishwasher']),
            'Domestic' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_domestic']),
            'Domestic By Pass' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_domestic_by_pass']),
            'Domestic By-Pass' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_domestic_by_pass']),
            'Drinking Fountain' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_drinking_fountain']),
            'Equipment Make Up' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_equipment_make_up']),
            'Equipment Make-Up' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_equipment_make_up']),
            'Film Processor' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_film_processor']),
            'Filter System' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_filter_system']),
            'Filter Water' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_filter_water']),
            'Fire Anti-Freeze' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_fire_anti_freeze']),
            'Fire By Pass' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_fire_by_pass']),
            'Fire By-Pass' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_fire_by_pass']),
            'FireBy-Pass' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_fire_by_pass']),
            'Fire Protection' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_fire_protection']),
            'Radiant Heat' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_radiant_heat']),
            'Fountain' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_fountain']),
            'Garbage Disposal' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_garbage_disposal']),
            'Generator' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_second_generator']),
            'Hoodwash' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hoodwash']),
            'Hose Bibb' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hose_bibb']),
            'Hose Conection' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hose_connection']),
            'Hose Connection' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hose_connection']),
            'Hose Reel' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hose_reel']),
            'Hot Pedi' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hot_feed_pedi_chair']),
            'Hot pedi chair' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hot_feed_pedi_chair']),
            'Hot Tub' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hot_tub']),
            'Hot Water' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hot_water_supply']),
            'Humidfier' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_humidfier']),
            'HVAC' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hvac_equip']),
            'Hydrant' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_hydrant']),
            'Ice Machine' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_ice_machine']),
            'Irrigation' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_irrigation']),
            'Jockey pump' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_jockey_pump']),
            'Laboratory' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_laboratory']),
            'Laundry Cold' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_laundry_cold']),
            'Laundry Hot' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_laundry_hot']),
            'Make Up Water' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_make_up_water']),
            'Melt Tank' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_melt_tank']),
            'Misters' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_misters']),
            'MRI' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_mri']),
            'Outside Pond' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_outside_pond']),
            'Photo Developer' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_photo_developer']),
            'Plaster Trimmer' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_plaster_trimmer']),
            'Pond Feed' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_pond_feed']),
            'Pool Feed' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_pool_feed']),
            'Portable Hydrant' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_portable_hydrant']),
            'Potable Water' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_potable_water']),
            'Pressure Water' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_pressure_water']),
            'Pressure Washer' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_pressure_washer']),
            'Process Water' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_process_water']),
            'Pressure Rectifier' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_pressure_rectifier']),
            'Reduced Pressure' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_pressure_reducer']),
            'RO System' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_ro_system']),
            'SAA' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_saa']),
            'Sanitation Flush' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_sanitation_flush']),
            'Sillcock' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_sillcock']),
            'Soap Dispenser' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_soap_dispenser']),
            'Soda Equip' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_soda_equip']),
            'Steamer' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_steamer']),
            'Sterilizer' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_sterilizer']),
            'Tank Fill' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_tank_fill']),
            'Tank Washer' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_tank_washer']),
            'Trash Compactor' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_trash_compactor']),
            'Trash Shoot' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_trash_shoot']),
            'Truck Fill' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_truck_fill']),
            'Utility Sink' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_utility_sink']),
            'Vacuum Pump' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_vaccum_pump']),
            'Vaccum Pump' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_vaccum_pump']),
            'Wall Hydrant' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_wall_hydrant']),
            'Wash Hose' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_wash_hose']),
            'Washroom' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_washroom']),
            'Waste' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_waste']),
            'Water Softner' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_water_softner']),
            'X-Ray' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_x_ray']),
            'Yard Hydrant' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_yard_hydrant']),
            'Floor Heat' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_hazard_floor_heat']),
        ];

        $this->dynamicFields['orientation'] = [
            'Horizontal' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_orientation_horizontal']),
            'Vertical' => $this->choicesRepository->findOneBy(['alias' => 'backflow_device_orientation_vertical'])
        ];
    }

    /**
     * @return array
     */
    public function getImportedDevicesOldIds()
    {
        $importedIds =[];
        $importedDevicesArray = $this->deviceRepository->getImportedDevices();
        foreach ($importedDevicesArray as $importedDevice) {
            $importedIds[] = $importedDevice["oldDeviceId"];
        }

        return $importedIds;
    }

    /**
     * @return int
     */
    public function import()
    {
        $oldDevices = $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::IMPORT_TABLE);
        $countImportedDevices = 0;
        $importedIds = $this->getImportedDevicesOldIds();
        $output = new Output\ConsoleOutput();
        $progress = new ProgressBar($output, count($oldDevices));
        $progress->start();

        foreach ($oldDevices as $oldDevice) {
            try {
                if (!in_array($oldDevice['DevID'], $importedIds)) {
                    /** @var Account $account */
                    $account = $this->accountRepository->findOneBy(['oldAccountId' => $oldDevice['AccountID']]);
                    if (!$account) {
                        $this->importErrorMessageManager->setErrorMessage('[import devices error][OldDeviceId:' . $oldDevice['DevID'] . ' ]: dont find account with AccountId: ' . $oldDevice['AccountID']);
                        continue;
                    }

                    /** @var Contractor $lastContractor */
                    $lastContractor = false;
                    $lastTesterName = trim($oldDevice['LastTester']);
                    if ($oldDevice['LastTester'] and $lastTesterName != 'NO Contractor') {
                        $lastContractor = $this->contractorsCache[$oldDevice['LastTester']] ?? $this->getContractorByName($lastTesterName);
                    }

                    /** @var Device $newDevice */
                    $newDevice = $this->createDeviceByOldDevice($oldDevice, $account);
                    /** @var Service $newService */
                    $newService = $this->createServiceByOldDevice($oldDevice, $newDevice, $account, $lastContractor);

                    $oldAccountId = $oldDevice['AccountID'];

                    $opportunity = $this->getOpportunityFromCache($oldAccountId, $newService->getInspectionDue()->format('Y-m-d'));
                    if (empty($opportunity)) {
                        $opportunity = $this->opportunityManager->creating($newService);
                    }

                    if ($this->opportunityManager->isPastDue($opportunity)) {
                        $this->opportunityManager->setStatus($opportunity, $this->opportunityManager::STATUS_PAST_DUE);
                    }

                    $opportunity->addService($newService);
                    $opportunity = $this->opportunityManager->updateSomeFields($opportunity);
                    $this->om->persist($opportunity);
                    $this->opportunityCache[$oldAccountId][$newService->getInspectionDue()->format('Y-m-d')] = $opportunity;

                    $newService->setOpportunity($opportunity);
                    $newService = $this->serviceManager->setStatus($newService, $this->serviceManager::STATUS_RETEST_OPPORTUNITY);
                    $this->om->persist($newService);
                    $this->refreshOldestOpportunityDate($opportunity->getAccount(), $newService->getInspectionDue());
                    $countImportedDevices++;

                    $progress->advance();

                    $this->needLoad($countImportedDevices);
//                    if ($countImportedDevices > 10000) {
//                        break;
//                    }
                }
            } catch (\Exception $exception) {
                $this->importErrorMessageManager->setErrorMessage('[import devices error][OldDeviceId:' . $oldDevice['DevID'] . ' ]: error: "'.$exception->getMessage().'" AccountId: ' . $oldDevice['AccountID']);
                continue;
            }
        }

        unset($oldDevices);
        $progress->finish();
        $this->om->flush();

        return $countImportedDevices;
    }

    /**
     * @param $contractorName
     * @return Contractor
     */
    private function getContractorByName($contractorName)
    {
//        $contractorType = $this->contractorTypeRepository->findOneBy(['alias' => self::CONTRACTOR_TYPE_COMPETITOR]);
        $oldNames = [
            'American Backflow Prevention Inc.','American Backflow & Fire Prevention, Inc.',
            'American Backflow & Fire Prevention, Inc.', 'American Backflow &amp; Fire Prevention, Inc.'
        ];
        if ( in_array($contractorName, $oldNames) ) {
            $contractorName = 'American Backflow & Fire Prevention';
        }
        /** @var Contractor|null $contractor */
        $contractor = null;
        $contractor = $this->contractorRepository->findOneBy(['name' => $contractorName]);

//        if (!$contractor) {
//            $contractor = new Contractor();
//            $contractor->setName($contractorName);
//            $contractor->setStateLicenseNumber('No Data');
//            $contractor->setStateLicenseExpirationDate(\DateTime::createFromFormat('Y-m-d', '1900-01-01'));
//            $contractor->setDateCreate(new \DateTime());
//            $contractor->setDateUpdate(new \DateTime());
//            $contractor->setType($contractorType);
//
//            $this->om->persist($contractor);
//        }

        if (!empty($contractor)) {
            $this->contractorsCache[$contractorName] = $contractor;
        }

        return $contractor;
    }

    /**
     * @param Account $account
     * @param $newOldestDate
     */
    private function refreshOldestOpportunityDate(Account $account, $newOldestDate)
    {
        $currentOldestDate = $account->getOldestOpportunityDate();
        if (!$currentOldestDate or $currentOldestDate > $newOldestDate) {
            $account->setOldestOpportunityBackflowDate($newOldestDate);
            $account->setOldestOpportunityDate($newOldestDate);
        }

        $this->om->persist($account);

        if ($account->getParent()) {
            $this->refreshOldestOpportunityDate($account->getParent(), $newOldestDate);
        }
    }

    /**
     * @param $oldDevice
     * @param Account $account
     * @return Device
     */
    private function createDeviceByOldDevice($oldDevice, Account $account)
    {
        $device = new Device();

        $device->setNamed($this->deviceNamedBackflow);
        $device->setOldDeviceId($oldDevice['DevID']);
        if ($oldDevice['DevLocation'] != 'NA') {
            $device->setLocation($oldDevice['DevLocation']);
        }
        $device->setComments($oldDevice['DevComments']);
        $device->setNoteToTester($oldDevice['CommentSur']);
        $device->setStatus($this->deviceStatusActive);

        if ($oldDevice['ABPActive'] == 'Inactive') {
            $device->setStatus($this->deviceStatusInActive);
        }

        if (!empty($this->isValidImagePath($oldDevice['DevImage']))) {
            $device->setPhoto($oldDevice['DevImage']);
        }

        $device->setAccount($account);
        $device->setSourceEntity('Backflow Devices');

        /** @var DynamicField[] $fields */
        $fields = $this->deviceNamedBackflow->getFields();
        /** @var DynamicField $field */
        foreach ($fields as $field) {
            $newValue = new DynamicFieldValue();

            if ($field->getAlias() == 'model' and $oldDevice['DevModel'] != 'NA') {
                $newValue->setValue($oldDevice['DevModel']);
            }

            if ($field->getAlias() == 'serial_number' and $oldDevice['DevSerialNumber'] != 'NA') {
                $newValue->setValue($oldDevice['DevSerialNumber']);
            }

            if ($field->getAlias() == 'size'
                and ($oldDevice['DevSize'] != 'NA' or $oldDevice['DevSize'] != '(Blanks)')
            ) {
                $size = $this->getSize($oldDevice);
                $newValue->setOptionValue($size);
            }

            if ($field->getAlias() == 'type' and $oldDevice['DevType'] != 'NA') {
                /** @var DynamicFieldDropboxChoices $type */
                $type = $this->getType($oldDevice);
                $newValue->setOptionValue($type);
            }

            if ($field->getAlias() == 'make' and $oldDevice['DevMake'] != 'NA') {
                /** @var DynamicFieldDropboxChoices $make */
                $make = $this->getMake($oldDevice);
                $newValue->setOptionValue($make);
            }

            if ($field->getAlias() == 'hazard' and
            ($oldDevice['DevHazzard'] != 'N/A' or $oldDevice['DevHazzard'] != 'NA')) {
                /** @var DynamicFieldDropboxChoices $hazard */
                $hazard = $this->getHazard($oldDevice);
                $newValue->setOptionValue($hazard);
            }

            if ($field->getAlias() == 'orientation' and $oldDevice['DevInstal'] != 'NA') {
                /** @var DynamicFieldDropboxChoices $orientation */
                $orientation = $this->getOrientation($oldDevice);
                $newValue->setOptionValue($orientation);
            }

            if ($field->getAlias() == 'approved_installation' and $oldDevice['ABPActive'] == 'Unapproved') {
                $newValue->setOptionValue($this->choiceApprovedInstallationNo);
            }

            $newValue->setDevice($device);
            $newValue->setField($field);
            $device->addField($newValue);
            $this->om->persist($newValue);
        }

        $device->setTitle($this->deviceManager->getTitle($device));
        $this->om->persist($device);

        return $device;
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getSize($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'DevSize', 'size');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getType($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'DevType', 'type');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getMake($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'DevMake', 'make');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getHazard($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'DevHazzard', 'hazard');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getOrientation($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'DevInstal', 'orientation');
    }

    /**
     * @param $oldDevice
     * @param $oldFieldName
     * @param $newFieldName
     * @return null
     */
    private function getDynamicField($oldDevice, $oldFieldName, $newFieldName)
    {
        $oldValue = $oldDevice[$oldFieldName];
        if (isset($this->dynamicFields[$newFieldName][$oldValue])) {
            return $this->dynamicFields[$newFieldName][$oldValue];
        }

        return null;
    }

    /**
     * @param $oldDevice
     * @param Device $newDevice
     * @param Account $account
     * @param $lastContractor
     * @return Service
     */
    private function createServiceByOldDevice($oldDevice, Device $newDevice, Account $account, $lastContractor)
    {
        /** @var Service $newService */
        $newService = new Service();
        $newService->setDevice($newDevice);
        $newService->setAccount($account);
        $newService->setNamed($this->serviceNamedAnnualBackflowInspection);
        $newService->setFixedPrice($oldDevice['DevFee']);

        $lastTested = $this->getDateTime($oldDevice['DevLastTestDate'] ? $oldDevice['DevLastTestDate'] : "1900-01-01 00:00:00");
        $newService->setLastTested($lastTested);
        $inspectionDue = $this->getDateTime($oldDevice['DevRetestDate'] ? $oldDevice['DevRetestDate'] : "1900-01-01 00:00:00");
        $newService->setInspectionDue($inspectionDue);

        if ($lastContractor) {
            $newService->setCompanyLastTested($lastContractor);
        }

        if (!empty($newService->getAccount()->getMunicipality())) {
            /** @var DepartmentChannel $departmentChannel */
            $departmentChannel = $this->departmentChannelManager->getChannelByDivision($newService);
            $newService->setDepartmentChannel($departmentChannel);
        } else {
            $this->importErrorMessageManager->setErrorMessage('[import devices error][OldDeviceId:' . $oldDevice['DevID'] . ' ]: department error AccountId: ' . $oldDevice['AccountID']);
        }

        $contractorType = $this->contractorTypeRepository->findOneBy(['alias' => 'my_company']);
        $contractorService = $this->contractorServiceRepository->getByServiceNamedId($newService->getNamed(), $contractorType);

        if (!empty($contractorService)) {
            $newService->setContractorService($contractorService[0]);
        }

        $this->om->persist($newService);

        return $newService;
    }

    /**
     * @param $oldAccountId
     * @param $lastTestedDate
     * @return Opportunity|bool
     */
    private function getOpportunityFromCache($oldAccountId, $lastTestedDate)
    {
        /** @var Opportunity $opportunity */
        $opportunity = false;
        if (isset($this->opportunityCache[$oldAccountId][$lastTestedDate])) {
            $opportunity = $this->opportunityCache[$oldAccountId][$lastTestedDate];
        }

        return $opportunity;
    }

    /**
     * @param $oldDateTime
     * @return bool|\DateTime
     */
    private function getDateTime($oldDateTime)
    {
        $dateTime = null;
        $oldDateTimeArray = explode(' ', $oldDateTime);
        if (!empty($oldDateTimeArray)) {
            $dateTime = \DateTime::createFromFormat('Y-m-d', $oldDateTimeArray[0]);
        }

        return $dateTime;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i != 0) {
            $this->om->flush();
        }
    }

    /**
     * @param $path
     * @return false|int
     */
    private function isValidImagePath($path)
    {
        return preg_match('/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/', $path);
    }
}
