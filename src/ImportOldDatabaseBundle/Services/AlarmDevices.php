<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DeviceStatus;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Opportunity;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\DeviceStatusRepository;
use AppBundle\Entity\Repository\DynamicFieldDropboxChoicesRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\OpportunityRepository;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use AppBundle\Entity\EntityManager\OpportunityManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output;

class AlarmDevices
{
    private const IMPORT_TABLE_FIRE = 'abfp.dbo.Fire AS f';
    private const NEED_FLUSH_ON = 1000;
    private const FIRE_FIELDS = 'f.FirSysID, f.FirImage, f.SysComments, f.SysDesc, f.AccountID, f.DelFir, f.SysType,
        f.SysPrice, f.SysLaTeDate, f.SysReTeDate, f.ComLaInspCo';
    private const FIRE_CONDITIONS = 'WHERE f.SysType IN (
        \'Alarm\', \'Alarm System\', \'Alarm Test\', \'Alarm Testing\', \'Fire Alarm\', \'Fire Alarm Panel\',
         \'Fire Alarm Control Panel (FACP)\', \'Fire Alarm Testing\', \'Fire Panel - FCI-7200\', \'Fire Alarm System\',
         \'Fire Alarm Panel\')';
    const IMPORT_TABLE_MAIN1 = 'abfp.dbo.Main1 AS m';
    const MAIN1_FIELDS = 'm.AccountID, m.AlarmPhone, m.AlarmPanLoc, m.AlarmPosition, m.AlarmPasscode';
    const MAIN1_CONDITIONS = 'WHERE m.AlarmPhone IS NOT NULL OR m.AlarmPanLoc IS NOT NULL OR m.AlarmPosition IS NOT NULL 
            OR m.AlarmPasscode IS NOT NULL OR m.AlarmPasscode <> \' \'';

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    private $opportunityCache = [];

    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var  OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var  AccountRepository */
    private $accountRepository;
    /** @var DeviceNamedRepository */
    private $deviceNamedRepository;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var DepartmentChannelManager $departmentChannelManager */
    private $departmentChannelManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var OpportunityManager */
    private $opportunityManager;
    /** @var OpportunityRepository */
    private $opportunityRepository;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var DynamicFieldDropboxChoicesRepository */
    private $choicesRepository;
    /** @var DeviceStatusRepository */
    private $deviceStatusRepository;
    /** @var DeviceStatus */
    private $deviceStatusActive;
    /** @var DeviceStatus */
    private $deviceStatusInActive;
    /** @var DeviceNamed[] */
    private $rootDeviceNameds;
    /** @var DeviceNamed[] */
    private $subDeviceNamed;
    private $dynamicFields = [
        'size' => []
    ];
    /** @var array */
    private $oldDevices;
    /** @var ContractorRepository */
    private $contractorRepository;
    /** @var array */
    private $contractors;
    /** @var array */
    private $main;
    /** @var ServiceNamed */
    private $serviceName;
    /** @var DeviceManager */
    private $deviceManager;
    /**
     * MasLevelToAccount constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
        $this->departmentChannelManager = $this->container->get('app.department_channel.manager');
        $this->serviceManager = $this->container->get('app.service.manager');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
        $this->opportunityManager = $this->container->get('app.opportunity.manager');

        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->deviceStatusRepository = $this->om->getRepository('AppBundle:DeviceStatus');
        $this->accountRepository = $this->om->getRepository('AppBundle:Account');
        $this->deviceNamedRepository = $this->om->getRepository('AppBundle:DeviceNamed');
        $this->serviceNamedRepository = $this->om->getRepository('AppBundle:ServiceNamed');
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->opportunityRepository = $this->om->getRepository('AppBundle:Opportunity');
        $this->choicesRepository = $this->om->getRepository('AppBundle:DynamicFieldDropboxChoices');
        $this->deviceRepository = $this->om->getRepository('AppBundle:Device');
        $this->contractorRepository = $this->om->getRepository('AppBundle:Contractor');
        $this->deviceManager = $this->container->get("app.device.manager");

        $this->deviceStatusActive = $this->deviceStatusRepository->findOneBy(['alias' => 'active']);
        $this->deviceStatusInActive = $this->deviceStatusRepository->findOneBy(['alias' => 'inactive']);

        $this->oldDevices = $this->oldDatabaseRepository->getDataByCondition(
            self::IMPORT_TABLE_FIRE, self::FIRE_FIELDS, self::FIRE_CONDITIONS
        );
        $this->main = $this->oldDatabaseRepository->getDataByCondition(
            self::IMPORT_TABLE_MAIN1,
            self::MAIN1_FIELDS,
            self::MAIN1_CONDITIONS
        );

        $this->serviceName = $this->serviceNamedRepository->findOneBy(['name' => 'Annual Fire Alarm System Inspection']);

        $this->rootDeviceNameds = [
            'Alarm' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Alarm System' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Alarm Test' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Alarm Testing' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Fire Alarm ' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Fire Alarm' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Fire Alarm Test' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Fire Alarm Control Panel (FACP)' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Fire Alarm Testing' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Fire Panel - FCI-7200 ' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Fire Alarm System' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']),
            'Fire Alarm Panel' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel'])
        ];

        $this->subDeviceNamed = $this->deviceNamedRepository->findOneBy(['alias' => 'fire_alarm_control_panel']);

        $this->contractors = [
            'A & A Sprinkler Company' => $this->contractorRepository->findOneBy(['name' => 'A & A Sprinkler Company']),
            'Absolute Fire Protection Inc.' => $this->contractorRepository->findOneBy(['name' => 'Absolute Fire Protection Inc.']),
            'Advanced Fire Protection & Safety Inc.' => $this->contractorRepository->findOneBy(['name' => 'Advanced Fire Protection & Safety Inc.']),
            'Affordable Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'Affordable Fire Protection, Inc.']),
            'Affordable Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Affordable Fire Protection, Inc.']),
            'Ahern Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'Ahern Fire Protection']),
            'Alarm Detection Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Alarm Detection Systems, Inc.']),
            'American Backflow & Fire Prevention, Inc.' => $this->contractorRepository->findOneBy(['name' => 'American Backflow & Fire Prevention']),
            'American Backflow &amp; Fire Prevention, Inc.' => $this->contractorRepository->findOneBy(['name' => 'American Backflow & Fire Prevention']),
            'American Backflow and Fire Prevention' => $this->contractorRepository->findOneBy(['name' => 'American Backflow & Fire Prevention']),
            'American Backflow Prevention Inc.' => $this->contractorRepository->findOneBy(['name' => 'American Backflow & Fire Prevention']),
            'C.L. Doucette, Inc.' => $this->contractorRepository->findOneBy(['name' => 'C.L. Doucette, Inc.']),
            'Cardinal Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Cardinal Fire Protection, Inc.']),
            'Central States Automatic Sprinklers, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Central States Automatic Sprinklers, Inc.']),
            'Century' => $this->contractorRepository->findOneBy(['name' => 'Century Automatic Sprinkler Company, Inc.']),
            'Century Automatic Sprinkler Company, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Century Automatic Sprinkler Company, Inc.']),
            'Chicago Metropolitan Fire Prevention Company' => $this->contractorRepository->findOneBy(['name' => 'Chicago Metropolitan Fire Prevention Company']),
            'Cybor Fire Protection Company' => $this->contractorRepository->findOneBy(['name' => 'Cybor Fire Protection Company']),
            'F. E. Moran, Inc. Fire Protection of N. Illinois' => $this->contractorRepository->findOneBy(['name' => 'F. E. Moran, Inc. Fire Protection of N. Illinois']),
            'Fe Moran' => $this->contractorRepository->findOneBy(['name' => 'Fe Moran']),
            'Fire Control Incorporated' => $this->contractorRepository->findOneBy(['name' => 'Fire Control Incorporated']),
            'Fox Valley Fire & Safety Company, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Fox Valley Fire & Safety Company, Inc.']),
            'Fox Valley Fire and Safety' => $this->contractorRepository->findOneBy(['name' => 'Fox Valley Fire and Safety']),
            'Fredriksen' => $this->contractorRepository->findOneBy(['name' => 'Fredriksen Fire Equipment Company']),
            'Fredriksen Fire Equipment Company' => $this->contractorRepository->findOneBy(['name' => 'Fredriksen Fire Equipment Company']),
            'Great Lakes Fire and Safety' => $this->contractorRepository->findOneBy(['name' => 'Great Lakes Fire and Safety']),
            'K & S Automatic Sprinklers, Inc.' => $this->contractorRepository->findOneBy(['name' => 'K & S Automatic Sprinklers, Inc.']),
            'Liberty Fire Protection Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Liberty Fire Protection Systems, Inc.']),
            'Metropolitan Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Metropolitan Fire Protection, Inc.']),
            'Monarch Fire Protection, Incorporated' => $this->contractorRepository->findOneBy(['name' => 'Monarch Fire Protection, Incorporated']),
            'MVP Fire Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'MVP Fire Systems, Inc.']),
            'Nelson Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'Nelson Fire Protection']),
            'Nova Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Nova Fire Protection, Inc.']),
            'Orion Fire Protection, Inc' => $this->contractorRepository->findOneBy(['name' => 'Orion Fire Protection, Inc']),
            'Other' => $this->contractorRepository->findOneBy(['name' => 'Other']),
            'Phoenix Fire Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Phoenix Fire Systems, Inc.']),
            'Profasts Incorporated' => $this->contractorRepository->findOneBy(['name' => 'Profasts Incorporated']),
            'RAM Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'RAM Fire Protection']),
            'Reliable Fire Equipment' => $this->contractorRepository->findOneBy(['name' => 'Reliable Fire Equipment']),
            'S. J. Carlson Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'S. J. Carlson Fire Protection']),
            'Shamrock Fire Protection LLC' => $this->contractorRepository->findOneBy(['name' => 'Shamrock Fire Protection LLC']),
            'SimplexGrinnell LP' => $this->contractorRepository->findOneBy(['name' => 'SimplexGrinnell LP']),
            'Skyhawk Sprinkler Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Skyhawk Sprinkler Systems, Inc.']),
            'Spillane Fire Protection LLC' => $this->contractorRepository->findOneBy(['name' => 'Spillane Fire Protection LLC']),
            'Total Fire' => $this->contractorRepository->findOneBy(['name' => 'Total Fire & Safety, Inc.']),
            'Total Fire & Safety, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Total Fire & Safety, Inc.']),
            'Tri State Fire Control Inc' => $this->contractorRepository->findOneBy(['name' => 'Tri State Fire Control Inc']),
            'Ultimate Fire Protection Inc.' => $this->contractorRepository->findOneBy(['name' => 'Ultimate Fire Protection Inc.']),
            'United States Alliance Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'United States Alliance Fire Protection, Inc.']),
            'Valley Fire Protection Services, LLC' => $this->contractorRepository->findOneBy(['name' => 'Valley Fire Protection Services, LLC']),
            'Xtreme Fire' => $this->contractorRepository->findOneBy(['name' => 'Xtreme Fire Protection, Inc.']),
            'Xtreme Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Xtreme Fire Protection, Inc.']),
        ];
    }

    // TODO: Потрібно робити вибірку з урахуванням дивіжина
    /**
     * @return array
     */
    public function getImportedDevicesOldIds()
    {
        $importedIds =[];
        $importedDevicesArray = $this->deviceRepository->getImportedDevices();
        foreach ($importedDevicesArray as $importedDevice) {
            $importedIds[] = $importedDevice["oldDeviceId"];
        }

        return $importedIds;
    }

    /**
     * @return mixed
     */
    public function import()
    {
        $quantityImportedDevices['device'] = $this->importAlarmSystem();
        $quantityImportedDevices['subDevice'] = $this->importAlarmPanels();

        return $quantityImportedDevices;
    }

    /**
     * @return int
     */
    public function importAlarmSystem()
    {
        $countImportedDevices = 0;
        $importedIds = $this->getImportedDevicesOldIds();

        $output = new Output\ConsoleOutput();
        $progress = new ProgressBar($output, count($this->oldDevices));
        $progress->start();

        foreach ($this->oldDevices as $oldDevice) {
            try {
                if (!in_array($oldDevice['FirSysID'], $importedIds)) {
                    $deviceNamed = $this->identifyNamed($oldDevice['SysType']);
                    if ($oldDevice['DelFir'] == 'del' or !$deviceNamed) {
                        continue;
                    }

                    /** @var Account $account */
                    $account = $this->accountRepository->findOneBy(['oldAccountId' => $oldDevice['AccountID']]);
                    if (!$account) {
                        $this->importErrorMessageManager->setErrorMessage('[import fire alarm devices error][OldDeviceId:' . $oldDevice['FirSysID'] . ' ]: don\'t find account with AccountId: ' . $oldDevice['AccountID']);
                        continue;
                    }

                    $oldAccountId = $oldDevice['AccountID'];
                    $lastTestedDate = $oldDevice['SysLaTeDate'];

                    /** @var Device $newDevice */
                    $newDevice = $this->createDeviceByOldDevice($oldDevice, $account);
                    /** @var Service $newService */
                    $newService = $this->createServiceByOldDevice($oldDevice, $newDevice, $account);
                    $opportunity = $this->opportunityManager->getNowExistingOpportunity($newService, $this->opportunityManager::TYPE_RETEST);

                    if (empty($opportunity)) {
                        $opportunity = $this->getOpportunityFromCache($oldAccountId, $lastTestedDate);
                    }

                    if (empty($opportunity)) {
                        $opportunity = $this->opportunityManager->creating($newService);
                    }

                    if ($this->opportunityManager->isPastDue($opportunity)) {
                        $this->opportunityManager->setStatus($opportunity, $this->opportunityManager::STATUS_PAST_DUE);
                    }

                    $opportunity->addService($newService);
                    $opportunity = $this->opportunityManager->updateSomeFields($opportunity);
                    $this->om->persist($opportunity);
                    $this->opportunityCache[$oldAccountId][$lastTestedDate] = $opportunity;

                    $newService->setOpportunity($opportunity);
                    $newService = $this->serviceManager->setStatus($newService, $this->serviceManager::STATUS_RETEST_OPPORTUNITY);
                    $this->om->persist($newService);
                    $this->refreshOldestOpportunityDate($opportunity->getAccount(), $newService->getInspectionDue());

                    $countImportedDevices++;
                    $progress->advance();

                    $this->needLoad($countImportedDevices);
                }
            } catch (\Exception $exception) {
                $this->importErrorMessageManager->setErrorMessage('[import fire devices error][OldDeviceId:' . $oldDevice['FirSysID'] . ' ]: department error AccountId: ' . $oldAccountId);
                continue;
            }
        }

        unset($this->oldDevices);
        $this->om->flush();
        $progress->finish();

        return $countImportedDevices;
    }

    /**
     * @return int
     */
    public function importAlarmPanels()
    {
        $countImportedDevices = 0;

        $output = new Output\ConsoleOutput();
        $progress = new ProgressBar($output, count($this->main));
        $progress->start();

        foreach ($this->main as $main) {
            try {
                /** @var Account $account */
                $account = $this->accountRepository->findOneBy(['oldAccountId' => $main['AccountID']]);
                if (!$account) {
                    $this->importErrorMessageManager->setErrorMessage('[import Alarm Panels error][AccountID:' . $main['AccountID'] . ' ]: don\'t find account with AccountId: ' . $main['AccountID']);
                    continue;
                }
                /** @var Device|null $deviceFireAlarmSystem */
                $deviceFireAlarmSystem = null;

                /** @var Device $device */
                foreach ($account->getDevices() as $device) {
                    if ($device->getNamed()->getAlias() == 'fire_alarm_control_panel') {
                        $deviceFireAlarmSystem = $device;
                        break;
                    }
                }

                if (empty($deviceFireAlarmSystem)) {
                    $oldDevice = [
                        'SysPrice'=>null, 'SysLaTeDate'=>null, 'SysReTeDate'=>null, 'FirSysID'=>null,
                        'AccountID'=>$main['AccountID'], 'ComLaInspCo'=>null
                    ];
                    /** @var Device $deviceFireAlarmSystem */
//                    $deviceFireAlarmSystem = $this->createEmptyFireAlarmSystem($account);
                    $deviceFireAlarmSystem = $this->createAlarmPanelByMain($main, $account);
                    /** @var Service $newService */
                    $newService = $this->createServiceByOldDevice($oldDevice, $deviceFireAlarmSystem, $account);

                    $opportunity = $this->opportunityManager->getNowExistingOpportunity($newService, $this->opportunityManager::TYPE_RETEST);

                    if (empty($opportunity)) {
                        $opportunity = $this->getOpportunityFromCache($main['AccountID'], $oldDevice['SysLaTeDate']);
                    }

                    if (empty($opportunity)) {
                        $opportunity = $this->opportunityManager->creating($newService);
                    }

                    if ($this->opportunityManager->isPastDue($opportunity)) {
                        $this->opportunityManager->setStatus($opportunity, $this->opportunityManager::STATUS_PAST_DUE);
                    }

                    $opportunity->addService($newService);
                    $opportunity = $this->opportunityManager->updateSomeFields($opportunity);
                    $this->om->persist($opportunity);
                    $this->opportunityCache[$main['AccountID']][$oldDevice['SysLaTeDate']] = $opportunity;

                    $newService->setOpportunity($opportunity);
                    $newService = $this->serviceManager->setStatus(
                        $newService,
                        $this->serviceManager::STATUS_RETEST_OPPORTUNITY
                    );
                    $this->om->persist($newService);
                    $this->refreshOldestOpportunityDate($opportunity->getAccount(), $newService->getInspectionDue());

                } else {
                    $this->setDynamicFields($deviceFireAlarmSystem, $main);

                }

//                $this->createAlarmPanelByMain($main, $deviceFireAlarmSystem, $account);

                $countImportedDevices++;
                $progress->advance();

                $this->needLoad($countImportedDevices);

            } catch (\Exception $exception) {
                $this->importErrorMessageManager->setErrorMessage('[import Alarm Panels error][AccountID:' . $main['AccountID'] . ' ]: Alarm Panel error AccountId: ' . $main['AccountID']);
                continue;
            }
        }

        unset($this->main);
        $this->om->flush();
        $progress->finish();

        return $countImportedDevices;
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function updateDevicesImagesLinks()
    {
        return $this->oldDatabaseRepository->changeImagesLinksForDevices();
    }

    /**
     * @param $oldDevice
     * @param Device $newDevice
     * @param Account $account
     * @return Service
     */
    private function createServiceByOldDevice($oldDevice, Device $newDevice, Account $account)
    {
        /** @var Service $newService */
        $newService = new Service();
        $newService->setDevice($newDevice);
        $newService->setAccount($account);
        $newService->setNamed($this->serviceName);
        $newService->setFixedPrice($oldDevice['SysPrice']);

        $lastTested = $this->getDateTime($oldDevice['SysLaTeDate']);
        $newService->setLastTested(!empty($lastTested) ? $lastTested : null);
        $inspectionDue = $this->getDateTime($oldDevice['SysReTeDate']);
        $newService->setInspectionDue(
            !empty($inspectionDue) ? $inspectionDue : \DateTime::createFromFormat('Y-m-d', '1900-01-01')
        );

        if ($contractor = $this->getContractor($oldDevice)) {
            $newService->setCompanyLastTested($contractor);
        }

        if (!empty($account->getMunicipality())) {
            /** @var DepartmentChannel $departmentChannel */
            $departmentChannel = $this->departmentChannelManager->getChannelByDivision($newService);
            $newService->setDepartmentChannel($departmentChannel);
        } else {
            $this->importErrorMessageManager->setErrorMessage('[import devices error][OldDeviceId:' . $oldDevice['FirSysID'] . ' ]: department error AccountId: ' . $oldDevice['AccountID']);
        }

        $this->om->persist($newService);

        return $newService;
    }

    /**
     * @param $oldDevice
     * @param Account $account
     * @return Device
     */
    private function createDeviceByOldDevice($oldDevice, Account $account)
    {
        $device = new Device();

        $device->setOldDeviceId($oldDevice['FirSysID']);
        $device->setStatus($this->deviceStatusActive);
        $device->setAccount($account);

        /** @var DeviceNamed $deviceNamed */
        $deviceNamed = $this->identifyNamed($oldDevice['SysType']);
        $device->setNamed($deviceNamed);

        $comments = '';
        if ($oldDevice['SysComments'] and $oldDevice['SysDesc']) {
            $comments = $oldDevice['SysComments'] . ' | ' . $oldDevice['SysDesc'];
        } else {
            $comments .= $oldDevice['SysComments'];
            $comments .= $oldDevice['SysDesc'];
        }
        $device->setComments($comments);
        $device->setSourceEntity('Fire Device');
        $device->setTitle($this->deviceManager->getTitle($device));
        $device = $this->createDynamicfields($device);

        $this->om->persist($device);

        return $device;
    }

    /**
     * @param $sysType
     * @return DeviceNamed|bool|mixed
     */
    private function identifyNamed($sysType)
    {
        $named = false;
        if (isset($this->rootDeviceNameds[$sysType])) {
            $named = $this->rootDeviceNameds[$sysType];
        }

        return $named;
    }

    /**
     * @param $oldDateTime
     * @return bool|\DateTime
     */
    private function getDateTime($oldDateTime)
    {
        $dateTime = null;
        $oldDateTimeArray = explode(' ', $oldDateTime);
        if (!empty($oldDateTimeArray)) {
            $dateTime = \DateTime::createFromFormat('Y-m-d', $oldDateTimeArray[0]);
        }

        return $dateTime;
    }

    /**
     * @param $oldDevice
     * @return bool|mixed
     */
    private function getContractor($oldDevice)
    {
        return $this->contractors[$oldDevice['ComLaInspCo']] ?? null;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i != 0) {
            $this->om->flush();
        }
    }

    /**
     * @param $main
     * @param $account
     * @return Device
     */
    private function createAlarmPanelByMain($main, $account)
    {
        $device = new Device();

        $device->setStatus($this->deviceStatusActive);
        $device->setLocation($main['AlarmPanLoc']);
        $device->setAccount($account);
        $device->setNamed($this->subDeviceNamed);
        $device->setTitle($this->deviceManager->getTitle($device));

        $this->om->persist($device);

        $this->setDynamicFields($device, $main);

        return $device;
    }

    /**
     * @param $oldAccountId
     * @param $lastTestedDate
     * @return Opportunity|bool
     */
    private function getOpportunityFromCache($oldAccountId, $lastTestedDate)
    {
        /** @var Opportunity $opportunity */
        $opportunity = false;
        if (isset($this->opportunityCache[$oldAccountId][$lastTestedDate])) {
            $opportunity = $this->opportunityCache[$oldAccountId][$lastTestedDate];
        }

        return $opportunity;
    }

    /**
     * @param Account $account
     * @param $newOldestDate
     */
    private function refreshOldestOpportunityDate(Account $account, $newOldestDate)
    {
        $currentOldestDate = $account->getOldestOpportunityDate();
        if (!$currentOldestDate or $currentOldestDate > $newOldestDate) {
            $account->setOldestOpportunityAlarmDate($newOldestDate);
            $account->setOldestOpportunityDate($newOldestDate);
        }

        $this->om->persist($account);

        if ($account->getParent()) {
            $this->refreshOldestOpportunityDate($account->getParent(), $newOldestDate);
        }
    }

    /**
     * @param Account $account
     * @return Device
     */
    private function createEmptyFireAlarmSystem(Account $account)
    {
        $device = new Device();

        $device->setStatus($this->deviceStatusActive);
        $device->setAccount($account);

        $device->setNamed($this->rootDeviceNameds['Alarm']);

        $device->setComments('Empty Fire Alarm Control Panel (FACP) device');

        $this->om->persist($device);

        return $device;
    }

    /**
     * @param Device $device
     * @param $main
     */
    private function setDynamicFields(Device $device, $main)
    {
        /** @var DynamicField[] $fields */
        $fields = $this->subDeviceNamed->getFields();
        /** @var DynamicField $field */
        foreach ($fields as $field) {
            $newValue = new DynamicFieldValue();
            $namedAlias = $this->subDeviceNamed->getAlias();
            $importedField = '';

            if ($namedAlias == 'fire_alarm_control_panel' and  $field->getAlias() == 'phone_number'
                and $main['AlarmPhone'] != 'NA'
            ) {
                $newValue->setValue($main['AlarmPhone']);
                $importedField = $field->getAlias();
            }

            if ($namedAlias == 'fire_alarm_control_panel' and  $field->getAlias() == 'position_number'
                and $main['AlarmPosition'] != 'NA'
            ) {
                $newValue->setValue($main['AlarmPosition']);
                $importedField = $field->getAlias();
            }

            if ($namedAlias == 'fire_alarm_control_panel' and  $field->getAlias() == 'passcode'
                and $main['AlarmPasscode'] != 'NA'
            ) {
                $newValue->setValue($main['AlarmPasscode']);
                $importedField = $field->getAlias();
            }

            if (empty($importedField)) {
                $newValue->setValue(null);
            }

            $newValue->setDevice($device);
            $newValue->setField($field);
            $device->addField($newValue);
            $this->om->persist($newValue);
        }
    }

    private function createDynamicfields(Device $device)
    {
        /** @var DynamicField $fields */
        $fields = $device->getNamed()->getFields();
        foreach ($fields as $field) {
            $newValue = new DynamicFieldValue();
            $newValue->setDevice($device);
            $newValue->setField($field);
            $device->addField($newValue);
            $this->om->persist($newValue);
        }

        return $device;
    }

}