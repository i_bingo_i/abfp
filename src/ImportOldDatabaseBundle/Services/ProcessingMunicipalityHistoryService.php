<?php
namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\EntityManager\MunicipalityHistoryManager;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\MunicipalityRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessingMunicipalityHistoryService
{
    private const NEED_FLUSH_ON = 1000;

    /** @var  ObjectManager */
    private $om;
    /** @var  ContainerInterface */
    private $container;
    /** @var  MunicipalityRepository */
    private $municipalityRepository;
    /** @var  MunicipalityHistoryManager */
    private $municipalityHistoryManager;
    /** @var  EntityManager */
    private $entityManager;
    /** @var OldDatbaseRepository */
    private $oldDatbaseRepository;

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->entityManager = $this->container->get('app.entity_manager');
        $this->municipalityRepository = $this->om->getRepository('AppBundle:Municipality');
        $this->municipalityHistoryManager = $this->container->get('app.municipality_history.manager');
        $this->oldDatbaseRepository = $this->container->get('import.old_database.repository');
    }

    /**
     * @return int
     */
    public function processMunicipalityHistory()
    {
        $i = 0;
        $municipalityArray = $this->municipalityRepository->findAll();
        if (!empty($municipalityArray)) {
            /** @var Municipality $municipality */
            foreach ($municipalityArray as $municipality) {
                $this->municipalityHistoryManager->createAccountHistoryItem($municipality, 0);
                $i++;
                $this->needLoad($i);
            }
            $this->om->flush();
        }
        unset($municipalityArray);

        return $i;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }
}
