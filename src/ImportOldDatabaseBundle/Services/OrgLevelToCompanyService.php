<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\EntityManager\CompanyManager;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\State;
use Doctrine\Common\Persistence\ObjectManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class OrgLevelToCompanyService
{
    private const TABLE_ORG_LEVEL = 'abfp.dbo.OrgLevel';
    private const ADDRESS_TYPE_PERSONAL = 'personal';
    private const ADDRESS_TYPE_COMPANY = 'company address';
    private const NEED_FLUSH_ON = 1000;

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var  OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var  StateRepository */
    private $stateRepository;
    /** @var  AddressTypeRepository */
    private $addressTypeRepository;
    /** @var  ContactPersonManager */
    private $contactPersonManager;
    /** @var  CompanyManager */
    private $companyManager;
    /** @var  CompanyRepository */
    private $companyRepository;
    /** @var ImportErrorMessageManager  */
    private $importErrorMessageManager;

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->contactPersonManager = $this->container->get('app.contact_person.manager');
        $this->companyManager = $this->container->get('app.company.manager');
        $this->stateRepository = $this->om->getRepository('AppBundle:State');
        $this->addressTypeRepository = $this->om->getRepository('AppBundle:AddressType');
        $this->companyRepository = $this->om->getRepository('AppBundle:Company');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
    }

    /**
     * @return array
     */
    public function getAllCompaniesFromOldDatabase()
    {
        return $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::TABLE_ORG_LEVEL);
    }

    /**
     * @return array
     */
    public function getImportedCompaniesOldIds()
    {
        $importedIds =[];
        $importedCompaniesArray = $this->companyRepository->getImportedCompanies();
        foreach ($importedCompaniesArray as $importedCompany) {
            $importedIds[] = $importedCompany["oldCompanyId"];
        }

        return $importedIds;
    }

    /**
     * @return int
     */
    public function importCompanies()
    {
        $i = 0;
        $oldCompanies = $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::TABLE_ORG_LEVEL);
        $importedIds = $this->getImportedCompaniesOldIds();

        foreach ($oldCompanies as $oldCompany) {
            $error = $this->importErrorMessageManager->checkOrgData($oldCompany);
            if ($error) {
                continue;
            }

            if (!in_array($oldCompany['OrgID'], $importedIds)) {
                $newCompany = $this->setCompanyTextFields($oldCompany);
                $newCompany = $this->createAndSetCompanyAddress($oldCompany, $newCompany);
                if (!empty($oldCompany["OrgLName"]) ||
                    !empty($oldCompany["OrgFName"]) ||
                    !empty($oldCompany["OrgTitle"]) ||
                    !empty($oldCompany["OrgPhone"]) ||
                    !empty($oldCompany["OrgExt"]) ||
                    !empty($oldCompany["OrgCell"]) ||
                    !empty($oldCompany["OrgFax"]) ||
                    !empty($oldCompany["OrgEmail"])
                ) {
                    $this->createCompaniesContactPerson($oldCompany, $newCompany);
                }
                if (!empty($oldCompany["OrgLName2"]) ||
                    !empty($oldCompany["OrgFName2"]) ||
                    !empty($oldCompany["OrgTitle2"]) ||
                    !empty($oldCompany["OrgPhone2"]) ||
                    !empty($oldCompany["OrgExt2"]) ||
                    !empty($oldCompany["OrgCell2"]) ||
                    !empty($oldCompany["OrgFax2"]) ||
                    !empty($oldCompany["OrgEmail2"])
                ) {
                    $this->createCompaniesContactPerson($oldCompany, $newCompany, 2);
                }
                $this->om->persist($newCompany);

                $i++;
                $this->needLoad($i);
            }
        }

        unset($oldData);
        unset($oldCompanies);
        unset($importedIds);
        $this->om->flush();

        return $i;
    }

    /**
     * @param \DateTime $datetime1
     * @param \DateTime $datetime2
     * @return string
     */
    public function getTimedifference(\DateTime $datetime1, \DateTime $datetime2)
    {
        $resultInterval = "Process took:";

        $interval = $datetime1->diff($datetime2);

        if ($interval->h > 0) {
            $resultInterval .= " ".$interval->h." hours";
        }
        if ($interval->i > 0) {
            $resultInterval .= " ".$interval->i." minutes";
        }
        if ($interval->s > 0) {
            $resultInterval .= " ".$interval->s." seconds";
        }

        return $resultInterval;
    }

    /**
     * @param $oldCompany
     * @return Company
     */
    private function setCompanyTextFields($oldCompany)
    {
        /** @var Company $newCompay */
        $newCompay = new Company();
        $newCompay->setName($oldCompany['OrgName']);
        $newCompay->setOldCompanyId($oldCompany['OrgID']);
        $newCompay->setNotes($oldCompany['OrgComments']);
        $newCompay->setWebsite($oldCompany['OrgURL']);
        $newCompay->setSourceEntity('Org');
        $this->om->persist($newCompay);

        return $newCompay;
    }

    /**
     * @param $oldCompany
     * @param Company $newCompany
     * @return Company
     */
    private function createAndSetCompanyAddress($oldCompany, Company $newCompany)
    {
            /** @var Address $address */
            $newAddress = new Address();
            $state = null;
            $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_COMPANY]);

            if ($addressType) {
                $newAddress->setAddressType($addressType);
            }

            if ($oldCompany['OrgState']) {
                /** @var State $state */
                $state = $this->stateRepository->findOneBy(['code' => strtoupper($oldCompany['OrgState'])]);
            }

            if ($state) {
                $newAddress->setState($state);
            }

            $addressLine = ($oldCompany['OrgAddress1'] ? $oldCompany['OrgAddress1'] : '') .
                ($oldCompany['OrgAddress2'] ? " " . $oldCompany['OrgAddress2'] : '');


            $newAddress->setAddress($addressLine);
            $newAddress->setCity($oldCompany['OrgCity']);
            $newAddress->setZip($oldCompany['OrgZip']);

            $this->om->persist($newAddress);

            $newCompany->setAddress($newAddress);

            $this->om->persist($newCompany);

            return $newCompany;
    }

    /**
     * @param $oldCompany
     * @param Company $newCompany
     * @param int $cpNum
     */
    private function createCompaniesContactPerson($oldCompany, Company $newCompany, $cpNum = 1)
    {
        /** @var ContactPerson $newContaPerson */
        $newContaPerson = new ContactPerson();
        $newContaPerson->setFirstName($oldCompany['OrgFName'.($cpNum == 2 ? 2 : '')] ?? 'Property Owner');
        $newContaPerson->setLastName($oldCompany['OrgLName'.($cpNum == 2 ? 2 : '')]);
        $newContaPerson->setTitle($oldCompany['OrgTitle'.($cpNum == 2 ? 2 : '')]);
        $newContaPerson->setPhone($oldCompany['OrgPhone'.($cpNum == 2 ? 2 : '')]);
        $newContaPerson->setExt($oldCompany['OrgExt'.($cpNum == 2 ? 2 : '')]);
        $newContaPerson->setCell($oldCompany['OrgCell'.($cpNum == 2 ? 2 : '')]);
        $newContaPerson->setFax($oldCompany['OrgFax'.($cpNum == 2 ? 2 : '')]);
        $newContaPerson->setEmail($oldCompany['OrgEmail'.($cpNum == 2 ? 2 : '')]);
        $newContaPerson->setSourceEntity('Org');
        $newContaPerson->setPersonalAddress($this->createEmptyAddressForCP());
        $newContaPerson->setSourceId($oldCompany['OrgID']);
        $newContaPerson->setNumContact($cpNum);
        $newContaPerson->setCompany($newCompany);

        $this->om->persist($newContaPerson);
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

    /**
     * @return Address
     */
    private function createEmptyAddressForCP()
    {
        $newAddress = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_PERSONAL]);
        $newAddress->setAddressType($addressType);
        $this->om->persist($newAddress);

        return $newAddress;
    }
}