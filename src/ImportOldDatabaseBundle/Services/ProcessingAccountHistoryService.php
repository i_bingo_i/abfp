<?php
namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\EntityManager\AccountHistoryManager;
use AppBundle\Entity\Repository\AccountRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessingAccountHistoryService
{
    private const NEED_FLUSH_ON = 1000;
    /** @var  ObjectManager */
    private $om;
    /** @var  ContainerInterface */
    private $container;
    /** @var  AccountRepository */
    private $accountRepository;
    /** @var  AccountHistoryManager */
    private $accountHistoryManager;
    /** @var  EntityManager */
    private $entityManager;
    /** @var OldDatbaseRepository */
    private $oldDatbaseRepository;

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->entityManager = $this->container->get('app.entity_manager');
        $this->accountRepository = $this->om->getRepository('AppBundle:Account');
        $this->accountHistoryManager = $this->container->get('app.account_history.manager');
        $this->oldDatbaseRepository = $this->container->get('import.old_database.repository');
    }

    /**
     * @return int
     */
    public function processAccountHistory()
    {
        $i = 0;
        $accountArray = $this->accountRepository->findBy(['isHistoryProcessed' => false]);
        if (!empty($accountArray)) {
            foreach ($accountArray as $account) {
                $this->accountHistoryManager->createAccountHistoryItem($account, 0);
                $account->setIsHistoryProcessed(true);
                $i++;
                $this->needLoad($i);
            }
            $this->om->flush();
        }
        unset($accountArray);

        return $i;
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function queryProcessAccountHistory()
    {
        return  $this->oldDatbaseRepository->queryProcessAccountHistory();
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeAllAccountHistory()
    {
        return  $this->oldDatbaseRepository->removeAllAccountHistory();
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

}