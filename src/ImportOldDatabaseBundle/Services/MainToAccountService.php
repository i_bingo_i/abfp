<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountBuildingType;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\Coordinate;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\AccountBuildingTypeRepository;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\AccountTypeRepository;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\ClientTypeRepository;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Entity\Repository\ContactPersonRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\StateRepository;
use Doctrine\Common\Persistence\ObjectManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MainToAccountService
{
    public const TABLE_MAIN1 = 'abfp.dbo.Main1';
    public const ACCOUNT_TYPE_ACCOUNT = 'account';
    public const ACCOUNT_CLIENT_TYPE_ACTIVE = 'active';
    private const ADDRESS_TYPE_PERSONAL = 'personal';
    public const ADDRESS_TYPE_SITE_ADDRESS = 'site address';
    public const DEFAULT_MUNICIPALITY = 'City of Chicago';
    public const TYPE_BUILDING_ACCOUNT_RESIDENTIAL = 'residential';
    public const TYPE_BUILDING_ACCOUNT_COMMERCIAL = 'commercial';
    private const NEED_FLUSH_ON = 1000;

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var AccountTypeRepository */
    private $accountTypeRepository;
    /** @var StateRepository */
    private $stateRepository;
    /** @var CompanyRepository */
    private $companyRepository;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var AccountBuildingTypeRepository */
    private $accountBuildingTypeRepository;
    /** @var AccountManager */
    private $accountManager;
    /** @var ContactPersonManager */
    private $contactPersonManager;
    /** @var  AccountRepository */
    private $accountRepository;
    /** @var  ClientTypeRepository */
    private $clientTypeRepository;
    /** @var  MunicipalityRepository */
    private $municipalityRepository;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var MasLevelToAccountService */
    private $masLevelToAccountService;
    /** @var ContactPersonRepository */
    private $contactPersonRepository;
    /** @var array */
    private $groupAccounts = [];
    /** @var  MunicipalToMunicipalityService */
    private $municipalityImportService;

    /**
     * MasLevelToAccount constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;

        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->accountTypeRepository = $this->om->getRepository('AppBundle:AccountType');
        $this->clientTypeRepository = $this->om->getRepository('AppBundle:ClientType');
        $this->stateRepository = $this->om->getRepository('AppBundle:State');
        $this->companyRepository = $this->om->getRepository('AppBundle:Company');
        $this->addressTypeRepository = $this->om->getRepository('AppBundle:AddressType');
        $this->accountRepository = $this->om->getRepository('AppBundle:Account');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
        $this->accountBuildingTypeRepository = $this->om->getRepository('AppBundle:AccountBuildingType');
        $this->municipalityRepository = $this->om->getRepository('AppBundle:Municipality');
        $this->accountManager = $this->container->get('app.account.manager');
        $this->contactPersonManager = $this->container->get('app.contact_person.manager');
        $this->municipalityImportService = $this->container->get('import.municipal_to_municipality.service');
        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->masLevelToAccountService = $this->container->get('import.mas_level_to_account.service');
        $this->contactPersonRepository = $this->om->getRepository('AppBundle:ContactPerson');
    }

    /**
     * @return int
     */
    public function parseData()
    {
        $masLevelData = $this->oldDatabaseRepository->getAllTablesRowsByTableName(
            self::TABLE_MAIN1
//            " WHERE SiteMunicipalityID <> FireMunicipalityID"
        );
        $result = $this->dataImport($masLevelData);
        unset($masLevelData);

        return $result;
    }

    /**
     * @param array $oldData
     * @return int
     */
    private function dataImport(array $oldData)
    {
        $i = 0;
        $importedIds = $this->getImportedAccountOldIds();

        foreach ($oldData as $data) {
            $company = null;
            $municipality = null;
            if (!in_array($data['AccountID'], $importedIds)) {
                $error = $this->importErrorMessageManager->checkSiteData($data);

                if ($error) {
                    continue;
                }

                $municipality = $this->getMunicipalityForAccount($data);

                if (!empty($data['OrgID'])) {
                    $company = $this->companyRepository->findOneBy(['oldCompanyId' => $data['OrgID']]);
                }

                /** @var Account $account */
                $account = $this->setAccount($data, $company, $municipality);
                if (!empty($data['SiteConLName']) ||
                    !empty($data['SiteConFName']) ||
                    !empty($data['SiteTitle']) ||
                    !empty($data['SiteConPhone']) ||
                    !empty($data['SitePhoneExt']) ||
                    !empty($data['SiteConCell']) ||
                    !empty($data['SiteConFax']) ||
                    !empty($data['SiteConEmail'])
                ) {
                    /** @var ContactPerson $firstContactPerson */
                    $firstContactPerson = $this->setContactPerson($data, $company);
                    $accountContactPerson = $this->setAccountContactPerson($account, $firstContactPerson, true);
                    $this->setRoleAndMailingAddress($accountContactPerson, $account, $data);
                }
                if (!empty($data['SiteConLName2']) ||
                    !empty($data['SiteConFName2']) ||
                    !empty($data['SiteTitle2']) ||
                    !empty($data['SiteConPhone2']) ||
                    !empty($data['SitePhoneExt2']) ||
                    !empty($data['SiteConCell2']) ||
                    !empty($data['SiteConFax2']) ||
                    !empty($data['SiteConEmail2'])
                ) {
                    /** @var ContactPerson $secondContactPerson */
                    $secondContactPerson = $this->setContactPerson($data, $company, 2);
                    $this->setAccountContactPerson($account, $secondContactPerson);
                }
                $i++;

                $this->needLoad($i);
//                if ($i == 5000) {
//                    break;
//                }
            }
        }

        unset($oldData);
        unset($importedIds);

        $this->om->flush();

        return $i;
    }

    /**
     * @return array
     */
    public function getImportedAccountOldIds()
    {
        $importedIds =[];
        $importedAccountsArray = $this->accountRepository->getImportedAccount();
        foreach ($importedAccountsArray as $importedAccount) {
            $importedIds[] = $importedAccount["oldAccountId"];
        }

        return $importedIds;
    }

    /**
     * @param array $data
     * @param Company|null $company
     * @param Municipality|null $municipality
     * @return Account
     */
    private function setAccount(array $data, ?Company $company, ?Municipality $municipality)
    {
        $groupAccount = null;
        if (!empty($data['MasID'])) {
            $groupAccount = $this->accountRepository->findOneBy(
                ['oldAccountId' => $data['MasID']]
            );
        }

        $account = new Account();
        $account->setOldAccountId($data['AccountID']);
        $account->setOldLinkId($data['MasID']);
        $account->setName($data['SiteName'] ?? 'No name');
        $account->setAddress($this->setAddress($data));
        $account->setType($this->accountTypeRepository->findOneBy(['alias' => self::ACCOUNT_TYPE_ACCOUNT]));
        $account->setClientType($this->clientTypeRepository->findOneBy(['alias' =>self::ACCOUNT_CLIENT_TYPE_ACTIVE]));
        $account->setBuildingType(
            $this->accountBuildingTypeRepository->findOneBy(['alias' => strtolower($data['AccountType'])])
        );
        $account->setCoordinate($this->setCoordinate($data));

        if ($company) {
            $account->setCompany($company);
        }
        if ($groupAccount) {
            $account->setParent($groupAccount);
        }
        if ($municipality) {
            $account->setMunicipality($municipality);
            $municipality->addAccount($account);
            $parentAcc = $account->getParent();
            if ($parentAcc) {
                $parentAcc->setMunicipality($municipality);
                $this->om->persist($parentAcc);
            }
            $this->om->persist($municipality);

        }

        $account->setSiteSendTo($data['SiteSendTo']);
        $this->om->persist($account);

        return $account;
    }

    /**
     * @param array $data
     * @return Address
     */
    private function setAddress(array $data)
    {
        $state = $this->stateRepository->findOneBy(['code' => strtoupper($data['SiteState'])]);

        $address = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_SITE_ADDRESS]);

        if ($addressType) {
            $address->setAddressType($addressType);
        }
        $address->setAddress($data['SiteAddr1']);
        $address->setCity($data['SiteCity']);
        $address->setZip($data['SiteZip']);
        $address->setState($state);

        $this->om->persist($address);

        return $address;
    }

    /**
     * @param array $data
     * @param Company|null $company
     * @param string $serialNumber '' - first contact person 2 - second contact person in old database
     * @return ContactPerson
     */
    private function setContactPerson(array $data, ?Company $company, $serialNumber = '')
    {
        $contactPerson = new ContactPerson();
        $contactPerson->setFirstName($data['SiteConLName'.$serialNumber] ?? 'Property Owner');
        $contactPerson->setLastName($data['SiteConFName'.$serialNumber]);
        $contactPerson->setTitle($data['SiteTitle'.$serialNumber]);
        $contactPerson->setPhone($data['SiteConPhone'.$serialNumber]);
        $contactPerson->setExt($data['SitePhoneExt'.$serialNumber]);
        $contactPerson->setCell($data['SiteConCell'.$serialNumber]);
        $contactPerson->setFax($data['SiteConFax'.$serialNumber]);
        $contactPerson->setEmail($data['SiteConEmail'.$serialNumber]);
        $contactPerson->setSourceEntity("Main1");
        $contactPerson->setSourceId($data['AccountID']);
        $contactPerson->setPersonalAddress($this->createEmptyAddressForCP());
        $contactPerson->setNumContact(empty($serialNumber) ? 1 : $serialNumber);
        if ($company) {
            $contactPerson->setCompany($company);
        }
        $this->om->persist($contactPerson);

        return $contactPerson;
    }

    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @param bool $accessPrimary
     * @return AccountContactPerson
     */
    private function setAccountContactPerson(Account $account, ContactPerson $contactPerson, $accessPrimary = null)
    {
        $accountContactPerson = new AccountContactPerson();
        $accountContactPerson->setContactPerson($contactPerson);
        $accountContactPerson->setAccount($account);
        $accountContactPerson->setAccess(true);
        $accountContactPerson->setAccessPrimary($accessPrimary);
        $accountContactPerson->setSendingAddress($this->checkAddressForACP($accountContactPerson));

        $this->om->persist($accountContactPerson);

        return $accountContactPerson;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }


    /**
     * @param AccountContactPerson $accountContactPerson
     * @return Address
     */
    private function checkAddressForACP(AccountContactPerson $accountContactPerson)
    {
        /** @var ContactPerson $acpContactPerson */
        $acpContactPerson = $accountContactPerson->getContactPerson();
        /** @var Company $acpCompany */
        $acpCompany = $acpContactPerson->getCompany();
        /** @var Account $acpAccount */
        $acpAccount = $accountContactPerson->getAccount();

        if (!empty($acpAccount) && !empty($acpAccount->getAddress())) {
                return $acpAccount->getAddress();

        } elseif (!empty($acpCompany) && !empty($acpCompany->getAddress())) {
            return $acpCompany->getAddress();
        }

        return $acpContactPerson->getPersonalAddress();
    }

    /**
     * @return Address
     */
    private function createEmptyAddressForCP()
    {
        $newAddress = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_PERSONAL]);
        $newAddress->setAddressType($addressType);
        $this->om->persist($newAddress);

        return $newAddress;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @param Account $account
     */
    private function setAccountContactPersonRole(AccountContactPerson $accountContactPerson, Account $account)
    {
        if ($account->getSiteSendTo() === 'Site') {
            $accountContactPerson->setAuthorizer(true);
            $this->setResponsibility($accountContactPerson);

            $this->om->persist($accountContactPerson);

        } elseif ($account->getSiteSendTo() === 'Org') {
            $contactPerson = $this->contactPersonRepository->findOneBy(
                ['company' => $account->getCompany(), 'numContact' => '1', 'deleted' => '0']
            );

            if ($contactPerson) {
                $acp = $this->masLevelToAccountService->setAccountContactPerson($account, $contactPerson, true);
                if (!empty($account->getCompany())) {
                    $acp->setSendingAddress($account->getCompany()->getAddress());
                }

                $this->om->persist($acp);
            }

        } elseif ($account->getSiteSendTo() === 'Master' || $account->getSiteSendTo() === 'Both') {
            $groupAccount = $account->getParent();

            if (!empty($groupAccount)
                && !in_array($groupAccount->getId(), $this->groupAccounts)
            ) {
                $this->masLevelToAccountService->setConflictAccountContactPerson($groupAccount);
                $this->groupAccounts[] = $groupAccount->getId();
            }
        }
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     */
    private function setResponsibility(AccountContactPerson $accountContactPerson)
    {
        $allDeviceCategories = $this->deviceCategoryRepository->findAll();

        /** @var DeviceCategory $division */
        foreach ($allDeviceCategories as $division) {
            $accountContactPerson->addResponsibility($division);
        }
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @param Account $account
     * @param $data
     */
    private function setRoleAndMailingAddress(
        AccountContactPerson $accountContactPerson,
        Account $account,
        $data
    ) {
        if ($data['SiteLetter'] === 'Retest Notice') {
            $this->setAccountContactPersonRole($accountContactPerson, $account);
        }
        if ($data['SiteLetter'] === 'Master Letter') {
            $groupAccount = $account->getParent();

            if (!empty($groupAccount)
                && !in_array($groupAccount->getId(), $this->groupAccounts)) {
                $this->masLevelToAccountService->setConflictAccountContactPerson($groupAccount);
                $this->groupAccounts[] = $groupAccount->getId();
            }
        }
    }

    /**
     * @param $oldAccountData
     * @return \AppBundle\Entity\Municipality|bool|null|object
     */
    private function getMunicipalityForAccount($oldAccountData)
    {
        $backflowMunicipalityId = $oldAccountData["SiteMunicipalityID"];
        $fireMunicipalityId = $oldAccountData["FireMunicipalityID"];
        $finalMunicipalityId = null;

        if (!empty($backflowMunicipalityId) and
            !empty($fireMunicipalityId) and
            $backflowMunicipalityId == $fireMunicipalityId
        ) {
            $finalMunicipalityId = $backflowMunicipalityId;
        } elseif (!empty($backflowMunicipalityId) and empty($fireMunicipalityId)) {
            $finalMunicipalityId = $backflowMunicipalityId;
        } elseif (empty($backflowMunicipalityId) and !empty($fireMunicipalityId)) {
            $finalMunicipalityId = $fireMunicipalityId;
        } elseif (!empty($backflowMunicipalityId) and
            !empty($fireMunicipalityId) and
            $backflowMunicipalityId != $fireMunicipalityId
        ) {
            $finalMunicipality = $this->municipalityImportService->createAndGetMixedMunicipality(
                $backflowMunicipalityId,
                $fireMunicipalityId
            );

            return $finalMunicipality;
        }

        if (!empty($finalMunicipalityId)) {
            return $this->municipalityRepository->findOneBy(['oldMunicipalityId' => $finalMunicipalityId]);
        }

        return null;
    }

    /**
     * @param $data
     * @return Coordinate
     */
    private function setCoordinate($data)
    {
        $coordinate = new Coordinate();
        $coordinate->setLatitude(!empty($data['SLat']) ? $data['SLat'] : 0);
        $coordinate->setLongitude(!empty($data['SLog']) ? $data['SLog'] : 0);

        $this->om->persist($coordinate);

        return $coordinate;
    }
}
