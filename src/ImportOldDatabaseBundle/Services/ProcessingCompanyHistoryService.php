<?php
namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\EntityManager\CompanyHistoryManager;
use AppBundle\Entity\Repository\CompanyRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessingCompanyHistoryService
{
    private const NEED_FLUSH_ON = 1000;

    /** @var  ObjectManager */
    private $om;
    /** @var  ContainerInterface */
    private $container;
    /** @var  CompanyRepository */
    private $companyRepository;
    /** @var  CompanyHistoryManager */
    private $companyHistoryManager;

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->companyRepository = $this->om->getRepository('AppBundle:Company');
        $this->companyHistoryManager = $this->container->get('app.company_history.manager');
    }

    /**
     * @return int
     */
    public function processCompanyHistory()
    {
        $i = 0;
        $companiesArray = $this->companyRepository->findBy(['isHistoryProcessed' => false]);
        if (!empty($companiesArray)) {
            foreach ($companiesArray as $company) {
                $this->companyHistoryManager->createCompanyHistoryItem($company, 0);
                $company->setIsHistoryProcessed(true);
                $i++;
                $this->needLoad($i);
            }
            $this->om->flush();
        }
        unset($companiesArray);

        return $i;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

}