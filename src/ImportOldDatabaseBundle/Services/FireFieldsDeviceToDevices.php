<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Device;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output;
use AppBundle\Entity\Repository\DynamicFieldDropboxChoicesRepository;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;

class FireFieldsDeviceToDevices
{
    private const IMPORT_TABLE = 'abfp.dbo.Fire';
    private const NEED_FLUSH_ON = 1000;

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;

    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var  OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var DynamicFieldDropboxChoicesRepository */
    private $choicesRepository;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var array */
    private $dynamicFields = [];

    /**
     * MasLevelToAccount constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;

        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->deviceRepository = $this->om->getRepository('AppBundle:Device');
        $this->choicesRepository = $this->om->getRepository('AppBundle:DynamicFieldDropboxChoices');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');

        $this->dynamicFields['fire_system_size'] = [
            '1' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_1']),
            '1 1/4' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_1_dot_25']),
            '1 1/2' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_1_dot_5']),
            '1.5"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_1_dot_5']),
            '2' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_2']),
            '2"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_2']),
            '2 1/2' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_2_dot_5']),
            '3' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_3']),
            '3"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_3']),
            '4' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_4']),
            '4"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_4']),
            '6' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_6']),
            '6"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_6']),
            '8' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_8']),
            '8"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_8']),
            '10' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_10']),
            '10"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_10']),
            '12' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_12'])
        ];

        $this->dynamicFields['fire_pump_make'] = [
            'Aurora' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_aurora']),
            'Fairbanks' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_fairbanks_morse']),
            'Patterson' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_patterson']),
            'peerless' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_peerless']),
            'Vertical' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_vertical']),
            'Marathon' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_marathon']),
            '1500gpm' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_1500gpm']),
        ];

        $this->dynamicFields['anti_freeze_system_size'] = [
            '1' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_1']),
            '1 1/2' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_1_dot_5']),
            '1 1/4' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_1_dot_25']),
            '1.5"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_1_dot_5']),
            '10' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_10']),
            '10 LB' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_10']),
            '10"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_10']),
            '2' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2']),
            '2 1/2' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2_dot_5']),
            '2 Lb' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2']),
            '2"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2']),
            '2.5"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2_dot_5']),
            '3' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_3']),
            '3"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_3']),
            '4' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_4']),
            '4 LB' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_4']),
            '4"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_4']),
            '6' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_6']),
            '6"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_6']),
            '6 LB' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_6']),
            '8' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_8']),
            '8"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_8']),
        ];


        $this->dynamicFields['dry_valve_size'] = [
            '1' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_1']),
            '1 1/4' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_125']),
            '2' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_2']),
            '2 1/2' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_25']),
            '2.5"' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_25']),
            '3' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_3']),
            '3"' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_3']),
            '4' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_4']),
            '4"' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_4']),
            '6' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_6']),
            '6"' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_6']),
            '8' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_8']),
        ];

        $this->dynamicFields['fire_extinguisher_size'] = [
            '2 Lb' => $this->choicesRepository->findOneBy(['alias' => 'fire_extinguisher_fire_extinguisher_size_2_lb']),
            '4 LB' => $this->choicesRepository->findOneBy(['alias' => 'fire_extinguisher_fire_extinguisher_size_4_lb']),
            '6 LB' => $this->choicesRepository->findOneBy(['alias' => 'fire_extinguisher_fire_extinguisher_size_6_lb']),
            '10 LB' => $this->choicesRepository->findOneBy(['alias' => 'fire_extinguisher_fire_extinguisher_size_10_lb']),
        ];
    }

    /**
     * @return int
     */
    public function import()
    {
        $oldDevices = $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::IMPORT_TABLE);
        $countImportedDevices = 0;
        $output = new Output\ConsoleOutput();
        $progress = new ProgressBar($output, count($oldDevices));
        $progress->start();

        foreach ($oldDevices as $oldDevice) {
            try {
                /** @var Device $device */
                $device = $this->deviceRepository->findOneBy(['oldDeviceId' => $oldDevice['FirSysID']]);
                if (!$device) {
                    $this->importErrorMessageManager->setErrorMessage('[additional import fire devices error][OldDeviceId:' . $oldDevice['FirSysID'] . ' ]: dont find device with FirSysID: ' . $oldDevice['FirSysID']);
                    continue;
                }

                /** @var DynamicFieldValue $field */
                foreach ($device->getFields() as $field) {
                    $wasSet = false;
                    $namedAlias = $device->getNamed()->getAlias();
                    $fieldAlias = $field->getField()->getAlias();
                    $fieldValue = $field->getOptionValue();

                    /** Fire system Size*/
                    if ($namedAlias == 'fire_sprinkler_system'
                        and $fieldAlias == 'fire_system_size'
                        and $oldDevice['SysSize'] != 'NA'
                        and is_null($fieldValue)
                    ) {
                        $size = $this->getSize($oldDevice);
                        $field->setOptionValue($size);
                        $wasSet = true;
                    }

                    /** Fire Pump Make */
                    if ($namedAlias == 'fire_pump'
                        and $fieldAlias == 'fire_pump_make'
                        and $oldDevice['FirPumMake'] != 'NA'
                        and is_null($fieldValue)
                    ) {
                        $firePumpMake = $this->getFirePumpMake($oldDevice);
                        $field->setOptionValue($firePumpMake);
                        $wasSet = true;
                    }

                    /** AntiFreeze system Size */
                    if ($namedAlias == 'anti_freeze_system'
                        and $fieldAlias == 'anti_freeze_size'
                        and $oldDevice['SysSize'] != 'NA'
                        and is_null($fieldValue)
                    ) {
                        $systemSize = $this->getSystemSize($oldDevice);
                        $field->setOptionValue($systemSize);
                        $wasSet = true;
                    }

                    /** Dry Valve Size*/
                    if ($namedAlias == 'dry_valve_system'
                        and $fieldAlias == 'dry_valve_size'
                        and $oldDevice['SysSize'] != 'NA'
                        and is_null($fieldValue)
                    ) {
                        $systemSize = $this->getDryValveSize($oldDevice);
                        $field->setOptionValue($systemSize);
                        $wasSet = true;
                    }

                    /** Fire Extinguisher  Size*/
                    if ($namedAlias == 'fire_extinguisher'
                        and $fieldAlias == 'fire_extinguisher_size'
                        and $oldDevice['SysSize'] != 'NA'
                        and is_null($fieldValue)
                    ) {
                        $waterSource = $this->getExtinguisher($oldDevice);
                        $field->setOptionValue($waterSource);
                        $wasSet = true;
                    }

                    if ($wasSet) {
                        $this->om->persist($field);
                    }
                }

                $countImportedDevices++;

                $progress->advance();

                $this->needLoad($countImportedDevices);

            } catch (\Exception $exception) {
                $this->importErrorMessageManager->setErrorMessage('[additional import fire devices error][OldDeviceId:' . $oldDevice['FirSysID'] . ' ]: error: "' . $exception->getMessage() . '" AccountId: ' . $oldDevice['AccountID']);
                continue;
            }
        }

        unset($oldDevices);
        $progress->finish();
        $this->om->flush();

        return $countImportedDevices;
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getSize($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'SysSize', 'fire_system_size');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getSystemSize($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'SysSize', 'anti_freeze_system_size');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getDryValveSize($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'SysSize', 'dry_valve_size');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getFirePumpMake($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'FirPumMake', 'fire_pump_make');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getExtinguisher($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'SysSize', 'fire_extinguisher_size');
    }

    /**
     * @param $oldDevice
     * @param $oldFieldName
     * @param $newFieldName
     * @return null
     */
    private function getDynamicField($oldDevice, $oldFieldName, $newFieldName)
    {
        $oldValue = $oldDevice[$oldFieldName];
        if (isset($this->dynamicFields[$newFieldName][$oldValue])) {
            return $this->dynamicFields[$newFieldName][$oldValue];
        }

        return null;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i != 0) {
            $this->om->flush();
        }
    }

    /**
     * @param \DateTime $datetime1
     * @param \DateTime $datetime2
     * @return string
     */
    public function getTimedifference(\DateTime $datetime1, \DateTime $datetime2)
    {
        $resultInterval = "Process took:";

        $interval = $datetime1->diff($datetime2);

        if ($interval->h > 0) {
            $resultInterval .= " " . $interval->h . " hours";
        }
        if ($interval->i > 0) {
            $resultInterval .= " " . $interval->i . " minutes";
        }
        if ($interval->s > 0) {
            $resultInterval .= " " . $interval->s . " seconds";
        }

        return $resultInterval;
    }
}
