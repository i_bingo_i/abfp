<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Account;
use AppBundle\Entity\Contractor;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\DeviceStatus;
use AppBundle\Entity\DynamicField;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\Opportunity;
use AppBundle\Entity\OpportunityType;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\ContractorTypeRepository;
use AppBundle\Entity\Repository\DeviceStatusRepository;
use AppBundle\Entity\Repository\DynamicFieldDropboxChoicesRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\OpportunityRepository;
use AppBundle\Entity\Repository\OpportunityTypeRepository;
use AppBundle\Entity\Repository\ServiceNamedRepository;
use AppBundle\Entity\Service;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use AppBundle\Entity\ServiceNamed;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use AppBundle\Entity\EntityManager\OpportunityManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output;

class TableFireToDevices
{
    private const IMPORT_TABLE = 'abfp.dbo.Fire';
    private const NEED_FLUSH_ON = 1000;

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    private $opportunityCache = [];

    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var  OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var  AccountRepository */
    private $accountRepository;
    /** @var DeviceNamedRepository */
    private $deviceNamedRepository;
    /** @var ServiceNamedRepository */
    private $serviceNamedRepository;
    /** @var ServiceNamed */
    private $serviceNamedAnnualBackflowInspection;
    /** @var DepartmentChannelManager $departmentChannelManager */
    private $departmentChannelManager;
    /** @var ServiceManager */
    private $serviceManager;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var OpportunityManager */
    private $opportunityManager;
    /** @var OpportunityRepository */
    private $opportunityRepository;
    /** @var DeviceRepository */
    private $deviceRepository;
    /** @var DynamicFieldDropboxChoicesRepository */
    private $choicesRepository;
    /** @var DeviceStatusRepository */
    private $deviceStatusRepository;
    /** @var DeviceStatus */
    private $deviceStatusActive;
    /** @var DeviceStatus */
    private $deviceStatusInActive;
    /** @var DeviceNamed[] */
    private $rootDeviceNameds;
    /** @var DeviceNamed[] */
    private $subDeviceNameds;
    /** @var array */
    private $dynamicFields = [
        'size' => []
    ];
    /** @var array */
    private $serviceNames;
    /** @var array */
    private $serviceNamesByDeviceNames;
    /** @var CompanyRepository */
    private $contractorRepository;
    /** @var array */
    private $contractors;
    /** @var array */
    private $contractorsCache;
    /** @var OpportunityTypeRepository */
    private $opportunityTypeRepository;
    /** @var OpportunityType */
    private $opportunityTypeRetest;
    /** @var DeviceManager */
    private $deviceManager;
    /** @var ContractorTypeRepository */
    private $contractorTypeRepository;
    /** @var ContractorServiceRepository */
    private $contractorServiceRepository;

    /**
     * MasLevelToAccount constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
        $this->departmentChannelManager = $this->container->get('app.department_channel.manager');
        $this->serviceManager = $this->container->get('app.service.manager');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
        $this->opportunityManager = $this->container->get('app.opportunity.manager');

        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->opportunityTypeRepository = $this->om->getRepository('AppBundle:OpportunityType');
        $this->deviceStatusRepository = $this->om->getRepository('AppBundle:DeviceStatus');
        $this->accountRepository = $this->om->getRepository('AppBundle:Account');
        $this->deviceNamedRepository = $this->om->getRepository('AppBundle:DeviceNamed');
        $this->serviceNamedRepository = $this->om->getRepository('AppBundle:ServiceNamed');
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->opportunityRepository = $this->om->getRepository('AppBundle:Opportunity');
        $this->choicesRepository = $this->om->getRepository('AppBundle:DynamicFieldDropboxChoices');
        $this->deviceRepository = $this->om->getRepository('AppBundle:Device');
        $this->contractorRepository = $this->om->getRepository('AppBundle:Contractor');
        $this->deviceManager = $this->container->get("app.device.manager");
        $this->contractorTypeRepository = $this->om->getRepository('AppBundle:ContractorType');
        $this->contractorServiceRepository = $this->om->getRepository('AppBundle:ContractorService');

        $this->deviceStatusActive = $this->deviceStatusRepository->findOneBy(['alias' => 'active']);
        $this->deviceStatusInActive = $this->deviceStatusRepository->findOneBy(['alias' => 'inactive']);
        $this->opportunityTypeRetest = $this->opportunityTypeRepository->findOneBy(['alisa' => 'retest']);

        $this->serviceNames = [
            'Annual Fire Sprinkler System Inspection' => $this->serviceNamedRepository->findOneBy(['name' => 'Annual Fire Sprinkler System Inspection']),
            '5 Year Obstruction Investigation' => $this->serviceNamedRepository->findOneBy(['name' => '5 Year Obstruction Investigation with Check Valve and Pipe Inspection']),
            'Annual Fire Pump Inspection' => $this->serviceNamedRepository->findOneBy(['name' => 'Annual Fire Pump Inspection']),
            'Annual Antifreeze System Test & Inspection' => $this->serviceNamedRepository->findOneBy(['name' => 'Annual Antifreeze System Test & Inspection']),
            'Annual Dry Valve Trip Test' => $this->serviceNamedRepository->findOneBy(['name' => 'Annual Dry Valve System Partial Trip Test']),
            'Annual Fire Extinguisher Inspection' => $this->serviceNamedRepository->findOneBy(['name' => 'Annual Fire Extinguisher(s) Inspection']),
            'Bi-Annual Range Hood Inspection' => $this->serviceNamedRepository->findOneBy(['name' => 'Bi-Annual Pre-Engineered System Inspection']),
        ];

        $this->serviceNamesByDeviceNames = [
            'Wet' => 'Annual Fire Sprinkler System Inspection',
            'Wet System' => 'Annual Fire Sprinkler System Inspection',
            'Fire Pump' => 'Annual Fire Pump Inspection',
            'Anti-Freeze System' => 'Annual Antifreeze System Test & Inspection',
            'Antifreeze' => 'Annual Antifreeze System Test & Inspection',
            'Dry' => 'Annual Dry Valve Trip Test',
            'Dry System' => 'Annual Dry Valve Trip Test',
            'Extinguisher' => 'Annual Fire Extinguisher Inspection',
            'Fire Extinguisher' => 'Annual Fire Extinguisher Inspection',
            'Fire Exts (6)' => 'Annual Fire Extinguisher Inspection',
            'Fire Exts(each)' => 'Annual Fire Extinguisher Inspection',
            'Ansol' => 'Bi-Annual Range Hood Inspection',
            'Ansul' => 'Bi-Annual Range Hood Inspection',
            'Hood Range' => 'Bi-Annual Range Hood Inspection',
            'Range Hood' => 'Bi-Annual Range Hood Inspection',
        ];

        $this->rootDeviceNameds = [
            'Wet' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_sprinkler_system']),
            'Wet System' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_sprinkler_system']),
            'Fire Pump' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_pump']),
            'Extinguisher' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_extinguishers']),
            'Fire Extinguisher' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_extinguishers']),
            'Fire Exts (6)' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_extinguishers']),
            'Fire Exts(each)' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_extinguishers']),
            'Ansol' => $this->deviceNamedRepository->findOneBy(['alias' => 'pre_engineered_fire_suppression_system']),
            'Ansul' => $this->deviceNamedRepository->findOneBy(['alias' => 'pre_engineered_fire_suppression_system']),
            'Hood Range' => $this->deviceNamedRepository->findOneBy(['alias' => 'pre_engineered_fire_suppression_system']),
            'Range Hood' => $this->deviceNamedRepository->findOneBy(['alias' => 'pre_engineered_fire_suppression_system']),
        ];

        $this->subDeviceNameds = [
            'Anti-Freeze System' => $this->deviceNamedRepository->findOneBy(['alias' => 'anti_freeze_system']),
            'Antifreeze' => $this->deviceNamedRepository->findOneBy(['alias' => 'anti_freeze_system']),
            'Dry' => $this->deviceNamedRepository->findOneBy(['alias' => 'dry_valve_system']),
            'Dry System' => $this->deviceNamedRepository->findOneBy(['alias' => 'dry_valve_system']),
            'Riser' => $this->deviceNamedRepository->findOneBy(['alias' => 'riser']),
            'Section' => $this->deviceNamedRepository->findOneBy(['alias' => 'section']),
            'extinguisher' => $this->deviceNamedRepository->findOneBy(['alias' => 'fire_extinguisher']),
        ];

        $this->dynamicFields['fire_system_size'] = [
            '1' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_1']),
            '1 1/4' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_1_dot_25']),
            '1 1/2' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_1_dot_5']),
            '1.5"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_1_dot_5']),
            '2' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_2']),
            '2"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_2']),
            '2 1/2' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_2_dot_5']),
            '3' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_3']),
            '3"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_3']),
            '4' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_4']),
            '4"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_4']),
            '6' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_6']),
            '6"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_6']),
            '8' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_8']),
            '8"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_8']),
            '10' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_10']),
            '10"' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_10']),
            '12' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_size_12'])
        ];

        $this->dynamicFields['fire_system_water_supply_source'] = [
            'City' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_water_supply_source_city']),
            'Well' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_water_supply_source_well']),
            'Pond Tower' => $this->choicesRepository->findOneBy(['alias' => 'fire_system_water_supply_source_pond']),
        ];

        $this->dynamicFields['fire_pump_make'] = [
            'Aurora' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_aurora']),
            'Fairbanks' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_fairbanks_morse']),
            'Patterson' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_patterson']),
            'peerless' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_peerless']),
            'Vertical' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_vertical']),
            'Marathon' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_marathon']),
            '1500gpm' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_make_1500gpm']),
        ];

        $this->dynamicFields['rated_gpm'] = [
            '25' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_25']),
            '50' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_50']),
            '75' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_75']),
            '250' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_250']),
            '400' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_400']),
            '500' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_500']),
            '750' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_750']),
            '1000' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_1000']),
            '1500' => $this->choicesRepository->findOneBy(['alias' => 'fire_pump_rated_gpm_1500']),
            ];
            
        $this->dynamicFields['anti_freeze_system_size'] = [
            '1' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_1']),
            '1 1/2' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_1_dot_5']),
            '1 1/4' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_1_dot_25']),
            '1.5"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_1_dot_5']),
            '10' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_10']),
            '10 LB' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_10']),
            '10"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_10']),
            '2' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2']),
            '2 1/2' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2_dot_5']),
            '2 Lb' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2']),
            '2"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2']),
            '2.5"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_2_dot_5']),
            '3' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_3']),
            '3"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_3']),
            '4' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_4']),
            '4 LB' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_4']),
            '4"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_4']),
            '6' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_6']),
            '6"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_6']),
            '6 LB' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_6']),
            '8' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_8']),
            '8"' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_size_8']),
        ];


        $this->dynamicFields['dry_valve_size'] = [
            '1' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_1']),
            '1 1/4' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_125']),
            '2' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_2']),
            '2 1/2' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_25']),
            '2.5"' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_25']),
            '3' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_3']),
            '3"' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_3']),
            '4' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_4']),
            '4"' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_4']),
            '6' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_6']),
            '6"' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_6']),
            '8' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_size_8']),
        ];

        $this->dynamicFields['dry_valve_water_supply_source'] = [
            'City' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_water_supply_source_city']),
            'Tank' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_water_supply_source_tank']),
            'Pump' => $this->choicesRepository->findOneBy(['alias' => 'dry_valve_water_supply_source_pump']),
        ];

        $this->dynamicFields['anti_freeze_system_water_supply_source'] = [
            'City' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_water_supply_source_city']),
            'Well' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_water_supply_source_well']),
            'Pond Tower' => $this->choicesRepository->findOneBy(['alias' => 'anti_freeze_system_water_supply_source_pond']),
        ];

        $this->contractors = [
            '  NO Contractor' => '',
            'NO Contractor' => '',
            'A & A Sprinkler Company' => $this->contractorRepository->findOneBy(['name' => 'A & A Sprinkler Company']),
            'Absolute Fire Protection Inc.' => $this->contractorRepository->findOneBy(['name' => 'Absolute Fire Protection Inc.']),
            'Advanced Fire Protection & Safety Inc.' => $this->contractorRepository->findOneBy(['name' => 'Advanced Fire Protection & Safety Inc.']),
            'Affordable Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'Affordable Fire Protection, Inc.']),
            'Affordable Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Affordable Fire Protection, Inc.']),
            'Ahern Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'Ahern Fire Protection']),
            'Alarm Detection Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Alarm Detection Systems, Inc.']),
            'American Backflow & Fire Prevention, Inc.' => $this->contractorRepository->findOneBy(['name' => 'American Backflow & Fire Prevention']),
            'American Backflow &amp; Fire Prevention, Inc.' => $this->contractorRepository->findOneBy(['name' => 'American Backflow & Fire Prevention']),
            'American Backflow and Fire Prevention' => $this->contractorRepository->findOneBy(['name' => 'American Backflow & Fire Prevention']),
            'American Backflow Prevention Inc.' => $this->contractorRepository->findOneBy(['name' => 'American Backflow & Fire Prevention']),
            'C.L. Doucette, Inc.' => $this->contractorRepository->findOneBy(['name' => 'C.L. Doucette, Inc.']),
            'Cardinal Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Cardinal Fire Protection, Inc.']),
            'Central States Automatic Sprinklers, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Central States Automatic Sprinklers, Inc.']),
            'Century' => $this->contractorRepository->findOneBy(['name' => 'Century Automatic Sprinkler Company, Inc.']),
            'Century Automatic Sprinkler Company, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Century Automatic Sprinkler Company, Inc.']),
            'Chicago Metropolitan Fire Prevention Company' => $this->contractorRepository->findOneBy(['name' => 'Chicago Metropolitan Fire Prevention Company']),
            'Cybor Fire Protection Company' => $this->contractorRepository->findOneBy(['name' => 'Cybor Fire Protection Company']),
            'F. E. Moran, Inc. Fire Protection of N. Illinois' => $this->contractorRepository->findOneBy(['name' => 'F. E. Moran, Inc. Fire Protection of N. Illinois']),
            'Fe Moran' => $this->contractorRepository->findOneBy(['name' => 'Fe Moran']),
            'Fire Control Incorporated' => $this->contractorRepository->findOneBy(['name' => 'Fire Control Incorporated']),
            'Fox Valley Fire & Safety Company, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Fox Valley Fire & Safety Company, Inc.']),
            'Fox Valley Fire and Safety' => $this->contractorRepository->findOneBy(['name' => 'Fox Valley Fire and Safety']),
            'Fredriksen' => $this->contractorRepository->findOneBy(['name' => 'Fredriksen Fire Equipment Company']),
            'Fredriksen Fire Equipment Company' => $this->contractorRepository->findOneBy(['name' => 'Fredriksen Fire Equipment Company']),
            'Great Lakes Fire and Safety' => $this->contractorRepository->findOneBy(['name' => 'Great Lakes Fire and Safety']),
            'K & S Automatic Sprinklers, Inc.' => $this->contractorRepository->findOneBy(['name' => 'K & S Automatic Sprinklers, Inc.']),
            'Liberty Fire Protection Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Liberty Fire Protection Systems, Inc.']),
            'Metropolitan Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Metropolitan Fire Protection, Inc.']),
            'Monarch Fire Protection, Incorporated' => $this->contractorRepository->findOneBy(['name' => 'Monarch Fire Protection, Incorporated']),
            'MVP Fire Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'MVP Fire Systems, Inc.']),
            'Nelson Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'Nelson Fire Protection']),
            'Nova Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Nova Fire Protection, Inc.']),
            'Orion Fire Protection, Inc' => $this->contractorRepository->findOneBy(['name' => 'Orion Fire Protection, Inc']),
            'Other' => $this->contractorRepository->findOneBy(['name' => 'Other']),
            'Phoenix Fire Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Phoenix Fire Systems, Inc.']),
            'Profasts Incorporated' => $this->contractorRepository->findOneBy(['name' => 'Profasts Incorporated']),
            'RAM Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'RAM Fire Protection']),
            'Reliable Fire Equipment' => $this->contractorRepository->findOneBy(['name' => 'Reliable Fire Equipment']),
            'S. J. Carlson Fire Protection' => $this->contractorRepository->findOneBy(['name' => 'S. J. Carlson Fire Protection']),
            'Shamrock Fire Protection LLC' => $this->contractorRepository->findOneBy(['name' => 'Shamrock Fire Protection LLC']),
            'SimplexGrinnell LP' => $this->contractorRepository->findOneBy(['name' => 'SimplexGrinnell LP']),
            'Skyhawk Sprinkler Systems, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Skyhawk Sprinkler Systems, Inc.']),
            'Spillane Fire Protection LLC' => $this->contractorRepository->findOneBy(['name' => 'Spillane Fire Protection LLC']),
            'Total Fire' => $this->contractorRepository->findOneBy(['name' => 'Total Fire & Safety, Inc.']),
            'Total Fire & Safety, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Total Fire & Safety, Inc.']),
            'Tri State Fire Control Inc' => $this->contractorRepository->findOneBy(['name' => 'Tri State Fire Control Inc']),
            'Ultimate Fire Protection Inc.' => $this->contractorRepository->findOneBy(['name' => 'Ultimate Fire Protection Inc.']),
            'United States Alliance Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'United States Alliance Fire Protection, Inc.']),
            'Valley Fire Protection Services, LLC' => $this->contractorRepository->findOneBy(['name' => 'Valley Fire Protection Services, LLC']),
            'Xtreme Fire' => $this->contractorRepository->findOneBy(['name' => 'Xtreme Fire Protection, Inc.']),
            'Xtreme Fire Protection, Inc.' => $this->contractorRepository->findOneBy(['name' => 'Xtreme Fire Protection, Inc.']),
        ];

        $this->dynamicFields['fire_extinguisher_size'] = [
            '2 Lb' => $this->choicesRepository->findOneBy(['alias' => 'fire_extinguisher_fire_extinguisher_size_2_lb']),
            '4 LB' => $this->choicesRepository->findOneBy(['alias' => 'fire_extinguisher_fire_extinguisher_size_4_lb']),
            '6 LB' => $this->choicesRepository->findOneBy(['alias' => 'fire_extinguisher_fire_extinguisher_size_6_lb']),
            '10 LB' => $this->choicesRepository->findOneBy(['alias' => 'fire_extinguisher_fire_extinguisher_size_10_lb']),
        ];
    }

    // TODO: Потрібно робити вибірку з урахуванням дивіжина
    /**
     * @return array
     */
    public function getImportedDevicesOldIds()
    {
        $importedIds =[];
        $importedDevicesArray = $this->deviceRepository->getImportedDevices();
        foreach ($importedDevicesArray as $importedDevice) {
            $importedIds[] = $importedDevice["oldDeviceId"];
        }

        return $importedIds;
    }


    /**
     * @return array
     */
    public function getOldDevicesByQuery()
    {
        return $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::IMPORT_TABLE);
    }

    /**
     * @param $oldDevices
     * @param bool $sub
     * @return int
     */
    public function import($oldDevices, $sub = false)
    {
        $countImportedDevices = 0;
        $importedIds = $this->getImportedDevicesOldIds();
        $output = new Output\ConsoleOutput();
        $progress = new ProgressBar($output, count($oldDevices));
        $progress->start();
        foreach ($oldDevices as $oldDevice) {
            try {
                if (!in_array($oldDevice['FirSysID'], $importedIds)) {
                    $deviceNamed = $this->identifyNamed($oldDevice['SysType'], $sub);
                    if ($oldDevice['DelFir'] == 'del' or !$deviceNamed) {
                        continue;
                    }

                    /** @var Account $account */
                    $account = $this->accountRepository->findOneBy(['oldAccountId' => $oldDevice['AccountID']]);
                    /** Create services */
                    if (!$account) {
                        $this->importErrorMessageManager->setErrorMessage('[import fire devices error][OldDeviceId:' . $oldDevice['FirSysID'] . ' ]: dont find account with AccountId: ' . $oldDevice['AccountID']);
                        continue;
                    }

                    /** @var Device $newDevice */
                    $newDevice = $this->createDeviceByOldDevice($oldDevice, $account, $sub);
                    /** @var array $newServices */
                    if ($oldDevice['SysType'] == 'Wet' || $oldDevice['SysType'] == 'Wet System') {
                        $newServices[] = $this->createServiceByOldDevice(
                            $oldDevice,
                            $newDevice,
                            $account,
                            true
                        );
                    }

                    if ($newDevice->getNamed()->getAlias() == "fire_extinguishers") {
                        $this->checkAndCloneFireExtinguisherIfNeed(
                            $newDevice,
                            $oldDevice
                        );
                    }

                    $newFireService = $this->createServiceByOldDevice(
                        $oldDevice,
                        $newDevice,
                        $account
                    );

                    $newServices[] = $newFireService;


                    $countImportedDevices++;
                    $progress->advance();

                    $this->needLoad($countImportedDevices);
//                    if ($countImportedDevices > 5000) {
//                        break;
//                    }
                }
            } catch (\Exception $exception) {
                $this->importErrorMessageManager->setErrorMessage('[import fire devices error][OldDeviceId:' . $oldDevice['FirSysID'] . ' ]: department error AccountId: ' . $oldDevice['AccountID']);
                continue;
            }
        }

        unset($oldDevices);
        $this->om->flush();
        $progress->finish();
        $output->writeln('');

        return $countImportedDevices;
    }

    /**
     * @param $oldDevice
     * @param Device $newDevice
     * @param Account $account
     * @param bool $lotOfServices
     * @return Service
     */
    private function createServiceByOldDevice($oldDevice, Device $newDevice, Account $account, $lotOfServices=false)
    {
        /** @var Service $newService */
        $newService = new Service();
        $newService->setDevice($newDevice);
        $newService->setAccount($account);
        if (($oldDevice['SysType'] == 'Wet' or $oldDevice['SysType'] == 'Wet System') and $lotOfServices) {
            $lastTested = $this->getDateTime($oldDevice['obstructionTestLastInsp']);
            $inspectionDue = $this->getDateTime($oldDevice['obstructionTestDue']);
            $newService->setNamed($this->serviceNames['5 Year Obstruction Investigation']);
            $newService->setFixedPrice("600");
        } else {
            $newService->setFixedPrice($this->wetSystemCheck($oldDevice));
            $lastTested = $this->getDateTime($oldDevice['SysLaTeDate']);
            $inspectionDue = $this->getDateTime($oldDevice['SysReTeDate'] ?? '01/01/1900');
            $newService->setNamed(
                $this->serviceNames[$this->serviceNamesByDeviceNames[$oldDevice['SysType']]]
            );

        }
        $inspectionDue = (!empty($inspectionDue) and $inspectionDue) ? $inspectionDue : \DateTime::createFromFormat('Y-m-d', '1900-01-01');
        $lastTested = $lastTested ? $lastTested : null;

        $newService->setLastTested($lastTested);
        $newService->setInspectionDue($inspectionDue);

        if ($contractor = $this->getContractor($oldDevice)) {
            $newService->setCompanyLastTested($contractor);
        }

        if (!empty($newService->getAccount()->getMunicipality())) {
            /** @var DepartmentChannel $departmentChannel */
            $departmentChannel = $this->departmentChannelManager->getChannelByDivision($newService);
            $newService->setDepartmentChannel($departmentChannel);
        } else {
            $this->importErrorMessageManager->setErrorMessage('[import devices error][OldDeviceId:' . $oldDevice['DevID'] . ' ]: department error AccountId: ' . $oldDevice['AccountID']);
        }

        $contractorType = $this->contractorTypeRepository->findOneBy(['alias' => 'my_company']);
        $contractorService = $this->contractorServiceRepository->getByServiceNamedId($newService->getNamed(), $contractorType);
        if (!empty($contractorService)) {
            $newService->setContractorService($contractorService[0]);
        }

        $this->om->persist($newService);

        $newService = $this->addOpportunityForService($oldDevice, $newService);

        return $newService;
    }



    private function addOpportunityForService($oldDevice, Service $newService)
    {
        $oldAccountId = $oldDevice['AccountID'];
        $opportunity = $this->getOpportunityFromCache($oldAccountId, $newService->getInspectionDue()->format('Y-m-d'));
        if (empty($opportunity)) {
            $opportunity = $this->opportunityManager->creating($newService);
        }

        if ($this->opportunityManager->isPastDue($opportunity)) {
            $this->opportunityManager->setStatus($opportunity, $this->opportunityManager::STATUS_PAST_DUE);
        }

        $opportunity->addService($newService);
        $opportunity->setType($this->opportunityTypeRetest);
        $opportunity = $this->opportunityManager->updateSomeFields($opportunity);
        $this->om->persist($opportunity);

        $this->opportunityCache[$oldAccountId][$newService->getInspectionDue()->format('Y-m-d')] = $opportunity;

        $newService->setOpportunity($opportunity);
        $newService = $this->serviceManager->setStatus($newService, $this->serviceManager::STATUS_RETEST_OPPORTUNITY);

        $this->om->persist($newService);

        if($newService->getInspectionDue()) {
            $this->refreshOldestOpportunityDate($opportunity->getAccount(), $newService->getInspectionDue());
        }

        return $newService;
    }

    /**
     * @param Account $account
     * @param $newOldestDate
     */
    private function refreshOldestOpportunityDate(Account $account, $newOldestDate)
    {
        $currentOldestDate = $account->getOldestOpportunityDate();
        if (!$currentOldestDate or $currentOldestDate > $newOldestDate) {
            $account->setOldestOpportunityFireDate($newOldestDate);
            $account->setOldestOpportunityDate($newOldestDate);
        }

        $this->om->persist($account);

        if ($account->getParent()) {
            $this->refreshOldestOpportunityDate($account->getParent(), $newOldestDate);
        }
    }

    /**
     * @param $oldDevice
     * @param Account $account
     * @param bool $sub
     * @return Device
     */
    private function createDeviceByOldDevice($oldDevice, Account $account, $sub = false)
    {
        $device = new Device();

        $device->setOldDeviceId($oldDevice['FirSysID']);
        $device->setStatus($this->deviceStatusActive);
        $device->setAccount($account);

        /** @var DeviceNamed $deviceNamed */
        $deviceNamed = $this->identifyNamed($oldDevice['SysType'], $sub);
        $device->setNamed($deviceNamed);

        if ($sub) {
            $parentFireSystem = $this->getParentFireSystem($account);
            $device->setParent($parentFireSystem);
        }

        $comments = '';
        if ($oldDevice['SysComments'] and $oldDevice['SysDesc']) {
            $comments = $oldDevice['SysComments'] . ' | ' . $oldDevice['SysDesc'];
        } else {
            $comments .= $oldDevice['SysComments'];
            $comments .= $oldDevice['SysDesc'];
        }

        $device->setComments($comments);
        $device->setSourceEntity('Fire Devices');

        /** @var DynamicField[] $fields */
        $fields = $deviceNamed->getFields();
        /** @var DynamicField $field */
        foreach ($fields as $field) {
            $newValue = new DynamicFieldValue();
            $namedAlias = $deviceNamed->getAlias();

            if ($namedAlias == 'fire_sprinkler_system' and  $field->getAlias() == 'fire_system_size' and $oldDevice['SysSize'] != 'NA') {
                $size = $this->getSize($oldDevice);
                $newValue->setOptionValue($size);
            }

            if ($namedAlias == 'fire_sprinkler_system' and  $field->getAlias() == 'water_supply_source'
                and $oldDevice['WatSource'] != 'NA'
            ) {
                $waterSupplySource = $this->getWaterSupplySource($oldDevice);
                $newValue->setOptionValue($waterSupplySource);
            }

            if ($namedAlias == 'fire_pump' and  $field->getAlias() == 'fire_pump_make' and $oldDevice['FirPumMake'] != 'NA') {
                $firePumpMake = $this->getFirePumpMake($oldDevice);
                $newValue->setOptionValue($firePumpMake);
            }

            if ($namedAlias == 'fire_pump' and  $field->getAlias() == 'model' and $oldDevice['FirPumModel'] != 'NA') {
                $newValue->setValue($oldDevice['FirPumModel']);
            }

            if ($namedAlias == 'fire_pump' and  $field->getAlias() == 'serial_number'
                and $oldDevice['FirPumSiNum'] != 'NA'
            ) {
                $newValue->setValue($oldDevice['FirPumSiNum']);
            }

            if ($namedAlias == 'fire_pump' and  $field->getAlias() == 'rated_gpm' and $oldDevice['PumRat'] != 'NA') {
                $firePumpRat = $this->getFirePumpRat($oldDevice);
                $newValue->setOptionValue($firePumpRat);
            }

            if ($namedAlias == 'anti_freeze_system' and  $field->getAlias() == 'anti_freeze_size' and $oldDevice['SysSize'] != 'NA'
            ) {
                $systemSize = $this->getSystemSize($oldDevice);
                $newValue->setOptionValue($systemSize);
            }

            if ($namedAlias == 'anti_freeze_system' and  $field->getAlias() == 'water_supply_source'
                and $oldDevice['WatSource'] != 'NA'
            ) {
                $waterSource = $this->getWaterSource($oldDevice);
                $newValue->setOptionValue($waterSource);
            }

            if ($namedAlias == 'fire_sprinkler_system' and  $field->getAlias() == 'number_of_risers'
                and $oldDevice['WatSource'] != 'NA') {
                $newValue->setValue($oldDevice['RisNum']);
                $risers = (int)$oldDevice['RisNum'];
                for ($i=1; $i<=$risers; $i++) {
                    $this->autoCreateSubDeviceForDevice(
                        $device,
                        $this->subDeviceNameds["Riser"],
                        ["location" => $oldDevice["RisLoc"]]
                    );
                }

            }

            if ($namedAlias == 'fire_sprinkler_system' and  $field->getAlias() == 'number_of_sections'
                and $oldDevice['WatSource'] != 'NA') {
                $newValue->setValue($oldDevice['BuilSecNum']);
                $sections = (int)$oldDevice['BuilSecNum'];
                for ($i=1; $i<=$sections; $i++) {
                    $this->autoCreateSubDeviceForDevice(
                        $device,
                        $this->subDeviceNameds["Section"]
                    );
                }
            }

            /** Dry Valve */
            if ($namedAlias == 'dry_valve_system' and  $field->getAlias() == 'dry_valve_size' and $oldDevice['SysSize'] != 'NA'
            ) {
                $systemSize = $this->getDryValveSize($oldDevice);
                $newValue->setOptionValue($systemSize);
            }

            if ($namedAlias == 'dry_valve_system' and  $field->getAlias() == 'water_supply_source' and $oldDevice['WatSource'] != 'NA'
            ) {
                $waterSupplySource = $this->getDryValveWaterSource($oldDevice);
                $newValue->setOptionValue($waterSupplySource);
            }

            if ($namedAlias == 'fire_extinguishers' and  $field->getAlias() == 'number_of_extinguishers'
                and $oldDevice['WatSource'] != 'NA'
            ) {
                continue;
            }

            $newValue->setDevice($device);
            $newValue->setDateCreate(new \DateTime());
            $newValue->setField($field);
            $device->addField($newValue);
            $this->om->persist($newValue);

        }
        $device->setTitle($this->deviceManager->getTitle($device));
        $this->om->persist($device);

        return $device;
    }

    /**
     * @param Account $account
     * @return Device
     */
    private function getParentFireSystem(Account $account)
    {
        /** @var DeviceNamed $fireSystemNamed */
        $fireSystemNamed = $this->rootDeviceNameds['Wet'];
        /** @var Device $fireSystem */
        $fireSystem = $this->deviceRepository->findOneBy([
            'named' => $fireSystemNamed,
            'account' => $account
        ]);

        if (!$fireSystem) {
            $fireSystem = new Device();
            $fireSystem->setStatus($this->deviceStatusActive);
            $fireSystem->setAccount($account);
            $fireSystem->setNamed($fireSystemNamed);
            $this->om->persist($fireSystem);


            /** @var DynamicField[] $fields */
            $fields = $fireSystemNamed->getFields();
            /** @var DynamicField $field */
            foreach ($fields as $field) {
                $newValue = new DynamicFieldValue();
                $newValue->setDateCreate(new \DateTime());
                $newValue->setDevice($fireSystem);
                $newValue->setField($field);
                $fireSystem->addField($newValue);
                $this->om->persist($newValue);
            }
        }

        return $fireSystem;
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getSize($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'SysSize', 'fire_system_size');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getWaterSupplySource($oldDevice)
    {
        return $this->getDynamicField(
            $oldDevice, 'WatSource', 'fire_system_water_supply_source'
        );
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getSystemSize($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'SysSize', 'anti_freeze_system_size');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getDryValveSize($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'SysSize', 'dry_valve_size');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getDryValveWaterSource($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'WatSource', 'dry_valve_water_supply_source');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getWaterSource($oldDevice)
    {
        return $this->getDynamicField(
            $oldDevice, 'WatSource', 'anti_freeze_system_water_supply_source'
        );
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getFirePumpMake($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'FirPumMake', 'fire_pump_make');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getFirePumpRat($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'PumRat', 'rated_gpm');
    }

    /**
     * @param $oldDevice
     * @return null
     */
    private function getExtinguisher($oldDevice)
    {
        return $this->getDynamicField($oldDevice, 'SysSize', 'fire_extinguisher_size');
    }

    /**
     * @param $oldDevice
     * @param $oldFieldName
     * @param $newFieldName
     * @return null
     */
    private function getDynamicField($oldDevice, $oldFieldName, $newFieldName)
    {
        $oldValue = $oldDevice[$oldFieldName];
        if (isset($this->dynamicFields[$newFieldName][$oldValue])) {
            return $this->dynamicFields[$newFieldName][$oldValue];
        }

        return null;
    }

    /**
     * @param Device $sourceDevice
     * @param $oldDevice
     * @return array
     */
    private function checkAndCloneFireExtinguisherIfNeed(Device $sourceDevice, $oldDevice)
    {
        $numbers = [];
        $clonesEx = [];
        $checkComment = $oldDevice['SysDesc'];
        if (strlen($checkComment) > 1) {
            preg_match_all('|\d+|', $checkComment, $numbers);
        }

        if (empty($numbers) || empty($numbers[0])) {
            $numbers[0][] = 1;
        }

        if (isset($numbers[0]) and count($numbers[0]) > 0) {
            $fireExCount = 0;
            foreach ($numbers[0] as $number) {
                $fireExCount += (int)$number;
            }

            for ($i = 0; $i < $fireExCount; $i++) {
                $clonesEx[] = $this->cloneFireExtinguisher($sourceDevice, $oldDevice);
            }

            $this->setExtinguisherDenamicFields($sourceDevice, $oldDevice, $fireExCount);
        }

        return $clonesEx;
    }

    /**
     * @param Device $sourceDevice
     * @param array
     * @return Device
     */
    private function cloneFireExtinguisher(Device $sourceDevice, $oldDevice = [])
    {
        $newDevice = new Device();
        $serviceName = $this->subDeviceNameds['extinguisher'];

        $newDevice->setNamed($serviceName);
        $newDevice->setComments($sourceDevice->getComments());
        $newDevice->setStatus($sourceDevice->getStatus());
        $newDevice->setTitle($sourceDevice->getTitle());
        $newDevice->setAccount($sourceDevice->getAccount());
        $newDevice->setPhoto($sourceDevice->getPhoto());
        $newDevice->setParent($sourceDevice);
        $newDevice->setOldDeviceId($sourceDevice->getOldDeviceId());

        $this->setExtinguisherDenamicFields($newDevice, $oldDevice);

        $this->om->persist($newDevice);

        return $newDevice;
    }


    /**
     * @param Device $parentDevice
     * @param DeviceNamed $subDeviceNamed
     * @param $staticFields
     */
    private function autoCreateSubDeviceForDevice(Device $parentDevice, DeviceNamed $subDeviceNamed, $staticFields = [])
    {
        $newSubDevice = new Device();
        $newSubDevice->setParent($parentDevice);
        $newSubDevice->setNamed($subDeviceNamed);
        $newSubDevice->setAccount($parentDevice->getAccount());
        $newSubDevice->setTitle($this->deviceManager->getTitle($newSubDevice));
        $newSubDevice->setStatus($this->deviceStatusActive);
        if (isset($staticFields['location']) and $staticFields['location']!="NA") {
            $newSubDevice->setLocation($staticFields['location']);
        }
        if (isset($staticFields['photo']) and $staticFields['photo']!="NA") {
            $newSubDevice->setPhoto($staticFields['photo']);
        }
        if (isset($staticFields['comments']) and $staticFields['comments']!="NA") {
            $newSubDevice->setComments($staticFields['comments']);
        }
        if (isset($staticFields['noteToTester']) and $staticFields['noteToTester']!="NA") {
            $newSubDevice->setNoteToTester($staticFields['noteToTester']);
        }


        $this->createDynamicFieldsForDevice($newSubDevice, $subDeviceNamed);

        $this->om->persist($newSubDevice);
    }

    /**
     * @param Device $newDevice
     * @param DeviceNamed $subDeviceNamed
     */
    private function createDynamicFieldsForDevice(Device &$newDevice, DeviceNamed $subDeviceNamed)
    {
        $dynamicFieldsArray = $subDeviceNamed->getFields();

        foreach ($dynamicFieldsArray as $dynamicField) {
            $newDynamicFieldValue = new DynamicFieldValue();
            $newDynamicFieldValue->setDevice($newDevice);
            $newDynamicFieldValue->setDateCreate(new \DateTime());
            $newDynamicFieldValue->setField($dynamicField);
            $newDevice->addField($newDynamicFieldValue);

            $this->om->persist($newDynamicFieldValue);
        }
    }

    /**
     * @param $sysType
     * @param bool $sub
     * @return DeviceNamed|bool|mixed
     */
    private function identifyNamed($sysType, $sub = false)
    {
        $named = false;
        if (!$sub and isset($this->rootDeviceNameds[$sysType])) {
            $named = $this->rootDeviceNameds[$sysType];
        }

        if ($sub and isset($this->subDeviceNameds[$sysType])) {
            $named = $this->subDeviceNameds[$sysType];
        }

        return $named;
    }

    /**
     * @param $oldDateTime
     * @return \DateTime|null
     */
    private function getDateTime($oldDateTime)
    {
        $dateTime = null;
        $oldDateTimeArray = explode(' ', $oldDateTime);
        if (!empty($oldDateTimeArray)) {
            $dateTime = \DateTime::createFromFormat('Y-m-d', $oldDateTimeArray[0]);
        }

        return $dateTime;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i != 0) {
            $this->om->flush();
        }
    }

    /**
     * @param $oldDevice
     * @return Contractor|mixed|null
     */
    private function getContractor($oldDevice)
    {
        $contractor = null;

        if ($oldDevice['ComLaInspCo'] != '  NO Contractor' and
            $oldDevice['ComLaInspCo'] != 'NO Contractor' and
            $oldDevice['ComLaInspCo'] != ''
        ) {
            $contractor = $this->contractors[$oldDevice['ComLaInspCo']] ?? null;

            if (!$contractor) {
                $contractor = $this->contractorsCache[$oldDevice['ComLaInspCo']] ?? null;
            }

//            if (!$contractor) {
//                $contractor = $this->createContractor($oldDevice['ComLaInspCo']) ?? null;
//            }
        }

        return $contractor;
    }


    /**
     * @param $oldDevice
     * @return Contractor
     */
    private function createContractor($oldDevice)
    {
        $newContractor = new Contractor();
        $newContractor->setName($oldDevice['ComLaInspCo']);
        $newContractor->setStateLicenseNumber('No Data');
        $newContractor->setStateLicenseExpirationDate(\DateTime::createFromFormat('Y-m-d', '1900-01-01'));
        $newContractor->setDateCreate(new \DateTime());
        $newContractor->setDateUpdate(new \DateTime());

        $this->om->persist($newContractor);

        $this->contractorsCache[$oldDevice['ComLaInspCo']] = $newContractor;

        return $newContractor;
    }

    /**
     * @param $oldAccountId
     * @param $lastTestedDate
     * @return Opportunity|bool
     */
    private function getOpportunityFromCache($oldAccountId, $lastTestedDate)
    {
        /** @var Opportunity $opportunity */
        $opportunity = false;
        if (isset($this->opportunityCache[$oldAccountId][$lastTestedDate])) {
            $opportunity = $this->opportunityCache[$oldAccountId][$lastTestedDate];
        }

        return $opportunity;
    }

    /**
     * @param $oldDevice
     * @return float|int
     */
    private function wetSystemCheck($oldDevice)
    {
        if ($oldDevice['SysType'] == 'Wet' || $oldDevice['SysType'] == 'Wet System') {
            if (!empty($oldDevice['SecPrice']) && !empty($oldDevice['BuilSecNum']) && !empty($oldDevice['SysPrice'])) {
                return ($oldDevice['SecPrice'] * $oldDevice['BuilSecNum']) + $oldDevice['SysPrice'];
            }
        }

        return $oldDevice['SysPrice'];
    }

    /**
     * @param Device $device
     * @param $oldDevice
     * @param null $quantityExtinguishers
     */
    private function setExtinguisherDenamicFields(Device $device, $oldDevice, $quantityExtinguishers = null)
    {
        /** @var DynamicField $field */
        foreach ($device->getNamed()->getFields() as $field) {
            $newField = new DynamicFieldValue();
            $namedAlias = $device->getNamed()->getAlias();

            if ($namedAlias == 'fire_extinguisher' and  $field->getAlias() == 'fire_extinguisher_size'
                and $oldDevice['SysSize'] != 'NA'
            ) {
                $waterSource = $this->getExtinguisher($oldDevice);
                $newField->setOptionValue($waterSource);
            }

            if ($namedAlias == 'fire_extinguishers' and  $field->getAlias() == 'number_of_extinguishers'
                and $quantityExtinguishers
            ) {
                $newField->setValue($quantityExtinguishers);
            }

            $newField->setDevice($device);
            $newField->setDateCreate(new \DateTime());
            $newField->setField($field);
            $this->om->persist($newField);

            $device->addField($newField);
            $device->setTitle($this->deviceManager->getTitle($device));
            $this->om->persist($device);
        }
    }


    /**
     * @param $oldDevice
     * @param $comments
     * @return string
     */
//    private function getCommentForDryValve($oldDevice, $comments)
//    {
//        if (strlen($comments)>1){
//            $comments .= "\n";
//        }
//        if ($oldDevice["SysSize"]) {
//            $comments .= "System Size: ".$oldDevice["SysSize"];
//        }
//        if (strlen($comments)>1){
//            $comments .= "\n";
//        }
//        if ($oldDevice["WatSource"]) {
//            $comments .= "Water Source: ".$oldDevice["WatSource"];
//        }
//
//        return $comments;
//    }
}
