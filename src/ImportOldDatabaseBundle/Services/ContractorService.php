<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Address;
use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ContractorTypeRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Doctrine\UserManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContractorService
{
    /** Table name in old database */
    private const TABLE_CONTRACTOR = 'Contractor';
    /** Address type */
    private const ADDRESS_TYPE_CONTRACTOR_ADDRESS = 'contractor address';
    /** Contractor types */
    private const CONTRACTOR_TYPE_PARTNER = 'partner';
    private const CONTRACTOR_TYPE_COMPETITOR = 'competitor';
    /** Success value for field in old database */
    private const USED_AS_SUB_VALUE_YES = 'Yes';
    /** Contractor users serial number */
    private const EMPTY_CONTRACTOR_USER = false;
    private const FIRST_CONTRACTOR_USER = 1;
    private const SECOND_CONTRACTOR_USER = 2;
    /** Static part for dynamic user email */
    private const STATIC_PART_EMAIL = '@dummyuser.com';
    /** One of these fields should not be empty */
    private const REQUIRED_FIELDS = [
        self::EMPTY_CONTRACTOR_USER => ['ContrContName'],
        self::FIRST_CONTRACTOR_USER => [
            'ContrConFName1', 'ContrConLName1', 'ContrConTitle1', 'ContrConPhone1', 'ContrConExt1', 'ContrConCell1',
            'ContrConFax1', 'ContrConEmail1'
        ],
        self::SECOND_CONTRACTOR_USER => [
            'ContrConFName2', 'ContrConLName2', 'ContrConTitle2', 'ContrConPhone2', 'ContrConExt2', 'ContrConCell2',
            'ContrConFax2', 'ContrConEmail2'
        ]
    ];

    /** @var OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var StateRepository */
    private $stateRepository;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var ContractorTypeRepository */
    private $contractorTypeRepository;
    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var UserManager */
    private $userRepository;
    /** @var  ContractorRepository */
    private $contractorRepository;

    private $users = [];

    /**
     * ContractorService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;

        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->stateRepository = $this->om->getRepository('AppBundle:State');
        $this->addressTypeRepository = $this->om->getRepository('AppBundle:AddressType');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
        $this->contractorTypeRepository = $this->om->getRepository('AppBundle:ContractorType');
        $this->contractorRepository = $this->om->getRepository('AppBundle:Contractor');
        $this->userRepository = $this->container->get('fos_user.user_manager');
    }

    /**
     * @return array
     */
    public function getImportedContractorsOldIds()
    {
        $importedIds =[];
        $importedContractorsArray = $this->contractorRepository->getImportedContractors();
        foreach ($importedContractorsArray as $importedContractor) {
            $importedIds[] = $importedContractor["RegContrTestID"];
        }

        return $importedIds;
    }

    /**
     * @return int
     */
    public function parseData()
    {
        $contractorData = $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::TABLE_CONTRACTOR);
        $result = $this->dataImport($contractorData);
        unset($contractorData);

        return $result;
    }


    /**
     * @param array $oldData
     * @return int
     */
    private function dataImport(array $oldData)
    {
        $i = 0;
        $importedIds = $this->getImportedContractorsOldIds();
        foreach ($oldData as $data) {
            if (!in_array($data['RegContrTestID'], $importedIds)) {
                if (trim($data['ContrCompName']) == 'American Backflow Prevention Inc.') {
                    continue;
                }
//            $error = $this->importErrorMessageManager->checkSiteData($data);
//
//            if ($error) {
//                continue;
//            }

                $contractor = $this->createContractor($data);
                if ($this->isShouldNotEmpty($data, self::REQUIRED_FIELDS[self::EMPTY_CONTRACTOR_USER])) {
                    $this->createContractorUser($data, $contractor, self::EMPTY_CONTRACTOR_USER);
                }
                if ($this->isShouldNotEmpty($data, self::REQUIRED_FIELDS[self::FIRST_CONTRACTOR_USER])) {
                    $this->createContractorUser($data, $contractor, self::FIRST_CONTRACTOR_USER);
                }
                if ($this->isShouldNotEmpty($data, self::REQUIRED_FIELDS[self::SECOND_CONTRACTOR_USER])) {
                    $this->createContractorUser($data, $contractor, self::SECOND_CONTRACTOR_USER);
                }

                $i++;
            }
        }

        unset($oldData);

        $this->om->flush();

        return $i;
    }

    /**
     * @param array $data
     * @return Contractor
     */
    private function createContractor(array $data)
    {
        if ($data['UsedAsSub'] === self::USED_AS_SUB_VALUE_YES) {
            $contractorType = $this->contractorTypeRepository->findOneBy(['alias' => self::CONTRACTOR_TYPE_PARTNER]);
        } else {
            $contractorType = $this->contractorTypeRepository->findOneBy(['alias' => self::CONTRACTOR_TYPE_COMPETITOR]);
        }
        $coiExpiration = !empty($data['CoiExpiration']) ? new \DateTime($data['CoiExpiration']) : null;

        $newContractor = new Contractor();
        $newContractor->setOldSistemId($data['RegContrTestID'] ?? null);
        $newContractor->setName($data['ContrCompName'] ?? '');
        $newContractor->setAddress($this->createContractorAddress($data));
        $newContractor->setPhone($data['ContrPhone'] ?? null);
        $newContractor->setWebsite($data['RegCompURL'] ?? null);
        $newContractor->setLogo($data['ContrLogo'] ?? null);
        $newContractor->setCoiExpiration($coiExpiration);
        $newContractor->setComment($data['Comments'] ?? null);
        $newContractor->setFax($data['ContrFax'] ?? null);
        $newContractor->setEmail($data['ContrEmail'] ?? null);
        $newContractor->setType($contractorType);
        $newContractor->setStateLicenseNumber($data['ContrStateLic'] ?? '');
        $newContractor->setStateLicenseExpirationDate(new \DateTime());
        $this->om->persist($newContractor);

        return $newContractor;
    }

    /**
     * @param $data
     * @return Address
     */
    private function createContractorAddress($data)
    {
        $stateCode = $this->stateRepository->findOneBy(['code' => strtoupper($data['ContrState'])]);
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_CONTRACTOR_ADDRESS]);

        $newAddress = new Address();
        $newAddress->setAddress($data['ContrAddr'] ?? null);
        $newAddress->setCity($data['ContrCity'] ?? null);
        $newAddress->setState($stateCode);
        $newAddress->setZip($data['ContrZip'] ?? null);
        $newAddress->setAddressType($addressType);
        $this->om->persist($newAddress);

        return $newAddress;
    }

    /**
     * @param array $data
     * @param int|bool $serialNumber
     * @return User
     */
    private function createNewUser(array $data, $serialNumber)
    {
        $accessTokenService = $this->container->get('api.user_access_token');
        $email = $this->prepareEmail($data, $serialNumber);

        $newUser = new User();
        $newUser->setFirstName($serialNumber ? $data['ContrConFName' . $serialNumber] : $data['ContrContName']);
        $newUser->setLastName($serialNumber ? $data['ContrConLName' . $serialNumber] : null);
        $newUser->addRole('ROLE_CONTACT');
        $newUser->setEmail($email);
        $newUser->setPlainPassword($email);
        $newUser->setEnabled(1);
        $newUserToken = $accessTokenService->createUserAccessToken($newUser);
        $newUser->setAccessToken($newUserToken);
        $this->om->persist($newUser);
        $this->users[$serialNumber ? $data['ContrConFName' . $serialNumber] : $data['ContrContName']] = $newUser;

        return $newUser;
    }

    /**
     * @param $data
     * @param Contractor $contractor
     * @param int|bool $serialNumber
     * @return ContractorUser
     */
    private function createContractorUser(
        $data,
        Contractor $contractor,
        $serialNumber = self::EMPTY_CONTRACTOR_USER
    ) {
        $user = null;
        if (!empty($data['ContrConEmail' . $serialNumber])) {
            $user = $this->userRepository->findUserBy(['email' => $data['ContrConEmail' . $serialNumber]]);
            $name = $serialNumber ? $data['ContrConFName' . $serialNumber] : $data['ContrContName'];
            if (empty($user) && array_key_exists($name, $this->users)) {
                $user = $this->users[$name];
            }
        }
        if (empty($user)) {
            $user = $this->createNewUser($data, $serialNumber);
        }

        $newContractorUser = new ContractorUser();
        $newContractorUser->setUser($user);
        $newContractorUser->setContractor($contractor);
        $newContractorUser->setTitle($serialNumber ? $data['ContrConTitle' . $serialNumber] : null);
        $newContractorUser->setFax($serialNumber ? $data['ContrConFax' . $serialNumber] : null);
//        $newContractorUser->setPersonalEmail($serialNumber ? $data['ContrConEmail' . $serialNumber] : null);
//        $newContractorUser->setPersonalPhone($serialNumber ? null : $data['ContrConPhone' . $serialNumber]);
        $newContractorUser->setWorkPhone($serialNumber ? $data['ContrConCell' . $serialNumber] : null);
        $this->om->persist($newContractorUser);

        return $newContractorUser;
    }

    /**
     * @param array $verifiableData
     * @param array $expectedFields
     * @return bool
     */
    public function isShouldNotEmpty(array $verifiableData, array $expectedFields)
    {
        foreach ($expectedFields as $field) {
            if (!empty($verifiableData[$field])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $data
     * @param int|bool $serialNumber
     * @return mixed|string
     */
    private function prepareEmail(array $data, $serialNumber)
    {
        if (!empty($data['ContrConEmail' . $serialNumber])) {
            $email = $data['ContrConEmail' . $serialNumber];
        } elseif (!empty($data['RegContrTestID']) && !empty($serialNumber)) {
            $email = 'id-' . $data['RegContrTestID'] . '-serial-number-' . $serialNumber . self::STATIC_PART_EMAIL;
        } else {
            $email = 'id-' . $data['RegContrTestID'] . self::STATIC_PART_EMAIL;
        }

        return $email;
    }
}
