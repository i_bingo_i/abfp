<?php
namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Services\AccountContactPersonHistory\Creator;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessingAccountContactPersonHistoryService
{
    private const NEED_FLUSH_ON = 1000;

    /** @var  ObjectManager */
    private $om;
    /** @var  ContainerInterface */
    private $container;
    /** @var  AccountContactPersonRepository */
    private $accountContactPersonRepository;
    /** @var  EntityManager */
    private $entityManager;
    /** @var OldDatbaseRepository */
    private $oldDatbaseRepository;
    /** @var Creator */
    private $accountContactPersonHistoryCreatorService;

    /**
     * ProcessingAccountContactPersonHistoryService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->entityManager = $this->container->get('app.entity_manager');
        $this->accountContactPersonRepository = $this->om->getRepository('AppBundle:AccountContactPerson');
        $this->accountContactPersonHistoryCreatorService =
            $this->container->get('app.account_contact_person_history_creator.service');
        $this->oldDatbaseRepository = $this->container->get('import.old_database.repository');
    }

    /**
     * @return int
     */
    public function processAccountContactPersonHistory()
    {
        $i = 0;
        $accountContactPersonsArray = $this->accountContactPersonRepository->findBy(['isHistoryProcessed' => false]);
        if (!empty($accountContactPersonsArray)) {
            foreach ($accountContactPersonsArray as $accountContactPerson) {
                $this->accountContactPersonHistoryCreatorService->create(
                    $accountContactPerson,
                    0
                );
                $accountContactPerson->setIsHistoryProcessed(true);
                $i++;
                $this->needLoad($i);
            }
            $this->om->flush();
        }
        unset($accountContactPersonsArray);

        return $i;
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function queryProcessAccountContactPersonHistory()
    {
        return  $this->oldDatbaseRepository->queryProcessAccountContactPersonHistory();
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeAllAccountContactPersonHistory()
    {
        return  $this->oldDatbaseRepository->removeAllAccountContactPersonHistory();
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

}