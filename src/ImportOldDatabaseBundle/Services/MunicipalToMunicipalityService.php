<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Address;
use AppBundle\Entity\Agent;
use AppBundle\Entity\AgentChannel;
use AppBundle\Entity\AgentChannelDynamicFieldValue;
use AppBundle\Entity\ChannelDynamicField;
use AppBundle\Entity\ChannelNamed;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Department;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\DepartmentChannelDynamicFieldValue;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\FeesBasis;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\AgentRepository;
use AppBundle\Entity\Repository\ChannelNamedRepository;
use AppBundle\Entity\Repository\ContactTypeRepository;
use AppBundle\Entity\Repository\DepartmentRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\FeesBasisRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\State;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\EntityTrait\PhoneFilterTrait;

class MunicipalToMunicipalityService
{
    use PhoneFilterTrait;

    private const TABLE_MUNICIPALITY_LEVEL = 'abfp.dbo.Municipal';
    private const ADDRESS_TYPE_MUNICIPALITY = 'municipality';
    private const ADDRESS_TYPE_CONTACT = 'contact address';
    private const CONTACT_TYPE_MUNICIPALITY = 'municipality_contact';
    private const CONTACT_TYPE_DEPARTMENT = 'department_contact';
    private const DIVISION_CATEGORY_BACKFLOW = 'backflow';
    private const DIVISION_CATEGORY_FIRE = 'fire';
    private const DIVISION_CATEGORY_PLUMBING = 'plumbing';
    private const FEES_BASIS = 'per device';
    private const CHANNEL_NAMED_UPLOAD = 'upload';
    private const CHANNEL_NAMED_EMAIL = 'email';
    private const CHANNEL_NAMED_FAX = 'fax';
    private const NEED_FLUSH_ON = 1000;

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var  MunicipalityRepository */
    private $municipalityRepository;
    /** @var  AddressTypeRepository */
    private $addressTypeRepository;
    /** @var StateRepository */
    private $stateRepository;
    /** @var  ContactTypeRepository */
    private $contactTypeRepository;
    /** @var  DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var  FeesBasisRepository */
    private $feesBasisRepository;
    /** @var ChannelNamedRepository */
    private $channelNamedRepository;
    /** @var  DepartmentRepository */
    private $departmentRepository;


    protected $existAgents = [];

    /**
     * MunicipalToMunicipalityService constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
        $this->municipalityRepository = $this->om->getRepository("AppBundle:Municipality");
        $this->addressTypeRepository = $this->om->getRepository('AppBundle:AddressType');
        $this->stateRepository = $this->om->getRepository('AppBundle:State');
        $this->contactTypeRepository = $this->om->getRepository('AppBundle:ContactType');
        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->feesBasisRepository = $this->om->getRepository('AppBundle:FeesBasis');
        $this->channelNamedRepository = $this->om->getRepository('AppBundle:ChannelNamed');
        $this->departmentRepository = $this->om->getRepository('AppBundle:Department');
    }

    /**
     * @return array
     */
    public function getAllMunicipalitiesFromOldDatabase()
    {
        return $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::TABLE_MUNICIPALITY_LEVEL);
    }

    /**
     * @return array
     */
    public function getImportedMunicipalityOldIds()
    {
        $importedIds =[];
        $importedMunicipalitiesArray = $this->municipalityRepository->getImportedMunicipality();

        foreach ($importedMunicipalitiesArray as $importedMunicipality) {
            $importedIds[] = $importedMunicipality["oldMunicipalityId"];
        }

        return $importedIds;
    }

    /**
     * @return int
     */
    public function importMunicipalities()
    {
        $i = 0;
        $oldMunicipalities = $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::TABLE_MUNICIPALITY_LEVEL);
        $importedIds = $this->getImportedMunicipalityOldIds();
        foreach ($oldMunicipalities as $oldMunicipality) {
            if (!in_array($oldMunicipality['MunicipalID'], $importedIds)) {
                $newMunicipality = $this->setMunicipalityTextFields($oldMunicipality);
                $newMunicipality = $this->createAndSetMunicipalityAddress($oldMunicipality, $newMunicipality);
                if ($oldMunicipality["MayorFName"] or $oldMunicipality["MayorLName"]) {
                    $mayorContact = $this->createMunicipalityContactPerson($oldMunicipality, "Mayor");
                    $newMunicipality->addContact($mayorContact);
                }
                if ($oldMunicipality["AdmFName"] or $oldMunicipality["AdmLName"]) {
                    $adminContact = $this->createMunicipalityContactPerson($oldMunicipality);
                    $newMunicipality->addContact($adminContact);
                }
                /** Create Backflow Municipality */
                $newMunicipality = $this->createDepartmentForMunicipality(
                    $oldMunicipality,
                    $newMunicipality,
                    self::DIVISION_CATEGORY_BACKFLOW
                );
                /** Create Fire Municipality */
                $newMunicipality = $this->createDepartmentForMunicipality(
                    $oldMunicipality,
                    $newMunicipality,
                    self::DIVISION_CATEGORY_FIRE
                );
                /** Create Plumbing Municipality */
                $newMunicipality = $this->createDepartmentForMunicipality(
                    $oldMunicipality,
                    $newMunicipality,
                    self::DIVISION_CATEGORY_PLUMBING
                );

                $this->om->persist($newMunicipality);
                $i++;
                $this->needLoad($i);
            }
        }
        unset($oldMunicipalities);
        unset($importedIds);
        $this->om->flush();

        return $i;
    }

    /**
     * @param $backflowMunicipalityOldId
     * @param $fireMunicipalityOldId
     * @return Municipality|null|object
     */
    public function createAndGetMixedMunicipality(
        $backflowMunicipalityOldId,
        $fireMunicipalityOldId
    ) {
        $backflowMunicipality = $this->municipalityRepository->findOneBy([
            'oldMunicipalityId' => $backflowMunicipalityOldId
        ]);

        $fireMunicipality = $this->municipalityRepository->findOneBy([
            'oldMunicipalityId' => $fireMunicipalityOldId
        ]);

        $checkExistMunicipality = $this->municipalityRepository->findOneBy([
            "oldBackflowDepartmentId" => $backflowMunicipalityOldId,
            "oldFireDepartmentId" => $fireMunicipalityOldId,
            "isMixedMunicipality" => true
        ]);

        if ($checkExistMunicipality) {
            return $checkExistMunicipality;
        }

        $newMixedMunicipality = null;

        if (!$backflowMunicipality or !$fireMunicipality) {
            // DO SOMETHING WITH THIS SHIT
            $noneId = $backflowMunicipalityOldId;
            if (!$fireMunicipality) {
                $noneId = $fireMunicipalityOldId;
            }
            $this->importErrorMessageManager->setErrorMessage("[create mixed Municipality error][oldMunicipalityId:" . $noneId . " ]: don't find");
        } else {

            $newMixedMunicipality = $this->createMixMunicipality(
                $backflowMunicipality,
                $fireMunicipality,
                $backflowMunicipalityOldId,
                $fireMunicipalityOldId
            );
        }

        return $newMixedMunicipality;
    }

    /**
     * Add channels for Mixed Municipalities Departments
     *
     * @return int
     */
    public function addChannelsForMixedMunicipality()
    {
        $mixedMunicipalitiesArray = $this->municipalityRepository->findBy(["isMixedMunicipality" =>true]);
        $i = 0;
        if ($mixedMunicipalitiesArray) {
            foreach ($mixedMunicipalitiesArray as $mixedMunicipality) {
                $this->processingMixedMunicipalitiesDepartments($mixedMunicipality);
                $i++;
            }
        }

        return $i;
    }


    private function processingMixedMunicipalitiesDepartments(Municipality $municipality)
    {
        $municipalitydepartmentsArray = $municipality->getDepartment();
        if ($municipalitydepartmentsArray) {
            foreach ($municipalitydepartmentsArray as $mixedMunicipalitydepartment) {
                $sourceDepartment = $this->getSourceDepartmentForMixedMunicipalitiesDepartment(
                    $mixedMunicipalitydepartment
                );

                $this->processingChannelsForMixedMunicipalityDepartments(
                    $sourceDepartment,
                    $mixedMunicipalitydepartment
                );

                $this->om->flush();
            }
        }
    }


//    public function ozalupitMunicipality()
//    {
//        $mixedMunicipalitiesArray = $this->municipalityRepository->findBy(["isMixedMunicipality" =>true]);
//        if ($mixedMunicipalitiesArray) {
//            foreach ($mixedMunicipalitiesArray as $mixedMunicipality) {
//                $accounts = $mixedMunicipality->getAccount();
//                foreach ($accounts as $account) {
//                    $this->container->get('app.account.manager')->checkDefaultDepartmentChanel($account, false);
//                }
//            }
//
//            $this->om->flush();
//        }
//    }

    /**
     * @param Department $mixedDepartment
     * @return mixed
     */
    private function getSourceDepartmentForMixedMunicipalitiesDepartment(Department $mixedDepartment)
    {
        $departmentDivision = $mixedDepartment->getDivision()->getAlias();
        $oldId = 0;
        if ($departmentDivision == self::DIVISION_CATEGORY_BACKFLOW) {
            $oldId = $mixedDepartment->getMunicipality()->getOldBackflowDepartmentId();

        } elseif ($departmentDivision == self::DIVISION_CATEGORY_FIRE) {
            $oldId = $mixedDepartment->getMunicipality()->getOldFireDepartmentId();
        }

        $sourceMunicipality = $this->municipalityRepository->findOneBy(['oldMunicipalityId' => $oldId]);

        $sourceDepartment = $this->departmentRepository->getMunicipalityDepartmentByDevisionAlias(
            $sourceMunicipality,
            $departmentDivision
        );

        return $sourceDepartment;
    }


    /**
     * @param Department $sourceDepartment
     * @param Department $mixedDepartment
     */
    private function processingChannelsForMixedMunicipalityDepartments(
        Department $sourceDepartment,
        Department $mixedDepartment
    ) {
        $channels = $mixedDepartment->getChannels();

        /** @var DepartmentChannel $channel */
        foreach ($channels as $channel) {
            $channelValues = $channel->getFields();
            /** @var DynamicFieldValue $value */
            foreach ($channelValues as $value) {
                $this->om->remove($value);
            }
            $mixedDepartment->removeChannel($channel);
            $this->om->remove($channel);
        }
        $this->om->persist($mixedDepartment);

        $this->copyChannelsForDepartment($sourceDepartment, $mixedDepartment);

    }

    /**
     * @param $oldMunicipality
     * @return Municipality
     */
    private function setMunicipalityTextFields($oldMunicipality)
    {
        $newMunicipality = new Municipality();
        $newMunicipality->setOldMunicipalityId($oldMunicipality["MunicipalID"]);
        $newMunicipality->setName($oldMunicipality["MunName"]);
        $newMunicipality->setWebsite($oldMunicipality["MunWebSite"]);
        $newMunicipality->setMunicipalityPhone($this->filterPhone($oldMunicipality["MunPhone"]));
        $newMunicipality->setMunicipalityFax($this->filterPhone($oldMunicipality["MunFax"]));

        return $newMunicipality;
    }

    /**
     * @param $oldMunicipality
     * @param Municipality $newMunicipality
     * @return Municipality
     */
    private function createAndSetMunicipalityAddress($oldMunicipality, Municipality $newMunicipality)
    {
        /** @var Address $address */
        $newAddress = new Address();
        $state = null;
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_MUNICIPALITY]);

        if ($addressType) {
            $newAddress->setAddressType($addressType);
        }
        if ($oldMunicipality['MunState']) {
            /** @var State $state */
            $state = $this->stateRepository->findOneBy(['code' => strtoupper($oldMunicipality['MunState'])]);
        }
        if ($state) {
            $newAddress->setState($state);
        }
        $addressLine = $oldMunicipality['MunAddr'] ? $oldMunicipality['MunAddr'] : '';

        $newAddress->setAddress($addressLine);
        $newAddress->setCity($oldMunicipality['MunCity']);
        $newAddress->setZip($oldMunicipality['MunZip']);

        $this->om->persist($newAddress);
        $newMunicipality->setAddress($newAddress);
        $this->om->persist($newMunicipality);

        return $newMunicipality;
    }

    /**
     * @param $oldMunicipality
     * @param string $title
     * @return Contact
     */
    private function createMunicipalityContactPerson($oldMunicipality, $title = "")
    {
        /** @var Contact $newContact */
        $newContact = new Contact();

        $contactType = $this->contactTypeRepository->findOneBy(["alias" => self::CONTACT_TYPE_MUNICIPALITY]);

        if ($contactType) {
            $newContact->setType($contactType);
        }
        $firstName = ($title == "Mayor") ? $oldMunicipality["MayorFName"] : $oldMunicipality["AdmFName"];
        if (empty($firstName)) {
            $firstName = "No Name";
        }

        $newContact->setFirstName($firstName);
        $newContact->setLastName(($title == "Mayor") ? $oldMunicipality["MayorLName"] : $oldMunicipality["AdmLName"]);
        $newContact->setTitle(($title == "Mayor") ? $title : $oldMunicipality["AdmTitle"]);
        $this->om->persist($newContact);

        return $newContact;
    }


    /**
     * @param $oldMunicipality
     * @param Municipality $newMunicipality
     * @param $category
     * @return Municipality
     */
    private function createDepartmentForMunicipality($oldMunicipality, Municipality $newMunicipality, $category)
    {
        /** @var Department $newDepartment */
        $newDepartment = new Department();

        /** @var DeviceCategory $division */
        $division = $this->deviceCategoryRepository->findOneBy(['alias' => $category]);

        if ($division) {
            $newDepartment->setDivision($division);
        }

        $newDepartment->setName($oldMunicipality["MunName"]." ".$division->getName()." Department");
        $newDepartment->setMunicipality($newMunicipality);
        $newMunicipality->addDepartment($newDepartment);
        $newDepartment = $this->setContactPersonForDepartment($oldMunicipality, $newDepartment, $category);
        $agentName = $this->checkDepartmentForAgentAndGetAgentName($oldMunicipality, $newDepartment);

        if ($agentName) {
            $newDepartment = $this->checkExistAgentAndProcess($oldMunicipality, $newDepartment, $agentName);
        } else {
            $newDepartment = $this->addChanelsForDepartment($oldMunicipality, $newDepartment);
        }

        $this->om->persist($newDepartment);

        return $newMunicipality;
    }

    /**
     * @param $oldMunicipality
     * @param Department $newDepartment
     * @param $category
     * @return Department
     */
    private function setContactPersonForDepartment($oldMunicipality, Department $newDepartment, $category)
    {
        $prefix = $this->createPrefix($category);

        if (!empty($oldMunicipality[$prefix."FName"]) or
            !empty($oldMunicipality[$prefix."LName"]) or
            !empty($oldMunicipality[$prefix."LName"]) or
            !empty($oldMunicipality[$prefix."Title"]) or
            !empty($oldMunicipality[$prefix."Addr"]) or
            !empty($oldMunicipality[$prefix."City"]) or
            !empty($oldMunicipality[$prefix."State"]) or
            !empty($oldMunicipality[$prefix."Zip"]) or
            !empty($oldMunicipality[$prefix."Phone"]) or
            !empty($oldMunicipality[$prefix."PhoneExt"]) or
            !empty($oldMunicipality[$prefix."Fax"]) or
            !empty($oldMunicipality[$prefix."Email"])
        ) {
            $newDepartment->addContact($this->createContactForDepartment($oldMunicipality, $prefix));
        }

        return $newDepartment;
    }


    /**
     * @param $oldMunicipality
     * @param string $prefix
     * @return Contact
     */
    private function createContactForDepartment($oldMunicipality, $prefix)
    {
        /** @var Contact $newContact */
        $newContact = new Contact();

        $contactType = $this->contactTypeRepository->findOneBy(["alias" => self::CONTACT_TYPE_DEPARTMENT]);
        if ($contactType) {
            $newContact->setType($contactType);
        }
        $newContact->setFirstName(
            !empty($oldMunicipality[$prefix."FName"]) ? $oldMunicipality[$prefix."FName"] : "No Name"
        );
        $newContact->setLastName($oldMunicipality[$prefix."LName"]);
        $newContact->setTitle($oldMunicipality[$prefix."Title"]);
        $newContact->setPhone($this->filterPhone($oldMunicipality[$prefix."Phone"]));
        $newContact->setExt($oldMunicipality[$prefix."PhoneExt"]);
        $newContact->setFax($this->filterPhone($oldMunicipality[$prefix."Fax"]));
        $newContact->setEmail($oldMunicipality[$prefix."Email"]);
        $newContact->setAddress($this->createDepartmentContacsAddress($oldMunicipality, $prefix));

        $this->om->persist($newContact);

        return $newContact;
    }

    /**
     * @param $oldMunicipality
     * @param string $prefix
     * @return Address
     */
    private function createDepartmentContacsAddress($oldMunicipality, $prefix = "Public")
    {
        /** @var Address $address */
        $newAddress = new Address();
        $state = null;

        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_CONTACT]);

        if ($addressType) {
            $newAddress->setAddressType($addressType);
        }
        if ($oldMunicipality[$prefix.'State']) {
            /** @var State $state */
            $state = $this->stateRepository->findOneBy(['code' => strtoupper($oldMunicipality[$prefix.'State'])]);
        }

        if ($state) {
            $newAddress->setState($state);
        }

        $newAddress->setAddress($oldMunicipality[$prefix."Addr"]);
        $newAddress->setCity($oldMunicipality[$prefix."City"]);
        $newAddress->setZip($oldMunicipality[$prefix."Zip"]);

        $this->om->persist($newAddress);

        return $newAddress;

    }

    /**
     * @param $oldMunicipality
     * @param Department $newDepartment
     * @return bool
     */
    private function checkDepartmentForAgentAndGetAgentName($oldMunicipality, Department $newDepartment)
    {

        if ($newDepartment->getDivision()->getAlias() == self::DIVISION_CATEGORY_BACKFLOW and
            !empty($oldMunicipality["MunOnline"])
        ) {
            return $oldMunicipality["MunOnline"];
        }

        if ($newDepartment->getDivision()->getAlias() == self::DIVISION_CATEGORY_FIRE and
            !empty($oldMunicipality["FiMunOnline"])
        ) {
            return $oldMunicipality["FiMunOnline"];
        }

        return false;
    }

    /**
     * Agent Logic for Department
     */


    /**
     * @param $oldMunicipality
     * @param $newDepartment
     * @param $agentName
     * @return mixed
     */
    public function checkExistAgentAndProcess($oldMunicipality, $newDepartment, $agentName)
    {
        $existAgent = !empty($this->existAgents[$agentName]) ? $this->existAgents[$agentName] : "";

        if (!$existAgent) {
            $newDepartment= $this->createAgentForDepartment($oldMunicipality, $newDepartment, $agentName);
        } else {
            $existAgent->addDepartment($newDepartment);
            $existAgent = $this->addChannelsForAgent($oldMunicipality, $existAgent, $newDepartment);
            $this->attachAgentToDepartment($newDepartment, $existAgent, $oldMunicipality);
            $this->om->persist($existAgent);
        }


        return $newDepartment;
    }

    /**
     * @param $oldMunicipality
     * @param Department $newDepartment
     * @param $agentName
     * @return Department
     */
    private function createAgentForDepartment($oldMunicipality, Department &$newDepartment, $agentName)
    {
        /** @var Agent $newAgent */
        $newAgent = new Agent();

        $newAgent->setName($agentName);
        $newAgent->addDepartment($newDepartment);
        $newAgent = $this->addChannelsForAgent($oldMunicipality, $newAgent, $newDepartment);
        $this->attachAgentToDepartment($newDepartment, $newAgent, $oldMunicipality);
        $this->om->persist($newAgent);

        $this->existAgents[$agentName] =  $newAgent;

        return $newDepartment;
    }


    private function addChannelsForAgent($oldMunicipality, Agent $newAgent, Department $newDepartment)
    {
        /** @var DeviceCategory $division */
        $division = $newDepartment->getDivision();
        $uploadChannelFee= 0;

        /** For Backflow Department */
        if ($division->getAlias() == self::DIVISION_CATEGORY_BACKFLOW) {
            $uploadChannelFee = $oldMunicipality["MunFee"];
            $emailChannelSendTo = $oldMunicipality["BfReportsEmail"];
            $faxChannelSendTo = $oldMunicipality["BfReportsFax"];

        } elseif ($division->getAlias() == self::DIVISION_CATEGORY_FIRE) {
            $uploadChannelFee = $oldMunicipality["FiMunFee"];
            $emailChannelSendTo = $oldMunicipality["FireReportsEmail"];
            $faxChannelSendTo = $oldMunicipality["FireReportsFax"];
        }

        /** Create Upload Channel */
        if (!empty($uploadChannelFee) and
            !$this->checkExistAgentChannel($newAgent, self::CHANNEL_NAMED_UPLOAD, $division->getAlias())
        ) {
            $newUploadChannel = new AgentChannel();
            $channelNamed = $this->channelNamedRepository->findOneBy(['alias' => self::CHANNEL_NAMED_UPLOAD]);
            $newUploadChannel->setIsDefault(true);
            $newUploadChannel->setNamed($channelNamed);
            $newUploadChannel->setAgent($newAgent);
            $newUploadChannel = $this->createAgentsChannelProperties($newUploadChannel, $newDepartment, '');
            $this->om->persist($newUploadChannel);
            $newAgent->addChannel($newUploadChannel);
        }

        /** Create Email Channel */
        if (!empty($emailChannelSendTo) and
            !$this->checkExistAgentChannel($newAgent, self::CHANNEL_NAMED_EMAIL, $division->getAlias())
        ) {
            $newEmailChannel = new AgentChannel();
            $channelNamed = $this->channelNamedRepository->findOneBy(['alias' => self::CHANNEL_NAMED_EMAIL]);
            $newEmailChannel->setNamed($channelNamed);
            $newEmailChannel->setAgent($newAgent);
            if (empty($uploadChannelFee)) {
                $newEmailChannel->setIsDefault(true);
            }
            $newEmailChannel = $this->createAgentsChannelProperties(
                $newEmailChannel,
                $newDepartment,
                ''
            );
            $this->om->persist($newEmailChannel);
            $newAgent->addChannel($newEmailChannel);
        }

        /** Create Fax Channel */
        if (!empty($faxChannelSendTo) and
            !$this->checkExistAgentChannel($newAgent, self::CHANNEL_NAMED_FAX, $division->getAlias())
        ) {
            $newFaxChannel = new AgentChannel();
            $channelNamed = $this->channelNamedRepository->findOneBy(['alias' => self::CHANNEL_NAMED_FAX]);
            $newFaxChannel->setNamed($channelNamed);
            $newFaxChannel->setAgent($newAgent);
            if (empty($uploadChannelFee) and empty($emailChannelSendTo)) {
                $newFaxChannel->setIsDefault(true);
            }
            $newFaxChannel = $this->createAgentsChannelProperties(
                $newFaxChannel,
                $newDepartment,
                ''
            );
            $this->om->persist($newFaxChannel);

            $newAgent->addChannel($newFaxChannel);
        }

        return $newAgent;
    }

    /**
     * @param Agent $agent
     * @param $channelNamed
     * @param $division
     * @return bool
     */
    private function checkExistAgentChannel(Agent $agent, $channelNamed, $division)
    {
        $agentsChannels = $agent->getChannels();
        /** @var AgentChannel $agentChanel */
        foreach ($agentsChannels as $agentChanel) {
            if ($agentChanel->getNamed()->getAlias() == $channelNamed and
                $agentChanel->getDeviceCategory()->getAlias() == $division
            ) {
                return true;
            }

        }

        return false;
    }

    /**
     * @param AgentChannel $newAgentChannel
     * @param Department $newDepartment
     * @param $value
     * @return AgentChannel
     */
    private function createAgentsChannelProperties(AgentChannel $newAgentChannel, Department $newDepartment, $value)
    {
        $deviceCategory = $newDepartment->getDivision();
        $newAgentChannel->setDeviceCategory($deviceCategory);
        $dynamicFields = $newAgentChannel->getNamed()->getFields();
        /** @var ChannelDynamicField $dynamicField */
        foreach ($dynamicFields as $dynamicField) {
            $newValue = new AgentChannelDynamicFieldValue();
            $newValue->setAgentChannel($newAgentChannel);
            $newValue->setField($dynamicField);
            if ($newAgentChannel->getNamed()->getAlias() == self::CHANNEL_NAMED_FAX) {
                $newValue->setValue($this->filterPhone($value));
            } else {
                $newValue->setValue($value);
            }
            $newAgentChannel->addDynamicField($newValue);

            $this->om->persist($newValue);
        }
        return $newAgentChannel;
    }


    public function attachAgentToDepartment(Department $newDepartment, Agent $newAgent, $oldMunicipality)
    {
        $agentChannels = $newAgent->getChannels();

        $newDepartment->setAgent($newAgent);

        $this->allUnActiveByDepartment($newDepartment);

        if (count($agentChannels)) {
            foreach ($agentChannels as $agentChannel) {
                if ($agentChannel->getDeviceCategory() === $newDepartment->getDivision()) {
                    $this->copyAgentToDepartment($agentChannel, $newDepartment, $oldMunicipality);
                }
            }
        }
    }

    /**
     * Copy AgentChannel to DepartmentChannel
     *
     *
     * @param AgentChannel $newAgentChannel
     * @param Department $newDepartment
     * @param $oldMunicipality
     */
    private function copyAgentToDepartment(AgentChannel $newAgentChannel, Department $newDepartment, $oldMunicipality)
    {

        $feesBasis = $this->feesBasisRepository->findOneBy(['alias' => self::FEES_BASIS]);

        $departmentChannel = new DepartmentChannel();
        $departmentChannel->setNamed($newAgentChannel->getNamed());
        $departmentChannel->setDepartment($newDepartment);
        $departmentChannel->setIsDefault($newAgentChannel->getIsDefault());
        $departmentChannel->setDevision($newAgentChannel->getDeviceCategory());
        $departmentChannel->setOwnerAgent($newAgentChannel->getAgent());
        $departmentChannel->setOwner($newAgentChannel);
        $departmentChannel->setActive(true);
        $departmentChannel->setFeesBasis($feesBasis);

        if ($departmentChannel->getNamed()->getAlias() == self::CHANNEL_NAMED_UPLOAD) {
            $uploadChannelFee = $oldMunicipality["MunFee"];
            if ($newDepartment->getDivision()->getAlias() == self::DIVISION_CATEGORY_FIRE) {
                $uploadChannelFee = $oldMunicipality["FiMunFee"];
            }

            if ($newDepartment->getDivision()->getAlias() == self::DIVISION_CATEGORY_BACKFLOW) {
                $processingFee = 0;
                if ($uploadChannelFee >= 1) {
                    $uploadChannelFee = $uploadChannelFee - 1;
                    $processingFee = 1;
                }
                $departmentChannel->setProcessingFee($processingFee);
            }
            $departmentChannel->setUploadFee($uploadChannelFee);


        }

        $this->om->persist($departmentChannel);

        if (count($newAgentChannel->getDynamicFields())) {
            foreach ($newAgentChannel->getDynamicFields() as $dynamicField) {
                    $this->copyDynamicFields($dynamicField, $departmentChannel, $oldMunicipality);

            }
        }
    }

    /**
     * Copy AgentChannelDynamicFieldValue to DepartmentChannelDynamicFieldValue
     *
     * @param AgentChannelDynamicFieldValue $dynamicField
     * @param DepartmentChannel $departmentChannel
     * @param $oldMunicipality
     */
    private function copyDynamicFields(
        AgentChannelDynamicFieldValue $dynamicField,
        DepartmentChannel $departmentChannel,
        $oldMunicipality
    ) {
        $departmentChannelDynamicFieldValue = new DepartmentChannelDynamicFieldValue();
        $valForField = $this->getValueForChannel($departmentChannel, $oldMunicipality);
        $departmentChannelDynamicFieldValue->setValue(!empty($valForField) ? $valForField : $dynamicField->getValue());
        $departmentChannelDynamicFieldValue->setEntityValue($dynamicField->getEntityValue());
        $departmentChannelDynamicFieldValue->setField($dynamicField->getField());
        $departmentChannelDynamicFieldValue->setDepartmentChanel($departmentChannel);

        $this->om->persist($departmentChannelDynamicFieldValue);
    }

    /**
     * @param DepartmentChannel $departmentChannel
     * @param $oldMunicipality
     * @return bool|mixed
     */
    private function getValueForChannel(DepartmentChannel $departmentChannel, $oldMunicipality)
    {
        /** @var DeviceCategory $division */
        $division = $departmentChannel->getDevision();
        /** @var ChannelNamed $chanelNamed */
        $chanelNamed = $departmentChannel->getNamed();
        $emailValue = false;
        $faxValue = false;

        if ($division->getAlias() == self::DIVISION_CATEGORY_BACKFLOW) {
            $emailValue = $oldMunicipality["BfReportsEmail"];
            $faxValue = $this->filterPhone($oldMunicipality["BfReportsFax"]);
        }

        if ($division->getAlias() == self::DIVISION_CATEGORY_FIRE) {
            $emailValue = $oldMunicipality["FireReportsEmail"];
            $faxValue = $this->filterPhone($oldMunicipality["FireReportsFax"]);
        }

        if ($chanelNamed->getAlias() == self::CHANNEL_NAMED_EMAIL) {
            return $emailValue;
        }

        if ($chanelNamed->getAlias() == self::CHANNEL_NAMED_FAX) {
            return $faxValue;
        }

        return false;

    }

    /**
     * @param Department $newDepartment
     */
    private function allUnActiveByDepartment(Department $newDepartment)
    {
        $this->setFieldsActiveByDepartment(false, $newDepartment);
    }
    /**
     * @param $value
     * @param Department $newDepartment
     */
    private function setFieldsActiveByDepartment($value, Department $newDepartment)
    {
        /** @var DepartmentChannel[] $channels */
        $channels = $newDepartment->getChannels();
        /** @var DepartmentChannel $channel */
        foreach ($channels as $channel) {
            $channel->setActive($value);
            $this->om->persist($channel);
        }

    }

    /**
     * Department Channels Logic for Department
     */

    /**
     * @param $oldMunicipality
     * @param Department $newDepartment
     * @return Department
     */
    private function addChanelsForDepartment($oldMunicipality, Department &$newDepartment)
    {
        $uploadChannelFee= 0;
        /** For Backflow Department */
        if ($newDepartment->getDivision()->getAlias() == self::DIVISION_CATEGORY_BACKFLOW) {
            $uploadChannelFee = $oldMunicipality["MunFee"];
            $emailChannelSendTo = $oldMunicipality["BfReportsEmail"];
            $faxChannelSendTo = $oldMunicipality["BfReportsFax"];

        } elseif ($newDepartment->getDivision()->getAlias() == self::DIVISION_CATEGORY_FIRE) {
            $uploadChannelFee = $oldMunicipality["FiMunFee"];
            $emailChannelSendTo = $oldMunicipality["FireReportsEmail"];
            $faxChannelSendTo = $oldMunicipality["FireReportsFax"];
        }
        $processingFee = 0;

        if ($uploadChannelFee >= 1) {
            $uploadChannelFee = $uploadChannelFee - 1;
            $processingFee = 1;
        }



        /** Create Backflow Upload Channel */
        if (!empty($uploadChannelFee)) {
            $newUploadChannel = new DepartmentChannel();
            $channelNamed = $this->channelNamedRepository->findOneBy(['alias' => self::CHANNEL_NAMED_UPLOAD]);
            $newUploadChannel->setIsDefault(true);
            $newUploadChannel->setNamed($channelNamed);
            $newUploadChannel->setUploadFee($uploadChannelFee);
            $newUploadChannel->setProcessingFee($processingFee);
            $newUploadChannel->setDepartment($newDepartment);
            $newUploadChannel = $this->createDepartmentChannelProperties($newUploadChannel, '');
            $this->om->persist($newUploadChannel);
        }

        /** Create Backflow Email Channel */
        if (!empty($emailChannelSendTo)) {
            $newEmailChannel = new DepartmentChannel();
            $channelNamed = $this->channelNamedRepository->findOneBy(['alias' => self::CHANNEL_NAMED_EMAIL]);
            $newEmailChannel->setNamed($channelNamed);
            $newEmailChannel->setDepartment($newDepartment);
            if (empty($uploadChannelFee)) {
                $newEmailChannel->setIsDefault(true);
            }
            $newEmailChannel = $this->createDepartmentChannelProperties(
                $newEmailChannel,
                $emailChannelSendTo
            );
            $this->om->persist($newEmailChannel);
        }

        /** Create Backflow Fax Channel */
        if (!empty($faxChannelSendTo)) {
            $newFaxChannel = new DepartmentChannel();
            $channelNamed = $this->channelNamedRepository->findOneBy(['alias' => self::CHANNEL_NAMED_FAX]);
            $newFaxChannel->setNamed($channelNamed);
            $newFaxChannel->setDepartment($newDepartment);
            if (empty($uploadChannelFee) and empty($emailChannelSendTo)) {
                $newFaxChannel->setIsDefault(true);
            }
            $newFaxChannel = $this->createDepartmentChannelProperties(
                $newFaxChannel,
                $faxChannelSendTo
            );
            $this->om->persist($newFaxChannel);
        }

        return $newDepartment;
    }

    /**
     * @param DepartmentChannel $newDepartmentChannel
     * @param $value
     * @return DepartmentChannel
     *
     */
    private function createDepartmentChannelProperties(DepartmentChannel $newDepartmentChannel, $value)
    {
        /** @var FeesBasis $feesBasis */
        $feesBasis = $this->feesBasisRepository->findOneBy(['alias' => self::FEES_BASIS]);
        $newDepartmentChannel->setFeesBasis($feesBasis);

        $dynamicFields = $newDepartmentChannel->getNamed()->getFields();
        /** @var ChannelDynamicField $dynamicField */
        foreach ($dynamicFields as $dynamicField) {
            $newValue = new DepartmentChannelDynamicFieldValue();
            $newValue->setDepartmentChanel($newDepartmentChannel);
            $newValue->setField($dynamicField);
            $newDepartmentChannel->addField($newValue);
            if ($newDepartmentChannel->getNamed()->getAlias() == self::CHANNEL_NAMED_FAX) {
                $newValue->setValue($this->filterPhone($value));
            } else {
                $newValue->setValue($value);
            }


            $this->om->persist($newValue);
        }
        return $newDepartmentChannel;
    }

    /**
     * @param $category
     * @return string
     */
    private function createPrefix($category)
    {
        $prefix = "Public";
        if ($category == self::DIVISION_CATEGORY_FIRE) {
            $prefix = "Fire";
        } elseif ($category == self::DIVISION_CATEGORY_PLUMBING) {
            $prefix = "Plumb";
        }

        return $prefix;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

    /**
     *
     * Logic for creating Mixed Municipality For Account with different departments
     *
     */


    /**
     * @param Municipality $backflowMunicipality
     * @param Municipality $fireMunicipality
     * @return Municipality
     */
    private function createMixMunicipality(
        Municipality $backflowMunicipality,
        Municipality $fireMunicipality,
        $backflowMunicipalityOldId,
        $fireMunicipalityOldId
    ) {
        $newMixedMunicipality = new Municipality();
        $newMixedMunicipality->setName("Mixed municipality of ".$backflowMunicipality->getName()." and ".$fireMunicipality->getName());
        $newMixedMunicipality->setIsMixedMunicipality(true);
        $newMixedMunicipality->setOldBackflowDepartmentId($backflowMunicipalityOldId);
        $newMixedMunicipality->setOldFireDepartmentId($fireMunicipalityOldId);
        $newMixedMunicipality = $this->collectDepartments(
            $backflowMunicipality,
            $fireMunicipality,
            $newMixedMunicipality
        );

        $this->om->persist($newMixedMunicipality);
        $this->om->flush();

        return $newMixedMunicipality;
    }


    /**
     * @param Municipality $backflowMunicipality
     * @param Municipality $fireMunicipality
     * @param Municipality $newMixedMunicipality
     * @return Municipality
     */
    private function collectDepartments(
        Municipality $backflowMunicipality,
        Municipality $fireMunicipality,
        Municipality $newMixedMunicipality
    ) {

        $backFlowDepartment = $this->departmentRepository->getMunicipalityDepartmentByDevisionAlias(
            $backflowMunicipality,
            self::DIVISION_CATEGORY_BACKFLOW
        );

        $fireFlowDepartment = $this->departmentRepository->getMunicipalityDepartmentByDevisionAlias(
            $fireMunicipality,
            self::DIVISION_CATEGORY_FIRE
        );

        $newMixedMunicipality = $this->copyDepartment($backFlowDepartment, $newMixedMunicipality);
        $newMixedMunicipality = $this->copyDepartment($fireFlowDepartment, $newMixedMunicipality);

        return $newMixedMunicipality;
    }


    /**
     * @param Department $oldDepartment
     * @param Municipality $newMixedMunicipality
     * @return Municipality
     */
    private function copyDepartment(Department $oldDepartment, Municipality $newMixedMunicipality)
    {
        /** @var Department $newDepartment */
        $newDepartment = new Department();
        $newDepartment->setName($oldDepartment->getName());
        $newDepartment->setDivision($oldDepartment->getDivision());
        $newDepartment->setAgent($oldDepartment->getAgent());
        $newDepartment->setMunicipality($newMixedMunicipality);
        $newDepartment = $this->copyContactsForDepartment($oldDepartment, $newDepartment);
        $newDepartment = $this->copyChannelsForDepartment($oldDepartment, $newDepartment);
        $newMixedMunicipality->addDepartment($newDepartment);
        $this->om->persist($newDepartment);

        return $newMixedMunicipality;
    }

    /**
     * @param Department $oldDepartment
     * @param Department $newDepartment
     * @return Department
     */
    private function copyContactsForDepartment(Department $oldDepartment, Department $newDepartment)
    {
        /** @var Collection $contacts */
        $contacts = $oldDepartment->getContacts();
        if (!empty($contacts)) {
            /** @var Contact $contact */
            foreach ($contacts as $oldContact) {
                $newDepartment->addContact($this->copyContact($oldContact));
            }
        }

        return $newDepartment;
    }

    /**
     * @param Contact $oldContact
     * @return Contact
     */
    private function copyContact(Contact $oldContact)
    {
        /** @var  $newContact */
        $newContact = new Contact();
        $newContact->setFirstName($oldContact->getFirstName());
        $newContact->setLastName($oldContact->getLastName());
        $newContact->setAddress($oldContact->getAddress());
        $newContact->setCell($oldContact->getCell());
        $newContact->setPhone($oldContact->getPhone());
        $newContact->setFax($oldContact->getFax());
        $newContact->setExt($oldContact->getExt());
        $newContact->setEmail($oldContact->getEmail());
        $newContact->setTitle($oldContact->getTitle());
        $newContact->setType($oldContact->getType());
        $this->om->persist($newContact);

        return $newContact;
    }

    /**
     * @param Department $oldDepartment
     * @param Department $newDepartment
     * @return Department
     */
    private function copyChannelsForDepartment(Department $oldDepartment, Department $newDepartment)
    {
        $channels = $oldDepartment->getChannels();
        if (!empty($channels)) {
            foreach ($channels as $oldChannel) {
                $newChannel = $this->copyChannel($oldChannel, $newDepartment);
                $newDepartment->addChannel($newChannel);
            }
        }

        return $newDepartment;
    }

    /**
     * @param DepartmentChannel $oldChannel
     * @param Department $newDepartment
     * @return DepartmentChannel
     */
    private function copyChannel(DepartmentChannel $oldChannel, Department $newDepartment)
    {
        /** @var DepartmentChannel $newChannel */
        $newChannel = new DepartmentChannel();
        $newChannel->setProcessingFee($oldChannel->getProcessingFee());
        $newChannel->setUploadFee($oldChannel->getUploadFee());
        $newChannel->setNamed($oldChannel->getNamed());
        $newChannel->setFeesBasis($oldChannel->getFeesBasis());
        $newChannel->setIsDefault($oldChannel->getIsDefault());
        $newChannel->setDevision($oldChannel->getDevision());
        $newChannel->setDeleted($oldChannel->getDeleted());
        $newChannel->setOwner($oldChannel->getOwner());
        $newChannel->setOwnerAgent($oldChannel->getOwnerAgent());
        $newChannel->setActive($oldChannel->getActive());
        $newChannel->setDepartment($newDepartment);
        $dynamicFields = $oldChannel->getFields();
        if (!empty($dynamicFields)) {
            foreach ($dynamicFields as $oldField) {
                $dynamicValue = $this->copyDynamicFieldsForChannel($oldField, $newChannel);
                $newChannel->addField($dynamicValue);
            }
        }
        $this->om->persist($newChannel);

        return $newChannel;
    }


    /**
     * @param DepartmentChannelDynamicFieldValue $oldDynamicValue
     * @param DepartmentChannel $newDepartmentChannel
     * @return DepartmentChannelDynamicFieldValue
     */
    private function copyDynamicFieldsForChannel(
        DepartmentChannelDynamicFieldValue $oldDynamicValue,
        DepartmentChannel $newDepartmentChannel
    ) {

        /** @var DepartmentChannelDynamicFieldValue $newDynamicFieldValue */
        $newDynamicFieldValue = new DepartmentChannelDynamicFieldValue();
        $newDynamicFieldValue->setField($oldDynamicValue->getField());
        $newDynamicFieldValue->setDepartmentChanel($newDepartmentChannel);
        $newDynamicFieldValue->setValue($oldDynamicValue->getValue());
        $newDynamicFieldValue->setEntityValue($oldDynamicValue->getEntityValue());
        $newDynamicFieldValue->setOptionValue($oldDynamicValue->getOptionValue());

        $this->om->persist($newDynamicFieldValue);

        return $newDynamicFieldValue;
    }
}
