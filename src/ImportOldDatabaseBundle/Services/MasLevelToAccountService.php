<?php

namespace ImportOldDatabaseBundle\Services;

//ini_set('max_execution_time', 900);

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\AccountTypeRepository;
use AppBundle\Entity\Repository\ClientTypeRepository;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Entity\Repository\ContactPersonRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\StateRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use AppBundle\Entity\Repository\AddressTypeRepository;

class MasLevelToAccountService
{
    private const TABLE_MAS_LEVEL = 'abfp.dbo.MasLevel';
    private const ACCOUNT_TYPE_GROUP_ACCOUNT = 'group account';
    private const ADDRESS_TYPE_PERSONAL = 'personal';
    public const ACCOUNT_CLIENT_TYPE_ACTIVE = 'active';
    public const DEFAULT_MUNICIPALITY = 'City of Chicago';
    private const ADDRESS_TYPE_GROUP_ACCOUNT = 'group account address';
    private const DUMMY_ADDRESS = 'Dummy Address';
    private const NEED_FLUSH_ON = 1000;
    /** First name conflict contact person */
    private const MAILING_ADDRESS_CONFLICT = 'Mailing Address Conflict!';
    /** One of these fields should not be empty */
    private const REQUIRED_FIELDS = [
        self::ACCOUNT_TYPE_GROUP_ACCOUNT => ['MasAddr', 'MasCity', 'MasZip', 'MasState']
    ];

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var DeviceCategoryRepository */
    private $deviceCategoryRepository;
    /** @var StateRepository */
    private $stateRepository;
    /** @var AccountTypeRepository */
    private $accountTypeRepository;
    /** @var CompanyRepository */
    private $companyRepository;
    /** @var  OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var  AddressTypeRepository */
    private $addressTypeRepository;
    /** @var AccountManager */
    private $accountManager;
    /** @var ContactPersonManager */
    private $contactPersonManager;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;
    /** @var  AccountRepository */
    private $accountRepository;
    /** @var  ClientTypeRepository */
    private $clientTypeRepository;
    /** @var  MunicipalityRepository */
    private $municipalityRepository;
    /** @var ContractorService */
    private $contractorService;
    /** @var ContactPersonRepository */
    private $contactPersonRepository;

    /**
     * MasLevelToAccount constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $container
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->om = $objectManager;
        $this->container = $container;

        $this->deviceCategoryRepository = $this->om->getRepository('AppBundle:DeviceCategory');
        $this->stateRepository = $this->om->getRepository('AppBundle:State');
        $this->accountRepository = $this->om->getRepository('AppBundle:Account');
        $this->accountTypeRepository = $this->om->getRepository('AppBundle:AccountType');
        $this->clientTypeRepository = $this->om->getRepository('AppBundle:ClientType');
        $this->companyRepository = $this->om->getRepository('AppBundle:Company');
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->addressTypeRepository = $this->om->getRepository('AppBundle:AddressType');
        $this->municipalityRepository = $this->om->getRepository('AppBundle:Municipality');
        $this->accountManager = $this->container->get('app.account.manager');
        $this->contactPersonManager = $this->container->get('app.contact_person.manager');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
        $this->contractorService = $this->container->get('import.contractor.service');
        $this->contactPersonRepository = $this->om->getRepository('AppBundle:ContactPerson');
    }

    /**
     * @return int
     */
    public function parseData()
    {
        $masLevelData = $this->oldDatabaseRepository->getAllTablesRowsByTableName(
            self::TABLE_MAS_LEVEL
//            " WHERE MasID IN (SELECT MasID FROM abfp.dbo.Main1 WHERE SiteMunicipalityID <> Main1.FireMunicipalityID)"
        );
        $result = $this->dataImport($masLevelData);
        unset($masLevelData);

        return $result;
    }

    /**
     * @param array $oldData
     * @return int
     */
    private function dataImport(array $oldData)
    {
        $i = 0;
        $importedIds = $this->getImportedAccountOldIds();
        foreach ($oldData as $data) {
            $company = null;
            $error = $this->importErrorMessageManager->checkMasLevelData($data);
            if ($error) {
                continue;
            }

            if (!in_array($data['MasID'], $importedIds)) {
                if (!empty($data['OrgID'])) {
                    $company = $this->companyRepository->findOneBy(['oldCompanyId' => $data['OrgID']]);
                }

                /** @var Account $account */
                $account = $this->setAccount($data, $company);
                if (!empty($data['MasConFName1']) ||
                    !empty($data['MasConLName1']) ||
                    !empty($data['MasConTitle1']) ||
                    !empty($data['MasConPhone1']) ||
                    !empty($data['MasExt']) ||
                    !empty($data['MasConCell1']) ||
                    !empty($data['MasConFax1']) ||
                    !empty($data['MasConEmail1'])
                ) {
                    /** @var ContactPerson $firstContactPerson */
                    $firstContactPerson = $this->setContactPerson($data, $company);
                    $this->setAccountContactPerson($account, $firstContactPerson, true);
                }
                if (!empty($data['MasConFName2']) ||
                    !empty($data['MasConLName2']) ||
                    !empty($data['MasConTitle2']) ||
                    !empty($data['MasConPhone2']) ||
                    !empty($data['MasExt2']) ||
                    !empty($data['MasConCell2']) ||
                    !empty($data['MasConFax2']) ||
                    !empty($data['MasConEmail2'])
                ) {
                    /** @var ContactPerson $secondContactPerson */
                    $secondContactPerson = $this->setContactPerson($data, $company, 2);
                    $this->setAccountContactPerson($account, $secondContactPerson);
                }

                $i++;

                $this->needLoad($i);
//                if ($i == 5000) {
//                    break;
//                }
            }
        }
        unset($oldData);
        unset($importedIds);

        $this->om->flush();

        return $i;
    }

    /**
     * @return array
     */
    public function getImportedAccountOldIds()
    {
        $importedIds =[];
        $importedAccountsArray = $this->accountRepository->getImportedAccount();
        foreach ($importedAccountsArray as $importedAccount) {
            $importedIds[] = $importedAccount["oldAccountId"];
        }

        return $importedIds;
    }

    /**
     * @param array $data
     * @param Company|null $company
     * @return Account
     */
    private function setAccount(array $data, ?Company $company)
    {
        if ($this->contractorService->isShouldNotEmpty(
            $data,
            self::REQUIRED_FIELDS[self::ACCOUNT_TYPE_GROUP_ACCOUNT]
        )) {
            $address = $this->setAddress($data);
        } else {
            $address = $this->setDefaultAddress();
        }

        $account = new Account();
        $account->setOldAccountId($data['MasID']);
        $account->setName($data['MasName'] ?? 'No name');
        $account->setAddress($address);
        $account->setNotes($data['Notes']);
        $account->setType($this->accountTypeRepository->findOneBy(['alias' => self::ACCOUNT_TYPE_GROUP_ACCOUNT]));
        $account->setClientType($this->clientTypeRepository->findOneBy(['alias' =>self::ACCOUNT_CLIENT_TYPE_ACTIVE]));
        $municipality = $this->municipalityRepository->findOneBy(['name' => self::DEFAULT_MUNICIPALITY]);
        $account->setMunicipality($municipality);
        $account->setCompany($company);
        $account->setOldSystemMasLevelAddress($this->setOldSystemMasLevelAddress($address));
        $this->om->persist($account);

        return $account;
    }

    /**
     * @param array $data
     * @return Address
     */
    private function setAddress(array $data)
    {
        $state = $this->stateRepository->findOneBy(['code' => strtoupper($data['MasState'])]);

        $address = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_GROUP_ACCOUNT]);

        $address->setAddressType($addressType);
        $address->setAddress($data['MasAddr']);
        $address->setCity($data['MasCity']);
        $address->setZip($data['MasZip']);
        $address->setState($state);

        $this->om->persist($address);

        return $address;
    }

    /**
     * @return Address
     */
    private function setDefaultAddress()
    {
        $state = $this->stateRepository->findOneBy(['code' => 'IL']);

        $address = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_GROUP_ACCOUNT]);

        $address->setAddressType($addressType);
        $address->setAddress('Dummy Address');
        $address->setCity('No Data in Old System');
        $address->setZip('0000');
        $address->setState($state);

        $this->om->persist($address);

        return $address;
    }

    /**
     * @param array $data
     * @param Company|null $company
     * @param int $flag 1 - first contact person 2 - second contact person in old database
     * @return ContactPerson
     */
    private function setContactPerson(array $data, ?Company $company, $flag = 1)
    {
        $contactPerson = new ContactPerson();
        $contactPerson->setFirstName($data['MasConFName'.$flag] ?? 'Property Owner');
        $contactPerson->setLastName($data['MasConLName'.$flag]);
        $contactPerson->setTitle($data['MasConTitle'.$flag]);
        $contactPerson->setPhone($data['MasConPhone'.$flag]);
        $contactPerson->setExt($flag === 2 ? $data['MasExt2'] : $data['MasExt']);
        $contactPerson->setCell($data['MasConCell'.$flag]);
        $contactPerson->setFax($data['MasConFax'.$flag]);
        $contactPerson->setEmail($data['MasConEmail'.$flag]);
        $contactPerson->setCompany($company);
        $contactPerson->setPersonalAddress($this->createEmptyAddressForCP());
        $contactPerson->setSourceEntity("MasLevel");
        $contactPerson->setSourceId($data['MasID']);
        $contactPerson->setNumContact($flag);
        $this->om->persist($contactPerson);

        return $contactPerson;
    }

    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @param bool $flag
     * @return AccountContactPerson
     */
    public function setAccountContactPerson(Account $account, ContactPerson $contactPerson, $flag = false)
    {
        $accountContactPerson = new AccountContactPerson();
        $accountContactPerson->setContactPerson($contactPerson);
        $accountContactPerson->setAccount($account);
        $accountContactPerson->setAuthorizer(true);
        if ($flag) {
            $divisions = $this->deviceCategoryRepository->findAll();
            foreach ($divisions as $division) {
                $accountContactPerson->addResponsibility($division);
            }
        }
        $accountContactPerson->setSendingAddress($this->checkAddressForACP($accountContactPerson));

        $this->om->persist($accountContactPerson);

        return $accountContactPerson;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @return Address
     */
    private function checkAddressForACP(AccountContactPerson $accountContactPerson)
    {
        /** @var ContactPerson $acpContactPerson */
        $acpContactPerson = $accountContactPerson->getContactPerson();
        /** @var Company $acpCompany */
        $acpCompany = $acpContactPerson->getCompany();
        if ($acpCompany) {
            return $acpCompany->getAddress();
        }
        return $acpContactPerson->getPersonalAddress();
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

    /**
     * @return Address
     */
    private function createEmptyAddressForCP()
    {
        $newAddress = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_PERSONAL]);
        $newAddress->setAddressType($addressType);
        $this->om->persist($newAddress);

        return $newAddress;
    }

    /**
     * @param Account $account
     * @return AccountContactPerson
     */
    public function setConflictAccountContactPerson(Account $account)
    {
        $contactPerson = $this->contactPersonRepository->findOneBy(['firstName' => self::MAILING_ADDRESS_CONFLICT]);

        $accountContactPerson = new AccountContactPerson();
        $accountContactPerson->setContactPerson($contactPerson);
        $accountContactPerson->setAccount($account);
        $accountContactPerson->setAuthorizer(true);

        $accountContactPerson->setSendingAddress($account->getOldSystemMasLevelAddress());

        $this->om->persist($accountContactPerson);

        return $accountContactPerson;
    }

    /**
     * @param Address $address
     * @return Address|null
     */
    private function setOldSystemMasLevelAddress(Address $address)
    {
        if ($address->getAddress() != self::DUMMY_ADDRESS) {
            $addressTypeOld = $this->addressTypeRepository->findOneBy(['alias'=>'old system master address']);
            $address->setAddressType($addressTypeOld);
            $this->om->persist($address);

            return $address;
        }

        return null;
    }
}
