<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\ContractorUserHistory;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ContractorUserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessingContractorUserHistoryService
{
    private const NEED_FLUSH_ON = 1000;

    /** @var  ObjectManager */
    private $om;
    /** @var  ContainerInterface */
    private $container;
    /** @var  EntityManager */
    private $entityManager;
    /** @var ContractorRepository */
    private $contractorRepository;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;
    /** @var OldDatbaseRepository */
    private $oldDatbaseRepository;

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->entityManager = $this->container->get('app.entity_manager');
        $this->contractorRepository = $this->om->getRepository('AppBundle:Contractor');
        $this->contractorUserRepository = $this->om->getRepository('AppBundle:ContractorUser');
        $this->oldDatbaseRepository = $this->container->get('import.old_database.repository');
    }

    /**
     * @return int
     */
    public function processContractorUserHistory()
    {
        $i = 0;
        $contractorUsers = $this->contractorUserRepository->getImportedContractorUser();
        if (!empty($contractorUsers)) {
            /** @var ContractorUser $contractorUser */
            foreach ($contractorUsers as $contractorUser) {
                $this->setContractorUserHistory($contractorUser);
                $i++;
                $this->needLoad($i);
            }
            $this->om->flush();
        }
        unset($contactPersonsArray);

        return $i;
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function queryProcessContractorUserHistory()
    {
        return  $this->oldDatbaseRepository->queryProcessContractorUserHistory();
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeAllContractorUserHistory()
    {
        return  $this->oldDatbaseRepository->removeAllContractorUserHistory();
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

    /**
     * @param ContractorUser $contractorUser
     */
    private function setContractorUserHistory(ContractorUser $contractorUser)
    {
        $newContractorUserHistory = new ContractorUserHistory();
        $newContractorUserHistory->setPhoto($contractorUser->getPhoto());
        $newContractorUserHistory->setSignature($contractorUser->getSignature());
        $newContractorUserHistory->setTitle($contractorUser->getTitle());
        $newContractorUserHistory->setPersonalEmail($contractorUser->getPersonalEmail());
        $newContractorUserHistory->setPersonalPhone($contractorUser->getPersonalPhone());
        $newContractorUserHistory->setWorkPhone($contractorUser->getWorkPhone());
        $newContractorUserHistory->setFax($contractorUser->getFax());
        $newContractorUserHistory->setDateSave(new \DateTime());
        $newContractorUserHistory->setUser($contractorUser->getUser());
        $newContractorUserHistory->setContractor($contractorUser->getContractor());
        $newContractorUserHistory->setOwnerEntity($contractorUser);

        $this->om->persist($newContractorUserHistory);
    }
}
