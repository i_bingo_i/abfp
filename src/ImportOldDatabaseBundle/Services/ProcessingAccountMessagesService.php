<?php
namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\MessageManager;
use AppBundle\Entity\Repository\AccountRepository;
use Doctrine\Common\Persistence\ObjectManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output;

class ProcessingAccountMessagesService
{
    private const NEED_FLUSH_ON = 1000;

    /** @var  ObjectManager */
    private $om;
    /** @var  ContainerInterface */
    private $container;
    /** @var  AccountRepository */
    private $accountRepository;
    /** @var  MessageManager */
    private $messageManager;
    /** @var  AccountManager */
    private $accountManager;
    /** @var ImportErrorMessageManager */
    private $importErrorMessageManager;

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->accountRepository = $this->om->getRepository('AppBundle:Account');
        $this->messageManager = $this->container->get('app.message.manager');
        $this->accountManager = $this->container->get('app.account.manager');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');

    }

    /**
     * @return int
     */
    public function processAccountMessage()
    {
        $i = 0;
        $accountArray = $this->accountRepository->findBy(['isMessagesProcessed' => false]);
        if (!empty($accountArray)) {
            $output = new Output\ConsoleOutput();
            $progress = new ProgressBar($output, count($accountArray));
            $progress->start();
            foreach ($accountArray as $account) {
                $this->messageManager->isChildAccountHaveOwnAuthorizer($account, false);
                if ($account->getMunicipality()) {
                    $this->accountManager->checkDefaultDepartmentChanel($account, false);
                } else {
                    $this->importErrorMessageManager->setErrorMessage('[account processing messages][AccountId:' . $account->getId() . ' ]: Municipality not found');
                }
                $this->accountManager->checkAuthorizer($account, false);
                $this->accountManager->checkMessageAuthorizerAddress($account, false);
                $this->accountManager->checkServiceFee($account, false);
                $account->setIsMessagesProcessed(true);
                $i++;
                $progress->advance();
                $this->needLoad($i);
            }
            $progress->finish();
            $this->om->flush();
        }
        unset($accountArray);

        return $i;
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

}