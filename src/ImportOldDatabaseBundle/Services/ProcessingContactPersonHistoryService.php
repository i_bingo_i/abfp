<?php
namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\EntityManager\ContactPersonHistoryManager;
use AppBundle\Entity\Repository\ContactPersonRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProcessingContactPersonHistoryService
{
    private const NEED_FLUSH_ON = 1000;

    /** @var  ObjectManager */
    private $om;
    /** @var  ContainerInterface */
    private $container;
    /** @var  ContactPersonRepository */
    private $contactPersonRepository;
    /** @var  ContactPersonHistoryManager */
    private $contactPersonHistoryManager;
    /** @var  EntityManager */
    private $entityManager;
    /** @var OldDatbaseRepository */
    private $oldDatbaseRepository;

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->entityManager = $this->container->get('app.entity_manager');
        $this->contactPersonRepository = $this->om->getRepository('AppBundle:ContactPerson');
        $this->contactPersonHistoryManager = $this->container->get('app.contact_person_history.manager');
        $this->oldDatbaseRepository = $this->container->get('import.old_database.repository');
    }

    /**
     * @return int
     */
    public function processContactPersonHistory()
    {
        $i = 0;
        $contactPersonsArray = $this->contactPersonRepository->findBy(['isHistoryProcessed' => false]);
        if (!empty($contactPersonsArray)) {
            foreach ($contactPersonsArray as $contactPerson) {
                $this->contactPersonHistoryManager->createContactPersonHistoryItem($contactPerson, 0);
                $contactPerson->setIsHistoryProcessed(true);
                $i++;
                $this->needLoad($i);
            }
            $this->om->flush();
        }
        unset($contactPersonsArray);

        return $i;
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function queryProcessContactPersonHistory()
    {
        return  $this->oldDatbaseRepository->queryProcessContactPersonHistory();
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeAllContactPersonHistory()
    {
        return  $this->oldDatbaseRepository->removeAllContactPersonHistory();
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

}