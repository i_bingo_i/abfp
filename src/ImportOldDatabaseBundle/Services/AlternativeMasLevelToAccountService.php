<?php

namespace ImportOldDatabaseBundle\Services;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\EntityManager\CompanyManager;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Entity\Repository\AddressTypeRepository;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\State;
use Doctrine\Common\Persistence\ObjectManager;
use ImportOldDatabaseBundle\Managers\ImportErrorMessageManager;
use ImportOldDatabaseBundle\Repository\OldDatbaseRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class AlternativeMasLevelToAccountService
{
    private const TABLE_MAS_LEVEL = 'abfp.dbo.MasLevel';
    private const ADDRESS_TYPE_PERSONAL = 'personal';
    private const ADDRESS_TYPE_COMPANY = 'company address';
    private const NEED_FLUSH_ON = 1000;

    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var  OldDatbaseRepository */
    private $oldDatabaseRepository;
    /** @var  StateRepository */
    private $stateRepository;
    /** @var  AddressTypeRepository */
    private $addressTypeRepository;
    /** @var  ContactPersonManager */
    private $contactPersonManager;
    /** @var  CompanyManager */
    private $companyManager;
    /** @var  CompanyRepository */
    private $companyRepository;
    /** @var ImportErrorMessageManager  */
    private $importErrorMessageManager;
    /** @var array */
    private $personalAddress;
    /** @var array */
    private $undesirableNames = [
        'current homeowner', 'current homeowner:', 'current homeowners', 'current  homeowner'
    ];

    public function __construct(
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->om = $objectManager;
        $this->container = $container;
        $this->oldDatabaseRepository = $this->container->get('import.old_database.repository');
        $this->contactPersonManager = $this->container->get('app.contact_person.manager');
        $this->companyManager = $this->container->get('app.company.manager');
        $this->stateRepository = $this->om->getRepository('AppBundle:State');
        $this->addressTypeRepository = $this->om->getRepository('AppBundle:AddressType');
        $this->companyRepository = $this->om->getRepository('AppBundle:Company');
        $this->importErrorMessageManager = $this->container->get('import.import_error_message.manager');
    }

    /**
     * @return array
     */
    public function getImportedCompaniesOldIds()
    {
        $importedIds =[];
        $importedCompaniesArray = $this->companyRepository->getImportedCompanies('MasLevel');
        foreach ($importedCompaniesArray as $importedCompany) {
            $importedIds[] = $importedCompany["oldCompanyId"];
        }

        return $importedIds;
    }

    /**
     * @return int
     */
    public function importCompanies()
    {
        $i = 0;
        $oldCompanies = $this->oldDatabaseRepository->getAllTablesRowsByTableName(self::TABLE_MAS_LEVEL);
        $importedIds = $this->getImportedCompaniesOldIds();

        foreach ($oldCompanies as $oldCompany) {
//            $error = $this->importErrorMessageManager->checkMasLevelData($oldCompany);
//            if ($error) {
//                continue;
//            }

            if (!in_array($oldCompany['MasID'], $importedIds)) {
                $newCompany = null;

                if (!$this->isCurrentHomeowner($oldCompany)) {
                    $newCompany = $this->createAndSetCompanyAddress(
                        $oldCompany,
                        $this->setCompanyTextFields($oldCompany)
                    );
                }

                if (!empty($oldCompany["MasConLName1"]) ||
                    !empty($oldCompany["MasConFName1"]) ||
                    !empty($oldCompany["MasConTitle1"]) ||
                    !empty($oldCompany["MasConPhone1"]) ||
                    !empty($oldCompany["MasExt"]) ||
                    !empty($oldCompany["MasConCell1"]) ||
                    !empty($oldCompany["MasConFax1"]) ||
                    !empty($oldCompany["MasConEmail1"])
                ) {
                    $this->createCompaniesContactPerson($oldCompany, $newCompany);
                }
                if (!empty($oldCompany["MasConLName2"]) ||
                    !empty($oldCompany["MasConFName2"]) ||
                    !empty($oldCompany["MasConTitle2"]) ||
                    !empty($oldCompany["MasConPhone2"]) ||
                    !empty($oldCompany["MasExt2"]) ||
                    !empty($oldCompany["MasConCell2"]) ||
                    !empty($oldCompany["MasConFax2"]) ||
                    !empty($oldCompany["MasConEmail2"])
                ) {
                    $this->createCompaniesContactPerson($oldCompany, $newCompany, 2);
                }

                if ($newCompany) {
                    $this->om->persist($newCompany);
                }

                $i++;
                $this->needLoad($i);
//                if ($i == 10000) {
//                    break;
//                }
            }
        }

        unset($oldData);
        unset($oldCompanies);
        unset($importedIds);
        $this->om->flush();

        return $i;
    }

    /**
     * @param \DateTime $datetime1
     * @param \DateTime $datetime2
     * @return string
     */
    public function getTimedifference(\DateTime $datetime1, \DateTime $datetime2)
    {
        $resultInterval = "Process took:";

        $interval = $datetime1->diff($datetime2);

        if ($interval->h > 0) {
            $resultInterval .= " ".$interval->h." hours";
        }
        if ($interval->i > 0) {
            $resultInterval .= " ".$interval->i." minutes";
        }
        if ($interval->s > 0) {
            $resultInterval .= " ".$interval->s." seconds";
        }

        return $resultInterval;
    }

    /**
     * @param $oldCompany
     * @return Company
     */
    private function setCompanyTextFields($oldCompany)
    {
        $orgCompany = null;
        if (!empty($oldCompany['OrgID'])) {
            $orgCompany = $this->companyRepository->findOneBy(['oldCompanyId' => $oldCompany['OrgID']]);
        }
        /** @var Company $newCompay */
        $newCompay = new Company();
        $newCompay->setName($oldCompany['MasName'] ?? 'No name');
        $newCompay->setOldCompanyId($oldCompany['MasID']);
        $newCompay->setNotes($oldCompany['Notes']);
        $newCompay->setWebsite($oldCompany['MasURL']);
        $newCompay->setParent($orgCompany);
        $newCompay->setSourceEntity('Master');
        $this->om->persist($newCompay);

        return $newCompay;
    }

    /**
     * @param $oldCompany
     * @param Company $newCompany
     * @return Company
     */
    private function createAndSetCompanyAddress($oldCompany, Company $newCompany)
    {
        /** @var Address $address */
        $newAddress = new Address();
        $state = null;
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_COMPANY]);

        if ($addressType) {
            $newAddress->setAddressType($addressType);
        }

        if ($oldCompany['MasState']) {
            /** @var State $state */
            $state = $this->stateRepository->findOneBy(['code' => strtoupper($oldCompany['MasState'])]);
        }

        if ($state) {
            $newAddress->setState($state);
        }

        $newAddress->setAddress($oldCompany['MasAddr'] ?? '');
        $newAddress->setCity($oldCompany['MasCity']);
        $newAddress->setZip($oldCompany['MasZip']);

        $this->om->persist($newAddress);

        $newCompany->setAddress($newAddress);

        $this->om->persist($newCompany);

        return $newCompany;
    }

    /**
     * @param $oldCompany
     * @param Company $newCompany
     * @param int $cpNum
     */
    private function createCompaniesContactPerson($oldCompany, ?Company $newCompany = null, $cpNum = 1)
    {
        /** @var ContactPerson $newContaPerson */
        $newContaPerson = new ContactPerson();
        $newContaPerson->setFirstName($oldCompany['MasConFName' . $cpNum] ?? 'Property Owner');
        $newContaPerson->setLastName($oldCompany['MasConLName' . $cpNum]);
        $newContaPerson->setTitle($oldCompany['MasConTitle' . $cpNum]);
        $newContaPerson->setPhone($oldCompany['MasConPhone'. $cpNum]);
        $newContaPerson->setExt($oldCompany['MasExt'.($cpNum == 1 ? '' : 2)]);
        $newContaPerson->setCell($oldCompany['MasConCell' . $cpNum]);
        $newContaPerson->setFax($oldCompany['MasConFax' . $cpNum]);
        $newContaPerson->setEmail($oldCompany['MasConEmail' . $cpNum]);
        $newContaPerson->setSourceEntity('Master');
        $newContaPerson->setPersonalAddress($this->getAddressForContactPerson($oldCompany));
        $newContaPerson->setSourceId($oldCompany['MasID']);
        $newContaPerson->setNumContact($cpNum);
        if($newCompany) {
            $newContaPerson->setCompany($newCompany);
        }

        $this->om->persist($newContaPerson);
    }

    /**
     * @param $i
     */
    private function needLoad($i)
    {
        if ($i % self::NEED_FLUSH_ON == 0 and $i!=0) {
            $this->om->flush();
        }
    }

    /**
     * @param array $oldCompany
     * @return Address
     */
    private function createEmptyAddressForCP($oldCompany = null)
    {
        $newAddress = new Address();
        $addressType = $this->addressTypeRepository->findOneBy(['alias' => self::ADDRESS_TYPE_PERSONAL]);
        $newAddress->setAddressType($addressType);

        if ($this->isCurrentHomeowner($oldCompany)) {
            /** @var State $state */
            $state = $this->stateRepository->findOneBy(['code' => strtoupper($oldCompany['MasState'])]);
            $newAddress->setState($state);
            $newAddress->setAddress($oldCompany['MasAddr'] ?? '');
            $newAddress->setCity($oldCompany['MasCity']);
            $newAddress->setZip($oldCompany['MasZip']);
        }

        $this->om->persist($newAddress);

        return $newAddress;
    }

    /**
     * @param $oldCompany
     * @return Address|array|mixed
     */
    private function getAddressForContactPerson($oldCompany)
    {
        $address = [];

        if (isset($this->personalAddress[$oldCompany['MasID']])) {
            $address = $this->personalAddress[$oldCompany['MasID']];
        } elseif ($this->isCurrentHomeowner($oldCompany)) {
            $this->personalAddress[$oldCompany['MasID']] = $address = $this->createEmptyAddressForCP($oldCompany);
        } else {
            $address = $this->createEmptyAddressForCP();
        }

        return $address;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isCurrentHomeowner($data)
    {
        return in_array(strtolower(trim($data['MasName'])), $this->undesirableNames);
    }
}