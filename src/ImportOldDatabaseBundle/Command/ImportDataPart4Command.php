<?php

namespace ImportOldDatabaseBundle\Command;

use ImportOldDatabaseBundle\Services\ContractorService;
use ImportOldDatabaseBundle\Services\MainToAccountService;
use ImportOldDatabaseBundle\Services\MasLevelToAccountService;
use ImportOldDatabaseBundle\Services\MunicipalToMunicipalityService;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use ImportOldDatabaseBundle\Services\ProcessingAccountContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountMessagesService;
use ImportOldDatabaseBundle\Services\ProcessingCompanyHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContractorUserHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingMunicipalityHistoryService;
use ImportOldDatabaseBundle\Services\TableFireToDevices;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use ImportOldDatabaseBundle\Services\OldSystemDeviceToDevices;
use ImportOldDatabaseBundle\Services\AlarmDevices;

/**
 * Class ImportDataCommand
 * @package ImportOldDatabaseBundle\Command
 */
class ImportDataPart4Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('olddb:data:importp4')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.'
            )
            ->setHelp(
                'Import (copies) a data from the old database of this project to the new one.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrgLevelToCompanyService $importCompaniesService */
        $importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');

        /** @var OldSystemDeviceToDevices $importDevicesService */
        $importDevicesService = $this->getContainer()->get('import.old_system_device.service');

        
        $finishTimeStamp = function (\DateTime $startTime, $finishMessage, $resultMessage) use ($output, $importCompaniesService) {
            $currentTime = new \DateTime('now');
            $interval = $importCompaniesService->getTimedifference($startTime, $currentTime);

            $output->writeln($currentTime->format("m/d/Y H:i:s") . ' ' . $finishMessage);
            $output->writeln($interval);
            $output->writeln($resultMessage);
            $output->writeln('');

            return $currentTime;
        };

        /**
         * Start Import
         */
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Import part 4 was started');
        $output->writeln('');


        /**
         * Import Devices
         */
        $devices = $importDevicesService->import();
        $devicesImportFinishTime = $finishTimeStamp(
            $startTime,
            "Devices Import completed!",
            "Imported: $devices devices"
        );


        /**
         * Finish Import
         */
        $finishTime = new \DateTime('now');
        $timeInterval = $importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln($finishTime->format("m/d/Y H:i:s").' All Import And Processing completed!');
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('Result: Done');
        $output->writeln('');
        $output->writeln('');
        
    }

}