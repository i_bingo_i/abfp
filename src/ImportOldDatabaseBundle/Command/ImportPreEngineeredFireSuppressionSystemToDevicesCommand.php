<?php

namespace ImportOldDatabaseBundle\Command;

use ImportOldDatabaseBundle\Services\PreEngineeredFireSuppressionSystemToDevices;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportPreEngineeredFireSuppressionSystemToDevicesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('olddb:data:ImportPreEngineeredFireSuppressionSystemToDevicesCommand')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.'
            )
            ->setHelp(
                'Import (copies) a data from the old database of this project to the new one.'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var PreEngineeredFireSuppressionSystemToDevices $importPreEngineeredFireSuppressionSystemToDevices */
        $importPreEngineeredFireSuppressionSystemToDevices = $this->getContainer()->get('import.fire_suppression_system.service');

        $finishTimeStamp = function (\DateTime $startTime, $finishMessage, $resultMessage) use ($output, $importPreEngineeredFireSuppressionSystemToDevices) {
            $currentTime = new \DateTime('now');
            $interval = $importPreEngineeredFireSuppressionSystemToDevices->getTimedifference($startTime, $currentTime);

            $output->writeln($currentTime->format("m/d/Y H:i:s") . ' ' . $finishMessage);
            $output->writeln($interval);
            $output->writeln($resultMessage);
            $output->writeln('');

            return $currentTime;
        };

        /**
         * Start Import
         */
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Import backflow dynamic fields was started');
        $output->writeln('');

        /**
         * Get Old Fire Devices Array
         */
        $oldFireDevices = $importPreEngineeredFireSuppressionSystemToDevices->getOldDevicesByQuery();
        /**
         * Import backflow dynamic fields
         */
        $fireDevices = $importPreEngineeredFireSuppressionSystemToDevices->import($oldFireDevices);
        $finishTimeStamp(
            $startTime,
            "Root Fire Devices Import completed!",
            "Imported: {$fireDevices} root fire devices"
        );

        /**
         * Finish Import
         */
        $finishTime = new \DateTime('now');
        $timeInterval = $importPreEngineeredFireSuppressionSystemToDevices->getTimedifference($startTime, $finishTime);
        $output->writeln($finishTime->format("m/d/Y H:i:s").' Import backflow dynamic fields completed!');
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('Result: Done');
        $output->writeln('');
        $output->writeln('');
    }
}
