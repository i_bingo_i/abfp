<?php

namespace ImportOldDatabaseBundle\Command;

use ImportOldDatabaseBundle\Services\ContractorService;
use ImportOldDatabaseBundle\Services\MainToAccountService;
use ImportOldDatabaseBundle\Services\MasLevelToAccountService;
use ImportOldDatabaseBundle\Services\MunicipalToMunicipalityService;
use ImportOldDatabaseBundle\Services\OldSystemDeviceToDevices;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use ImportOldDatabaseBundle\Services\ProcessingAccountContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountMessagesService;
use ImportOldDatabaseBundle\Services\ProcessingCompanyHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContractorUserHistoryService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;



/**
 * Class ImportDataCommand
 * @package ImportOldDatabaseBundle\Command
 *
 * This is Pasha's NOT REFACTORTED ConsoleCommand for Import Contractors
 * I don't understand this code
 */
class ImportTestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('olddb:test:import')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.'
            )
            ->setHelp(
                'Import (copies) a data from the old database of this project to the new one.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrgLevelToCompanyService $importCompaniesService */
        $importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');
        /** @var MunicipalToMunicipalityService $importMunicipalitiesService */
        $importMunicipalitiesService = $this->getContainer()->get('import.municipal_to_municipality.service');

        /** @var OldSystemDeviceToDevices $importDevicesService */
        $importDevicesService = $this->getContainer()->get('import.old_system_device.service');

        /** @var ProcessingAccountMessagesService $processingAccountMessageService */
        $processingAccountMessageService = $this->getContainer()->get('import.processing_account_message.service');

        $finishTimeStamp = function (\DateTime $startTime, $finishMessage, $resultMessage) use ($output, $importCompaniesService) {
            $currentTime = new \DateTime('now');
            $interval = $importCompaniesService->getTimedifference($startTime, $currentTime);

            $output->writeln($currentTime->format("m/d/Y H:i:s") . ' ' . $finishMessage);
            $output->writeln($interval);
            $output->writeln($resultMessage);
            $output->writeln('');

            return $currentTime;
        };

        /**
         * Start Import
         */
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Import was started');
        $output->writeln('');

//        /**
//         * Import Municipalities
//         */
//        $municipalities = $importMunicipalitiesService->importMunicipalities();
//        $municipalitiesImportFinishTime = new \DateTime('now');
//        $municipalitiesImportTimeInterval = $importCompaniesService->getTimedifference(
//            $startTime,
//            $municipalitiesImportFinishTime
//        );
//        $output->writeln($municipalitiesImportFinishTime->format("m/d/Y H:i:s").' Municipalities Import completed!');
//        $output->writeln($municipalitiesImportTimeInterval);
//        $output->writeln('Processed: '.$municipalities." municipalities");
//        $output->writeln('');

//        $devices = $importDevicesService->import();
//        $devicesImportFinishTime = $finishTimeStamp(
//            $startTime,
//            "Devices Import completed!",
//            "Imported: $devices devices"
//        );

        /**
         * Processing Account Messages
         */
        $accountProcessedMessage = $processingAccountMessageService->processAccountMessage();
        $accountProcessedMessageFinishTime = new \DateTime('now');
        $accountProcessedMessageInterval = $importCompaniesService->getTimedifference(
            $startTime,
            $accountProcessedMessageFinishTime
        );
        $output->writeln(
            $accountProcessedMessageFinishTime->format("m/d/Y H:i:s").' Accounts Messages Processing completed!'
        );
        $output->writeln($accountProcessedMessageInterval);
        $output->writeln('Processed: '.$accountProcessedMessage." accounts");
        $output->writeln('');

        /**
         * Finish Import
         */
        /** @var \DateTime $finishTime */
        $finishTime = new \DateTime('now');
        $timeInterval = $importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln($finishTime->format("m/d/Y H:i:s").' All Import And Processing completed!');
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('Result: Was processed ' . $accountProcessedMessage. " accounts");
    }
}
