<?php

namespace ImportOldDatabaseBundle\Command;

use ImportOldDatabaseBundle\Services\ContractorService;
use ImportOldDatabaseBundle\Services\MainToAccountService;
use ImportOldDatabaseBundle\Services\MasLevelToAccountService;
use ImportOldDatabaseBundle\Services\MunicipalToMunicipalityService;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use ImportOldDatabaseBundle\Services\ProcessingAccountContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountMessagesService;
use ImportOldDatabaseBundle\Services\ProcessingCompanyHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContractorUserHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingMunicipalityHistoryService;
use ImportOldDatabaseBundle\Services\TableFireToDevices;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use ImportOldDatabaseBundle\Services\OldSystemDeviceToDevices;
use ImportOldDatabaseBundle\Services\AlarmDevices;

/**
 * Class ImportDataCommand
 * @package ImportOldDatabaseBundle\Command
 */
class ImportDataPart1Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('olddb:data:importp1')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.'
            )
            ->setHelp(
                'Import (copies) a data from the old database of this project to the new one.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrgLevelToCompanyService $importCompaniesService */
        $importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');

        /** @var ProcessingCompanyHistoryService $processingCompanyHistoryService */
        $processingCompanyHistoryService = $this->getContainer()->get('import.processing_company_history.service');

        /** @var MunicipalToMunicipalityService $importMunicipalityService */
        $importMunicipalityService = $this->getContainer()->get("import.municipal_to_municipality.service");
        /** @var ContractorService $contractorService */
        $contractorService = $this->getContainer()->get('import.contractor.service');

        $finishTimeStamp = function (\DateTime $startTime, $finishMessage, $resultMessage) use ($output, $importCompaniesService) {
            $currentTime = new \DateTime('now');
            $interval = $importCompaniesService->getTimedifference($startTime, $currentTime);

            $output->writeln($currentTime->format("m/d/Y H:i:s") . ' ' . $finishMessage);
            $output->writeln($interval);
            $output->writeln($resultMessage);
            $output->writeln('');

            return $currentTime;
        };

        /**
         * Start Import
         */
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Import part 1 was started');
        $output->writeln('');

        /**
         * Import Companies
         */
        $companies = $importCompaniesService->importCompanies();
        $companiesImportFinishTime = $finishTimeStamp(
            $startTime,
            "Companies Import completed!",
            "Processed: $companies companies"
        );

        /**
         * Processing Company History
         */
        $companiesHistoryProcessed =$processingCompanyHistoryService->processCompanyHistory();
        $companiesProcessingFinishFinishTime = new \DateTime('now');
        $companiesPHTimeInterval = $importCompaniesService->getTimedifference(
            $companiesImportFinishTime,
            $companiesProcessingFinishFinishTime
        );
        $output->writeln(
            $companiesProcessingFinishFinishTime->format("m/d/Y H:i:s").' Companies History Processing completed!'
        );
        $output->writeln($companiesPHTimeInterval);
        $output->writeln('Processed: '.$companiesHistoryProcessed." companies");
        $output->writeln('');

        /**
         * Import Contractor and ContractorUser
         */
        $contractor = $contractorService->parseData();
        $contractorFinishTime = new \DateTime('now');
        $contractorTimeInterval = $importCompaniesService->getTimedifference(
            $companiesProcessingFinishFinishTime,
            $contractorFinishTime
        );
        $output->writeln($contractorFinishTime->format("m/d/Y H:i:s").' Contractor Import completed!');
        $output->writeln($contractorTimeInterval);
        $output->writeln('Processed: '.$contractor." contractor");
        $output->writeln('');

        /**
         * Import Municipality
         */
        $municipality = $importMunicipalityService->importMunicipalities();
        $municipalitiesImportFinishTime = new \DateTime('now');
        $municipalitiesImportInterval = $importCompaniesService->getTimedifference(
            $companiesProcessingFinishFinishTime,
            $municipalitiesImportFinishTime
        );

        $output->writeln($municipalitiesImportFinishTime->format("m/d/Y H:i:s").' Municipalities Import completed!');
        $output->writeln($municipalitiesImportInterval);
        $output->writeln('Processed: '.$municipality." municipalities");
        $output->writeln('');


        /**
         * Finish Import
         */
        $finishTime = new \DateTime('now');
        $timeInterval = $importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln($finishTime->format("m/d/Y H:i:s").' All Import And Processing completed!');
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('Result: Done');
        $output->writeln('');
        $output->writeln('');

    }

}