<?php

namespace ImportOldDatabaseBundle\Command;

use ImportOldDatabaseBundle\Services\ContractorService;
use ImportOldDatabaseBundle\Services\MainToAccountService;
use ImportOldDatabaseBundle\Services\MasLevelToAccountService;
use ImportOldDatabaseBundle\Services\MunicipalToMunicipalityService;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use ImportOldDatabaseBundle\Services\ProcessingAccountContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountMessagesService;
use ImportOldDatabaseBundle\Services\ProcessingCompanyHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContractorUserHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingMunicipalityHistoryService;
use ImportOldDatabaseBundle\Services\TableFireToDevices;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use ImportOldDatabaseBundle\Services\OldSystemDeviceToDevices;
use ImportOldDatabaseBundle\Services\AlarmDevices;

/**
 * Class ImportDataCommand
 * @package ImportOldDatabaseBundle\Command
 *
 * This is Pasha's NOT REFACTORTED ConsoleCommand for Import Contractors
 * I don't understand this code
 */
class ImportOldDatabaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('olddb:data:import')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.'
            )
            ->setHelp(
                'Import (copies) a data from the old database of this project to the new one.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrgLevelToCompanyService $importCompaniesService */
        $importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');
        /** @var MasLevelToAccountService $importGroupAccountService */
        $importGroupAccountService = $this->getContainer()->get('import.mas_level_to_account.service');
        /** @var MainToAccountService $importSiteAccountService */
        $importSiteAccountService = $this->getContainer()->get('import.main_to_account.service');
        /** @var ProcessingCompanyHistoryService $processingCompanyHistoryService */
        $processingCompanyHistoryService = $this->getContainer()->get('import.processing_company_history.service');
        /** @var ProcessingAccountHistoryService $processingAccountHistoryService */
        $processingAccountHistoryService = $this->getContainer()->get('import.processing_account_history.service');
        /** @var ProcessingAccountMessagesService $processingAccountMessageService */
        $processingAccountMessageService = $this->getContainer()->get('import.processing_account_message.service');
        /** @var ProcessingContactPersonHistoryService $processingContactPersonHistoryService */
        $processingContactPersonHistoryService = $this->getContainer()->get(
            'import.processing_contact_person_history.service'
        );
        /** @var ProcessingAccountContactPersonHistoryService $processingACPHistoryService */
        $processingACPHistoryService = $this->getContainer()->get(
            'import.processing_account_contact_person_history.service'
        );
        /** @var ContractorService $contractorService */
        $contractorService = $this->getContainer()->get('import.contractor.service');
        /** @var ProcessingContractorUserHistoryService $ProcessingContractorUserHistoryService */
        $processingContractorUserHistoryService = $this->getContainer()->get(
            'import.processing_contractor_user_history.service'
        );
        /** @var MunicipalToMunicipalityService $importMunicipalityService */
        $importMunicipalityService = $this->getContainer()->get("import.municipal_to_municipality.service");
        /** @var ProcessingMunicipalityHistoryService $processingMunicipalityHistoryService */
        $processingMunicipalityHistoryService  = $this->getContainer()->get("import.municipality_history.service");

        /** @var OldSystemDeviceToDevices $importDevicesService */
        $importDevicesService = $this->getContainer()->get('import.old_system_device.service');
        /** @var TableFireToDevices $importDevicesService */
        $importFireDevicesService = $this->getContainer()->get('import.old_system_fire_device.service');
        /** @var AlarmDevices $importAlarmService */
        $importAlarmService = $this->getContainer()->get('import.alarm_device.service');

        $finishTimeStamp = function (\DateTime $startTime, $finishMessage, $resultMessage) use ($output, $importCompaniesService) {
            $currentTime = new \DateTime('now');
            $interval = $importCompaniesService->getTimedifference($startTime, $currentTime);

            $output->writeln($currentTime->format("m/d/Y H:i:s") . ' ' . $finishMessage);
            $output->writeln($interval);
            $output->writeln($resultMessage);
            $output->writeln('');

            return $currentTime;
        };

        /**
         * Start Import
         */
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Import was started');
        $output->writeln('');

//        /**
//         * Import Companies
//         */
//        $companies = $importCompaniesService->importCompanies();
//        $companiesImportFinishTime = $finishTimeStamp(
//            $startTime,
//            "Companies Import completed!",
//            "Processed: $companies companies"
//        );
//
//        /**
//         * Processing Company History
//         */
//        $companiesHistoryProcessed =$processingCompanyHistoryService->processCompanyHistory();
//        $companiesProcessingFinishFinishTime = new \DateTime('now');
//        $companiesPHTimeInterval = $importCompaniesService->getTimedifference(
//            $companiesImportFinishTime,
//            $companiesProcessingFinishFinishTime
//        );
//        $output->writeln(
//            $companiesProcessingFinishFinishTime->format("m/d/Y H:i:s").' Companies History Processing completed!'
//        );
//        $output->writeln($companiesPHTimeInterval);
//        $output->writeln('Processed: '.$companiesHistoryProcessed." companies");
//        $output->writeln('');
//
//        /**
//         * Import Contractor and ContractorUser
//         */
//        $contractor = $contractorService->parseData();
//        $contractorFinishTime = new \DateTime('now');
//        $contractorTimeInterval = $importCompaniesService->getTimedifference(
//            $companiesProcessingFinishFinishTime,
//            $contractorFinishTime
//        );
//        $output->writeln($contractorFinishTime->format("m/d/Y H:i:s").' Contractor Import completed!');
//        $output->writeln($contractorTimeInterval);
//        $output->writeln('Processed: '.$contractor." contractor");
//        $output->writeln('');
//
//        /**
//         * Import Municipality
//         */
//        $municipality = $importMunicipalityService->importMunicipalities();
//        $municipalitiesImportFinishTime = new \DateTime('now');
//        $municipalitiesImportInterval = $importCompaniesService->getTimedifference(
//            $companiesProcessingFinishFinishTime,
//            $municipalitiesImportFinishTime
//        );
//
//        $output->writeln($municipalitiesImportFinishTime->format("m/d/Y H:i:s").' Municipalities Import completed!');
//        $output->writeln($municipalitiesImportInterval);
//        $output->writeln('Processed: '.$municipality." municipalities");
//        $output->writeln('');
//
//        /**
//         * Processing Municipalities History
//         */
//        $municipalitiesHistoryProcessed = $processingMunicipalityHistoryService->processMunicipalityHistory();
//        $municipalitiesProcessingFinishFinishTime = new \DateTime('now');
//        $municipalitiesTimeInterval = $importCompaniesService->getTimedifference(
//            $municipalitiesImportFinishTime,
//            $municipalitiesProcessingFinishFinishTime
//        );
//        $output->writeln(
//            $municipalitiesProcessingFinishFinishTime->format("m/d/Y H:i:s").
//            ' Municipalities History Processing completed!'
//        );
//        $output->writeln($municipalitiesTimeInterval);
//        $output->writeln('Processed: '.$municipalitiesHistoryProcessed." municipalities");
//        $output->writeln('');
//        /**
//         * Import Group Accounts
//         */
//        $groupAccounts = $importGroupAccountService->parseData();
//        $groupAccountFinishTime = new \DateTime('now');
//        $groupAccountTimeInterval = $importCompaniesService->getTimedifference(
//            $municipalitiesProcessingFinishFinishTime,
//            $groupAccountFinishTime
//        );
//        $output->writeln($groupAccountFinishTime->format("m/d/Y H:i:s").' Group Accounts Import completed!');
//        $output->writeln($groupAccountTimeInterval);
//        $output->writeln('Processed: '.$groupAccounts." group accounts");
//        $output->writeln('');
//
//        /**
//         * Import Site Accounts
//         */
//        $siteAccounts = $importSiteAccountService->parseData();
//        $siteAccountFinishTime = new \DateTime('now');
//        $siteAccountTimeInterval = $importCompaniesService->getTimedifference(
//            $groupAccountFinishTime,
//            $siteAccountFinishTime
//        );
//        $output->writeln($siteAccountFinishTime->format("m/d/Y H:i:s").' Site Accounts Import completed!');
//        $output->writeln($siteAccountTimeInterval);
//        $output->writeln('Processed: '.$siteAccounts." site accounts");
//        $output->writeln('');
//
//        /**
//         * Processing Account History
//         */
//        $accountHistoryProcessed = $processingAccountHistoryService->queryProcessAccountHistory();
//        $accountProcessingFinishTime = new \DateTime('now');
//        $accountProcessingFinishInterval = $importCompaniesService->getTimedifference(
//            $siteAccountFinishTime,
//            $accountProcessingFinishTime
//        );
//        $output->writeln($accountProcessingFinishTime->format("m/d/Y H:i:s").' Accounts History Processing completed!');
//        $output->writeln($accountProcessingFinishInterval);
//        $output->writeln('Processed: '.$accountHistoryProcessed." accounts");
//        $output->writeln('');
//
//        /**
//         * Processsing ContactPerson History
//         */
//        $cpHistoryProcessed = $processingContactPersonHistoryService->queryProcessContactPersonHistory();
//        $cpProcessingFinishTime = new \DateTime('now');
//        $cpProcessingFinishInterval = $importCompaniesService->getTimedifference(
//            $accountProcessingFinishTime,
//            $cpProcessingFinishTime
//        );
//        $output->writeln(
//            $cpProcessingFinishTime->format("m/d/Y H:i:s").' Contact Persons History Processing completed!'
//        );
//        $output->writeln($cpProcessingFinishInterval);
//        $output->writeln('Processed: '.$cpHistoryProcessed." contact persons");
//        $output->writeln('');
//
//        /**
//         * Processsing AccountContactPerson (Authorizer) History
//         */
//        $acpHistoryProcessed = $processingACPHistoryService->queryProcessAccountContactPersonHistory();
//        $acpProcessingFinishTime = new \DateTime('now');
//        $acpProcessingFinishInterval = $importCompaniesService->getTimedifference(
//            $cpProcessingFinishTime,
//            $acpProcessingFinishTime
//        );
//        $output->writeln(
//            $acpProcessingFinishTime->format("m/d/Y H:i:s").' Contact Persons History Processing completed!'
//        );
//        $output->writeln($acpProcessingFinishInterval);
//        $output->writeln('Processed: '.$acpHistoryProcessed." account contact persons (authorizers)");
//        $output->writeln('');
//
//        /**
//         * Processing Contractor User History
//         */
//        $puHistoryProcessed = $processingContractorUserHistoryService->queryProcessContractorUserHistory();
//        $puProcessingFinishTime = new \DateTime('now');
//        $puProcessingFinishInterval = $importCompaniesService->getTimedifference(
//            $acpProcessingFinishTime,
//            $puProcessingFinishTime
//        );
//        $output->writeln(
//            $puProcessingFinishTime->format("m/d/Y H:i:s").' Contractor User History Processing completed!'
//        );
//        $output->writeln($puProcessingFinishInterval);
//        $output->writeln('Processed: '.$puHistoryProcessed." contractor user");
//        $output->writeln('');
//
//        /**
//         * Processing Mixed Municipality Departments Channels
//         */
//        $mixedMunicipalitiess = $importMunicipalityService->addChannelsForMixedMunicipality();
//        $mixedMunicipalitiessFinishTime = new \DateTime('now');
//        $mixedMunicipalitiessInterval = $importCompaniesService->getTimedifference(
//            $puProcessingFinishTime,
//            $mixedMunicipalitiessFinishTime
//        );
//        $output->writeln(
//            $mixedMunicipalitiessFinishTime->format("m/d/Y H:i:s").
//            ' Processing Mixed Municipalities Departments Channels completed!'
//        );
//        $output->writeln($mixedMunicipalitiessInterval);
//        $output->writeln('Processed: '.$mixedMunicipalitiess." Mixed Municipalities");
//        $output->writeln('');
//        /**
//         * Import Devices
//         */
//        $devices = $importDevicesService->import();
//        $devicesImportFinishTime = $finishTimeStamp(
//            $startTime,
//            "Devices Import completed!",
//            "Imported: $devices devices"
//        );

//        /**
//         * Get Old Fire Devices Array
//         */
//            $oldFireDevices = $importFireDevicesService->getOldDevicesByQuery();
//        /**
//         * Import root Fire Devices
//         */
//        $fireDevices = $importFireDevicesService->import($oldFireDevices);
//        $rootFireDevicesImportFinishTime = $finishTimeStamp(
//            $startTime,
//            "Root Fire Devices Import completed!",
//            "Imported: $fireDevices root fire devices"
//        );
//
//        /**
//         * Import sub Fire Devices
//         */
//        $subFireDevices = $importFireDevicesService->import($oldFireDevices, true);
//        $subFireDevicesImportFinishTime = $finishTimeStamp(
//            $rootFireDevicesImportFinishTime,
//            "Sub Fire Devices Import completed!",
//            "Imported: $subFireDevices sub fire devices"
//        );

        /**
         * Import Alarm Devices
         */
        $alarmDevicesCount = $importAlarmService->import();
        $alarmDevicesImportFinishTime = $finishTimeStamp(
            $startTime,
            "Alarm Devices Import completed!",
            "Imported: {$alarmDevicesCount['device']} alarm devices and {$alarmDevicesCount['subDevice']} alarm panels"
        );

//        /**
//         * Processing Account Messages (yellow zlp)
//         */
//        $accountProcessedMessage = $processingAccountMessageService->processAccountMessage();
//        $accountProcessedMessageFinishTime = new \DateTime('now');
//        $accountProcessedMessageInterval = $importCompaniesService->getTimedifference(
//            $startTime,
//            $accountProcessedMessageFinishTime
//        );
//        $output->writeln(
//            $accountProcessedMessageFinishTime->format("m/d/Y H:i:s").' Accounts Messages Processing completed!'
//        );
//        $output->writeln($accountProcessedMessageInterval);
//        $output->writeln('Processed: '.$accountProcessedMessage." accounts");
//        $output->writeln('');


//        /**
//         * Finish Import
//         */
//        /** @var \DateTime $finishTime */
//        $finishTime = new \DateTime('now');
//        $timeInterval = $importCompaniesService->getTimedifference($startTime, $finishTime);
//        $output->writeln($finishTime->format("m/d/Y H:i:s").' All Import And Processing completed!');
//        $output->writeln('');
//        $output->writeln($timeInterval);
//        $output->writeln(
//            'Result: Was imported ' .
//            $companies. " companies, ".
//            $groupAccounts." group accounts, ".
//            $siteAccounts. ' site accounts'
//        );
    }
}
