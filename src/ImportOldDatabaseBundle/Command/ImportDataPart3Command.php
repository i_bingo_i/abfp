<?php

namespace ImportOldDatabaseBundle\Command;

use ImportOldDatabaseBundle\Services\ContractorService;
use ImportOldDatabaseBundle\Services\MainToAccountService;
use ImportOldDatabaseBundle\Services\MasLevelToAccountService;
use ImportOldDatabaseBundle\Services\MunicipalToMunicipalityService;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use ImportOldDatabaseBundle\Services\ProcessingAccountContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountMessagesService;
use ImportOldDatabaseBundle\Services\ProcessingCompanyHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContractorUserHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingMunicipalityHistoryService;
use ImportOldDatabaseBundle\Services\TableFireToDevices;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use ImportOldDatabaseBundle\Services\OldSystemDeviceToDevices;
use ImportOldDatabaseBundle\Services\AlarmDevices;

/**
 * Class ImportDataCommand
 * @package ImportOldDatabaseBundle\Command
 */
class ImportDataPart3Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('olddb:data:importp3')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.'
            )
            ->setHelp(
                'Import (copies) a data from the old database of this project to the new one.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrgLevelToCompanyService $importCompaniesService */
        $importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');

        /** @var ProcessingContactPersonHistoryService $processingContactPersonHistoryService */
        $processingContactPersonHistoryService = $this->getContainer()->get(
            'import.processing_contact_person_history.service'
        );
        /** @var ProcessingAccountContactPersonHistoryService $processingACPHistoryService */
        $processingACPHistoryService = $this->getContainer()->get(
            'import.processing_account_contact_person_history.service'
        );
        /** @var ProcessingContractorUserHistoryService $ProcessingContractorUserHistoryService */
        $processingContractorUserHistoryService = $this->getContainer()->get(
            'import.processing_contractor_user_history.service'
        );
        /** @var MunicipalToMunicipalityService $importMunicipalityService */
        $importMunicipalityService = $this->getContainer()->get("import.municipal_to_municipality.service");
        /** @var ProcessingMunicipalityHistoryService $processingMunicipalityHistoryService */
        $processingMunicipalityHistoryService  = $this->getContainer()->get("import.municipality_history.service");

        $finishTimeStamp = function (\DateTime $startTime, $finishMessage, $resultMessage) use ($output, $importCompaniesService) {
            $currentTime = new \DateTime('now');
            $interval = $importCompaniesService->getTimedifference($startTime, $currentTime);

            $output->writeln($currentTime->format("m/d/Y H:i:s") . ' ' . $finishMessage);
            $output->writeln($interval);
            $output->writeln($resultMessage);
            $output->writeln('');

            return $currentTime;
        };

        /**
         * Start Import
         */
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Import part 3 was started');
        $output->writeln('');


        /**
         * Processing Municipalities History
         */
        $municipalitiesHistoryProcessed = $processingMunicipalityHistoryService->processMunicipalityHistory();
        $municipalitiesProcessingFinishFinishTime = new \DateTime('now');
        $municipalitiesTimeInterval = $importCompaniesService->getTimedifference(
            $startTime,
            $municipalitiesProcessingFinishFinishTime
        );
        $output->writeln(
            $municipalitiesProcessingFinishFinishTime->format("m/d/Y H:i:s").
            ' Municipalities History Processing completed!'
        );
        $output->writeln($municipalitiesTimeInterval);
        $output->writeln('Processed: '.$municipalitiesHistoryProcessed." municipalities");
        $output->writeln('');

        /**
         * Processing ContactPerson History
         */
        $cpHistoryProcessed = $processingContactPersonHistoryService->queryProcessContactPersonHistory();
        $cpProcessingFinishTime = new \DateTime('now');
        $cpProcessingFinishInterval = $importCompaniesService->getTimedifference(
            $municipalitiesProcessingFinishFinishTime,
            $cpProcessingFinishTime
        );
        $output->writeln(
            $cpProcessingFinishTime->format("m/d/Y H:i:s").' Contact Persons History Processing completed!'
        );
        $output->writeln($cpProcessingFinishInterval);
        $output->writeln('Processed: '.$cpHistoryProcessed." contact persons");
        $output->writeln('');

        /**
         * Processing AccountContactPerson (Authorizer) History
         */
        $acpHistoryProcessed = $processingACPHistoryService->queryProcessAccountContactPersonHistory();
        $acpProcessingFinishTime = new \DateTime('now');
        $acpProcessingFinishInterval = $importCompaniesService->getTimedifference(
            $cpProcessingFinishTime,
            $acpProcessingFinishTime
        );
        $output->writeln(
            $acpProcessingFinishTime->format("m/d/Y H:i:s").' Contact Persons History Processing completed!'
        );
        $output->writeln($acpProcessingFinishInterval);
        $output->writeln('Processed: '.$acpHistoryProcessed." account contact persons (authorizers)");
        $output->writeln('');

        /**
         * Processing Contractor User History
         */
        $puHistoryProcessed = $processingContractorUserHistoryService->queryProcessContractorUserHistory();
        $puProcessingFinishTime = new \DateTime('now');
        $puProcessingFinishInterval = $importCompaniesService->getTimedifference(
            $acpProcessingFinishTime,
            $puProcessingFinishTime
        );
        $output->writeln(
            $puProcessingFinishTime->format("m/d/Y H:i:s").' Contractor User History Processing completed!'
        );
        $output->writeln($puProcessingFinishInterval);
        $output->writeln('Processed: '.$puHistoryProcessed." contractor user");
        $output->writeln('');

        /**
         * Processing Mixed Municipality Departments Channels
         */
        $mixedMunicipalitiess = $importMunicipalityService->addChannelsForMixedMunicipality();
        $mixedMunicipalitiessFinishTime = new \DateTime('now');
        $mixedMunicipalitiessInterval = $importCompaniesService->getTimedifference(
            $puProcessingFinishTime,
            $mixedMunicipalitiessFinishTime
        );
        $output->writeln(
            $mixedMunicipalitiessFinishTime->format("m/d/Y H:i:s").
            ' Processing Mixed Municipalities Departments Channels completed!'
        );
        $output->writeln($mixedMunicipalitiessInterval);
        $output->writeln('Processed: '.$mixedMunicipalitiess." Mixed Municipalities");
        $output->writeln('');


        /**
         * Finish Import
         */
        $finishTime = new \DateTime('now');
        $timeInterval = $importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln($finishTime->format("m/d/Y H:i:s").' All Import And Processing completed!');
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('Result: Done');
        $output->writeln('');
        $output->writeln('');
        
    }

}