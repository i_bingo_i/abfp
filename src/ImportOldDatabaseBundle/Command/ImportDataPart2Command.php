<?php

namespace ImportOldDatabaseBundle\Command;

use ImportOldDatabaseBundle\Services\ContractorService;
use ImportOldDatabaseBundle\Services\MainToAccountService;
use ImportOldDatabaseBundle\Services\MasLevelToAccountService;
use ImportOldDatabaseBundle\Services\MunicipalToMunicipalityService;
use ImportOldDatabaseBundle\Services\OrgLevelToCompanyService;
use ImportOldDatabaseBundle\Services\ProcessingAccountContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingAccountMessagesService;
use ImportOldDatabaseBundle\Services\ProcessingCompanyHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContactPersonHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingContractorUserHistoryService;
use ImportOldDatabaseBundle\Services\ProcessingMunicipalityHistoryService;
use ImportOldDatabaseBundle\Services\TableFireToDevices;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use ImportOldDatabaseBundle\Services\OldSystemDeviceToDevices;
use ImportOldDatabaseBundle\Services\AlarmDevices;

/**
 * Class ImportDataCommand
 * @package ImportOldDatabaseBundle\Command
 */
class ImportDataPart2Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('olddb:data:importp2')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.'
            )
            ->setHelp(
                'Import (copies) a data from the old database of this project to the new one.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrgLevelToCompanyService $importCompaniesService */
        $importCompaniesService = $this->getContainer()->get('import.org_level_to_company.service');
        /** @var MasLevelToAccountService $importGroupAccountService */
        $importGroupAccountService = $this->getContainer()->get('import.mas_level_to_account.service');
        /** @var MainToAccountService $importSiteAccountService */
        $importSiteAccountService = $this->getContainer()->get('import.main_to_account.service');
        /** @var ProcessingAccountHistoryService $processingAccountHistoryService */
        $processingAccountHistoryService = $this->getContainer()->get('import.processing_account_history.service');

        $finishTimeStamp = function (\DateTime $startTime, $finishMessage, $resultMessage) use ($output, $importCompaniesService) {
            $currentTime = new \DateTime('now');
            $interval = $importCompaniesService->getTimedifference($startTime, $currentTime);

            $output->writeln($currentTime->format("m/d/Y H:i:s") . ' ' . $finishMessage);
            $output->writeln($interval);
            $output->writeln($resultMessage);
            $output->writeln('');

            return $currentTime;
        };

        /**
         * Start Import
         */
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Import part 2 was started');
        $output->writeln('');

        /**
         * Import Group Accounts
         */
        $groupAccounts = $importGroupAccountService->parseData();
        $groupAccountFinishTime = new \DateTime('now');
        $groupAccountTimeInterval = $importCompaniesService->getTimedifference(
            $startTime,
            $groupAccountFinishTime
        );
        $output->writeln($groupAccountFinishTime->format("m/d/Y H:i:s").' Group Accounts Import completed!');
        $output->writeln($groupAccountTimeInterval);
        $output->writeln('Processed: '.$groupAccounts." group accounts");
        $output->writeln('');

        /**
         * Import Site Accounts
         */
        $siteAccounts = $importSiteAccountService->parseData();
        $siteAccountFinishTime = new \DateTime('now');
        $siteAccountTimeInterval = $importCompaniesService->getTimedifference(
            $groupAccountFinishTime,
            $siteAccountFinishTime
        );
        $output->writeln($siteAccountFinishTime->format("m/d/Y H:i:s").' Site Accounts Import completed!');
        $output->writeln($siteAccountTimeInterval);
        $output->writeln('Processed: '.$siteAccounts." site accounts");
        $output->writeln('');

        /**
         * Processing Account History
         */
        $accountHistoryProcessed = $processingAccountHistoryService->queryProcessAccountHistory();
        $accountProcessingFinishTime = new \DateTime('now');
        $accountProcessingFinishInterval = $importCompaniesService->getTimedifference(
            $siteAccountFinishTime,
            $accountProcessingFinishTime
        );
        $output->writeln($accountProcessingFinishTime->format("m/d/Y H:i:s").' Accounts History Processing completed!');
        $output->writeln($accountProcessingFinishInterval);
        $output->writeln('Processed: '.$accountHistoryProcessed." accounts");
        $output->writeln('');


        /**
         * Finish Import
         */
        $finishTime = new \DateTime('now');
        $timeInterval = $importCompaniesService->getTimedifference($startTime, $finishTime);
        $output->writeln($finishTime->format("m/d/Y H:i:s").' All Import And Processing completed!');
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('Result: Done');
        $output->writeln('');
        $output->writeln('');
        
    }

}