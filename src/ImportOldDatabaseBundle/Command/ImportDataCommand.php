<?php

namespace ImportOldDatabaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Class ImportDataCommand
 * @package ImportOldDatabaseBundle\Command
 *
 * This is Pasha's NOT REFACTORTED ConsoleCommand for Import Contractors
 * I don't understand this code
 */
class ImportDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oldolddb:olddata:oldimport')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.
                
                Options:
                    --force -f prevents asking a question before command executing.'
            )
            ->setHelp(
                'Imports (copies) a data from the old database of this project to the new one.'
            )
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Prevents asking a question before command executing.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //dialog with user if "force" option has not been transfered
        if ($input->getOption('force') === false) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion(
                'Would you like to import data from the old database to the new one? (y/n, default: n) ',
                false
            );

            if (!$helper->ask($input, $output, $question)) {
                return;
            }
        }

        //get begin execution time
        $beginTime = microtime(true);

        //get entity managers for working with old and new databases
        $newEm = $this->getContainer()->get('doctrine')->getManager('default'); //to insert data into
        $oldEm = $this->getContainer()->get('doctrine')->getManager('olddb'); //to select data from

        //get connection handle for old database
        $oldConnection = $oldEm->getConnection();

        //get list of old database tables
        //Part of condition "AND TABLE_NAME IN('Contractor')" is temporary
        $sqlOldDBTablesList = "SELECT ".
            "TABLE_NAME FROM abfp.INFORMATION_SCHEMA.TABLES ".
            "WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME IN('Contractor')";
        $oldStatement = $oldConnection->prepare($sqlOldDBTablesList);
        $oldStatement->execute();
        $oldDBTables = $oldStatement->fetchAll();

        foreach ($oldDBTables as $oldDBTable) {
            foreach ($oldDBTable as $from) {
                //select from old database table
                $oldStatement = $oldConnection->prepare('SELECT * FROM dbo.'.$from);
                $oldStatement->execute();
                $oldResults = $oldStatement->fetchAll();

                //get new entities names and intersect it with considered entities names
                $newEntitiesMetadata = $newEm->getMetadataFactory()->getAllMetadata();
                $newEntitiesNames = [];

                /* TODO: add new entity name to this array,
                   whenever you add a new key in the second dimension of array,
                   returned by $this->prepareImportDataToNewEntity() function
                */
                $consideredEntitiesNames = [
                    '\AppBundle\Entity\Contractor',
                    '\AppBundle\Entity\ContractorUser',
                ]; //spike solution.

                foreach ($newEntitiesMetadata as $metadataItem) {
                    $newEntitiesNames[] = "\\".$metadataItem->rootEntityName;
                }
                $newEntitiesNames = array_intersect($consideredEntitiesNames, $newEntitiesNames);

                //insert into new database table
                foreach ($oldResults as $oldResult) {
                    foreach ($newEntitiesNames as $newEntityName) {
                        $newEntity = new $newEntityName();
                        $newEntitySettings = $this->prepareImportDataToNewEntity(
                            $oldResult,
                            $newEm,
                            $from,
                            $newEntityName
                        );

                        foreach ($newEntitySettings as $methodName => $methodSetting) {
                            $newEntity->$methodName($methodSetting);
                        }

                        $newEm->persist($newEntity);
                        $newEm->flush();
                    }
                }

            }
        }

        //output to console
        $output->writeln('Data has been successfully imported!');

        //get end time of execution, check and output execution time
        $endTime = microtime(true);
        $execTime = $endTime - $beginTime;
        $output->writeln('Data import had been being executed for '.$execTime.' sec.');
    }

    private function prepareImportDataToNewEntity(
        array $importingData,
        ObjectManager $newEntityManager,
        ?string $oldTableName = null,
        ?string $newEntityName = null
    ) : array {
        //array we are going to return
        $resultArray = [
            'Contractor' => [
                '\AppBundle\Entity\Contractor' => [
                    'setName' => $importingData['ContrCompName'],
                    'setWebsite' => $importingData['RegCompURL'],

                    'setStateLicenseNumber' =>
                        (!empty($importingData['ContrStateLic'])) ?
                            $importingData['ContrStateLic'] :
                            rand(100000000, 999999999),

                    'setStateLicenseExpirationDate' => new \DateTime(),

                    'setCoiExpiration' => (!empty($importingData['CoiExpiration'])) ?
                        \DateTime::createFromFormat('Y-m-d', $importingData['CoiExpiration']) :
                        null,

                    'setLogo' => $importingData['ContrLogo'],
                    'setDateCreate' => new \DateTime(),
                    'setDateUpdate' => new \DateTime(),
                    'setAddress' => $this->getEntity(
                        '\AppBundle\Entity\Address',
                        $newEntityManager,
                        [
                            'address' => $importingData['ContrAddr'],
                            'city' => $importingData['ContrCity'],
                            'zip' => $importingData['ContrZip'],
                            'state' => $this->getEntity(
                                '\AppBundle\Entity\State',
                                $newEntityManager,
                                [
                                    'code' => strtoupper($importingData['ContrState']),
                                ]
                            )
                        ],
                        true
                    ),
                    'setType' => $this->getEntity(
                        '\AppBundle\Entity\ContractorType',
                        $newEntityManager,
                        [
                            'alias' => ($importingData['UsedAsSub'] == 'Yes') ? 'partner' : 'competitor',
                        ]
                    ),
                    'setPhone' => $importingData['ContrPhone'],
                ],
                '\AppBundle\Entity\ContractorUser' => [
                    'setUser' => $this->getEntity(
                        '\AppBundle\Entity\User',
                        $newEntityManager,
                        [
                            'firstName' => $importingData['ContrConFName1'],
                            'lastName' => $importingData['ContrConLName1'],
                            'enabled' => 1,
                        ],
                        true,
                        [
                            'firstName' => $importingData['ContrConFName1'],
                            'lastName' => $importingData['ContrConLName1'],
                            'enabled' => 1,
                            'email' => (!empty($importingData['ContrConEmail1'])) ?
                                $importingData['ContrConEmail1'] :
                                'yyyy'.(rand(0, 65535)).'@yahoo.com',
                            'plainPassword' => $this->generateRandomPassword(),
                            'accessToken' => $this->generateRandomAccessToken(),
                        ]
                    ),
                    'setContractor' => $this->getEntity(
                        '\AppBundle\Entity\Contractor',
                        $newEntityManager,
                        [
                            'name' => $importingData['ContrCompName'],
                            'website' => $importingData['RegCompURL'],
                            'logo' => $importingData['ContrLogo'],
                        ]
                    ),
                    'setAddress' => $this->getEntity(
                        '\AppBundle\Entity\Address',
                        $newEntityManager,
                        [
                            'address' => $importingData['ContrAddr'],
                            'city' => $importingData['ContrCity'],
                            'zip' => $importingData['ContrZip'],
                            'state' => $this->getEntity(
                                '\AppBundle\Entity\State',
                                $newEntityManager,
                                [
                                    'code' => strtoupper($importingData['ContrState']),
                                ]
                            )
                        ],
                        true
                    ),
                    'setTitle' => $importingData['ContrConTitle1'],
                    'setPersonalPhone' => $importingData['ContrConPhone1'],
                    'setWorkPhone' => $importingData['ContrConPhone2'],
                    'setFax' => $importingData['ContrConFax1'],
                ],
            ]
        ];

        //return data depending on input parameters of function
        if (!empty($oldTableName)) {
            if (!empty($newEntityName)) {
                $result = $resultArray[$oldTableName][$newEntityName];
            } else {
                $result = $resultArray[$oldTableName];
            }
        } else {
            $result = $resultArray;
        }

        return $result;
    }

    private function generateRandomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

        $pass = '';
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $pass .= $alphabet[$n];
        }

        return $pass;
    }

    private function generateRandomAccessToken()
    {
        $alphabet = '0123456789abcdef';

        $hex = '';
        for ($i = 0; $i < 32; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $hex .= $alphabet[$n];
        }

        return $hex;
    }

    private function getEntity(
        string $entityFullQualifiedName,
        ObjectManager $entityManager,
        array $dataToFindBy,
        bool $createIfAbsent = false,
        array $dataToCreate = []
    ) {
        //get repository name
        $entityFullQualifiedNameParts = explode("\\", $entityFullQualifiedName);
        $bundleName = $entityFullQualifiedNameParts[1];
        $entityName = $entityFullQualifiedNameParts[3];
        $repositoryName = $bundleName.':'.$entityName;
        unset($entityName, $bundleName, $entityFullQualifiedNameParts);

        //get repository
        $entityRepository = $entityManager->getRepository($repositoryName);

        //find data
        $data = $entityRepository->findBy($dataToFindBy);
        if (count($data) > 0) {
            $result = $data[0];
        } else {
            if (!$createIfAbsent) {
                $result = null;
            } else {
                $newEntityData = (!empty($dataToCreate)) ? $dataToCreate : $dataToFindBy;
                if ($this->isEntityEmpty($newEntityData) == false) {
                    $result = new $entityFullQualifiedName();
                    foreach ($newEntityData as $fieldName => $fieldValue) {
                        if (method_exists($result, 'set' . ucfirst($fieldName))) {
                            $methodName = 'set' . ucfirst($fieldName);
                        } else {
                            $methodName = 'add' . ucfirst(substr($fieldName, 0, -1));
                        }
                        $result->$methodName($fieldValue);
                    }
                    $entityManager->persist($result);
                    $entityManager->flush();
                } else {
                    $result = null;
                }
            }
        }

        return $result;
    }

    private function isEntityEmpty(array $data, array $excludingFields = []) : bool
    {
        $result = false;
        $emptyFieldsQuantity = 0;
        foreach ($data as $key => $value) {
            if (empty($value) && !in_array($key, $excludingFields)) {
                $emptyFieldsQuantity++;
            }
        }
        if ($emptyFieldsQuantity == (count($data) - count($excludingFields))) {
            $result = true;
        }

        return $result;
    }
}
