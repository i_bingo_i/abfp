<?php

namespace ImportOldDatabaseBundle\Command;

use ImportOldDatabaseBundle\Services\BackflowFieldsDeviceToDevices;
use ImportOldDatabaseBundle\Services\FireFieldsDeviceToDevices;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportFieldsFromFireDeviceCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('olddb:data:importFieldsFromFireDeviceCommand')
            ->setDescription(
                'Imports (copies) data from this project`s database previous version to the current one.'
            )
            ->setHelp(
                'Import (copies) a data from the old database of this project to the new one.'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var FireFieldsDeviceToDevices $importFireFieldsDevice */
        $importFireFieldsDevice = $this->getContainer()->get('import.fire_fields.service');

        $finishTimeStamp = function (\DateTime $startTime, $finishMessage, $resultMessage) use ($output, $importFireFieldsDevice) {
            $currentTime = new \DateTime('now');
            $interval = $importFireFieldsDevice->getTimedifference($startTime, $currentTime);

            $output->writeln($currentTime->format("m/d/Y H:i:s") . ' ' . $finishMessage);
            $output->writeln($interval);
            $output->writeln($resultMessage);
            $output->writeln('');

            return $currentTime;
        };

        /**
         * Start Import
         */
        $startTime = new \DateTime('now');
        $output->writeln($startTime->format("m/d/Y H:i:s").' Import fire dynamic fields was started');
        $output->writeln('');

        /**
         * Import backflow dynamic fields
         */
        $fireDevices = $importFireFieldsDevice->import();
        $finishTimeStamp(
            $startTime,
            "Fire Devices Import completed!",
            "Imported: {$fireDevices} fire devices"
        );

        /**
         * Finish Import
         */
        $finishTime = new \DateTime('now');
        $timeInterval = $importFireFieldsDevice->getTimedifference($startTime, $finishTime);
        $output->writeln($finishTime->format("m/d/Y H:i:s").' Import fire dynamic fields completed!');
        $output->writeln('');
        $output->writeln($timeInterval);
        $output->writeln('Result: Done');
        $output->writeln('');
        $output->writeln('');
    }
}
