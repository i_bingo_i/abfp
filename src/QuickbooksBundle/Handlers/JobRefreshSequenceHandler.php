<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class JobRefreshSequenceHandler extends BaseHandler
{
    /**
     * JobRefreshSequenceHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::REFRESH_SEQUENCE_FOR_JOB;
        $this->exceptionMessage = "Job sequence was not updated";
    }
}
