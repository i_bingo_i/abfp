<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class ContactPersonCustomerModHandler extends BaseHandler
{
    /**
     * ContactPersonCustomerModHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::UPDATE_CONTACT_PERSON_CUSTOMER_QB;
        $this->exceptionMessage = "Contact Person customer wasn't update in Quickbooks.";
    }
}
