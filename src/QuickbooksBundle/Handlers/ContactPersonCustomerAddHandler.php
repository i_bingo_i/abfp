<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class ContactPersonCustomerAddHandler extends BaseHandler
{
    /**
     * ContactPersonCustomerAddHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::ADD_CUSTOMER_IN_QB;
        $this->exceptionMessage = "Contact person customer wasn't add in Quickbooks.";
    }
}
