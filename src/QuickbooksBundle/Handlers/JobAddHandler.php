<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class JobAddHandler extends BaseHandler
{
    /**
     * JobAddHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::CREATE_JOB_IN_QB;
        $this->exceptionMessage = "Job wasn't add in Quickbooks.";
    }
}
