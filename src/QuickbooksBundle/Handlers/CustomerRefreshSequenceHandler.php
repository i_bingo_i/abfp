<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class CustomerRefreshSequenceHandler extends BaseHandler
{
    /**
     * ContactPersonCustomerModHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::REFRESH_SEQUENCE_FOR_CUSTOMER;
        $this->exceptionMessage = "Customer sequence was not updated";
    }
}
