<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class CompanyCustomerModHandler extends BaseHandler
{
    /**
     * CompanyCustomerModHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::UPDATE_COMPANY_CUSTOMER_QB;
        $this->exceptionMessage = "Company customer wasn't update in Quickbooks.";
    }
}
