<?php

namespace QuickbooksBundle\Handlers;

use GuzzleHttp\Exception\ServerException;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;
use LoggerBundle\Services\QuickbooksRequestLogger;
use QuickbooksBundle\Exceptions\FailIQuickbooksAction;
use QuickbooksBundle\QBQuery\Query;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Response\Status;

abstract class BaseHandler
{
    /** @var QuickbooksRequestLogger  */
    protected $requestLogger;
    protected $logAction;
    protected $exceptionMessage;

    /**
     * BaseHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        $this->requestLogger = $requestLogger;
    }

    /**
     * @param Query $query
     * @param QuickbooksRequestLoggerDTO $requestLoggerDto
     * @return Response
     * @throws FailIQuickbooksAction
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function executeQuery(Query $query, QuickbooksRequestLoggerDTO $requestLoggerDto)
    {
        try {
            /** @var Response $response */
            $response = $query->getResult();
        } catch (ServerException $exception) {
            $requestLoggerDto->setResponse($exception->getMessage());
            $this->requestLogger->createNegativeRecord($requestLoggerDto, $this->logAction);

            throw new FailIQuickbooksAction($this->exceptionMessage);
        }

        $this->responseHandler($response, $requestLoggerDto, $query);

        return $response;
    }

    /**
     * @param Response $response
     * @param QuickbooksRequestLoggerDTO $requestLoggerDto
     * @param Query $query
     * @return bool
     * @throws FailIQuickbooksAction
     */
    private function responseHandler(Response $response, QuickbooksRequestLoggerDTO $requestLoggerDto, Query $query)
    {
        /** @var Status $responseStatus */
        $responseStatus = $response->status();

        if ($responseStatus->code() == 1 and $responseStatus->message() ==
            'A query request did not find a matching object in QuickBooks') {
            $requestLoggerDto->setResponse($query->body());
            $this->requestLogger->createWarningRecord($requestLoggerDto, $this->logAction);
            return false;
        }

        if ($responseStatus->code() == 0 and $responseStatus->message() == 'Status OK') {
            $requestLoggerDto->setResponse($query->body());
            $this->requestLogger->createPositiveRecord($requestLoggerDto, $this->logAction);
        } else {
            $requestLoggerDto->setResponse($responseStatus->message());
            $this->requestLogger->createNegativeRecord($requestLoggerDto, $this->logAction);

            throw new FailIQuickbooksAction($this->exceptionMessage);
        }
    }
}
