<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class ModifiedDate extends BaseHandler
{
    /**
     * JobModHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::SYNCHRONIZE_INVOICE;
        $this->exceptionMessage = "QB Items names were not synchronized";
    }
}