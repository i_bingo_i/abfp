<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class JobModHandler extends BaseHandler
{
    /**
     * JobModHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::UPDATE_JOB_IN_QB;
        $this->exceptionMessage = "Job wasn't update in Quickbooks.";
    }
}
