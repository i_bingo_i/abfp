<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class InvoiceSubmitNewHandler extends BaseHandler
{
    /**
     * InvoiceSubmitNewHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::SUBMIT_NEW_INVOICE_TO_QB;
        $this->exceptionMessage = "Invoice wasn't send to Quickbooks.";
    }
}
