<?php

namespace QuickbooksBundle\Handlers;

use LoggerBundle\Services\QuickbooksRequestLogger;

class InvoiceVoidHandler extends BaseHandler
{
    /**
     * InvoiceVoidHandler constructor.
     * @param QuickbooksRequestLogger $requestLogger
     */
    public function __construct(QuickbooksRequestLogger $requestLogger)
    {
        parent::__construct($requestLogger);

        $this->logAction = QuickbooksRequestLogger::VOID_INVOICE_IN_QB;
        $this->exceptionMessage = "Invoice wasn't send to Quickbooks.";
    }
}
