<?php

namespace QuickbooksBundle\Meneger;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Services\Item\CreatorItemService;
use QuickbooksBundle\Services\Item\GetterItemService;
use QuickbooksBundle\Services\Item\UpdaterItemService;

class ItemServiceManager
{
    /** @var GetterItemService */
    private $getterItemService;
    /** @var CreatorItemService */
    private $creatorItemService;
    /** @var UpdaterItemService */
    private $updaterItemService;

    /**
     * ItemServiceManager constructor.
     * @param GetterItemService $getterItemService
     * @param CreatorItemService $creatorItemService
     * @param UpdaterItemService $updaterItemService
     */
    public function __construct(
        GetterItemService $getterItemService,
        CreatorItemService $creatorItemService,
        UpdaterItemService $updaterItemService
    ) {
        $this->getterItemService = $getterItemService;
        $this->creatorItemService = $creatorItemService;
        $this->updaterItemService = $updaterItemService;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function copyAllRecordsFromQB()
    {
        $allItemServices = $this->getterItemService->getAll();
        $this->creatorItemService->createSeveralRecordsDB($allItemServices);
    }

    public function updateIfWasModified()
    {
        $QBItemServices = $this->getterItemService->getByModifiedDate();

        if ($QBItemServices instanceof Item) {
            $itemService = $this->getterItemService->getFromDBByQBId($QBItemServices->getQbId());
            $this->updaterItemService->updateOnDBRecord($itemService, $QBItemServices);
        }

        if (is_array($QBItemServices)) {
            /** @var Item $QBItemService */
            foreach ($QBItemServices as $QBItemService) {
                $itemService = $this->getterItemService->getFromDBByQBId($QBItemService->getQbId());
                if ($itemService) {
                    $this->updaterItemService->updateOnDBRecord($itemService, $QBItemService);

                } else {
                    $this->creatorItemService->createNewItemService($itemService);
                }
            }
        }
    }
}