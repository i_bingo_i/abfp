<?php

namespace QuickbooksBundle\Managers;

use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\EntityType;
use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\Factory\Logger\CustomerFactory;
use QuickbooksBundle\Handlers\CompanyCustomerModHandler;
use QuickbooksBundle\Handlers\ContactPersonCustomerAddHandler;
use QuickbooksBundle\Handlers\ContactPersonCustomerModHandler;
use QuickbooksBundle\Parser\AddCustomer;
use QuickbooksBundle\Parser\ModCustomer;
use QuickbooksBundle\QBQuery\QueryBuilder;
use QuickbooksBundle\QBQuery\QueryBuilderCreator;
use QuickbooksBundle\QBQuery\AddressTrait;
use QuickbooksBundle\Services\Quickbooks\Customer\ParserCustomer;

class CustomerManager
{
    use AddressTrait;

    /** @var QueryBuilderCreator */
    private $queryBuilderCreator;
    /** @var ParserCustomer */
    private $parserCustomer;
    /** @var ContactPersonCustomerModHandler */
    private $contactPersonCustomerModHandler;
    /** @var ContactPersonCustomerAddHandler */
    private $contactPersonCustomerAddHandler;
    /** @var CompanyCustomerModHandler */
    private $companyCustomerModHandler;

    /**
     * CustomerManager constructor.
     * @param QueryBuilderCreator $queryBuilderCreator
     * @param ParserCustomer $parserCustomer
     * @param ContactPersonCustomerModHandler $contactPersonCustomerModHandler
     */
    public function __construct(
        QueryBuilderCreator $queryBuilderCreator,
        ParserCustomer $parserCustomer,
        ContactPersonCustomerModHandler $contactPersonCustomerModHandler,
        ContactPersonCustomerAddHandler $contactPersonCustomerAddHandler,
        CompanyCustomerModHandler $companyCustomerModHandler
    ) {
        $this->queryBuilderCreator = $queryBuilderCreator;
        $this->parserCustomer = $parserCustomer;
        $this->contactPersonCustomerModHandler = $contactPersonCustomerModHandler;
        $this->contactPersonCustomerAddHandler = $contactPersonCustomerAddHandler;
        $this->companyCustomerModHandler = $companyCustomerModHandler;
    }

    /**
     * @param Customer $customer
     * @return \QuickbooksBundle\Response\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function modCustomer(Customer $customer)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder
            ->mod('Customer')
            ->setParameters([
                'ListID' => $customer->getAccountingSystemId(),
                'EditSequence' => $customer->getEditSequence(),
                'Name' => $this->getCustomerName($customer),
                'BillAddress' => $this->getAddress($customer->getBillingAddress()),
            ]);

        $query = $queryBuilder->getQuery();
        $query->setParser(new ModCustomer());

        $requestLoggerDto = CustomerFactory::make($customer);
        $requestLoggerDto->setRequest($query->body());

        /** @var EntityType $customerType */
        $customerType = $customer->getEntityType();

        if ($customerType->getAlias() == $customerType::TYPE_COMPANY) {
            return $this->companyCustomerModHandler->executeQuery($query, $requestLoggerDto);
        }
        return $this->contactPersonCustomerModHandler->executeQuery($query, $requestLoggerDto);
    }

    /**
     * @param Invoices $invoice
     * @return \QuickbooksBundle\Response\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function addCustomer(Invoices $invoice)
    {
        /** @var Customer $customer */
        $customer = $invoice->getJob()->getCustomer();

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder
            ->add('Customer')
            ->setParameters([
                'Name' => $this->getCustomerName($customer),
                'IsActive' => 'true',
                'BillAddress' => $this->getAddress($customer->getBillingAddress()),
            ]);

        $query = $queryBuilder->getQuery();
        $query->setParser(new AddCustomer());

        $requestLoggerDto = CustomerFactory::make($customer);
        $requestLoggerDto->setRequest($query->body());
        $requestLoggerDto->setWorkorder($invoice->getWorkorder());

        return $this->contactPersonCustomerAddHandler->executeQuery($query, $requestLoggerDto);
    }

    /**
     * @param Customer $customer
     * @return string
     */
    private function getCustomerName(Customer $customer)
    {
        $name = '[' . $customer->getEntityId() . ']' . ' ' . $customer->getName();
        return $name;
    }
}
