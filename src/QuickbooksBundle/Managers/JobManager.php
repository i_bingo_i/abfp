<?php

namespace QuickbooksBundle\Managers;

use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\Factory\Logger\JobFactory;
use QuickbooksBundle\Handlers\JobAddHandler;
use QuickbooksBundle\Handlers\JobModHandler;
use QuickbooksBundle\Parser\AddCustomer;
use QuickbooksBundle\Parser\ModCustomer;
use QuickbooksBundle\QBQuery\QueryBuilderCreator;
use QuickbooksBundle\QBQuery\QueryBuilder;
use QuickbooksBundle\QBQuery\AddressTrait;

class JobManager
{
    use AddressTrait;

    /** @var QueryBuilderCreator */
    private $queryBuilderCreator;
    /** @var JobAddHandler */
    private $jobAddHandler;
    /** @var JobModHandler */
    private $jobModHandler;

    /**
     * JobManager constructor.
     * @param QueryBuilderCreator $queryBuilderCreator
     * @param JobAddHandler $jobAddHandler
     * @param JobModHandler $jobModHandler
     */
    public function __construct(
        QueryBuilderCreator $queryBuilderCreator,
        JobAddHandler $jobAddHandler,
        JobModHandler $jobModHandler
    ) {
        $this->queryBuilderCreator = $queryBuilderCreator;
        $this->jobAddHandler = $jobAddHandler;
        $this->jobModHandler = $jobModHandler;
    }

    /**
     * @param Invoices $invoice
     * @return \QuickbooksBundle\Response\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function addJob(Invoices $invoice)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder
            ->add('Customer')
            ->setParameters([
                'Name' => $invoice->getJob()->getName(),
                'IsActive' => 'true',
                'ParentRef' => [
                    'ListID' => $invoice->getJob()->getCustomer()->getAccountingSystemId()
                ],
                'BillAddress' => $this->getAddress(
                    $this->getBillAddress($invoice->getAccountContactPerson())
                ),
                'ShipAddress' => $this->getAddress($invoice->getShipToAddress())
            ]);

        $query = $queryBuilder->getQuery();
        $query->setParser(new AddCustomer());

        $requestLoggerDto = JobFactory::make($invoice);
        $requestLoggerDto->setRequest($query->body());

        return $this->jobAddHandler->executeQuery($query, $requestLoggerDto);
    }

    /**
     * @param Invoices $invoice
     * @return \QuickbooksBundle\Response\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function modJob(Invoices $invoice)
    {
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder
            ->mod('Customer')
            ->setParameters([
                'ListID' => $invoice->getJob()->getAccountingSystemId(),
                'EditSequence' => $invoice->getJob()->getEditSequence(),
                'Name' => $invoice->getJob()->getName(),
                'ParentRef' => [
                    'ListID' => $invoice->getJob()->getCustomer()->getAccountingSystemId()
                ],
                'BillAddress' => $this->getAddress(
                    $this->getBillAddress($invoice->getAccountContactPerson())
                ),
                'ShipAddress' => $this->getAddress($invoice->getShipToAddress())
            ]);

        $query = $queryBuilder->getQuery();
        $query->setParser(new ModCustomer());

        $requestLoggerDto = JobFactory::make($invoice);
        $requestLoggerDto->setRequest($query->body());

        return $this->jobModHandler->executeQuery($query, $requestLoggerDto);
    }
}
