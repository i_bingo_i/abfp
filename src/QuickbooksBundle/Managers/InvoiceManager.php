<?php

namespace QuickbooksBundle\Managers;

use AppBundle\Services\SystemUser;
use InvoiceBundle\Entity\InvoiceLine;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Factory\Logger\InvoiceFactory;
use QuickbooksBundle\Factory\Logger\InvoiceModifiedDateFactory;
use QuickbooksBundle\Handlers\InvoiceSubmitDraftHandler;
use QuickbooksBundle\Handlers\InvoiceSubmitNewHandler;
use QuickbooksBundle\Handlers\InvoiceVoidHandler;
use QuickbooksBundle\Parser\ModifiedDate;
use QuickbooksBundle\Parser\VoidInvoice;
use QuickbooksBundle\QBQuery\QueryBuilderCreator;
use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\QBQuery\QueryBuilder;
use QuickbooksBundle\QBQuery\AddressTrait;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Handlers\ModifiedDate as ModifiedDateHandler;

class InvoiceManager
{
    use AddressTrait;

    /** @var QueryBuilderCreator */
    private $queryBuilderCreator;
    /** @var InvoiceSubmitDraftHandler  */
    private $invoiceSubmitDraftHandler;
    /** @var InvoiceSubmitNewHandler  */
    private $invoiceSubmitNewHandler;
    /** @var InvoiceVoidHandler  */
    private $invoiceVoidHandler;
    /** @var ModifiedDate */
    private $modifiedDateParser;
    /** @var ModifiedDateHandler */
    private $modifiedDateHandler;
    /** @var SystemUser */
    private $systemUser;

    private $queryBody;

    private $responseAttributes;

    /**
     * InvoiceManager constructor.
     * @param QueryBuilderCreator $queryBuilderCreator
     * @param InvoiceSubmitDraftHandler $draftHandler
     * @param InvoiceSubmitNewHandler $invoiceSubmitNewHandler
     * @param InvoiceVoidHandler $voidHandler
     * @param ModifiedDate $modifiedDateParser
     * @param ModifiedDateHandler $modifiedDateHandler
     * @param SystemUser $systemUser
     */
    public function __construct(
        QueryBuilderCreator $queryBuilderCreator,
        InvoiceSubmitDraftHandler $draftHandler,
        InvoiceSubmitNewHandler $invoiceSubmitNewHandler,
        InvoiceVoidHandler $voidHandler,
        ModifiedDate $modifiedDateParser,
        ModifiedDateHandler $modifiedDateHandler,
        SystemUser $systemUser
    ) {
        $this->queryBuilderCreator = $queryBuilderCreator;

        $this->invoiceSubmitDraftHandler = $draftHandler;
        $this->invoiceSubmitNewHandler = $invoiceSubmitNewHandler;
        $this->invoiceVoidHandler = $voidHandler;
        $this->modifiedDateParser = $modifiedDateParser;
        $this->modifiedDateHandler = $modifiedDateHandler;
        $this->systemUser = $systemUser;
    }

    /**
     * @param Invoices $invoice
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function voidInvoice(Invoices $invoice)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder
            ->void('Invoice', $invoice->getAccountingSystemId());

        $this->queryBody = $queryBuilder->getQuery()->body();
        $query = $queryBuilder->getQuery();
        $query->setParser(new VoidInvoice());

        $requestLoggerDto = InvoiceFactory::make($invoice);
        $requestLoggerDto->setRequest($query->body());

        return $this->invoiceVoidHandler->executeQuery($query, $requestLoggerDto);
    }

    /**
     * @param Invoices $invoice
     * @param bool $isNewInvoice
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function createInvoice(Invoices $invoice, $isNewInvoice = false)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder
            ->add('Invoice')
            ->setParameters([
                'CustomerRef' => [
                    'ListID' => $invoice->getJob()->getAccountingSystemId()
                ],
                'TemplateRef' => [
                    'FullName' => 'Copy 3 : Customized Invoice'
                ],
                'TxnDate' => $invoice->getInvoiceDate()->format('Y-m-d'),
                'BillAddress' => $this->getAddress($invoice->getBillToAddress()),
                'ShipAddress' => $this->getAddress($invoice->getShipToAddress()),
                'PONumber' => $invoice->getWorkorder()->getPoNo() ?? '',
                'TermsRef' => [
                    'ListID' => $invoice->getPaymentTerm()->getAccountingSystemId() ?? ''
                ],
                'FOB' => $invoice->getInspector(),
                'Memo' => $invoice->getMemo() ?? '',
                'CustomerMsgRef' => [
                    'FullName' => 'Thank you for your business.'
                ],
                'Other' => $invoice->getUser()->getFirstName() . ' ' . $invoice->getUser()->getLastName(),
                'InvoiceLineAdd' => $this->invoiceLines($invoice)
            ]);

        $requestLoggerDto = InvoiceFactory::make($invoice);
        $requestLoggerDto->setRequest($queryBuilder->getQuery()->body());

        if ($isNewInvoice) {
            return $this->invoiceSubmitNewHandler->executeQuery($queryBuilder->getQuery(), $requestLoggerDto);
        } else {
            return $this->invoiceSubmitDraftHandler->executeQuery($queryBuilder->getQuery(), $requestLoggerDto);
        }
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function getByModifiedDate($dateFrom, $dateTo, $accessToken = null)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder
            ->modifiedDate($dateFrom, $dateTo);

        $this->queryBody = $queryBuilder->getQuery()->body();
        $query = $queryBuilder->getQuery();
        $query->setParser($this->modifiedDateParser);

        $requestLoggerDto = InvoiceModifiedDateFactory::make($query);
        $requestLoggerDto->setUser($this->systemUser->getCurrent($accessToken));

        return $this->modifiedDateHandler->executeQuery($query, $requestLoggerDto);
    }

    /**
     * @return mixed
     */
    public function getSourceBody()
    {
        return $this->queryBody;
    }

    /**
     * @return mixed
     */
    public function getResponseAttributes()
    {
        return $this->responseAttributes;
    }

    /**
     * @param Invoices $invoices
     * @return array
     */
    private function invoiceLines(Invoices $invoices)
    {
        $lines = [];
        /** @var InvoiceLine $line */
        foreach ($invoices->getLines()->toArray() as $key => $line) {
            $lines[$key]['ItemRef']['ListID'] = $line->getItem()->getQbId();
            $lines[$key]['Desc'] = $line->getDescription();
            if ($line->getItem()->getType()->getAlias() != ItemType::TYPE_DISCOUNT) {
                $lines[$key]['Quantity'] = $line->getQuantity();
            }
            $lines[$key]['Rate'] = $line->getRate();
        }

        return $lines;
    }
}
