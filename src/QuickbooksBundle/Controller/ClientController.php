<?php

namespace QuickbooksBundle\Controller;

use DateTime;
use phpDocumentor\Reflection\Types\This;
use Doctrine\Common\Collections\ArrayCollection;
use QuickbooksBundle\Entity\Dummy\ListDeleted;
use QuickbooksBundle\Repository\QBRepository\QBItemRepository;
use QuickbooksBundle\Services\App\Item\Synchronizer as ItemSynchronizer;
use QuickbooksBundle\Services\App\Term\Synchronizer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ClientController extends Controller
{
    /**
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function indexAction()
    {
        $rep = $this->get('quickbooks.item.repository');
        $qp = $this->get('quickbooks.query_preparer');
        $q = '<?xml version="1.0"?>
<?qbxml version="13.0"?>
<QBXML>
<QBXMLMsgsRq onError="stopOnError">
    <InvoiceQueryRq>
        <EntityFilter>
            <ListID >80000152-1368112275</ListID>
        </EntityFilter>
    </InvoiceQueryRq>
</QBXMLMsgsRq>
</QBXML>
';

        dump($qp->executeRawRequest($q));
        return $this->render('@Quickbooks/for-test.html.twig');
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function saveAllItemsAction()
    {
        $itemSynchronizer = $this->get('quickbooks.item.synchronizer');

        $itemSynchronizer->createPrimaryList();

        return new Response('Success');
    }

    /**
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function synchronizePaymentTermAction()
    {
        $termsSyn = $this->get('quickbooks.term.synchronizer');
        $termsSyn->primarySynchronize();

        return new Response('Success');
    }
}
