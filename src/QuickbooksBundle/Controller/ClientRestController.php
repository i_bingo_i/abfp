<?php
namespace QuickbooksBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use QuickbooksBundle\Services\LogService;
use QuickbooksBundle\Services\QbClients;
//use QuickbooksBundle\Services\Response;
use SoapServer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientRestController extends FOSRestController
{
    /**
     * Quickbooks
     *
     * ### Response OK ###
     *     {
     *          "result": (string)"success"
     *     }
     *
     *
     * @ApiDoc(
     *   section = "quickbooks",
     *   resource = true,
     *   description = "Connect with quickbooks web connector",
     * )
     *
     * @param Request $request
     *
     * @Rest\Get("/quickbooks")
     * @return Response
     */
    public function getQuickbooksCallAction(Request $request)
    {
        return new Response('success');
    }

    /**
     * Quickbooks
     *
     * ### Response OK ###
     *     {
     *          "result": (string)"success"
     *     }
     *
     *
     * @ApiDoc(
     *   section = "quickbooks",
     *   resource = true,
     *   description = "Connect with quickbooks web connector",
     * )
     *
     * @param Request $request
     *
     * @Rest\Post("/quickbooks")
     * @return Response
     */
    public function postQuickbooksCallAction(Request $request)
    {
        $parametersConfig = $this->getParameter('quickbooks');
        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        $QbClient = new QbClients();
        $QbClient->login = $parametersConfig['login'];
        $QbClient->password = $parametersConfig['password'];
        $QbClient->ticket = '93f91a390fa604207f40e8a94d0d8fd11005de108ec1664234305e17e';

        $server = new SoapServer(__DIR__.'/../Resources/qbwebconnectorsvc.wsdl', ['cache_wsdl' => WSDL_CACHE_NONE]);

        $server->setObject($QbClient);
        ob_start();
        $server->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }
}