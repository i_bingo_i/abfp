<?php

namespace QuickbooksBundle\Factory;

use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Response\Status;

class ResponseFactory
{
    /**
     * @param string $xmlResponse
     *
     * @return Response
     */
    public static function make(string $xmlResponse, $parentAttribute): Response
    {
        /** @var Response $response */
        $response = new Response();

        $xmlResponseObject = simplexml_load_string($xmlResponse);
        $response->setXmlObject($xmlResponseObject);

        /** @var array $attributes */
        $attributes = (array) $xmlResponseObject->QBXMLMsgsRs->{$parentAttribute}->attributes();
        /** @var Status $status */
        $status = new Status();
        $status->setCode($attributes['@attributes']['statusCode']);
        $status->setMessage($attributes['@attributes']['statusMessage']);
        $status->setSeverity($attributes['@attributes']['statusSeverity']);
        $response->setStatus($status);

        return $response;
    }
}
