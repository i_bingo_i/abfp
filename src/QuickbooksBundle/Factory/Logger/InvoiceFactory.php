<?php

namespace QuickbooksBundle\Factory\Logger;

use InvoiceBundle\Entity\Invoices;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;

class InvoiceFactory
{
    /**
     * @param Invoices $invoice
     * @return QuickbooksRequestLoggerDTO
     */
    public static function make(Invoices $invoice) : QuickbooksRequestLoggerDTO
    {
        $requestLoggerDto = new QuickbooksRequestLoggerDTO();
        $requestLoggerDto->setEntity($invoice);
        $requestLoggerDto->setWorkorder($invoice->getWorkorder());

        return $requestLoggerDto;
    }
}
