<?php

namespace QuickbooksBundle\Factory\Logger;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\Job;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;

class JobFactory
{
    /**
     * @param Job $job
     * @return QuickbooksRequestLoggerDTO
     */
    public static function make(Invoices $invoices) : QuickbooksRequestLoggerDTO
    {
        $requestLoggerDTO = new QuickbooksRequestLoggerDTO();
        $requestLoggerDTO->setWorkorder($invoices->getWorkorder());
        $requestLoggerDTO->setEntity($invoices->getJob());

        return $requestLoggerDTO;
    }
}
