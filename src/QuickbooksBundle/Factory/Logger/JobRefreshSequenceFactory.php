<?php

namespace QuickbooksBundle\Factory\Logger;

use InvoiceBundle\Entity\Job;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;
use QuickbooksBundle\QBQuery\Query;

class JobRefreshSequenceFactory
{
    /**
     * @param Job $job
     * @param Query $query
     * @return QuickbooksRequestLoggerDTO
     */
    public static function make(Job $job, Query $query) : QuickbooksRequestLoggerDTO
    {
        $requestLoggerDto = new QuickbooksRequestLoggerDTO();
        $requestLoggerDto->setEntity($job);
        $requestLoggerDto->setRequest($query->body());
        $requestLoggerDto->setNeedToWritePositiveRecord(false);
        $requestLoggerDto->setNeedToPositiveFlashMessage(false);

        return $requestLoggerDto;
    }
}
