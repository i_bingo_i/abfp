<?php

namespace QuickbooksBundle\Factory\Logger;

use InvoiceBundle\Entity\Customer;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;

class CustomerFactory
{
    /**
     * @param Customer $customer
     * @return QuickbooksRequestLoggerDTO
     */
    public static function make(Customer $customer) : QuickbooksRequestLoggerDTO
    {
        $requestLoggerDto = new QuickbooksRequestLoggerDTO();
        $requestLoggerDto->setEntity($customer);

        return $requestLoggerDto;
    }
}
