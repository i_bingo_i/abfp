<?php

namespace QuickbooksBundle\Factory\Logger;

use InvoiceBundle\Entity\Customer;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;
use QuickbooksBundle\QBQuery\Query;

class CustomerRefreshSequenceFactory
{
    /**
     * @param Customer $customer
     * @param Query $query
     * @return QuickbooksRequestLoggerDTO
     */
    public static function make(Customer $customer, Query $query) : QuickbooksRequestLoggerDTO
    {
        $requestLoggerDto = new QuickbooksRequestLoggerDTO();
        $requestLoggerDto->setEntity($customer);
        $requestLoggerDto->setRequest($query->body());
        $requestLoggerDto->setNeedToWritePositiveRecord(false);
        $requestLoggerDto->setNeedToPositiveFlashMessage(false);

        return $requestLoggerDto;
    }
}
