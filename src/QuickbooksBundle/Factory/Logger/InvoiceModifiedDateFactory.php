<?php

namespace QuickbooksBundle\Factory\Logger;

use InvoiceBundle\Entity\Invoices;
use LoggerBundle\DTO\QuickbooksRequestLoggerDTO;
use QuickbooksBundle\QBQuery\Query;

class InvoiceModifiedDateFactory
{
    /**
     * @param Invoices $invoice
     * @return QuickbooksRequestLoggerDTO
     */
    public static function make(Query $query) : QuickbooksRequestLoggerDTO
    {
        $requestLoggerDto = new QuickbooksRequestLoggerDTO();
        $requestLoggerDto->setRequest($query->body());
        $requestLoggerDto->setNeedToPositiveFlashMessage(false);
        $requestLoggerDto->setNeedToNegativeFlashMessage(false);

        return $requestLoggerDto;
    }
}
