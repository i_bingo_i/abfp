<?php

namespace QuickbooksBundle\Validators\Request;

class Quickbook
{
    /** @var array $errorMessages */
    protected $errorMessages = [];

    /**
     * @param $response
     * @return bool
     */
    public function checkResponse($response)
    {
        if ($response['@attributes']['statusCode'] != 0
            && $response['@attributes']['statusMessage'] != 'A query request did not find a matching object in QuickBooks'
        ) {
            $this->errorMessages[] = $response['@attributes']['statusMessage'];

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }
}
