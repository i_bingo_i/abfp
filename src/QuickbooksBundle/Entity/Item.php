<?php

namespace QuickbooksBundle\Entity;

class Item
{
    private $id;

    private $qbId;

    private $qbEditSequence;

    private $qbTimeModified;

    private $name;

    private $description;

    private $type;

    private $rate;

    private $rateType;

    private $created;

    private $updated;

    private $isActive;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getQbId()
    {
        return $this->qbId;
    }

    /**
     * @param mixed $qbId
     */
    public function setQbId($qbId)
    {
        $this->qbId = $qbId;
    }

    /**
     * @return mixed
     */
    public function getQbEditSequence()
    {
        return $this->qbEditSequence;
    }

    /**
     * @param mixed $qbEditSequence
     */
    public function setQbEditSequence($qbEditSequence)
    {
        $this->qbEditSequence = $qbEditSequence;
    }

    /**
     * @return mixed
     */
    public function getQbTimeModified()
    {
        return $this->qbTimeModified;
    }

    /**
     * @param mixed $qbTimeModified
     */
    public function setQbTimeModified($qbTimeModified)
    {
        $this->qbTimeModified = $qbTimeModified;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate = null)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return ItemType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param ItemType $type
     */
    public function setType(ItemType $type)
    {
        $this->type = $type;
    }

    /**
     * @return ItemRateType
     */
    public function getRateType()
    {
        return $this->rateType;
    }

    /**
     * @param ItemRateType $rateType
     */
    public function setRateType(ItemRateType $rateType)
    {
        $this->rateType = $rateType;
    }

    public function setCreatedAtValue()
    {
        $this->created = new \DateTime();
    }

    public function setUpdatedAtValue()
    {
        $this->updated = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }
}