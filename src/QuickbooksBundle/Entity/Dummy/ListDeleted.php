<?php

namespace QuickbooksBundle\Entity\Dummy;

use DateTime;

class ListDeleted
{
    /** @var string */
    private $listDelType;

    /** @var string */
    private $listID;

    /** @var string */
    private $timeCreated;

    /** @var DateTime */
    private $timeDeleted;

    /** @var DateTime */
    private $fullName;

    /**
     * @return string
     */
    public function getListDelType()
    {
        return $this->listDelType;
    }

    /**
     * @param string $listDelType
     */
    public function setListDelType($listDelType)
    {
        $this->listDelType = $listDelType;
    }

    /**
     * @return string
     */
    public function getListID()
    {
        return $this->listID;
    }

    /**
     * @param string $listID
     */
    public function setListID($listID)
    {
        $this->listID = $listID;
    }

    /**
     * @return string
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * @param string $timeCreated
     */
    public function setTimeCreated($timeCreated)
    {
        $this->timeCreated = $timeCreated;
    }

    /**
     * @return DateTime
     */
    public function getTimeDeleted()
    {
        return $this->timeDeleted;
    }

    /**
     * @param DateTime $timeDeleted
     */
    public function setTimeDeleted(DateTime $timeDeleted)
    {
        $this->timeDeleted = $timeDeleted;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }
}