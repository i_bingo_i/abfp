<?php

namespace QuickbooksBundle\Interfaces;

use QuickbooksBundle\Response\Response;

interface ResponseParser
{
    public function parse(string $strXmlResponse): Response;
}
