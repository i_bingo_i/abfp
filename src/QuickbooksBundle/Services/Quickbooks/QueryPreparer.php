<?php

namespace QuickbooksBundle\Services\Quickbooks;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use SimpleXMLElement;

class QueryPreparer extends ConnectorWithQB
{
    /**
     * GetterItemService constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->login = $parameters['login'];
        $this->password = $parameters['password'];
        $this->url = $parameters['url'];
    }

    /**
     * @param array
     * @return SimpleXMLElement
     * @throws GuzzleException
     * @throws Exception
     */
    public function getResponse(array $query)
    {
        $this->createQuery($query);
        /** @var ResponseInterface $responseGuzzle */
        $responseGuzzle = $this->executeRequest();
        $body = utf8_encode($responseGuzzle->getBody());
        $responseObject = simplexml_load_string($body);

        return $responseObject;
    }

    /**
     * @param string $xml
     * @return SimpleXMLElement
     * @throws GuzzleException
     * @throws Exception
     */
    public function executeRawRequest($xml)
    {
        $this->body = trim($xml);
        $this->headers();
        $this->checkFields();

        $responseGuzzle = $this->executeRequest();
        $body = utf8_encode($responseGuzzle->getBody());
        $responseObject = simplexml_load_string($body);

        return $responseObject;
    }
}