<?php

namespace QuickbooksBundle\Services\Quickbooks\Job;

use QuickbooksBundle\Services\App\Job\CreatorJobByQbData;
use QuickbooksBundle\Services\Quickbooks\BaseParser;
use SimpleXMLElement;

class ParserJob extends BaseParser
{
    /**
     * ParserInvoice constructor.
     * @param CreatorJobByQbData $creatorJobByQbData
     */
    public function __construct(CreatorJobByQbData $creatorJobByQbData)
    {
        $this->creator = $creatorJobByQbData;
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingAdd(SimpleXMLElement $response)
    {
        $jobRs = (array) $response->QBXMLMsgsRs->CustomerAddRs;
        $job = (array) $jobRs['CustomerRet'];

        return $this->createResponse($job);
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingModify(SimpleXMLElement $response)
    {
        $jobRs = (array) $response->QBXMLMsgsRs->CustomerModRs;
        $job = (array) $jobRs['CustomerRet'];

        return $this->createResponse($job);
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        // TODO: Implement parsingQuery() method.
    }
}