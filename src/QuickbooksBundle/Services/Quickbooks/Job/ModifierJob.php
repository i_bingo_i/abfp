<?php

namespace QuickbooksBundle\Services\Quickbooks\Job;

use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\QBQuery\Job\JobModifyTrait;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;

class ModifierJob
{
    use JobModifyTrait;

    /** @var QueryPreparer */
    private $queryPreparer;
    /** @var ParserJob */
    private $parserJob;

    /**
     * CreatorJob constructor.
     * @param QueryPreparer $queryPreparer
     * @param ParserJob $parserJob
     */
    public function __construct(QueryPreparer $queryPreparer, ParserJob $parserJob)
    {
        $this->queryPreparer = $queryPreparer;
        $this->parserJob = $parserJob;
    }

    /**
     * @param Invoices $invoices
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function modifyJob(Invoices $invoices)
    {
        $query = $this->modify($invoices);
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->CustomerModRs->attributes();

        Validator::checkResponse($attributes['@attributes']);
        /** @var array $customers */
        $customers = $this->parserJob->parsingModify($responseXMLObject);

        return array_shift($customers);
    }
}
