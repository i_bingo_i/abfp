<?php
namespace QuickbooksBundle\Services\Quickbooks;

use DOMNode;
use DOMDocument;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ConnectorWithQB
{
    private const TIME_CONNECT_TIMEOUT = 10;
    private const TIME_TIMEOUT = 20;

    /** @var array */
    protected $headers;
    /** @var string */
    protected $login;
    /** @var string */
    protected $password;
    /** @var string */
    protected $body;
    /** @var string */
    protected $url;

    /**
     * @param array $query
     * @throws Exception
     */
    protected function createQuery(array $query)
    {
        $this->prepareBody($query);
        $this->headers();
        $this->checkFields();
    }

    /**
     * @return mixed|ResponseInterface
     * @throws GuzzleException
     */
    protected function executeRequest()
    {
        $guzzleClient = new Client();

        $response = $guzzleClient->request('POST', $this->url, [
            'auth' => [$this->login, $this->password],
            'headers' => $this->headers,
            'body' => $this->body,
            'connect_timeout' => self::TIME_CONNECT_TIMEOUT,
            'timeout' => self::TIME_TIMEOUT
        ]);

        return $response;
    }

    /**
     * @param array $query
     */
    protected function prepareBody(array $query)
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $encoder = new XmlEncoder('QBXML');
        $queryXML = $encoder->encode($query, 'xml');

        $dom->loadXML($queryXML);

        /** @var DOMNode $rootNode */
        $rootNode = $dom->getElementsByTagName('QBXML')->item(0);

        $dom->insertBefore($dom->createProcessingInstruction('qbxml', 'version="13.0"'), $rootNode);

        $this->body = $dom->saveXml();
    }

    /**
     * @description Create headers for query
     */
    protected function headers()
    {
        $this->headers = [
            'Content-Length' => mb_strlen($this->body, 'utf8'),
            'Accept-Encoding' => 'gzip, deflate',
            'Connection' => 'close',
            'X-AcctSyncInteractionType' => 0,
        ];
    }

    /**
     * @throws Exception
     */
    protected function checkFields()
    {
        Validator::checkRequiredFields($this->password, 'password');
        Validator::checkRequiredFields($this->login, 'login');
        Validator::checkRequiredFields($this->url, 'url');
        Validator::checkRequiredFields($this->body, 'body');
    }

}