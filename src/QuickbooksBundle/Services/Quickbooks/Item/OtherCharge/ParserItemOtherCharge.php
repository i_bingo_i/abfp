<?php

namespace QuickbooksBundle\Services\Quickbooks\Item\OtherCharge;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemRateType;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Repository\ItemRateTypeRepository;
use QuickbooksBundle\Repository\ItemTypeRepository;
use QuickbooksBundle\Services\App\Item\CreatorByQbData\CreatorItemOtherCharge;
use QuickbooksBundle\Services\Quickbooks\Item\BaseItemParser;
use SimpleXMLElement;

class ParserItemOtherCharge extends BaseItemParser
{
    /** @var ItemRateType */
    private $itemRateTypePercent;
    /** @var ItemRateType */
    private $itemRateTypeFloat;
    /** @var float */
    private $rate;

    /**
     * ParserItemOtherChargeResponse constructor.
     * @param ItemTypeRepository $itemTypeRepository
     * @param ItemRateTypeRepository $itemRateTypeRepository,
     * @param CreatorItemOtherCharge $creatorItemOtherCharge
     */
    public function __construct(
        ItemTypeRepository $itemTypeRepository,
        ItemRateTypeRepository $itemRateTypeRepository,
        CreatorItemOtherCharge $creatorItemOtherCharge
    ) {
        parent::__construct($itemTypeRepository, $itemRateTypeRepository, $creatorItemOtherCharge);
        $this->itemType = $this->itemTypeRepository->findOneBy(['alias' => ItemType::TYPE_OTHER_CHARGE]);
        $this->itemRateTypePercent = $this->itemRateTypeRepository->findOneBy(['alias' => ItemRateType::TYPE_PERCENT]);
        $this->itemRateTypeFloat = $this->itemRateTypeRepository->findOneBy(['alias' => ItemRateType::TYPE_FLOAT]);
    }

    /**
     * @param SimpleXMLElement $response
     * @return string|array|Item
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $itemQuery = (array) $response->QBXMLMsgsRs->ItemQueryRs;
        $items = (array) $itemQuery['ItemOtherChargeRet'];

        return $this->createResponse($items);
    }

    /**
     * @param array $items
     * @param string $idField
     * @return array
     */
    protected function createResponse(array $items, $idField = 'ListID')
    {
        $res = [];

        if (array_key_exists($idField, $items)) {
            $this->getRate($items);
            $res[] = $this->creator->createObj($items, $this->itemType, $this->itemRateType, $this->rate);

        } else {
            foreach ($items as $item) {
                settype($item, 'array');
                $this->getRate($item);
                $res[] = $this->creator->createObj($item, $this->itemType, $this->itemRateType, $this->rate);
            }
        }

        return $res;
    }

    /**
     * @param array $item
     */
    private function getRate(array $item)
    {
        if (isset($item['SalesOrPurchase'])) {
            settype($item['SalesOrPurchase'], 'array');

            if (isset($item['SalesOrPurchase']['PricePercent'])) {
                $this->itemRateType = $this->itemRateTypePercent;
                $this->rate = $item['SalesOrPurchase']['PricePercent'];

            } elseif (isset($item['SalesOrPurchase']['Price'])) {
                $this->itemRateType = $this->itemRateTypeFloat;
                $this->rate = $item['SalesOrPurchase']['Price'];

            }

            if (!isset($item['SalesOrPurchase']['PricePercent']) and !isset($item['SalesOrPurchase']['Price'])) {
                $this->itemRateType = $this->itemRateTypeFloat;
                $this->rate = 0;
            }
        }
    }
}