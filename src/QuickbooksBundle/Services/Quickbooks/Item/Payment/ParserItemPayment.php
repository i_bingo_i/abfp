<?php

namespace QuickbooksBundle\Services\Quickbooks\Item\Payment;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Repository\ItemRateTypeRepository;
use QuickbooksBundle\Repository\ItemTypeRepository;
use QuickbooksBundle\Services\App\Item\CreatorByQbData\CreatorItemPayment;
use QuickbooksBundle\Services\Quickbooks\Item\BaseItemParser;
use SimpleXMLElement;

class ParserItemPayment extends BaseItemParser
{
    /**
     * ParserItemOtherChargeResponse constructor.
     * @param ItemTypeRepository $itemTypeRepository
     * @param ItemRateTypeRepository $itemRateTypeRepository,
     * @param CreatorItemPayment $creatorItemPayment
     */
    public function __construct(
        ItemTypeRepository $itemTypeRepository,
        ItemRateTypeRepository $itemRateTypeRepository,
        CreatorItemPayment $creatorItemPayment
    ) {
        parent::__construct($itemTypeRepository, $itemRateTypeRepository, $creatorItemPayment);
        $this->itemType = $this->itemTypeRepository->findOneBy(['alias' => ItemType::TYPE_PAYMENT]);
    }

    /**
     * @param SimpleXMLElement $response
     * @return string|array|Item
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $itemQuery = (array) $response->QBXMLMsgsRs->ItemQueryRs;
        $items = (array) $itemQuery['ItemPaymentRet'];

        return $this->createResponse($items);
    }
}