<?php

namespace QuickbooksBundle\Services\Quickbooks\Item\Subtotal;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Repository\ItemRateTypeRepository;
use QuickbooksBundle\Repository\ItemTypeRepository;
use QuickbooksBundle\Services\App\Item\CreatorByQbData\CreatorItemSubtotal;
use QuickbooksBundle\Services\Quickbooks\Item\BaseItemParser;
use SimpleXMLElement;

class ParserItemSubtotal extends BaseItemParser
{
    /**
     * ParserItemOtherChargeResponse constructor.
     * @param ItemTypeRepository $itemTypeRepository
     * @param ItemRateTypeRepository $itemRateTypeRepository,
     * @param CreatorItemSubtotal $creatorItemSubtotal
     */
    public function __construct(
        ItemTypeRepository $itemTypeRepository,
        ItemRateTypeRepository $itemRateTypeRepository,
        CreatorItemSubtotal $creatorItemSubtotal
    ) {
        parent::__construct($itemTypeRepository, $itemRateTypeRepository, $creatorItemSubtotal);
        $this->itemType = $this->itemTypeRepository->findOneBy(['alias' => ItemType::TYPE_SUBTOTAL]);
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $itemQuery = (array) $response->QBXMLMsgsRs->ItemQueryRs;
        $items = (array) $itemQuery['ItemSubtotalRet'];

        return $this->createResponse($items);
    }
}