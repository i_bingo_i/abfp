<?php

namespace QuickbooksBundle\Services\Quickbooks\Item\Service;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemRateType;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Repository\ItemRateTypeRepository;
use QuickbooksBundle\Repository\ItemTypeRepository;
use QuickbooksBundle\Services\App\Item\CreatorByQbData\CreatorItemService;
use QuickbooksBundle\Services\Quickbooks\Item\BaseItemParser;
use SimpleXMLElement;

class ParserItemService extends BaseItemParser
{
    /**
     * ParserItemOtherChargeResponse constructor.
     * @param ItemTypeRepository $itemTypeRepository
     * @param ItemRateTypeRepository $itemRateTypeRepository,
     * @param CreatorItemService $creatorItemService
     */
    public function __construct(
        ItemTypeRepository $itemTypeRepository,
        ItemRateTypeRepository $itemRateTypeRepository,
        CreatorItemService $creatorItemService
    ) {
        parent::__construct($itemTypeRepository, $itemRateTypeRepository, $creatorItemService);
        $this->itemType = $this->itemTypeRepository->findOneBy(['alias' => ItemType::TYPE_SERVICE]);
        $this->itemRateType = $this->itemRateTypeRepository->findOneBy(['alias' => ItemRateType::TYPE_FLOAT]);
    }

    /**
     * @param SimpleXMLElement $response
     * @return string|array|Item
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $itemQuery = (array) $response->QBXMLMsgsRs->ItemQueryRs;
        $items = (array) $itemQuery['ItemServiceRet'];

        return $this->createResponse($items);
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingAdd(SimpleXMLElement $response)
    {
        $itemServiceAddRs = (array) $response->QBXMLMsgsRs->ItemServiceAddRs;
        $items = (array) $itemServiceAddRs['ItemServiceRet'];

        return $this->createResponse($items);
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingMod(SimpleXMLElement $response)
    {
        $itemServiceAddRs = (array) $response->QBXMLMsgsRs->ItemServiceModRs;
        $items = (array) $itemServiceAddRs['ItemServiceRet'];

        return $this->createResponse($items);
    }
}