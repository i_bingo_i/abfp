<?php

namespace QuickbooksBundle\Services\Quickbooks\ItemService;

use QuickbooksBundle\QBQuery\Item\ItemServiceMod;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;

class UpdaterItemService
{
    use ParserItemOtherChargeResponse;
    use ItemServiceMod;

    /** @var QueryPreparer */
    private $queryPreparer;

    /**
     * UpdaterItemService constructor.
     * @param QueryPreparer $queryPreparer
     */
    public function __construct(QueryPreparer $queryPreparer)
    {
        $this->queryPreparer = $queryPreparer;
    }

    /**
     * @description values looks like:
     * [
     *   'ListID' => '', require
     *   'EditSequence' => '', require
     *   'Name' => '',
     *   'SalesOrPurchaseMod' => [
     *      'Price' => ''
     *   ]
     * ]
     * @param array $newValues
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function modify(array $newValues) : mixed
    {
        $query = $this->modifyItemService($newValues);
        $result = $this->parsingItemServiceModRs($this->queryPreparer->getResponse($query));
        Validator::checkResponse($result);

        return $result['result'];
    }
}