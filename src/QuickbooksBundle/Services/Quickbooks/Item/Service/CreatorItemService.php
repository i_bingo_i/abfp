<?php

namespace QuickbooksBundle\Services\Quickbooks\ItemService;

use QuickbooksBundle\QBQuery\Item\ItemServiceAdd;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;

class CreatorItemService
{
    use ParserItemOtherChargeResponse;
    use ItemServiceAdd;

    /** @var QueryPreparer */
    private $queryPreparer;

    /**
     * CreatorItemService constructor.
     * @param QueryPreparer $queryPreparer
     */
    public function __construct(QueryPreparer $queryPreparer)
    {
        $this->queryPreparer = $queryPreparer;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createNewItemServiceInBothSystem()
    {
        $query = $this->add();
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $result = $this->parsingItemServiceAddRs($responseXMLObject);
        Validator::checkResponse($result);

        return $result['result'];
    }
}