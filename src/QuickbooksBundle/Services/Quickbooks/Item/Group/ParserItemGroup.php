<?php

namespace QuickbooksBundle\Services\Quickbooks\Item\Group;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Repository\ItemRateTypeRepository;
use QuickbooksBundle\Repository\ItemTypeRepository;
use QuickbooksBundle\Services\App\Item\CreatorByQbData\CreatorItemGroup;
use QuickbooksBundle\Services\Quickbooks\Item\BaseItemParser;
use SimpleXMLElement;

class ParserItemGroup extends BaseItemParser
{

    /**
     * ParserItemOtherChargeResponse constructor.
     * @param ItemTypeRepository $itemTypeRepository
     * @param ItemRateTypeRepository $itemRateTypeRepository
     * @param CreatorItemGroup $creatorItemGroup
     */
    public function __construct(
        ItemTypeRepository $itemTypeRepository,
        ItemRateTypeRepository $itemRateTypeRepository,
        CreatorItemGroup $creatorItemGroup
    ) {
        parent::__construct($itemTypeRepository, $itemRateTypeRepository, $creatorItemGroup);
        $this->itemType = $this->itemTypeRepository->findOneBy(['alias' => ItemType::TYPE_GROUP]);
    }

    /**
     * @param SimpleXMLElement $response
     * @return string|array|Item
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $itemQuery = (array) $response->QBXMLMsgsRs->ItemQueryRs;
        $items = (array) $itemQuery['ItemGroupRet'];

        return $this->createResponse($items);
    }
}