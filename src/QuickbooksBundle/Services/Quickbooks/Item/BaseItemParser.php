<?php

namespace QuickbooksBundle\Services\Quickbooks\Item;

use QuickbooksBundle\Entity\ItemRateType;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Repository\ItemRateTypeRepository;
use QuickbooksBundle\Repository\ItemTypeRepository;
use QuickbooksBundle\Services\App\Item\CreatorByQbData\BaseCreatorItem;
use QuickbooksBundle\Services\Quickbooks\BaseParser;
use SimpleXMLElement;

class BaseItemParser extends BaseParser
{
    /** @var ItemTypeRepository */
    protected $itemTypeRepository;
    /** @var ItemRateTypeRepository */
    protected $itemRateTypeRepository;
    /** @var ItemType */
    protected $itemType;
    /** @var BaseCreatorItem */
    protected $creator;
    /** @var ItemRateType */
    protected $itemRateType;

    /**
     * BaseParser constructor.
     * @param ItemTypeRepository $itemTypeRepository
     * @param ItemRateTypeRepository $itemRateTypeRepository
     * @param BaseCreatorItem $creatorItemByQbData
     */
    public function __construct(
        ItemTypeRepository $itemTypeRepository,
        ItemRateTypeRepository $itemRateTypeRepository,
        BaseCreatorItem $creatorItemByQbData
    ) {
        $this->itemTypeRepository = $itemTypeRepository;
        $this->itemRateTypeRepository = $itemRateTypeRepository;
        $this->creator = $creatorItemByQbData;
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        return [];
    }

    /**
     * @param array $items
     * @param string $idField
     * @return array
     */
    protected function createResponse(array $items, $idField = 'ListID')
    {
        $res = [];

        if (array_key_exists($idField, $items)) {
            $res[] = $this->creator->createObj($items, $this->itemType, $this->itemRateType);

        } else {
            foreach ($items as $item) {
                settype($item, 'array');
                $res[] = $this->creator->createObj($item, $this->itemType, $this->itemRateType);
            }

        }

        return $res;
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingAdd(SimpleXMLElement $response)
    {
        // TODO: Implement parsingAdd() method.
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingModify(SimpleXMLElement $response)
    {
        // TODO: Implement parsingModify() method.
    }
}