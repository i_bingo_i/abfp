<?php

namespace QuickbooksBundle\Services\Quickbooks\Item\Discount;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemRateType;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\Repository\ItemRateTypeRepository;
use QuickbooksBundle\Repository\ItemTypeRepository;
use QuickbooksBundle\Services\App\Item\CreatorByQbData\CreatorItemDiscount;
use QuickbooksBundle\Services\Quickbooks\Item\BaseItemParser;
use SimpleXMLElement;

class ParserItemDiscount extends BaseItemParser
{
    /** @var ItemRateType */
    private $itemRateTypePercent;
    /** @var ItemRateType */
    private $itemRateTypeFloat;
    /** @var integer */
    private $rate;

    /**
     * ParserItemOtherChargeResponse constructor.
     * @param ItemTypeRepository $itemTypeRepository
     * @param ItemRateTypeRepository $itemRateTypeRepository
     * @param CreatorItemDiscount $creatorItemDiscount
     */
    public function __construct(
        ItemTypeRepository $itemTypeRepository,
        ItemRateTypeRepository $itemRateTypeRepository,
        CreatorItemDiscount $creatorItemDiscount
    ) {
        parent::__construct($itemTypeRepository, $itemRateTypeRepository, $creatorItemDiscount);
        $this->itemType = $this->itemTypeRepository->findOneBy(['alias' => ItemType::TYPE_DISCOUNT]);
        $this->itemRateTypePercent = $this->itemRateTypeRepository->findOneBy(['alias' => ItemRateType::TYPE_PERCENT]);
        $this->itemRateTypeFloat = $this->itemRateTypeRepository->findOneBy(['alias' => ItemRateType::TYPE_FLOAT]);
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $itemQuery = (array) $response->QBXMLMsgsRs->ItemQueryRs;
        $items = (array) $itemQuery['ItemDiscountRet'];

        return $this->createResponse($items);
    }

    /**
     * @param array $items
     * @param string $idField
     * @return array
     */
    protected function createResponse(array $items, $idField = 'ListID')
    {
        $res = [];

        if (array_key_exists('ListID', $items)) {
            $this->getRate($items);
            $res[] = $this->creator->createObj($items, $this->itemType, $this->itemRateType, $this->rate);

        } else {
            foreach ($items as $item) {
                settype($item, 'array');
                $this->getRate($item);

                $res[] = $this->creator->createObj($item, $this->itemType, $this->itemRateType, $this->rate);
            }
        }

        return $res;
    }

    /**
     * @param $item
     */
    private function getRate($item)
    {
        if (isset($item['DiscountRatePercent'])) {
            $this->itemRateType = $this->itemRateTypePercent;
            $this->rate = $item['DiscountRatePercent'];

        } elseif (isset($item['DiscountRate'])) {
            $this->itemRateType = $this->itemRateTypeFloat;
            $this->rate = $item['DiscountRate'];

        }
    }
}