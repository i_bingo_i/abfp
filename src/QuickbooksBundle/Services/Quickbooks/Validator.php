<?php

namespace QuickbooksBundle\Services\Quickbooks;

use Exception;

class Validator
{
    /**
     * @param $param
     * @param $fieldName
     * @throws Exception
     */
    public static function checkRequiredFields($param, $fieldName)
    {
        if (empty($param)) {
            throw new Exception($fieldName .' parameter is empty.');
        }
    }

    /**
     * @param $response
     * @throws Exception
     */
    public static function checkResponse($response)
    {
        if ($response['statusCode'] != 0
            && $response['statusMessage'] != 'A query request did not find a matching object in QuickBooks'
        ) {
            throw new \Exception($response['statusMessage']);
        }
    }
}