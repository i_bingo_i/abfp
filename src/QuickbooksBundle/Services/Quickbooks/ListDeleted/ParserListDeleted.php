<?php

namespace QuickbooksBundle\Services\Quickbooks\ListDeleted;

use QuickbooksBundle\Entity\Dummy\ListDeleted;
use QuickbooksBundle\Services\App\ListDeleted\CreatorListDeletedByQbData;
use QuickbooksBundle\Services\Quickbooks\BaseParser;
use SimpleXMLElement;

class ParserListDeleted extends BaseParser
{
    /**
     * ParserListDeleted constructor.
     * @param CreatorListDeletedByQbData $creatorListDeletedByQbData
     */
    public function __construct(CreatorListDeletedByQbData $creatorListDeletedByQbData)
    {
        $this->creator = $creatorListDeletedByQbData;
    }

    /**
     * @param SimpleXMLElement $response
     * @return string|array|ListDeleted
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $termQuery = (array) $response->QBXMLMsgsRs->ListDeletedQueryRs;
        $term = (array) $termQuery['ListDeletedRet'];

        return $this->createResponse($term);
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingAdd(SimpleXMLElement $response)
    {
        // TODO: Implement parsingAdd() method.
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingModify(SimpleXMLElement $response)
    {
        // TODO: Implement parsingModify() method.
    }
}