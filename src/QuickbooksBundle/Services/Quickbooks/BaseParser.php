<?php

namespace QuickbooksBundle\Services\Quickbooks;

use SimpleXMLElement;

abstract class BaseParser
{
    /** @var object */
    protected $creator;

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    abstract public function parsingQuery(SimpleXMLElement $response);

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    abstract public function parsingAdd(SimpleXMLElement $response);

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    abstract public function parsingModify(SimpleXMLElement $response);

    /**
     * @param array $qbElements
     * @param string $idField
     * @return array
     */
    protected function createResponse(array $qbElements, $idField = 'ListID')
    {
        $res = [];

        if (array_key_exists($idField, $qbElements)) {
            $res[] = $this->creator->createObj($qbElements);

        } else {
            foreach ($qbElements as $oneRecord) {
                settype($oneRecord, 'array');

                $res[] = $this->creator->createObj($oneRecord);
            }
        }

        return $res;
    }
}