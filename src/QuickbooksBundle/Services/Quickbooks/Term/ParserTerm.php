<?php

namespace QuickbooksBundle\Services\Quickbooks\Term;

use AppBundle\Entity\PaymentTerm;
use QuickbooksBundle\Services\App\Term\CreatorTerm;
use QuickbooksBundle\Services\Quickbooks\BaseParser;
use SimpleXMLElement;

class ParserTerm extends BaseParser
{
    /**
     * ParserTerm constructor.
     * @param CreatorTerm $creatorTerm
     */
    public function __construct(CreatorTerm $creatorTerm)
    {
        $this->creator = $creatorTerm;
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $termQuery = (array) $response->QBXMLMsgsRs->TermsQueryRs;
        $term = (array) $termQuery['StandardTermsRet'];

        return $this->createResponse($term);
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingAdd(SimpleXMLElement $response)
    {
        // TODO: Implement parsingAdd() method.
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingModify(SimpleXMLElement $response)
    {
        // TODO: Implement parsingModify() method.
    }
}