<?php

namespace QuickbooksBundle\Services\Quickbooks;

class Helper
{
    /**
     * @param $string
     * @return string
     */
    public static function stringToSnakeAnnotationString($string)
    {
        $array = explode(' ', strtolower($string));
        $alias = '';
        foreach ($array as $key => $word) {
            $alias .= $key != 0 ? '_' . $word : $word;
        }

        return $alias;
    }
}