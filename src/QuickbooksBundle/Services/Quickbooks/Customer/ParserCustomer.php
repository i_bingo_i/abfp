<?php

namespace QuickbooksBundle\Services\Quickbooks\Customer;

use QuickbooksBundle\Services\App\Customer\CreatorCustomerByQbData;
use QuickbooksBundle\Services\Quickbooks\BaseParser;
use SimpleXMLElement;

class ParserCustomer extends BaseParser
{
    /**
     * ParserInvoice constructor.
     * @param CreatorCustomerByQbData $creatorInvoiceByQbData
     */
    public function __construct(CreatorCustomerByQbData $creatorInvoiceByQbData)
    {
        $this->creator = $creatorInvoiceByQbData;
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingAdd(SimpleXMLElement $response)
    {
        $customerRs = (array) $response->QBXMLMsgsRs->CustomerAddRs;
        $customer = (array) $customerRs['CustomerRet'];

        return $this->createResponse($customer);
    }
    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        // TODO: Implement parsingQuery() method.
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingModify(SimpleXMLElement $response)
    {
        $customerRs = (array) $response->QBXMLMsgsRs->CustomerModRs;
        $customer = (array) $customerRs['CustomerRet'];

        return $this->createResponse($customer);
    }
}