<?php

namespace QuickbooksBundle\Services\Quickbooks\Customer;

use InvoiceBundle\Entity\Customer;
use QuickbooksBundle\QBQuery\Customer\CustomerAdd;
use QuickbooksBundle\Services\Quickbooks\BaseParser;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;

class CreatorCustomer
{
    use CustomerAdd;

    /** @var QueryPreparer */
    private $queryPreparer;
    /** @var BaseParser */
    private $parserCustomer;

    /**
     * CreatorItemService constructor.
     * @param QueryPreparer $queryPreparer
     * @param ParserCustomer $parserCustomer
     */
    public function __construct(QueryPreparer $queryPreparer, ParserCustomer $parserCustomer)
    {
        $this->queryPreparer = $queryPreparer;
        $this->parserCustomer = $parserCustomer;
    }

    /**
     * @param Customer $customer
     * @return null|
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function createCustomer(Customer $customer)
    {
        $query = $this->add($customer);
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->CustomerAddRs->attributes();

        Validator::checkResponse($attributes['@attributes']);
        /** @var array $customers */
        $customers = $this->parserCustomer->parsingAdd($responseXMLObject);

        return array_shift($customers);
    }
}
