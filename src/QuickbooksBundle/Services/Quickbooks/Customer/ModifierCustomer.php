<?php

namespace QuickbooksBundle\Services\Quickbooks\Customer;

use InvoiceBundle\Entity\Customer;
use QuickbooksBundle\QBQuery\Customer\CustomerModifyTrait;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;

class ModifierCustomer
{
    use CustomerModifyTrait;

    /** @var QueryPreparer */
    private $queryPreparer;
    /** @var ParserCustomer */
    private $parserCustomer;

    /**
     * ModifierCustomer constructor.
     * @param QueryPreparer $queryPreparer
     * @param ParserCustomer $parserCustomer
     */
    public function __construct(QueryPreparer $queryPreparer, ParserCustomer $parserCustomer)
    {
        $this->queryPreparer = $queryPreparer;
        $this->parserCustomer = $parserCustomer;
    }

    /**
     * @param Customer $customer
     * @return mixed
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function modifyCustomer(Customer $customer)
    {
        $query = $this->modify($customer);
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->CustomerModRs->attributes();

        Validator::checkResponse($attributes['@attributes']);
        /** @var array $customers */
        $customers = $this->parserCustomer->parsingModify($responseXMLObject);

        return array_shift($customers);
    }
}