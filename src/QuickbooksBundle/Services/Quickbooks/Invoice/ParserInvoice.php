<?php

namespace QuickbooksBundle\Services\Quickbooks\Invoice;

use QuickbooksBundle\Services\App\Invoice\CreatorInvoiceByQbData;
use QuickbooksBundle\Services\Quickbooks\BaseParser;
use SimpleXMLElement;

class ParserInvoice extends BaseParser
{
    /**
     * ParserInvoice constructor.
     * @param CreatorInvoiceByQbData $creatorInvoiceByQbData
     */
    public function __construct(CreatorInvoiceByQbData $creatorInvoiceByQbData)
    {
        $this->creator = $creatorInvoiceByQbData;
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingAdd(SimpleXMLElement $response)
    {
        $invoiceRs = (array) $response->QBXMLMsgsRs->InvoiceAddRs;
        $invoices = (array) $invoiceRs['InvoiceRet'];

        return $this->createResponse($invoices, 'TxnID');
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingQuery(SimpleXMLElement $response)
    {
        $invoiceRs = (array) $response->QBXMLMsgsRs->InvoiceQueryRs;
        $invoices = (array) $invoiceRs['InvoiceRet'];

        return $this->createResponse($invoices, 'TxnID');
    }

    /**
     * @param SimpleXMLElement $response
     * @return array
     */
    public function parsingModify(SimpleXMLElement $response)
    {
        // TODO: Implement parsingModify() method.
    }
}