<?php

namespace QuickbooksBundle\Services\Quickbooks\Invoice;

use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\QBQuery\Invoice\InvoiceAdd;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;

class CreatorInvoice
{
    use InvoiceAdd;

    /** @var QueryPreparer */
    private $qp;
    /** @var ParserInvoice */
    private $parserInvoice;

    /**
     * CreatorInvoice constructor.
     * @param QueryPreparer $queryPreparer
     * @param ParserInvoice $parserInvoice
     */
    public function __construct(QueryPreparer $queryPreparer, ParserInvoice $parserInvoice)
    {
        $this->qp = $queryPreparer;
        $this->parserInvoice = $parserInvoice;
    }

    /**
     * @param Invoices $invoice
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(Invoices $invoice)
    {
        $query = $this->add($invoice);
        $response = $this->qp->getResponse($query);
        $attributes = (array) $response->QBXMLMsgsRs->InvoiceAddRs->attributes();

        Validator::checkResponse($attributes['@attributes']);
        $invoices = $this->parserInvoice->parsingAdd($response);

        return array_shift($invoices);
    }
}
