<?php

namespace QuickbooksBundle\Services\NativeQuickbooksConnector;

class Qb
{
    /**
     * Response object
     * @var string
     */
    public $response;
    /** @var string */
    public $login;
    /** @var string */
    public $password;
    /** @var string */
    public $ticket;

    /**
     * Constructor
     *
     * @return   void
     * @access   public
     * @version  2013-10-20
     */
    public function __construct()
    {
        $this->response = new Response();
    }


    /**
     * Function return client version
     *
     * @return  string
     * @param   object $param
     * @access  public
     * @version 2013-10-20
     */
    public function clientVersion($param = '')
    {
        $this->response->clientVersionResult = "";
        return $this->response;
    }


    /**
     * Function return server version
     *
     * @return  string
     * @access  public
     * @version 2013-10-20
     */
    public function serverVersion()
    {
        $this->response->serverVersionResult = "";
        return $this->response;
    }


    /**
     * Function try authenticate user by username/password
     *
     * @return  string
     * @param   object $param
     * @access  public
     * @version 2013-10-20
     */
    public function authenticate($param = '')
    {
        if (($param->strUserName == $this->login) && ($param->strPassword == $this->password)) {
            $this->response->authenticateResult = array($this->ticket, "");

        } else {
            $this->response->authenticateResult = array("", "nvu");
        }

        return $this->response;
    }


    /**
     * Function return last error
     *
     * @return  string
     * @param   object $param
     * @access  public
     * @version 2013-10-20
     */
    public function connectionError($param = '')
    {
        $this->response->connectionErrorResult = "connectionError";
        return $this->response;
    }


    /**
     * Function return last error
     *
     * @return  string
     * @param   object $param
     * @access  public
     * @version 2013-10-20
     */
    public function getLastError($param = '')
    {
        $this->response->getLastErrorResult = "getLastError";
        return $this->response;
    }


    /**
     * Function close connection
     *
     * @return  string
     * @param   object $param
     * @access  public
     * @version 2013-10-20
     */
    public function closeConnection($param = '')
    {
        $this->response->closeConnectionResult = "Complete";
        return $this->response;
    }
}