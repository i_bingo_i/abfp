<?php
namespace QuickbooksBundle\Services\NativeQuickbooksConnector;


class LogService
{
    /**
     * Log function
     *
     * @param string $mess
     */
    public static function log($mess = '')
    {
        $file_name = __DIR__ . '/../../../web/log/clients.log';
        if (!file_exists(dirname($file_name)))
            mkdir(dirname($file_name), 0777);

        $f = fopen($file_name, "ab");
        fwrite($f, "==============================================\n");
        fwrite($f, "[" . date("m/d/Y H:i:s") . "] ".$mess."\n");
        fclose($f);
    }

    /**
     * Log function
     *
     * @param string $id
     * @return string
     */
    public static function requestId($id = '')
    {
        $file_name = __DIR__ . '/../../../web/log/clients_id.log';
        if (!file_exists(dirname($file_name)))
            mkdir(dirname($file_name), 0777);

        // save id into file
        if(trim($id) !== ''){
            $f = fopen($file_name, "c+b");
            fwrite($f, $id);
            fclose($f);
        }

        $id = trim(file_get_contents($file_name));
        return $id;
    }
}