<?php

namespace QuickbooksBundle\Services\App\Term;

use DateTime;
use AppBundle\Entity\PaymentTerm;
use AppBundle\Entity\Repository\PaymentTermRepository;
use Doctrine\Common\Persistence\ObjectManager;
use QuickbooksBundle\Repository\QBRepository\QBTermRepository;
use QuickbooksBundle\Services\Quickbooks\Helper;

class Synchronizer
{
    /** @var ObjectManager */
    private $em;
    /** @var PaymentTermRepository */
    private $paymentTermRepository;
    /** @var QBTermRepository */
    private $qbTermRepository;
    /** @var TermProvider */
    private $termProvider;
    /** @var ModifierTerm */
    private $modifierTerm;

    /**
     * Synchronizer constructor.
     * @param ObjectManager $objectManager
     * @param QBTermRepository $QBTermRepository
     * @param ModifierTerm $modifierTerm
     * @param TermProvider $termProvider
     */
    public function __construct(
        ObjectManager $objectManager,
        QBTermRepository $QBTermRepository,
        ModifierTerm $modifierTerm,
        TermProvider $termProvider
    ) {
        $this->em = $objectManager;
        $this->paymentTermRepository = $this->em->getRepository('AppBundle:PaymentTerm');
        $this->qbTermRepository = $QBTermRepository;
        $this->modifierTerm = $modifierTerm;
        $this->termProvider = $termProvider;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function primarySynchronize()
    {
        /** @var array $QBTerms */
        $QBTerms = $this->qbTermRepository->getAllRecords();

        if (!empty($QBTerms)) {
            /** @var PaymentTerm $QBterm */
            foreach ($QBTerms as $QBterm) {
                $paymentTerm = $this->termProvider->getByQbName($QBterm->getName());

                if (!empty($paymentTerm)) {
                    $this->modifierTerm->updateQBFields($QBterm, $paymentTerm);
                    $this->em->flush();

                }
            }
        }
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function synchronizeItems()
    {
        /** @var string $from 'Y-m-d\TH:i:sP'*/
        $from = (new DateTime())->setTime(00, 00)->format(DateTime::ATOM);
        /** @var string $to 'Y-m-d\TH:i:sP'*/
        $to = (new DateTime())->setTime(23, 59, 59)->format(DateTime::ATOM);
        /** @var array $QBItems */
        $QBTerms = $this->qbTermRepository->getRecordsByModifiedDate($from, $to);

        if (!empty($QBTerms)) {
            /** @var PaymentTerm $term */
            foreach ($QBTerms as $term) {
                $appPaymentTerm = $this->termProvider->getByQbId($term->getAccountingSystemId());

                if (!empty($appPaymentTerm)) {
                    $this->updateOrCreate($term, $appPaymentTerm);

                }
            }
        }
    }

    /**
     * @param PaymentTerm $QBTerm
     * @param null|PaymentTerm $appPaymentTerm
     */
    private function updateOrCreate(PaymentTerm $QBTerm, ?PaymentTerm $appPaymentTerm = null)
    {
        if ($appPaymentTerm && ($QBTerm->getEditSequence() != $appPaymentTerm->getEditSequence())) {
            $this->modifierTerm->updateExistingRecord($QBTerm, $appPaymentTerm);

        } else {
            $this->em->persist($QBTerm);

        }

        $this->em->flush();
    }
}