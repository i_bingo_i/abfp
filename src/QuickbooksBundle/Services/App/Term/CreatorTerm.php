<?php

namespace QuickbooksBundle\Services\App\Term;

use AppBundle\Entity\PaymentTerm;
use QuickbooksBundle\Services\Quickbooks\Helper;

class CreatorTerm
{
    /**
     * @param array $term
     * @return PaymentTerm|array
     */
    public function createObj(array $term)
    {
        $newTerm = new PaymentTerm();
        $newTerm->setName($term['Name']);
        $newTerm->setAlias(Helper::stringToSnakeAnnotationString($term['Name']));
        $newTerm->setDescription($term['Name']);
        $newTerm->setAccountingSystemId($term['ListID']);
        $newTerm->setEditSequence($term['EditSequence']);

        return $newTerm;
    }
}