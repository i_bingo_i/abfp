<?php

namespace QuickbooksBundle\Services\App\Term;

use AppBundle\Entity\PaymentTerm;
use AppBundle\Entity\Repository\PaymentTermRepository;

class TermProvider
{
    /** @var PaymentTermRepository */
    private $paymentTermRepository;
    /** @var CreatorTerm */
    private $creatorTerm;
    /** @var array */
    private $conformity = [
        'Net 15' => PaymentTerm::NET_FIFTEEN_DAYS,
        'Net 30' => PaymentTerm::NET_THIRTY_DAYS,
        'COD' => PaymentTerm::COD,
        'Pre-Paid' => PaymentTerm::FULL_PAID_BEFORE_STARTED,
        '50% Deposit Due' => PaymentTerm::FIFTY_PERCENT_DEPOSIT
    ];

    /**
     * TermProvider constructor.
     * @param PaymentTermRepository $paymentTermRepository
     * @param CreatorTerm $creatorTerm
     */
    public function __construct(PaymentTermRepository $paymentTermRepository, CreatorTerm $creatorTerm)
    {
        $this->paymentTermRepository = $paymentTermRepository;
        $this->creatorTerm = $creatorTerm;
    }

    /**
     * @param string $name
     * @return null|PaymentTerm
     */
    public function getByQbName($name)
    {
        $paymentTerm = null;

        if (array_key_exists($name, $this->conformity)) {
            /** @var PaymentTerm $paymentTerm */
            $paymentTerm = $this->paymentTermRepository->findOneBy(
                ['alias' => $this->conformity[$name]]
            );

        }

        return $paymentTerm;
    }

    /**
     * @param $qbId
     * @return PaymentTerm
     */
    public function getByQbId($qbId)
    {
        /** @var PaymentTerm $paymentTerm */
        $paymentTerm = $this->paymentTermRepository->findOneBy(['accountingSystemId' => $qbId]);

        return $paymentTerm;
    }

    /**
     * @param array $qbTerm
     * @return PaymentTerm|null
     */
    public function getByQbData(array $qbTerm)
    {
        if (!$paymentTerm = $this->getByQbId($qbTerm['ListID'])) {
            $paymentTerm = $this->getByQbName($qbTerm['FullName']);
        }

        return $paymentTerm;
    }
}