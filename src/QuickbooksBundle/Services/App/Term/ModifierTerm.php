<?php

namespace QuickbooksBundle\Services\App\Term;

use AppBundle\Entity\PaymentTerm;

class ModifierTerm
{
    /**
     * @param PaymentTerm $QBTerm
     * @param PaymentTerm $AppPaymentTerm
     */
    public function updateExistingRecord(PaymentTerm $QBTerm, PaymentTerm $AppPaymentTerm)
    {
            $AppPaymentTerm->setAccountingSystemId($QBTerm->getAccountingSystemId());
            $AppPaymentTerm->setEditSequence($QBTerm->getEditSequence());
            $AppPaymentTerm->setName($QBTerm->getName());
            $AppPaymentTerm->setAlias($QBTerm->getAlias());
            $AppPaymentTerm->setDescription($QBTerm->getDescription());
    }

    /**
     * @param PaymentTerm $QBTerm
     * @param PaymentTerm $AppPaymentTerm
     */
    public function updateQBFields(PaymentTerm $QBTerm, PaymentTerm $AppPaymentTerm)
    {
        $AppPaymentTerm->setAccountingSystemId($QBTerm->getAccountingSystemId());
        $AppPaymentTerm->setEditSequence($QBTerm->getEditSequence());
    }
}