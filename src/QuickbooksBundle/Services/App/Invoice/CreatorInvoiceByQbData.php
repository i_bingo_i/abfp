<?php

namespace QuickbooksBundle\Services\App\Invoice;

use AppBundle\Entity\Address;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\InvoiceLine;
use QuickbooksBundle\Services\App\Address\AddressProvider;
use QuickbooksBundle\Services\App\InvoiceLine\InvoiceLineProvider;
use QuickbooksBundle\Services\App\Term\TermProvider;

class CreatorInvoiceByQbData
{
    /** @var AddressProvider */
    private $addressProvider;
    /** @var TermProvider */
    private $termProvider;
    /** @var InvoiceLineProvider */
    private $invoiceLineProvider;


    public function __construct(
        AddressProvider $addressProvider,
        TermProvider $termProvider,
        InvoiceLineProvider $invoiceLineProvider
    ) {
        $this->addressProvider = $addressProvider;
        $this->termProvider = $termProvider;
        $this->invoiceLineProvider = $invoiceLineProvider;
    }

    /**
     * @param array $qbInvoice
     * @return Invoices
     */
    public function createObj(array $qbInvoice)
    {
        $invoice = new Invoices();
        $invoice->setAccountingSystemId($qbInvoice['TxnID']);
        $invoice->setEditSequence($qbInvoice['EditSequence']);
        $invoice->setTotal($qbInvoice['Subtotal'] ?? 0);
        $invoice->setPaymentApplied($qbInvoice['AppliedAmount'] ?? 0);
        $invoice->setBalanceDue($qbInvoice['BalanceRemaining'] ?? 0);
        $invoice->setMemo($qbInvoice['Memo'] ?? '');
        $invoice->setRefNumber($qbInvoice['RefNumber'] ?? '');

        $this->setPaymentTerm($invoice, $qbInvoice);
        $this->setBillToAddress($invoice, $qbInvoice);
        $this->setShipToAddress($invoice, $qbInvoice);
        $this->setLine($invoice, $qbInvoice);

        return $invoice;
    }

    /**
     * @param Invoices $invoice
     * @param array $qbInvoice
     */
    private function setPaymentTerm(Invoices $invoice, array $qbInvoice)
    {
        if (isset($qbInvoice['TermsRef'])) {
            settype($qbInvoice['TermsRef'], 'array');
            $invoice->setPaymentTerm($this->termProvider->getByQbData($qbInvoice['TermsRef']));
        }
    }

    /**
     * @param Invoices $invoice
     * @param array $qbInvoice
     */
    private function setBillToAddress(Invoices $invoice, array $qbInvoice)
    {
        if (isset($qbInvoice['BillAddress'])) {
            settype($qbInvoice['BillAddress'], 'array');
            /** @var Address $billToAddress */
            $billToAddress = $this->addressProvider->getAddress($qbInvoice['BillAddress']);
            $invoice->setBillToAddress($billToAddress);
        }
    }

    /**
     * @param Invoices $invoice
     * @param array $qbInvoice
     */
    private function setShipToAddress(Invoices $invoice, array $qbInvoice)
    {
        if (isset($qbInvoice['ShipAddress'])) {
            settype($qbInvoice['ShipAddress'], 'array');
            /** @var Address $billToAddresses */
            $shipToAddresses = $this->addressProvider->getAddress($qbInvoice['ShipAddress']);
            /** @var Address $shipToAddress */
            $invoice->setShipToAddress($shipToAddresses);
        }
    }

    /**
     * @param Invoices $invoice
     * @param array $qbInvoice
     */
    private function setLine(Invoices $invoice, array $qbInvoice)
    {
        if (isset($qbInvoice['InvoiceLineRet'])) {
            settype($qbInvoice['InvoiceLineRet'], 'array');
            $invoiceLines = $this->invoiceLineProvider->getByQbInvoiceLine($qbInvoice['InvoiceLineRet']);
            /** @var InvoiceLine $line */
            foreach ($invoiceLines as $line) {
                $invoice->addLine($line);
            }
        }
    }
}