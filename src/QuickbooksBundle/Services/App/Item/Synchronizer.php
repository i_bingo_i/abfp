<?php

namespace QuickbooksBundle\Services\App\Item;

use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use QuickbooksBundle\Entity\Dummy\ListDeleted;
use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Exceptions\FailIQuickbooksAction;
use QuickbooksBundle\Managers\InvoiceManager;
use QuickbooksBundle\Repository\ItemRepository;
use QuickbooksBundle\Repository\QBRepository\QBItemRepository;
use QuickbooksBundle\Repository\QBRepository\QBListDeletedRepository;

class Synchronizer
{
    /** @var ObjectManager */
    private $em;
    /** @var QBItemRepository */
    private $qbItemServiceRepository;
    /** @var ItemProvider */
    private $itemProvider;
    /** @var QBListDeletedRepository */
    private $listDeletedRepository;
    /** @var ModifierItem */
    private $modifierItem;
    /** @var ItemRepository */
    private $itemRepository;
    /** @var InvoiceManager */
    private $invoiceManager;

    /**
     * Synchronizer constructor.
     * @param ObjectManager $objectManager
     * @param QBItemRepository $QBItemServiceRepository
     * @param ItemProvider $itemProvider
     * @param QBListDeletedRepository $listDeletedRepository
     * @param ModifierItem $modifierItem
     */
    public function __construct(
        ObjectManager $objectManager,
        QBItemRepository $QBItemServiceRepository,
        ItemProvider $itemProvider,
        QBListDeletedRepository $listDeletedRepository,
        ModifierItem $modifierItem,
        ItemRepository $itemRepository,
        InvoiceManager $invoiceManager
    ) {
        $this->em = $objectManager;
        $this->qbItemServiceRepository = $QBItemServiceRepository;
        $this->itemProvider = $itemProvider;
        $this->listDeletedRepository = $listDeletedRepository;
        $this->modifierItem = $modifierItem;
        $this->itemRepository = $itemRepository;
        $this->invoiceManager = $invoiceManager;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createPrimaryList()
    {
        /** @var array $QBItems */
        $QBItems = $this->qbItemServiceRepository->getAllRecords();

        if (!empty($QBItems)) {
            /** @var Item $itemService */
            foreach ($QBItems as $item) {
                if ($item instanceof Item) {
                    $existingItem = $this->itemRepository->findBy(['qbId' => $item->getQbId()]);
                    if (empty($existingItem)) {
                        $this->em->persist($item);
                    }
                }
            }

            $this->em->flush();
        }
    }

    /**
     * @param $accessToken
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function synchronizeItems($accessToken)
    {
        try {
            /** @var string $from 'Y-m-d\TH:i:sP'*/
            $from = (new DateTime())->setTime(00, 00)->format(DateTime::ATOM);
            /** @var string $to 'Y-m-d\TH:i:sP'*/
            $to = (new DateTime())->setTime(23, 59, 59)->format(DateTime::ATOM);

            $response = $this->invoiceManager->getByModifiedDate($from, $to, $accessToken);
        } catch (FailIQuickbooksAction $exception) {
            return false;
        }

        /** @var array $QBItems */
        $QBItems = $response->get('ResultItems');
        if (!empty($QBItems)) {
            $this->synchronizeExisting($QBItems);
        }

        return true;
    }

    /**
     * @param Item $QBItemService
     * @param null|Item $appItemService
     */
    private function updateOrCreate(Item $QBItemService, ?Item $appItemService = null)
    {
        if ($appItemService) {
            $this->modifierItem->updateExistingRecord($QBItemService, $appItemService);

        } elseif ($QBItemService->getIsActive()) {
            $this->em->persist($QBItemService);
        }

        $this->em->flush();
    }

    /**
     * @param array $QBItems
     */
    private function synchronizeExisting(array $QBItems)
    {
        /** @var Item $item */
        foreach ($QBItems as $item) {
            /** @var Item | null $appItemService */
            $appItemService = $this->itemProvider->getOneByQbId($item->getQbId());
            $this->updateOrCreate($item, $appItemService);
        }
    }

    /**
     * @param array $deletedItems
     */
    private function synchronizeDeleted(array $deletedItems)
    {
        $QBitemIds = array_map(function (ListDeleted $listDeleted) {
            return $listDeleted->getListID();
        }, $deletedItems);

        /** @var array | null $appItems */
        $appItems = $this->itemProvider->getByQbId($QBitemIds);

        if (!empty($appItems)) {
            /** @var Item $item */
            foreach ($appItems as $item) {
                $this->em->remove($item);
                $this->em->flush();
            }
        }
    }

}