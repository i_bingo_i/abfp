<?php

namespace QuickbooksBundle\Services\App\Item;

use QuickbooksBundle\Entity\Item;

class ModifierItem
{
    /**
     * @param Item $QBItemService
     * @param Item $AppItemService
     */
    public function updateExistingRecord(Item $QBItemService, Item $AppItemService)
    {
            $AppItemService->setQbId($QBItemService->getQbId());
            $AppItemService->setQbEditSequence($QBItemService->getQbEditSequence());
            $AppItemService->setQbTimeModified($QBItemService->getQbTimeModified());
            $AppItemService->setName($QBItemService->getName());
            $AppItemService->setDescription($QBItemService->getDescription());
            $AppItemService->setType($QBItemService->getType());
            $AppItemService->setRate($QBItemService->getRate());
            $AppItemService->setRateType($QBItemService->getRateType());
            $AppItemService->setIsActive($QBItemService->getIsActive());
    }
}