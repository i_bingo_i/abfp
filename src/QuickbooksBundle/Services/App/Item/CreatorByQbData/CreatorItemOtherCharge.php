<?php

namespace QuickbooksBundle\Services\App\Item\CreatorByQbData;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemRateType;
use QuickbooksBundle\Entity\ItemType;

class CreatorItemOtherCharge extends BaseCreatorItem implements InterfaceCreatorItem
{

    /**
     * @param array $item
     * @param ItemType|null $itemType
     * @param ItemRateType|null $rateType
     * @param null $rate
     * @return Item
     */
    public function createObj(
        array $item,
        ItemType $itemType = null,
        ItemRateType $rateType = null,
        $rate = null
    ) {
        $newItemOtherChargeObj = $this->createItem($item);
        $newItemOtherChargeObj->setType($itemType);

        if (isset($item['SalesOrPurchase'])) {
            settype($item['SalesOrPurchase'], 'array');
            $newItemOtherChargeObj->setDescription($item['SalesOrPurchase']['Desc'] ?? '');
            $newItemOtherChargeObj->setRate($rate);
            $newItemOtherChargeObj->setRateType($rateType);
        }

        return $newItemOtherChargeObj;
    }
}