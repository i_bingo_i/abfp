<?php

namespace QuickbooksBundle\Services\App\Item\CreatorByQbData;

use DateTime;
use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemRateType;
use QuickbooksBundle\Entity\ItemType;

class BaseCreatorItem implements InterfaceCreatorItem
{
    /**
     * @param array $item
     * @param ItemType|null $itemType
     * @param ItemRateType|null $rateType
     * @param null $rate
     * @return Item
     */
    public function createObj(
        array $item,
        ItemType $itemType = null,
        ItemRateType $rateType = null,
        $rate = null
    ) {
        return new Item();
    }

    /**
     * @param array $item
     * @return Item
     */
    protected function createItem(array $item)
    {
        $newItemServiceObj = new Item();
        $newItemServiceObj->setQbId($item['ListID'] ?? '');
        $newItemServiceObj->setQbTimeModified(new DateTime($item['TimeModified']) ?? '');
        $newItemServiceObj->setQbEditSequence($item['EditSequence'] ?? '');
        $newItemServiceObj->setName($item['Name'] ?? '');
        $newItemServiceObj->setIsActive((bool)filter_var($item['IsActive'], FILTER_VALIDATE_BOOLEAN));

        return $newItemServiceObj;
    }
}