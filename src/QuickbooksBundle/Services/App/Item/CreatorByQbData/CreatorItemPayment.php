<?php

namespace QuickbooksBundle\Services\App\Item\CreatorByQbData;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemRateType;
use QuickbooksBundle\Entity\ItemType;

class CreatorItemPayment extends BaseCreatorItem implements InterfaceCreatorItem
{

    /**
     * @param array $item
     * @param ItemType|null $itemType
     * @param ItemRateType|null $rateType
     * @param null $rate
     * @return Item
     */
    public function createObj(
        array $item,
        ItemType $itemType = null,
        ItemRateType $rateType = null,
        $rate = null
    ) {
        /** @var Item $newItemOtherChargeObj */
        $newItemPayment = $this->createItem($item);
        $newItemPayment->setType($itemType);
        $newItemPayment->setDescription($item['ItemDesc'] ?? '');

        return $newItemPayment;
    }
}