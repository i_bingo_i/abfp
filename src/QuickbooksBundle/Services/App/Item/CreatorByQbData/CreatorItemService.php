<?php

namespace QuickbooksBundle\Services\App\Item\CreatorByQbData;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Entity\ItemRateType;
use QuickbooksBundle\Entity\ItemType;

class CreatorItemService extends BaseCreatorItem implements InterfaceCreatorItem
{
    /**
     * @param array $item
     * @param ItemType|null $itemType
     * @param ItemRateType|null $rateType
     * @param null $rate
     * @return Item
     */
    public function createObj(
        array $item,
        ItemType $itemType = null,
        ItemRateType $rateType = null,
        $rate = null
    ) {
        $newItemService = $this->createItem($item);
        $newItemService->setType($itemType);

        if (isset($item['SalesOrPurchase'])) {
            settype($item['SalesOrPurchase'], 'array');
            $newItemService->setDescription($item['SalesOrPurchase']['Desc'] ?? '');
            $newItemService->setRate($item['SalesOrPurchase']['Price'] ?? null);
            $newItemService->setRateType($rateType);

        }

        return $newItemService;
    }
}