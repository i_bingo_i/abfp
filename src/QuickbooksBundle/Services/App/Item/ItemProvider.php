<?php

namespace QuickbooksBundle\Services\App\Item;

use QuickbooksBundle\Entity\Item;
use QuickbooksBundle\Repository\ItemRepository;

class ItemProvider
{
    /** @var ItemRepository */
    private $itemRepository;

    /**
     * ItemProvider constructor.
     * @param ItemRepository $itemRepository
     */
    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param array|int $id
     * @return Item | null
     */
    public function getOneByQbId($id)
    {
        /** @var Item | null $item */
        $item = $this->itemRepository->findOneBy(['qbId' => $id]);

        return $item;
    }

    /**
     * @param array|int $id
     * @return array|null
     */
    public function getByQbId($id)
    {
        /** @var array | null $item */
        $item = $this->itemRepository->findBy(['qbId' => $id]);

        return $item;
    }
}