<?php

namespace QuickbooksBundle\Services\App\InvoiceLine;

use InvoiceBundle\Entity\InvoiceLine;
use QuickbooksBundle\Services\App\Item\ItemProvider;

class CreatorInvoiceLine
{
    /** @var ItemProvider */
    private $itemProvider;

    public function __construct(ItemProvider $itemProvider)
    {
        $this->itemProvider = $itemProvider;
    }

    /**
     * @param array $qbInvoiceLine
     * @return InvoiceLine
     */
    public function createByQbData(array $qbInvoiceLine)
    {
        $invoiceLine = new InvoiceLine();
        $invoiceLine->setAccountingSystemId($qbInvoiceLine['TxnLineID']);
        $invoiceLine->setDescription($qbInvoiceLine['Desc'] ?? null);
        $invoiceLine->setQuantity($qbInvoiceLine['Quantity'] ?? null);
        $invoiceLine->setAmount($qbInvoiceLine['Amount'] ?? null);
        $invoiceLine->setRate($qbInvoiceLine['Rate'] ?? null);
        $this->setItem($invoiceLine, $qbInvoiceLine);

        return $invoiceLine;
    }

    /**
     * @param InvoiceLine $invoiceLine
     * @param array $qbInvoiceLine
     */
    private function setItem(InvoiceLine $invoiceLine, array $qbInvoiceLine)
    {
        if (isset($qbInvoiceLine['ItemRef'])) {
            settype($qbInvoiceLine['ItemRef'], 'array');
            $invoiceLine->setItem($this->itemProvider->getOneByQbId($qbInvoiceLine['ItemRef']['ListID']));
        }
    }
}