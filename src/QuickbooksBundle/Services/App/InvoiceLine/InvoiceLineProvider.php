<?php

namespace QuickbooksBundle\Services\App\InvoiceLine;

use InvoiceBundle\Repository\InvoiceLineRepository;

class InvoiceLineProvider
{
    /** @var InvoiceLineRepository */
    private $invoiceLineRepository;
    /** @var CreatorInvoiceLine */
    private $creatorInvoiceLine;

    /**
     * InvoiceLineProvider constructor.
     * @param InvoiceLineRepository $invoiceLineRepository
     * @param CreatorInvoiceLine $creatorInvoiceLine
     */
    public function __construct(
        InvoiceLineRepository $invoiceLineRepository,
        CreatorInvoiceLine $creatorInvoiceLine
    ) {
        $this->invoiceLineRepository = $invoiceLineRepository;
        $this->creatorInvoiceLine = $creatorInvoiceLine;
    }

    /**
     * @param array $qbInvoiceLines
     * @return array
     */
    public function getByQbInvoiceLine(array $qbInvoiceLines)
    {
        $invoiceLines = [];

        if (key_exists('TxnLineID', $qbInvoiceLines)) {
            $invoiceLines[] = $this->creatorInvoiceLine->createByQbData($qbInvoiceLines);

        } else {
            foreach ($qbInvoiceLines as $line) {
                settype($line, 'array');
                $invoiceLines[] = $this->creatorInvoiceLine->createByQbData($line);
            }

        }

        return $invoiceLines;
    }
}