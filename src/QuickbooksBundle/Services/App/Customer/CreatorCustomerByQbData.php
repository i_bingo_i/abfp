<?php

namespace QuickbooksBundle\Services\App\Customer;

use AppBundle\Entity\Address;
use InvoiceBundle\Entity\Customer;
use QuickbooksBundle\Services\App\Address\AddressProvider;

class CreatorCustomerByQbData
{
    /** @var AddressProvider */
    private $addressProvider;

    /**
     * CreatorCustomerByQbData constructor.
     * @param AddressProvider $addressProvider
     */
    public function __construct(AddressProvider $addressProvider)
    {
        $this->addressProvider = $addressProvider;
    }

    /**
     * @param array $qbCustomer
     * @return Customer
     */
    public function createObj(array $qbCustomer)
    {
        $customer = new Customer();
        $customer->setAccountingSystemId($qbCustomer['ListID']);
        $customer->setEditSequence($qbCustomer['EditSequence']);
        $customer->setName($qbCustomer['Name']);

        $this->setBillToAddress($customer, $qbCustomer);

        return $customer;
    }

    /**
     * @param Customer $customer
     * @param array $qbCustomer
     */
    private function setBillToAddress(Customer $customer, array $qbCustomer)
    {
        if (isset($qbCustomer['BillAddress'])) {
            settype($qbCustomer['BillAddress'], 'array');
            /** @var Address $billToAddress */
            $billToAddress = $this->addressProvider->getAddress($qbCustomer['BillAddress']);
            $customer->setBillingAddress($billToAddress);
        }
    }
}
