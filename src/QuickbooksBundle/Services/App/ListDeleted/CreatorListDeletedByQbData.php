<?php

namespace QuickbooksBundle\Services\App\ListDeleted;

use DateTime;
use QuickbooksBundle\Entity\Dummy\ListDeleted;

class CreatorListDeletedByQbData
{
    /**
     * @param array $listDeleted
     * @return ListDeleted
     */
    public function createObj(array $listDeleted)
    {
        $newListDeleted = new ListDeleted();
        $newListDeleted->setListID($listDeleted['ListID']);
        $newListDeleted->setFullName($listDeleted['FullName']);
        $newListDeleted->setListDelType($listDeleted['ListDelType']);
        $newListDeleted->setTimeCreated(new DateTime($listDeleted['TimeCreated']));
        $newListDeleted->setTimeDeleted(new DateTime($listDeleted['TimeDeleted']));

        return $newListDeleted;
    }
}