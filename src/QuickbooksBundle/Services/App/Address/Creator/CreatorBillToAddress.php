<?php

namespace QuickbooksBundle\Services\App\Address\Creator;

use AppBundle\Entity\Address;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\State;

class CreatorBillToAddress extends BaseAddressCreator
{
    /**
     * CreatorBillAddress constructor.
     * @param StateRepository $stateRepository
     */
    public function __construct(StateRepository $stateRepository)
    {
        parent::__construct($stateRepository);
    }

    /**
     * @param array $qbAddress
     * @param State $state
     * @param string $address
     * @return Address
     */
    public function createByQb($qbAddress, $state, $address)
    {
        $billToAddress = new Address();
        $billToAddress->setAddress($address);
        $billToAddress->setCity($qbAddress['City'] ?? '');
        $billToAddress->setState($state);
        $billToAddress->setZip($qbAddress['PostalCode'] ?? '');

        return $billToAddress;
    }
}