<?php

namespace QuickbooksBundle\Services\App\Address\Creator;

use AppBundle\Entity\Address;
use AppBundle\Entity\Repository\StateRepository;
use AppBundle\Entity\State;

abstract class BaseAddressCreator implements InterfaceAddressCreator
{
    /** @var StateRepository */
    private $stateRepository;

    /**
     * CreatorBillAddress constructor.
     * @param StateRepository $stateRepository
     */
    public function __construct(StateRepository $stateRepository)
    {
        $this->stateRepository = $stateRepository;
    }

    /**
     * @param array $qbAddress
     * @return Address
     */
    public function creatingByQbData(array $qbAddress)
    {
        $number = 1;
        /** @var State|null $state */
        $state = isset($qbAddress['State']) ? $this->getState($qbAddress['State']) : null;
        /** @var string $result */
        $address = '';

        while (isset($qbAddress['Addr' . $number])) {
            $nextLine = $qbAddress['Addr' . $number];
            $address .= !empty($address) ? PHP_EOL . $nextLine : $nextLine;
            $number++;
        }

        return $this->createByQb($qbAddress, $state, $address);
    }

    /**
     * @param array $qbAddress
     * @param State $state
     * @param string $address
     * @return Address
     */
    abstract public function createByQb($qbAddress, $state, $address);

    /**
     * @param string $state
     * @return object | null
     */
    protected function getState($state)
    {
        return $this->stateRepository->findOneBy(['code' => strtoupper(trim($state))]);
    }
}