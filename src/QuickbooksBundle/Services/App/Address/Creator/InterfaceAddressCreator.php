<?php

namespace QuickbooksBundle\Services\App\Address\Creator;

use AppBundle\Entity\Address;
use AppBundle\Entity\State;

interface InterfaceAddressCreator
{
    /**
     * @param array $qbAddress
     * @param State $state
     * @param integer $number
     * @return Address
     */
    public function createByQb($qbAddress, $state, $number);
}