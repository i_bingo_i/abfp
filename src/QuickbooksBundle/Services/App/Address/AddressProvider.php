<?php

namespace QuickbooksBundle\Services\App\Address;

use AppBundle\Entity\Address;
use AppBundle\Entity\Repository\AddressRepository;
use QuickbooksBundle\Services\App\Address\Creator\CreatorBillToAddress;

class AddressProvider
{
    /** @var CreatorBillToAddress */
    private $creatorBillToAddress;
    /** @var AddressRepository */
    private $billToAddressRepository;


    public function __construct(
        CreatorBillToAddress $creatorBillToAddress,
        AddressRepository $billToAddressRepository
    ) {
        $this->creatorBillToAddress = $creatorBillToAddress;
        $this->billToAddressRepository = $billToAddressRepository;
    }

    /**
     * @param array $address
     * @return Address
     */
    public function getAddress($address)
    {
        $newBillToAddresses = $this->creatorBillToAddress->creatingByQbData($address);

        $resultBillToAddressesCollection = $this->billToAddressRepository->findOneBy([
                'address' => $newBillToAddresses->getAddress()
            ]) ?? $newBillToAddresses;

        return $resultBillToAddressesCollection;
    }
}
