<?php

namespace QuickbooksBundle\Services\App\Job;

use InvoiceBundle\Entity\Job;

class CreatorJobByQbData
{
    /**
     * @param array $qbJob
     * @return Job
     */
    public function createObj(array $qbJob)
    {
        $job = new Job();
        $job->setAccountingSystemId($qbJob['ListID']);
        $job->setEditSequence($qbJob['EditSequence']);

        return $job;
    }
}
