<?php

namespace QuickbooksBundle\QBQuery;

class ConnectionOptions
{
    /** @var string */
    private $host;
    /** @var string */
    private $login;
    /** @var string */
    private $password;
    /** @var int */
    private $timeConnectTimeout;
    /** @var int */
    private $timeout;

    /**
     * @param $host
     *
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return string
     */
    public function host() : string
    {
        return $this->host;
    }

    /**
     * @param $login
     *
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return string
     */
    public function login() : string
    {
        return $this->login;
    }

    /**
     * @param $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function password() : string
    {
        return $this->password;
    }

    /**
     * @param $timeConnectTimeout
     *
     * @return $this
     */
    public function setTimeConnectTimeout($timeConnectTimeout)
    {
        $this->timeConnectTimeout = $timeConnectTimeout;

        return $this;
    }

    /**
     * @return int
     */
    public function timeConnectTimeout() : int
    {
        return $this->timeConnectTimeout;
    }

    /**
     * @param $timeout
     *
     * @return $this
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @return int
     */
    public function timeout() : int
    {
        return $this->timeout;
    }
}
