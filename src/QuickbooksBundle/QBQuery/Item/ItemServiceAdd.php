<?php

namespace QuickbooksBundle\QBQuery\Item;

use QuickbooksBundle\QBQuery\BaseQuery;

trait ItemServiceAdd
{
    use BaseQuery;

    private function addCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'ItemServiceAddRq' => [
                    'ItemServiceAdd' => $fields
                ]
            ]
        );
    }

    protected function add()
    {
        return $this->addCarcassOfRequest([
            'Name' => 'Test event three',
            'IsActive' => 'true',
            'SalesOrPurchase' => [
                'Desc' => 'Test event three',
                'Price' => '12345.00',
                'AccountRef' => [
                    'FullName' => 'Services'
                ]
            ]
        ]);
    }
}