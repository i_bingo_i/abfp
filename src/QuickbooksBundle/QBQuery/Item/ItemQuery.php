<?php

namespace QuickbooksBundle\QBQuery\Item;

use QuickbooksBundle\QBQuery\BaseQuery;

trait ItemQuery
{
    use BaseQuery;

    /**
     * @param array $fields
     * @return array
     */
    private function queryCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'ItemQueryRq' => $fields
            ]
        );
    }

    /**
     * @return array
     */
    protected function getAll()
    {
        return $this->getById();
    }

    /**
     * @param string $id
     * @return array
     */
    protected function getById($id = '')
    {
        /** @var array $fields */
        $fields = [
            'ListID' => $id
        ];

        return $this->queryCarcassOfRequest($fields);
    }

    /**
     * @param $from
     * @param $to
     * @return array
     */
    protected function getByModifiedDate($from, $to)
    {
        $fields = [
            'ActiveStatus' => 'All',
            'FromModifiedDate' => $from,
            'ToModifiedDate' => $to
        ];

        return $this->queryCarcassOfRequest($fields);
    }
}