<?php

namespace QuickbooksBundle\QBQuery\Item;

use QuickbooksBundle\QBQuery\BaseQuery;

trait ItemServiceMod
{
    use BaseQuery;

    /**
     * @param array $fields
     * @return array
     */
    private function modifyCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'ItemServiceModRq' => [
                    'ItemServiceMod' => $fields
                ]
            ]
        );
    }

    /**
     *
     * @param array $newValues
     * @return array
     * @throws \Exception
     */
    protected function modifyItemService(array $newValues)
    {
        if (!isset($newValues['ListID']) && !isset($newValues['EditSequence'])) {
            throw new \Exception('ListID and EditSequence are required fields');
        }

        return $this->modifyCarcassOfRequest($newValues);
    }
}