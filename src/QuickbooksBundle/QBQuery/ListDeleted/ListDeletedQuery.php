<?php

namespace QuickbooksBundle\QBQuery\ListDeleted;

use QuickbooksBundle\QBQuery\BaseQuery;

trait ListDeletedQuery
{
    use BaseQuery;

    private function queryCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'ListDeletedQueryRq' => $fields
            ]
        );
    }

    /**
     * @param $from
     * @param $to
     * @return array
     */
    protected function getItems($from, $to)
    {
        return $this->queryCarcassOfRequest([
            'ListDelType' => [
                'ItemDiscount', 'ItemGroup', 'ItemOtherCharge', 'ItemPayment', 'ItemService', 'ItemSubtotal'
            ],
            'DeletedDateRangeFilter' => [
                'FromDeletedDate' => $from,
                'ToDeletedDate' => $to
            ]
        ]);
    }
}