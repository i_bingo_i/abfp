<?php

namespace QuickbooksBundle\QBQuery;

use DOMNode;
use DOMDocument;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class QueryFactory
{
    /**
     * @param QueryDTO $queryDTO
     *
     * @return Query
     */
    public static function make(QueryDTO $queryDTO)
    {
        if ($queryDTO->queryType() != Query::TYPE_RAW_XML) {
            $queryStructure = self::createStructure(
                $queryDTO->entityName(),
                $queryDTO->queryType(),
                $queryDTO->parameters()
            );

            /** @var Query $qbQuery */
            $qbQuery = self::makeByQueryStructure($queryStructure);
        } else {
            /** @var Query $qbQuery */
            $qbQuery = self::makeByXml($queryDTO->rawXmlRequest());
        }

        if ($queryDTO->parser()) {
            $qbQuery->setParser($queryDTO->parser());
        }

        return $qbQuery;
    }

    /**
     * @param $queryStructure
     *
     * @return Query
     */
    public static function makeByQueryStructure($queryStructure)
    {
        $qbQuery = new Query();
        $xmlBody = self::createXmlQuery($queryStructure);
        $qbQuery->setBody($xmlBody);
        self::setDefaultHeaders($qbQuery);

        return $qbQuery;
    }

    /**
     * @param $xmlRequest
     *
     * @return Query
     */
    public static function makeByXml($xmlRequest)
    {
        $qbQuery = new Query();
        $qbQuery->setBody($xmlRequest);
        self::setDefaultHeaders($qbQuery);

        return $qbQuery;
    }

    /**
     * @param $queryStructure
     *
     * @return string
     */
    private static function createXmlQuery($queryStructure)
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $encoder = new XmlEncoder('QBXML');
        $queryXML = $encoder->encode($queryStructure, 'xml');

        $dom->loadXML($queryXML);

        /** @var DOMNode $rootNode */
        $rootNode = $dom->getElementsByTagName('QBXML')->item(0);
        $dom->insertBefore($dom->createProcessingInstruction('qbxml', 'version="13.0"'), $rootNode);

        return $dom->saveXml();
    }

    /**
     * @param $entityName
     * @param $queryType
     * @param array $parameters
     *
     * @return array|bool
     */
    private static function createStructure($entityName, $queryType, array $parameters)
    {
        if (in_array($queryType, [Query::TYPE_ADD, Query::TYPE_MODIFICATION])) {
            return self::createFullStructure($entityName, $queryType, $parameters);
        }

        if (in_array($queryType, [Query::TYPE_QUERY, Query::TYPE_VOID])) {
            return self::createShortStructure($entityName, $queryType, $parameters);
        }

        return false;
    }

    /**
     * @param $entityName
     * @param $queryType
     * @param array $parameters
     *
     * @return array
     */
    private static function createFullStructure($entityName, $queryType, array $parameters)
    {
        $queryName = $entityName . $queryType;
        $queryNameRq = $queryName . 'Rq';

        $queryStructure = [
            'QBXMLMsgsRq' => [
                '@onError' => 'stopOnError',
                $queryNameRq => [
                    $queryName => $parameters
                ]
            ]
        ];

        return $queryStructure;
    }

    /**
     * @param $entityName
     * @param $queryType
     * @param array $parameters
     *
     * @return array
     */
    private static function createShortStructure($entityName, $queryType, array $parameters)
    {
        $queryNameRq = $entityName . $queryType . 'Rq';

        $queryStructure = [
            'QBXMLMsgsRq' => [
                '@onError' => 'stopOnError',
                $queryNameRq => $parameters
            ]
        ];

        return $queryStructure;
    }

    /**
     * @param Query $query
     */
    private static function setDefaultHeaders(Query $query)
    {
        $query->setHeaders(
            [
                'Content-Length' => mb_strlen($query->body(), 'utf8'),
                'Accept-Encoding' => 'gzip, deflate',
                'Connection' => 'close',
                'X-AcctSyncInteractionType' => 0,
            ]
        );
    }
}
