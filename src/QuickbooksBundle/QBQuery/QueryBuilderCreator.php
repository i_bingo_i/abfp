<?php

namespace QuickbooksBundle\QBQuery;

class QueryBuilderCreator
{
    private $qbConnectConfig;

    /**
     * QueryBuilderCreator constructor.
     *
     * @param $qbConnectConfig
     */
    public function __construct($qbConnectConfig)
    {
        $this->qbConnectConfig = $qbConnectConfig;
    }

    /**
     * @return QueryBuilder
     */
    public function make()
    {
        /** @var ConnectionOptions $connectionOptions */
        $connectionOptions = ConnectionOptionsFactory::make($this->qbConnectConfig);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = new QueryBuilder($connectionOptions);

        return $queryBuilder;
    }
}
