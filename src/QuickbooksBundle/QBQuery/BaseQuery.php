<?php

namespace QuickbooksBundle\QBQuery;

trait BaseQuery
{
    /**
     * @param $query
     * @return array
     */
    protected function baseQueryNode($query)
    {
        return [
            'QBXMLMsgsRq' => $query
        ];
    }
}