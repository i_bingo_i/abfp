<?php

namespace QuickbooksBundle\QBQuery;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client as GuzzleClient;
use QuickbooksBundle\Interfaces\ResponseParser;
use QuickbooksBundle\Response\Response;

class Query
{
    const TYPE_ADD = 'Add';
    const TYPE_MODIFICATION = 'Mod';
    const TYPE_QUERY = 'Query';
    const TYPE_VOID = 'Void';
    const TYPE_RAW_XML = 'Raw Xml';

    private $body = '';
    private $headers = [];
    /** @var ConnectionOptions */
    private $connectionOptions;
    /** @var ResponseParser */
    private $parser;

    /**
     * @param $body
     *
     * @return Query
     */
    public function setBody($body): Query
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function body(): string
    {
        return $this->body;
    }

    /**
     * @param $headers
     *
     * @return Query
     */
    public function setHeaders($headers): Query
    {
        foreach ($headers as $key => $header) {
            $this->headers[$key] = $header;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function headers(): array
    {
        return $this->headers;
    }

    /**
     * @param ConnectionOptions $connectionOptions
     *
     * @return Query
     */
    public function setConnectionOptions(ConnectionOptions $connectionOptions): Query
    {
        $this->connectionOptions = $connectionOptions;

        return $this;
    }

    /**
     * @return ConnectionOptions
     */
    public function connectionOptions(): ConnectionOptions
    {
        return $this->connectionOptions;
    }

    /**
     * @param $parser
     *
     * @return Query
     */
    public function setParser($parser): Query
    {
        $this->parser = $parser;

        return $this;
    }

    /**
     * @return Response|\SimpleXMLElement
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getResult()
    {
        $guzzleClient = new GuzzleClient();

        /** @var ResponseInterface $guzzleResponse */
        $guzzleResponse = $guzzleClient->request(
            'POST',
            $this->connectionOptions->host(),
            [
                'auth' => [
                    $this->connectionOptions->login(),
                    $this->connectionOptions->password()
                ],
                'headers' => $this->headers(),
                'body' => $this->body(),
                'connect_timeout' =>  $this->connectionOptions->timeConnectTimeout(),
                'timeout' =>  $this->connectionOptions->timeout()
            ]
        );

        $xmlResponseBody = utf8_encode($guzzleResponse->getBody());

        if (!$this->parser) {
            $response = simplexml_load_string($xmlResponseBody);
        } else {
            /** @var Response $response */
            $response = $this->parser->parse($xmlResponseBody);
        }

        return $response;
    }
}
