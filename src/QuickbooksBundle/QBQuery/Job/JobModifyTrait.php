<?php

namespace QuickbooksBundle\QBQuery\Job;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\Job;
use QuickbooksBundle\QBQuery\AddressTrait;
use QuickbooksBundle\QBQuery\BaseQuery;

trait JobModifyTrait
{
    use BaseQuery;
    use AddressTrait;

    /**
     * @param $fields
     * @return array
     */
    private function addCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'CustomerModRq' => [
                    'CustomerMod' => $fields
                ]
            ]
        );
    }

    /**
     * @param Invoices $invoices
     * @return array
     */
    protected function modify(Invoices $invoices)
    {
        return $this->addCarcassOfRequest([
            'ListID' => $invoices->getJob()->getAccountingSystemId(),
            'EditSequence' => $invoices->getJob()->getEditSequence(),
            'Name' => $invoices->getJob()->getName(),
            'ParentRef' => [
                'ListID' => $invoices->getJob()->getCustomer()->getAccountingSystemId()
            ],
            'BillAddress' => $this->getAddress(
                $this->getBillAddress($invoices->getAccountContactPerson())
            ),
            'ShipAddress' => $this->getAddress($invoices->getShipToAddress())
        ]);
    }
}