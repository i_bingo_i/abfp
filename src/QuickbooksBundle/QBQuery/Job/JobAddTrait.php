<?php

namespace QuickbooksBundle\QBQuery\Job;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\Job;
use QuickbooksBundle\QBQuery\AddressTrait;
use QuickbooksBundle\QBQuery\BaseQuery;

trait JobAddTrait
{
    use BaseQuery;
    use AddressTrait;

    /**
     * @param $fields
     * @return array
     */
    private function addCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'CustomerAddRq' => [
                    'CustomerAdd' => $fields
                ]
            ]
        );
    }

    /**
     * @param Invoices $invoices
     * @return array
     */
    protected function add(Invoices $invoices)
    {
        return $this->addCarcassOfRequest([
            'Name' => $invoices->getJob()->getName(),
            'IsActive' => 'true',
            'ParentRef' => [
                'ListID' => $invoices->getJob()->getCustomer()->getAccountingSystemId()
            ],
            'BillAddress' => $this->getAddress(
                $this->getBillAddress($invoices->getAccountContactPerson())
            ),
            'ShipAddress' => $this->getAddress($invoices->getShipToAddress())
        ]);
    }
}