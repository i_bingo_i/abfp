<?php

namespace QuickbooksBundle\QBQuery;

use QuickbooksBundle\Parser\AddInvoice as AddInvoiceParser;

class QueryBuilder
{
    /** @var ConnectionOptions */
    private $connectionOptions;
    /** @var QueryDTO */
    private $queryDTO;

    /**
     * QueryBuilder constructor.
     *
     * @param ConnectionOptions $connectionOptions
     */
    public function __construct(ConnectionOptions $connectionOptions)
    {
        $this->connectionOptions = $connectionOptions;
    }

    /**
     * @param $entityName
     *
     * @return QueryBuilder
     */
    public function add($entityName): QueryBuilder
    {
        $this->queryDTO = new QueryDTO();
        $this->queryDTO->setQueryType(Query::TYPE_ADD);
        $this->queryDTO->setEntityName($entityName);

        if ($entityName == 'Invoice') {
            $addInvoiceParser = new AddInvoiceParser();
            $this->queryDTO->setParser($addInvoiceParser);
        }

        return $this;
    }

    /**
     * @param $entityName
     *
     * @return QueryBuilder
     */
    public function mod($entityName): QueryBuilder
    {
        $this->queryDTO = new QueryDTO();
        $this->queryDTO->setQueryType(Query::TYPE_MODIFICATION);
        $this->queryDTO->setEntityName($entityName);

        return $this;
    }

    /**
     * @param $entityName
     *
     * @return QueryBuilder
     */
    public function query($entityName): QueryBuilder
    {
        $this->queryDTO = new QueryDTO();
        $this->queryDTO->setQueryType(Query::TYPE_QUERY);
        $this->queryDTO->setEntityName($entityName);

        return $this;
    }

    /**
     * @param $entityName
     * @param $entityId
     *
     * @return QueryBuilder
     */
    public function void($entityName, $entityId): QueryBuilder
    {
        $this->queryDTO = new QueryDTO();
        $this->queryDTO->setQueryType(Query::TYPE_VOID);
        $this->queryDTO->setEntityName('Txn');
        $this->queryDTO->setParameters([
            'TxnVoidType' => $entityName,
            'TxnID' => $entityId
        ]);

        return $this;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return QueryBuilder
     */
    public function modifiedDate($dateFrom, $dateTo) : QueryBuilder
    {
        $this->queryDTO = new QueryDTO();
        $this->queryDTO->setQueryType(Query::TYPE_QUERY);
        $this->queryDTO->setEntityName('Item');
        $this->queryDTO->setParameters([
            'ActiveStatus' => 'All',
            'FromModifiedDate' => $dateFrom,
            'ToModifiedDate' => $dateTo
        ]);

        return $this;
    }

    /**
     * @param $rawXmlRequest
     *
     * @return QueryBuilder
     */
    public function queryByRawXml($rawXmlRequest): QueryBuilder
    {
        $this->queryDTO = new QueryDTO();
        $this->queryDTO->setQueryType(Query::TYPE_RAW_XML);
        $this->queryDTO->setRawXmlRequest(trim($rawXmlRequest));

        return $this;
    }

    /**
     * @param array $parameters
     *
     * @return QueryBuilder
     */
    public function setParameters(array $parameters): QueryBuilder
    {
        $this->queryDTO->setParameters($parameters);

        return $this;
    }

    /**
     * @param $parameter
     * @param $key
     *
     * @return QueryBuilder
     */
    public function addParameter($parameter, $key): QueryBuilder
    {
        $this->queryDTO->addParameter($parameter, $key);

        return $this;
    }

    /**
     * @return Query
     */
    public function getQuery()
    {
        /** @var Query $query */
        $query = QueryFactory::make($this->queryDTO);

        if ($query) {
            $query->setConnectionOptions($this->connectionOptions);
        }

        return $query;
    }
}
