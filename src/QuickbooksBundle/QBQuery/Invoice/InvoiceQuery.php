<?php

namespace QuickbooksBundle\QBQuery\Invoice;

use QuickbooksBundle\QBQuery\BaseQuery;

trait InvoiceQuery
{
    use BaseQuery;

    /**
     * @param array $fields
     * @return array
     */
    private function queryCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'InvoiceQueryRq' => $fields
            ]
        );
    }

    /**
     * @return array
     */
    protected function getAll()
    {
        return $this->getById();
    }

    /**
     * @param string $id
     * @return array
     */
    protected function getById($id = '')
    {
        /** @var array $fields */
        $fields = [
            'TxnID' => $id,
            'IncludeLineItems' => 'true'
        ];

        return $this->queryCarcassOfRequest($fields);
    }
}