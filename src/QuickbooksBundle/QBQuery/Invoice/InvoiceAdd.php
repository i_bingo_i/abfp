<?php

namespace QuickbooksBundle\QBQuery\Invoice;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\InvoiceLine;
use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\QBQuery\AddressTrait;
use QuickbooksBundle\QBQuery\BaseQuery;

trait InvoiceAdd
{
    use BaseQuery;
    use AddressTrait;

    /**
     * @param array $fields
     * @return array
     */
    private function queryCarcassOfRequest(array $fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'InvoiceAddRq' => $fields
            ]
        );
    }

    /**
     * @param Invoices $invoices
     * @return array
     * @throws \Exception
     */
    public function add(Invoices $invoices)
    {
        return $this->queryCarcassOfRequest(
            [
                'InvoiceAdd' => [
                    'CustomerRef' => [
                        'ListID' => $invoices->getJob()->getAccountingSystemId()
                    ],
                    'TemplateRef' => [
                        'FullName' => 'Copy 3 : Customized Invoice'
                    ],
                    'TxnDate' => $invoices->getInvoiceDate()->format('Y-m-d'),
                    'BillAddress' => $this->getAddress($invoices->getBillToAddress()),
                    'ShipAddress' => $this->getAddress($invoices->getShipToAddress()),
                    'PONumber' => $invoices->getWorkorder()->getPoNo() ?? '',
                    'TermsRef' => [
                        'ListID' => $invoices->getPaymentTerm()->getAccountingSystemId() ?? ''
                    ],
                    'FOB' => $invoices->getInspector(),
                    'Memo' => $invoices->getMemo() ?? '',
                    'CustomerMsgRef' => [
                        'FullName' => 'Thank you for your business.'
                    ],
                    'Other' => $invoices->getUser()->getFirstName() . ' ' . $invoices->getUser()->getLastName(),
                    'InvoiceLineAdd' => $this->invoiceLines($invoices)
                ]
            ]
        );
    }

    /**
     * @param Invoices $invoices
     * @return array
     */
    public function invoiceLines(Invoices $invoices)
    {
        $lines = [];
        /** @var InvoiceLine $line */
        foreach ($invoices->getLines()->toArray() as $key => $line) {
            $lines[$key]['ItemRef']['ListID'] = $line->getItem()->getQbId();
            $lines[$key]['Desc'] = $line->getDescription();
            if ($line->getItem()->getType()->getAlias() != ItemType::TYPE_DISCOUNT) {
                $lines[$key]['Quantity'] = $line->getQuantity();
            }
            $lines[$key]['Rate'] = $line->getRate();
        }

        return $lines;
    }
}
