<?php

namespace QuickbooksBundle\QBQuery\Invoice;

use InvoiceBundle\Entity\Invoices;
use QuickbooksBundle\QBQuery\BaseQuery;

trait InvoiceVoid
{
    use BaseQuery;

    /**
     * @param Invoices $invoices
     * @return array
     */
    public function void(Invoices $invoice)
    {
        return $this->queryCarcassOfRequest(
            [
                'TxnVoidType' => 'Invoice',
                'TxnID' => $invoice->getAccountingSystemId()
            ]
        );
    }

    /**
     * @param array $fields
     * @return array
     */
    private function queryCarcassOfRequest(array $fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'TxnVoidRq' => $fields
            ]
        );
    }
}
