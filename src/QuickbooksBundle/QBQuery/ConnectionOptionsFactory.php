<?php

namespace QuickbooksBundle\QBQuery;

class ConnectionOptionsFactory
{
    private const DEFAULT_TIME_CONNECT_TIMEOUT = 10;
    private const DEFAULT_TIME_TIMEOUT = 20;

    /**
     * @param $qbConnectConfig
     *
     * @return ConnectionOptions
     */
    public static function make($qbConnectConfig)
    {
        $connectionOptions = new ConnectionOptions();

        if (isset($qbConnectConfig['url'])) {
            $connectionOptions->setHost($qbConnectConfig['url']);
        }

        if (isset($qbConnectConfig['login'])) {
            $connectionOptions->setLogin($qbConnectConfig['login']);
        }

        if (isset($qbConnectConfig['password'])) {
            $connectionOptions->setPassword($qbConnectConfig['password']);
        }

        if (isset($qbConnectConfig['time_connect_timeout'])) {
            $connectionOptions->setTimeConnectTimeout($qbConnectConfig['time_connect_timeout']);
        } else {
            $connectionOptions->setTimeConnectTimeout(self::DEFAULT_TIME_CONNECT_TIMEOUT);
        }

        if (isset($qbConnectConfig['timeout'])) {
            $connectionOptions->setTimeout($qbConnectConfig['timeout']);
        } else {
            $connectionOptions->setTimeout(self::DEFAULT_TIME_TIMEOUT);
        }

        return $connectionOptions;
    }
}
