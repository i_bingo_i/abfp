<?php

namespace QuickbooksBundle\QBQuery;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;

trait AddressTrait
{
    /**
     * First iteration start from 0
     * @var int
     */
    private $MAX_ITERATION_QUANTITY = 2;

    /**
     * @param AccountContactPerson $accountContactPerson
     * @return Address
     */
    protected function getBillAddress(AccountContactPerson $accountContactPerson)
    {
        $address = $accountContactPerson->getCustomer()->getBillingAddress();

        if ($accountContactPerson->getCustomBillingAddress()
            && $accountContactPerson->getCustomBillingAddress()->getAddress()
        ) {
            $address = $accountContactPerson->getCustomBillingAddress();
        }

        return $address;
    }

    /**
     * @param Address $address
     * @return array
     */
    protected function getAddress(Address $address)
    {
        $res = [];

        if ($address->getAddress()) {
            $dividedAddresses = preg_split('/(\r\n|\r|\n)/', $address->getAddress());

            $addressesArray = [];
            $serialNumber = 0;

            while ($serialNumber <= $this->MAX_ITERATION_QUANTITY) {
                $addressLine = $dividedAddresses[$serialNumber] ?? '';
                $addressesArray["Addr" . ++$serialNumber] = $addressLine;
            }

            $billAddress = [
                'City' => $address->getCity(),
                'State' => $address->getState()->getCode(),
                'PostalCode' => $address->getZip(),
            ];

            $res = array_merge($addressesArray, $billAddress);
        }

        return $res;
    }
}

