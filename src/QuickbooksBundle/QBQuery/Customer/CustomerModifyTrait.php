<?php

namespace QuickbooksBundle\QBQuery\Customer;

use AppBundle\Entity\Address;
use InvoiceBundle\Entity\Customer;
use QuickbooksBundle\QBQuery\AddressTrait;
use QuickbooksBundle\QBQuery\BaseQuery;

trait CustomerModifyTrait
{
    use BaseQuery;
    use AddressTrait;

    /**
     * @param $fields
     * @return array
     */
    private function addCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'CustomerModRq' => [
                    'CustomerMod' => $fields
                ]
            ]
        );
    }

    /**
     * @param Customer $customer
     * @return array
     */
    protected function modify(Customer $customer)
    {
        $name = '[' . $customer->getEntityId() . ']' . ' ' . $customer->getName();

        return $this->addCarcassOfRequest([
            'ListID' => $customer->getAccountingSystemId(),
            'EditSequence' => $customer->getEditSequence(),
            'Name' => $name,
//            'FirstName' => $customer->getName(),
            'BillAddress' => $this->getBillAddress($customer),
        ]);
    }

    /**
     * @param Customer $customer
     * @return array
     */
    private function getBillAddress(Customer $customer)
    {
        /** @var Address $address */
        $address = $customer->getBillingAddress();

        return $this->getAddress($address);
    }
}
