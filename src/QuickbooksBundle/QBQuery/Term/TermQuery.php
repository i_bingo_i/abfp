<?php

namespace QuickbooksBundle\QBQuery\Term;

use QuickbooksBundle\QBQuery\BaseQuery;

trait TermQuery
{
    use BaseQuery;

    /**
     * @param array $fields
     * @return array
     */
    private function queryCarcassOfRequest($fields)
    {
        return $this->baseQueryNode(
            [
                '@onError' => 'stopOnError',
                'TermsQueryRq' => $fields
            ]
        );
    }

    /**
     * @return array
     */
    protected function getAll()
    {
        return $this->getById();
    }

    /**
     * @param string $id
     * @return array
     */
    protected function getById($id = '')
    {
        /** @var array $fields */
        $fields = [
            'ListID' => $id
        ];

        return $this->queryCarcassOfRequest($fields);
    }

    /**
     * @param string $name
     * @return array
     */
    protected function getByName($name = '')
    {
        /** @var array $fields */
        $fields = [
            'FullName' => $name
        ];

        return $this->queryCarcassOfRequest($fields);
    }

    /**
     * @param $from
     * @param $to
     * @return array
     */
    protected function getByModifiedDate($from, $to)
    {
        $fields = [
            'FromModifiedDate' => $from,
            'ToModifiedDate' => $to
        ];

        return $this->queryCarcassOfRequest($fields);
    }
}