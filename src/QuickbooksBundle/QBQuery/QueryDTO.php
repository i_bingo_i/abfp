<?php

namespace QuickbooksBundle\QBQuery;

use QuickbooksBundle\Interfaces\ResponseParser;

class QueryDTO
{
    private $entityName;
    private $queryType;
    /** @var null | ResponseParser */
    private $parser = null;
    /** @var null | string */
    private $rawXmlRequest = null;
    private $parameters = [];

    /**
     * @param $entityName
     *
     * @return $this
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * @return string
     */
    public function entityName() : string
    {
        return $this->entityName;
    }

    /**
     * @param $queryType
     *
     * @return $this
     */
    public function setQueryType($queryType)
    {
        $this->queryType = $queryType;

        return $this;
    }

    /**
     * @return string
     */
    public function queryType() : string
    {
        return $this->queryType;
    }

    /**
     * @param $parser
     *
     * @return $this
     */
    public function setParser($parser)
    {
        $this->parser = $parser;

        return $this;
    }

    /**
     * @return ResponseParser
     */
    public function parser()
    {
        return $this->parser;
    }

    /**
     * @param $rawXmlRequest
     *
     * @return $this
     */
    public function setRawXmlRequest($rawXmlRequest)
    {
        $this->rawXmlRequest = $rawXmlRequest;

        return $this;
    }

    /**
     * @return string|null
     */
    public function rawXmlRequest()
    {
        return $this->rawXmlRequest;
    }

    /**
     * @param array $parameters
     *
     * @return $this
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @param $parameter
     * @param $key
     *
     * @return $this
     */
    public function addParameter($parameter, $key)
    {
        $this->parameters[$key] = $parameter;

        return $this;
    }

    /**
     * @return array
     */
    public function parameters() : array
    {
        return $this->parameters;
    }
}
