<?php

namespace QuickbooksBundle\Repository\QBRepository;

use QuickbooksBundle\Factory\Logger\CustomerFactory;
use QuickbooksBundle\Factory\Logger\CustomerRefreshSequenceFactory;
use QuickbooksBundle\Handlers\ContactPersonCustomerModHandler;
use QuickbooksBundle\Handlers\CustomerRefreshSequenceHandler;
use QuickbooksBundle\Parser\QueryCustomer;
use QuickbooksBundle\QBQuery\Query;
use QuickbooksBundle\QBQuery\QueryBuilder;
use QuickbooksBundle\QBQuery\QueryBuilderCreator;
use QuickbooksBundle\Response\Response;
use InvoiceBundle\Entity\Customer as CustomerEntity;

class Customer
{
    /** @var QueryBuilderCreator */
    private $queryBuilderCreator;
    /** @var ContactPersonCustomerModHandler */
    private $contactPersonCustomerModHandler;
    /** @var CustomerRefreshSequenceHandler */
    private $customerRefreshSequenceHandler;

    /**
     * Customer constructor.
     * @param QueryBuilderCreator $queryBuilderCreator
     * @param ContactPersonCustomerModHandler $contactPersonCustomerModHandler
     * @param CustomerRefreshSequenceHandler $customerRefreshSequenceHandler
     */
    public function __construct(
        QueryBuilderCreator $queryBuilderCreator,
        ContactPersonCustomerModHandler $contactPersonCustomerModHandler,
        CustomerRefreshSequenceHandler $customerRefreshSequenceHandler
    ) {
        $this->queryBuilderCreator = $queryBuilderCreator;
        $this->contactPersonCustomerModHandler = $contactPersonCustomerModHandler;
        $this->customerRefreshSequenceHandler = $customerRefreshSequenceHandler;
    }

    /**
     * @param CustomerEntity $customer
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function find(CustomerEntity $customer)
    {
        $qbCustomerId = $customer->getAccountingSystemId();

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder->query('Customer')
            ->addParameter($qbCustomerId, 'ListID');

        /** @var Query $query */
        $query = $queryBuilder->getQuery();
        $query->setParser(new QueryCustomer());

        $requestLoggerDto = CustomerRefreshSequenceFactory::make($customer, $query);

        return $this->customerRefreshSequenceHandler->executeQuery($query, $requestLoggerDto);
    }
}
