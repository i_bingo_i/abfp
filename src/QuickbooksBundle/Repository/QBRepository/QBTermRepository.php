<?php

namespace QuickbooksBundle\Repository\QBRepository;


use AppBundle\Entity\PaymentTerm;
use QuickbooksBundle\QBQuery\Term\TermQuery;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Term\ParserTerm;
use QuickbooksBundle\Services\Quickbooks\Validator;
use SimpleXMLElement;

class QBTermRepository
{
    use TermQuery;

    /** @var ParserTerm  */
    private $parserTerm;
    /** @var QueryPreparer */
    private $queryPreparer;

    /**
     * QBTermRepository constructor.
     * @param QueryPreparer $queryPreparer
     * @param ParserTerm $parserTerm
     */
    public function __construct(QueryPreparer $queryPreparer, ParserTerm $parserTerm)
    {
        $this->queryPreparer = $queryPreparer;
        $this->parserTerm = $parserTerm;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllRecords()
    {
        $query = $this->getAll();
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->TermsQueryRs->attributes();

        Validator::checkResponse($attributes['@attributes']);

        return $this->parsingResponse($responseXMLObject);
    }

    /**
     * @param string $from
     * @param string $to
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRecordsByModifiedDate($from, $to)
    {
        $query = $this->getByModifiedDate($from, $to);
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->TermsQueryRs->attributes();

        Validator::checkResponse($attributes['@attributes']);

        return $this->parsingResponse($responseXMLObject);
    }

    /**
     * @param $responseXMLObject
     * @return PaymentTerm|array|null|string
     */
    private function parsingResponse($responseXMLObject)
    {
        $result = null;

        if (isset($responseXMLObject->QBXMLMsgsRs->TermsQueryRs->StandardTermsRet)) {
            $result = $this->parserTerm->parsingQuery($responseXMLObject);
        }

        return $result;
    }
}