<?php

namespace QuickbooksBundle\Repository\QBRepository;

use QuickbooksBundle\QBQuery\Invoice\InvoiceQuery;
use QuickbooksBundle\Services\Quickbooks\Invoice\ParserInvoice;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;

class QBInvoiceRepository
{
    use InvoiceQuery;

    /** @var QueryPreparer */
    private $queryPreparer;
    /** @var ParserInvoice */
    private $parserInvoice;

    public function __construct(QueryPreparer $queryPreparer, ParserInvoice $parserInvoice)
    {
        $this->queryPreparer = $queryPreparer;
        $this->parserInvoice = $parserInvoice;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRecordById($id)
    {
        $query = $this->getById($id);
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->InvoiceQueryRs->attributes();

        Validator::checkResponse($attributes['@attributes']);

        return $this->parserInvoice->parsingQuery($responseXMLObject);
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllRecords()
    {
        $query = $this->getAll();
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->InvoiceQueryRs->attributes();

        Validator::checkResponse($attributes['@attributes']);

        return $this->parserInvoice->parsingQuery($responseXMLObject);
    }
}