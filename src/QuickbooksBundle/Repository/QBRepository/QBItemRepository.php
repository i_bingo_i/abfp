<?php

namespace QuickbooksBundle\Repository\QBRepository;

use QuickbooksBundle\Entity\ItemType;
use QuickbooksBundle\QBQuery\Item\ItemQuery;
use QuickbooksBundle\Services\Quickbooks\Item\Discount\ParserItemDiscount;
use QuickbooksBundle\Services\Quickbooks\Item\Group\ParserItemGroup;
use QuickbooksBundle\Services\Quickbooks\Item\OtherCharge\ParserItemOtherCharge;
use QuickbooksBundle\Services\Quickbooks\Item\Payment\ParserItemPayment;
use QuickbooksBundle\Services\Quickbooks\Item\Subtotal\ParserItemSubtotal;
use QuickbooksBundle\Services\Quickbooks\Item\Service\ParserItemService;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;
use SimpleXMLElement;

class QBItemRepository
{
    use ItemQuery;

    /** @var QueryPreparer */
    private $queryPreparer;
    /** @var ParserItemService */
    private $parserItemService;
    /** @var ParserItemOtherCharge */
    private $parserItemOtherCharge;
    /** @var ParserItemSubtotal */
    private $parserItemSubtotal;
    /** @var ParserItemDiscount */
    private $parserItemDiscount;
    /** @var ParserItemPayment */
    private $parserItemPayment;
    /** @var ParserItemGroup */
    private $parserItemGroup;

    /**
     * QBItemRepository constructor.
     * @param QueryPreparer $queryPreparer
     * @param ParserItemService $parserItemService
     * @param ParserItemOtherCharge $parserItemOtherCharge
     * @param ParserItemSubtotal $parserItemSubtotal
     * @param ParserItemDiscount $parserItemDiscount
     * @param ParserItemPayment $parserItemPayment
     * @param ParserItemGroup $parserItemGroup
     */
    public function __construct(
        QueryPreparer $queryPreparer,
        ParserItemService $parserItemService,
        ParserItemOtherCharge $parserItemOtherCharge,
        ParserItemSubtotal $parserItemSubtotal,
        ParserItemDiscount $parserItemDiscount,
        ParserItemPayment $parserItemPayment,
        ParserItemGroup $parserItemGroup
    ) {
        $this->queryPreparer = $queryPreparer;
        $this->parserItemService = $parserItemService;
        $this->parserItemOtherCharge = $parserItemOtherCharge;
        $this->parserItemSubtotal = $parserItemSubtotal;
        $this->parserItemDiscount = $parserItemDiscount;
        $this->parserItemPayment = $parserItemPayment;
        $this->parserItemGroup = $parserItemGroup;
    }

    /**
     * @param string $id
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRecordById($id)
    {
        $query = $this->getById($id);
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->ItemQueryRs->attributes();

        Validator::checkResponse($attributes['@attributes']);

        return $this->parsingResponse($responseXMLObject);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllRecords()
    {
        $query = $this->getAll();
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->ItemQueryRs->attributes();

        Validator::checkResponse($attributes['@attributes']);

        return $this->parsingResponse($responseXMLObject);
    }

    /**
     * @param string $from
     * @param string $to
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRecordsByModifiedDate($from, $to)
    {
        $query = $this->getByModifiedDate($from, $to);
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->ItemQueryRs->attributes();

        Validator::checkResponse($attributes['@attributes']);

        return $this->parsingResponse($responseXMLObject);
    }

    /**
     * @param SimpleXMLElement $responseXMLObject
     * @return array
     */
    private function parsingResponse(SimpleXMLElement $responseXMLObject)
    {
        $result = [];

        if (isset($responseXMLObject->QBXMLMsgsRs->ItemQueryRs->ItemServiceRet)) {
            $result = array_merge($result, $this->parserItemService->parsingQuery($responseXMLObject));
        }

        if (isset($responseXMLObject->QBXMLMsgsRs->ItemQueryRs->ItemOtherChargeRet)) {
            $result = array_merge($result, $this->parserItemOtherCharge->parsingQuery($responseXMLObject));
        }

//        if (isset($responseXMLObject->QBXMLMsgsRs->ItemQueryRs->ItemSubtotalRet)) {
//            $result = array_merge($result, $this->parserItemSubtotal->parsingQuery($responseXMLObject));
//        }

        if (isset($responseXMLObject->QBXMLMsgsRs->ItemQueryRs->ItemDiscountRet)) {
            $result = array_merge($result, $this->parserItemDiscount->parsingQuery($responseXMLObject));
        }

//        if (isset($responseXMLObject->QBXMLMsgsRs->ItemQueryRs->ItemPaymentRet)) {
//            $result = array_merge($result, $this->parserItemPayment->parsingQuery($responseXMLObject));
//        }
//
//        if (isset($responseXMLObject->QBXMLMsgsRs->ItemQueryRs->ItemGroupRet)) {
//            $result = array_merge($result, $this->parserItemGroup->parsingQuery($responseXMLObject));
//        }

        return $result;
    }
}