<?php

namespace QuickbooksBundle\Repository\QBRepository;

use QuickbooksBundle\Entity\Dummy\ListDeleted;
use QuickbooksBundle\QBQuery\ListDeleted\ListDeletedQuery;
use QuickbooksBundle\Services\Quickbooks\ListDeleted\ParserListDeleted;
use QuickbooksBundle\Services\Quickbooks\QueryPreparer;
use QuickbooksBundle\Services\Quickbooks\Validator;

class QBListDeletedRepository
{
    use ListDeletedQuery;

    /** @var QueryPreparer */
    private $queryPreparer;
    /** @var ParserListDeleted */
    private $parserListDeleted;

    public function __construct(QueryPreparer $queryPreparer, ParserListDeleted $parserListDeleted)
    {
        $this->queryPreparer = $queryPreparer;
        $this->parserListDeleted = $parserListDeleted;
    }

    /**
     * @param $from
     * @param $to
     * @return array|null|ListDeleted|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDeletedItems($from, $to)
    {
        $query = $this->getItems($from, $to);
        $responseXMLObject = $this->queryPreparer->getResponse($query);
        $attributes = (array) $responseXMLObject->QBXMLMsgsRs->ListDeletedQueryRs->attributes();

        Validator::checkResponse($attributes['@attributes']);

        return $this->parsingResponse($responseXMLObject);
    }

    /**
     * @param $responseXMLObject
     * @return array|null
     */
    private function parsingResponse($responseXMLObject)
    {
        $result = null;

        if (isset($responseXMLObject->QBXMLMsgsRs->ListDeletedQueryRs->ListDeletedRet)) {
            $result = $this->parserListDeleted->parsingQuery($responseXMLObject);
        }

        return $result;
    }

}