<?php

namespace QuickbooksBundle\Repository\QBRepository;

use QuickbooksBundle\Factory\Logger\JobRefreshSequenceFactory;
use QuickbooksBundle\Handlers\JobRefreshSequenceHandler;
use QuickbooksBundle\Parser\QueryCustomer;
use QuickbooksBundle\QBQuery\Query;
use QuickbooksBundle\QBQuery\QueryBuilder;
use QuickbooksBundle\QBQuery\QueryBuilderCreator;
use QuickbooksBundle\Response\Response;
use InvoiceBundle\Entity\Job as JobEntity;

class Job
{
    /** @var QueryBuilderCreator */
    private $queryBuilderCreator;
    /** @var JobRefreshSequenceHandler */
    private $jobRefreshSequenceHandler;

    /**
     * Job constructor.
     * @param QueryBuilderCreator $queryBuilderCreator
     * @param JobRefreshSequenceHandler $jobRefreshSequenceHandler
     */
    public function __construct(
        QueryBuilderCreator $queryBuilderCreator,
        JobRefreshSequenceHandler $jobRefreshSequenceHandler
    ) {
        $this->queryBuilderCreator = $queryBuilderCreator;
        $this->jobRefreshSequenceHandler = $jobRefreshSequenceHandler;
    }

    /**
     * @param JobEntity $job
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function find(JobEntity $job)
    {
        $qbJobId = $job->getAccountingSystemId();

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->queryBuilderCreator->make();
        $queryBuilder->query('Customer')
            ->addParameter($qbJobId, 'ListID');

        /** @var Query $query */
        $query = $queryBuilder->getQuery();
        $query->setParser(new QueryCustomer());

        $requestLoggerDto = JobRefreshSequenceFactory::make($job, $query);

        return $this->jobRefreshSequenceHandler->executeQuery($query, $requestLoggerDto);
    }
}
