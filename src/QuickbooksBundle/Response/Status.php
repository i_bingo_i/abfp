<?php

namespace QuickbooksBundle\Response;

class Status
{
    /** @var int */
    private $code;
    /** @var string */
    private $message;
    /** @var string */
    private $severity;

    /**
     * @return int
     */
    public function code()
    {
        return (int)$this->code;
    }

    /**
     * @param $code
     *
     * @return Status
     */
    public function setCode($code): Status
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function message()
    {
        return $this->message;
    }

    /**
     * @param $message
     *
     * @return Status
     */
    public function setMessage($message): Status
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function severity()
    {
        return $this->severity;
    }

    /**
     * @param $severity
     *
     * @return Status
     */
    public function setSeverity($severity): Status
    {
        $this->severity = $severity;

        return $this;
    }
}
