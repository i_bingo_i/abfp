<?php

namespace QuickbooksBundle\Response;

class Response
{
    /** @var string */
    private $rawResponse;
    /** @var Status */
    private $status;
    /** @var array */
    private $errors = [];
    private $xmlObject;
    /** @var array */
    private $content = [];

    /**
     * @return string
     */
    public function rawResponse(): string
    {
        return $this->rawResponse;
    }

    /**
     * @param $rawResponse
     *
     * @return Response
     */
    public function setRawResponse($rawResponse): Response
    {
        $this->rawResponse = $rawResponse;

        return $this;
    }

    /**
     * @return Status
     */
    public function status(): Status
    {
        return $this->status;
    }

    /**
     * @param Status $status
     *
     * @return Response
     */
    public function setStatus(Status $status): Response
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function xmlObject()
    {
        return $this->xmlObject;
    }

    /**
     * @param $xmlObject
     *
     * @return Response
     */
    public function setXmlObject($xmlObject): Response
    {
        $this->xmlObject = $xmlObject;

        return $this;
    }

    /**
     * @param $error
     *
     * @return Response
     */
    public function addError($error): Response
    {
        $this->errors[] = $error;

        return $this;
    }

    /**
     * @return array
     */
    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * @param $fieldKey
     * @param $fieldContent
     *
     * @return Response
     */
    public function addField($fieldKey, $fieldContent): Response
    {
        $this->content[$fieldKey] = $fieldContent;

        return $this;
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    public function get($key)
    {
        return $this->content[$key];
    }

    /**
     * @return array
     */
    public function content(): array
    {
        return $this->content;
    }
}
