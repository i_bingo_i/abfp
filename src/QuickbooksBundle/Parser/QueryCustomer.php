<?php

namespace QuickbooksBundle\Parser;

use QuickbooksBundle\Interfaces\ResponseParser;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Factory\ResponseFactory;

class QueryCustomer implements ResponseParser
{
    /**
     * @param string $strXmlResponse
     *
     * @return Response
     */
    public function parse(string $strXmlResponse): Response
    {
        /** @var Response $response */
        $response = ResponseFactory::make($strXmlResponse, 'CustomerQueryRs');

        $xmlResponseObject = $response->xmlObject();

        $customerRsNode = (array) $xmlResponseObject->QBXMLMsgsRs->CustomerQueryRs;
        $customerRetNode = (array) $customerRsNode['CustomerRet'];

        $response->addField('ListID', $customerRetNode['ListID']);
        $response->addField('EditSequence', $customerRetNode['EditSequence']);

        return $response;
    }
}
