<?php

namespace QuickbooksBundle\Parser;

use QuickbooksBundle\Interfaces\ResponseParser;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Factory\ResponseFactory;
use QuickbooksBundle\Services\Quickbooks\Item\Service\ParserItemService;
use QuickbooksBundle\Services\Quickbooks\Item\OtherCharge\ParserItemOtherCharge;
use QuickbooksBundle\Services\Quickbooks\Item\Discount\ParserItemDiscount;

class ModifiedDate implements ResponseParser
{
    /** @var ParserItemService */
    private $parserItemService;
    /** @var ParserItemOtherCharge */
    private $parserItemOtherCharge;
    /** @var ParserItemDiscount */
    private $parserItemDiscount;

    /**
     * ModifiedDate constructor.
     * @param ParserItemService $parserItemService
     * @param ParserItemOtherCharge $parserItemOtherCharge
     * @param ParserItemDiscount $parserItemDiscount
     */
    public function __construct(
        ParserItemService $parserItemService,
        ParserItemOtherCharge $parserItemOtherCharge,
        ParserItemDiscount $parserItemDiscount
    ) {
        $this->parserItemService = $parserItemService;
        $this->parserItemOtherCharge = $parserItemOtherCharge;
        $this->parserItemDiscount = $parserItemDiscount;
    }

    /**
     * @param string $strXmlResponse
     *
     * @return Response
     */
    public function parse(string $strXmlResponse): Response
    {
        /** @var Response $response */
        $response = ResponseFactory::make($strXmlResponse, 'ItemQueryRs');

        $xmlResponseObject = $response->xmlObject();

        return $this->parsingResponse($xmlResponseObject, $response);
    }

    /**
     * @param $xmlResponseObject
     * @param Response $response
     * @return Response
     */
    private function parsingResponse($xmlResponseObject, Response $response)
    {
        $result = [];

        if (isset($xmlResponseObject->QBXMLMsgsRs->ItemQueryRs->ItemServiceRet)) {
            $result = array_merge($result, $this->parserItemService->parsingQuery($xmlResponseObject));
        }

        if (isset($xmlResponseObject->QBXMLMsgsRs->ItemQueryRs->ItemOtherChargeRet)) {
            $result = array_merge($result, $this->parserItemOtherCharge->parsingQuery($xmlResponseObject));
        }

        if (isset($xmlResponseObject->QBXMLMsgsRs->ItemQueryRs->ItemDiscountRet)) {
            $result = array_merge($result, $this->parserItemDiscount->parsingQuery($xmlResponseObject));
        }

        $response->addField('ResultItems', $result);

        return $response;
    }
}
