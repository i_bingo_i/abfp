<?php

namespace QuickbooksBundle\Parser;

use QuickbooksBundle\Interfaces\ResponseParser;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Factory\ResponseFactory;

class ModCustomer implements ResponseParser
{
    /**
     * @param string $strXmlResponse
     *
     * @return Response
     */
    public function parse(string $strXmlResponse): Response
    {
        /** @var Response $response */
        $response = ResponseFactory::make($strXmlResponse, 'CustomerModRs');

        $xmlResponseObject = $response->xmlObject();

        $customerRsNode = (array) $xmlResponseObject->QBXMLMsgsRs->CustomerModRs;
        $customerRetNode = (array) $customerRsNode['CustomerRet'];

        $response->addField('ListID', $customerRetNode['ListID']);
        $response->addField('EditSequence', $customerRetNode['EditSequence']);

        return $response;
    }
}
