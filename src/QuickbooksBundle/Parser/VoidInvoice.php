<?php

namespace QuickbooksBundle\Parser;

use QuickbooksBundle\Interfaces\ResponseParser;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Factory\ResponseFactory;

class VoidInvoice implements ResponseParser
{
    public function parse(string $strXmlResponse): Response
    {
        /** @var Response $response */
        $response = ResponseFactory::make($strXmlResponse, 'TxnVoidRs');

        return $response;
    }
}
