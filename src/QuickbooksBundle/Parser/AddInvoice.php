<?php

namespace QuickbooksBundle\Parser;

use QuickbooksBundle\Interfaces\ResponseParser;
use QuickbooksBundle\Response\Response;
use QuickbooksBundle\Factory\ResponseFactory;

class AddInvoice implements ResponseParser
{
    /**
     * @param string $strXmlResponse
     *
     * @return Response
     */
    public function parse(string $strXmlResponse): Response
    {
        /** @var Response $response */
        $response = ResponseFactory::make($strXmlResponse, 'InvoiceAddRs');

        $xmlResponseObject = $response->xmlObject();
        $InvoiceAddRsNode = (array) $xmlResponseObject->QBXMLMsgsRs->InvoiceAddRs;
        $InvoiceRetNode = (array) $InvoiceAddRsNode['InvoiceRet'];

        $response->addField('TxnID', $InvoiceRetNode['TxnID']);
        $response->addField('EditSequence', $InvoiceRetNode['EditSequence']);

        return $response;
    }
}
