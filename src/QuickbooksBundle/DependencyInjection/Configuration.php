<?php

namespace QuickbooksBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('quickbooks');

        $rootNode
            ->children()
                ->scalarNode('login')->end()
                ->scalarNode('password')->end()
                ->scalarNode('url')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}