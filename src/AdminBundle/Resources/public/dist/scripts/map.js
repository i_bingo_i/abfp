var map;
var currentOrderMarker;
var currentOrderMarkerIcon;
var currentOrderInfoWindow;
var orderIcon;
var ordersMarkers = new Object();
var ordersInfoWindows = new Object();
var ordersMarkersClustering = new Object();
var currentCoord;
/**
 *
 * @param inLat
 * @param inLng
 * @param workorder
 */
function initMap(inLat, inLng, workorder) {

    /** Set center map coordinate and coordinate current WO */
    currentCoord = {lat: inLat, lng: inLng};
    /** Set Map Options */
    var mapOption = {
        center: currentCoord,
        zoom: 17
    };
    map = new google.maps.Map(document.getElementById('assignMap'), mapOption);

    SetAllMarkersForWorkorders(workorder);

    // /** Set Current WO marker options */
    // orderIcon = CreateIconParams(currentCoord, String(workorder.id));
    //
    // currentOrderMarker = new google.maps.Marker(orderIcon);

}

function ReCenterMap(event)
{
    event.preventDefault();
    map.setCenter(currentCoord);
    // map.setZoom(17);
}

/**
 *
 * @param workorder
 */
function SetAllMarkersForWorkorders(workorder)
{
    $.when(GetAllWorkordersInfo(workorder)).then(
        function (response) {
            if (response.length > 0) {
                response.forEach(function(orderItem) {
                    var itemID = parseInt(orderItem.id);

                    var orderCoord = {
                        lat: orderItem.coordinates.latitude,
                        lng: orderItem.coordinates.longitude
                    };

                    var orderIcon = CreateIconParams(orderCoord, orderItem);
                    ordersMarkers[itemID] = new google.maps.Marker(orderIcon);
                    ordersInfoWindows[itemID] = CreateInfoWindowParams(orderItem);

                    ordersMarkers[itemID].addListener('click', function() {
                        ordersInfoWindows[itemID].open(map, ordersMarkers[itemID]);
                    });

                    if (orderItem.current === false) {
                        ordersMarkersClustering[itemID] = ordersMarkers[itemID];
                    }
                });

                var markerCluster = new MarkerClusterer(map, ordersMarkersClustering, {
                    maxZoom: 14,
                    imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                });
            }
        }
    );
}

/**
 *
 * Get All workorders with with coordinates by currents division
 * @param workorder
 * @returns {*}
 * @constructor
 */
function GetAllWorkordersInfo(workorder){
    return $.ajax({
        type: "GET",
        headers: {'X-ACCESS-TOKEN': workorder.accessToken},
        url: "/api/v1/orders/map/"+workorder.id,
        dataType: "JSON",
        success: function (result) {

        }
    });
}

/**
 * Create infowindow params and content
 *
 * @param workorder
 * @returns {google.maps.InfoWindow}
 * @constructor
 */
function CreateInfoWindowParams(workorder)
{
    var contentString = '<div class="info-window-content">'+
        '<div></div>';
    if (workorder.events.length > 0) {
        workorder.events.forEach(function (eventItem) {
            contentString += '<div><b>' + eventItem.userName +
                '</b> - <b>' + eventItem.dateAs + "</b> - " + eventItem.timeFrom + " - " + eventItem.timeTo +
                '</b></div>';
        });
    }

    contentString += '<div></div>'+
        '<div>WO #' +
        workorder.id + ' - <b>' + String(workorder.accountAddress) +
        '</b></div></div>';

    return new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 400
    });
}

/**
 * Create Marker image, text and params
 *
 * @param orderPosition
 * @param workorder
 * @returns {{position: *, map: *, icon: *, label: {text: string, color: string, fontSize: string, fontWeight: string}}}
 * @constructor
 */
function CreateIconParams(orderPosition, workorder)
{
    var newOrerIcon;
    if (workorder.current === true) {
        newOrerIcon = {
            url: GetImageForMarkerIcon(workorder),
            scaledSize: new google.maps.Size(25, 41),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(14, 50),
            labelOrigin: new google.maps.Point(17, 20)
        };

        return {
            position: orderPosition,
            map: map,
            icon: newOrerIcon
        };
    }

    return {
        position: orderPosition,
        map: map
    };
}

// function pinSymbol(color) {
//     return {
//         path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
//         fillColor: color,
//         fillOpacity: 1,
//         strokeColor: '#000',
//         strokeWeight: 2,
//         scale: 1
//     };
// }

/**
 *
 * @param workorder
 * @returns {string}
 */
function GetImageForMarkerIcon(workorder)
{
    var path = 'https://admin.backflowandfire.com/bundles/admin/dist/images/map/blue_pin.png';
    // var path = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';

    // if (workorder.current === true) {
    //     path = 'https://abfp-staging.idap.pro/bundles/admin/dist/images/map/blue_pin.svg';
    // } else {
    //     path = 'https://abfp-staging.idap.pro/bundles/admin/dist/images/map/red_pin.svg';
    // }

    return path;
}
