<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class DeleteServiceForDeviceTest extends FunctionalTestCase
{
    public function testDeleteServiceForDevice()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to view Device page
        $deviceId = 1;
        $crawler = $client->request('GET', '/admin/device/view/'.$deviceId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Backflow Device ID - '.$deviceId,
            $crawler->filter('.app-detailed__info__data-id.app-device__info__data-id')->text()
        );

        //search for device services
        $servicesLinkWrapper = $crawler->filter('.app-account-site__container table tbody tr td .app-wrapper div')
            ->eq(1);
        $servicesCount = intval($servicesLinkWrapper->filter('a .app-font--inherit')->text());
        $this->assertGreaterThan(0, $servicesCount);

        //select link to watch services list and click it
        $servicesListLink = $servicesLinkWrapper->filter('a')->link();
        $crawler = $client->click($servicesListLink);

        //choice row with service we are going to delete
        $deletingServiceRow = $crawler->filter('.app-account-site__container table tbody tr')->last();

        //check, that service we are going to delete has not been deleted yet
        $this->assertNotContains(
            'Deleted',
            $deletingServiceRow->filter('td')->eq(5)->html()
        );

        //select delete service link, get data-link attribute of it and make request to the URI in data-link attribute
        $deleteServiceLink = $deletingServiceRow->filter('td')->last()->filter('a')->link();
        $deleteServiceHref = $deleteServiceLink->getNode()->getAttribute('data-link');
        $client->request('GET', $deleteServiceHref);
        $crawler = $client->followRedirect();

        //search for device services
        $servicesLinkWrapper = $crawler->filter('.app-account-site__container table tbody tr td .app-wrapper div')
            ->eq(1);
        $servicesCount = intval($servicesLinkWrapper->filter('a .app-font--inherit')->text());
        $this->assertGreaterThan(0, $servicesCount);

        //select link to watch services list and click it
        $servicesListLink = $servicesLinkWrapper->filter('a')->link();
        $crawler = $client->click($servicesListLink);

        //check, that service we were deleting has been deleted
        $this->assertContains(
            'Deleted',
            $crawler->filter('.app-account-site__container table tbody tr')->last()
                ->filter('td')->eq(5)->html()
        );
    }
}
