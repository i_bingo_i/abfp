<?php

namespace AdminBundle\tests;

use AppBundle\Tests\RetestNoticeTestingManager;

class ViewFilteredProposalsTest extends RetestNoticeTestingManager
{
    public function testViewFilteredProposals()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //filter data to show records for only one Account
        $filterOpportunitiesForm = $crawler->filter('#filter-opportunities-form')->form();
        $dateFrom = (date('d') - 1 < 10) ? '0'.(date('d') - 1) : date('d') - 1;
        $dateTo = (date('d') + 1 < 10) ? '0'.(date('d') + 1) : date('d') + 1;
        $filterOpportunitiesForm['date[from]'] = date('m/').$dateFrom.date('/Y');
        $filterOpportunitiesForm['date[to]'] = date('m/').$dateTo.date('/Y');
        $crawler = $client->submit($filterOpportunitiesForm);

        //go to page of Account for adding ContactPerson with authorizer roles to it
        $accountLink = $crawler->filter('.app-opportunities__table tbody tr td div div a')->link();
        $accountLinkText = $accountLink->getNode()->textContent;
        $crawler = $client->click($accountLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            $accountLinkText,
            $crawler->filter('.app-detailed__info__data-title')->text()
        );

        //add ContactPerson with authorizer roles to selected Account
        $crawler = $this->addContactPersonToSelectedAccount($crawler, $client);

        //filter data to create Retest Notice further
        $filterOpportunitiesForm = $crawler->filter('#filter-opportunities-form')->form();
        $dateFrom = (date('d') - 1 < 10) ? '0'.(date('d') - 1) : date('d') - 1;
        $dateTo = (date('d') + 1 < 10) ? '0'.(date('d') + 1) : date('d') + 1;
        $filterOpportunitiesForm['date[from]'] = date('m/').$dateFrom.date('/Y');
        $filterOpportunitiesForm['date[to]'] = date('m/').$dateTo.date('/Y');
        $crawler = $client->submit($filterOpportunitiesForm);

        //quick Retest Notice creation
        $quickRNCreationLink = $crawler->filter('#createNoticeModal div div div')->eq(1)
            ->filter('a')->eq(0)->link();
        $client->click($quickRNCreationLink);
        $crawler = $client->followRedirect();

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Retest Notices',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //check, that table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-notice-list tbody tr:not(.app-opportunities__table-row--account)')->count()
        );

        //filter data by division
        $filterRetestNoticesForm = $crawler->filter('#filter-opportunities-form')->form();
        $filterRetestNoticesForm['division'] = '1';
        $crawler = $client->submit($filterRetestNoticesForm);

        //get value in filter to check, whether filter is working further
        $valueInFilter = $crawler
            ->filter('select[name="division"] option[value="'.($filterRetestNoticesForm['division']->getValue()).'"]')
            ->text();

        //get table data after retest notices filtering
        $divisionsAfterFiltering = $crawler
            ->filter('.app-notice-list tbody tr:not(.app-opportunities__table-row--account)')->each(function ($node) {
                $retestNoticeDivision = $node->filter('td')->eq(2)->filter('a')->text();

                return trim(strstr($retestNoticeDivision, ' Retest Notice', true));
            });

        $afterFilteringDivisionsQuantity = 0;
        foreach ($divisionsAfterFiltering as $divisionAfterFiltering) {
            if ($divisionAfterFiltering == $valueInFilter) {
                $afterFilteringDivisionsQuantity++;
            }
        }

        //check, that there are no other data in table, except data we were searching
        $this->assertEquals(
            $afterFilteringDivisionsQuantity,
            $crawler->filter('.app-notice-list tbody tr:not(.app-opportunities__table-row--account)')->count()
        );
    }

    public function testViewFilteredProposalsOfAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $accountId = 14;
        $crawler = $client->request('GET', '/admin/account/'.$accountId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //go Account retest notices tab
        $retestNoticesTabLink = $crawler->filter('.app-account-tabs__list a')->eq(2)->link();
        $crawler = $client->click($retestNoticesTabLink);

        //check, that retest notices table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-notice-list tbody tr')->count()
        );

        //filter data by division
        $filterRetestNoticesForm = $crawler->filter('#filter-opportunities-form')->form();
        $filterRetestNoticesForm['division'] = '1';
        $crawler = $client->submit($filterRetestNoticesForm);

        //get value in filter to check, whether filter is working further
        $valueInFilter = $crawler
            ->filter('select[name="division"] option[value="'.($filterRetestNoticesForm['division']->getValue()).'"]')
            ->text();

        //get table data after retest notices filtering
        $divisionsAfterFiltering = $crawler->filter('.app-notice-list tbody tr')->each(function ($node) {
            $retestNoticeDivision = $node->filter('td')->eq(2)->filter('a')->text();

            return trim(strstr($retestNoticeDivision, ' Retest Notice', true));
        });

        $afterFilteringDivisionsQuantity = 0;
        foreach ($divisionsAfterFiltering as $divisionAfterFiltering) {
            if ($divisionAfterFiltering == $valueInFilter) {
                $afterFilteringDivisionsQuantity++;
            }
        }

        //check, that there are no other data in table, except data we were searching
        $this->assertEquals(
            $afterFilteringDivisionsQuantity,
            $crawler->filter('.app-notice-list tbody tr')->count()
        );
    }

    public function testRetestNoticesNonCreationOnNonFilteredOpportunities()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //select Quick Retest Notice creation link and click it
        $quickRNCreationLink = $crawler->filter('#createNoticeModal div div div')->eq(1)
            ->filter('a')->eq(0)->link();
        $client->click($quickRNCreationLink);
        $crawler = $client->followRedirect();

        //check, that page is the same as we requested before
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );
    }

    public function testRetestNoticesNonCreationOnNonFilteredOpportunitiesOfAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $accountId = 3;
        $crawler = $client->request('GET', '/admin/account/'.$accountId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //select Device linked to Account
        $devicesLink = $crawler->filter('#device-fire'.$accountId)->link();
        $crawler = $client->click($devicesLink);
        $testingDeviceLink = $crawler->filter('tr[device-type="showFire"]')->eq(2)
            ->filter('td')->eq(1)->filter('div div a.app-link__select')
            ->link();
        $crawler = $client->click($testingDeviceLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Fire Device ID',
            $crawler->filter('.app-detailed__info__data-id')->text()
        );

        //add Service to Device
        $crawler = $this->addServiceToDevice($crawler, $client);

        //go Account opportunities tab
        $opportunitiesTabLink = $crawler->filter('.app-account-tabs__list a')->eq(1)->link();
        $crawler = $client->click($opportunitiesTabLink);

        //check, that table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-opportunities__table tbody tr')->count()
        );

        //create quick retest notice
        $quickRNCreationLink = $crawler->filter('#createNoticeModal div div div')->eq(1)
            ->filter('a')->eq(0)->link();
        $client->click($quickRNCreationLink);
        $client->followRedirect();

        //check, that we are not redirected
        $this->assertFalse(
            $client->getResponse()->isRedirect('/admin/opportunity/list')
        );
    }
}
