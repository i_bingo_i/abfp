<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class SearchAccountForSelectTest extends FunctionalTestCase
{
    public function testSearchAccountForSelect()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Account page
        $crawler = $this->goToSearchAccountPage($client);

        //select search form, fullize and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = 'a';
        $crawler = $client->submit($form);

        //check, that search has given some results
        $this->assertContains(
            'Search results: ',
            $crawler->filter('.app-data .app-link__result')->text()
        );
    }

    public function testSearchNoResult()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Account page
        $crawler = $this->goToSearchAccountPage($client);

        //select Search form, fullize and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = 'test44';
        $crawler = $client->submit($form);

        //check, that search has not given any result
        $this->assertContains(
            'No results',
            $crawler->filter('.app-data .app-data__not-found')->text()
        );
    }

    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Account page
        $crawler = $this->goToSearchAccountPage($client);

        //check, that CANCEL link is present, select CANCEL link and click it
        $backLink = $crawler->selectLink('CANCEL')->link();
        $this->assertEquals('CANCEL', $backLink->getNode()->textContent, 'No button named "CANCEL"');
        $crawler = $client->click($backLink);

        //check, that we are on requested page
        $this->assertContains(
            'Group Account ID  -  5',
            $crawler->filter('.app-detailed__info__data-id span')->eq(0)->text(),
            'This is not a page of account with ID 5'
        );
    }

    private function goToSearchAccountPage(Client $client)
    {
        //log in
        $crawler = $client->request('GET', '/admin/account/5');

        //check, that requested page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertContains(
            'Group Account ID  -  5',
            $crawler->filter('.app-detailed__info__data-id span')->eq(0)->text(),
            'This is not a page of account with ID 5'
        );

        //select "Add Account" link and click it
        $addAccountLink = $crawler->selectLink('Add Account')->link();
        $crawler = $client->click($addAccountLink);

        //check, that we are on requested page
        $this->assertContains(
            '
            Link Site Account to
            ',
            $crawler->filter('.app-main__page-title .app-link__header--site.app-main__page-title-name')
                ->text()
        );

        //return Crawler, that crawls by search Account page
        return $crawler;
    }
}
