<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class SelectAndLinkedCompanyToContactPersonTest extends FunctionalTestCase
{
    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Company for select page
        $urlSearchCompanyForSelect = '/admin/contact/search-company-for-select/4';
        $crawler = $client->request('GET', $urlSearchCompanyForSelect);

        //check, that CANCEL link is present, select CANCEL link and click it
        $backLink = $crawler->selectLink('CANCEL')->link();
        $this->assertEquals('CANCEL', $backLink->getNode()->textContent, 'No button named "CANCEL"');
        $crawler = $client->click($backLink);

        //check, that we are on expected page
        $this->assertEquals(
            'Contact Person ID  -  4',
            $crawler
                ->filter('.app-detailed__info__data-id.app-save-space')
                ->eq(0)
                ->text()
        );
    }

    public function testGoToSearchCompanyByLinkPage()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Contact page
        $urlToContactPerson = '/admin/contact/4';
        $crawler = $client->request('GET', $urlToContactPerson);

        //check, that we are on expected page
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Contact Person ID  -  4',
            $crawler
                ->filter('.app-detailed__info__data-id.app-save-space')
                ->eq(0)
                ->text()
        );

        //check, that select link is present, select "Select" link and click it
        $link = $crawler->selectLink('Select')->link();
        $this->assertEquals('Select', $link->getNode()->textContent, 'No button named "Select"');
        $crawler = $client->click($link);

        //check, that we are on expected page
        $this->assertEquals(
            "Search and select Company",
            $crawler
                ->filter('.app-link__container.app-form--bordered .app-form-header')
                ->eq(0)
                ->text()
        );
    }

    public function testSearchAndLinkedCompanyToAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Company for select page
        $urlSearchCompanyForSelect = '/admin/contact/search-company-for-select/4';
        $crawler = $client->request('GET', $urlSearchCompanyForSelect);

        //select search form, full and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = "company";
        $form['cn_s_in_notes'] = 1;
        $crawler = $client->submit($form);

        //check count of search results
        $this->assertGreaterThan(
            0,
            $crawler->filter(".table.app-data__table.app-bordered tbody tr")->count()
        );

        //check, that "Select" link is present
        $link = $crawler->selectLink('Select')->link();
        $this->assertEquals('Select', $link->getNode()->textContent, 'No button named "Select"');

        //go to company-select page
        $urlSelectCompany = '/admin/contact/company-select/4/5';
        $client->request('GET', $urlSelectCompany);

        //go to ContactPerson page
        $urlToContactPerson = '/admin/contact/4';

        //check, that we were redirected to expected page
        $this->assertTrue(
            $client->getResponse()->isRedirect($urlToContactPerson),
            'Response is a redirect to contact page'
        );

        //go to page we were redirected
        $crawler = $client->request('GET', $urlToContactPerson);

        //check, that Company was linked to ContactPerson
        $this->assertEquals(
            'McDonalds',
            $crawler
                ->filter('.app-contact-main__line-text.app-color--white.app-cut-string a.app-link__select')
                ->eq(0)
                ->text()
        );
    }
}
