<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class EditContactPersonTest extends FunctionalTestCase
{
    public function testEditLinkInViewPage()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to ContactPerson page
        $urlToContactPerson = '/admin/contact/2';
        $crawler = $client->request('GET', $urlToContactPerson);

        //check, that requested page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');

        //get previous content of ContactPerson name block
        $prevNameBlockContent = $crawler->filter('.app-detailed__info__data-title')->text();

        //check, that "Edit" link is present, select it and click it.
        $link = $crawler->selectLink('Edit')->link();
        $this->assertEquals('Edit', $link->getNode()->textContent, 'No button named "Edit"');
        $crawler = $client->click($link);

        //check, that update ContactPerson page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');

        //on update ContactPerson page

        //check presence of update ContactPerson form
        $this->assertEquals(
            'Update Contact Person Information',
            $crawler
                ->filter('.app-main__page-title-name')
                ->eq(0)
                ->text()
        );

        //select update ContactPerson form, full required field and submit it
        $form = $crawler->selectButton('UPDATE')->form();
        $form['contact_person[firstName]'] = ($prevNameBlockContent == 'Test Viktorovich') ? 'TestEdited' : 'Test';
        $client->submit($form);
        $crawler = $client->followRedirect();

        //on page we were redirected (ContactPerson page)

        //get content of ContactPerson name block after editing
        $nameBlockContent = $crawler->filter('.app-detailed__info__data-title')->text();

        //check, that ContactPerson name block content was changed
        $this->assertNotEquals(
            $nameBlockContent,
            $prevNameBlockContent
        );
    }
}
