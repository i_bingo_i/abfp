<?php

namespace AdminBundle\tests\TestTrait;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\DomCrawler\Field\ChoiceFormField;

trait SearchClientsTrait
{
    /**
     * @param Client $client
     * @return Crawler
     */
    private function goToSearchPage(Client $client)
    {
        /** @var Crawler $crawler */
        $crawler = $client->request('GET', '/admin/client/search');

        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Clients',
            $crawler
                ->filter('.app-main__page-title-name')
                ->eq(0)
                ->text()
        );

        return $crawler;
    }

    /**
     * @param Crawler $crawler
     * @param $searchTab
     * @param $count
     */
    private function assertCountSearchResult(Crawler $crawler, $searchTab, $count)
    {
        $this->assertEquals(
            $count,
            $crawler->filter(".tab-content div$searchTab tr.app-data__table-row--close")->count()
        );
    }

    /**
     * @param Crawler $crawler
     * @param $searchPhrase
     * @return Form
     */
    private function getSearchForm(Crawler $crawler, $searchPhrase)
    {
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = $searchPhrase;

        return $form;
    }

    /**
     * @param Form $form
     * @param $fieldName
     * @return Form
     */
    private function unTickCheckbox(Form $form, $fieldName)
    {
        /** @var ChoiceFormField $checkbox */
        $checkbox = $form[$fieldName];
        $checkbox->untick();

        return $form;
    }

    /**
     * @param Form $form
     * @param $fieldName
     * @return Form
     */
    private function tickCheckbox(Form $form, $fieldName)
    {
        /** @var ChoiceFormField $checkbox */
        $checkbox = $form[$fieldName];
        $checkbox->tick();

        return $form;
    }
}
