<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewListOfRelatedThroughAuthorizerRetestNoticesTest extends FunctionalTestCase
{
    public function testViewListOfRelatedThroughAuthorizerRetestNoticesTest()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to opportunities tab of Account page
        $accountId = 3;
        $crawler = $client->request('GET', '/admin/account/reset-notices/'.$accountId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Account ID',
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //get Retest Notice reference id to check, that we will be on the correct Retest Notice page further
        $retestNoticeRefId = $crawler->filter('.app-notice-list tbody tr')->first()->filter('td')
            ->first()->text();

        //select Retest Notice link and click it
        $retestNoticeLink = $crawler->filter('.app-notice-list tbody tr')->first()->filter('td')
            ->eq(2)->filter('a')->link();
        $crawler = $client->click($retestNoticeLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            $retestNoticeRefId,
            $crawler->filter('.app-device__info__data-id')->first()->text()
        );

        //get Contact Person name to check, that we are on the Retest Notices page of the correct ContactPerson
        $contactPersonName = $crawler->filter('.app-contact-list__contact-name')->eq(1)->text();

        //select view other person related retest notices link and click it
        $otherPersonRelatedRNLink = $crawler->selectLink('View other person related retest notices')->link();
        $crawler = $client->click($otherPersonRelatedRNLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Retest Notices related to',
            $crawler->filter('.app-link__header--retest-notice')->text()
        );
        $this->assertContains(
            $contactPersonName,
            $crawler->filter('.app-link__title--retest-notice a')->text()
        );

        //check, that Retest Notices table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-notice-list tbody tr')->count()
        );
    }
}
