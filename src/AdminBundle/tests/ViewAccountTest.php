<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewAccountTest extends FunctionalTestCase
{
    public function testViewAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $urlToAccount = '/admin/account/3';

        //check, that Account page has been shown correctly
        $crawler = $client->request('GET', $urlToAccount);
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Site Account ID  -  3',
            $crawler
                ->filter('.app-detailed__info__data-id')
                ->filter('span')
                ->text()
        );
    }

    public function testViewGroupAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Group Account page
        $urlToAccount = '/admin/account/2';
        $crawler = $client->request('GET', $urlToAccount);

        //check that Group Account page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Group Account ID  -  2',
            $crawler
                ->filter('.app-detailed__info__data-id')
                ->filter('span')
                ->text()
        );
    }
}
