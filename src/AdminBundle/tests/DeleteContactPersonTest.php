<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class DeleteContactPersonTest extends FunctionalTestCase
{
    public function testDeleteContactPerson()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Contact page
        $urlToContactPerson = "/admin/contact/1";
        $crawler = $client->request('GET', $urlToContactPerson);

        //check, that Contact page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');

        //check, that "Delete" link is present, select and click it
        $link = $crawler->selectLink('Delete')->link();
        $this->assertEquals('Delete', $link->getNode()->textContent, 'No button named "Delete"');
        $client->click($link);


        //check, that confirm delete link is present in dialog, select and click it
        $confirmDeleteLink = $crawler
            ->filter('a.app-detailed__button.app-detailed__button--red.btn')
            ->eq(1)
            ->link()
        ;
        $this->assertEquals('Yes', $confirmDeleteLink->getNode()->textContent, 'No button named "Yes"');
        $client->click($confirmDeleteLink);

        //check, that we are redirecting to expected page
        $this->assertTrue(
            $client->getResponse()->isRedirect($urlToContactPerson),
            'Response is a redirect to search account page'
        );

        //go to the page, we were redirected
        $crawler = $client->request('GET', $urlToContactPerson);

        //check, that ContactPerson checked as deleted
        $this->assertContains(
            "Deleted",
            $crawler
                ->filter('.app-detailed__info__data-id.app-save-space')
                ->html()
        );
    }
}
