<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class DeleteAgentFromDepartmentTest extends FunctionalTestCase
{
    public function testDeleteAgentFromDepartment()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to view Municipality page
        $municipalityId = 2;
        $crawler = $client->request('GET', '/admin/municipality/view/'.$municipalityId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );

        //check, that Agent we are going to delete has not been deleted yet
        $this->assertContains(
            'Apple',
            $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1)->html()
        );

        //get block with Department, which we are going to delete Agent from
        $deletingAgentDepartmentBlock = $crawler->filter('.municipalities-devices.app-detailed__wrapper')
            ->eq(1);
        $deletingAgentLinksBlock = $deletingAgentDepartmentBlock->filter('.municipalities-devices__content')
            ->eq(1)->filter('.municipalities-department-agent-title');

        //get deleting Agent link text to check its absence further in selected department block
        $deletingAgentLinkText = $deletingAgentLinksBlock->filter('span a')->text();

        //select link for unlink Agent from Department, get its "data-link" attribute to make request there further
        $deleteAgentLinkHref = $deletingAgentLinksBlock->filter('.municipalities-styles__contacts-nav span a')
            ->attr('data-link');

        //make request to unlink Agent from Department
        $client->request('GET', $deleteAgentLinkHref);
        $crawler = $client->followRedirect();

        //check, that we were redirected to the page we had to be redirected
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );
        
        //check, that Agent we were deleting has been deleted
        $this->assertNotContains(
            $deletingAgentLinkText,
            $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1)->html()
        );
    }
}
