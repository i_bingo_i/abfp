<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class DeleteComplianceChannelFromDepartmentTest extends FunctionalTestCase
{
    public function testDeleteComplianceChannelFromDepartment()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to view Municipality page
        $municipalityId = 2;
        $crawler = $client->request('GET', '/admin/municipality/view/'.$municipalityId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );

        //select department we are going to work with
        $departmentBlock = $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1);

        //get row object for department channel we are going to delete
        $deletingDepartmentChannelRow = $departmentBlock->filter('.municipalities-channel-table table tbody tr');

        //get department channels count to compare it with department channels count after deleting
        $prevDepartmentChannelsCount = $deletingDepartmentChannelRow->count();

        //select department channels table row with department channel we are going to delete
        $deletingDepartmentChannel = $deletingDepartmentChannelRow->first();

        //select link for deleting channel, get "data-link" attribute of it to make request further
        $deleteChannelLink = $deletingDepartmentChannel->filter('td')->last()->filter('a')->link();
        $deleteChannelLinkHref = $deleteChannelLink->getNode()->getAttribute('data-link');

        //make request to delete department channel
        $client->request('GET', $deleteChannelLinkHref);
        $crawler = $client->followRedirect();

        //check, that we are on the page we have been redirected
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );

        //get current department channels count
        $departmentChannelsCount = $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1)
            ->filter('.municipalities-channel-table table tbody tr')->count();
        
        //check, that current department channels count has been increased by 1
        $this->assertEquals(
            $prevDepartmentChannelsCount - 1,
            $departmentChannelsCount
        );
    }
}
