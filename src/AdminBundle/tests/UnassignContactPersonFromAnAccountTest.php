<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class UnassignContactPersonFromAnAccountTest extends FunctionalTestCase
{
    public function testUnassignContactPersonFromAnAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to account contact person manage page
        $accountId = 1;
        $acpManageURL = $this->generateUrl('admin_account_contact_person_manage', ['account' => $accountId]);
        $crawler = $client->request('GET', $acpManageURL);
        $acpManagePageTitleBlock = $crawler->filter('.app-main__page-title-name');

        //check, that we are on requested page
        $this->assertContains(
            'Contacts',
            $acpManagePageTitleBlock->text()
        );
        $this->assertEquals(
            $this->generateUrl('admin_account_get', ['accountId' => $accountId]),
            $acpManagePageTitleBlock->filter('a')->attr('href')
        );

        //get unassigning ContactPerson href to check its absence further
        $contactPersonTableRow = $crawler->filter('.app-contact-list__container--main')->first()
            ->filter('div div table tbody tr')->first();
        $contactPersonLinkHref = $contactPersonTableRow->filter('td')->first()
            ->filter('div span a')->attr('href');
        $deleteContactPersonLink = $contactPersonTableRow->filter('td')->last()
            ->filter('div a.app-detailed__button--red')->link();

        //select delete ContactPerson link and click it
        $deleteContactPersonLinkText = $deleteContactPersonLink->getNode()->getAttribute('data-link');
        $client->request('GET', $deleteContactPersonLinkText);
        $crawler = $client->followRedirect();

        //check, that Account not contains ContactPerson we have just deleted
        $this->assertNotContains(
            $contactPersonLinkHref,
            $crawler->filter('.app-main')->html()
        );
    }
}
