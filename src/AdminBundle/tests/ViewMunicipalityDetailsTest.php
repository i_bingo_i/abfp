<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewMunicipalityDetailsTest extends FunctionalTestCase
{
    public function testViewMunicipalityDetails()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to municipalities list page
        $crawler = $client->request('GET', '/admin/municipality/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipalities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //check, that municipalities list is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );

        //select search form, full and submit it
        $searchForm = $crawler->filter('#searchForm')->form();
        $searchForm['searchPhrase'] = 'a';
        $crawler = $client->submit($searchForm);

        //check, that search has brought at least one result
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );

        //get Municipality details page link and its text content from search results table
        $testingMunicipality = $crawler->filter('.app-contractors-table_style table tbody tr')->first();
        $municipalityDetailsPageLink = $testingMunicipality->filter('td')->eq(2)->filter('a')
            ->link();
        $municipalityDetailsPageLinkText = $municipalityDetailsPageLink->getNode()->textContent;

        //go to testing Municipality details page
        $crawler = $client->click($municipalityDetailsPageLink);

        //check, that requested page has been shown correctly
        $this->assertEquals(
            $municipalityDetailsPageLinkText,
            $crawler->filter('.municipalities-styles__wrapper')->filter('div')->eq(5)->text()
        );
    }
}
