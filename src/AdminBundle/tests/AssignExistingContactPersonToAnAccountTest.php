<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class AssignExistingContactPersonToAnAccountTest extends FunctionalTestCase
{
    public function testAssignExistingContactPersonToAnAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search and select contact person page
        $accountId = 1;
        $crawler = $this->goToSearchAndSelectContactPersonPage($client, $accountId, $prevContsQ);

        //search for existing contact person
        $contactPersonSearchForm = $crawler->selectButton('SEARCH')->form();
        $contactPersonSearchForm['searchPhrase'] = 'a';
        $crawler = $client->submit($contactPersonSearchForm);

        //expect that quantity of found results is greater than 0
        $this->assertGreaterThan(
            0,
            substr($crawler->filter('.app-link__result')->text(), 16)
        );

        //find the first contact person showing in contact people table and select it
        $selectingContactPersonRow = $crawler->filter('.app-data table tbody tr')->eq(0);
        $selectContactPersonLink = $selectingContactPersonRow->filter('td')->last()->filter('a')->link();
        $contactPersonID = $selectingContactPersonRow->filter('td')->eq(1)->text();
        $crawler = $client->click($selectContactPersonLink);

        //check, that we are on expected page
        $this->assertContains(
            'Set Contact Person Role and Responsibility',
            $crawler->filter('.app-form-header')->text()
        );

        //test cancel button on contact person roles and responsibility form
        $this->intermediateTestContactPersonResponsibilityCancelButton(
            $client,
            $accountId,
            $contactPersonID,
            $prevContsQ
        );

        //select contact person roles and responsibilities form and submit it
        $cpRoleAndResponsibilityForm = $crawler->selectButton('SAVE')->form();
        $client->submit($cpRoleAndResponsibilityForm);
        $crawler = $client->followRedirect();

        //check, that we are redirected to expected page (Contacts)
        $accountContactsPageTitleBlock = $crawler->filter('.app-main__page-title-name');
        $this->assertContains(
            'Contacts',
            $accountContactsPageTitleBlock->text()
        );
        $accountLink = $accountContactsPageTitleBlock->filter('a')->link();
        $crawler = $client->click($accountLink);

        //check, that we are on the page of the testing account
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that quantity of account contacts equals previous quantity of accounts contact + 1
        $viewAndManageAllContactsLinkText = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->getNode(0)->textContent;
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $contsQ = $matches[1];
        $this->assertEquals($contsQ, $prevContsQ + 1);
    }

    public function testSearchNoResults()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search and select contact person page
        $accountId = 1;
        $crawler = $this->goToSearchAndSelectContactPersonPage($client, $accountId);

        //search for non-existing contact person and submit search request
        $contactPersonSearchForm = $crawler->selectButton('SEARCH')->form();
        $contactPersonSearchForm['searchPhrase'] = 'no results';
        $crawler = $client->submit($contactPersonSearchForm);

        //check, that no search results found
        $this->assertContains(
            'No results',
            $crawler->filter('.app-data__not-found')->text()
        );
    }

    public function testSelectAndSearchContactPersonCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search and select contact person page
        $accountId = 1;
        $crawler = $this->goToSearchAndSelectContactPersonPage($client, $accountId, $prevContsQ);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that we are on the expected page (Account)
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that quantity of account contacts was not changed
        $viewAndManageAllContactsLinkText = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->getNode(0)->textContent;
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $contsQ = $matches[1];
        $this->assertEquals($contsQ, $prevContsQ);
    }

    private function goToSearchAndSelectContactPersonPage(
        Client $client,
        int $accountId,
        ?int &$prevContsQ = null
    ) : Crawler {

        //go to account page
        $crawler = $client->request('GET', '/admin/account/'.$accountId);

        //check, that we are on Account page
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //go to page of all account contacts
        $viewAndManageAllContactsLink = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->link();
        $viewAndManageAllContactsLinkText = $viewAndManageAllContactsLink->getNode()->textContent;

        //assign previous contacts quantity to $prevContsQ variable to test whether contacts were added
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $prevContsQ = $matches[1];

        //go to page "View and Manage All Contacts"
        $crawler = $client->click($viewAndManageAllContactsLink);
        $this->assertContains(
            'Contacts',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //go to adding contact person to account with $accountId page
        $addContactPersonLink = $crawler->selectLink('Add Contact Person')->link();
        $crawler = $client->click($addContactPersonLink);
        $this->assertContains(
            'Search and select Contact person',
            $crawler->filter('.app-form-header')->text()
        );
        return $crawler;
    }

    private function intermediateTestContactPersonResponsibilityCancelButton(
        Client $client,
        int $accountId,
        int $contactPersonId,
        int $prevContsQ
    ) {

        //go to AccountContactPerson set roles page
        $crawler = $client->request('GET', '/admin/account-contact-person-set-roles/'.$accountId.'/'.$contactPersonId);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);
        $accountContactsPageTitleBlock = $crawler->filter('.app-main__page-title-name');
        $this->assertContains(
            'Contacts',
            $accountContactsPageTitleBlock->text()
        );

        //go to Account page from the contacts page
        $accountLink = $accountContactsPageTitleBlock->filter('a')->link();
        $crawler = $client->click($accountLink);

        //check, that we are on expected page
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that quantity of Account contacts was not changed
        $viewAndManageAllContactsLinkText = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->getNode(0)->textContent;
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $contsQ = $matches[1];
        $this->assertEquals($contsQ, $prevContsQ);
    }
}
