<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class UnrelateRelatedSiteFromCurrentGroupAccountTest extends FunctionalTestCase
{
    public function testUnrelateRelatedSiteFromCurrentGroupAccount()
    {
        //Log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Group Account page
        $testingAccountID = 2;
        $testingAccountURL = $this->generateUrl('admin_account_get', ['accountId' => $testingAccountID]);
        $crawler = $client->request('GET', $testingAccountURL);

        //check, that we are on requested page
        $this->assertContains(
            'Group Account ID  -  '.$testingAccountID,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //search for related account href
        $relatedAccountDataRow = $crawler->filter('.app-devices-table__container table tbody tr')->first();
        $relatedAccountDataDiv = $relatedAccountDataRow->filter('td div .app-detailed__info');
        $relatedAccountHref = $relatedAccountDataDiv->filter('div')->eq(0)->filter('a')->attr('href');

        //select delete related Account link and make request to route it points
        $deleteRelatedAccountLink = $relatedAccountDataDiv->filter('div a')->eq(1)->link();
        $deleteRelatedAccountRoute = $deleteRelatedAccountLink->getNode()->getAttribute('data-link');
        $client->request('GET', $deleteRelatedAccountRoute);
        $crawler = $client->followRedirect();

        //assert, that Site Account is no longer relate to Group Account
        $this->assertNotContains(
            $relatedAccountHref,
            $crawler->filter('.app-devices-table__container ')->html()
        );
    }
}
