<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Form;
use AdminBundle\tests\TestTrait\SearchClientsTrait;

class SearchClientPersonTest extends FunctionalTestCase
{
    use SearchClientsTrait;

    public function testSearchClientPerson()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        /**
         * @var Form $form
         * Select search form and submit it
         */
        $form = $this->getSearchForm($crawler, 'test');
        $crawler = $client->submit($form);

        //check count of search results
        $this->assertCountSearchResult($crawler, '#panel2', 7);
    }

    public function testSearchClientPersonNotNotes()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        /**
         * @var Form $form
         * Select search form, then full and submit it
         */
        $form = $this->getSearchForm($crawler, 'test');
        $form = $this->unTickCheckbox($form, 'c_s_in_notes');
        $crawler = $client->submit($form);

        //check quantity of search results
        $this->assertCountSearchResult($crawler, '#panel2', 6);
    }

    public function testSearchClientPersonCOD()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        /**
         * @var Form $form
         * Select search form, then full and submit it
         */
        $form = $this->getSearchForm($crawler, 'test');
        $form = $this->tickCheckbox($form, 'c_s_cod');
        $crawler = $client->submit($form);

        //check count of search results
        $this->assertCountSearchResult($crawler, '#panel2', 2);
    }

    public function testSearchNoResult()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        //Select search form, then full and submit it
        $form = $this->getSearchForm($crawler, 'test44');
        $crawler = $client->submit($form);

        //check, that search has not given any result
        $this->assertEquals(
            'No results',
            $crawler->filter('.tab-content div#panel2 div.app-data__not-found')->text()
        );
    }
}
