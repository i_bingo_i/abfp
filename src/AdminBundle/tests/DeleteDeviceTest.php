<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class DeleteDeviceTest extends FunctionalTestCase
{
    public function testDeleteDevice()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Device page
        $urlToDevice = "/admin/device/view/2";
        $crawler = $client->request('GET', $urlToDevice);

        //check, that Device page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');

        //check, that "Delete" link is present, select and click it
        $link = $crawler->selectLink('Delete')->link();
        $this->assertEquals('Delete', $link->getNode()->textContent, 'No button named "Delete"');
        $client->click($link);

        //check, that confirm delete link is present in dialog, select and click it
        $confirmDeleteLink = $crawler
            ->filter('#deleteDeviceModal a.app-detailed__button.app-detailed__button--red.btn')
            ->eq(0)
            ->link();
        $this->assertEquals('Yes', $confirmDeleteLink->getNode()->textContent, 'No button named "Yes"');
        $client->click($confirmDeleteLink);

        //check, that we are redirecting to expected page
        $this->assertTrue(
            $client->getResponse()->isRedirect($urlToDevice),
            'Response is a redirect to search client page'
        );

        //go to the page, we were redirected
        $crawler = $client->request('GET', $urlToDevice);

        //check, that Device checked as deleted
        $this->assertEquals(
            "Deleted",
            $crawler
                ->filter('.app-detailed__info__data-id.app-save-space span.label.label-danger')
                ->eq(0)
                ->text()
        );
    }
}
