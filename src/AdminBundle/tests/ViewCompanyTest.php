<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewCompanyTest extends FunctionalTestCase
{
    public function testViewCompany()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Company page
        $urlToCompany = "/admin/company/1";
        $crawler = $client->request('GET', $urlToCompany);

        //check, that requested page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Company One',
            $crawler
                ->filter('.app-detailed__info__data-title.app-cut-string')
                ->eq(0)
                ->text()
        );
    }
}
