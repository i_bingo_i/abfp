<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class AddNewDeviceTest extends FunctionalTestCase
{
    public function testAddNewDevice()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to add new device page
        $accountId = 1;
        $crawler = $this->goToDeviceInformationForm($client, $accountId, $previousDevicesCount);

        //check that we are on the page with the same form as we requested
        $this->assertContains(
            'Choose Size',
            $crawler->filter('#form_device_device_fields_0_optionValue option')->eq(0)->text()
        );
        $this->assertContains(
            'Choose Water Supply Source',
            $crawler->filter('#form_device_device_fields_2_optionValue option')->eq(0)->text()
        );

        //select add new device form
        $deviceForm = $crawler->selectButton('CREATE')->form();

        //fullize device form required fields and submit it
        $deviceForm['form_device[device][fields][0][optionValue]'] = 1;
        $deviceForm['form_device[device][fields][2][optionValue]'] = 12;
        $client->submit($deviceForm);
        $crawler = $client->followRedirect();

        //check, that we are redirected to the page we expected to see
        $this->assertContains(
            'Devices and Services',
            $crawler->filter('.app-account-site__header div')->text()
        );

        //if quantity of devices equals previous quantity of devices + 1, new device was added
        $this->assertEquals(
            $previousDevicesCount + 1,
            (int) $crawler->filter('#deviceCount')->text()
        );
    }

    public function testAddNewEmptyDevice()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to add new device page
        $accountId = 1;
        $crawler = $this->goToDeviceInformationForm($client, $accountId, $previousDevicesCount);

        //check that we are on the page with the same form as we requested
        $this->assertContains(
            'Choose Size',
            $crawler->filter('#form_device_device_fields_0_optionValue option')->eq(0)->text()
        );
        $this->assertContains(
            'Choose Water Supply Source',
            $crawler->filter('#form_device_device_fields_2_optionValue option')->eq(0)->text()
        );

        //send empty form
        $deviceForm = $crawler->selectButton('CREATE')->form();
        $client->submit($deviceForm);
        $crawler = $client->followRedirect();

        //check, that we are redirected to the page we expected to see
        $this->assertContains(
            'Devices and Services',
            $crawler->filter('.app-account-site__header div')->text()
        );

        //if quantity of devices equals previous quantity of devices + 1, new device was added
        $this->assertEquals(
            $previousDevicesCount + 1,
            (int) $crawler->filter('#deviceCount')->text()
        );
    }

    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to add new device page
        $accountId = 1;
        $crawler = $this->goToDeviceInformationForm($client, $accountId, $previousDevicesCount);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that we are redirected to the page we expected to see
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data.app-wrapper .app-detailed__info__data-id span')->text()
        );
        $this->assertContains(
            'Devices and Services',
            $crawler->filter('.app-account-site__header div')->text()
        );

        //if quantity of devices was not changed comparing with previous devices count, then device was not added
        $this->assertEquals(
            $previousDevicesCount,
            (int) $crawler->filter('#deviceCount')->text()
        );
    }

    private function goToDeviceInformationForm(Client $client, int $accountId, ?int &$previousDevicesCount) : Crawler
    {
        //go to account page and checking, that we are on it
        $crawler = $client->request('GET', '/admin/account/'.$accountId);
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data.app-wrapper .app-detailed__info__data-id span')->text()
        );

        //checking presence "Add New Device" link on the page
        $this->assertContains(
            'Add New Device',
            $crawler->selectLink('Add New Device')->text()
        );

        //get previous (before adding or canceling of adding) devices count and return it by reference
        $previousDevicesCount = ($crawler->filter('#deviceCount')->count() == 0) ? 0 :
            intval($crawler->filter('#deviceCount')->text());

        //then select add new device link and click it
        $addNewDeviceLink = $crawler->selectLink('Add New Device')->link();
        $crawler = $client->click($addNewDeviceLink);

        //check apearing of "Select Device" modal dialog
        $this->assertContains(
            'Select Device',
            $crawler->filter('#selectDeviceForm select option')->eq(0)->text()
        );

        //select device type and submit form with that data
        $deviceNamedHref = $crawler->filter('#selectDeviceForm select option')->eq(1)->attr('data-link');
        $selectDeviceForm = $crawler->filter('#selectDeviceForm .app-modal__footer--right button[type="submit"]')
            ->form();
        $client->submit($selectDeviceForm);

        //go to form for adding device with selected type
        $crawler = $client->request('GET', $deviceNamedHref);

        //check that we are on the page with requested form
        $this->assertContains(
            'Device Information',
            $crawler->filter('.app-form-header')->text()
        );

        //return Crawler which Crawls by add new device page
        return $crawler;
    }
}
