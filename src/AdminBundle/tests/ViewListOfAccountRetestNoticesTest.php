<?php

namespace AdminBundle\tests;

use AppBundle\Tests\RetestNoticeTestingManager;

class ViewListOfAccountRetestNoticesTest extends RetestNoticeTestingManager
{
    public function testViewListOfAccountRetestNotices()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //filter opportunities by date and division
        $filterOpportunitiesForm = $crawler->filter('#filter-opportunities-form')->form();
        $dateFrom = new \DateTime('-1 day');
        $dateTo = new \DateTime('+1 day');
        $filterOpportunitiesForm['date[from]'] = $dateFrom->format('m/d/Y');
        $filterOpportunitiesForm['date[to]'] = $dateTo->format('m/d/Y');
        $crawler = $client->submit($filterOpportunitiesForm);

        //check, that opportunities table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-opportunities__table tbody tr')->count()
        );

        //select Account link and click it
        $accountLink = $crawler->filter('.app-opportunities__table tbody tr.app-opportunities__table-row--account')
            ->last()->filter('td div div a')->link();
        $crawler = $client->click($accountLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Account ID  -  ',
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //add ContactPerson with authorizer roles to selected Account and return to opportunities list page
        $crawler = $this->addContactPersonToSelectedAccount($crawler, $client);

        //filter opportunities by date and division
        $filterOpportunitiesForm = $crawler->filter('#filter-opportunities-form')->form();
        $dateFrom = new \DateTime('-1 day');
        $dateTo = new \DateTime('+1 day');
        $filterOpportunitiesForm['date[from]'] = $dateFrom->format('m/d/Y');
        $filterOpportunitiesForm['date[to]'] = $dateTo->format('m/d/Y');
        $crawler = $client->submit($filterOpportunitiesForm);

        //check, that opportunities table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-opportunities__table tbody tr')->count()
        );

        //create quick Retest Notice
        //select quick Retest Notice creation link and click it
        $quickRNCreationLink = $crawler->filter('#createNoticeModal div div div')->eq(1)
            ->filter('a')->eq(0)->link();
        $client->click($quickRNCreationLink);

        //select Account link and click it
        $accountLink = $crawler->filter('.app-opportunities__table tbody tr.app-opportunities__table-row--account')
            ->last()->filter('td div div a')->link();
        $crawler = $client->click($accountLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Account ID  -  ',
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //select Retest Notices tab
        $retestNoticesTabLink = $crawler->filter('.app-account-tabs__list a')->eq(2)->link();
        $crawler = $client->click($retestNoticesTabLink);

        //check, that there are, at least, one Retest Notice in the table
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-notice-list tbody tr')->count()
        );
    }
}
