<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class SetSendingAddressTest extends FunctionalTestCase
{
    public function testSetSendingAddress()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $accountId = 8;
        $crawler = $client->request('GET', '/admin/account/'.$accountId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Account ID',
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );
        
        //select "View and Manage all Contacts" link and click it
        $viewAccountContactsLink = $crawler->filter('.app-detailed__button-container')->eq(1)->filter('a')->link();
        $crawler = $client->click($viewAccountContactsLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Contacts',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //select "Add Contact Person" link and click it
        $addContactPersonLink = $crawler->selectLink('Add Contact Person')->link();
        $crawler = $client->click($addContactPersonLink);

        //select form for ContactPerson searching and submit it
        $searchContactPersonForm = $crawler->selectButton('SEARCH')->form();
        $searchContactPersonForm['searchPhrase'] = 'test';
        $crawler = $client->submit($searchContactPersonForm);

        //check, that table of search results is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-data__table tbody tr')->count()
        );

        //select row from table of search results
        $searchResultRow = $crawler->filter('.app-data__table tbody tr')->first();

        //select link for ContactPerson selecting and click it
        $contactPersonSelectLink = $searchResultRow->filter('td')->last()->filter('a')->link();
        $crawler = $client->click($contactPersonSelectLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Set Contact Person Role and Responsibility',
            $crawler->filter('.app-form-header')->text()
        );

        //select form for changing ContactPerson role and responsibility and submit it
        $contactPersonRolesAndResponsibilitiesForm = $crawler->selectButton('SAVE')->form();
        $client->submit($contactPersonRolesAndResponsibilitiesForm);
        $crawler = $client->followRedirect();

        //check, that the page we had been redirected was shown correctly
        $this->assertContains(
            'Contacts',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //get sending address to compare it with sending address we are going to set further
        $addressBefore = $crawler->filter('.app-contact-list__table--large')->eq(0)
            ->filter('.app-contact-main__line-text')->eq(3)->text();

        //select "Change Roles" link and follow it to set another sending address
        $contactPersonRolesChangingLink = $crawler->selectLink('Change Roles')->link();
        $crawler = $client->click($contactPersonRolesChangingLink);

        //select ContactPerson roles and responsibility changing form, change sending address and submit it
        $contactPersonRolesResponsibilitiesAndSendingAddressForm = $crawler->selectButton('SAVE')->form();
        $contactPersonRolesResponsibilitiesAndSendingAddressForm['account_contact_person[sendingAddress]'] = 55;
        $client->submit($contactPersonRolesResponsibilitiesAndSendingAddressForm);
        $crawler = $client->followRedirect();

        //check, that the page we had been redirected was shown correctly
        $this->assertContains(
            'Contacts',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //get sending address after changing
        $addressAfter = $crawler->filter('.app-contact-list__table--large')->eq(0)
            ->filter('.app-contact-main__line-text')->eq(3)->text();

        //check, that sending addresses before and after changing are different
        $this->assertNotContains(
            $addressBefore,
            $crawler->filter('.app-contact-list__table--large')->eq(0)
                ->filter('.app-contact-main__line-text')->eq(3)->text()
        );
        $this->assertContains(
            $addressAfter,
            $crawler->filter('.app-contact-list__table--large')->eq(0)
                ->filter('.app-contact-main__line-text')->eq(3)->text()
        );
    }
}
