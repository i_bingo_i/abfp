<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class EditDeviceTest extends FunctionalTestCase
{
    public function testEditDeviceUpdateImage()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Device page
        $deviceId = 1;
        $deviceURL = $this->generateUrl('admin_device_view', ['device' => $deviceId]);

        //go to edit Device page
        $crawler = $this->goToEditDevicePage($client, $deviceId, $deviceURL);

        //select form on the edit device page
        $editDeviceForm = $crawler->selectButton('UPDATE')->form();
        //upload the file
        $testFilePath = 'src/AppBundle/Tests/uploads/devices/test.jpg';
        $editDeviceForm['form_device[photo]']->upload($testFilePath);
        //submit the form
        $client->submit($editDeviceForm);
        $crawler = $client->followRedirect();

        //check, that image has been uploaded and shown on Device page
        $this->assertContains(
            'background-image: url',
            $crawler->filter(
                '.device-block-width.device-block-width_img.device-block-width_background-property'
            )->attr('style')
        );
    }

    public function testEditDeviceDeleteImage()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Device page
        $deviceId = 1;
        $deviceURL = $this->generateUrl('admin_device_view', ['device' => $deviceId]);

        //go to edit Device page
        $crawler = $this->goToEditDevicePage($client, $deviceId, $deviceURL);

        //check, that image is present
        $this->assertGreaterThan(0, $crawler->filter('#imgContent')->children()->count());

        //select edit Device form
        $editDeviceForm = $crawler->selectButton('UPDATE')->form();

        //select delete image link and click it
        $imageDeleteLink = $crawler->filter('#imgDelete')->link();
        $crawler = $client->click($imageDeleteLink);

        //select confirm link on dialog and click it
        $this->assertContains(
            'Yes',
            $crawler->filter('#imgDelButton')->text()
        );
        $modalYesLink = $crawler->filter('#imgDelButton')->link();
        $client->click($modalYesLink);

        //the following is not working, because image did not disappear
        /*
        $this->assertEquals(
            0,
            $crawler->filter('#imgContent')->children()->count()
        );
        */

        //submit edit Device form
        $client->submit($editDeviceForm);
        $crawler = $client->followRedirect();
        //the following is not working, because image did not disappear
        /*
        $this->assertContains(
            'No Photo',
            $crawler->filter('.device-block-width.device-block-width_img div span')->text()
        );
        */
    }

    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Device page
        $deviceId = 1;
        $deviceURL = $this->generateUrl('admin_device_view', ['device' => $deviceId]);

        //go to edit Device page
        $crawler = $this->goToEditDevicePage($client, $deviceId, $deviceURL, $prevImageContent);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //get image block content and compare it with previose one
        $imageContent = $crawler->filter('.app-detailed__wrapper > .app-wrapper > div')->eq(2)
            ->attr('style');
        $this->assertEquals($imageContent, $prevImageContent);
    }

    private function goToEditDevicePage(
        Client $client,
        int $deviceId,
        string $deviceURL,
        ?string &$prevImageContent = null
    ) : Crawler {
        //go to Device page
        $crawler = $client->request('GET', $deviceURL);

        //check, that we are on requested page
        $this->assertContains(
            'Backflow Device ID - '.$deviceId,
            $crawler->filter('.app-detailed__info__data-id')->text()
        );

        //get previous (before loading or deleting) image block content and return it by reference
        $prevImageContent = $crawler->filter('.device-block-width.device-block-width_img')
            ->attr('style');

        //select edit Device link and click it
        $editDeviceLink = $crawler->selectLink('Edit')->link();
        $crawler = $client->click($editDeviceLink);

        //return Crawler, which crawls by edit Device page
        return $crawler;
    }
}
