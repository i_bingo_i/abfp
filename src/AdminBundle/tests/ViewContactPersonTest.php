<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewContactPersonTest extends FunctionalTestCase
{
    public function testViewContactPerson()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to ContactPerson page
        $urlToContactPerson = $this->generateUrl('admin_contact_person_get', ['contactPerson' => 1]);
        $crawler = $client->request('GET', $urlToContactPerson);

        //check, that requested page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Eddard Stark',
            $crawler
                ->filter('.app-detailed__info__data-title.app-cut-string')
                ->eq(0)
                ->text()
        );
    }
}
