<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class UnRelateCompanyFromAccountTest extends FunctionalTestCase
{
    public function testUnRelateCompanyFromAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page and check, that it has been shown correctly
        $accountId = 14;
        $urlToAccount = '/admin/account/'.$accountId;
        $crawler = $client->request('GET', $urlToAccount);
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Site Account ID  -  '.$accountId,
            $crawler
                ->filter('.app-detailed__info__data-id')
                ->filter('span')
                ->text()
        );

        //select unrelate Company link and click it
        $unRelateLink = $crawler
            ->filter('a.app-link__delete')
            ->eq(0)
            ->link()
        ;
        $client->click($unRelateLink);

        //select confirm Company unrelating link and click it
        $confirmUnRelateLink = $crawler
            ->filter('div#deleteCompanyModal a.app-detailed__button.app-detailed__button--red.btn')
            ->eq(0)
            ->link();
        $client->click($confirmUnRelateLink);

        //go to testing Account page and check, that "Select" link is present (Company is not selected)
        $crawler = $client->request('GET', $urlToAccount);
        $link = $crawler->selectLink('Select')->link();
        $this->assertEquals('Select', $link->getNode()->textContent, 'No button named "Select"');
    }
}
