<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class SearchContractorTest extends FunctionalTestCase
{
    public function testSearchContractor()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Contractor page
        $crawler = $this->goToSearchContractorPage($client);

        //select search form, fill search field and submit form
        $searchForm = $crawler->selectButton('SEARCH')->form();
        $searchForm['searchPhrase'] = 'Contractor';
        $crawler = $client->submit($searchForm);

        //check, that, at least, one Contractor has been found
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );
    }

    public function testSearchNoResults()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Contractor page
        $crawler = $this->goToSearchContractorPage($client);

        //select search form, fill search field by phrase that must not give any result and submit form
        $searchForm = $crawler->selectButton('SEARCH')->form();
        $searchForm['searchPhrase'] = 'test44'; //this search phrase must not give any search result
        $crawler = $client->submit($searchForm);

        //check, that, any Contractor has not been found
        $this->assertContains(
            'No results',
            $crawler->filter('.app-data__not-found')->text()
        );
    }

    /**
     * @param Client $client
     * @return Crawler
     */
    private function goToSearchContractorPage(Client $client) : Crawler
    {
        //go to contractor list page (there are search form on that page)
        $crawler = $client->request('GET', '/admin/contractor/list');

        //check, that we are on requested page
        $this->assertContains(
            'Contractors',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //return Crawler, which crawls by contractors list page
        return $crawler;
    }
}
