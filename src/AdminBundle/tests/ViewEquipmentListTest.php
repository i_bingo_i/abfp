<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewEquipmentListTest extends FunctionalTestCase
{
    public function testViewEquipmentList()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Equipment list page
        $crawler = $client->request('GET', '/admin/equipment/list');

        //check, that requested page has been shown correctly
        $this->assertEquals(
            'Backflow Gauges',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //check, that equipment count greater, than 0
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );
    }
}
