<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Form;
use AdminBundle\tests\TestTrait\SearchClientsTrait;

class SearchAccountTest extends FunctionalTestCase
{
    use SearchClientsTrait;

    public function testSearchAccount()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        //select search form
        $form = $this->getSearchForm($crawler, 'test');

        //submit search form
        $crawler = $client->submit($form);

        //check, that search has given some results
        $this->assertGreaterThan(
            0,
            $crawler->filter('.tab-content #panel1 tr.app-data__table-row--close')->count()
        );
    }

    public function testSearchActiveAccount()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        /**
         * @var Form $form
         * Select search form, then full and submit it
         */
        $form = $this->getSearchForm($crawler, 'test');
        $form = $this->unTickCheckbox($form, 'clientType[1]');
        $form = $this->unTickCheckbox($form, 'clientType[2]');
        $form = $this->unTickCheckbox($form, 'clientType[3]');
        $crawler = $client->submit($form);

        //check, that search has given some results
        $this->assertGreaterThan(
            0,
            $crawler->filter('.tab-content #panel1 tr.app-data__table-row--close')->count()
        );
    }

    public function testSearchInactiveAccount()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        //Select search form, then full and submit it
        $form = $this->getSearchForm($crawler, 'test');
        $form = $this->unTickCheckbox($form, 'clientType[0]');
        $form = $this->unTickCheckbox($form, 'clientType[2]');
        $form = $this->unTickCheckbox($form, 'clientType[3]');
        $crawler = $client->submit($form);

        //check count of search results
        $this->assertCountSearchResult($crawler, '#panel1', 6);
    }

    public function testSearchSpecialDiscountAccount()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        //Select search form, then full and submit it
        $form = $this->getSearchForm($crawler, 'test');
        $form = $this->unTickCheckbox($form, 'clientType[0]');
        $form = $this->unTickCheckbox($form, 'clientType[1]');
        $form = $this->unTickCheckbox($form, 'clientType[3]');
        $crawler = $client->submit($form);

        //check count of search results
        $this->assertCountSearchResult($crawler, '#panel1', 1);
    }

    public function testSearchPotentialAccount()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        //Select search form, then full and submit it
        $form = $this->getSearchForm($crawler, 'test');
        $form = $this->unTickCheckbox($form, 'clientType[0]');
        $form = $this->unTickCheckbox($form, 'clientType[1]');
        $form = $this->unTickCheckbox($form, 'clientType[2]');
        $crawler = $client->submit($form);

        //check, that search has not given any result
        $this->assertCountSearchResult($crawler, '#panel1', 1);
    }

    public function testSearchNoResult()
    {
        /**
         * @var Client $client
         * Log in
         */
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        /**
         * @var Crawler $crawler
         * Go to search page
         */
        $crawler = $this->goToSearchPage($client);

        //Select search form, then full and submit it
        $form = $this->getSearchForm($crawler, 'test');
        $form = $this->unTickCheckbox($form, 'clientType[0]');
        $form = $this->unTickCheckbox($form, 'clientType[1]');
        $form = $this->unTickCheckbox($form, 'clientType[2]');
        $form = $this->unTickCheckbox($form, 'clientType[3]');
        $crawler = $client->submit($form);

        //check, that search has not given any result
        $this->assertEquals(
            'No results',
            $crawler->filter('.tab-content div#panel1 div.app-data__not-found')->text()
        );
    }
}
