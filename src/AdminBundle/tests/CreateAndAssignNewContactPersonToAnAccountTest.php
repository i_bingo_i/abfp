<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class CreateAndAssignNewContactPersonToAnAccountTest extends FunctionalTestCase
{
    public function testCreateAndAssignNewContactPersonToAnAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search and select ContactPerson page
        $accountId = 1;
        $crawler = $this->goToSearchAndSelectContactPersonPage($client, $accountId, $prevContsQ);

        //go to create new ContactPerson page
        $createNewContactPersonLink = $crawler->selectLink('Create New Contact Person')->link();
        $crawler = $client->click($createNewContactPersonLink);

        //check, that we are on the requested page
        $this->assertContains(
            'Create New Contact Person to Link it to',
            $crawler->filter('.app-main__page-title-name.app-link__header--contact-form')->text()
        );

        //test cancel button on create new contact person form
        $this->intermediateTestCreateNewContactPersonCancelButton($client, $accountId, $prevContsQ);

        //create new contact person
        $createNewContactPersonForm = $crawler->selectButton('CREATE')->form();
        $createNewContactPersonForm['contact_person[firstName]'] = 'Test First';
        $client->submit($createNewContactPersonForm);
        $crawler = $client->followRedirect();
        //get contact person id from url
        $uri = $crawler->getUri();
        $uriParts = explode('/', $uri);
        //contact person id is needed for intermediateTestContactPersonResponsibilityCancelButton()
        $contactPersonId = end($uriParts);
        $this->assertContains(
            'Set Contact Person Role and Responsibility',
            $crawler->filter('.app-form-header')->text()
        );

        //test cancel button on set contact person role and responsibility form
        $this->intermediateTestContactPersonResponsibilityCancelButton(
            $client,
            $accountId,
            $contactPersonId,
            $prevContsQ
        );

        //submit contact person role and responsibility form
        $cpRoleAndResponsibilityForm = $crawler->selectButton('SAVE')->form();
        $client->submit($cpRoleAndResponsibilityForm);
        $crawler = $client->followRedirect();

        $accountContactsPageTitleBlock = $crawler->filter('.app-main__page-title-name');
        //check, that we are redirected to expected page
        $this->assertContains(
            'Contacts',
            $accountContactsPageTitleBlock->text()
        );

        //select Account link and click it
        $accountLink = $accountContactsPageTitleBlock->filter('a')->link();
        $crawler = $client->click($accountLink);

        //check, that we are on requested page
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that quantity of linked contact people was increased by 1 (ContactPerson was added)
        $viewAndManageAllContactsLinkText = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->getNode(0)->textContent;
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $contsQ = $matches[1];
        $this->assertEquals($contsQ, $prevContsQ + 1);
    }

    public function testSelectAndSearchContactPersonCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search and select ContactPerson page
        $accountId = 1;
        $crawler = $this->goToSearchAndSelectContactPersonPage($client, $accountId, $prevContsQ);

        //click CANCEL link
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that we are redirected to requested page
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that quantity of linked contact people was not increased by 1 (ContactPerson was not added)
        $viewAndManageAllContactsLinkText = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->getNode(0)->textContent;
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $contsQ = $matches[1];
        $this->assertEquals($contsQ, $prevContsQ);
    }

    private function intermediateTestCreateNewContactPersonCancelButton(Client $client, int $accountId, int $prevContsQ)
    {
        //go to create ContactPerson and linked to Account page
        $crawler = $client->request('GET', '/admin/contact/create-and-linked-to-account/'.$accountId);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that we are on requested page
        $searchContactPersonPageTitleBlock = $crawler->filter('.app-main__page-title');
        $this->assertContains(
            'Link Contact person to',
            $searchContactPersonPageTitleBlock->filter('.app-main__page-title-name')->text()
        );

        //check, that we are editing contact people of the account we selected from the beginning of this test
        $accountLink = $searchContactPersonPageTitleBlock->filter('.app-link__title--contact a')->link();
        $crawler = $client->click($accountLink);
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that quantity of Account contacts was not changed
        $viewAndManageAllContactsLinkText = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->getNode(0)->textContent;
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $contsQ = $matches[1];
        $this->assertEquals($contsQ, $prevContsQ);
    }

    private function intermediateTestContactPersonResponsibilityCancelButton(
        Client $client,
        int $accountId,
        int $contactPersonId,
        int $prevContsQ
    ) {
        //go to account-contact-person-set-roles page
        $crawler = $client->request('GET', '/admin/account-contact-person-set-roles/'.$accountId.'/'.$contactPersonId);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that we are on requested page
        $accountContactsPageTitleBlock = $crawler->filter('.app-main__page-title-name');
        $this->assertContains(
            'Contacts',
            $accountContactsPageTitleBlock->text()
        );

        //check, that we are editing contact people of the account we selected from the beginning of this test
        $accountLink = $accountContactsPageTitleBlock->filter('a')->link();
        $crawler = $client->click($accountLink);
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that quantity of Account Contacts was not changed
        $viewAndManageAllContactsLinkText = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->getNode(0)->textContent;
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $contsQ = $matches[1];
        $this->assertEquals($contsQ, $prevContsQ);
    }

    private function goToSearchAndSelectContactPersonPage(
        Client $client,
        int $accountId,
        ?int &$prevContsQ = null
    ) : Crawler {

        //go to Account page
        $crawler = $client->request('GET', '/admin/account/'.$accountId);

        //check, that we are on requested page
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //going to page of all account contacts
        $viewAndManageAllContactsLink = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button')->link();
        $viewAndManageAllContactsLinkText = $viewAndManageAllContactsLink->getNode()->textContent;
        //assign previous contacts quantity to $prevContsQ variable to test whether contacts were added
        preg_match('/\((\d+)\)/', $viewAndManageAllContactsLinkText, $matches);
        $prevContsQ = $matches[1];

        //go to page "View and Manage All Contacts" and check, that we are on it
        $crawler = $client->click($viewAndManageAllContactsLink);
        $this->assertContains(
            'Contacts',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //go to adding contact person to account with $accountId page
        $addContactPersonLink = $crawler->selectLink('Add Contact Person')->link();
        $crawler = $client->click($addContactPersonLink);
        $this->assertContains(
            'Search and select Contact person',
            $crawler->filter('.app-form-header')->text()
        );

        //return Crawler, which Crawls by search/select ContactPerson page
        return $crawler;
    }
}
