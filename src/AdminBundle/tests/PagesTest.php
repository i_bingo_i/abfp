<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class PagesTest extends FunctionalTestCase
{
    /**
     * @dataProvider urlProvider
     * @param $url
     */
    public function testPageIsSuccessful($url)
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to page, which url is url
        $client->request('GET', $url);

        //check, that page which url is url has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        //return array of checking urls
        return [
            ['/admin/'],
            ['/admin/client/search'],
            ['/admin/account/create'],
            ['/admin/account/1'],
            ['/admin/account/update/3'],
            ['/admin/contact/create'],
            ['/admin/contact/update/2'],
            ['/admin/contact/1'],
            ['/admin/company/create'],
            ['/admin/company/update/2'],
            ['/admin/company/1'],
            ['/admin/account/search-company-for-select/3'],
            ['/admin/company/create-and-linked-to-account/4'],
            ['/admin/contact/search-company-for-select/5'],
            ['/admin/company/create-and-linked-to-contact-person/5'],
            ['/admin/contractor/list'],
            ['/admin/contractor/view/3'],
            ['/admin/municipality/list'],
            ['/admin/municipality/view/1'],
            ['/admin/contractor-user/view/4'],
            ['/admin/agent/list'],
            ['/admin/agent/view/1'],
            ['/admin/equipment/list'],
            ['/admin/opportunity/list'],
        ];
    }
}
