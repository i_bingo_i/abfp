<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class AutomaticContractorServiceAddingAfterServiceForDeviceCreationTest extends FunctionalTestCase
{
    public function testAutomaticContractorServiceAddingAfterServiceForDeviceCreation()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to view device page
        $deviceId = 1;
        $crawler = $client->request('GET', '/admin/device/view/'.$deviceId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Backflow Device ID - '.$deviceId,
            $crawler->filter('.app-detailed__info__data-id.app-device__info__data-id')->text()
        );

        //go to add service to testing device page
        $addServiceLink = $crawler->selectLink('Add Service')->link();
        $crawler = $client->click($addServiceLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Service Information',
            $crawler->filter('.app-form-header')->text()
        );

        //select create service form, full required fields and "Company Last Tested" field, submit form
        $addServiceForm = $crawler->selectButton('CREATE')->form();
        $addServiceForm['admin_bundle_service[named]'] = 1;
        $addServiceForm['admin_bundle_service[inspectionDue]'] = date('m/d/Y');
        $addServiceForm['admin_bundle_service[companyLastTested]'] = 2;
        $client->submit($addServiceForm);
        $crawler = $client->followRedirect();

        //select link to watch services list and click it
        $servicesListLink = $crawler->filter('.app-account-site__container table tbody tr td .app-wrapper div')
            ->eq(1)->filter('a')->link();
        $crawler = $client->click($servicesListLink);

        //get last added service row to get its name and Company last tested link then
        $addedServiceRow = $crawler->filter('.app-account-site__container table tbody tr')->last();

        //get added service name to check it is right on Contractor page
        $addedServiceName = $addedServiceRow->filter('td')->eq(5)
            ->filter('.app-line--right-text ')->text();
        $addedServiceCompanyLastTestedLink = $addedServiceRow->filter('td')->eq(12)
            ->filter('a')->link();

        //get name of Company last tested to check, which Contractor we will be redirected to
        $addedServiceCompanyLastTestedName = $addedServiceCompanyLastTestedLink->getNode()->textContent;

        //go to Contractor page
        $crawler = $client->click($addedServiceCompanyLastTestedLink);

        //check, that we are on page of Contractor we added as Company last tested (not other)
        $this->assertContains(
            $addedServiceCompanyLastTestedName,
            $crawler->filter('.app-detailed__info__data-title.app-contractor-information_data-title')->text()
        );

        //go to Services tab on Contractor page
        $contractorServicesTabLink = $crawler->filter('.app-contractors-nav #tab1 a')->link();
        $crawler = $client->click($contractorServicesTabLink);

        //check, that Contractor has Service we added in its services list
        $this->assertContains(
            $addedServiceName,
            $crawler->filter('#panel1 table tbody')->html()
        );
    }
}
