<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class AutoCreatingRetestOpportunitiesTest extends FunctionalTestCase
{
    public function testAutoCreatingRetestOpportunities()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $accountId = 7;
        $accountHref = '/admin/account/'.$accountId;
        $crawler = $client->request('GET', $accountHref);

        //check, that Account page has been shown correctly
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id')->text()
        );

        //make request for adding a new device
        $crawler = $client->request('GET', '/admin/device/create/'.$accountId.'/1');

        //select form to add new device and submit it
        $deviceForm = $crawler->filter('#deviceForm')->form();
        $client->submit($deviceForm);
        $crawler = $client->followRedirect();

        //select and click to added device for adding new service to it further
        $devicesTableRowObject = $crawler->filter('.app-devices-table__container table tbody tr');
        $devicesTableRowsLink = $devicesTableRowObject->eq(2)->filter('td div')->filter('div')
            ->eq(2)->filter('a')->link();
        $client->click($devicesTableRowsLink);
        $deviceLink = $devicesTableRowObject->eq(4)->filter('td')->eq(1)
            ->filter('.app-wrapper div a')->link();

        //get device link text for checking, that we are on the page of correct device further
        $deviceLinkText = $deviceLink->getNode()->textContent;
        $crawler = $client->click($deviceLink);

        //check, that we are on the correct device page
        $this->assertContains(
            $deviceLinkText,
            $crawler->filter('.app-device__info__data-title')->text()
        );

        //go to add service to added device page
        $addServiceLink = $crawler->selectLink('Add Service')->link();
        $crawler = $client->click($addServiceLink);

        //select form for adding service to device, complete required fields and submit it
        $serviceForm = $crawler->filter('#serviceForm')->form();
        $serviceForm['admin_bundle_service[named]'] = 2;
        $serviceForm['admin_bundle_service[inspectionDue]'] = date('m/d/Y');
        $serviceForm['admin_bundle_service[fixedPrice]'] = '$160';
        $client->submit($serviceForm);
        $crawler = $client->followRedirect();

        //check, that we are redirected to the correct page
        $this->assertContains(
            $deviceLinkText,
            $crawler->filter('.app-device__info__data-title')->text()
        );

        //select Account link
        $accountLink = $crawler->filter('a.app-link__select')->link();

        //get href attribute of Account link to check if it is Account we have come from
        $accountLinkHref = $accountLink->getNode()->getAttribute('href');
        $this->assertEquals(
            $accountHref,
            $accountLinkHref
        );

        //click Account link
        $crawler = $client->click($accountLink);

        //select "Opportunities" tab
        $opportunitiesTabLink = $crawler->filter('.app-account-tabs__list a')->eq(1)->link();
        $crawler = $client->click($opportunitiesTabLink);

        //check opportunities table for last added opportunity with Retest type
        $lastOpportunitiesTableRow = $crawler->filter('.app-opportunities__table tbody tr')->last();
        $opportunityTypeColumnText = $lastOpportunitiesTableRow->filter('td')->eq(1)
            ->filter('span')->text();

        $this->assertEquals(
            'Retest',
            $opportunityTypeColumnText
        );
    }
}
