<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class WarningNoWayToDetermineUploadFeesTest extends FunctionalTestCase
{
    public function testWarningNoWayToDetermineUploadFees()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $accountId = 9;
        $crawler = $client->request('GET', '/admin/account/'.$accountId);

        //check, that Account page has been shown correctly
        $this->assertContains(
            'Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //make request to add new Device to Account
        $crawler = $client->request('GET', '/admin/device/create/'.$accountId.'/5');

        //select form for Device creation and submit it
        $addNewDeviceForm = $crawler->selectButton('CREATE')->form();
        $client->submit($addNewDeviceForm);
        $crawler = $client->followRedirect();

        //check, that page we had been redirected has been shown correctly
        $this->assertContains(
            'Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that testing warning is present
        $this->assertContains(
            'Information about uplaod fees is missing for some Divisions under this Account',
            $crawler->filter('.tooltip-warning-container img')->attr('title')
        );

        //select link for Account editing to change Municipality
        $editAccountLink = $crawler->selectLink('Edit')->link();

        //click link for Account editing
        $crawler = $client->click($editAccountLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'General Account Information',
            $crawler->filter('.app-form-header')->text()
        );

        //select form for Account editing, change Municipality and submit form
        $updateAccountForm = $crawler->selectButton('UPDATE')->form();
        $updateAccountForm['account[municipality]'] = '2';
        $client->submit($updateAccountForm);
        $crawler = $client->followRedirect();

        //check, that page we had been redirected has been shown correctly
        $this->assertContains(
            'Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //check, that warning is absent
        $this->assertNotContains(
            'Information about uplaod fees is missing for some Divisions under this Account',
            $crawler->filter('.app-detailed__info__data-title')->html()
        );
    }
}
