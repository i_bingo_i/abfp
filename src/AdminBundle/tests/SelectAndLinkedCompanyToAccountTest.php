<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class SelectAndLinkedCompanyToAccountTest extends FunctionalTestCase
{
    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Company for select page
        $urlSearchCompanyForSelect = '/admin/account/search-company-for-select/3';
        $crawler = $client->request('GET', $urlSearchCompanyForSelect);

        //check, that CANCEL link is present, select CANCEL link and click it
        $backLink = $crawler->selectLink('CANCEL')->link();
        $this->assertEquals('CANCEL', $backLink->getNode()->textContent, 'No button named "CANCEL"');
        $crawler = $client->click($backLink);

        //check, that we are on expected page
        $this->assertEquals(
            'Site Account ID  -  3',
            $crawler
                ->filter('.app-detailed__info__data-id')
                ->filter('span')
                ->text()
        );
    }

    public function testGoToSearchCompanyByLinkPage()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $urlToAccount = '/admin/account/3';
        $crawler = $client->request('GET', $urlToAccount);

        //check, that requested page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Site Account ID  -  3',
            $crawler
                ->filter('.app-detailed__info__data-id')
                ->filter('span')
                ->text()
        );

        //check, that link for Company selecting is present, select link and click it
        $link = $crawler->selectLink('Select')->link();
        $this->assertEquals('Select', $link->getNode()->textContent, 'No button named "Select"');
        $crawler = $client->click($link);

        //check, that we are on expected page
        $this->assertEquals(
            "Search and select Company",
            $crawler
                ->filter('.app-link__container.app-form--bordered .app-form-header')
                ->eq(0)
                ->text()
        );
    }

    public function testSearchAndLinkedCompanyToAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Company for select page
        $urlSearchCompanyForSelect = '/admin/account/search-company-for-select/3';
        $crawler = $client->request('GET', $urlSearchCompanyForSelect);

        //select search form, full and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = "company";
        $form['cn_s_in_notes'] = 1;
        $crawler = $client->submit($form);

        //check that number of found rows is expected
        $this->assertGreaterThan(
            0,
            $crawler->filter(".table.app-data__table.app-bordered tbody tr")->count()
        );

        //check, that link for Company selecting is present
        $link = $crawler->selectLink('Select')->link();
        $this->assertEquals('Select', $link->getNode()->textContent, 'No button named "Select"');

        //go to account/company-select page
        $urlSelectCompany = '/admin/account/company-select/3/5';
        $client->request('GET', $urlSelectCompany);

        //check, that we were redirected to expected page and go to it
        $urlToAccount = '/admin/account/3';
        $this->assertTrue(
            $client->getResponse()->isRedirect($urlToAccount),
            'Response is a redirect to account page'
        );
        $crawler = $client->request('GET', $urlToAccount);

        //check, that Company was linked to Account
        $this->assertEquals(
            'McDonalds',
            $crawler
                ->filter('.app-contact-main__line-text.app-color--white.app-cut-string a.app-link__select')
                ->eq(0)
                ->text()
        );
    }
}
