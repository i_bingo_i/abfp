<?php
namespace AdminBundle\tests;

use AppBundle\Tests\BehaviorTestCase;

class DeleteAccountTest extends BehaviorTestCase
{
    public function testDeleteAccount()
    {
        $this->logIn('schur.maksim@gmail.com');
        $this->goToPage('/admin/account/14');
        $this->see('Acc');

        $this->seeLinkButton('Delete');
        $this->clickLinkButton('Delete');
        $this->seeConfirmPopup('Are you sure you want to delete this Account?');
        $this->clickNoInConfirmPopup();

        $this->seeLinkButton('Delete');
        $this->clickLinkButton('Delete');
        $this->seeConfirmPopup('Are you sure you want to delete this Account?');
        $this->clickYesInConfirmPopup();
        $this->seeLabel('Test');
    }
}
