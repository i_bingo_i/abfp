<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class CreateNewSiteAndRelateItToCurrentGroupAccountTest extends FunctionalTestCase
{
    public function testCreateNewSite()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search and select site account page
        $accountId = 5;
        $crawler = $this->goToSearchAndSelectSiteAccountPage($client, $accountId);

        //go to create new site account page
        $crawler = $this->goToCreateNewSiteAccountPage($client, $crawler);

        //select form for new account adding
        $accountForm = $crawler->selectButton('CREATE')->form();

        //fullize form fields and submit it
        $accountForm['account[name]'] = 'TestAccountName';
        $accountForm['account[address][address]'] = '11/1, Test str.';
        $accountForm['account[address][city]'] = 'New York';
        $accountForm['account[address][state]'] = 32;
        $accountForm['account[address][zip]'] = '02111';
        $accountForm['account[municipality]'] = '1';
        $client->submit($accountForm);

        //check, that new Account was added
        $crawler = $client->request('GET', '/admin/account/'.$accountId);
        $this->assertContains(
            'TestAccountName',
            $crawler->filter('.app-main .app-devices-table__container .app-data__table .app-link__select.app-link__title')->html()
        );
    }

    public function testCreateNewSiteCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search and select site account page
        $accountId = 5;
        $crawler = $this->goToSearchAndSelectSiteAccountPage($client, $accountId);

        //go to create new site account page
        $crawler = $this->goToCreateNewSiteAccountPage($client, $crawler);

        //select CANCEL link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that we are on requested page
        $this->assertContains(
            'Search and select Site Account',
            $crawler->filter('.app-form-header')->text()
        );
    }

    public function testSearchSiteAccountCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search and select site account page
        $accountId = 5;
        $crawler = $this->goToSearchAndSelectSiteAccountPage($client, $accountId);

        //select CANCEL link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that we are on requested page
        $this->assertContains(
            'Group Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );
    }

    private function goToSearchAndSelectSiteAccountPage(Client $client, int $accountId) : Crawler
    {
        //log in
        $crawler = $client->request('GET', '/admin/account/'.$accountId);

        //select add account link and click it
        $addAccountLink = $crawler->selectLink('Add Account')->link();
        $crawler = $client->click($addAccountLink);

        //check, that we are on requested page
        $this->assertContains(
            'Search and select Site Account',
            $crawler->filter('.app-form-header')->text()
        );

        //return Crawler, which Crawls by Search and select Site page
        return $crawler;
    }

    private function goToCreateNewSiteAccountPage(Client $client, Crawler $crawler) : Crawler
    {
        //select create new site account page and click it
        $createNewSiteAccountLink = $crawler->selectLink('Create New Site Account')->link();
        $crawler = $client->click($createNewSiteAccountLink);

        //return Crawler, that crawls by page for creating new Site
        return $crawler;
    }
}
