<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class SearchContactPersonForSelectTest extends FunctionalTestCase
{
    public function testSearchContactPersonForSelect()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search ContactPerson for select page
        $crawler = $this->goToSearchContactPersonForSelectPage($client);

        //select search form, full and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = 'test';
        $crawler = $client->submit($form);

        //check, that search has given some results
        $this->assertContains(
            'Search results: ',
            $crawler->filter('.app-link__result')->eq(0)->text()
        );
    }

    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search ContactPerson for select page
        $crawler = $this->goToSearchContactPersonForSelectPage($client);

        //check, that CANCEL link is present on the page, select CANCEL link and click it
        $backLink = $crawler->selectLink('CANCEL')->link();
        $this->assertEquals('CANCEL', $backLink->getNode()->textContent, 'No button named "CANCEL"');
        $crawler = $client->click($backLink);

        //check, that we are on requested page
        $this->assertEquals(
            'Site Account ID  -  1',
            $crawler->filter('.app-detailed__info__data-id span')->text(),
            'Site account with this ID not found'
        );
    }

    public function testSearchNoResult()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search ContactPerson for select page
        $crawler = $this->goToSearchContactPersonForSelectPage($client);

        //select search form, full and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = 'test44';
        $crawler = $client->submit($form);

        //check, that search has not given any results
        $this->assertEquals(
            'No results',
            $crawler->filter('.app-data__not-found')->eq(0)->text()
        );
    }

    private function goToSearchContactPersonForSelectPage(Client $client) : Crawler
    {
        //go to search ContactPerson for select page
        $crawler = $client->request('GET', '/admin/account/search-contact-person-for-select/1');

        //check, that we are on requested page
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertContains(
            'Link Contact person to',
            $crawler->filter('.app-main__page-title-name')->eq(0)->text(),
            'This is not a page for Link contact person to account search'
        );

        //return Crawler, which crawls by search ContactPerson for select page
        return $crawler;
    }
}
