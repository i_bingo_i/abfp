<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewGeneralContractorInformationTest extends FunctionalTestCase
{
    public function testViewGeneralContractorInformation()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to contractors list page
        $crawler = $client->request('GET', '/admin/contractor/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Contractors',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //select Search form, full and submit it
        $searchForm = $crawler->selectButton('SEARCH')->form();
        $searchForm['searchPhrase'] = 'Contractor';
        $crawler = $client->submit($searchForm);

        //check, that count of search results is greater than 0
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );

        //select testing contractor link, get its href attribute and click it
        $testingContractorRow = $crawler->filter('.app-contractors-table_style table tbody tr')->eq(0);
        $testingContractorLink = $testingContractorRow->filter('td')->eq(2)->filter('a')->link();
        $testingContractorHref = $testingContractorLink->getNode()->getAttribute('href');
        $crawler = $client->click($testingContractorLink);

        //check, that we are on the page, which link was clicked in the table of contractors
        $currentTestingPagePathArray = array_slice(explode('/', $crawler->getUri()), 3);
        $testingContractorHrefArray = array_slice(explode('/', $testingContractorHref), 1);
        $this->assertEquals(
            $testingContractorHrefArray,
            $currentTestingPagePathArray
        );

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Contractor ID',
            $crawler->filter('.app-detailed__info__data-id.app-contractor-information_data-id')->text()
        );
    }

    public function testSearchNoResults()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to contractors list page
        $crawler = $client->request('GET', '/admin/contractor/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Contractors',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //select Search form, full and submit it
        $searchForm = $crawler->selectButton('SEARCH')->form();
        $searchForm['searchPhrase'] = 'test44';
        $crawler = $client->submit($searchForm);

        //check, that any search results not found
        $this->assertContains(
            'No results',
            $crawler->filter('.app-contractors-table_style .app-data__not-found')->text()
        );
    }
}
