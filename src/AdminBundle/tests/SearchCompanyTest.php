<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class SearchCompanyTest extends FunctionalTestCase
{
    public function testSearchCompany()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Company page
        $crawler = $this->goToSearchCompanyPage($client);

        //select search form, full and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = 'a';
        $crawler = $client->submit($form);

        //select company tab
        $companyTab = $crawler->filter('.app-data ul #tab3 a[href="#panel3"]')->link();
        $crawler = $client->click($companyTab);

        //check, that count of records is greater than 0
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-data .tab-content #panel3 table tbody tr')->count()
        );
    }

    public function testSearchNoResult()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search Company page
        $crawler = $this->goToSearchCompanyPage($client);

        //select search form, full and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = 'test44';
        $crawler = $client->submit($form);

        //select company tab
        $companyTab = $crawler->filter('.app-data ul #tab3 a[href="#panel3"]')->link();
        $crawler = $client->click($companyTab);

        //check, that search has not given any result
        $this->assertEquals(
            'No results',
            $crawler->filter('#panel3 .app-data__not-found')->text()
        );
    }

    private function goToSearchCompanyPage(Client $client) : Crawler
    {
        //log in
        $crawler = $client->request('GET', '/admin/client/search');

        //check, that requested page has been shown correctly
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertContains(
            'Clients',
            $crawler->filter('.app-main__page-title-name')->eq(0)->text(),
            'This is not a page for company search'
        );

        //return Crawler, which crawls by search page
        return $crawler;
    }
}
