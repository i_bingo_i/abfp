<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class AddAgentToDepartmentTest extends FunctionalTestCase
{
    public function testAddAgentToDepartment()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to view Municipality page
        $municipalityId = 2;
        $crawler = $client->request('GET', '/admin/municipality/view/'.$municipalityId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'City of Chicago',
            $crawler->filter('.municipalities-title')->text()
        );

        //check, that Agent we are going to add has not been added yet
        $this->assertNotContains(
            'Apple',
            $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1)->html()
        );

        //select "Add Agent" link and click it
        $addAgentLink = $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1)
            ->filter('div .municipalities-devices__title div')->eq(1)->filter('a')->link();

        //get data-departmentid attribute to make request to attach department further
        $departmentId = $addAgentLink->getNode()->getAttribute('data-departmentid');

        //click "Add Agent" link
        $crawler = $client->click($addAgentLink);

        //select "Add agent" form, full field and submit form
        $selectAgentForm = $crawler->filter('#selectAgentForm')->form();
        $selectAgentForm['agentName'] = 1;
        $client->submit($selectAgentForm);

        //request page we must be redirected (temporary solution)
        $client->request('GET', '/admin/municipality/attach/'.$departmentId.'/1');
        $crawler = $client->request('GET', '/admin/municipality/view/'.$municipalityId);
        
        //check, that Agent has been added
        $this->assertContains(
            'Apple',
            $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1)
                ->filter('.municipalities-devices__content')->eq(1)
                ->filter('.municipalities-department-agent-title span a')->text()
        );
    }
}
