<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewGeneralAgentInformationTest extends FunctionalTestCase
{
    public function testViewGeneralAgentInformation()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to agents list page
        $crawler = $client->request('GET', '/admin/agent/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Agents',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //check, that agents list is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );

        //select testing agent row, get link to general Agent information page from it
        $testingAgentRow = $crawler->filter('.app-contractors-table_style table tbody tr')->first();
        $testingAgentDetailsLink = $testingAgentRow->filter('td')->eq(2)->filter('a')->link();
        $testingAgentDetailsLinkText = $testingAgentDetailsLink->getNode()->textContent;

        //go to general Agent information page
        $crawler = $client->click($testingAgentDetailsLink);
        
        //check, that requested page has been shown correctly
        $this->assertEquals(
            $testingAgentDetailsLinkText,
            $crawler->filter('.app-detailed__info__data-title')->text()
        );
    }
}
