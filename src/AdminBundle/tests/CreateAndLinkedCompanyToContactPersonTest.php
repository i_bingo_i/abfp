<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class CreateAndLinkedCompanyToContactPersonTest extends FunctionalTestCase
{
    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to create and linked company to ContactPerson page
        $urlCreateAndLinkedCompany = '/admin/company/create-and-linked-to-contact-person/5';
        $crawler = $client->request('GET', $urlCreateAndLinkedCompany);

        //select "CANCEL" link and click it
        $backLink = $crawler->selectLink('CANCEL')->link();
        $this->assertEquals('CANCEL', $backLink->getNode()->textContent, 'No button named "CANCEL"');
        $crawler = $client->click($backLink);

        //check, that we are on expected page
        $this->assertEquals(
            "Search and select Company",
            $crawler
                ->filter('.app-link__container.app-form--bordered .app-form-header')
                ->eq(0)
                ->text()
        );
    }

    public function testCreateAndLinkedCompanyToContactPerson()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to create and linked company to ContactPerson page
        $urlSearchCompanyForSelect = '/admin/contact/search-company-for-select/5';
        $crawler = $client->request('GET', $urlSearchCompanyForSelect);

        //check, that response from server was successful
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');

        //check, that we are on expected page
        $this->assertEquals(
            "Search and select Company",
            $crawler
                ->filter('.app-link__container.app-form--bordered .app-form-header')
                ->eq(0)
                ->text()
        );

        //select "Create New Company" link and click it
        $createNewCompanyLink = $crawler->selectLink('Create New Company')->link();
        $this->assertEquals(
            'Create New Company',
            $createNewCompanyLink->getNode()->textContent,
            'No button named "Create New Company"'
        );
        $crawler = $client->click($createNewCompanyLink);

        //check, that we are on "Create New Company" form page
        $this->assertEquals(
            "General Company Information",
            $crawler
                ->filter('.app-form.app-form--flex.app-form--bordered .app-form-header')
                ->eq(0)
                ->text()
        );

        //select form and fullize required fields on it
        $form = $crawler->selectButton('CREATE')->form();
        $form['company[name]'] = "Test company";

        //submit form
        $client->submit($form);

        //check, that we have added new Company to ContactPerson
        $urlAccount = '/admin/contact/5';
        $crawler = $client->request('GET', $urlAccount);
//
//        $this->assertEquals(
//            'Test company',
//            $crawler
//                ->filter('.app-contact-main__line-text.app-color--white.app-cut-string a.app-link__select')
//                ->eq(0)
//                ->text()
//        );
    }
}
