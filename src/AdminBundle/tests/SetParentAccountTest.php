<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class SetParentAccountTest extends FunctionalTestCase
{
    public function testSetParentAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to search-account-for-select page
        $linkingAccountID = 5;
        $crawler = $client->request('GET', '/admin/account/search-account-for-select/'.$linkingAccountID);

        //check, that we are on requested page
        $this->assertEquals(
            'Search and select Site Account',
            $crawler->filter('.app-form-header')->text()
        );

        //select search form, full and submit it
        $form = $crawler->selectButton('SEARCH')->form();
        $form['searchPhrase'] = 'a';
        $crawler = $client->submit($form);

        //check, that search has given some results
        $this->assertContains(
            'Search results: ',
            $crawler->filter('.app-data .app-link__result')->text()
        );

        //check, "Select" link is present in search results table, select it
        $this->assertContains(
            'Select',
            $crawler->filter('.app-data__table tbody tr td a.app-link__table-button')->eq(0)->text(),
            'Unable to find "Select" link. Possibly, account was deleted.'
        );

        //select table row with testing account we are going to select
        $siteAccountRow = $crawler->filter('.app-data__table tbody tr')->eq(2);
        $selectLink = $siteAccountRow->filter('td a.app-link__table-button')->eq(0)->link();

        //get Site account ID and Site account name
        $siteAccountID = $siteAccountRow->filter('td')->eq(1)->text();
        $siteAccountName = $crawler->filter('td')->eq(2)->text();

        //click "Select" link
        $client->click($selectLink);

        //go to the page from confirm dialog link
        $client->request('GET', '/admin/account/parent-select/'.$siteAccountID.'/'.$linkingAccountID);
        $crawler = $client->followRedirect();

        //check, that site account has been linked to the group account
        $this->assertContains(
            trim($siteAccountName),
            trim($crawler->filter('.app-main .app-devices-table__container table tbody')->html())
        );
    }
}
