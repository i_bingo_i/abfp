<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class EditServiceForDeviceTest extends FunctionalTestCase
{
    public function testEditServiceForDevice()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to view Service page
        $deviceId = 1;
        $crawler = $this->goToEditServicePage($client, $deviceId, $prevServiceComment);

        //on edit Service page
        //select edit Service form, fill some fields and submit it
        $editServiceForm = $crawler->selectButton('UPDATE')->form();
        $editServiceForm['admin_bundle_service[comment]'] =
            ($editServiceForm['admin_bundle_service[comment]']->getValue() == 'test comment') ?
                'test comment edited' :
                'test comment';
        $client->submit($editServiceForm);
        $crawler = $client->followRedirect();

        //check, that page we were redirected has been shown correctly
        $this->assertContains(
            'Backflow Device ID - '.$deviceId,
            $crawler->filter('.app-detailed__info__data-id.app-device__info__data-id')->text()
        );

        //select link to watch services list and click it
        $servicesListLink = $crawler->filter('.app-account-site__container table tbody tr td .app-wrapper div')
            ->eq(1)->filter('a')->link();
        $crawler = $client->click($servicesListLink);

        //get service name after editing
        $serviceComment = $crawler->filter('.app-account-site__container table tbody tr')->eq(3)
            ->filter('td')->eq(6)->text();

        //check, that Service name has been changed after editing
        $this->assertNotEquals(
            $serviceComment,
            $prevServiceComment
        );
    }

    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to edit Service page
        $deviceId = 1;
        $crawler = $this->goToEditServicePage($client, $deviceId, $prevServiceComment);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that requested page has been shown successfully
        $this->assertContains(
            'Backflow Device ID - '.$deviceId,
            $crawler->filter('.app-detailed__info__data-id.app-device__info__data-id')->text()
        );

        //get service name after "CANCEL" link clicking
        $serviceComment = $crawler->filter('.app-account-site__container table tbody tr')->eq(3)
            ->filter('td')->eq(6)->text();

        //check, that value of field has not been changed after "CANCEL" link clicking
        $this->assertEquals(
            $serviceComment,
            $prevServiceComment
        );
    }

    private function goToEditServicePage(Client $client, int $deviceId, ?string &$prevServiceComment) : Crawler
    {
        //go to view Device page
        $crawler = $client->request('GET', '/admin/device/view/'.$deviceId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Backflow Device ID - '.$deviceId,
            $crawler->filter('.app-detailed__info__data-id.app-device__info__data-id')->text()
        );

        //search for device services
        $servicesLinkWrapper = $crawler->filter('.app-account-site__container table tbody tr td .app-wrapper div')
            ->eq(1);
        $servicesCount = intval($servicesLinkWrapper->filter('a .app-font--inherit')->text());
        $this->assertGreaterThan(0, $servicesCount);

        //select link to watch services list and click it
        $servicesListLink = $servicesLinkWrapper->filter('a')->link();
        $crawler = $client->click($servicesListLink);

        //select link for service editing and click it
        $editingServiceRow = $crawler->filter('.app-account-site__container table tbody tr')->eq(3);
        $prevServiceComment = $editingServiceRow->filter('td')->eq(6)->text();
        $editServiceLink = $editingServiceRow->filter('td')->eq(14)->filter('a')->link();
        $crawler = $client->click($editServiceLink);

        //return Crawler, which crawls by edit Device page
        return $crawler;
    }
}
