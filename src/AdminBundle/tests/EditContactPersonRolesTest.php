<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class EditContactPersonRolesTest extends FunctionalTestCase
{
    public function testEditContactPersonRoles()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to change ContactPerson roles page
        $accountId = 1;
        $crawler = $this->goToChangeContactPersonRolesPage($client, $accountId);

        //select ContactPerson roles and responsibility form
        $cpRoleAndResponsibilityForm = $crawler->selectButton('SAVE')->form();

        //change form state
        $cpRoleAndResponsibilityForm['account_contact_person[authorizer]']->tick();
        foreach ($cpRoleAndResponsibilityForm['account_contact_person[responsibilities]'] as $ACPResponsibility) {
            if ($ACPResponsibility->getValue()) {
                $ACPResponsibility->untick();
            } else {
                $ACPResponsibility->tick();
            }
        }

        $newAuthorizerBackflowValue = $cpRoleAndResponsibilityForm['account_contact_person[responsibilities][0]']
            ->getValue();
        $newAuthorizerFireValue = $cpRoleAndResponsibilityForm['account_contact_person[responsibilities][1]']
            ->getValue();
        if ($cpRoleAndResponsibilityForm['account_contact_person[payment]']->getValue()) {
            $cpRoleAndResponsibilityForm['account_contact_person[payment]']->untick();
        } else {
            $cpRoleAndResponsibilityForm['account_contact_person[payment]']->tick();
        }
        $newPaymentsValue = $cpRoleAndResponsibilityForm['account_contact_person[payment]']->getValue();
        $client->submit($cpRoleAndResponsibilityForm);
        $crawler = $client->followRedirect();

        //go to the same page as we`ve tested
        $changeRolesLink = $crawler->filter('.app-contact-list__container--main')
            ->eq(0)->filter('div div table tbody tr')->first()->filter('td')->last()
            ->filter('div')->eq(0)->filter('a.app-detailed__button--blue')->link();
        $crawler = $client->click($changeRolesLink);
        $cpRoleAndResponsibilityForm = $crawler->selectButton('SAVE')->form();
        
        //check the form state
        $this->assertEquals(
            $newAuthorizerBackflowValue,
            $cpRoleAndResponsibilityForm['account_contact_person[responsibilities][0]']->getValue()
        );
        $this->assertEquals(
            $newAuthorizerFireValue,
            $cpRoleAndResponsibilityForm['account_contact_person[responsibilities][1]']->getValue()
        );
        $this->assertEquals(
            $newPaymentsValue,
            $cpRoleAndResponsibilityForm['account_contact_person[payment]']->getValue()
        );
    }

    private function goToChangeContactPersonRolesPage(Client $client, int $accountId) : Crawler
    {
        //log in
        $crawler = $client->request('GET', '/admin/account-contact-person/manage/'.$accountId);

        //check, that we are on expected page
        $this->assertContains(
            'Contacts',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //link ContactPerson to an account
        $crawler = $this->linkContactPersonToAnAccount($client, $crawler);

        //assign href to account page from contact person manage page
        $testingAccountContactPersonRow = $crawler->filter('.app-contact-list__container--main')
            ->eq(0)->filter('div div table tbody tr')->first();
        $testingAccountContactPersonHref = $testingAccountContactPersonRow->filter('td')->first()
            ->filter('div span a')->attr('href');

        //check, that we are changing roles of the testing ContactPerson (not other) and go to change roles page
        $changeRolesLink = $testingAccountContactPersonRow->filter('td')->last()
            ->filter('div')->eq(0)->filter('a.app-detailed__button--blue')->link();
        $crawler = $client->click($changeRolesLink);
        $contactPersonRespPageTitleBlock = $crawler->filter('.app-main__page-title-name');
        $this->assertContains(
            'Link',
            $contactPersonRespPageTitleBlock->text()
        );
        $contactPersonRespPageTitleCPHref = $contactPersonRespPageTitleBlock
            ->filter('a')->eq(0)->attr('href');
        $this->assertEquals($testingAccountContactPersonHref, $contactPersonRespPageTitleCPHref);

        //return Crawler, which crawls by Change Contactperson roles page
        return $crawler;
    }

    private function linkContactPersonToAnAccount(Client $client, Crawler $crawler) : Crawler
    {
        //Select "Add Contact Person" link and click it
        $addContactPersonLink = $crawler->selectLink('Add Contact Person')->link();
        $crawler = $client->click($addContactPersonLink);

        //check, that we are on expected page
        $this->assertContains(
            'Search and select Contact person',
            $crawler->filter('.app-form-header')->text()
        );

        //search for ContactPerson and expect, that we`ve got some results
        $cpSearchForm = $crawler->selectButton('SEARCH')->form();
        $cpSearchForm['searchPhrase'] = 'Tyrion Lannister';
        $crawler = $client->submit($cpSearchForm);
        $this->assertContains(
            'Search results',
            $crawler->filter('.app-link__result')->text()
        );

        //select link for ContactPerson linking and click it
        $selectCPLink = $crawler->filter('.table.app-data__table tbody tr')->last()
            ->filter('td')->last()->filter('a')->link();
        $crawler = $client->click($selectCPLink);

        //check, that we are on expected page
        $this->assertContains(
            'Set Contact Person Role and Responsibility',
            $crawler->filter('.app-form-header')->text()
        );

        //select form for ContactPerson changing and bring it to initial state
        $cpChangeRolesForm = $crawler->selectButton('SAVE')->form();
        foreach ($cpChangeRolesForm['account_contact_person[responsibilities]'] as $accContactPersonResponsibility) {
            $accContactPersonResponsibility->untick();
        }
        $cpChangeRolesForm['account_contact_person[authorizer]']->untick();
        $cpChangeRolesForm['account_contact_person[payment]']->untick();
        $cpChangeRolesForm['account_contact_person[paymentPrimary]']->untick();
        $client->submit($cpChangeRolesForm);
        $crawler = $client->followRedirect();

        //check if contact person has been added
        $pacStr = $crawler->filter('.app-contact-list__container--title')->eq(0)->text();
        preg_match('/\((\d+)\)/', $pacStr, $matches);
        $pacQuantity = intval($matches[1]);
        $this->assertGreaterThan(0, $pacQuantity);

        //return Crawler, which crawls by change ContactPerson roles page
        return $crawler;
    }
}
