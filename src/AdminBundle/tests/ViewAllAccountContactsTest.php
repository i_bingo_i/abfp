<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewAllAccountContactsTest extends FunctionalTestCase
{
    public function testViewAllAccountContacts()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $accountId = 1;
        $testingAccountHref = '/admin/account/'.$accountId;
        $crawler = $client->request('GET', $testingAccountHref);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Site Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //select "View and Manage All Contacts" link and click it
        $viewAllAccountContactsLink = $crawler->filter('.app-detailed__button-container')->eq(1)
            ->filter('a.app-detailed__button--blue')->link();
        $crawler = $client->click($viewAllAccountContactsLink);

        //check, that we are on requested page
        $contactsPageTitleBlock = $crawler->filter('.app-main__page-title-name');
        $this->assertContains(
            'Contacts',
            $contactsPageTitleBlock->text()
        );

        //check, that we are viewing Contacts of Account we are testing (not other)
        $contactsPageAccountHref = $contactsPageTitleBlock->filter('a')->attr('href');
        $this->assertEquals($contactsPageAccountHref, $testingAccountHref);

        //check, that we are on "View and Manage All Contacts" page
        $this->assertContains(
            'Primary Account Contacts',
            $crawler->filter('.app-contact-list__container--main')
                ->eq(0)->filter('.app-contact-list__container--title')->text()
        );
        $this->assertContains(
            'Other Account Contacts',
            $crawler->filter('.app-contact-list__container--main')
                ->eq(1)->filter('.app-contact-list__container--title')->text()
        );
    }
}
