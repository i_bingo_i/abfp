<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class SearchMunicipalityTest extends FunctionalTestCase
{
    public function testSearchMunicipality()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to municipalities list page
        $crawler = $client->request('GET', '/admin/municipality/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipalities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //check, that municipalities list is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );

        //select search form, full and submit it
        $searchForm = $crawler->filter('#searchForm')->form();
        $searchForm['searchPhrase'] = 'City';
        $crawler = $client->submit($searchForm);

        //check, that search has brought at least one result
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );
    }

    public function testSearchNoResult()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to municipalities list page
        $crawler = $client->request('GET', '/admin/municipality/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipalities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //check, that municipalities list is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-contractors-table_style table tbody tr')->count()
        );

        //select search form, full and submit it
        $searchForm = $crawler->filter('#searchForm')->form();
        $searchForm['searchPhrase'] = 'Test44';
        $crawler = $client->submit($searchForm);

        //check, that search has not brought any result
        $this->assertContains(
            'No results',
            $crawler->filter('.app-contractors-table_style div')->text()
        );
    }
}
