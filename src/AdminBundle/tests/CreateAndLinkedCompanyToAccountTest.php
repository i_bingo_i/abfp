<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class CreateAndLinkedCompanyToAccountTest extends FunctionalTestCase
{
    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to General Company Information form
        $urlCreateAndLinkedCompany = '/admin/company/create-and-linked-to-account/14';
        $crawler = $client->request('GET', $urlCreateAndLinkedCompany);

        //select "CANCEL" link and click it
        $backLink = $crawler->selectLink('CANCEL')->link();
        $this->assertEquals('CANCEL', $backLink->getNode()->textContent, 'No button named "CANCEL"');
        $crawler = $client->click($backLink);

        //check, chet we are on expected page
        $this->assertEquals(
            "Search and select Company",
            $crawler
                ->filter('.app-link__container.app-form--bordered .app-form-header')
                ->eq(0)
                ->text()
        );
    }

    public function testCreateAndLinkedCompanyToAccount()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to General Company Information form
        $urlSearchCompanyForSelect = '/admin/account/search-company-for-select/14';
        $crawler = $client->request('GET', $urlSearchCompanyForSelect);

        //check, that we are on expected page and it is shown as we expected
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');

        $this->assertEquals(
            "Search and select Company",
            $crawler
                ->filter('.app-link__container.app-form--bordered .app-form-header')
                ->eq(0)
                ->text()
        );

        //select "Create New Company" link and click it
        $createNewCompanyLink = $crawler->selectLink('Create New Company')->link();
        $this->assertEquals(
            'Create New Company',
            $createNewCompanyLink->getNode()->textContent,
            'No button named "Create New Company"'
        );
        $crawler = $client->click($createNewCompanyLink);

        //check, that we are on expected page
        $this->assertEquals(
            "General Company Information",
            $crawler
                ->filter('.app-form.app-form--flex.app-form--bordered .app-form-header')
                ->eq(0)
                ->text()
        );

        //select "Create New Company" form, fullize required fields and submit it
        $form = $crawler->selectButton('CREATE')->form();
        $form['company[name]'] = "Test company";
        $client->submit($form);

        //go to Account page
        $urlAccount = '/admin/account/14';
        $crawler = $client->request('GET', $urlAccount);
//
//        $this->assertEquals(
//            'Test company',
//            $crawler
//                ->filter('.app-contact-main__line-text.app-color--white.app-cut-string a.app-link__select')
//                ->eq(0)
//                ->text()
//        );
    }
}
