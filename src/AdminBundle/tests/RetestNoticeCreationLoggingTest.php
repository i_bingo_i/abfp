<?php

namespace AdminBundle\tests;

use AppBundle\Tests\RetestNoticeTestingManager;

class RetestNoticeCreationLoggingTest extends RetestNoticeTestingManager
{
    public function testRetestNoticeCreationLogging()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //filter opportunities by date and division
        $filterOpportunitiesForm = $crawler->filter('#filter-opportunities-form')->form();
        $dateFrom = new \DateTime('-1 month');
        $dateTo = new \DateTime('+1 months');
        $filterOpportunitiesForm['date[from]'] = $dateFrom->format('m/d/Y');
        $filterOpportunitiesForm['date[to]'] = $dateTo->format('m/d/Y');
        $crawler = $client->submit($filterOpportunitiesForm);

        //check, that opportunities table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-opportunities__table tbody tr')->count()
        );

        //select Account link and click it
        $accountLink = $crawler->filter('.app-opportunities__table tbody tr.app-opportunities__table-row--account')
            ->last()->filter('td div div a')->link();
        $crawler = $client->click($accountLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Account ID  -  ',
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //add ContactPerson with authorizer roles to selected Account and return to opportunities list page
        $crawler = $this->addContactPersonToSelectedAccount($crawler, $client);

        //filter opportunities by date and division
        $filterOpportunitiesForm = $crawler->filter('#filter-opportunities-form')->form();
        $dateFrom = new \DateTime('-1 month');
        $dateTo = new \DateTime('+1 month');
        $filterOpportunitiesForm['date[from]'] = $dateFrom->format('m/d/Y');
        $filterOpportunitiesForm['date[to]'] = $dateTo->format('m/d/Y');
        $crawler = $client->submit($filterOpportunitiesForm);

        //check, that opportunities table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-opportunities__table tbody tr')->count()
        );

        //create quick Retest Notice
        //select quick Retest Notice creation link and click it
        $quickRNCreationLink = $crawler->filter('#createNoticeModal div div div')->eq(1)
            ->filter('a')->eq(0)->link();
        $client->click($quickRNCreationLink);
        $crawler = $client->followRedirect();

        //check, that page we were redirected has been shown correctly
        $this->assertContains(
            'Retest Notices',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //check, that Retest Notices table is not empty
        $this->assertGreaterThan(
            0,
            $crawler->filter('.app-notice-list tbody tr')->count()
        );

        //select Retest Notice we are going to view

        //select table row with information about the first retest notice
        $retestNoticeRowObject = $crawler->filter('.app-notice-list tbody tr')->eq(1);

        //select value from "Ref. ID" field of selected table row to check, whether we are on requested page further
        $selectedRetestNoticeRefID = $retestNoticeRowObject->filter('td')->eq(0)->text();

        //select the first reference to Retest Notice and click it
        $retestNoticeLink = $retestNoticeRowObject->filter('td')->eq(2)->filter('a')->link();
        $crawler = $client->click($retestNoticeLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Ref. ID - '.$selectedRetestNoticeRefID,
            $crawler->filter('.app-detailed__info__data-id.app-device__info__data-id')->eq(0)->text()
        );

        //check log message about Retest Notice creation
        $this->assertContains(
            'RETEST NOTICE WAS CREATED',
            $crawler->filter('.app-proposal-logs__table tbody tr td div')->eq(1)
                ->filter('span')->text()
        );
    }
}
