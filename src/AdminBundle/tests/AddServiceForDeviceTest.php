<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class AddServiceForDeviceTest extends FunctionalTestCase
{
    public function testAddServiceForDevice()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to add service for device page
        $deviceId = 1;
        $crawler = $this->goToAddServicePage($client, $deviceId, $prevServicesCount);

        //fullize required fields of form and submit it
        $addServiceForm = $crawler->selectButton('CREATE')->form();
        $addServiceForm['admin_bundle_service[named]'] = '1';
        $addServiceForm['admin_bundle_service[inspectionDue]'] = date('m/d/Y');
        $addServiceForm['admin_bundle_service[fixedPrice]'] = '$160';
        $client->submit($addServiceForm);
        $crawler = $client->followRedirect();

        //get current services count
        $servicesCountText = $crawler->filter('.table.app-data__table tbody tr')->eq(0)
            ->filter('td')->eq(0)->filter('.app-wrapper div')->eq(1)
            ->filter('a .app-font--inherit')->text();
        $servicesCount = intval($servicesCountText);

        //assert, that current services count equals previous services count + 1 (new service has been added)
        $this->assertEquals(
            $servicesCount,
            $prevServicesCount + 1
        );
    }

    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to add service for device page
        $deviceId = 1;
        $crawler = $this->goToAddServicePage($client, $deviceId, $prevServicesCount);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //get current services count
        $servicesCountText = $crawler->filter('.table.app-data__table tbody tr')->eq(0)
            ->filter('td')->eq(0)->filter('.app-wrapper div')->eq(1)
            ->filter('a .app-font--inherit')->text();
        $servicesCount = intval($servicesCountText);

        //assert that current services count equals previous services count (new service has not been added)
        $this->assertEquals(
            $servicesCount,
            $prevServicesCount
        );
    }

    /**
     * @param Client $client
     * @param int $deviceId
     * @param int|null $prevServicesCount
     * @return Crawler
     */
    private function goToAddServicePage(Client $client, int $deviceId, ?int &$prevServicesCount) : Crawler
    {
        //go to view device with id {deviceId} page
        $crawler = $client->request('GET', '/admin/device/view/'.$deviceId);

        //expect to see Page with details of device {deviceId}
        $this->assertContains(
            'Backflow Device ID - '.$deviceId,
            $crawler->filter('.app-detailed__info__data-id.app-device__info__data-id')->text()
        );

        //get previous (before adding or clicking "CANCEL" link) services count and returning it by reference
        $servicesCountText = $crawler->filter('.table.app-data__table tbody tr')->eq(0)
            ->filter('td')->eq(0)->filter('.app-wrapper div')->eq(1)
            ->filter('a .app-font--inherit')->text();
        $prevServicesCount = intval($servicesCountText);

        //select "Add Service" link and click it for Adding service
        $addServiceLink = $crawler->selectLink('Add Service')->link();
        $crawler = $client->click($addServiceLink);

        //assert, that we are on the page for adding new service
        $this->assertContains(
            'Service Information',
            $crawler->filter('.app-form-header')->text()
        );

        //return Crawler which crawls by add new service page
        return $crawler;
    }
}
