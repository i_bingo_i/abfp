<?php

namespace AdminBundle\tests;

use AppBundle\Tests\RetestNoticeTestingManager;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Bundle\FrameworkBundle\Client;

class WarningAccountHasNoAuthorizerTest extends RetestNoticeTestingManager
{
    public function testWarningAccountHasNoAuthorizer()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $accountId = 13;
        $crawler = $client->request('GET', '/admin/account/'.$accountId);

        //check, that Account page has been shown correctly
        $this->assertContains(
            'Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //make request to add new Device to Account
        $crawler = $client->request('GET', '/admin/device/create/'.$accountId.'/1');

        //select form for new Device adding and submit it
        $addNewDeviceForm = $crawler->selectButton('CREATE')->form();
        $client->submit($addNewDeviceForm);
        $crawler = $client->followRedirect();

        //check, that page, we had been redirected, has been shown correctly
        $this->assertContains(
            'Account ID  -  '.$accountId,
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //open part of "Devices and Services" table with devices
        $devicesLink = $crawler->filter('#device-fire'.$accountId)->link();
        $crawler = $client->click($devicesLink);

        //select device link and click it
        $deviceLink = $crawler->filter('#deviceTable tbody tr')->eq(4)->filter('td')->eq(1)->filter('div div a')->link();
        $crawler = $client->click($deviceLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Device ID',
            $crawler->filter('.app-device__info__data-id')->text()
        );

        //add Service to Device
        $this->addServiceToDevice($crawler, $client);

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that opportunities list page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //get Account table row object
        $accountTableRowObject = $crawler
            ->filter('.app-opportunities__table tbody tr.app-opportunities__table-row--account')->first();

        //get Account link and warning container
        $accountLinkAndWarningContainer = $accountTableRowObject->filter('td div div');

        //get Account link to folloow it further
        $accountLink = $accountLinkAndWarningContainer->filter('a')->link();

        //get Account link text to check, that we will be on the page of right Account further
        $accountLinkText = $accountLink->getNode()->textContent;

        //get Account warning text
        $accountWarningText = $accountLinkAndWarningContainer->filter('span')->first()->filter('img')
            ->attr('title');

        //check, that authorizer for some divisions under selected Account is missing
        $this->assertEquals(
            'Authorizer for some divisions under this account is missing',
            $accountWarningText
        );

        //go to selected Account page to set authorizer further
        $crawler = $client->click($accountLink);

        //overtake, that we are on the page of selected Account
        $this->assertContains(
            $accountLinkText,
            $crawler->filter('.app-detailed__info__data-title')->text()
        );

        //go to Account contacts page
        $accountContactsLink = $crawler->filter('.app-detailed__button-container')->eq(1)->filter('a')->link();
        $crawler = $client->click($accountContactsLink);

        //check, that Account contacts page has been shown correctly
        $this->assertContains(
            $accountLinkText,
            $crawler->filter('.app-main__page-title-name a')->text()
        );
        $this->assertContains(
            'Contacts',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //select "Add Contact Person" link and click it
        $addContactPersonLink = $crawler->selectLink('Add Contact Person')->link();
        $crawler = $client->click($addContactPersonLink);

        //check, that page for adding ContactPerson has been shown correctly
        $this->assertContains(
            'Link Contact person to',
            $crawler->filter('.app-link__header--contact')->text()
        );
        $this->assertContains(
            $accountLinkText,
            $crawler->filter('.app-link__title--contact a')->text()
        );

        $crawler = $this->addContactPersonToTestingAccount($crawler, $client);

        //get Account table row object
        $accountTableRowObject = $crawler
            ->filter('.app-opportunities__table tbody tr.app-opportunities__table-row--account')->first();

        //check, that tested Account has not contain tested warning
        $this->assertContains(
            $accountLinkText,
            $accountTableRowObject->filter('td div div a')->text()
        );
        $this->assertNotContains(
            $accountWarningText,
            $accountTableRowObject->filter('td div div')->html()
        );
    }

    private function addContactPersonToTestingAccount(Crawler $crawler, Client $client) : Crawler
    {
        //get Account link text to overtake, that ContactPersonRoles page has been shown correctly further
        $accountLinkText = $crawler->filter('.app-link__title--contact a')->text();

        //select search form, input search phrase and submit it
        $searchForm = $crawler->selectButton('SEARCH')->form();
        $searchForm['searchPhrase'] = 'Test Stark';
        $crawler = $client->submit($searchForm);

        //get the latest ContactPerson from search results table
        $lastSearchResultTableRow = $crawler->filter('.app-data__table tbody tr')->last();

        //get ContactPerson name to overtake we are on the page of roles changing for the selected ContactPerson
        $contactPersonName = $lastSearchResultTableRow->filter('td')->eq(2)->filter('a')->text();

        //select ContactPerson linking link and click it
        $contactPersonSelectLink = $lastSearchResultTableRow->filter('td')->last()->filter('a')->link();
        $crawler = $client->click($contactPersonSelectLink);

        //overtake, that requested page has been shown correctly
        $this->assertContains(
            'Link',
            $crawler->filter('.app-main__page-title-name')->text()
        );
        $this->assertContains(
            $contactPersonName,
            $crawler->filter('.app-main__page-title-name a')->first()->text()
        );
        $this->assertContains(
            $accountLinkText,
            $crawler->filter('.app-main__page-title-name a')->eq(1)->text()
        );

        //select ContactPerson roles form and submit it
        $contactPersonRolesForm = $crawler->filter('#roleForm')->form();
        $client->submit($contactPersonRolesForm);

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that opportunities list page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //return Crawler, which crawls by requested page
        return $crawler;
    }
}
