<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewRetestOpportunitiesListTest extends FunctionalTestCase
{
    public function testViewRetestOpportunitiesList()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that opportunities list page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //select form for opportunities filtering and filter them by type
        $filterOpportunitiesForm = $crawler->filter('#filter-opportunities-form')->form();
        $filterOpportunitiesForm['type'] = 1; //select type "Retest"

        //submit form for opportunities filtering
        $crawler = $client->submit($filterOpportunitiesForm);

        //get table row objects, in which opportunity type is present
        $opportunityTypeRow = $crawler
            ->filter('.app-opportunities__table tbody tr.app-opportunities__table-row--service');

        //get table rows count, in which opportunity type is present
        $opportunityTypeRowsCount = $opportunityTypeRow->count();

        //get table columns, in which opportunity type is "Retest"
        $retestOpportunityTypeColumns = $opportunityTypeRow->each(function ($node) {
            $opportunityTypeTableColumnText = $node->filter('td')->eq(1)->filter('span')->text();

            if ($opportunityTypeTableColumnText == 'Retest') {
                $retestOpportunityType = $opportunityTypeTableColumnText;
            }

            return $retestOpportunityType;
        });

        //check, that all opportunity types are "Retest" in table (type filter is working correctly)
        $this->assertEquals(
            $opportunityTypeRowsCount,
            count($retestOpportunityTypeColumns)
        );
    }
}
