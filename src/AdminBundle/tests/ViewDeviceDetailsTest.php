<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewDeviceDetailsTest extends FunctionalTestCase
{
    public function testViewDeviceDetails()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to Account page
        $accountId = 1;
        $accountURL = $this->generateUrl('admin_account_get', ['accountId' => $accountId]);
        $crawler = $client->request('GET', $accountURL);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Site Account ID  -  ',
            $crawler->filter('.app-detailed__info__data-id span')->text()
        );

        //select link to the list of devices, related to Account, and click it
        $testingDevicesListLink = $crawler->filter('#device-fire1')->link();
        $crawler = $client->click($testingDevicesListLink);

        //select link to the testing device and click it
        $testingDeviceLink = $crawler->filter('tr[device-type="showFire"]')->eq(2)
            ->filter('td')->eq(1)->filter('div div a.app-link__select')
            ->link();
        $crawler = $client->click($testingDeviceLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Fire Device ID',
            $crawler->filter('.app-detailed__info__data-id')->text()
        );

        //check, that we are referenced to the Account we selected, but not other
        $deviceDetailsPageAccountHref = $crawler
            ->filter('.app-device-styles__after-title-block_styling-link a')->attr('href');
        $this->assertEquals(
            $deviceDetailsPageAccountHref,
            $accountURL
        );
    }
}
