<?php
namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class UnRelateCompanyFromContactPersonTest extends FunctionalTestCase
{
    public function testUnRelateCompanyFromContactPerson()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to ContactPerson page and check, that it has been shown correctly
        $urlToContactPerson = '/admin/contact/5';
        $crawler = $client->request('GET', $urlToContactPerson);
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Response status is 2xx');
        $this->assertEquals(
            'Contact Person ID  -  5',
            $crawler
                ->filter('.app-detailed__info__data-id.app-save-space')
                ->eq(0)
                ->text()
        );

        //select unrelate Company link and click it
        $unRelateLink = $crawler
            ->filter('a.app-link__delete')
            ->eq(0)
            ->link()
        ;
        $client->click($unRelateLink);

        //select confirm Company unrelating link and click it
        $confirmUnRelateLink = $crawler
            ->filter('div#deleteCompanyModal a.app-detailed__button.app-detailed__button--red.btn')
            ->eq(0)
            ->link();
        $client->click($confirmUnRelateLink);

        //go to testing ContactPerson page and check, that "Select" link is present (Company is not selected)
        $crawler = $client->request('GET', $urlToContactPerson);
        $link = $crawler->selectLink('Select')->link();
        $this->assertEquals('Select', $link->getNode()->textContent, 'No button named "Select"');
    }
}
