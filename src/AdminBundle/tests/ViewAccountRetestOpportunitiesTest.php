<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;

class ViewAccountRetestOpportunitiesTest extends FunctionalTestCase
{
    public function testViewAccountRetestOpportunities()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to opportunities list page
        $crawler = $client->request('GET', '/admin/opportunity/list');

        //check, that opportunities list page has been shown correctly
        $this->assertContains(
            'Opportunities',
            $crawler->filter('.app-main__page-title-name')->text()
        );

        //get Account row from the opportunities table
        $opportunitiesTableAccountRowObject = $crawler
            ->filter('.app-opportunities__table tbody tr.app-opportunities__table-row--account')->eq(1);

        //select Account link
        $accountLink = $opportunitiesTableAccountRowObject->filter('td .app-detailed__info div a')->link();

        //get Account link text to check that we are on the correct page further
        $accountLinkText = $accountLink->getNode()->textContent;

        //go to Account page
        $crawler = $client->click($accountLink);

        //check, that we are on the correct page
        $this->assertContains(
            $accountLinkText,
            $crawler->filter('.app-detailed__info__data-title')->text()
        );

        //select and click "Opportunities" tab link on Account page
        $opportunitiesTabLink = $crawler->filter('.app-account-tabs__list a')->eq(1)->link();
        $crawler = $client->click($opportunitiesTabLink);

        //select form for opportunities filtering and filter them by type
        $filterOpportunitiesForm = $crawler->filter('#filter-opportunities-form')->form();
        $filterOpportunitiesForm['type'] = 1; //select type "Retest"

        //submit form for opportunities filtering
        $crawler = $client->submit($filterOpportunitiesForm);

        //get opportunity table row objects
        $opportunityRow = $crawler
            ->filter('.app-opportunities__table tbody tr.app-opportunities__table-row--service');

        //get opportunity table rows count
        $opportunityRowsCount = $opportunityRow->count();

        //get opportunity table columns, in which opportunity type is "Retest"
        $retestOpportunityTypeColumns = $opportunityRow->each(function ($node) {
            $opportunityTypeTableColumnText = $node->filter('td')->eq(1)->filter('span')->text();

            if ($opportunityTypeTableColumnText == 'Retest') {
                $retestOpportunityType = $opportunityTypeTableColumnText;
            }

            return $retestOpportunityType;
        });

        //check, that all opportunity types are "Retest" in table (type filter is working correctly)
        $this->assertEquals(
            $opportunityRowsCount,
            count($retestOpportunityTypeColumns)
        );
    }
}
