<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class AddComplianceChannelToDepartmentTest extends FunctionalTestCase
{
    public function testAddComplianceChannelToDepartment()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to view Municipality page
        $municipalityId = 2;
        $crawler = $this->goToAddComplianceChannelPage($client, $municipalityId);

        //select "Compliance Channel information" form and submit it
        $complianceChannelInformationForm = $crawler->selectButton('CREATE')->form();
        $complianceChannelInformationForm['department_channel[fields][0][value]'] =
            '('.rand(100, 999).') '.rand(100, 999).'-'.rand(1000, 9999); //random number of fax to avoid repeats
        $client->submit($complianceChannelInformationForm);
        $client->followRedirect();
    }

    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to view Municipality page
        $municipalityId = 2;
        $crawler = $this->goToAddComplianceChannelPage($client, $municipalityId);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );
    }

    private function goToAddComplianceChannelPage(Client $client, int $municipalityId) : Crawler
    {
        $crawler = $client->request('GET', '/admin/municipality/view/'.$municipalityId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );

        //select department we are going to work with
        $departmentBlock = $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1);

        //select "Add Compliance Channel" link on selecting department block and click it
        $addComplianceChannelLink = $departmentBlock->selectLink('Add Compliance Channel')->link();
        $departmentId = $addComplianceChannelLink->getNode()->getAttribute('data-departmentid');
        $client->click($addComplianceChannelLink);

        //select some compliance channel to make request for adding compliance channel to department
        $complianceChannelId = 3;

        //make request
        $crawler = $client->request('GET', '/admin/department/channel/create/'.$departmentId.'/'.$complianceChannelId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Compliance Channel information',
            $crawler->filter('.app-form-header')->text()
        );

        //return Crawler, that crawls by needed page
        return $crawler;
    }
}
