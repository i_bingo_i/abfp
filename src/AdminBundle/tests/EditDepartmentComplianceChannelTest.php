<?php

namespace AdminBundle\tests;

use AppBundle\Tests\FunctionalTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;

class EditDepartmentComplianceChannelTest extends FunctionalTestCase
{
    public function testEditDepartmentComplianceChannel()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to edit department compliance channel page
        $municipalityId = 2;
        $crawler = $this->goToEditDepartmentComplianceChannelPage($client, $municipalityId);

        //select edit compliance channel form, full required field and submit it
        $editDepartmentComplianceChannelForm = $crawler->selectButton('UPDATE')->form();

        //temporary variable for storing value we are going to change on
        $valueAfterEditing =
            ($editDepartmentComplianceChannelForm['department_channel[fields][0][entityValue][address]']
                ->getValue() == 'Street 1') ?
            'Street 1 Edited' :
            'Street 1';

        $editDepartmentComplianceChannelForm['department_channel[fields][0][entityValue][address]'] =
            $valueAfterEditing;
        $client->submit($editDepartmentComplianceChannelForm);
        $crawler = $client->followRedirect();

        //check, that page we have been redirected has been shown correctly
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );

        //check, that department compliance channel we were editing contains new value
        $this->assertContains(
            $valueAfterEditing,
            $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1)
                ->filter('.municipalities-channel-table table tbody tr')->first()
                ->filter('td')->eq(1)->text()
        );
    }

    public function testCancelButton()
    {
        //log in
        $client = $this->createAuthorizedClient('schur.maksim@gmail.com');

        //go to edit department compliance channel page
        $municipalityId = 2;
        $crawler = $this->goToEditDepartmentComplianceChannelPage($client, $municipalityId);

        //select "CANCEL" link and click it
        $cancelLink = $crawler->selectLink('CANCEL')->link();
        $crawler = $client->click($cancelLink);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );
    }

    private function goToEditDepartmentComplianceChannelPage(Client $client, int $municipalityId) : Crawler
    {
        //go to view Municipality page
        $crawler = $client->request('GET', '/admin/municipality/view/'.$municipalityId);

        //check, that requested page has been shown correctly
        $this->assertContains(
            'Municipality ID - '.$municipalityId,
            $crawler->filter('.municipalities-id')->text()
        );

        //select department we are going to work with
        $departmentBlock = $crawler->filter('.municipalities-devices.app-detailed__wrapper')->eq(1);

        //get row object of compliance channel we are going to edit
        $editingDepartmentChannelRow = $departmentBlock->filter('.municipalities-channel-table table tbody tr')
            ->first();

        //select link for editing compliance channel and click it
        $editDepartmentChannelLink = $editingDepartmentChannelRow->filter('td')->eq(5)
            ->filter('a')->link();
        $crawler = $client->click($editDepartmentChannelLink);

        //check, that we are on the requested page
        $this->assertEquals(
            'Compliance Channel information',
            $crawler->filter('.app-form-header')->text()
        );

        //return Crawler, which crawls by requested page
        return $crawler;
    }
}
