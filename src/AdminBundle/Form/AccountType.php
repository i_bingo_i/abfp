<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AccountType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('notes', TextareaType::class, [
                'required' => false
            ])
            ->add('type', EntityType::class, [
                'class' => 'AppBundle\Entity\AccountType',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => false
            ])
            ->add('clientType', EntityType::class, [
                'class' => 'AppBundle\Entity\ClientType',
                'choice_label' => 'name'
            ])
            ->add('address', AddressType::class, [
                'required' => false,
            ])
//            ->add('billingAddress', AddressType::class, [
//                'required' => false,
//            ])
            ->add('municipality', EntityType::class, [
                'class' => 'AppBundle\Entity\Municipality',
                'choice_label' => 'name',
                'required'    => true,
                'placeholder' => 'Choose Municipality',
                'empty_data'  => null,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.name', 'ASC');
                },
            ])
            ->add('paymentTerm', EntityType::class, [
                'class' => 'AppBundle\Entity\PaymentTerm',
                'choice_label' => 'name'
            ])
            ->add('buildingType', EntityType::class, [
                'class' => 'AppBundle\Entity\AccountBuildingType',
                'choice_label' => 'name',
                'placeholder' => 'Select Type',
                'required' => false
            ])
            ->add('website', TextType::class, [
                'required' => false,
            ])
            ->add('specialDiscount', CheckboxType::class, ['required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Account',
            'allow_extra_fields' => true,
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
