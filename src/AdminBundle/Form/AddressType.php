<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', TextType::class, ['required' => false, "label" => "Address"])
            ->add('city', TextType::class, ['required' => false, "label" => "City"])
            ->add('zip', TextType::class, ['required' => false, "label" => "Zip"])
            ->add('state', EntityType::class, [
                'class' => 'AppBundle\Entity\State',
                "label" => "State",
                'choice_label' => 'code',
                'placeholder' => 'Select State',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Address',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
