<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class InspectionNamedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => false
            ])
            ->add('isNeedSendMunicipalityReport', CheckboxType::class, [
                'required' => false,
            ])
            ->add('isNeedMunicipalityFee', CheckboxType::class, [
                'required' => false,
            ])
            ->add('frequency', EntityType::class, [
                'class' => 'AppBundle\Entity\ServiceFrequency',
                'choice_label' => 'name',
                'required'    => true,
                'empty_data'  => null,
                'choice_attr' => function ($val, $key) {
                    return ['data-frequency' => $val->getName()];
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('sf');
                },
            ]);
    }
}
