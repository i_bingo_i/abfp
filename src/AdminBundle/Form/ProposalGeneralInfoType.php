<?php

namespace AdminBundle\Form;

use AppBundle\Entity\NotIncluded;
use AppBundle\Entity\Proposal;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\Length;

class ProposalGeneralInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $division = $builder->getData()->getDivision();

        $builder
            ->add('afterHoursWorkCost', TextType::class)
            ->add('discount', TextType::class)
            ->add('additionalCost', TextType::class, [
                'required' => false
            ])
            ->add('internalComments', TextareaType::class, [
                'required' => false
            ])
            ->add('additionalComment', TextareaType::class, [
                "required" => false
            ])
            ->add('additionalCostComment', TextareaType::class, [
                "required" => false
            ])
            ->add('notIncludedItems', EntityType::class, [
                'class' => NotIncluded::class,
                'expanded' => true,
                'multiple' => true,
                'required' => true,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) use ($division) {
                    return $er->createQueryBuilder('ni')
                        ->where('ni.division = :division')
                        ->setParameters(['division' => $division]);
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Proposal',
            'validation_groups' => [
                'generailInfo'
            ],
            'cascade_validation' => true,
        ));
    }
}
