<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Device;
use AppBundle\Entity\ServiceNamed;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceNameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Device $device */
        $device = $options['device'];
        $builder
            ->add('name', EntityType::class, array(
                'choice_label' => false,
                'class' => ServiceNamed::class,
                'query_builder' => function (EntityRepository $er) use ($device) {
                    $query = $er->createQueryBuilder("sn");
                    $query
                        ->leftJoin('sn.deviceNamed', 'dn')
                        ->where($query->expr()->isNull('sn.frequency'))
                        ->andWhere('dn.alias = :deviceName')
                        ->setParameter("deviceName", $device->getNamed()->getAlias())
                    ;

                    return $query;
                },
                'multiple' => true,
                'expanded' => true,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
        $resolver->setRequired('device');
    }
}
