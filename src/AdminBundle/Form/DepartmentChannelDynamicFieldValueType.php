<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\ORM\EntityRepository;

class DepartmentChannelDynamicFieldValueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($builder) {
                $form = $event->getForm();
                /** @var DynamicFieldValue $child */
                $child = $event->getData();
                $fieldTypeAlias = $child->getField()->getType()->getAlias();

                if ($fieldTypeAlias == 'text') {
                    $form->add('value', TextType::class, [
                            'required' => true,
                            'label' => $child->getField()->getName(),
                        ])
                    ;
                }

                if ($fieldTypeAlias == 'entity') {
                    $form->add('entityValue', AddressType::class, [
                        'required' => false,
                    ]);
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DepartmentChannelDynamicFieldValue',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
