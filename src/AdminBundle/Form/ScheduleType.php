<?php

namespace AdminBundle\Form;

use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Workorder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ScheduleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Workorder $workorder */
        $workorder = $options['workorder'];
        $builder
            ->add('user', EntityType::class, [
                'class' => 'AppBundle\Entity\ContractorUser',
                'placeholder' => 'Choose Technician',
                'choice_label' => function(ContractorUser $contractorUser) {
                    return $contractorUser->getUser()->getFirstName() . ' ' . $contractorUser->getUser()->getLastName();
                },
                'query_builder' => function (ContractorUserRepository $cu) use ($workorder) {
                    return $cu->createQueryBuilder('cu')
                        ->leftJoin('cu.contractor', 'c')
                        ->leftJoin('cu.license', 'l')
                        ->leftJoin('l.named', 'ln')
                        ->leftJoin('ln.category', 'lc')
                        ->where("c.name = 'American Backflow & Fire Prevention'")
                        ->andWhere('lc.alias = :division')
                        ->setParameter('division', $workorder->getDivision()->getAlias());
                },
                'constraints' => [new NotBlank()],

            ])
            ->add('startDate',TextType::class, ['constraints' => [new NotBlank()]])
            ->add('startTime',TextType::class, ['constraints' => [new NotBlank()]])
//            ->add('endDate',TextType::class, ['constraints' => [new NotBlank()]])
            ->add('endTime',TextType::class, ['constraints' => [new NotBlank()]])
            ->add('isTimeCritical', CheckboxType::class, ['label' => 'Time Critical', 'required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
        $resolver->setRequired('workorder');
    }

    public function getBlockPrefix()
    {
        return 'schedule';
    }
}
