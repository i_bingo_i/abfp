<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ContractorServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('state', EntityType::class, [
                'class' => 'AppBundle\Entity\State',
                'choice_label' => 'code',
                'required'    => true,
                'choice_attr' => function ($val, $key) {
                    return ['data-state' => $val->getName()];
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('st');
                },
            ])
            ->add('fixedPrice', TextType::class, ['required' => false])
            ->add('reference', TextType::class, ['required' => false])
            ->add('estimationTime', TextType::class, [
                'required' => false,
                'empty_data'  => 0,
            ])
        ;
    }
}
