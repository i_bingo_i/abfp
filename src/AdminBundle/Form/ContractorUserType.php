<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ContractorUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contractor', EntityType::class, [
                'class' => 'AppBundle\Entity\Contractor',
                'choice_label' => 'name'
            ])
            ->add('user', UserType::class, [
                'required' => false
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\ContractorUser',
            'allow_extra_fields' => true,
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true
        ]);
    }
}
