<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ContactPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class, ['required' => false])
            ->add('title', TextType::class, ['required' => false])
            ->add('email', EmailType::class, ['required' => false])
            ->add('phone', TextType::class, ['required' => false])
            ->add('ext', TextType::class, ['required' => false])
            ->add('cell', TextType::class, ['required' => false])
            ->add('fax', TextType::class, ['required' => false])
            ->add('cod', CheckboxType::class)
            ->add('notes', TextareaType::class, ['required' => false])
            ->add('personalAddress', AddressType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ContactPerson',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
