<?php

namespace AdminBundle\Form;

use AppBundle\Entity\ServiceNamed;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class InspectionsInformationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $named = $options['data']['named'];

        $builder
            ->add('fixedPrice', TextType::class, [
                'required' => false,
                'label' => 'Change all selected inspections fee to:',
            ])
            ->add('inspectionDue', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'MM/dd/yyyy',
                'required' => false,
                'label' => 'Change Due Date to:'
            ])
            ->add('lastTested', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'MM/dd/yyyy',
                'required' => false,
                'label' => 'Change Last Tested Date to:'
            ])
            ->add('named', EntityType::class, [
                'class' => 'AppBundle\Entity\ServiceNamed',
                'choice_label' => function (ServiceNamed $serviceNamed) {
                    /** @var ServiceNamed $serviceNamed */
                    $name = $serviceNamed->getName();
                    return $name;
                },
                'required'    => false,
                'empty_data'  => null,
                'placeholder' => 'Please select inspection name',
                'query_builder' =>function (EntityRepository $er) use ($named) {
                    $query = $er->createQueryBuilder("sn");
                    $query
                        ->where('sn IN (:named)')
                        ->setParameter("named", $named);
                    return $query;
                }
            ])
            ->add('companyLastTested', EntityType::class, [
                'class' => 'AppBundle\Entity\Contractor',
                'choice_label' => 'name',
                'required'    => false,
                'placeholder' => 'Choose Company Last Tested',
                'empty_data'  => null,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
            ]);
    }


    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'inspections_information';
    }
}
