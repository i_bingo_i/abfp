<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Contractor;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class RepairServiceNamedWithContractor extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Contractor $contractor */
        $contractor = $options['data']['contractor'];

        $builder
            ->add('deficiency', TextType::class, [
                'required' => true
            ])
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('deviceNamed', EntityType::class, [
                'class' => 'AppBundle\Entity\DeviceNamed',
                'choice_label' => 'name',
                'required'    => true,
                'choice_attr' => function ($val, $key) {
                    return ['data-device-named' => $val->getName()];
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('dn');
                },
            ])
            ->add('contractor', HiddenType::class, [
                'data' => $contractor->getId()
            ])
            ->add('isNeedSendMunicipalityReport', CheckboxType::class, [
                'required' => false,
            ])
            ->add('isNeedMunicipalityFee', CheckboxType::class, [
                'required' => false,
            ])
        ;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_bundle_repair_service_named_with_contractor';
    }
}
