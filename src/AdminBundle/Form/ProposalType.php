<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Account;
use AppBundle\Entity\Device;
use AppBundle\Entity\Service;
use AppBundle\Entity\SpecialNotification;
use AppBundle\Entity\Workorder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;

class ProposalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Device $device */
        $device = $options['device'];
        $builder
            ->add('services', EntityType::class, array(
                'choice_label' => false,
                'class' => Service::class,
                'query_builder' => function (EntityRepository $er) use ($device) {
                    $query = $er->createQueryBuilder("s");
                    $query
                        ->leftJoin('s.named', 'sn')
                        ->leftJoin('s.status', 'ss')
                        ->where('s.device = :device')
                        ->andWhere($query->expr()->notIn('ss.alias', ['resolved', 'discarded']))
                        ->andWhere('s.deleted = false')
                        ->andWhere($query->expr()->isNull('sn.frequency'))
                        ->setParameter("device", $device)
                    ;

                    return $query;
                },
                'multiple' => true,
                'expanded' => true,
//                'choice_attr' => function () {
//                    return ['checked' => 'checked'];
//                },
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Proposal',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
        $resolver->setRequired('device');
    }
}
