<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactPersonFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contactPerson', ContactPersonType::class)
            ->add('billingAddress', BillingAddressType::class, [
                'validation_groups' => [
                    'create_contact_person'
                ],
            ])
        ;
    }
}
