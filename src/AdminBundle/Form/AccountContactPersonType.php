<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Twig\AppExtension;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AccountContactPersonType extends AbstractType
{
    /** @var  AppExtension */
    private $appExtension;

    public function __construct(AppExtension $appExtension)
    {
        $this->appExtension = $appExtension;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var AccountContactPerson $accountContactPerson */
        $accountContactPerson = $builder->getData();
        $customers = $options['customers'];
        $customer = $accountContactPerson->getCustomer() ?? $customers['Account Person'];

        $builder
            ->add('authorizer', CheckboxType::class)
            ->add('access', CheckboxType::class)
            ->add('accessPrimary', CheckboxType::class)
            ->add('payment', CheckboxType::class)
            //->add('paymentPrimary', CheckboxType::class)
            ->add('notes', TextareaType::class, [
                'required' => false
            ])
            ->add('responsibilities', EntityType::class, [
                'class' => 'AppBundle\Entity\DeviceCategory',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true
            ])
            ->add('paymentResponsibilities', EntityType::class, [
                'class' => 'AppBundle\Entity\DeviceCategory',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true
            ])
            ->add("sendingAddress", EntityType::class, [
                'class' => 'AppBundle\Entity\Address',
                'placeholder' => false,
                'choice_label' => function ($address) {
                    $addressForSelect = (strlen($this->appExtension->addressFilter($address)) <= 1) ?
                        "No Address" :
                        $this->appExtension->addressFilter($address);
                    if ($address->getAddressType()->getAlias()) {
                        return $address->getAddressType()->getName() . " - " . $addressForSelect;
                    }

                    return $address->getAddressType()->getAlias();
                },
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use ($accountContactPerson) {
                    /** @var Address[] $addresses */
                    $addresses = [];
                    /** @var ContactPerson $contactPerson */
                    $contactPerson = $accountContactPerson->getContactPerson();
                    if ($contactPerson->getPersonalAddress()) {
                        $addresses[] = $contactPerson->getPersonalAddress();
                    }

                    /** @var Company $company */
                    $company = $accountContactPerson->getContactPerson()->getCompany();
                    if ($company) {
                        $addresses[] = $company->getAddress();
                    }

                    /** @var Account $account */
                    $account = $accountContactPerson->getAccount();
                    if ($account->getType()->getAlias() === 'account' and $account->getAddress()) {
                        $addresses[] = $account->getAddress();
                    }
                    if ($account->getType()->getAlias() === 'group account'
                        and $account->getOldSystemMasLevelAddress()
                    ) {
                        $addresses[] = $account->getOldSystemMasLevelAddress();
                    }

                    $query = $er->createQueryBuilder("a");
                    return $query
                        ->leftJoin("a.addressType", "at")
                        ->where("a IN (:addresses)")
                        ->setParameter('addresses', $addresses)
                        ->orderBy("at.sort", "ASC");
                },
            ])
            ->add('customer', ChoiceType::class, [
                'required' => false,
                'choices' => $customers,
                'data' => $customer
            ])
            ->add('billingAddress', ACPBillingAddressType::class, [
                'required' => false,
                'mapped' => false,
                'customer' => $customer,
                'acp' => $accountContactPerson
            ])
            ->add('check', CheckboxType::class, [
                'required' => false,
                'mapped' => false
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\AccountContactPerson',
            'allow_extra_fields' => true,
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true
        ]);
        $resolver->setRequired('customers');
    }
}
