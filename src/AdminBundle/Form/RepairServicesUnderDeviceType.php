<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Device;
use AppBundle\Entity\Service;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class RepairServicesUnderDeviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Device $device */
        $device = $options['device'];
        $builder
            ->add('services', EntityType::class, array(
                'choice_label' => false,
                'class' => Service::class,
                'query_builder' => function (EntityRepository $er) use ($device) {
                    $query = $er->createQueryBuilder("s");
                    $query
                        ->leftJoin('s.named', 'sn')
                        ->leftJoin('s.status', 'ss')
                        ->where('s.device = :device')
                        ->andWhere($query->expr()->notIn('ss.alias', ['resolved', 'discarded']))
                        ->andWhere('s.deleted = false')
                        ->andWhere($query->expr()->isNull('sn.frequency'))
                        ->setParameter("device", $device)
                    ;

                    return $query;
                },
                'multiple' => true,
                'expanded' => true,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Workorder',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
        $resolver->setRequired('device');
    }
}
