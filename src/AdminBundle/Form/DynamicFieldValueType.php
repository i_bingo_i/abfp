<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\DynamicFieldValue;
use AppBundle\Entity\DynamicFieldDropboxChoices;
use Doctrine\ORM\EntityRepository;

class DynamicFieldValueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($builder) {
                $form = $event->getForm();
                /** @var DynamicFieldValue $child */
                $child = $event->getData();
                $fieldTypeAlias = $child->getField()->getType()->getAlias();

                if ($fieldTypeAlias == 'text') {
                    $form->add('value', TextType::class, [
                            'required' => false,
                            'label' => $child->getField()->getName(),
                        ])
                    ;
                }

                if ($fieldTypeAlias == 'dropdown') {
                    $form->add('optionValue', EntityType::class, array(
                        'class' => DynamicFieldDropboxChoices::class,
                        'expanded' => false,
                        'multiple' => false,
                        'required' => false,
                        'placeholder' => "Choose " . $child->getField()->getName(),
                        'choice_label' => 'optionValue',
                        'label' => $child->getField()->getName(),
                        'query_builder' => function (EntityRepository $er) use ($child) {
                            return $er->createQueryBuilder('u')
                                ->where('u.field = :field')
                                ->setParameters(['field' => $child->getField()]);
                        },
                    ));
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DynamicFieldValue',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
