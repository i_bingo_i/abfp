<?php

namespace AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotEqualTo;

class ChangeDueDateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('named', EntityType::class, [
                'class' => 'AppBundle\Entity\ServiceNamed',
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    $query = $er->createQueryBuilder('sn');
                    return $query
                        ->leftJoin('sn.deviceNamed', 'dn')
                        ->where($query->expr()->isNotNull('sn.frequency'));
                }
            ])
            ->add('months', ChoiceType::class, [
                    'choices'  => [
                        'January' => 1,
                        'February' => 2,
                        'March' => 3,
                        'April' => 4,
                        'May' => 5,
                        'June' => 6,
                        'July' => 7,
                        'August' => 8,
                        'September' => 9,
                        'October' => 10,
                        'November' => 11,
                        'December' => 12
                    ]
                ]
            )
            ->add('year', ChoiceType::class, [
                'choices' => (function () {
                    $yearNow = (int)date('Y');
                    $res = [];
                    $quantityIterations = $yearNow - 1989;
                    while ($quantityIterations--) {
                        $year = $yearNow--;
                        $res[$year] = $year;
                    }

                    $res['1900'] = '1900';

                    return $res;
                })(),
            ])
            ->add('yearTo', ChoiceType::class, [
                'choices' => (function(){
                    $yearNow = (int) date('Y');
                    $fetureYear = (int) (new \DateTime())->modify('+10 years')->format('Y');
                    $res = [];
                    $quantityIterations = $fetureYear - $yearNow;
                    while ($quantityIterations--) {
                        $year = $yearNow++;
                        $res[$year] = $year;
                    }

                    return $res;
                })(),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([]);
    }

    public function getBlockPrefix()
    {
        return 'admin_bundle_change_due_date_service';
    }
}
