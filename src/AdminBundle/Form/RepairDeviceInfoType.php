<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Device;
use AppBundle\Entity\SpecialNotification;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RepairDeviceInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Device $device */
        $device = $options['device'];
        $builder
            ->add('specialNotification', EntityType::class, [
                'choice_label' => 'name',
                'placeholder' => 'Select Special Notification or Warnings',
                'required' => false,
                'class' => SpecialNotification::class,
                'query_builder' => function (EntityRepository $er) use ($device) {
                    $query = $er->createQueryBuilder("sn");
                    $query
                        ->leftJoin('sn.division', 'dc')
                        ->where('dc.id = :division')
                        ->setParameter("division", $device->getNamed()->getCategory()->getId())
                    ;

                    return $query;
                },
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('price', TextType::class, ['required' => false])
            ->add('estimationTime', TextType::class, ['required' => false])
            ->add('comments', TextareaType::class, ['required' => false])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\RepairDeviceInfo',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
        $resolver->setRequired('device');
    }
}
