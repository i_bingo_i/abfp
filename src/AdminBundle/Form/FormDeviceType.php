<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Image;
use AdminBundle\Form\DeviceType;

class FormDeviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('device', DeviceType::class)
            ->add('photo', FileType::class, [
                "required" => false,
                "constraints" => [
                    new Image([
                        'maxSize' => '10M',
                        'mimeTypes' => [
                            "image/png",
                            "image/jpeg",
                            "image/jpg"
                        ],
                        'mimeTypesMessage' => 'Please use JPG, JPEG or PNG images only',
                        'maxSizeMessage' => 'The file is too large. Allowed maximum size is 10Mb',
                        'groups' => ['create', 'update']
                    ])
                ],
            ])
        ;

    }
}
