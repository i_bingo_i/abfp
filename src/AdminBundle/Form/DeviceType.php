<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Entity\DynamicFieldValue;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Image;
use AppBundle\Entity\Device;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DeviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('location', TextType::class, [
                'required' => false,
                'label' => "Location:"
            ])
            ->add('comments', TextareaType::class, [
                'required' => false,
                'label' => "Comments:"
            ])
            ->add('noteToTester', TextareaType::class, [
                'required' => false,
                'label' => 'Note to tester:'
            ])
            ->add('status', EntityType::class, [
                'class' => 'AppBundle\Entity\DeviceStatus',
                'choice_label' => 'name',
                'placeholder' => false,
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'label' => 'Status: '
            ])
            ->add('fields', CollectionType::class, [
                'entry_type'   => DynamicFieldValueType::class,
                'required' => false

            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Device',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
