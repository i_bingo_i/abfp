<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class InspectionServiceNamedWithContractor extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('serviceNamed', InspectionNamedType::class, [
                'data' => $builder->getData()['serviceNamed']
                ])
            ->add('contractorService', ContractorServiceType::class, [
                'data' => $builder->getData()['contractorService']
            ])
            ->add('deviceNamed', EntityType::class, [
                'class' => 'AppBundle\Entity\DeviceNamed',
                'choice_label' => 'name',
                'required'    => true,
                'choice_attr' => function ($val, $key) {
                    return ['data-device-named' => $val->getName()];
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('dn');
                },
            ])
        ;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_bundle_inspection_named_with_contractor';
    }
}
