<?php

namespace AdminBundle\Form;

use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\ContractorUserHistory;
use AppBundle\Entity\Letter;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LetterType extends AbstractType
{
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;

    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $currentUser */
        $currentUser = $this->serviceContainer->get('security.token_storage')->getToken()->getUser();
        /** @var ContractorUser $contractorUser */
        $contractorUser = $this->contractorUserRepository->findOneBy(['user' => $currentUser]);
        $contractor = $contractorUser->getContractor();
        $builder
            ->add('from', EntityType::class, [
                'class' => 'AppBundle\Entity\ContractorUserHistory',
                'choice_label' => function (ContractorUserHistory $contractorUser) {
                    /** @var User $user */
                    $user = $contractorUser->getOwnerEntity()->getUser();
                    return $user->getFirstName() . ' ' . $user->getLastName() . ' ' . '-' . ' ' . $user->getEmail();
                },
                'required'    => true,
                'placeholder' => 'Choose Contractor User',
                'query_builder' => function (EntityRepository $er) use ($contractor) {
                    /** @var QueryBuilder $query */
                    $query = $er->createQueryBuilder('cuh');
                    $query->where('cuh.contractor = :contractor')
                        ->setParameter('contractor', $contractor)
                        ->orderBy('cuh.dateSave', 'DESC')
                        ->groupBy('cuh.user');

                    return $query;
                },
                'expanded' => false,
                'multiple' => false
            ])
            ->add('subject', TextType::class)
            ->add('body', TextareaType::class, [
                'required' => false
            ])
            ->add('emails', TextType::class, [
                'required' => true
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Letter'
        ));
    }
}
