<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class WorkorderSendLetterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', TextareaType::class)
            ->add('photo', FileType::class, [
                "required" => false,
                "constraints" => [
                    new Image([
                        'maxSize' => '10M',
                        'mimeTypes' => [
                            "image/png",
                            "image/jpeg",
                            "image/jpg",
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please use JPG, JPEG or PNG images only',
                        'maxSizeMessage' => 'The file is too large. Allowed maximum size is 10Mb',
                        'groups' => ['create', 'update']
                    ])
                ],
            ])
            ->add('forAdminUsersOnly', CheckboxType::class, ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\WorkorderComment',
            'validation_groups' => [
                'create', 'update'
            ],
        ]);
    }

    public function getBlockPrefix()
    {
        return 'admin_bundle_workorder_comment';
    }
}
