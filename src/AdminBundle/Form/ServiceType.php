<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Device;
use AppBundle\Entity\EntityManager\DeviceManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use AppBundle\Entity\Service;

class ServiceType extends AbstractType
{
    /** @var DeviceManager $deviceManager */
    private $deviceManager;

    /**
     * ServiceType constructor.
     * @param DeviceManager $deviceManager
     */
    public function __construct(DeviceManager $deviceManager) {
        $this->deviceManager = $deviceManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Service $service */
        $service = $options['data'];
        /** @var Device $device */
        $device = $service->getDevice();
        $deviceNamedId = $service->getDevice()->getNamed()->getId();

        $builder
            ->add('named', EntityType::class, [
                'class' => 'AppBundle\Entity\ServiceNamed',
                'choice_label' => 'name',
                'required'    => false,
                'placeholder' => 'Choose Inspection Name',
                'empty_data'  => null,
                'choice_attr' => function($val, $key) {
                    return ['data-frequency' => $val->getFrequency()->getName()];
                },
                'query_builder' => function (EntityRepository $er) use ($deviceNamedId) {
                    return $er->createQueryBuilder('sn')
                        ->leftJoin('sn.deviceNamed', 'dn', 'sn.id=dn.id')
                        ->where('sn.frequency IS NOT NULL')
                        ->andWhere('dn.id = :deviceNamedId')
                        ->setParameter('deviceNamedId', $deviceNamedId);
                },
            ])
            ->add('fixedPrice', TextType::class, ['required' => false])
//            ->add('hourlyPrice', MoneyType::class, ['currency' => false])
            ->add('lastTested', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'MM/dd/yyyy',
                'required' => false
            ])
            ->add('inspectionDue', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'MM/dd/yyyy',
                'required' => false
            ])
            ->add('device', EntityType::class, [
                'class' => 'AppBundle:Device',
                'choices' => $this->deviceManager->getTreeForDevice($device),
                'choice_label' => 'named.name',
                'expanded' => true
            ])
            ->add('companyLastTested', EntityType::class, [
                'class' => 'AppBundle\Entity\Contractor',
                'choice_label' => 'name',
                'required'    => true,
                'placeholder' => 'Choose Company Last Tested',
                'empty_data'  => null,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
            ])
            ->add('comment', TextareaType::class, ['required' => false])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Service',
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'admin_bundle_service';
    }
}
