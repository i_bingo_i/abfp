<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MunicipalityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['required' => false])
            ->add('municipalityPhone', TextType::class, ['required' => false])
            ->add('municipalityFax', TextType::class, ['required' => false])
            ->add('website', TextType::class, ['required' => false])
            ->add('address', AddressType::class, [
                'required' => false,
            ]);
    }
}
