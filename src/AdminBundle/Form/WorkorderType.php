<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Workorder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\NotBlank;

class WorkorderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Account $account */
        $account = $options['account'];
        $builder
            ->add('comment', TextType::class, [
                'required' => false,
                'label' => "Comment:"
            ])
            ->add('workorder', TextType::class, [
                'required' => false,
                'label' => "Workorder:"
            ])
            ->add('authorizer', EntityType::class, [
                'class' => 'AppBundle\Entity\AccountContactPerson',
                'required'    => true,
                'constraints' => [new NotBlank()],
                'placeholder'    => 'Choose authorizer',
                'choice_label' => function (AccountContactPerson $accountContactPerson) {
                    $name = $accountContactPerson->getContactPerson()->getFirstName()." ".
                        $accountContactPerson->getContactPerson()->getLastName();

                    return $name;
                },
                'expanded' => false,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) use ($account) {
                    $query = $er->createQueryBuilder("acp");
                    $query
                        ->where('acp.account = :account')
                        ->andWhere('acp.deleted = false')
                        ->setParameter("account", $account)
                    ;

                    return $query;
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
        $resolver->setRequired('account');
    }

    public function getBlockPrefix()
    {
        return 'workorder_log';
    }
}
