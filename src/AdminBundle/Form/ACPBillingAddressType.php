<?php

namespace AdminBundle\Form;

use AppBundle\Entity\AccountContactPerson;
use InvoiceBundle\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ACPBillingAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Customer $customer */
        $customer = $options['customer'];
        /** @var AccountContactPerson $acp */
        $acp = $options['acp'];
        $billAddress = $customer->getBillingAddress();

        if (!empty($acp->getCustomBillingAddress())
            && !empty($acp->getCustomBillingAddress()->getAddress())
        ) {
            $billAddress = $acp->getCustomBillingAddress();
        }

        $builder
            ->add('address', TextareaType::class, [
                'required' => false,
                'label' => "Address",
                'data' => $billAddress->getAddress()
            ])
            ->add('city', TextType::class, [
                'required' => false,
                "label" => "City",
                'data' => $billAddress->getCity()
            ])
            ->add('zip', TextType::class, [
                'required' => false,
                "label" => "Zip",
                'data' => $billAddress->getZip()
            ])
            ->add('state', EntityType::class, [
                'class' => 'AppBundle\Entity\State',
                "label" => "State",
                'choice_label' => 'code',
                'placeholder' => 'State',
                'required' => false,
                'data' => $billAddress->getState()
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Address',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
        $resolver->setRequired('customer');
        $resolver->setRequired('acp');
    }
}
