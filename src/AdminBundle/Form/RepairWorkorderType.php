<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Device;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RepairWorkorderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        /** @var Device $device */
        $device = $data['device'];

        $builder
            ->add('workorderId', TextType::class, [
                'required' => false,
                'label' => "Workorder:"
            ])
            ->add('workorder', RepairServicesUnderDeviceType::class, [
                'device' => $device
            ])
            ->add('serviceName', ServiceNameType::class, ['device' => $device])
            ->add('deviceInfo', RepairDeviceInfoType::class, [
                    'data' => $builder->getData()['repairDeviceInfo'],
                    'device' => $device
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}