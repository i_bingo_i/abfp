<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Account;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\NotIncluded;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class WorkorderGeneralInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Workorder $workorder */
        $workorder = $builder->getData();
        /** @var Account $account */
        $account = $workorder->getAccount();
        $division = $builder->getData()->getDivision();

        $builder
            ->add('paymentMethod', EntityType::class, [
                'class' => 'AppBundle\Entity\PaymentMethod',
                'required'    => true,
                'placeholder'    => false,
                'expanded' => false,
                'multiple' => false,
                'choice_label' => 'name'
            ])
            ->add('directions', TextareaType::class, [
                'required' => false,
                'label' => 'Workorder Directions:'
            ])
            ->add('additionalComments', TextareaType::class, [
                'required' => false,
                'label' => 'Additional Comments:'
            ])
            ->add('internalComment', TextareaType::class, [
                'required' => false,
                'label' => 'Internal Comments:'
            ])
            ->add('additionalCostComment', TextareaType::class, [
                'required' => false,
                'label' => 'Additional Cost Comment:'
            ])
            ->add('afterHoursWorkCost', TextType::class, [
                'required' => false,
                'label' => 'After Hours Work Cost:'
            ])
            ->add('additionalServicesCost', TextType::class, [
                'required' => false,
                'label' => 'Additional Service Cost:'
            ])
            ->add('discount', TextType::class, [
                'required' => false,
                'label' => 'Discount:'
            ])
            ->add('authorizer', EntityType::class, [
                'class' => 'AppBundle\Entity\AccountContactPerson',
                'placeholder'    => 'Choose authorizer',
                'choice_label' => function ($accountContactPerson) {
                    $name = $accountContactPerson->getContactPerson()->getFirstName()." ".
                        $accountContactPerson->getContactPerson()->getLastName();

                    return $name;
                },
                'expanded' => false,
                'multiple' => false,
                'required'    => false,
                'query_builder' =>function (EntityRepository $er) use ($account) {
                    $query = $er->createQueryBuilder("acp");
                    $query
                        ->where('acp.account = :account')
                        ->andWhere('acp.deleted = false')
                        ->setParameter("account", $account)
                    ;

                    return $query;
                }
            ])
            ->add('notIncludedItems', EntityType::class, [
                'class' => NotIncluded::class,
                'expanded' => true,
                'multiple' => true,
                'required' => true,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) use ($division) {
                    return $er->createQueryBuilder('ni')
                        ->where('ni.division = :division')
                        ->setParameters(['division' => $division]);
                },
            ])
            ->add('poNo', TextType::class, [
                'required' => false,
                'label' => 'P.O NO:'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Workorder',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
