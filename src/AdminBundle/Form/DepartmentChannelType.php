<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Entity\DynamicFieldValue;
use Symfony\Component\Validator\Constraints\NotBlank;
use AppBundle\Entity\DepartmentChannel;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class DepartmentChannelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var DepartmentChannel $entity */
        $entity =$builder->getData();
        $builder
            ->add("uploadFee", TextType::class, [
                'required' => false,
                'label' => "Upload Fee: "
            ])
            ->add("processingFee", TextType::class, [
                'required' => false,
                'label' => "Processing Fee: "
            ])
            ->add("feesWillVary", CheckboxType::class, [
                'label'    => 'Fees will vary',
                'required' => false,
                'label_attr' => [
                    "class" => "app-checkbox-label app-detailed__minus-margin-left"
                ],
                'attr' => [
                    "class" => "app-checkbox-input"
                ]
            ]);

        if ($entity->getIsDefault()) {
            $builder
                ->add("isDefault", HiddenType::class, [
                    'label'    => 'Set as Default',
                    'required' => false,
                    'label_attr' => [
                        "class" => "app-checkbox-label"
                    ]
                ]);
        } else {
            $builder
                ->add("isDefault", CheckboxType::class, [
                    'label'    => 'Set as Default',
                    'required' => false,
                    'label_attr' => [
                        "class" => "app-checkbox-label"
                    ],
                    'attr' => [
                        "class" => "app-checkbox-input"
                    ]
                ]);
        }
        $builder
            ->add('fields', CollectionType::class, [
                'entry_type'   => DepartmentChannelDynamicFieldValueType::class,
                'required' => false

            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DepartmentChannel',
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
