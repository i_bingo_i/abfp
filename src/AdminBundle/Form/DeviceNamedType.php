<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class DeviceNamedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => false,
            ])
            ->add('alias', TextType::class, [
                'required' => true,
            ])
            ->add('parent', EntityType::class, [
                'class' => 'AppBundle\Entity\DeviceNamed',
                'choice_label' => 'name',
                'placeholder' => 'Select Device',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('dp')
                        ->orderBy('dp.name', 'ASC');
                },
            ])
            ->add('category', EntityType::class, [
                'class' => 'AppBundle\Entity\DeviceCategory',
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    $query = $er->createQueryBuilder('dc');
                    $query
                        ->orderBy('dc.name', 'ASC');
                    return $query;
                },
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'create_device_named';
    }
}
