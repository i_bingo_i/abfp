<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Device;
use AppBundle\Entity\Service;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class RepairProposalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        /** @var Device $device */
        $device = $data['device'];

        $builder
            ->add('proposal', ProposalType::class,
                [
                    'data' => $builder->getData()['proposal'],
                    'device' => $device
                ])
            ->add('serviceName', ServiceNameType::class, ['device' => $device])
            ->add('deviceInfo', RepairDeviceInfoType::class,
                [
                    'data' => $builder->getData()['repairDeviceInfo'],
                    'device' => $device
                ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => [
                'create', 'update'
            ],
            'cascade_validation' => true,
        ));
    }
}
