<?php

namespace AdminBundle\Form;

use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\ContractorUserHistory;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Validator\Constraints\NotBlank;

class WorkorderLetterType extends AbstractType
{
    /** @var ContainerInterface */
    private $serviceContainer;
    /** @var ObjectManager */
    private $objectManager;
    /** @var ContractorUserRepository */
    private $contractorUserRepository;

    public function __construct(ObjectManager $objectManager, ContainerInterface $container)
    {
        $this->objectManager = $objectManager;
        $this->serviceContainer = $container;
        $this->contractorUserRepository = $this->objectManager->getRepository('AppBundle:ContractorUser');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $formData = $options["data"];
        /** @var User $currentUser */
        $currentUser = $this->serviceContainer->get('security.token_storage')->getToken()->getUser();
        /** @var ContractorUser $contractorUser */
        $contractorUser = $this->contractorUserRepository->findOneBy(['user' => $currentUser]);
        $contractor = $contractorUser->getContractor();
        $builder
            ->add('from', EntityType::class, [
                'class' => 'AppBundle\Entity\ContractorUserHistory',
                'choice_label' => function (ContractorUserHistory $contractorUser) {
                    /** @var User $user */
                    $user = $contractorUser->getOwnerEntity()->getUser();
                    return $user->getFirstName() . ' ' . $user->getLastName() . ' ' . '-' . ' ' . $user->getEmail();
                },
                'required'    => true,
                'placeholder' => 'Choose Contractor User',
                 'query_builder' => function (EntityRepository $er) use ($contractor) {
                     /** @var QueryBuilder $query */
                     $query = $er->createQueryBuilder('cuh');
                     $query->where('cuh.contractor = :contractor')
                         ->setParameter('contractor', $contractor)
                         ->orderBy('cuh.dateSave', 'DESC')
                         ->groupBy('cuh.user');

                     return $query;
                 },
                'expanded' => false,
                'multiple' => false
            ])
            ->add("to", TextType::class, [
                "label" => "To",
                "data" => $formData["recipient"]
            ])
            ->add("emailSubject", TextType::class, [
                "label" => "Email Sybject"
            ])
            ->add("emailBody", TextareaType::class, [
                "label" => "Email Body"
            ])
            ->add("attachedFiles", ChoiceType::class, [
                "expanded" => true,
                "multiple" => true,
                'required' => false,
                "choices" => $formData["composedData"]["choices"]
            ])
            ->add('file', FileType::class, [
                "required" => false,
                "multiple" => true,
                "constraints" => [
                    new File([
                        'maxSize' => '10M',
                        'mimeTypes' => [
                            "image/png",
                            "image/jpeg",
                            "image/jpg",
                            "image/bmp",
                            "application/pdf",
                            "application/msword"
                        ],
                        'mimeTypesMessage' => 'Please use JPG, JPEG, PNG, PDF or DOC files only',
                        'maxSizeMessage' => 'The file is too large. Allowed maximum size is 10Mb',
                        'groups' => ['create', 'update']
                    ])
                ],
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'validation_groups' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return 'admin_bundle_file';
    }
}
