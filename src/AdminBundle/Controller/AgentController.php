<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Agent;
use AppBundle\Entity\EntityManager\AgentChannelManager;
use AppBundle\Entity\Repository\AgentChannelRepository;
use AppBundle\Entity\Repository\AgentRepository;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AgentController extends Controller
{
    use PaginateQueryTrait;

    /**
     * @param Agent $agent
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Agent $agent)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AgentChannelRepository $agentChannelRepository */
        $agentChannelRepository = $objectManager->getRepository('AppBundle:AgentChannel');
        /** @var AgentChannelManager $agentChannelManager */
        $agentChannelManager = $this->get('app.agent.channel.manager');
        /** @var array $divisionsInContacts */
        $divisionsInContacts = [];

        foreach ($agent->getContacts() as $contact) {
            foreach ($contact->getDivisions() as $division) {
                $divisionsInContacts[$contact->getId()][] = $division->getAlias();
            }
        }

        return $this->render(
            'AdminBundle:Agent:view.html.twig',
            [
                'agent' => $agent,
                'channels' => $agentChannelManager->getByDeviceCategory($agent),
                'quantityChannels' => $agentChannelRepository->getQuantityChannels($agent),
                'divisionsInContacts' => $divisionsInContacts
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AgentRepository $contractorRepository */
        $agentRepository = $objectManager->getRepository('AppBundle:Agent');

        $agentsList = $agentRepository->findAllWithPagination($request->get('page'));

        return $this->render('@Admin/Agent/list.html.twig', [
            'agentsList' => $agentsList,
        ]);
    }
}
