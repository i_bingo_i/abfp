<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\OpportunityType;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Security\FilterVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\OpportunityRepository;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\EntityManager\OpportunityManager;
use AppBundle\Entity\Repository\OpportunityTypeRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;

class OpportunityController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var OpportunityManager $opportunityManager */
        $opportunityManager = $this->get('app.opportunity.manager');
        /** @var OpportunityRepository $opportunityRepository */
        $opportunityRepository = $objectManager->getRepository('AppBundle:Opportunity');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');
        /** @var OpportunityTypeRepository $opportunityTypeRepository */
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        $opportunityTypeRepository = $objectManager->getRepository('AppBundle:OpportunityType');
        $filterParams = $request->query->all();
        $page = $request->query->get('page');
        $divisions = $deviceCategory->findAll();

        //TODO: виправити костиль
        //past RETEST type
        /** @var OpportunityType $opportunityTypeRetest */
        $opportunityTypeRetest = $opportunityTypeRepository->findOneBy(['alisa' => 'retest']);
        $filterParams['type'] = $opportunityTypeRetest->getAlisa();

        $checkOnCorrectFilterForCreatingRn =
            $this->isGranted(FilterVoter::CHECK_ON_CORRECT_FILTER_FOR_CREATING_RN, $filterParams);

        $res = $opportunityRepository->findByFilterWithPagination($filterParams, $page);
        $res['results'] = $opportunityManager->groupByAccount($res['results']);

        $quantitiesItems = $accountRepository->getQuantitiesWithFilters($filterParams);

        return $this->render(
            'AdminBundle:Opportunity:list.html.twig',
            [
                'opportunitySortByAccount' => $res,
                'quantitiesItems' => $quantitiesItems,
                'filterContent' =>$opportunityManager->getInfoForFilters(),
                'filterParams' => $filterParams,
                'divisions' => $divisions,
                'checkOnCorrectFilterForCreatingRn' => $checkOnCorrectFilterForCreatingRn
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listRepairsAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var OpportunityManager $opportunityManager */
        $opportunityManager = $this->get('app.opportunity.manager');
        /** @var OpportunityRepository $opportunityRepository */
        $opportunityRepository = $objectManager->getRepository('AppBundle:Opportunity');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');
        /** @var OpportunityTypeRepository $opportunityTypeRepository */
        $opportunityTypeRepository = $objectManager->getRepository('AppBundle:OpportunityType');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        $filterParams = $request->query->all();
        $page = $request->query->get('page');
        $divisions = $deviceCategory->findAll();

        /** @var OpportunityType $opportunityTypeRepair */
        $opportunityTypeRepair = $opportunityTypeRepository->findOneBy(['alisa' => 'repair']);
        $filterParams['type'] = $opportunityTypeRepair->getId();

        if (!array_key_exists('date', $filterParams)
            ||(empty($filterParams['date']['from']) || empty($filterParams['date']['to']))
        ) {
            $this->get('session')->getFlashBag()->add('error', '');
        }
        $error = !empty($this->get('session')->getFlashBag()->get('error')) ? false : true;

        $res = $opportunityRepository->findByFilterWithPagination($filterParams, $page);

        $res['results'] = $opportunityManager->groupByAccount($res['results']);

        $quantitiesItems = $accountRepository->getQuantitiesWithFilters($filterParams);

        return $this->render(
            'AdminBundle:Opportunity:listRepairs.html.twig',
            [
                'opportunitySortByAccount' => $res,
                'quantitiesItems' => $quantitiesItems,
                'filterContent' =>$opportunityManager->getInfoForFilters(),
                'divisions' => $divisions,
                'filterParams' => $filterParams,
                'error' => $error
            ]
        );
    }
}
