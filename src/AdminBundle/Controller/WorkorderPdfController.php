<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Services\Workorder\PDFManager;
use AppBundle\Entity\Workorder;

class WorkorderPdfController extends Controller
{
    /**
     * @param Workorder $workorder
     *
     * @return Response
     */
    public function viewPdfForDebugAction(Workorder $workorder)
    {
        /** @var PDFManager $woPdfManager */
        $woPdfManager = $this->get('workorder.pdf.manager');
        $pdfRenderedToHtml = $woPdfManager->renderToHtml($workorder);

        return new Response($pdfRenderedToHtml);
    }

    /**
     * @param Workorder $workorder
     * @return Response
     */
    public function viewAction(Workorder $workorder)
    {
        /** @var PDFManager $woPdfManager */
        $woPdfManager = $this->get('workorder.pdf.manager');

        $stringPdf = $woPdfManager->create($workorder);
        $fileName = $woPdfManager->getFileName($workorder);

        return new Response($stringPdf, 200, [
            "Content-Type" => "application/pdf",
            "Content-Disposition" => "inline; filename={$fileName}"
        ]);
    }
}
