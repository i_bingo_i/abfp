<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\WorkorderLetterType;
use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use AppBundle\Entity\Workorder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use WorkorderBundle\Services\GetLetterRecipientService;
use WorkorderBundle\Services\SendInvoiceViaEmail;
use WorkorderBundle\Services\WorkorderFilesComposer;

class SendInvoiceController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \exception
     */
    public function viaEmailForWorkorderAction(Workorder $workorder, Request $request)
    {
        /** @var WorkorderFilesComposer $workorderFilesComposer */
        $workorderFilesComposer = $this->get("workorder.files.composer.service");
        /** @var GetLetterRecipientService $getLetterRecipientService */
        $getLetterRecipientService = $this->get("workorder.get.letter.recipient.service");

        $composedData = $workorderFilesComposer->composeForSendInvoice($workorder);
        $letterRecipient = $getLetterRecipientService->getPaymentEmail($workorder);

        $form = $this->createForm(
            WorkorderLetterType::class,
            ["workorder" => $workorder, "composedData" => $composedData, "recipient" => $letterRecipient]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $letterFormSendViaEmailParser = $this->get("letter.form_send_via_email.parser");
            /** @var SendFormViaEmailDTO $sendInvoiceViaEmailDTO */
            $sendInvoiceViaEmailDTO = $letterFormSendViaEmailParser->parse($form->getData());
            /** @var SendInvoiceViaEmail $sendInvoiceViaEmailService */
            $sendInvoiceViaEmailService = $this->get("workorder.send_invoice_via_email.service");
            $sendInvoiceViaEmailService->sendInvoiceViaEmailProcess($sendInvoiceViaEmailDTO);

            return $this->redirectToRoute("admin_workorder_view", ["workorder" => $workorder->getId()]);
        }

        return $this->render('@Admin/Letter/emails/workorder/send_invoice_via_email.html.twig', [
            'form' => $form->createView(),
            'typeEmail' => 'sent_via_email',
            'type' => 'invoice',
            'workorder' => $workorder
        ]);
    }
}
