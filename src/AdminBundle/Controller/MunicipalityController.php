<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\MunicipalityType;
use AppBundle\Entity\Address;
use AppBundle\Entity\Agent;
use AppBundle\Entity\Department;
use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\DepartmentManager;
use AppBundle\Entity\EntityManager\MunicipalityManager;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\AgentRepository;
use AppBundle\Entity\Repository\DepartmentRepository;
use AppBundle\Entity\Repository\DepartmentChannelRepository;
use AppBundle\Services\Search;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Municipality;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\ChannelNamedRepository;

class MunicipalityController extends Controller
{
    /**
     * @param Request $request
     * @param Municipality $municipality
     * @return Response
     */
    public function viewAction(Request $request, Municipality $municipality) : Response
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var DepartmentRepository $departmentRepository */
        $departmentRepository = $objectManager->getRepository('AppBundle:Department');
        /** @var DepartmentChannelRepository $departmentChannelRepository */
        $departmentChannelRepository = $objectManager->getRepository('AppBundle:DepartmentChannel');
        /** @var AgentRepository $agentRepository */
        $agentRepository = $objectManager->getRepository('AppBundle:Agent');
        /** @var ChannelNamedRepository $channelNamedRepository */
        $channelNamedRepository = $objectManager->getRepository('AppBundle:ChannelNamed');
        /** @var DepartmentManager $departmentManager */
        $departmentManager = $this->get('app.department.manager');
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->get('app.department_channel.manager');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');

        $allChannelNamed = $channelNamedRepository->findAll();

        $departmentChannelWithFeesWillVary = [];
        //get departments of Municipality $municipality
        $municipalityDepartments = $departmentRepository->findBy(['municipality' => $municipality]);
        foreach ($municipalityDepartments as $key => $department) {
            $municipalityDepartments[$key] = $departmentManager->serialize($department);
            $departmentChannelWithFeesWillVary[$department->getId()] =
                $departmentChannelManager->checkIfDepartmentChannelsHaveFeesWillVary($department);
        }

        $agentChannels = [];
        /** @var  Department $department */
        foreach ($municipalityDepartments as $department) {
            if (!empty($department->getAgent())) {
                $agentChannels[$department->getId()] = $departmentChannelRepository->findBy([
                    "ownerAgent" => $department->getAgent(),
                    "department" => $department,
                    "deleted" => false
                ]);
            }
        }

        $page = $request->query->get('page') ? $request->query->get('page') : 1;

        $res = $accountRepository->getAccountsByMunicipalityWithPagination($municipality, $page);

        return $this->render(
            '@Admin/Municipality/detail.html.twig',
            [
                'municipality' => $municipality,
                'accountsResults' =>$res,
                'municipalityDepartments' => $municipalityDepartments,
                'agents' => $agentRepository->findAll(),
                'agentChannels' => $agentChannels,
                'channelNamed' => $allChannelNamed,
                'departmentChannelWithFeesWillVary' => $departmentChannelWithFeesWillVary,
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request) : Response
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var MunicipalityRepository $municipalityRepository */
        $municipalityRepository = $objectManager->getRepository('AppBundle:Municipality');
        /** @var Search $searchService */
        $searchService = $this->get('app.search');

        return $this->render(
            'AdminBundle:Municipality:list.html.twig',
            [
                'searchResult' => $searchService->searchMunicipality($request->query->all()),
                'quantity' => $municipalityRepository->getCount()
            ]
        );
    }

    /**
     * @param Department $department
     * @param Agent $agent
     * @param Request $request
     * @return RedirectResponse
     */
    public function addAgentAction(Department $department, Agent $agent, Request $request)
    {
        /** @var MunicipalityManager $municipalityManager */
        $municipalityManager = $this->get('app.municipality.manager');

        $municipalityManager->attachAgentToDepartment($department, $agent, $this->getUser());

        $redirectUrl = $request->headers->get('referer')
            ? $this->redirect($request->headers->get('referer'))
            : $this->redirectToRoute('admin_municipality_view', [
                'municipality' => $department->getMunicipality()->getId()
            ]);

        return $redirectUrl;
    }

    /**
     * @param Request $request
     * @param Department $department
     * @return RedirectResponse
     */
    public function deleteAgentAction(Request $request, Department $department)
    {
        /** @var MunicipalityManager $municipalityManager */
        $municipalityManager = $this->get('app.municipality.manager');

        $municipalityManager->deleteAgent($department, $this->getUser());

        $redirectUrl = $request->headers->get('referer')
            ? $this->redirect($request->headers->get('referer'))
            : $this->redirectToRoute('admin_municipality_view', [
                'municipality' => $department->getMunicipality()->getId()
            ]);

        return $redirectUrl;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        /** @var MunicipalityManager $municipalityManager */
        $municipalityManager = $this->get('app.municipality.manager');
        /** @var DepartmentManager $departmentManager */
        $departmentManager = $this->get('app.department.manager');
        /** @var Municipality $newMunicipality */
        $newMunicipality = $municipalityManager->prepareCreate();

        $form = $this->createForm(MunicipalityType::class, $newMunicipality, [
            'validation_groups' => ['create']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $municipality = $municipalityManager->create($newMunicipality);
            $departmentManager->createForNewMunicipality($municipality);

            return $this->redirectToRoute('admin_municipality_view', ['municipality' => $municipality->getId()]);
        }

        return $this->render('@Admin/Municipality/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
