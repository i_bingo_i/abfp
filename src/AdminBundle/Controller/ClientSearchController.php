<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Services\Search;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\ContactPersonRepository;
use AppBundle\Entity\Repository\CompanyRepository;

class ClientSearchController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        /** @var Search $searchService */
        $searchService = $this->get('app.search');
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');
        /** @var ContactPersonRepository $contactPersonRepository */
        $contactPersonRepository = $objectManager->getRepository('AppBundle:ContactPerson');
        /** @var CompanyRepository $companyRepository */
        $companyRepository = $objectManager->getRepository('AppBundle:Company');

        $searchOptions = $request->query->all();

        return $this->render('@Admin/Client/search.html.twig', [
            'accountCount' => $accountRepository->getCount(),
            'contactPersonCount' => $contactPersonRepository->getCount(),
            'companyCount' => $companyRepository->getCount(),
            'searchResult' => $searchService->searchEverywhere($searchOptions)
        ]);
    }
}
