<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\RepairDeviceInfoManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Message;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\DeviceHistoryRepository;
use AppBundle\Entity\Repository\DeviceRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Account\Authorizer;
use AppBundle\Services\AccountContactPersonCompose;
use AppBundle\Services\Helper;
use AppBundle\Services\PreviousPageService;
use AppBundle\Services\Proposal\Division;
use WorkorderBundle\Services\Grouping;
use WorkorderBundle\Services\IncludeService;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\EntityManager\DeviceHistoryManager;
use AppBundle\Entity\Device;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AdminBundle\Form\FormDeviceType;
use AdminBundle\Form\RepairWorkorderType;
use AppBundle\Entity\Repository\DeviceNamedRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Security\WorkOrderVoter;
use WorkorderBundle\Services\Creator;
use AppBundle\Security\ErrorByAccountVoter;

class DeviceController extends Controller
{
    /**
     * @param Request $request
     * @param Account $account
     * @param DeviceNamed $deviceNamed
     * @return Response
     */
    public function createAction(Request $request, Account $account, DeviceNamed $deviceNamed)
    {
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var DeviceHistoryManager $deviceHistoryManager */
        $deviceHistoryManager = $this->get('app.device_history.manager');

        $this->denyAccessUnlessGranted('create_device', $account);

        $newDevice = $deviceManager->createByDeviceNamed($deviceNamed);
        $newDevice->setAccount($account);

        $form = $this->createForm(
            FormDeviceType::class,
            ['device' => $newDevice],
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $devicePhoto = $form['photo']->getData();
            if ($devicePhoto) {
                $newDevice = $deviceManager->saveImageFromFrom($newDevice, $devicePhoto);
            }

            $deviceManager->create($newDevice);

            // TODO: Перенести це в DeviceListener PostCreate
//            $messageManager->isMunicipalityComplianceChannelByAccount($newDevice->getAccount());
            $deviceHistoryManager->createDeviceHistoryItem($newDevice);

            return $this->redirectToRoute('admin_device_view', [
                'device' => $newDevice->getId()
            ]);
        }

        return $this->render('@Admin/Device/create.html.twig', [
            'form' => $form->createView(),
            'account' => $account,
            'deviceNamed' => $deviceNamed
        ]);
    }

    /**
     * @param Device $parent
     * @param DeviceNamed $deviceNamed
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createSubDeviceAction(Device $parent, DeviceNamed $deviceNamed, Request $request)
    {
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var DeviceHistoryManager $deviceHistoryManager */
        $deviceHistoryManager = $this->get('app.device_history.manager');

        /** @var Device $newDevice */
        $newDevice = $deviceManager->createByDeviceNamed($deviceNamed, $parent);
        $newDevice->setAccount($parent->getAccount());
        $form = $this->createForm(
            FormDeviceType::class,
            ['device' => $newDevice],
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $devicePhoto = $form['photo']->getData();
            if ($devicePhoto) {
                $newDevice = $deviceManager->saveImageFromFrom($newDevice, $devicePhoto);
            }

            $deviceManager->create($newDevice);
            // TODO: Перенести це в DeviceListener PostCreate
            $deviceHistoryManager->createDeviceHistoryItem($newDevice);
            
            $redirectToId = $newDevice->getId();
            $parent = $newDevice->getParent();

            if (!empty($parent)) {
                $redirectToId = $parent->getId();
            }

            return $this->redirectToRoute('admin_device_view', [
                'device' => $redirectToId
            ]);
        }

        return $this->render('@Admin/Device/create.html.twig', [
            'form' => $form->createView(),
            'account' => $parent->getAccount(),
            'deviceNamed' => $deviceNamed
        ]);
    }

    /**
     * @param Device $device
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updateAction(Device $device, Request $request)
    {
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var DeviceHistoryManager $deviceHistoryManager */
        $deviceHistoryManager = $this->get('app.device_history.manager');

        $formUpdateDevice = $this->createForm(
            FormDeviceType::class,
            ['device' => $device],
            ['validation_groups' => ['update']]
        );

        $formUpdateDevice->handleRequest($request);

        if ($formUpdateDevice->isSubmitted() && $formUpdateDevice->isValid()) {
            $devicePhoto = $formUpdateDevice['photo']->getData();
            $deletePhoto = $request->request->get('forceDeleteImage');

            if ($deletePhoto == 1 and $device->getPhoto()) {
                $device = $deviceManager->removeDevicePhoto($device, $this->get('kernel')->getRootDir());
            }

            if ($devicePhoto) {
                $device = $deviceManager->saveImageFromFrom($device, $devicePhoto);
            }
            $deviceManager->save($device);

            $deviceHistoryManager->createDeviceHistoryItem($device);

            return $this->redirectToRoute('admin_device_view', [
                'device' => $device->getId()
            ]);
        }

        return $this->render('@Admin/Device/update.html.twig', [
            'form' => $formUpdateDevice->createView(),
            'device' => $device
        ]);
    }


    /**
     * Render detail information about device by $id
     * @param Device $device
     * @return Response
     */
    public function viewAction(Device $device)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var DeviceRepository $deviceRepository */
        $deviceRepository = $objectManager->getRepository('AppBundle:Device');
        /** @var DeviceNamedRepository $accountDeviceNamedRepository */
        $accountDeviceNamedRepository = $objectManager->getRepository('AppBundle:DeviceNamed');
        /** @var DeviceHistoryRepository $deviceHistoryRepository */
        $deviceHistoryRepository = $objectManager->getRepository('AppBundle:DeviceHistory');
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var ContractorServiceRepository $contractorServiceRepository */
        $contractorServiceRepository = $objectManager->getRepository('AppBundle:ContractorService');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $objectManager->getRepository('AppBundle:Service');
        /** @var AccountContactPersonRepository $accountContactPersonRepository */
        $accountContactPersonRepository = $objectManager->getRepository('AppBundle:AccountContactPerson');
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository('AppBundle:Workorder');
        /** @var Authorizer $accountAuthorizerService */
        $accountAuthorizerService = $this->get('app.account_authorizer.service');
        /** @var Helper $helper */
        $helper = $this->get('app.helper');
        /** @var Grouping $woGroupingService */
        $woGroupingService = $this->get('workorder.grouping.service');
        /** @var Account $account */
        $account = $device->getAccount();
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $device->getNamed()->getCategory();
        /** @var Account $ownerAccount */
        $ownerAccount = $accountAuthorizerService->getAccountWithAuthorizerForDivision($account, $deviceCategory);
        /** @var AccountContactPersonCompose $accountContactPersonCompose */
        $accountContactPersonCompose = $this->get('app.account_contact_person_compose');
        /** @var AccountContactPerson[] $accountContactPersons */
        $accountContactPersons = $accountContactPersonRepository->findBy([
            'account' => $ownerAccount,
            'deleted' => false
        ]);

        $helper->removeSessionByCondition('previousPage');

        $contractorServices = $contractorServiceRepository->findAll();
        $deviceHistory = $deviceHistoryRepository->findOneBy(
            ['ownerEntity' => $device->getId()],
            ['id' => 'DESC']
        );

        return $this->render('@Admin/Device/view.html.twig', [
            'device' => $deviceManager->serialize($device),
            'subDevicesNamed' => $accountDeviceNamedRepository->findBy([
                'parent' => $device->getNamed()
            ]),
            'inspections' => $serviceRepository->getInspectionsByDevice($device),
            'repairs' => $serviceRepository->getRepairsByDevice($device),
            'deviceHistory' => $deviceHistory,
            'parentsDevice' => $deviceRepository->getParents($device),
            'contractorServices' => $contractorServices,
            'quantityDevices' => $serviceRepository->getQuantityNotDeletedServicesByDevice($device),
            'contacts' => $accountContactPersonCompose->compose($accountContactPersons),
            'ownerAccount' => $ownerAccount,
            'suitableWOsForCreation' =>
                $workorderRepository->findSuitableWOsForCreating($ownerAccount, $deviceCategory),
            'suitableWOsForCreationBySubDevices' => $woGroupingService->byDeviceUnderAccount($ownerAccount, $device)
        ]);
    }

    /**
     * Deleting devices and sub devices
     * @param Device $device
     * @return Response
     */
    public function deleteAction(Device $device)
    {
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        $deviceManager->delete($device, $this->getUser());

        $deviceManager->detachAlarmPanel($device);

        return $this->redirectToRoute('admin_device_view', ['device' => $device->getId()]);
    }

    /**
     * @param Device $device
     * @param Request $request
     * @return Response
     */
    public function addAlarmPanelAction(Device $device, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var DeviceRepository $deviceRepository */
        $deviceRepository = $objectManager->getRepository('AppBundle:Device');
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');

        /** @var array Device $alarmPanels */
        $alarmPanels = $deviceRepository->getAlarmPanels($device->getAccount());
        /** @var Device $alarmPanelId */
        $alarmPanelId = $deviceRepository->findOneBy(['id' => $request->query->get('panel')]);

        if ($alarmPanelId) {
            $device->setAlarmPanel($alarmPanelId);
            $deviceManager->save($device);

            return $this->redirectToRoute('admin_device_view', ['device' => $device->getId()]);
        }

        return $this->render(
            '@Admin/Device/alarm_panels.html.twig',
            ['alarmPanels' => $alarmPanels, 'rootDevice' => $device]
        );
    }

    /**
     * @param Device $device
     * @return RedirectResponse
     */
    public function detachAlarmPanelAction(Device $device)
    {
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');

        if ($device->getAlarmPanel()) {
            $device->setAlarmPanel();
            $deviceManager->save($device);
        }

        return $this->redirectToRoute('admin_device_view', ['device' => $device->getId()]);
    }

    /**
     * @param Device $device
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function addRepairToWorkorderAction(Device $device, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var MunicipalityRepository $municipalityRepository */
        $municipalityRepository = $objectManager->getRepository('AppBundle:Municipality');
        /** @var Division $divisionService */
        $divisionService = $this->get('app.division.service');
        /** @var IncludeService $woIncludeService */
        $woIncludeService = $this->get("workorder.include_service.service");
        /** @var RepairDeviceInfoManager $repairDeviceInfoManager */
        $repairDeviceInfoManager = $this->get('app.repair_device_info.manager');
        /** @var Authorizer $accountAuthorizerService */
        $accountAuthorizerService = $this->get('app.account_authorizer.service');
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->get('app.department_channel.manager');
        /** @var \AppBundle\Services\Coordinates\Helper $coordinatesHelper */
        $coordinatesHelper = $this->get('app.coordinates_helper.service');
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository("AppBundle:Workorder");
        /** @var Creator $workorderCreatorService */
        $workorderCreatorService = $this->get('workorder.creator.service');
        /** @var PreviousPageService $previousPageService */
        $previousPageService = $this->get('app.previous_page.service');
        /** @var DeviceCategory $deviceCategory */
        $deviceCategory = $device->getNamed()->getCategory();
        /** @var Account $account */
        $account = $device->getAccount();
        /** @var Account $ownerAccount */
        $ownerAccount = $params['ownerAccount'] = $accountAuthorizerService->getAccountWithAuthorizerForDivision(
            $account,
            $deviceCategory
        );
        /** @var Municipality $municipality */
        $municipality = $account->getMunicipality();
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        // added case when no session previos page and no referer and add previous page - service's device
        $previousPage = $previousPageService->getByRequestAndDevice($request, $device);

        if (!$this->isGranted(ErrorByAccountVoter::ADD_REPAIR_TO_WO, $params)) {
            return $this->redirectToRoute('admin_account_get', [
                'accountId' => $ownerAccount->getId()
            ]);
        }

        $form = $this->createForm(
            RepairWorkorderType::class,
            [
                'workorders' => $workorderRepository->findSuitableWOsForCreating($ownerAccount, $deviceCategory),
                'repairDeviceInfo' => new RepairDeviceInfo(),
                'device' => $device,
                'account' => $ownerAccount
            ],
            ['validation_groups' => ['create']]
        );


        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $addedRepairsServicesNamed = $form['serviceName']->getData()['name']->toArray();
            $addedUnderOportunityServices = $form['workorder']['services']->getData()->toArray();
            /** @var RepairDeviceInfo $repairDeviceInfo */
            $repairDeviceInfo = $form['deviceInfo']->getData();
            $workorderId = $form['workorderId']->getData();

            if ($workorderId) {
                /** @var Workorder $workorder */
                $workorder = $workorderRepository->find($workorderId);
            } else {
                /** @var Workorder $workorder */
                $workorder = $workorderCreatorService->create($ownerAccount, $deviceCategory, $user);
            }

            if (!$this->isGranted(WorkOrderVoter::CAN_NOT_ADD_SERVICES_TO_FROZEN_WO, $workorder)) {
                return $this->redirect($request->headers->get('referer'));
            }
            $services = $woIncludeService->addRepairs($addedRepairsServicesNamed, $device);
            // we must merging added services with select Under Opportunity services
            $services =
                $woIncludeService->mergingServicesNamed($services, $addedUnderOportunityServices);
            /** @var Workorder $workorder */
            $workorder = $woIncludeService->addServices(
                $device,
                $services,
                $repairDeviceInfo,
                $workorder
            );

            //set coordinates by Account Address
            $coordinatesHelper->refreshByAccount($workorder->getAccount());
            //create logs for repair services
            $woIncludeService->creatingRepairServicesLogs($workorder, $services);
            //remove RepairDeviceInfo after added to WORepairInfo
            $repairDeviceInfoManager->remove($repairDeviceInfo);

            return $this->redirect($previousPage);
        }

        $serviceNames = $serializer->serialize(
            $serviceManager->getNonFrequencyServiceNames($device),
            'json'
        );

        return $this->render('@Admin/Device/add_repair_to_workorder.html.twig', [
            'device' => $deviceManager->serialize($device),
            'serviceNames' => $serviceNames,
            'form' => $form->createView(),
            'fee' => $municipalityRepository->getMunicipalityFee(
                $device->getAccount(),
                $divisionService->temporaryForAlarmAndPlumbingDivisions($device->getNamed()->getCategory())
            ),
            'previousPage' =>$previousPage,
            'isNotSelectedServiceName' => $deviceManager->isExistNotSelectedService($device),
            'defaultDepartmentChannelWithFeesWillVary' =>
                $departmentChannelManager->checkIfDefaultDepartmentChannelHasFeesWillVary(
                    $municipality,
                    $deviceCategory
                ),
            'workorders' => $workorderRepository->findSuitableWOsForCreating($ownerAccount, $deviceCategory)
        ]);
    }

    /**
     * @param Device $device
     * @return BinaryFileResponse|Response
     */
    public function viewImageAction(Device $device)
    {
        $appPath = $this->getParameter('kernel.root_dir');
        $uploadsPath = realpath($appPath . '/../web');
        $fullImagePath = "$uploadsPath/{$device->getPhoto()}";

        if (!file_exists($fullImagePath)) {
            return new Response('Image no exist');
        }

        return new BinaryFileResponse($fullImagePath);
    }
}
