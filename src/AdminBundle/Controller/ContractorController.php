<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Contractor;
use AppBundle\Entity\ContractorType;
use AppBundle\Entity\Repository\ContractorRepository;
use AppBundle\Entity\Repository\ContractorServiceRepository;
use AppBundle\Entity\Repository\ContractorTypeRepository;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Services\Search;

class ContractorController extends Controller
{
    /**
     * @param Contractor $contractor
     * @return Response
     */
    public function viewAction(Contractor $contractor)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ContractorServiceRepository $contractorServiceRepository */
        $contractorServiceRepository = $objectManager->getRepository('AppBundle:ContractorService');
        /** @var DeviceCategoryRepository $deviceCategoryRepository */
        $deviceCategoryRepository = $objectManager->getRepository('AppBundle:DeviceCategory');
        /** @var ContractorUserRepository $contractorUserRepository */
        $contractorUserRepository = $objectManager->getRepository('AppBundle:ContractorUser');

        $quantityEachContractorServices = [];
        $allDeviceCategory = $deviceCategoryRepository->findAll();
        $contractorServices = $contractorServiceRepository->findBy(
            ['contractor' => $contractor->getId()],
            ['deviceCategory'=>'ASC']
        );

        foreach ($allDeviceCategory as $oneCategory) {
            $quantityEachContractorServices[$oneCategory->getAlias()] = $contractorServiceRepository
                ->getCountServices($contractor, $oneCategory->getAlias());
        }

        $contractorUsers = $contractorUserRepository->findBy(['contractor' => $contractor, 'deleted' => false]);

        return $this->render('@Admin/Contractor/view.html.twig', [
            'contractor' => $contractor,
            'contractorUsers' => $contractorUsers,
            'quantityEachServices' => $quantityEachContractorServices,
            'quantityAllServices' => count($contractorServices),
            'services' => $contractorServices,
            'allDeviceCategory' => $allDeviceCategory
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ContractorRepository $contractorRepository */
        $contractorRepository = $objectManager->getRepository('AppBundle:Contractor');
        /** @var ContractorTypeRepository $contractorTypeRepository */
        $contractorTypeRepository = $objectManager->getRepository('AppBundle:ContractorType');
        /** @var Search $searchService */
        $searchService = $this->get('app.search');

        $contractorTypes = [];
        /** @var ContractorType $type */
        foreach ($contractorTypeRepository->findAll() as $type) {
            $contractorTypes[$type->getAlias()] = $type;
        }

        return $this->render('@Admin/Contractor/list.html.twig', [
            'partnersCount' => $contractorRepository->getCountByType($contractorTypes['partner']),
            'competitorsCount' => $contractorRepository->getCountByType($contractorTypes['competitor']),
            'contractorTypes' => $contractorTypes,
            'searchResult' => $searchService->searchContractorExtended($request->query->all())
        ]);
    }
}
