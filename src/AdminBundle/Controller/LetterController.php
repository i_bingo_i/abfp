<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\EntityManager\LetterManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\Letter;
use AppBundle\Services\Proposal\Freeze;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use AdminBundle\Form\LetterType;
use AppBundle\Entity\Proposal;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class LetterController extends Controller
{
    /**
     * @param Letter $letter
     * @return Response
     */
    public function getAction(Letter $letter)
    {
        if (!$letter) {
            throw new HttpException("404", "Letter not found");
        }
        $viewParams = ['letter' => $letter];

        return $this->render('@Admin/Letter/detail.html.twig', $viewParams);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Twig\Error\Error
     */
    public function sendAction(Proposal $proposal, Request $request)
    {
        $this->denyAccessUnlessGranted('mail_email_action', $proposal);
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var LetterManager $letterManager */
        $letterManager = $this->get('app.letter.manager');
        /** @var Freeze $freezeProposalService */
        $freezeProposalService = $this->get('app.proposal.freeze.service');
        $newLetter = $letterManager->prepareLetter($proposal);

        $formSendMail = $this->createForm(
            LetterType::class,
            $newLetter,
            ['validation_groups' => ['create']]
        );
        $formSendMail->handleRequest($request);

        $proposalTypeAlias = $proposal->getType()->getAlias();

        if ($formSendMail->isSubmitted() && $formSendMail->isValid()) {
            if (!$proposal->getIsFrozen()) {
                $proposal = $freezeProposalService->freezing($proposal);
            }

            $newLetter->setAttachment($proposal->getReport());
            $newLetter = $letterManager->sendEmail(
                $newLetter,
                'sent_via_email_' . $proposalTypeAlias . '_notice.html.twig',
                $proposal->getStatus()
            );

            if (!$newLetter) {
                $this->get('session')->getFlashBag()->add('failed', 'All emails are not valid');
                return $this->redirect($request->headers->get('referer'));
            }

            $proposalManager->sendViaEmail($proposal, $newLetter);

            $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";

            return $this->redirectToRoute("$proposalRoute", ['proposal' => $proposal->getId()]);
        }

        $emailName = $proposalTypeAlias . '/create_' . $proposalTypeAlias . '_notice_' . $proposal->getDivision()->getAlias();
        return $this->render('@Admin/Letter/emails/' . $emailName . '.html.twig', [
            'form' => $formSendMail->createView(),
            'typeEmail' => 'sent_via_email'
        ]);
    }

    /**
     * @param Proposal $proposal
     * @return RedirectResponse
     * @throws \Twig\Error\Error
     */
    public function mailAction(Proposal $proposal)
    {
        $this->denyAccessUnlessGranted('mail_email_action', $proposal);
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var Freeze $freezeProposalService */
        $freezeProposalService = $this->get('app.proposal.freeze.service');

        if (!$proposal->getIsFrozen()) {
            $proposal = $freezeProposalService->freezing($proposal);
        }
        $proposalManager->sendViaMail($proposal);

        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";

        return $this->redirectToRoute("$proposalRoute", ['proposal' => $proposal->getId()]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function emailReminderAction(Proposal $proposal, Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var LetterManager $letterManager */
        $letterManager = $this->get('app.letter.manager');
        /** @var Freeze $freezeProposalService */
        $freezeProposalService = $this->get('app.proposal.freeze.service');
        $newLetter = $letterManager->prepareLetter($proposal);

        $formSendMail = $this->createForm(
            LetterType::class,
            $newLetter,
            ['validation_groups' => ['create']]
        );

        $formSendMail->handleRequest($request);

        if ($formSendMail->isSubmitted() && $formSendMail->isValid()) {
            if (!$proposal->getIsFrozen()) {
                $proposal = $freezeProposalService->freezing($proposal);
            }
            $newLetter = $letterManager->sendEmail(
                $newLetter,
                'email_reminder_sent_retest_notice.html.twig',
                $proposal->getStatus()
            );
            $proposalManager->sendEmailReminder($proposal, $newLetter);

            $proposalTypeAlias = $proposal->getType()->getAlias();
            $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";

            return $this->redirectToRoute("$proposalRoute", ['proposal' => $proposal->getId()]);
        }

        $emailName = 'retest_notice_' . $proposal->getDivision()->getAlias();
        return $this->render('@Admin/Letter/reminderEmails/' . $emailName . '.html.twig', [
            'form' => $formSendMail->createView(),
            'typeEmail' => 'email_reminder_sent'
        ]);
    }
}

