<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\DeviceNamedType;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\EntityManager\DeviceNamedManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DeviceNamedController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        /** @var DeviceNamedManager $deviceNamedManager */
        $deviceNamedManager = $this->get('app.device_named.manager');

        $newDeviceNamed = new DeviceNamed();
        $form = $this->createForm(
            DeviceNamedType::class,
            $newDeviceNamed,
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $deviceNamedManager->create($newDeviceNamed);

            return $this->redirectToRoute('admin_account_search');
        }

        return $this->render('@Admin/DeviceNamed/create_named.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
