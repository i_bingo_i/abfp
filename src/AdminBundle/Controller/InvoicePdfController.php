<?php

namespace AdminBundle\Controller;

use AppBundle\Services\PdfManagers\PDFInvoiceManager;
use InvoiceBundle\Entity\Invoices;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

class InvoicePdfController extends Controller
{
    /**
     * @param $name
     *
     * @return BinaryFileResponse
     */
    public function viewPdfAction($name)
    {
        $appPath = $this->getParameter('kernel.root_dir');
        $uploadsPath = realpath($appPath . '/../web/uploads/invoices');

        return new BinaryFileResponse("$uploadsPath/$name");
    }

    /**
     * @param $division
     * @return Response
     */
    public function viewPdfForDebugAction($division)
    {
        $template = "AdminBundle:Invoice:pdf/index-{$division}.html.twig";

        return $this->render($template, [
            'assetsPackage' => null
        ]);
    }
}