<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ChangeDueDateType;
use AppBundle\Services\Service\ChangerDueDateService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller
{
    /**
     * @return Response
     */
    public function dashboardAction()
    {
        return $this->render('@Admin/Dashboard/index.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function changeDueDateAction(Request $request)
    {
        /** @var ChangerDueDateService $changeDueDateService */
        $changeDueDateService = $this->get('app.change_due_date.service');
        $success = null;
        $yearFromAndToEqual = false;

        $form = $this->createForm(ChangeDueDateType::class, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()['yearTo'] != $form->getData()['year']) {
                $success = $changeDueDateService->changeInspectionDueDate($form->getData());
            } else {
                $yearFromAndToEqual = true;
            }
        }

        return $this->render('@Admin/Dashboard/change_due_date.html.twig',
            [
                'form' => $form->createView(),
                'currentYear' => date('Y'),
                'success' => $success,
                'failMessage' => $yearFromAndToEqual
            ]
        );
    }
}
