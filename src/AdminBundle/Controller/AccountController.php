<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\AccountHistory;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\EntityManager\AddressManager;
use AppBundle\Entity\EntityManager\MunicipalityManager;
use AppBundle\Entity\EntityManager\ContractorUserManager;
use AppBundle\Entity\EntityManager\OpportunityManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\EntityManager\ProposalStatusManager;
use AppBundle\Entity\Repository\AccountHistoryRepository;
use AppBundle\Entity\Repository\ClientTypeRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\ProposalRepository;
use AppBundle\Entity\Repository\ProposalStatusRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Repository\OpportunityRepository;
use AppBundle\Entity\Repository\OpportunityTypeRepository;
use AppBundle\Services\Helper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\Account;
use AppBundle\Entity\Company;
use AdminBundle\Form\AccountType;
use AppBundle\Entity\AccountType as AccountTypeEntity;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\AccountTypeRepository;
use AppBundle\Entity\Repository\AccountRepository;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;
use AppBundle\Entity\EntityManager\AccountHistoryManager;
use AppBundle\Services\Search;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use AppBundle\Events\ParentAccountSetEvent;
use AppBundle\Events\ParentAccountUnsetEvent;
use AppBundle\Entity\EntityManager\WorkorderManager;
use WorkorderBundle\Services\Grouping;

class AccountController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountTypeRepository $accountTypeRepository */
        $accountTypeRepository = $objectManager->getRepository('AppBundle:AccountType');
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        /** @var AddressManager $addressManager */
        $addressManager = $this->get('app.address.manager');

        // TODO: Винести код створення нового акаунта в фабрику
        $newAccount = new Account();
        $accountTypeSite = $accountTypeRepository->findOneBy(['alias' => 'account']);
        $newAccount->setType($accountTypeSite);
        $newAccount = $accountManager->setAddressForAccount($newAccount);

        $form = $this->createForm(
            AccountType::class,
            $newAccount,
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $accountManager->create($newAccount);
            return $this->redirectToRoute('admin_account_get', ['accountId' => $newAccount->getId()]);
        }

        return $this->render('@Admin/Account/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $parentId
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAndSetParentAction($parentId, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountTypeRepository $accountTypeRepository */
        $accountTypeRepository = $objectManager->getRepository('AppBundle:AccountType');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        /** @var AddressManager $addressManager */
        $addressManager = $this->get('app.address.manager');

        /** @var AccountType $accountTypeSite */
        $accountTypeSite = $accountTypeRepository->findOneBy(['alias' => 'account']);
        /** @var AccountType $accountTypeGroupAccount */
        $accountTypeGroupAccount = $accountTypeRepository->findOneBy(['alias' => 'group account']);

        /** @var Account $groupAccount */
        $groupAccount = $accountRepository->findOneBy([
            'id' => $parentId,
            'type' => $accountTypeGroupAccount
        ]);

        if (!$groupAccount) {
            throw new HttpException(404, "Parent account not found");
        }

        // TODO: Винести код створення нового акаунта в фабрику
        /** @var Account $newAccount */
        $newAccount = new Account();
        $newAccount->setParent($groupAccount);
        $newAccount->setType($accountTypeSite);
        $newAccount = $accountManager->setAddressForAccount($newAccount);

        $form = $this->createForm(AccountType::class, $newAccount, [
            'validation_groups' => ['create']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $accountManager->create($newAccount);
            return $this->redirectToRoute('admin_account_get', ['accountId' => $groupAccount->getId()]);
        }

        return $this->render('@Admin/Account/create_and_set_parent.html.twig', [
            'form' => $form->createView(),
            'groupAccount' => $groupAccount
        ]);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updateAction(Account $account, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $account);
        /** @var AccountManager $accountManager */
        $accountManager = $this->get("app.account.manager");
        /** @var Account $account */
        $account = $accountManager->setAddressForAccount($account);

        $formUpdateAccount = $this->createForm(
            AccountType::class,
            $account,
            ['validation_groups' => ['update']]
        );

        $formUpdateAccount->handleRequest($request);

        if ($formUpdateAccount->isSubmitted() && $formUpdateAccount->isValid()) {
            $accountManager->update($account);

//            $accountHistoryManager->createAccountHistoryItem($account);
//            $this->get('app.message.manager')->isMunicipalityComplianceChannelByAccount($account);
            return $this->redirectToRoute('admin_account_get', [
                'accountId' => $account->getId()
            ]);
        }

        return $this->render('@Admin/Account/update.html.twig', [
            'form' => $formUpdateAccount->createView()
        ]);
    }

    //TODO: rename parameter $accountId to $account
    /**
     * @param Account $accountId
     * @return Response
     */
    public function getAction(Account $accountId)
    {
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        /** @var Helper $helper */
        $helper = $this->get('app.helper');
        /** @var MunicipalityManager $municipalityManager */
        $municipalityManager = $this->get('app.municipality.manager');
        /** @var AccountHistoryRepository $accountHistoryRepository */
        $accountHistoryRepository = $this->get('app.entity_manager')->getRepository('AppBundle:AccountHistory');
        /** @var Grouping $workorderGroupingService */
        $workorderGroupingService = $this->get('workorder.grouping.service');

        $helper->removeSessionByCondition('previousPage');

        $accountManager->checkDefaultDepartmentChanel($accountId);

        $viewParams = $accountManager->getAccountViewParameters($accountId);
        $this->getUser();

        $viewParams['accountHistory'] = $accountHistoryRepository->findOneBy(
            ['ownerEntity' => $accountId],
            ['id' => 'DESC']
        );

        if ($accountId->getType()->getAlias() == 'account') {
            $viewParams['municipalityProcessingFee'] = $municipalityManager
                ->prepareMunicipalityAndProcessingFeeByDivision($accountId);
            $viewParams['account'] = $accountManager->serializeByDeviceCategories($accountId);
            $viewParams['ownerAccount'] = $viewParams['account'];
            $viewParams['groupAccountPageLabel'] = false;
            $viewParams['workordersByAccount'] = $workorderGroupingService->byCategoryUnderAccount($accountId);
            return $this->render('@Admin/Account/view_site_account.html.twig', $viewParams);
        }
        $viewParams['municipalityProcessingFee'] = $municipalityManager
            ->prepareMunicipalityAndProcessingFeeByDivision($accountId);
        $viewParams['account'] = $accountManager->serializeGroupAccountByDeviceCategories($accountId);
        $viewParams['ownerAccount'] = $viewParams['account'];
        $viewParams['groupAccountPageLabel'] = true;
        $viewParams['workordersByAccount'] = $workorderGroupingService->byCategoryUnderAccount($accountId);

        return $this->render('@Admin/Account/view_group_account.html.twig', $viewParams);
    }

    /**
     * @param Account $account1
     * @param Account $account2
     * @return Response
     */
    public function getDistanceBetweenAddressesAction(Account $account1, Account $account2) : Response
    {
        $address1 = $account1->getAddress();
        $address2 = $account2->getAddress();

        $providers = $this->get('bazinga_geocoder.geocoder')->getProviders();
        $googleMapsProvider = $providers['google_maps'];

        //point 1
        $point1Address = $address1->getAddress().' '.$address1->getCity().', '.$address1->getZip();
        $geocodeQueryResult = $googleMapsProvider->geocode($point1Address);
        $point1 = $geocodeQueryResult->first();
        $lat1 = deg2rad($point1->getCoordinates()->getLatitude());
        $lng1 = deg2rad($point1->getCoordinates()->getLongitude());

        //point 2
        $point2Address = $address2->getAddress().' '.$address2->getCity().', '.$address2->getZip();
        $geocodeQueryResult = $googleMapsProvider->geocode($point2Address);
        $point2 = $geocodeQueryResult->first();
        $lat2 = deg2rad($point2->getCoordinates()->getLatitude());
        $lng2 = deg2rad($point2->getCoordinates()->getLongitude());

        $geoDistanceService = $this->get('app.geodistance_service');
        $distanceBetweenPoints = $geoDistanceService->getDistanceBetweenPoints($lat1, $lng1, $lat2, $lng2);
        $distanceNotGreater = $geoDistanceService->isDistanceBetweenPointsGreaterThan(
            $lat1,
            $lng1,
            $lat2,
            $lng2,
            5000
        );

        return $this->render('AdminBundle:Account:get_distance_between_addresses.html.twig', [
            'address1' => $address1,
            'address2' => $address2,
            'address1Latitude' => $lat1,
            'address1Longitude' => $lng1,
            'address2Latitude' => $lat2,
            'address2Longitude' => $lng2,
            'distanceBetweenPoints' => $distanceBetweenPoints,
            'smallDistance' => $distanceNotGreater,
        ]);
    }

    /**
     * @param Account $account
     * @return RedirectResponse
     */
    public function deleteAction(Account $account)
    {
        $this->denyAccessUnlessGranted('delete', $account);

        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        $accountManager->delete($account);

        return $this->redirectToRoute('admin_account_get', [
            'accountId' => $account->getId()
        ]);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return Response
     */
    public function searchCompanyForSelectAction(Account $account, Request $request)
    {
        /** @var Search $searchService */
        $searchService = $this->get('app.search');

        $this->denyAccessUnlessGranted('search_company', $account);
        $searchOptions = $request->query->all();

        return $this->render('@Admin/Account/search_company_for_select.html.twig', [
            'account' => $account,
            'searchResult' => $searchService->searchCompany($searchOptions)
        ]);
    }

    /**
     * @param Account $account
     * @param Company $company
     * @return RedirectResponse
     */
    public function companySelectAction(Account $account, Company $company)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountHistoryManager $accountHistoryManager */
        $accountHistoryManager = $this->get("app.account_history.manager");

        $this->denyAccessUnlessGranted('select_company', $account);
        $this->denyAccessUnlessGranted('add_account', $company);

        $account->setCompany($company);
        $objectManager->persist($account);
        $objectManager->flush();
        $accountHistoryManager->createAccountHistoryItem($account);

        return $this->redirectToRoute('admin_account_get', [
            'accountId' => $account->getId()
        ]);
    }

    /**
     * @param Account $account
     * @return RedirectResponse
     */
    public function deleteLinkToCompanyAction(Account $account)
    {
        /** @var AccountHistoryManager $accountHistoryManager */
        $accountHistoryManager = $this->get("app.account_history.manager");

        $account->setCompany(null);
        $accountHistoryManager->createAccountHistoryItem($account);

        return $this->redirectToRoute('admin_account_get', [
            'accountId' => $account->getId()
        ]);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return Response
     */
    public function searchContactPersonForSelectAction(Account $account, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var Search $searchService */
        $searchService = $this->get('app.search');
        /** @var AccountContactPersonRepository $accountContactPersonRepository */
        $accountContactPersonRepository = $objectManager->getRepository('AppBundle:AccountContactPerson');

        $this->denyAccessUnlessGranted('search_contact', $account);
        $searchOptions = $request->query->all();
        $searchResult = $searchService->searchContactPerson($searchOptions);

        // TODO: перевірку чи встановлено якісь ролі потрібно кудись винести
        if (isset($searchResult['results'])) {
            foreach ($searchResult['results'] as $key => $item) {
                $isSetRoles = $accountContactPersonRepository->getCountForAccountAndContact($account, $item);
                if ($isSetRoles) {
                    $searchResult['results'][$key]->isSetRoles = true;
                }
            }
        }

        return $this->render('@Admin/Account/search_contact_person_for_select.html.twig', [
            'account' => $account,
            'searchResult' => $searchResult
        ]);
    }

    /**
     * @param $groupAccountId
     * @param Request $request
     * @return Response
     */
    public function searchAccountForSelectAction($groupAccountId, Request $request)
    {
        /** @var Search $searchService */
        $searchService = $this->get('app.search');
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository('AppBundle:Account');
        /** @var AccountTypeRepository $accountTypeRepository */
        $accountTypeRepository = $objectManager->getRepository('AppBundle:AccountType');

        /** @var AccountTypeEntity $accountTypeGroupAccount */
        $accountTypeGroupAccount = $accountTypeRepository->findOneBy(['alias' => 'group account']);
        /** @var Account $groupAccount */
        $groupAccount = $accountRepository->findOneBy([
            'id' => $groupAccountId,
            'type' => $accountTypeGroupAccount
        ]);
        if (!$groupAccount) {
            throw new HttpException(404, "Group Account not found");
        }
        $this->denyAccessUnlessGranted('search_child', $groupAccount);
        $searchOptions = $request->query->all();

        return $this->render('@Admin/Account/search_account_for_select.html.twig', [
            'groupAccount' => $groupAccount,
            'searchResult' => $searchService->searchSiteAccount($searchOptions)
        ]);
    }

    /**
     * @param Account $siteAccount
     * @param Account $groupAccount
     * @return RedirectResponse
     */
    public function groupAccountSelectAction(Account $siteAccount, Account $groupAccount)
    {
        /** @var AccountHistoryManager $accountHistoryManager */
        $accountHistoryManager = $this->get("app.account_history.manager");
        /** @var OpportunityManager $opportunityManager */
        $opportunityManager = $this->get("app.opportunity.manager");
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');

        $this->denyAccessUnlessGranted('select_parent', $siteAccount);
        $this->denyAccessUnlessGranted('add_child', $groupAccount);

        $siteAccount->setParent($groupAccount);
        $accountManager->update($siteAccount);
        $opportunityManager->refreshForAccount($siteAccount);
        $accountHistoryManager->createAccountHistoryItem($siteAccount);
//        $this->get('app.message.manager')->isMunicipalityComplianceChannelByAccount($siteAccount);

        // TODO: КОСТИЛЬ (не було часу)
        /** @var EventDispatcherInterface $dispatcher */
        $dispatcher = $this->get('event_dispatcher');
        $event = new ParentAccountSetEvent($siteAccount, $groupAccount);
        $dispatcher->dispatch('account.set_parent', $event);

        return $this->redirectToRoute('admin_account_get', [
            'accountId' => $groupAccount->getId()
        ]);
    }

    /**
     * @param Account $account
     * @param $parentAccountId
     * @return RedirectResponse
     */
    public function groupAccountClearAction(Account $account, $parentAccountId)
    {
        /** @var AccountHistoryManager $accountHistoryManager */
        $accountHistoryManager = $this->get("app.account_history.manager");
        /** @var OpportunityManager $opportunityManager */
        $opportunityManager = $this->get("app.opportunity.manager");
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');

        /** @var Account $groupAccount */
        $groupAccount = $account->getParent();
        $account->setParent(null);
        $accountManager->update($account);
        $opportunityManager->refreshForAccount($account);
//        $accountHistoryManager->createAccountHistoryItem($account);

        // TODO: КОСТИЛЬ (не було часу)
        /** @var EventDispatcherInterface $dispatcher */
        $dispatcher = $this->get('event_dispatcher');
        $event = new ParentAccountUnsetEvent($account, $groupAccount);
        $dispatcher->dispatch('account.unset_parent', $event);

        return $this->redirectToRoute('admin_account_get', [
            'accountId' => $parentAccountId
        ]);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return Response
     */
    public function getOpportunityAction(Account $account, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        /** @var OpportunityManager $opportunityManager */
        $opportunityManager = $this->get('app.opportunity.manager');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        $filterParams = $request->query->all();
        $divisions = $deviceCategory->findAll();
        /** @var AccountHistoryRepository $accountHistoryRepository */
        $accountHistoryRepository = $this->get('app.entity_manager')->getRepository('AppBundle:AccountHistory');
        /** @var Grouping $workorderGroupingService */
        $workorderGroupingService = $this->get('workorder.grouping.service');

        //TODO: виправити костиль
        //past RETEST type
        $filterParams['type'] = 1;

        if (!array_key_exists('date', $filterParams)
            ||(empty($filterParams['date']['from']) || empty($filterParams['date']['to']))
        ) {
            $this->get('session')->getFlashBag()->add('error', '');
        }

        $viewParams = $accountManager->getAccountViewParameters($account, $filterParams);
        $viewParams['accountHistory'] = $accountHistoryRepository->findOneBy(
            ['ownerEntity' => $account],
            ['id' => 'DESC']
        );
        $viewParams['error'] = !empty($this->get('session')->getFlashBag()->get('error')) ? false : true;
        $viewParams['account'] = $account;
        $viewParams['filterContent'] = $opportunityManager->getInfoForFilters();
        $viewParams['filterParams'] = $filterParams;
        $viewParams['divisions'] = $divisions;
        $viewParams['workordersByAccount'] = $workorderGroupingService->byCategoryUnderAccount($account);

        return $this->render('@Admin/Account/view_opportunity.html.twig', $viewParams);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return Response
     */
    public function getResetNoticesAction(Account $account, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var ClientTypeRepository $proposalTypes */
        $proposalTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        /** @var ProposalStatusManager $proposalStatusManager */
        $proposalStatusManager = $this->get('app.proposal_status.manager');
        /** @var AccountHistoryRepository $accountHistoryRepository */
        $accountHistoryRepository = $this->get('app.entity_manager')->getRepository('AppBundle:AccountHistory');

        $divisions = $deviceCategory->findAll();
        $clientTypes = $proposalTypeRepository->findAll();
        $filters = $request->query->all();
        $filters['account'] = $account;

        if (!array_key_exists('date', $filters)
            ||(empty($filters['date']['from']) || empty($filters['date']['to']))
        ) {
            $this->get('session')->getFlashBag()->add('error', '');
        }
        $error = !empty($this->get('session')->getFlashBag()->get('error')) ? false : true;

        $proposalsTypeRetest = ["retest"];
        $res = $proposalRepository->findAllWithFilters($filters, $proposalsTypeRetest);
        $res['results'] = $proposalManager->setCountersForProposalsArray($res["results"]);


        $viewParams = $accountManager->getAccountViewParameters($account);
        $viewParams['account'] = $account;
        $viewParams['accountHistory'] = $accountHistoryRepository->findOneBy(
            ['ownerEntity' => $account], ['id' => 'DESC']
        );
        $viewParams['paginatedProposalList'] = $res;
        $viewParams['divisions'] = $divisions;
        $viewParams['proposalStatuses'] = $proposalStatusManager->getForRetestNotices();
        $viewParams['clientTypes'] = $clientTypes;
        $viewParams['filters'] = $filters;
        $viewParams['error'] = $error;
        $viewParams['proposalsForMerge'] = $proposalManager->isShowButtonForMergePDF($filters);

        return $this->render('@Admin/Account/view_reset_notices.html.twig', $viewParams);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return Response
     */
    public function getProposalsAction(Account $account, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalStatusManager $proposalStatusManager */
        $proposalStatusManager = $this->get('app.proposal_status.manager');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var ClientTypeRepository $proposalTypes */
        $proposalTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        /** @var AccountHistoryRepository $accountHistoryRepository */
        $accountHistoryRepository = $this->get('app.entity_manager')->getRepository('AppBundle:AccountHistory');

        $divisions = $deviceCategory->findAll();
        $clientTypes = $proposalTypeRepository->findAll();
        $filters = $request->query->all();
        $filters['account'] = $account;

        if (!array_key_exists('date', $filters)
            ||(empty($filters['date']['from']) || empty($filters['date']['to']))
        ) {
            $this->get('session')->getFlashBag()->add('error', '');
        }
        $error = !empty($this->get('session')->getFlashBag()->get('error')) ? false : true;

        $proposalsTypes = ["custom", "repair"];

        $res = $proposalRepository->findAllWithFilters($filters, $proposalsTypes);
        $res['results'] = $proposalManager->setCountersForProposalsArray($res["results"]);

        $viewParams = $accountManager->getAccountViewParameters($account);
        $viewParams['account'] = $account;
        $viewParams['accountHistory'] = $accountHistoryRepository->findOneBy(
            ['ownerEntity' => $account], ['id' => 'DESC']
        );
        $viewParams['paginatedProposalList'] = $res;
        $viewParams['divisions'] = $divisions;
        $viewParams['proposalStatuses'] = $proposalStatusManager->getForRepairProposal();
        $viewParams['clientTypes'] = $clientTypes;
        $viewParams['filters'] = $filters;
        $viewParams['error'] = $error;
        $viewParams['proposalsForMerge'] = $proposalManager->isShowButtonForMergePDF($filters);

        return $this->render('@Admin/Account/view_proposals.html.twig', $viewParams);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return Response
     */
    public function getWorkOrdersAction(Account $account, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository('AppBundle:Workorder');
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        /** @var WorkorderManager $workorderManager */
        $workorderManager = $this->get('app.work_order.manager');
        /** @var ContractorUserManager $contractorUserManager */
        $contractorUserManager = $this->get('app.contractor_user.manager');
        /** @var AccountHistoryRepository $accountHistoryRepository */
        $accountHistoryRepository = $this->get('app.entity_manager')->getRepository('AppBundle:AccountHistory');

        $viewParams = $accountManager->getAccountViewParameters($account);
        $viewParams['account'] = $account;
        $viewParams['allDivisions'] = $objectManager->getRepository('AppBundle:DeviceCategory')->findAll();
        $viewParams['allStatuses'] = $objectManager->getRepository('AppBundle:WorkorderStatus')->findAll();
        $viewParams['filters'] = $request->query->all();
        $viewParams['filters']['account'] = $account;
        $viewParams['accountHistory'] = $accountHistoryRepository->findOneBy(
            ['ownerEntity' => $account], ['id' => 'DESC']
        );

        if (empty($viewParams['filters']['pagerCount'])) {
            $viewParams['filters']['pagerCount'] = 50;
        }

        $viewParams['workorders'] = $workorderRepository->findAllWithPagination($viewParams['filters']);
        $viewParams['contractorUsers'] = $contractorUserManager->getContractorUsersGroupedByWorkorder(
            $viewParams['workorders']['results']
        );
        $viewParams['workorders']['results'] = $workorderManager
            ->processWorkorderFindFrozen($viewParams['workorders']['results']);
        $viewParams['workorders']['results'] = $workorderManager
            ->groupingByScheduleDate($viewParams['workorders']['results']);

        //set current day by default (if empty all filters)
        $viewParams['currentDayByDefault'] = false;

        return $this->render('@Admin/Account/view_work_order.html.twig', $viewParams);
    }

    /**
     * @param Account $account
     * @return Response
     */
    public function getRepairProposalAction(Account $account)
    {
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');

        $viewParams = $accountManager->getAccountViewParameters($account);
        $viewParams['account'] = $account;

        return $this->render('@Admin/Account/view_repair_proposal.html.twig', $viewParams);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return Response
     */
    public function getRepairOpportunitiesAction(Account $account, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var OpportunityRepository $opportunityRepository */
        $opportunityRepository = $objectManager->getRepository('AppBundle:Opportunity');
        /** @var ProposalStatusRepository $proposalStatusesRepository */
        $proposalStatusRepository = $objectManager->getRepository('AppBundle:ProposalStatus');
        /** @var ClientTypeRepository $proposalTypes */
        $proposalTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        /** @var OpportunityManager $opportunityManager */
        $opportunityManager = $this->get('app.opportunity.manager');
        /** @var OpportunityTypeRepository $opportunityTypeRepository */
        $opportunityTypeRepository = $objectManager->getRepository('AppBundle:OpportunityType');
        /** @var AccountHistoryRepository $accountHistoryRepository */
        $accountHistoryRepository = $this->get('app.entity_manager')->getRepository('AppBundle:AccountHistory');
        /** @var Session $session */
        $session = $this->get('session');
        $session->set('previousPage', $request->getRequestUri());

        $divisions = $deviceCategory->findAll();
        $proposalStatuses = $proposalStatusRepository->findAll();
        $clientTypes = $proposalTypeRepository->findAll();
        $filters = $request->query->all();
        $filters['account'] = $account;

        /** @var OpportunityTypeRepository $opportunityTypeRetest */
        $opportunityTypeRepair = $opportunityTypeRepository->findBy(['alisa' => 'repair']);
        $filters['type'] = $opportunityTypeRepair;

        if (!array_key_exists('date', $filters)
            ||(empty($filters['date']['from']) || empty($filters['date']['to']))
        ) {
            $this->get('session')->getFlashBag()->add('error', '');
        }
        $error = !empty($this->get('session')->getFlashBag()->get('error')) ? false : true;

        $res = $opportunityRepository->findByFilterWithPaginationForAccount($account, $filters);
        $res['results'] = $opportunityManager->groupByAccount($res['results']);

        $viewParams = $accountManager->getAccountViewParameters($account);
        $viewParams['account'] = $account;
        $viewParams['accountHistory'] = $accountHistoryRepository->findOneBy(
            ['ownerEntity' => $account], ['id' => 'DESC']
        );
        $viewParams['paginatedProposalList'] = $res;
        $viewParams['divisions'] = $divisions;
        $viewParams['proposalStatuses'] = $proposalStatuses;
        $viewParams['clientTypes'] = $clientTypes;
        $viewParams['filters'] = $filters;
        $viewParams['error'] = $error;
        $viewParams['proposalsForMerge'] = $proposalManager->isShowButtonForMergePDF($filters);

        return $this->render('@Admin/Account/view_repair_opportunities.html.twig', $viewParams);
    }
}
