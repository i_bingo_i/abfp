<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Repository\TrashRequestsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class TrashController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var TrashRequestsRepository $trashRequestsRepository */
        $trashRequestsRepository = $objectManager->getRepository('AppBundle:TrashRequests');
        $filters = $request->query->all();
        $page = $request->query->get('page');

        $trashResults = $trashRequestsRepository->findByFilterWithPagination($filters, $page);

        return $this->render('@Admin/Trash/list.html.twig', [
            'trashResults' => $trashResults
        ]);
    }
}