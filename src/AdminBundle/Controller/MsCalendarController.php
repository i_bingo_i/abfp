<?php

namespace AdminBundle\Controller;

use AppBundle\Services\MSCalendarToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MsCalendarController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function signInAction(Request $request)
    {
        /** @var MSCalendarToken $msCalendarToken */
        $msCalendarToken = $this->get('app.ms_calendar_token.service');
        /** @var Session $session */
        $session = $this->get('session');

        $authorizationCode = false;
        if ($request->query->has('code') && $request->get('code')) {
            $authorizationCode = $request->get('code');
        }

        if ($authorizationCode) {
            $msCalendarToken->refresh($authorizationCode);
        }

        $redirectUri = $session->get('redirect_uri');
        $session->remove('redirect_uri');

        return $this->redirect($redirectUri);
    }
}
