<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ProcessingStats;
use AppBundle\Entity\Repository\ProcessingStatsRepository;

class ProcessingStatsController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function getProcessingStatsListAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get("app.entity_manager");
        /** @var ProcessingStatsRepository $processingStatsRepository */
        $processingStatsRepository = $objectManager->getRepository("AppBundle:ProcessingStats");
        $filters = $request->query->all() ?? [];
        $results = $processingStatsRepository->findAllWithPagination($filters);

        return $this->render("AdminBundle:ProcessingStats:processing_stats_list.html.twig", ["results" =>$results]);
    }
}