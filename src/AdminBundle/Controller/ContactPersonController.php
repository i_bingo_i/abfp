<?php

namespace AdminBundle\Controller;

use AppBundle\ActionManagers\ContactPersonActionManager;
use AppBundle\Creators\ContactPersonFormCreator;
use AppBundle\DTO\Requests\SaveContactPersonRequest;
use InvoiceBundle\Entity\Customer;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use InvoiceBundle\Repository\CustomerRepository;
use AppBundle\Entity\Workorder;
use AppBundle\Parsers\ContactPerson\Form\CreateContactPersonParser;
use AppBundle\Services\AccountContactPersonHistory\Creator;
use AppBundle\Services\AccountContactPerson\Creator as AccountContactPersonCreator;
use InvoiceBundle\Services\Customer\Provider\CustomerContactPersonProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\ContactPerson;
use AdminBundle\Form\ContactPersonType;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Address;
use AppBundle\Entity\Account;
use AppBundle\Services\Search;
use AppBundle\Services\AccountContactPersonCompose;
use AppBundle\Entity\Company;

class ContactPersonController extends Controller
{
    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        /** @var ContactPersonFormCreator $contactPersonFormCreator */
        $contactPersonFormCreator = $this->get("contact_person.form.creator");
        $form = $contactPersonFormCreator->makeFormCreate();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CreateContactPersonParser $parser */
            $parser = $this->get("contact_person.form_create.parser");
            /** @var SaveContactPersonRequest $createContactPersonDTO */
            $createContactPersonDTO = $parser->parse($form);

            /** @var ContactPersonActionManager $contactPersonManager */
            $contactPersonManager = $this->get('contact_person.action.manager');
            /** @var ContactPerson $newContactPerson */
            $newContactPerson = $contactPersonManager->create($createContactPersonDTO);

            return $this->redirectToRoute('admin_contact_person_get', ['contactPerson' => $newContactPerson->getId()]);
        }

        return $this->render('@Admin/ContactPerson/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Account $account
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function createAndLinkToAccountAction(Account $account, Request $request)
    {
        /** @var ContactPersonFormCreator $contactPersonFormCreator */
        $contactPersonFormCreator = $this->get("contact_person.form.creator");
        $form = $contactPersonFormCreator->makeFormCreate();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CreateContactPersonParser $parser */
            $parser = $this->get("contact_person.form_create.parser");
            /** @var SaveContactPersonRequest $createContactPersonDTO */
            $createContactPersonDTO = $parser->parse($form);

            /** @var ContactPersonActionManager $contactPersonManager */
            $contactPersonManager = $this->get('contact_person.action.manager');
            /** @var ContactPerson $newContactPerson */
            $newContactPerson = $contactPersonManager->create($createContactPersonDTO);

            return $this->redirectToRoute('admin_account_contact_person_set_roles', [
                'account' => $account->getId(),
                'contactPerson' => $newContactPerson->getId()
            ]);
        }

        return $this->render('@Admin/ContactPerson/create_and_linked_to_account.html.twig', [
            'form' => $form->createView(),
            'account' => $account
        ]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateAction(ContactPerson $contactPerson, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $contactPerson);

        /** @var ContactPersonFormCreator $contactPersonFormCreator */
        $contactPersonFormCreator = $this->get("contact_person.form.creator");
        $form = $contactPersonFormCreator->makeFormUpdate($contactPerson);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CreateContactPersonParser $parser */
            $parser = $this->get("contact_person.form_create.parser");
            /** @var SaveContactPersonRequest $saveContactPersonDTO */
            $saveContactPersonDTO = $parser->parse($form);

            /** @var ContactPersonActionManager $contactPersonManager */
            $contactPersonManager = $this->get('contact_person.action.manager');
            /** @var ContactPerson $updatedContactPerson */
            $updatedContactPerson = $contactPersonManager->update($saveContactPersonDTO);

            return $this->redirectToRoute('admin_contact_person_get', [
                'contactPerson' => $updatedContactPerson->getId()
            ]);
        }

        return $this->render('@Admin/ContactPerson/update.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @return Response
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAction(ContactPerson $contactPerson)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountContactPersonRepository $accountContactPersonsRepository */
        $accountContactPersonsRepository = $objectManager->getRepository("AppBundle:AccountContactPerson");
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $objectManager->getRepository('InvoiceBundle:Customer');
        /** @var AccountContactPersonCompose $accountContactPersonComposeManager */
        $accountContactPersonComposeManager = $this->get('app.account_contact_person_compose');
        /** @var Customer $customer */
        $customer = $customerRepository->findByContactPerson($contactPerson);

        $accountContactPersons = $accountContactPersonsRepository->findBy([
            'contactPerson' => $contactPerson,
            'deleted' => false
        ]);
        $accountContactPersonsComposed = $accountContactPersonComposeManager->compose($accountContactPersons);
        $accountContacts = array_merge(
            $accountContactPersonsComposed['primary'],
            $accountContactPersonsComposed['other']
        );

        return $this->render('@Admin/ContactPerson/detail.html.twig', [
            'contactPerson' => $contactPerson,
            'accountContacts' => $accountContacts,
            'customer' => $customer
        ]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @return RedirectResponse
     */
    public function deleteAction(ContactPerson $contactPerson)
    {
        /** @var ContactPersonManager $contactPersonManager */
        $contactPersonManager = $this->get('app.contact_person.manager');
        $this->denyAccessUnlessGranted('delete', $contactPerson);

        // TODO: перенести це в ContactPersonListener PostDelete
        $this->get('app.account_contact_person.manager')->deleteByContactPerson($contactPerson);

        $contactPersonManager->delete($contactPerson);

        return $this->redirectToRoute('admin_contact_person_get', [
            'contactPerson' => $contactPerson->getId()
        ]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @return RedirectResponse
     */
    public function deleteLinkToCompanyAction(ContactPerson $contactPerson)
    {
        /** @var ContactPersonManager $contactPersonManager */
        $contactPersonManager = $this->get('app.contact_person.manager');
        $contactPersonManager->unsetContactPersonFromCompany($contactPerson);

        return $this->redirectToRoute('admin_contact_person_get', [
            'contactPerson' => $contactPerson->getId()
        ]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @param Request $request
     * @return Response
     */
    public function searchCompanyForSelectAction(ContactPerson $contactPerson, Request $request)
    {
        /** @var Search $searchService */
        $searchService = $this->get('app.search');

        $this->denyAccessUnlessGranted('search_company', $contactPerson);
        $searchOptions = $request->query->all();

        return $this->render('@Admin/ContactPerson/search_company_for_select.html.twig', [
            'contactPerson' => $contactPerson,
            'searchResult' => $searchService->searchCompany($searchOptions)
        ]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @param Company $company
     * @return RedirectResponse
     */
    public function companySelectAction(ContactPerson $contactPerson, Company $company)
    {
        /** @var ContactPersonManager $contactPersonManager */
        $contactPersonManager = $this->get('app.contact_person.manager');

        $this->denyAccessUnlessGranted('select_company', $contactPerson);
        $this->denyAccessUnlessGranted('add_contact', $company);

        $contactPerson->setCompany($company);
        $contactPersonManager->update($contactPerson);

        return $this->redirectToRoute('admin_contact_person_get', [
            'contactPerson' => $contactPerson->getId()
        ]);
    }

    /**
     * @param Account $account
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function quickCreateAction(Account $account, Workorder $workorder, Request $request)
    {
        /** @var ContactPersonFormCreator $contactPersonFormCreator */
        $contactPersonFormCreator = $this->get("contact_person.form.creator");
        $form = $contactPersonFormCreator->makeFormCreate();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CreateContactPersonParser $parser */
            $parser = $this->get("contact_person.form_create.parser");
            /** @var SaveContactPersonRequest $createContactPersonDTO */
            $createContactPersonDTO = $parser->parse($form);

            /** @var ContactPersonActionManager $contactPersonManager */
            $contactPersonManager = $this->get('contact_person.action.manager');
            /** @var ContactPerson $newContactPerson */
            $newContactPerson = $contactPersonManager->create($createContactPersonDTO);

            /** @var AccountContactPersonCreator $accountContactPersonCreator */
            $accountContactPersonCreator = $this->get('app.account_contact_person_creator.service');
            $accountContactPerson = $accountContactPersonCreator->createForWorkOrder($account, $newContactPerson);
            $accountContactPerson->setCustomer($contactPersonManager->customer);
            /** It is  SPIKE */
            /** @var Creator $accountContactPersonHistoryCreatorService */
            $accountContactPersonHistoryCreatorService = $this->get('app.account_contact_person_history_creator.service');
            $accountContactPersonHistoryCreatorService->create($accountContactPerson);

            return $this->redirectToRoute('admin_workorder_view', ["workorder" => $workorder->getId()]);
        }

        return $this->render('@Admin/ContactPerson/create_for_workorder.html.twig', [
            'form' => $form->createView(),
            "workorder" => $workorder
        ]);
    }
}
