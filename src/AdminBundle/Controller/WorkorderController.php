<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\CustomProposalOrWorkorderType;
use AdminBundle\Form\WorkorderCommentType;
use AdminBundle\Form\WorkorderGeneralInfoType;
use AdminBundle\Form\ScheduleType;
use AdminBundle\Form\WorkorderLetterType;
use AdminBundle\Form\WorkorderType;
use AppBundle\Entity\Account;
use AppBundle\Entity\ContractorCalendar;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\EntityManager\ContractorUserManager;
use AppBundle\Entity\EntityManager\EventManager;
use AppBundle\Entity\Event;
use InvoiceBundle\Manager\InvoiceManager;
use AppBundle\Entity\ReportStatus;
use AppBundle\Entity\Repository\ContractorCalendarRepository;
use AppBundle\Entity\Repository\ReportStatusRepository;
use AppBundle\Entity\Repository\WorkorderCommentRepository;
use AppBundle\Entity\Repository\WorkorderLogRepository;
use AppBundle\Entity\EntityManager\ProposalLogManager;
use AppBundle\Entity\User;
use AppBundle\Entity\WorkorderComment;
use AppBundle\Entity\WorkorderLogType;
use AppBundle\Security\WorkOrderVoter;
use AppBundle\Services\AccountContactPerson\Authorizer;
use AppBundle\Services\Coordinates\Helper;
use AppBundle\Services\Event\Compose;
use AppBundle\Services\InvoiceCreator;
use AppBundle\Services\LogComments;
use AppBundle\Services\MSCalendarToken;
use AppBundle\Services\TechnicianAssignmentSyncService;
use AppBundle\Services\Files;
use AppBundle\Services\Tree\Grouping;
use WorkorderBundle\Services\CalculateTotals;
use WorkorderBundle\Services\ComposerFormService;
use WorkorderBundle\Services\Creator;
use WorkorderBundle\Services\GetLetterRecipientService;
use WorkorderBundle\Services\Freeze;
use WorkorderBundle\Services\IncludeProposal;
use WorkorderBundle\Services\CommentService;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\Workorder;
use AppBundle\Entity\Repository\WorkorderRepository;
use KnpU\OAuth2ClientBundle\Client\Provider\MicrosoftClient;
use League\OAuth2\Client\Provider\AbstractProvider;
use LoggerBundle\Services\EventLogger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use WorkorderBundle\Services\ReportMerger;
use WorkorderBundle\Services\LogCreator;
use WorkorderBundle\Services\LogMessage;
use \Twig\Error\Error as TwigError;
use WorkorderBundle\Services\WorkorderFilesComposer;
use Symfony\Component\Finder\Finder;

class WorkorderController extends Controller
{
    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function createAction(Proposal $proposal, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var IncludeProposal $woIncludeProposal */
        $woIncludeProposal = $this->get("workorder.include_proposal.service");
        /** @var Helper $coordinateHelper */
        $coordinateHelper = $this->get('app.coordinates_helper.service');
        /** @var Creator $workorderCreatorService */
        $workorderCreatorService = $this->get('workorder.creator.service');
        /** @var Account $account */
        $account = $proposal->getAccount();
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        /** @var DeviceCategory $division */
        $division = $proposal->getDivision();
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository("AppBundle:Workorder");

        $params['proposal'] = $proposal;
        if (!$this->isGranted(WorkOrderVoter::CAN_NOT_ADD_PROPOSAL_TO_WO, $params)) {
            return $this->redirect($request->headers->get('referer'));
        }

        $form = $this->createForm(WorkorderType::class, [], [
            'validation_groups' => ['create'],
            'account' => $account
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            if ($formData['workorder']) {
                /** @var Workorder $workorder */
                $workorder = $workorderRepository->find($formData['workorder']);
            } else {
                /** @var Workorder $workorder */
                $workorder = $workorderCreatorService->create($account, $division, $user);
            }

            $params['workorder'] = $workorder;
            if (!$this->isGranted(WorkOrderVoter::CAN_NOT_ADD_PROPOSAL_TO_FROZEN_WO, $params)) {
                return $this->redirect($request->headers->get('referer'));
            }

            /** @var Workorder $workorder */
            $workorder = $woIncludeProposal->addProposal($proposal, $workorder);

            //set coordinates by Account Address
            $coordinateHelper->refreshByAccount($workorder->getAccount());

            //creating proposal and WO logs
            $woIncludeProposal->creatingLogs($proposal, $formData);

            return $this->redirectToRoute("admin_workorder_view", ['workorder' => $workorder->getId()]);
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Workorder $workorder
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function viewAction(Workorder $workorder): Response
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalManager $proposalManager */
        $workorderManager = $this->get('app.work_order.manager');
        /** @var WorkorderLogRepository $workorderLogRepository */
        $workorderLogRepository = $objectManager->getRepository("AppBundle:WorkorderLog");
        /** @var ReportStatusRepository $reportStatusRepositrory */
        $reportStatusRepositrory = $objectManager->getRepository('AppBundle:ReportStatus');
        /** @var ReportStatus $reportStatus */
        $reportStatus = $reportStatusRepositrory->findAllExceptAliases(['completed']);
        /** @var Grouping $treeService */
        $treeService = $this->get('app.tree.service');
        /** @var WorkorderCommentRepository $workorderCommentsRepository */
        $workorderCommentsRepository = $objectManager->getRepository('AppBundle:WorkorderComment');
        /** @var Authorizer $acpAuthorizerService */
        $acpAuthorizerService = $this->get('app.account_contact_person_authorizer.service');
        /** @var Compose $eventCompose */
        $eventCompose = $this->get('app.event_compose.service');
        /** @var GetLetterRecipientService $getRecipientService */
        $getRecipientService = $this->get("workorder.get.letter.recipient.service");

        $events = $eventCompose->composeByWorkorder($workorder);
        $workorderCommentForm = $this->createForm(WorkorderCommentType::class);
        $workorderComments = $workorderCommentsRepository->findBy(
            ['workorder' => $workorder],
            ['dateCreate' => 'DESC']
        );

        return $this->render(
            'AdminBundle:Workorder:view.html.twig',
            [
                'workorder' => $workorder,
                'workorderAuthorizer' => $acpAuthorizerService->getAuthorizerForWorkorder($workorder),
                'account' => $workorderManager->getAccountForWorkorder($workorder),
                'tree' => $treeService->getTreeForWorkorder($workorder),
                'accountAuthorizer' => $acpAuthorizerService->getAccountAuthorizerForWorkorder($workorder),
                'logs' => $workorderLogRepository->findBy(['workorder' => $workorder], ['id' => 'DESC']),
                'isFrozen' => $workorder->getIsFrozen(),
                'optionsArray' => $reportStatus,
                'events' => $events,
                'workorderGrandTotalPrice' => $workorderManager->calculatePricesForGrandTotal($workorder),
                'countServicesReports' => $workorderManager->getCountsWOReports($workorder),
                'commentForm' => $workorderCommentForm->createView(),
                'workorderComments' => $workorderComments,
                'recipients' => $getRecipientService->composeAllEmails($workorder),
                'primaryPayment' => (bool)$acpAuthorizerService->getPrimaryPaymentByAccountForDivisions($workorder),
                'liveAccountAuthorizer' => (bool)$acpAuthorizerService
                    ->getPrimaryAuthorizersByAccountForDivisions($workorder)
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request): Response
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var WorkorderManager $workorderManager */
        $workorderManager = $this->get('app.work_order.manager');
        /** @var ContractorUserManager $contractorUserManager */
        $contractorUserManager = $this->get('app.contractor_user.manager');

        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository('AppBundle:Workorder');
        $allDivisions = $objectManager->getRepository('AppBundle:DeviceCategory')->findAll();
        $allStatuses = $objectManager->getRepository('AppBundle:WorkorderStatus')->findAll();

        $filters = $request->query->all();

        //set current day by default (if empty all filters)
        $currentDayByDefault = false;
        if (empty($filters)) {
            $currentDayByDefault = true;
            $filters["date"]["from"] = date("m/d/Y");
            $filters["date"]["to"] = date("m/d/Y");
        }

        if (empty($filters['pagerCount'])) {
            $filters['pagerCount'] = 100;
        }

        $workorders = $workorderRepository->findAllWithPagination($filters);
        $technicians = $contractorUserManager->getContractorUsersGroupedByWorkorder($workorders['results']);
        $workorders['results'] = $workorderManager->processWorkorderFindFrozen($workorders['results']);
        $workorders['results'] = $workorderManager->groupingByScheduleDate($workorders['results']);

        return $this->render(
            'AdminBundle:Workorder:list.html.twig',
            [
                'workorders' => $workorders,
                'allDivisions' => $allDivisions,
                'allStatuses' => $allStatuses,
                'filters' => $filters,
                'currentDayByDefault' => $currentDayByDefault,
                'contractorUsers' => $technicians
            ]
        );
    }

    // TODO: Стара реалізація прикріплення інвойсу до WO. На даний момент застаріла і використовується лише для старого WO Life Cycle
    /**
     * @deprecated This action deprecated use new action submitCustomInvoiceAction in SubmitCustomInvoiceController
     *
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function uploadInvoiceAction(Workorder $workorder, Request $request)
    {
        /** @var $workorderManager */
        $workorderManager = $this->get('app.work_order.manager');
        /** @var Freeze $woFreeze */
        $woFreeze = $this->get('workorder.freeze.service');
        /** @var InvoiceCreator $invoiceCreator */
        $invoiceCreator = $this->get('app.invoice_creator.service');

        $data = ['comment' => 'Type your comment here'];
        $form = $this->createFormBuilder($data)
            ->add('comment', TextType::class)
            ->add('image', FileType::class, ['constraints' => [new NotBlank()]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var string $image */
            $image = $workorderManager->saveImage($form['image']->getData());
            $isChangingStatus = $workorderManager->setStatus($workorder, $workorderManager::STATUS_PENDING_PAYMENT);
            $woFreeze->freezing($workorder);
            $workorderManager->update($workorder);

            $invoice = $invoiceCreator->create($workorder, $image);
            $workorder->addInvoice($invoice);

            /** @var LogComments $logComments */
            $logComments = $this->get('app.log.comments');
            $comment = $logComments->parseByParams($form->getData());

            /** @var LogMessage $logMessage */
            $logMessage = $this->get('workorder.log_message');
            /** @var LogCreator $woLogCreator */
            $woLogCreator = $this->get('workorder.log_creator');
            $woLogCreator->createRecord($workorder, [
                'invoice' => $invoice,
                'changingStatus' => $isChangingStatus,
                'type' => WorkorderLogType::TYPE_PAYMEN_RECEIVED,
                'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_PAYMEN_RECEIVED),
                'comment' => $comment
            ]);
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function sendReportsAction(Workorder $workorder, Request $request)
    {
        /** @var $workorderManager */
        $workorderManager = $this->get('app.work_order.manager');

        $data = [];
        $form = $this->createFormBuilder($data)
            ->add('comment', TextType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $isChangingStatus = $workorderManager->setStatus($workorder, $workorderManager::STATUS_DONE);
            $workorderManager->update($workorder);

            /** @var LogMessage $logMessage */
            $logMessage = $this->get('workorder.log_message');
            /** @var LogCreator $woLogCreator */
            $woLogCreator = $this->get('workorder.log_creator');
            $woLogCreator->createRecord($workorder, [
                'changingStatus' => $isChangingStatus,
                'type' => WorkorderLogType::TYPE_DONE,
                'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_DONE),
                'comment' => $form['comment']->getData()
            ]);
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function pendingPaymentAction(Workorder $workorder, Request $request)
    {
        /** @var $workorderManager */
        $workorderManager = $this->get('app.work_order.manager');

        $data = [];
        $form = $this->createFormBuilder($data)
            ->add('comment', TextType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $isChangingManager = $workorderManager->setStatus($workorder, $workorderManager::STATUS_SEND_REPORTS);
            $workorderManager->update($workorder);

            /** @var LogMessage $logMessage */
            $logMessage = $this->get('workorder.log_message');
            /** @var LogCreator $woLogCreator */
            $woLogCreator = $this->get('workorder.log_creator');
            $woLogCreator->createRecord($workorder, [
                'type' => WorkorderLogType::TYPE_REPORTS_SENT,
                'message' => $logMessage->makeByLogType(WorkorderLogType::TYPE_REPORTS_SENT),
                'comment' => $form['comment']->getData(),
                'changingStatus' => $isChangingManager
            ]);
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function assignTechnicianAction(Workorder $workorder, Request $request)
    {
        $params['workorder'] = $workorder;
        if (!$this->isGranted(WorkOrderVoter::CAN_SCHEDULING_WO, $params)) {
            return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
        }
        /** @var WorkorderManager $workorderManager */
        $workorderManager = $this->get('app.work_order.manager');
        /** @var TechnicianAssignmentSyncService $technicianAssignmentSyncService */
        $technicianAssignmentSyncService = $this->get('app.technician_assignment_sync.service');
        /** @var MSCalendarToken $msCalendarToken */
        $msCalendarToken = $this->get('app.ms_calendar_token.service');
        /** @var Session $session */
        $session = $this->get('session');
        /** @var \AppBundle\Services\Event\Creator $eventCreatorService */
        $eventCreatorService = $this->get('app.event_creator.service');
        /** @var \AppBundle\Services\Event\Helper $eventHelperService */
        $eventHelperService = $this->get('app.event_helper.service');
        /** @var EventLogger $eventLoggerService */
        $eventLoggerService = $this->get('monolog.event_logger.service');
        /** @var Compose $eventCompose */
        $eventCompose = $this->get('app.event_compose.service');

        $form = $this->createForm(ScheduleType::class, null, ['workorder' => $workorder]);
        $form->handleRequest($request);

        $accessToken = $msCalendarToken->getOrRefreshAccessTokenForWo($workorder);

        if (!$accessToken) {
            $msCalendarLoginUrl = $msCalendarToken->getLoginUrl();
            return $this->redirect($msCalendarLoginUrl);
        }

        if ($form->isValid() && $form->isSubmitted() && !$workorderManager->isDateValid($form->getData())) {
            $isChangedWorkorderStatus = $workorderManager->setScheduledDate($workorder, $form->getData(), true);
            /** @var Event $event */
            $event = $eventCreatorService->create($workorder, $form->getData());

            $workorder->addEvent($event);
            $workorderManager->update($workorder);

            $calendarId = $event->getUser()->getMsCalendarId();
            if ($calendarId) {
                $msocEventId = $technicianAssignmentSyncService->setAnotherCalendarEvent(
                    $accessToken,
                    $calendarId,
                    $event
                );
                $eventHelperService->setNewMsOutlookCalendarEventId($event, $msocEventId);
            }
            //end Store Event to Microsoft Outlook Calendar

            // create log record for event
            if (isset($msocEventId) and is_string($msocEventId)) {
                $session->getFlashBag()->add("success", 'New event has been added to MS Calendar');
                $eventLoggerService->create($event, $msocEventId);
            } else {
                $session->getFlashBag()->add("failed", 'New event has NOT been added to MS Calendar');
                $eventLoggerService->create($event, 'New event has NOT been added to MS Calendar');
            }

            //log record
            /** @var LogMessage $logMessage */
            $logMessage = $this->get('workorder.log_message');
            /** @var LogCreator $woLogCreator */
            $woLogCreator = $this->get('workorder.log_creator');
            $woLogCreator->createRecord($workorder, [
                'type' => WorkorderLogType::TYPE_WORKORDER_WAS_SCHEDULED,
                'message' => $logMessage->makeByCreateEvent($event),
                'changingStatus' => $isChangedWorkorderStatus
            ]);

            return $this->redirectToRoute('admin_assign_technician_workorder', ['workorder' => $workorder->getId()]);
        }

        return $this->render(
            '@Admin/Workorder/assign-technician.html.twig',
            [
                'form' => $form->createView(),
                'times' => $eventHelperService->generateTime(),
                'workorder' => $workorder,
                'events' => $eventCompose->composeByWorkorder($workorder),
                'errors' => $session->getFlashBag()->get('error', []),
                'previousPage' => 'admin_workorder_view',
                'user' => $this->getUser()
            ]
        );
    }

    /**
     * @param Event $event
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function detachTechnicianAction(Event $event, Request $request)
    {
        /** @var EventManager $eventManager */
        $eventManager = $this->get('app.event.manager');
        /** @var WorkorderManager $workorderManager */
        $workorderManager = $this->get('app.work_order.manager');
        /** @var string $previousPage */
        $previousPage = $request->headers->get('referer');
        /** @var EventLogger $eventLoggerService */
        $eventLoggerService = $this->get('monolog.event_logger.service');
        /** @var Event $deletedEvent */
        $deletedEvent = clone $event;
        $eventManager->delete($event);

        //delete Event from Microsoft Outlook Calendar
        //get services for access token getting
        /** @var TechnicianAssignmentSyncService $technicianAssignmentSyncService */
        $technicianAssignmentSyncService = $this->get('app.technician_assignment_sync.service');
        /** @var MSCalendarToken $msCalendarToken */
        $msCalendarToken = $this->get('app.ms_calendar_token.service');

        $redirectUri = $this->generateUrl(
            'admin_assign_technician_workorder',
            ['workorder' => $deletedEvent->getWorkorder()->getId()]
        );

        $accessToken = $msCalendarToken->getAccessToken($redirectUri);

        if (!$accessToken) {
            $msCalendarLoginUrl = $msCalendarToken->getLoginUrl();
            return $this->redirect($msCalendarLoginUrl);
        }

        try {
            $msOutlookCalendarEventId = $event->getMsOutlookCalendarEventId();
            if ($msOutlookCalendarEventId) {
                $technicianAssignmentSyncService->deleteEvent(
                    $accessToken,
                    $event->getMsOutlookCalendarEventId()
                );
            }
        } catch (\Exception $exception) {
            $this->redirect($request->headers->get('referer'));
        }
        //end Delete Event from Microsoft Outlook Calendar

        // create log record for event
        $eventLoggerService->remove($deletedEvent);

        $isChangedWorkorderStatus = $workorderManager->checkScheduleDate($deletedEvent->getWorkorder(), $deletedEvent);
        /** @var LogMessage $logMessage */
        $logMessage = $this->get('workorder.log_message');
        /** @var LogCreator $woLogCreator */
        $woLogCreator = $this->get('workorder.log_creator');
        $woLogCreator->createRecord($event->getWorkorder(), [
            'type' => WorkorderLogType::TYPE_WORKORDER_TO_BE_SCHEDULED,
            'message' => $logMessage->makeByDeleteEvent($event),
            'changingStatus' => $isChangedWorkorderStatus
        ]);

        return $this->redirect($previousPage);
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return Response
     */
    public function editGeneralInfoAction(Workorder $workorder, Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $workorderManager = $this->get('app.work_order.manager');
        /** @var CalculateTotals $workorderCalculateTotalService */
        $workorderCalculateTotalService = $this->get('workorder.calculate_totals.service');

        $form = $this->createForm(
            WorkorderGeneralInfoType::class,
            $workorder,
            ['validation_groups' => ['create']]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $workorder->setGrandTotal($workorderCalculateTotalService->calculateGrandTotal($workorder));
            $workorderManager->update($workorder);

            return $this->redirectToRoute("admin_workorder_view", ['workorder' => $workorder->getId()]);
        }

        return $this->render(
            '@Admin/Workorder/edit_general_info.html.twig',
            [
                "form" => $form->createView(),
                'account' => $workorderManager->getAccountForWorkorder($workorder),
                'workorder' => $workorder,
            ]
        );
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function createGenericWorkorderAction(Account $account, Request $request)
    {
        /** @var Creator $workorderCreator */
        $workorderCreator = $this->get('workorder.creator.service');
        /** @var Files $fileService */
        $fileService = $this->get('app.files');
        /** @var Helper $coordinateHelper */
        $coordinateHelper = $this->get('app.coordinates_helper.service');

        $form = $this->createForm(CustomProposalOrWorkorderType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //set coordinates by Account Address
            $coordinateHelper->refreshByAccount($account);

            $formData = $form->getData();
            $properties = [];
            $properties['currentUser'] = $this->get('security.token_storage')->getToken()->getUser();
            if (isset($formData['proposalFile']) && !empty($formData['proposalFile'])) {
                $properties['file'] = $fileService->saveUploadFile($formData['proposalFile'], '/uploads/workorders');
            }
            $workorder = $workorderCreator->createGeneric(
                $account,
                $formData['division'],
                $properties
            );

            //log record
            /** @var LogCreator $woLogCreator */
            $woLogCreator = $this->get('workorder.log_creator');
            $woLogCreator->createRecord($workorder, [
                'type' => WorkorderLogType::TYPE_WORKORDER_WAS_CREATED,
                'message' => "Generic Workorder was created",
                'changingStatus' => true
            ]);
        }

        return $this->redirectToRoute('admin_account_workorders_get', ['account' => $account->getId()]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function createGenericWorkorderByCustomProposalAction(Proposal $proposal, Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var Creator $workorderCreator */
        $workorderCreator = $this->get('workorder.creator.service');
        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->get('app.proposal_log.manager');
        /** @var Helper $coordinateHelper */
        $coordinateHelper = $this->get('app.coordinates_helper.service');
        /** @var Account $account */
        $account = $proposal->getAccount();

        $form = $this->createForm(WorkorderType::class, [], [
            'account' => $proposal->getAccount()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //set coordinates by Account Address
            $coordinateHelper->refreshByAccount($account);

            $properties = [];
            $properties['currentUser'] = $this->get('security.token_storage')->getToken()->getUser();
            $properties['proposal'] = $proposal;
            /** @var Workorder $workorder */
            $workorder = $workorderCreator->createGeneric(
                $account,
                $proposal->getDivision(),
                $properties
            );

            $proposalManager->setStatus($proposal, 'workorder_created');
            $proposal->setWorkorder($workorder);
            $proposalManager->update($proposal);

            /** @var LogComments $logComments */
            $logComments = $this->get('app.log.comments');
            $comment = $logComments->parseByParams($form->getData());

            //Proposal log record
            $proposalLogManager->create(
                $proposal,
                'Proposal was added to Workorder',
                $comment
            );

            //WO log record
            /** @var LogCreator $woLogCreator */
            $woLogCreator = $this->get('workorder.log_creator');
            $woLogCreator->createRecord($workorder, [
                'type' => WorkorderLogType::TYPE_WORKORDER_WAS_CREATED,
                'proposal' => $proposal,
                'message' => "Custom proposal was added to Generic Workorder",
                'comment' => $comment,
                'changingStatus' => true
            ]);

            return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
        }

        return $this->redirectToRoute('admin_custom_proposal_view', ['proposal' => $proposal->getId()]);
    }

    /**
     * @param $name
     * @return BinaryFileResponse
     */
    public function viewFrozenPdfAction($name)
    {
        $appPath = $this->getParameter('kernel.root_dir');
        $uploadsPath = realpath($appPath . '/../web/uploads/workorders');

        return new BinaryFileResponse("$uploadsPath/$name");
    }

    /**
     * @param Workorder $workorder
     * @return BinaryFileResponse|RedirectResponse
     */
    public function viewMergeWorkorderReportsAction(Workorder $workorder)
    {
        try {
            /** @var ReportMerger $reportMerger */
            $reportMerger = $this->get('workorder.report_merger.service');

            $nameMergedReport = $reportMerger->merge($workorder);

            $appPath = $this->getParameter('kernel.root_dir');
            $uploadsPath = realpath($appPath . '/../web/uploads/workorders_merged_reports');

            return new BinaryFileResponse("$uploadsPath/$nameMergedReport");
        } catch (\Exception $exception) {
            $this->get('session')->getFlashBag()->add(
                'failed',
                $exception->getMessage()
            );
            return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
        }
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function deleteAction(Workorder $workorder, Request $request)
    {
        /** @var WorkorderManager $workorderManager */
        $workorderManager = $this->get("app.work_order.manager");
        /** @var MSCalendarToken $msCalendarToken */
        $msCalendarToken = $this->get('app.ms_calendar_token.service');
        /** @var EventManager $eventManager */
        $eventManager = $this->get('app.event.manager');
        /** @var TechnicianAssignmentSyncService $technicianAssignmentSyncService */
        $technicianAssignmentSyncService = $this->get('app.technician_assignment_sync.service');

        $workorderEvents = $workorder->getEvents();
        $comment = $request->request->get('comment');

        $workorderStatusAlias = $workorder->getStatus()->getAlias();
        $canNotDeleteStatusesAlias = ["done", "delete"];

        if (in_array($workorderStatusAlias, $canNotDeleteStatusesAlias)) {
            $this->get('session')->getFlashBag()->add('failed', 'You can not delete this Workorder');

            return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
        }

        if (empty(trim($request->get('comment')))) {
            $this->get('session')->getFlashBag()->add('failed', 'Please leave your comment below describing
             the reason why you would like to delete the workorder and confirm the action.');

            return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
        }

        //deleting events from admin panel and from MOcalendar
        foreach ($workorderEvents as $event) {
            /** @var Event $deletedEvent */
            $deletedEvent = clone $event;
            $eventManager->delete($event);

            $redirectUri = $this->generateUrl(
                'admin_assign_technician_workorder',
                ['workorder' => $deletedEvent->getWorkorder()->getId()]
            );

            $accessToken = $msCalendarToken->getAccessToken($redirectUri);

            if (!$accessToken) {
                $msCalendarLoginUrl = $msCalendarToken->getLoginUrl();
                return $this->redirect($msCalendarLoginUrl);
            }

            try {
                $msOutlookCalendarEventId = $event->getMsOutlookCalendarEventId();
                if ($msOutlookCalendarEventId) {
                    $technicianAssignmentSyncService->deleteEvent(
                        $accessToken,
                        $event->getMsOutlookCalendarEventId()
                    );
                }
            } catch (\Exception $exception) {
                $this->redirect($request->headers->get('referer'));
            }
            //end Delete Event from Microsoft Outlook Calendar

            /** @var LogMessage $logMessage */
            $logMessage = $this->get('workorder.log_message');
            /** @var LogCreator $woLogCreator */
            $woLogCreator = $this->get('workorder.log_creator');
            $woLogCreator->createRecord($deletedEvent->getWorkorder(), [
                'type' => WorkorderLogType::TYPE_WORKORDER_TO_BE_SCHEDULED,
                'message' => $logMessage->makeByDeleteEvent($deletedEvent),
            ]);
        }


        $workorderManager->deleteWorkorder($workorder, $comment);

        return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
    }


    /**
     * @param Workorder $workorder
     * @return Response
     */
    public function viewMapForAssignTechnicianAction(Workorder $workorder)
    {
        return $this->render(
            '@Admin/Workorder/view-map.html.twig',
            [
                "workorder" => $workorder,
                "user" => $this->getUser()
            ]
        );
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function rescheduleAction(Workorder $workorder, Request $request)
    {
        $params['workorder'] = $workorder;
        if (!$this->isGranted(WorkOrderVoter::CAN_RESCHEDULING_WO, $params)) {
            return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
        }

        /** @var WorkorderManager $workorderManager */
        $workorderManager = $this->get("app.work_order.manager");
        $comment = $request->request->get('comment');

        if (empty(trim($request->get('comment')))) {
            $this->get('session')->getFlashBag()->add('failed', 'Please leave your comment below describing
             the reason why you would like to rescheduling the workorder and confirm the action.');
            return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
        }

        $workorderManager->reschedule($workorder, $comment);

        return $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse
     */
    public function workorderCommentCreateAction(Workorder $workorder, Request $request)
    {
        /** @var CommentService $workorderCommentService */
        $workorderCommentService = $this->get('workorder.comment.service');
        /** @var RedirectResponse $response */
        $response = $this->redirectToRoute('admin_workorder_view', ['workorder' => $workorder->getId()]);

        $form = $this->createForm(WorkorderCommentType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $workorderCommentService->creatingComment($form, $workorder);
        }

        return $response;
    }

    /**
     * @param WorkorderComment $workorderComment
     * @return BinaryFileResponse
     */
    public function viewCommentFileAction(WorkorderComment $workorderComment)
    {
        $appPath = $this->getParameter('kernel.root_dir');
        $uploadsPath = realpath($appPath . '/../web');

        return new BinaryFileResponse("$uploadsPath/{$workorderComment->getPhoto()}");
    }

}
