<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Event\Helper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EventController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Event $event
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeCanFinishWoAction(Workorder $workorder, Event $event)
    {
        /** @var Helper $eventHelperService */
        $eventHelperService = $this->get('app.event_helper.service');

        $eventHelperService->changeCanFinishWo($workorder, $event);

        return $this->redirectToRoute('admin_assign_technician_workorder', ['workorder' => $workorder->getId()]);
    }
}
