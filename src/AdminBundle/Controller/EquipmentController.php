<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Repository\EquipmentRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Equipment;
use AppBundle\Entity\ContractorUser;

class EquipmentController extends Controller
{

    public function listAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');

        /** @var EquipmentRepository $equipmentRepository */
        $equipmentRepository = $objectManager->getRepository("AppBundle:Equipment");

        $equipments = $equipmentRepository->findAllWithPagination($request->get('page'));

        return $this->render('@Admin/Equipment/list.html.twig', [
            'equipmentList' => $equipments,
            'equipmentCount' => count($equipmentRepository->findAll())
        ]);

    }
}
