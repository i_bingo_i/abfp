<?php

namespace AdminBundle\Controller;

use AppBundle\Creators\AccountContactPersonFormCreator;
use AppBundle\Creators\CompanyFormCreator;
use AppBundle\DTO\Requests\CreateCompanyAndLinkedToAccountDTO;
use AppBundle\DTO\Requests\CreateCompanyAndLinkedToCPDTO;
use AppBundle\DTO\Requests\SaveCompanyRequest;
use AppBundle\Entity\ContactPerson;
use AppBundle\Services\AccountContactPerson\RolesService;
use AppBundle\Services\Search;
use InvoiceBundle\Entity\Customer;
use AppBundle\Entity\EntityManager\CompanyManager;
use AppBundle\Entity\EntityManager\ContactPersonHistoryManager;
use AppBundle\Entity\EntityManager\ContactPersonManager;
use AppBundle\Entity\Repository\AccountRepository;
use InvoiceBundle\Repository\CustomerRepository;
use AppBundle\Parsers\Company\Form\CreateAndLinkedToAccountParser;
use AppBundle\Parsers\Company\Form\CreateParser;
use AppBundle\Services\Account\Grouping;
use AppBundle\ActionManagers\CompanyActionManager;
use InvoiceBundle\Services\Customer\Provider\CustomerCompanyProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\Company;
use AppBundle\Entity\Account;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\EntityManager\AccountHistoryManager;
use AppBundle\Entity\Repository\CompanyRepository;
use AppBundle\Entity\Repository\ContactPersonRepository;
use AppBundle\Parsers\Company\Form\CreateAndLinkedToCpParser;

class CompanyController extends Controller
{
    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        /** @var CompanyFormCreator $companyFormCreator */
        $companyFormCreator = $this->get("company.form.creator");
        $form = $companyFormCreator->makeFormCreate();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CreateParser $formCreateParser */
            $formCreateParser = $this->get("company.form_create.parser");
            /** @var SaveCompanyRequest $saveCompanyRequest */
            $saveCompanyRequest = $formCreateParser->parse($form);

            /** @var CompanyActionManager $companyActionManager */
            $companyActionManager = $this->get("company.action.manager");
            /** @var Company $newCompany */
            $newCompany = $companyActionManager->create($saveCompanyRequest);

            return $this->redirectToRoute('admin_company_get', ['company' => $newCompany->getId()]);
        }

        return $this->render('@Admin/Company/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function createAndLinkedToContactPersonAction(ContactPerson $contactPerson, Request $request)
    {
        /** @var CompanyFormCreator $companyFormCreator */
        $companyFormCreator = $this->get("company.form.creator");
        $form = $companyFormCreator->makeFormCreate();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CreateAndLinkedToCpParser $parser */
            $parser = $this->get("company.form_create_and_linked_to_cp.parser");
            /** @var CreateCompanyAndLinkedToCPDTO $createAndLinkedToCpRequest */
            $createAndLinkedToCpRequest = $parser->parse($form, $contactPerson);

            /** @var CompanyActionManager $companyActionManager */
            $companyActionManager = $this->get("company.action.manager");
            $companyActionManager->createAndLinkedCP($createAndLinkedToCpRequest);

            return $this->redirectToRoute('admin_contact_person_get', [
                'contactPerson' => $contactPerson->getId()
            ]);
        }

        return $this->render('@Admin/Company/create_and_linked_to_contact_person.html.twig', [
            'form' => $form->createView(),
            'contactPerson' => $contactPerson
        ]);
    }

    /**
     * @param Company $company
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \QuickbooksBundle\Exceptions\FailIQuickbooksAction
     */
    public function updateAction(Company $company, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $company);

        /** @var CompanyFormCreator $companyFormCreator */
        $companyFormCreator = $this->get("company.form.creator");
        $formUpdateCompany = $companyFormCreator->makeFormUpdate($company);

        $formUpdateCompany->handleRequest($request);

        if ($formUpdateCompany->isSubmitted() && $formUpdateCompany->isValid()) {
            /** @var CreateParser $formCreateParser */
            $formCreateParser = $this->get("company.form_create.parser");
            /** @var SaveCompanyRequest $saveCompanyRequest */
            $saveCompanyRequest = $formCreateParser->parse($formUpdateCompany);

            /** @var CompanyActionManager $companyActionManager */
            $companyActionManager = $this->get("company.action.manager");
            /** @var Company $updatedCompany */
            $updatedCompany = $companyActionManager->update($saveCompanyRequest);

            return $this->redirectToRoute('admin_company_get', [
                'company' => $updatedCompany->getId()
            ]);
        }

        return $this->render('@Admin/Company/update.html.twig', [
            'form' => $formUpdateCompany->createView()
        ]);
    }

    /**
     * @param Company $company
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAction(Company $company, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var Grouping $accountGrouping */
        $accountGrouping = $this->get('app.account_grouping.service');
        /** @var ContactPersonRepository $contactPersonRepository */
        $contactPersonRepository = $objectManager->getRepository("AppBundle:ContactPerson");
        /** @var AccountRepository $accountRepository */
        $accountRepository = $objectManager->getRepository("AppBundle:Account");
        /** @var CompanyRepository $companyRepository */
        $companyRepository = $objectManager->getRepository("AppBundle:Company");
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $objectManager->getRepository('InvoiceBundle:Customer');
        /** @var Customer $customer */
        $customer = $customerRepository->findByCompany($company);

        $contactPersonPage = $request->get('c_page') ? $request->get('c_page') : 1;
        $childrenCompaniesPage = $request->get('ch_page') ? $request->get('ch_page') : 1;

        $accountResults = $accountRepository->findByCompanyWithoutPagination($company);
        $accountsCount = count($accountResults);
        $accountResults = $accountGrouping->grouping($accountResults);

        return $this->render('@Admin/Company/detail.html.twig', [
            'company' => $company,
            'contacts' => $contactPersonRepository->findByCompany($company, $contactPersonPage),
            'accounts' => $accountResults,
            'accountsCount' => $accountsCount,
            'children' => $companyRepository->findByParent($company, $childrenCompaniesPage),
            'customer' => $customer
        ]);
    }

    /**
     * @param Company $company
     * @return RedirectResponse
     */
    public function deleteAction(Company $company)
    {
        /** @var CompanyManager $companyManager */
        $companyManager = $this->get('app.company.manager');

        $this->denyAccessUnlessGranted('delete', $company);
        $companyManager->delete($company);

        return $this->redirectToRoute('admin_company_get', [
            'company' => $company->getId()
        ]);
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAndLinkedToAccountAction(Account $account, Request $request)
    {
        /** @var CompanyFormCreator $companyFormCreator */
        $companyFormCreator = $this->get("company.form.creator");
        $form = $companyFormCreator->makeFormCreate();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CreateAndLinkedToAccountParser $parser */
            $parser = $this->get("company.form_create_and_linked_to_account.parser");
            /** @var CreateCompanyAndLinkedToAccountDTO $createAndLinkedToAccountRequest */
            $createAndLinkedToAccountRequest = $parser->parse($form, $account);

            /** @var CompanyActionManager $companyActionManager */
            $companyActionManager = $this->get("company.action.manager");
            $companyActionManager->createAndLinkedToAccount($createAndLinkedToAccountRequest);

            /** @var  AccountHistoryManager $accountHistoryManager */
            $accountHistoryManager = $this->get("app.account_history.manager");
            $accountHistoryManager->createAccountHistoryItem($account);

            return $this->redirectToRoute('admin_account_get', [
                "accountId" => $account->getId()
            ]);
        }

        return $this->render('@Admin/Company/create_and_linked_to_account.html.twig', [
            'form' => $form->createView(),
            'account' => $account
        ]);
    }

    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @param Request $request
     * @return Response
     */
    public function searchCompanyForSelectToACPAction(Account $account, ContactPerson $contactPerson, Request $request)
    {
        /** @var Search $searchService */
        $searchService = $this->get('app.search');
        /** @var array $personCompany */
        $existCompany = [];

        if ($request->get('personCompany')) {
            $existCompany[] = $request->get('personCompany');
        }

        if ($request->get('accountCompany')) {
            $existCompany[] = $request->get('accountCompany');
        }

        if ($request->get('otherCompany')) {
            $existCompany[] = $request->get('otherCompany');
        }

        $this->denyAccessUnlessGranted('search_company', $account);
        $searchOptions = $request->query->all();

        return $this->render('@Admin/Company/search_company_for_select.html.twig', [
            'account' => $account,
            'contactPerson' => $contactPerson,
            'searchResult' => $searchService->searchCompany($searchOptions),
            'existCompany' => $existCompany
        ]);
    }
}
