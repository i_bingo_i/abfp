<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ContractorUserType;
use AdminBundle\Form\UserType;
use AppBundle\Entity\ContractorUser;
use AppBundle\Entity\EntityManager\ContractorUserManager;
use AppBundle\Entity\EntityManager\UserManager;
use AppBundle\Entity\Repository\ContractorUserRepository;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Services\ContractorUser\Creator;
use AppBundle\Validators\UserValidator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

class UserController extends Controller
{

    /**
     * @param User $user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(User $user, Request $request)
    {
        /** @var UserManager $userManager */
        $userManager = $this->get('app.user.manager');
        /** @var UserValidator $userValidator */
        $userValidator = $this->get('app.user_validator.service');

        $formUpdateUser = $this->createForm(
            UserType::class,
            $user,
            ['validation_groups' => ['update']]
        );

        $formUpdateUser->handleRequest($request);
        /** @var User $user */
        $user = $formUpdateUser->getData();

        $userErrors = $userValidator->update($user);
        foreach ($userErrors as $userError) {
            $error = new FormError($userError->getMessage());
            $formUpdateUser->addError($error);
        }

        $checkNotUniqueEmail = $userValidator->checkNotUniqueEmail($user);
        if ($checkNotUniqueEmail) {
            $error = new FormError($checkNotUniqueEmail);
            $formUpdateUser->addError($error);
        }

        if ($formUpdateUser->isSubmitted() && $formUpdateUser->isValid()) {
            $userManager->setAccessToken($user);
            $userManager->save($user);

            return $this->redirectToRoute('admin_user_list');
        }

        return $this->render('@Admin/User/update.html.twig', [
            'form' => $formUpdateUser->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        /** @var UserValidator $userValidator */
        $userValidator = $this->get('app.user_validator.service');
        /** @var Creator $contractorUserCreator */
        $contractorUserCreator = $this->get('app.contractor_user_creator.service');
        /** @var ContractorUser $contractorUser */
        $contractorUser = $contractorUserCreator->prepareCreate();
        /** @var UserManager $userManager */
        $userManager = $this->get('app.user.manager');
        /** @var ContractorUserManager $contractorUserManager */
        $contractorUserManager = $this->get('app.contractor_user.manager');

        $formCreateContractorUser = $this->createForm(
            ContractorUserType::class,
            $contractorUser,
            ['validation_groups' => ['create']]
        );
        $formCreateContractorUser->handleRequest($request);
        /** @var ContractorUser $contractorUser */
        $contractorUser = $formCreateContractorUser->getData();
        /** @var User $user */
        $user = $contractorUser->getUser();

        if ($formCreateContractorUser->isSubmitted()) {
            $userErrors = $userValidator->create($user);
            foreach ($userErrors as $userError) {
                $error = new FormError($userError->getMessage());
                $formCreateContractorUser->addError($error);
            }

            $checkNotUniqueEmail = $userValidator->checkNotUniqueEmail($user);
            if ($checkNotUniqueEmail) {
                $error = new FormError($checkNotUniqueEmail);
                $formCreateContractorUser->addError($error);
            }

            if ($formCreateContractorUser->isValid()) {
                $userManager->setRoles($user);
                $userManager->setAccessToken($user);
                $userManager->save($user);
                $contractorUserManager->create($contractorUser);

                return $this->redirectToRoute('admin_user_list');
            }
        }

        return $this->render('@Admin/User/create.html.twig', [
            'form' => $formCreateContractorUser->createView()
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var UserRepository $userRepository */
        $userRepository = $objectManager->getRepository('AppBundle:User');
        /** @var User[] $allUsers */
        $allUsers = $userRepository->findBy(["deleted" => false]);

        return $this->render('AdminBundle:User:list.html.twig', ['users' => $allUsers]);
    }

    /**
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(User $user)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var UserManager $userManager */
        $userManager = $this->get('app.user.manager');
        /** @var ContractorUserManager $contractorUserManager */
        $contractorUserManager = $this->get('app.contractor_user.manager');
        /** @var ContractorUserRepository $contractorUserRepository */
        $contractorUserRepository = $objectManager->getRepository('AppBundle:ContractorUser');
        /** @var ContractorUser $contractorUser */
        $contractorUser = $contractorUserRepository->findOneBy(['user' => $user]);

        $userManager->delete($user);
        $contractorUserManager->delete($contractorUser);

        return $this->redirectToRoute('admin_user_list');
    }
}
