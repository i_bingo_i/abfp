<?php

namespace AdminBundle\Controller;

use AppBundle\ActionManagers\AccountContactPersonActionManger;
use AppBundle\Creators\AccountContactPersonFormCreator;
use AppBundle\Services\Account\ErrorMessagesService;
use AppBundle\Services\AccountContactPerson\RolesService;
use AppBundle\Services\Address\AddressProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\Account;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\AccountContactPerson;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Services\AccountContactPersonCompose;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;

class AccountContactPersonController extends Controller
{
    /**
     * @param Account $account
     * @return Response
     */
    public function manageAction(Account $account)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountContactPersonRepository $accountContactPersonRepository */
        $accountContactPersonRepository = $objectManager->getRepository('AppBundle:AccountContactPerson');
        /** @var AccountContactPersonCompose $accountContactPersonCompose */
        $accountContactPersonCompose = $this->get('app.account_contact_person_compose');
        /** @var ErrorMessagesService $accountErrorMessagesService */
        $accountErrorMessagesService = $this->get('app.account_error_messages.service');

        /** @var AccountContactPerson[] $accountContactPersons */
        $accountContactPersons = $accountContactPersonRepository->findBy(['account' => $account, 'deleted' => false]);

        return $this->render('@Admin/AccountContactPerson/manage.html.twig', [
            'contacts' => $accountContactPersonCompose->compose($accountContactPersons),
            'errorMessages' => $accountErrorMessagesService->getErrorMessages($account),
            'account' => $account
        ]);
    }

    /**
     * @param Request $request
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @return RedirectResponse|Response
     */
    public function setRolesAction(Request $request, Account $account, ContactPerson $contactPerson)
    {
        $this->denyAccessUnlessGranted('set_roles', $account);
        $this->denyAccessUnlessGranted('set_roles', $contactPerson);

        /** @var RolesService $accountContactPersonRoleService */
        $accountContactPersonRoleService = $this->get('app.account_contact_person_roles.service');
        /** @var AddressProvider $addressProvider */
        $addressProvider = $this->get('app.address.provider');
        /** @var AccountContactPersonFormCreator $formBuilder */
        $formBuilder = $this->get('app.account_contact_person.form.creator');

        $form = $formBuilder->makeFormSetRoles($request);
        $accountContactPerson = $formBuilder->accountContactPerson;
        $addressesList = $addressProvider->getPersonalBillingAddresses(
            $accountContactPerson,
            $formBuilder->customers,
            $formBuilder->otherCompany
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $accountContactPersonRoleService->updateCustomBillingAddress(
                $accountContactPerson,
                $form->get('billingAddress')->getData(),
                $form->get('check')->getData()
            );
            $accountContactPersonRoleService->setRoleAndResponsibility($accountContactPerson);

            return $this->redirectToRoute('admin_account_contact_person_manage', ['account' => $account->getId()]);
        }

        return $this->render('@Admin/AccountContactPerson/set_roles.html.twig', [
            'form' => $form->createView(),
            'account' => $account,
            'contactPerson' => $contactPerson,
            'accountContactPerson' => $accountContactPerson,
            'addressesList' => $addressesList,
            'existCompany' => $formBuilder->getExistCompaniesForACP()
        ]);
    }

    // TODO: add history
    /**
     * @param Account $account
     * @param ContactPerson $contactPerson
     * @return RedirectResponse
     */
    public function deleteAccountContactPersonAction(Account $account, ContactPerson $contactPerson)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var AccountContactPersonRepository $accountContactPersonRepository */
        $accountContactPersonRepository = $objectManager->getRepository('AppBundle:AccountContactPerson');
        /** @var AccountContactPersonManager $accountContactPersonManager */
        $accountContactPersonManager = $this->get('app.account_contact_person.manager');
        /** @var AccountContactPersonActionManger $accountContactPersonActionManager */
        $accountContactPersonActionManager = $this->get('app.account_contact_person.action_manager');

        /** @var AccountContactPerson $accountContactPerson */
        $accountContactPerson = $accountContactPersonRepository->findOneBy([
            'account' => $account,
            'contactPerson' => $contactPerson
        ]);

        $accountContactPersonActionManager->resetCustomerToContactPerson($accountContactPerson);

        $accountContactPersonManager->delete($accountContactPerson);

        return $this->redirectToRoute('admin_account_contact_person_manage', ['account' => $account->getId()]);
    }
}
