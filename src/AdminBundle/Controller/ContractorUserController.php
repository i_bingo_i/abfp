<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\ContractorUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\EntityManager\ContractorUserManager;

class ContractorUserController extends Controller
{
    public function viewAction(ContractorUser $contractorUser)
    {
        /** @var ContractorUserManager $contractorUserManager */
        $contractorUserManager = $this->get("app.contractor_user.manager");

        return $this->render(
            'AdminBundle:ContractorUser:view.html.twig',
            [
                'contractorUser' => $contractorUser,
                'licenses' => $contractorUserManager->getContractorUserLicenses($contractorUser),
            ]
        );
    }
}
