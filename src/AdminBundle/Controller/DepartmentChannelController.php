<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\DepartmentChannel;
use AppBundle\Entity\EntityManager\MessageManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\DepartmentChannelType;
use AppBundle\Entity\Repository\DepartmentRepository;
use AppBundle\Entity\Municipality;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Department;
use AppBundle\Entity\ChannelNamed;
use AppBundle\Entity\Agent;

use AppBundle\Entity\EntityManager\DepartmentChannelManager;

class DepartmentChannelController extends Controller
{
    /**
     * @param Department $department
     * @param ChannelNamed $channelNamed
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Department $department, ChannelNamed $channelNamed, Request $request)
    {
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->get('app.department_channel.manager');
        /** @var DepartmentChannel $newDepartmentChannel */
        $newDepartmentChannel = $departmentChannelManager->prepareToCreate($department, $channelNamed);

        $form = $this->createForm(
            DepartmentChannelType::class,
            $newDepartmentChannel,
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newDepartmentChannel = $departmentChannelManager->filteringFax($newDepartmentChannel);
            if ($newDepartmentChannel->getIsDefault()) {
                $departmentChannelManager->unDefaultDepartmentChannels($department);
            }

            $departmentChannelManager->create($newDepartmentChannel);
            if ($newDepartmentChannel->getIsDefault()) {
                $this->get('app.service.manager')->setDepartmentChannel($newDepartmentChannel);
//                $this->get('app.message.manager')->isMunicipalityComplianceChannelByDepartment(
//                    $newDepartmentChannel->getDepartment()
//                );
            }

            return $this->redirectToRoute('admin_municipality_view', [
                'municipality' => $department->getMunicipality()->getId()
            ]);
        }

        return $this->render('@Admin/Channel/department_channel_create.html.twig', [
            'form' => $form->createView(),
            'department' => $department,
            'channelNamed' => $channelNamed
        ]);
    }

     /**
     * @param DepartmentChannel $departmentChannel
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(DepartmentChannel $departmentChannel, Request $request)
    {
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->get('app.department_channel.manager');
        $departmentChannelManager->deleteChannel($departmentChannel);

        $redirectUrl = $request->headers->get('referer')
            ? $this->redirect($request->headers->get('referer'))
            : $this->redirectToRoute('admin_municipality_list');

        return $redirectUrl;
    }

    /**
     * @param Department $department
     * @param ChannelNamed $channelNamed
     * @param Agent $agent
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createChannelForDepartmentAgentAction(
        Department $department,
        ChannelNamed $channelNamed,
        Agent $agent,
        Request $request
    ) {
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->get('app.department_channel.manager');
        /** @var DepartmentChannel $newDepartmentChannel */
        $newDepartmentChannel = $departmentChannelManager->prepareToCreateForAgent($department, $channelNamed, $agent);

        $form = $this->createForm(
            DepartmentChannelType::class,
            $newDepartmentChannel,
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newDepartmentChannel = $departmentChannelManager->filteringFax($newDepartmentChannel);
            if ($newDepartmentChannel->getIsDefault()) {
                $departmentChannelManager->unDefaultDepartmentChannels($department, $agent);
            }
            $departmentChannelManager->save($newDepartmentChannel);

            return $this->redirectToRoute('admin_municipality_view', [
                'municipality' => $department->getMunicipality()->getId()
            ]);
        }

        return $this->render('@Admin/Channel/department_channel_create.html.twig', [
            'form' => $form->createView(),
            'department' => $department,
            'channelNamed' => $channelNamed
        ]);
    }

    /**
     * @param DepartmentChannel $departmentChannel
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateDepartmentChannelAction(DepartmentChannel $departmentChannel, Request $request)
    {
        $departmentChannelManager = $this->get('app.department_channel.manager');

        $departmentChannel = $departmentChannelManager->setDefaultFeesBasicForDepartment($departmentChannel);

        $form = $this->createForm(
            DepartmentChannelType::class,
            $departmentChannel,
            ['validation_groups' => ['update']]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $departmentChannel = $departmentChannelManager->filteringFax($departmentChannel);
            if ($departmentChannel->getIsDefault()) {
                if ($departmentChannel->getOwnerAgent()) {
                    $departmentChannelManager->unDefaultDepartmentChannels(
                        $departmentChannel->getDepartment(),
                        $departmentChannel->getOwnerAgent()
                    );
                } else {
                    $departmentChannelManager->unDefaultDepartmentChannels($departmentChannel->getDepartment());
                }
                $departmentChannel->setIsDefault(true);
                $this->get('app.service.manager')->setDepartmentChannel($departmentChannel);
            }
            $departmentChannelManager->save($departmentChannel);

            return $this->redirectToRoute('admin_municipality_view', [
                'municipality' => $departmentChannel->getDepartment()->getMunicipality()->getId()
            ]);
        }

        return $this->render('@Admin/Channel/department_channel_update.html.twig', [
            'form' => $form->createView(),
            'department' => $departmentChannel->getDepartment(),
            'channelNamed' => $departmentChannel->getNamed()
        ]);
    }
}
