<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\WorkorderLetterType;
use AppBundle\DTO\Requests\SendFormViaEmailDTO;
use AppBundle\Entity\EntityManager\ReportManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\Report;
use AppBundle\Entity\ReportStatus;
use AppBundle\Entity\Repository\ReportStatusRepository;
use AppBundle\Entity\Repository\RepositoryTrait\PaginateQueryTrait;
use AppBundle\Entity\Service;
use AppBundle\Entity\Workorder;
use AppBundle\Services\Letter\LetterActionManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use WorkorderBundle\Services\ComposerFormService;
use WorkorderBundle\Services\GetLetterRecipientService;
use WorkorderBundle\Services\SendReportsToAHJViaEmail;
use WorkorderBundle\Services\SendReportsToClientViaEmail;
use WorkorderBundle\Services\WorkorderFilesComposer;

class ReportController extends Controller
{
    use PaginateQueryTrait;

    /**
     * @param Service $service
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Service $service, Request $request)
    {
        /** @var ReportManager $reportManager */
        $reportManager = $this->get('app.report.manager');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');

        $form = $this->createFormBuilder([])
            ->add('isNeedRepair', CheckboxType::class)
            ->add('reportStatus', EntityType::class, [
                'class' => "AppBundle\Entity\ReportStatus",
            ])
            ->add('reportFile', FileType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $newReport = new Report();

            /** @var UploadedFile $pdfReport */
            $pdfReport = $formData["reportFile"];
            $saveFile = $reportManager->saveFile($pdfReport);
            $newReport->setStatus($formData["reportStatus"]);
            $newReport->setFileReport($saveFile);
            $newReport->setService($service);
            $newReport->setWorkorder($service->getWorkorder());
            $newReport->setIsNeedRepair($formData["isNeedRepair"]);

            $reportManager->create($newReport);

            $serviceManager->finishServiceActions($service, $newReport);
        }

        return $this->redirectToRoute('admin_workorder_view', ['workorder' => $service->getWorkorder()->getId()]);
    }

    /**
     * @param Service $service
     * @return RedirectResponse
     */
    public function finishSimpleServiceAction(Service $service)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ReportManager $reportManager */
        $reportManager = $this->get('app.report.manager');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var ReportStatusRepository $reportStatusRepository */
        $reportStatusRepository = $objectManager->getRepository('AppBundle:ReportStatus');

        /** @var ReportStatus $completedReportStatus */
        $completedReportStatus = $reportStatusRepository->findOneBy([
            'alias' => 'completed',
        ]);

        /** @var Workorder $workorder */
        $workorder = $service->getWorkorder();
        /** @var Report $newReport */
        $newReport = new Report();

        $newReport->setStatus($completedReportStatus);
        $newReport->setService($service);
        $newReport->setWorkorder($workorder);

        $reportManager->create($newReport);

        $serviceManager->finishServiceActions($service, $newReport);

        return $this->redirectToRoute('admin_workorder_view', [
            'workorder' => $workorder->getId()
        ]);
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function updateAction(Service $service, Request $request)
    {
        /** @var ReportManager $reportManager */
        $reportManager = $this->get('app.report.manager');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');

        $form = $this->createFormBuilder([])
            ->add('isNeedRepair', CheckboxType::class)
            ->add('reportStatus', EntityType::class, [
                'class' => "AppBundle\Entity\ReportStatus",
            ])
            ->add('reportFile', FileType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $newReport = new Report();

            /** @var UploadedFile $pdfReport */
            $pdfReport = $formData["reportFile"];
            $saveFile = $reportManager->saveFile($pdfReport);
            $newReport->setStatus($formData["reportStatus"]);
            $newReport->setFileReport($saveFile);
            $newReport->setService($service);
            $newReport->setWorkorder($service->getWorkorder());
            $newReport->setIsNeedRepair($formData["isNeedRepair"]);

            $reportManager->create($newReport);

            $serviceManager->finishServiceActions($service, $newReport, false);
        }

        return $this->redirectToRoute('admin_workorder_view', ['workorder' => $service->getWorkorder()->getId()]);
    }


    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \exception
     */
    public function workorderSendReportToClientAction(Workorder $workorder, Request $request)
    {
        /** @var WorkorderFilesComposer $workorderFilesComposer */
        $workorderFilesComposer = $this->get("workorder.files.composer.service");
        /** @var ComposerFormService $workorderFormParserService */
        $workorderFormParserService = $this->get("workorder.compose.form.service");
        /** @var GetLetterRecipientService $getLetterRecipientService */
        $getLetterRecipientService = $this->get("workorder.get.letter.recipient.service");

        $composedData = $workorderFilesComposer->composeForSendReportsToClients($workorder);
        $letterRecipient = $getLetterRecipientService->getAuthorizerEmail($workorder);

        $form = $this->createForm(
            WorkorderLetterType::class,
            ["workorder" => $workorder, "composedData" => $composedData, "recipient" => $letterRecipient]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $letterFormSendViaEmailParser = $this->get("letter.form_send_via_email.parser");
            /** @var SendFormViaEmailDTO $sendInvoiceViaEmailDTO */
            $sendInvoiceViaEmailDTO = $letterFormSendViaEmailParser->parse($form->getData());

            /** @var SendReportsToClientViaEmail $sendReportsToClientViaEmailService */
            $sendReportsToClientViaEmailService = $this->get("workorder.send_reports_to_client_via_email.service");
            $sendReportsToClientViaEmailService->sendReportsToClientViaEmailProcess($sendInvoiceViaEmailDTO);

            return $this->redirectToRoute("admin_workorder_view", ["workorder" => $workorder->getId()]);
        }

        return $this->render('@Admin/Letter/emails/workorder/send_reports_to_client_via_email.html.twig', [
            'form' => $form->createView(),
            'typeEmail' => 'sent_via_email',
            'type' => 'report_to_client',
            'workorder' => $workorder
        ]);
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function workorderSendReportToAHJByEmailAction(Workorder $workorder, Request $request)
    {
        /** @var WorkorderFilesComposer $workorderFilesComposer */
        $workorderFilesComposer = $this->get("workorder.files.composer.service");
        /** @var ComposerFormService $workorderFormParserService */
        $workorderFormParserService = $this->get("workorder.compose.form.service");
        /** @var GetLetterRecipientService $getLetterRecipientService */
        $getLetterRecipientService = $this->get("workorder.get.letter.recipient.service");

        $composedData = $workorderFilesComposer->composeForSendReportsToAHJ($workorder);
        $letterRecipient = $getLetterRecipientService->getMunicipalityEmail($workorder);

        $form = $this->createForm(
            WorkorderLetterType::class,
            ["workorder" => $workorder, "composedData" => $composedData, "recipient" => $letterRecipient]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $letterFormSendViaEmailParser = $this->get("letter.form_send_via_email.parser");
            /** @var SendFormViaEmailDTO $sendInvoiceViaEmailDTO */
            $sendInvoiceViaEmailDTO = $letterFormSendViaEmailParser->parse($form->getData());
            /** @var SendReportsToAHJViaEmail $sendReportsToAHJViaEmailService */
            $sendReportsToAHJViaEmailService = $this->get("workorder.send_reports_to_ahj_via_email.service");
            $sendReportsToAHJViaEmailService->sendReportsToAHJViaEmailProcess($sendInvoiceViaEmailDTO);

            return $this->redirectToRoute("admin_workorder_view", ["workorder" => $workorder->getId()]);
        }

        return $this->render('@Admin/Letter/emails/workorder/send_reports_to_ahj_via_email.html.twig', [
            'form' => $form->createView(),
            'typeEmail' => 'sent_via_email',
            'type' => 'report_to_ahj',
            'workorder' => $workorder
        ]);
    }
}
