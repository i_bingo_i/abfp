<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\CustomProposalOrWorkorderType;
use AdminBundle\Form\InternalCommentType;
use AdminBundle\Form\ProposalGeneralInfoType;
use AdminBundle\Form\WorkorderType;
use AppBundle\Entity\Account;
use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\ContactPerson;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceCategory;
use AppBundle\Entity\DeviceNamed;
use AppBundle\Entity\EntityManager\AccountContactPersonManager;
use AppBundle\Entity\EntityManager\DepartmentChannelManager;
use AppBundle\Entity\EntityManager\MunicipalityManager;
use AppBundle\Entity\EntityManager\ProposalLogManager;
use AppBundle\Entity\EntityManager\ProposalManager;
use AppBundle\Entity\EntityManager\ProposalStatusManager;
use AppBundle\Entity\EntityManager\RepairDeviceInfoManager;
use AppBundle\Entity\EntityManager\ServiceManager;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Message;
use AppBundle\Entity\Municipality;
use AppBundle\Entity\Proposal;
use AppBundle\Entity\ProposalType;
use AppBundle\Entity\RepairDeviceInfo;
use AppBundle\Entity\Repository\AccountContactPersonRepository;
use AppBundle\Entity\Repository\ClientTypeRepository;
use AppBundle\Entity\Repository\DeviceCategoryRepository;
use AppBundle\Entity\Repository\MessageRepository;
use AppBundle\Entity\Repository\MunicipalityRepository;
use AppBundle\Entity\Repository\NotIncludedRepository;
use AppBundle\Entity\Repository\OpportunityRepository;
use AppBundle\Entity\Repository\ProposalLogRepository;
use AppBundle\Entity\Repository\ProposalStatusRepository;
use AppBundle\Entity\Repository\ServiceRepository;
use AppBundle\Entity\Repository\WorkorderRepository;
use AppBundle\Entity\Service;
use AppBundle\Events\ProposalCreateEvent;
use AppBundle\Security\DeviceVoter;
use AppBundle\Security\ErrorByAccountVoter;
use AppBundle\Security\FilterVoter;
use AppBundle\Security\ProposalCreatorVoter;
use AppBundle\Security\ProposalVoter;
use AppBundle\Security\ServiceVoter;
use AppBundle\Services\Account\Authorizer;
use AppBundle\Services\PreviousPageService;
use AppBundle\Services\Proposal\Creator;
use AppBundle\Services\Proposal\Division;
use AppBundle\Services\Proposal\Freeze;
use AppBundle\Services\Proposal\ProposalService;
use AppBundle\Services\Proposal\RecreateReport;
use AppBundle\Services\Proposal\Replace;
use AppBundle\Services\Proposal\ReportMerger;
use AppBundle\Services\Service\EstimationTime;
use AppBundle\Services\Tree\Grouping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Repository\ProposalRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Serializer\Serializer;
use AdminBundle\Form\RepairProposalType;
use AppBundle\Entity\EntityManager\DeviceManager;
use AppBundle\Entity\EntityManager\AccountManager;
use AppBundle\Entity\Repository\RepairDeviceInfo as RepairDeviceInfoRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use WorkorderBundle\Services\Helper;

class ProposalController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var ClientTypeRepository $clientTypeRepository */
        $clientTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        /** @var ProposalStatusManager $proposalStatusManager */
        $proposalStatusManager = $this->get('app.proposal_status.manager');

        $typesAlias = ["retest"];

        $divisions = $deviceCategory->findAll();
        $clientTypes = $clientTypeRepository->findAll();
        $filters = $request->query->all();
        $page = $request->query->get('page');
        $quantitiesItems = $proposalRepository->getQuantitiesWithFilters($filters, $typesAlias);

        // TODO: Код дублюється з таким кодом в createAction
        if (!array_key_exists('date', $filters)
            ||(empty($filters['date']['from']) || empty($filters['date']['to']))
        ) {
            $this->get('session')->getFlashBag()->add('error', '');
        }
        $error = !empty($this->get('session')->getFlashBag()->get('error')) ? false : true;

        $res = $proposalRepository->findAllWithPagination($filters, $typesAlias, $page);
        $res['results'] = $proposalManager->groupByAccount($res['results']);

        $proposalsForMerge = $proposalManager->isShowButtonForMergePDF($filters);

        return $this->render('AdminBundle:Proposal:list.html.twig', [
            'paginatedProposalList' => $res,
            'divisions' => $divisions,
            'proposalStatuses' => $proposalStatusManager->getForRetestNotices(),
            'clientTypes' => $clientTypes,
            'filters' => $filters,
            'quantitiesItems' => $quantitiesItems,
            'error' => $error,
            'proposalsForMerge' => $proposalsForMerge
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function retestNoticeHiddenListAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var ClientTypeRepository $clientTypeRepository */
        $clientTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        /** @var ProposalStatusManager $proposalStatusManager */
        $proposalStatusManager = $this->get('app.proposal_status.manager');

        $typesAlias = ["retest"];

        $divisions = $deviceCategory->findAll();
        $clientTypes = $clientTypeRepository->findAll();
        $filters = $request->query->all();
        $page = $request->query->get('page');
        $quantitiesItems = $proposalRepository->getQuantitiesWithFilters($filters, $typesAlias);

        // TODO: Код дублюється з таким кодом в createAction
        if (!array_key_exists('date', $filters)
            ||(empty($filters['date']['from']) || empty($filters['date']['to']))
        ) {
            $this->get('session')->getFlashBag()->add('error', '');
        }
        $error = !empty($this->get('session')->getFlashBag()->get('error')) ? false : true;

        $res = $proposalRepository->findAllWithPagination($filters, $typesAlias, $page);
        $res['results'] = $proposalManager->groupByAccount($res['results']);

        $proposalsForMerge = $proposalManager->isShowButtonForMergePDF($filters);

        return $this->render('AdminBundle:Proposal:retest-notice-hidden-list.html.twig', [
            'paginatedProposalList' => $res,
            'divisions' => $divisions,
            'proposalStatuses' => $proposalStatusManager->getForRetestNotices(),
            'clientTypes' => $clientTypes,
            'filters' => $filters,
            'quantitiesItems' => $quantitiesItems,
            'error' => $error,
            'proposalsForMerge' => $proposalsForMerge
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Twig\Error\Error
     */
    public function recreateRetestNoticesReportAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var RecreateReport $proposalRecreateReportService */
        $proposalRecreateReportService = $this->get('app.proposal.recreate_report.service');
        $typesAlias = ["retest"];
        $filters = $request->query->all();
        $page = $request->query->get('page');

        $retestNotices = $proposalRepository->findAllWithPagination($filters, $typesAlias, $page);

        if ($retestNotices['results']) {
            $proposalRecreateReportService->recreate($retestNotices['results']);
        }

        return $this->redirectToRoute("admin_retest_notice_hidden_list");
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function allProposalsAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var ClientTypeRepository $clientTypeRepository */
        $clientTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');
        /** @var ProposalStatusManager $proposalStatusManager */
        $proposalStatusManager = $this->get('app.proposal_status.manager');

        $divisions = $deviceCategory->findAll();
        $clientTypes = $clientTypeRepository->findAll();
        $filters = $request->query->all();
        $page = $request->query->get('page');

        $typesAlias = ["repair", "custom"];

        $quantitiesItems = $proposalRepository->getQuantitiesWithFilters($filters, $typesAlias);

        // TODO: Код дублюється з таким кодом в createAction
        if (!array_key_exists('date', $filters)
            ||(empty($filters['date']['from']) || empty($filters['date']['to']))
        ) {
            $this->get('session')->getFlashBag()->add('error', '');
        }
        $error = !empty($this->get('session')->getFlashBag()->get('error')) ? false : true;

        $res = $proposalRepository->findAllWithPagination($filters, $typesAlias, $page);
        $proposalsCount = count($res['results']);
        $res['results'] = $proposalManager->groupByAccount($res['results']);
        $proposalsForMerge = $proposalManager->isShowButtonForMergePDF($filters);

        return $this->render('AdminBundle:Proposal:allProposals.html.twig', [
            'paginatedProposalList' => $res,
            'divisions' => $divisions,
            'proposalStatuses' => $proposalStatusManager->getForRepairProposal(),
            'clientTypes' => $clientTypes,
            'filters' => $filters,
            'quantitiesItems' => $quantitiesItems,
            'error' => $error,
            'proposalsForMerge' => $proposalsForMerge,
            'proposalsCount' => $proposalsCount
        ]);
    }

    /**
     * @param Proposal $proposal
     * @return Response
     */
    public function viewRetestProposalAction(Proposal $proposal) : Response
    {
        $this->denyAccessUnlessGranted('retest', $proposal);
        /** @var Grouping $treeService */
        $treeService = $this->get('app.tree.service');
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalLogRepository $proposalLogRepository */
        $proposalLogRepository = $objectManager->getRepository("AppBundle:ProposalLog");
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository("AppBundle:Workorder");
        /** @var AccountContactPersonManager $accountContactPersonManager */
        $accountContactPersonManager = $this->get('app.account_contact_person.manager');
        /** @var Account $account */
        $account = $proposal->getAccount();
        /** @var DeviceCategory $division */
        $division = $proposal->getDivision();

        return $this->render("AdminBundle:Proposal:viewRetest.html.twig", [
            'proposal' => $proposal,
            'tree' => $treeService->getTreeForProposal($proposal),
            'logs' => $proposalLogRepository->findBy(['proposal' => $proposal], ['id' => 'DESC']),
            'workorder' => $proposal->getWorkorder(),
            'acp' => $accountContactPersonManager->accountContactPersonsForWOCreate($account),
            'suitableWOsForCreation' => $workorderRepository->findSuitableWOsForCreating($account, $division)
        ]);
    }

    /**
     * @param Proposal $proposal
     * @return Response
     */
    public function viewRepairProposalAction(Proposal $proposal) : Response
    {
        $this->denyAccessUnlessGranted('repair', $proposal);
        /** @var Grouping $treeService */
        $treeService = $this->get('app.tree.service');
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalLogRepository $proposalLogRepository */
        $proposalLogRepository = $objectManager->getRepository("AppBundle:ProposalLog");
        /** @var AccountContactPersonManager $accountContactPersonManager */
        $accountContactPersonManager = $this->get('app.account_contact_person.manager');
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository("AppBundle:Workorder");
        /** @var Account $account */
        $account = $proposal->getAccount();
        /** @var DeviceCategory $division */
        $division = $proposal->getDivision();

        return $this->render("AdminBundle:Proposal:viewRepair.html.twig", [
            'proposal' => $proposal,
            'tree' => $treeService->getTreeForProposal($proposal),
            'logs' => $proposalLogRepository->findBy(['proposal' => $proposal], ['id' => 'DESC']),
            'workorder' => $proposal->getWorkorder(),
            'acp' => $accountContactPersonManager->accountContactPersonsForWOCreate($account),
            'suitableWOsForCreation' => $workorderRepository->findSuitableWOsForCreating($account, $division)
        ]);
    }

    /**
     * @param Proposal $proposal
     * @return Response
     */
    public function viewCustomProposalAction(Proposal $proposal) : Response
    {
        $this->denyAccessUnlessGranted('custom', $proposal);
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalLogRepository $proposalLogRepository */
        $proposalLogRepository = $objectManager->getRepository("AppBundle:ProposalLog");
        /** @var AccountContactPersonManager $accountContactPersonManager */
        $accountContactPersonManager = $this->get('app.account_contact_person.manager');
        /** @var WorkorderRepository $workorderRepository */
        $workorderRepository = $objectManager->getRepository("AppBundle:Workorder");
        /** @var Account $account */
        $account = $proposal->getAccount();
        /** @var DeviceCategory $division */
        $division = $proposal->getDivision();

        return $this->render("AdminBundle:Proposal:viewCustom.html.twig", [
            'proposal' => $proposal,
            'logs' => $proposalLogRepository->findBy(['proposal' => $proposal], ['id' => 'DESC']),
            'acp' => $accountContactPersonManager->accountContactPersonsForWOCreate($account),
            'suitableWOsForCreation' => $workorderRepository->findSuitableWOsForCreating($account, $division)
        ]);
    }

    // TODO: Remove Account ID and refactor method

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Twig\Error\Error
     */
    public function quickCreateAction(Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var OpportunityRepository $opportunityRepository */
        $opportunityRepository = $objectManager->getRepository('AppBundle:Opportunity');
        /** @var Creator $proposalCreator */
        $proposalCreator = $this->get('app.proposal.creator.service');

        $filters = $request->query->get('filters') ?? [];
        if (!$this->isGranted(FilterVoter::CHECK_ON_CORRECT_FILTER_FOR_CREATING_RN, $filters)) {
            return $this->redirect($request->headers->get('referer'));
        }

        $page = $filters['page'] ?? null;
        $opportunitiesPagination = $opportunityRepository->findByFilterWithPagination($filters, $page);
        if (!$this->isGranted(
            ProposalCreatorVoter::CHECK_OPPORTUNITY_LIMIT_FOR_CREATING_RN,
            $opportunitiesPagination['count']
        )) {
            return $this->redirect($request->headers->get('referer'));
        }
        /** @var Proposal[] $proposals */
        $proposals = $proposalCreator->createByRetestOpportunity($opportunitiesPagination['results']);

        if ($proposals) {
            return $this->redirectToRoute('admin_proposal_list');
        }

        return $this->redirect($request->headers->get('referer'));
    }

    // TODO: Remove Account ID and refactor method

    /**
     * @param Request $request
     * @param Account $account
     * @return RedirectResponse
     * @throws \Twig\Error\Error
     */
    public function quickCreateForAccountAction(Request $request, Account $account)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var OpportunityRepository $opportunityRepository */
        $opportunityRepository = $objectManager->getRepository('AppBundle:Opportunity');
        /** @var Creator $proposalCreator */
        $proposalCreator = $this->get('app.proposal.creator.service');

        $filters = $request->query->get('filters') ?? [];
        if (!$this->isGranted(FilterVoter::CHECK_ON_CORRECT_FILTER_FOR_CREATING_RN, $filters)) {
            return $this->redirect($request->headers->get('referer'));
        }

        $opportunitiesPagination = $opportunityRepository->findByFilterWithPaginationForAccount($account, $filters);
        if (!$this->isGranted(
            ProposalCreatorVoter::CHECK_OPPORTUNITY_LIMIT_FOR_CREATING_RN,
            $opportunitiesPagination['count']
        )) {
            return $this->redirect($request->headers->get('referer'));
        }

        /** @var Proposal[] $proposals */
        $proposals = $proposalCreator->createByRetestOpportunity($opportunitiesPagination['results'], false);

        if ($proposals) {
            return $this->redirectToRoute('admin_account_reset_notices_get', ["account" => $account->getId()]);
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @return Response
     */
    public function manualCreateAction() : Response
    {
        return $this->render('AdminBundle:Proposal:manualCreate.html.twig', [
            'welcome' => 'Welcome to manual create action of proposal controller'
        ]);
    }

    /**
     * @param ContactPerson $contact
     * @param Request $request
     * @return Response
     */
    public function listByContactPersonAction(ContactPerson $contact, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var ProposalStatusRepository $proposalStatusesRepository */
        $proposalStatusRepository = $objectManager->getRepository('AppBundle:ProposalStatus');
        /** @var ClientTypeRepository $proposalTypes */
        $proposalTypeRepository = $objectManager->getRepository('AppBundle:ClientType');
        /** @var DeviceCategoryRepository $deviceCategory */
        $deviceCategory = $objectManager->getRepository('AppBundle:DeviceCategory');

        $divisions = $deviceCategory->findAll();
        $proposalStatuses = $proposalStatusRepository->findAll();
        $clientTypes = $proposalTypeRepository->findAll();
        $filters = $request->query->all();
        $filters["contactPerson"] = $contact->getId();
        $page = $request->query->get('page');
        $res = $proposalRepository->findAllWithPagination($filters, null, $page, $contact);
        $res['results'] = $proposalManager->groupByAccount($res['results']);

        return $this->render('AdminBundle:Proposal:listByContactPerson.html.twig', [
            'paginatedProposalList' => $res,
            'divisions' => $divisions,
            'proposalStatuses' => $proposalStatuses,
            'clientTypes' => $clientTypes,
            'filters' => $filters,
            'contactPerson' => $contact
        ]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return Response
     */
    public function clientWasCalledAction(Proposal $proposal, Request $request) : Response
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');

        $clientWasCalledStatus = 'client_was_called';
        if ($proposal->getStatus()->getAlias() != $clientWasCalledStatus) {
            $proposalManager->setStatus($proposal, $clientWasCalledStatus);
        }

        $comment = $request->request->get('comment');
        $this->get('app.proposal_log.manager')->create($proposal, 'Client was called', $comment);

        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";

        return $this->redirectToRoute("$proposalRoute", ['proposal' => $proposal->getId()]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse
     * @throws \Twig\Error\Error
     */
    public function rejectedByClientAction(Proposal $proposal, Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var Freeze $freezeProposalService */
        $freezeProposalService = $this->get('app.proposal.freeze.service');
        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->get('app.proposal_log.manager');

        if (!$proposal->getIsFrozen()) {
            $proposal = $freezeProposalService->freezing($proposal);
        }

        $rejectedByClientStatus = 'rejected_by_client';
        $proposalManager->setStatus($proposal, $rejectedByClientStatus);

        /** @var ProposalType $proposalTypeAlias */
        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalLogMessages = [
            "retest" => "Rejected by client",
            "repair" => "Proposal was rejected by client.",
            "custom" => "Proposal was rejected by client.",
        ];

        $comment = $request->request->get('comment');
        $proposalLogManager->create($proposal, $proposalLogMessages["$proposalTypeAlias"], $comment);

        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";

        return $this->redirectToRoute("$proposalRoute", [
            'proposal' => $proposal->getId()
        ]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse
     */
    public function discardAllRepairServicesAction(Proposal $proposal, Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->get('app.proposal_log.manager');

        $proposalManager->discardAllServices($proposal);

        $servicesDiscardedStatus = 'services_discarded';
        $proposalManager->setStatus($proposal, $servicesDiscardedStatus);

        $comment = $request->request->get('comment');
        $proposalLogManager->create($proposal, 'Proposal services are discarded', $comment);

        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";

        return $this->redirectToRoute("$proposalRoute", ['proposal' => $proposal->getId()]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse
     * @throws \Twig\Error\Error
     */
    public function deleteAction(Proposal $proposal, Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->get('app.proposal_log.manager');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var ServiceRepository $servicesRepository */
        $servicesRepository = $this->get('app.entity_manager')->getRepository('AppBundle:Service');

        /** @var Freeze $freezeProposalService */
        $freezeProposalService = $this->get('app.proposal.freeze.service');
        if (!$proposal->getIsFrozen()) {
            $proposal = $freezeProposalService->freezing($proposal);
        }

        $services = $servicesRepository->findBy(['proposal' => $proposal->getId()]);

        /** @var ProposalType $proposalTypeAlias */
        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";

        if (empty(trim($request->get('comment')))) {
            $this->get('session')->getFlashBag()->add('failed', 'Please leave your comment below describing
             the reason why you would like to delete the proposal and confirm the action.');

            return $this->redirectToRoute("$proposalRoute", ['proposal' => $proposal->getId()]);
        }

        //TODO: update пропозала по факту вызывается 2 раза подряд, один в setStatus, второй в этом action
        $proposalManager->setStatus($proposal, 'deleted');
        $proposalManager->update($proposal);

        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalLogMailMessages = [
            "retest" => "Retest Notice was deleted",
            "repair" => "Proposal was deleted.",
            "custom" => "Proposal was deleted."
        ];

        $proposalLogManager->create(
            $proposal,
            $proposalLogMailMessages["$proposalTypeAlias"],
            $request->get('comment')
        );

        $services = $serviceManager->detachProposalForCollection($services);
        $serviceManager->createOpportunityByService($services);

        return $this->redirectToRoute("$proposalRoute", ['proposal' => $proposal->getId()]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse
     */
    public function resetServicesToNextTimeAction(Proposal $proposal, Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->get('app.proposal_log.manager');
        if ($proposal->getStatus()->getAlias() != "reset_services_to_next_time") {
            $proposalManager->resetServicesToNextTime($proposal);

            $proposalManager->setStatus($proposal, 'reset_services_to_next_time');
            $proposalLogManager->create($proposal, "Services were reset to next time", $request->get('comment'));
        }

        $proposalTypeAlias = $proposal->getType()->getAlias();
        $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";

        return $this->redirectToRoute("$proposalRoute", ['proposal' => $proposal->getId()]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws string
     */
    public function getMergeProposalsReportsAction(Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ReportMerger $reportMerger */
        $reportMerger = $this->get('app.proposal_report_merger.service');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $this->get('app.entity_manager')->getRepository('AppBundle:Proposal');

        $filters = $request->query->all();
        /** @var Proposal[] $retestNotices */
        $retestNotices = $proposalRepository->findRetestNoticesForMergedPdfByFilters($filters);

        if ($retestNotices) {
            $fullMergedReportPath = $reportMerger->execution($retestNotices);

            /** @var Proposal $retestNotice */
            foreach ($retestNotices as $retestNotice) {
                $proposalManager->sendViaMail($retestNotice);
            }

            return $this->redirect($fullMergedReportPath);
        }

        return $this->redirectToRoute('admin_proposal_list');
    }

    /**
     * @param Account $account
     * @param Request $request
     * @return RedirectResponse
     * @throws \Twig\Error\Error
     */
    public function createCustomProposalAction(Account $account, Request $request)
    {
        /** @var AccountManager $accountManager */
        $accountManager = $this->get('app.account.manager');

        $form = $this->createForm(CustomProposalOrWorkorderType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            /** @var DeviceCategory $division */
            $division = $formData["division"];
            /** @var UploadedFile $proposalFile */
            $proposalFile = $formData["proposalFile"];

            //check if can create custom proposal
            $formDataForAccess["division"] = $division;
            $formDataForAccess["account"] = $account;
            $formDataForAccess["accountDivisions"] = $accountManager->getAccountDivisions($account);
            $this->denyAccessUnlessGranted('create_custom_proposal', $formDataForAccess);

            /** @var Creator $proposalCreator */
            $proposalCreator = $this->get('app.proposal.creator.service');
            /** @var Proposal $newProposal */
            $newProposal = $proposalCreator->createCustomProposal($account, $division, $proposalFile);

            if ($newProposal) {
                return $this->redirectToRoute('admin_account_proposals_get', ['account' => $account->getId()]);
            }
        }

        return $this->redirectToRoute('admin_account_get', ['accountId' => $account->getId()]);
    }

    /**
     * @param Device $device
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createRepairProposalAction(Device $device, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $objectManager->getRepository('AppBundle:Service');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var MunicipalityRepository $municipalityRepository */
        $municipalityRepository = $objectManager->getRepository('AppBundle:Municipality');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var Creator $proposalCreator */
        $proposalCreator = $this->get('app.proposal.creator.service');
        /** @var ProposalService $proposalService */
        $proposalService = $this->get('app.proposal_service.service');
        /** @var Division $divisionService */
        $divisionService = $this->get('app.division.service');
        /** @var Authorizer $accountAuthorizerService */
        $accountAuthorizerService = $this->get('app.account_authorizer.service');
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->get('app.department_channel.manager');
        /** @var PreviousPageService $previousPageService */
        $previousPageService = $this->get('app.previous_page.service');
        /** @var MessageRepository */
        $messageRepository = $objectManager->getRepository('AppBundle:Message');

        /** @var Session $session */
        $session = $this->get('session');
        // added case when no session previos page and no referer and add previous page - service's device
        $previousPage = $previousPageService->getByRequestAndDevice($request, $device);


        // TODO: NEED REFACTORING (потрібно це винести в Voter)
        /** @var DeviceCategory $division */
        $division = $device->getNamed()->getCategory();
        /** @var Account $account */
        $account = $device->getAccount();
        /** @var Municipality $municipality */
        $municipality = $account->getMunicipality();
        /** @var Account $ownerAccount */
        $ownerAccount = $accountAuthorizerService->getAccountWithAuthorizerForDivision($account, $division);

        // TODO: NEED REFACTORING (потрібно це винести в Voter)
        $messages = $messageRepository->findErrorByAccount($ownerAccount);

        if (!empty($messages)) {
            /** @var Message $message */
            foreach ($messages as $message) {
                if (!($message->getAlias() == 'service_no_fee')) {
                    $session->getFlashBag()->add(
                        "failed",
                        'Proposal was not created due to the issues with related Accounts'
                    );

                    return $this->redirectToRoute('admin_account_get', [
                        'accountId' => $ownerAccount->getId()
                    ]);
                }
            }
        }

        $isNewProposal = false;
        /** @var Proposal $proposal */
        $proposal = $proposalRepository->findDraftRepairProposal($ownerAccount, $division);
        if (empty($proposal)) {
            $proposal = $proposalCreator->createProposal($ownerAccount, $division);
            $isNewProposal = true;
        }

        $form = $this->createForm(
            RepairProposalType::class,
            [
                'proposal' => $proposal,
                'repairDeviceInfo' => new RepairDeviceInfo(),
                'selectedRepairs' => $serviceRepository->findBy(['device' => $device]),
                'device' => $device
            ],
            ['validation_groups' => ['create']]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Proposal $proposal */
            $proposal = $form['proposal']->getData();
            $proposalManager->save($proposal);

            if ($isNewProposal) {
                // TODO: NEED REFACTORING
                /** @var ProposalCreateEvent $event */
                $event = new ProposalCreateEvent($proposal);
                /** @var EventDispatcherInterface */
                $dispatcher = $this->get('event_dispatcher');
                $dispatcher->dispatch('proposal.create', $event);
            }

            $addedRepairs = $form['serviceName']->getData()['name']->toArray();
            $proposalService->addRepairs($addedRepairs, $device, $proposal);
            $proposalService->addServicesToProposal($proposal, $proposal->getServices());

            $repairDeviceInfoCreator = $this->get('app.device_info_creator.service');
            /** @var RepairDeviceInfo $repairDeviceInfo */
            $repairDeviceInfo = $form['deviceInfo']->getData();
            if ($repairDeviceInfo->getId()) {
                $repairDeviceInfoCreator->creating($device, $proposal, $repairDeviceInfo);
            } else {
                $repairDeviceInfoCreator->edit($repairDeviceInfo, $proposal, $device);
            }

            // TODO: NEED REFACTORING
            $proposalService->calculateAndSetAllProposalTotals($proposal);

            return $this->redirect($previousPage);
        }

        $serviceNames = $serializer->serialize(
            $serviceManager->getNonFrequencyServiceNames($device),
            'json'
        );

        return $this->render('@Admin/Proposal/create_repair_proposal.html.twig', [
            'device' => $deviceManager->serialize($device),
            'serviceNames' => $serviceNames,
            'form' =>$form->createView(),
            'fee' => $municipalityRepository->getMunicipalityFee(
                $device->getAccount(),
                $divisionService->temporaryForAlarmAndPlumbingDivisions($device->getNamed()->getCategory())
            ),
            'previousPage' =>$previousPage,
            'isNotSelectedServiceName' => $deviceManager->isExistNotSelectedService($device),
            'defaultDepartmentChannelWithFeesWillVary' =>
                $departmentChannelManager->checkIfDefaultDepartmentChannelHasFeesWillVary($municipality, $division)
        ]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updateRepairProposalInfoAction(Proposal $proposal, Request $request)
    {
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var ProposalService $proposalService */
        $proposalService = $this->get('app.proposal_service.service');
        /** @var string $redirectTo */
        $redirectTo = 'admin_repair_proposal_view';
        /** @var ProposalGeneralInfoType|InternalCommentType $formType */
        $formType = ProposalGeneralInfoType::class;

        if ($proposal->getType()->getAlias() == 'custom') {
            $redirectTo = "admin_custom_proposal_view";
            $formType =  InternalCommentType::class;
        }

        $form = $this->createForm($formType, $proposal);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $proposalManager->update($proposal);
            $proposalService->calculateAndSetAllProposalTotals($proposal);

            return $this->redirectToRoute($redirectTo, ["proposal" => $proposal->getId()]);
        }

        return $this->render('@Admin/Proposal/update_repair_proposal_info.html.twig', [
            "form" => $form->createView(),
            "proposal" => $proposal
        ]);
    }

    /**
     * @param Proposal $proposal
     * @param Device $device
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Twig\Error\Error
     */
    public function editRepairProposalAction(Proposal $proposal, Device $device, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $objectManager->getRepository('AppBundle:Service');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        /** @var DeviceManager $deviceManager */
        $deviceManager = $this->get('app.device.manager');
        /** @var ProposalService $proposalService */
        $proposalService = $this->get('app.proposal_service.service');
        /** @var Division $divisionService */
        $divisionService = $this->get('app.division.service');
        /** @var DepartmentChannelManager $departmentChannelManager */
        $departmentChannelManager = $this->get('app.department_channel.manager');

        /** @var MunicipalityRepository $municipalityRepository */
        $municipalityRepository = $objectManager->getRepository('AppBundle:Municipality');
        /** @var ServiceManager $serviceManager */
        $serviceManager = $this->get('app.service.manager');
        /** @var RepairDeviceInfoRepository $repairDeviceInfoRepository */
        $repairDeviceInfoRepository = $objectManager->getRepository('AppBundle:RepairDeviceInfo');
        /** @var RepairDeviceInfo $repairDeviceInfo */
        $repairDeviceInfo = $repairDeviceInfoRepository->findOneBy(['proposal' => $proposal, 'device' => $device]);
        /** @var DeviceCategory $division */
        $division = $device->getNamed()->getCategory();
        /** @var Account $account */
        $account = $device->getAccount();
        /** @var Municipality $municipality */
        $municipality = $account->getMunicipality();

        /** @var Session $session */
        $session = $this->get('session');

        /** @var PreviousPageService $previewPageService */
        $previewPageService = $this->get("app.previous_page.service");
        // added case when no session previos page and no referer and add previous page - service's device
        $previousPage = $previewPageService->getForInspectionByRequestAndDevice($request, $device);
        $form = $this->createForm(
            RepairProposalType::class,
            [
                'proposal' => $proposal,
                'repairDeviceInfo' => $repairDeviceInfo,
                'selectedRepairs' => $serviceRepository->findBy(['device' => $device]),
                'device' => $device
            ],
            ['validation_groups' => ['create']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Proposal $proposal */
            $proposal = $form['proposal']->getData();
            $proposalManager->save($proposal);
            $addedRepairs = $form['serviceName']->getData()['name']->toArray();
            $proposalService->addRepairs($addedRepairs, $device, $proposal);
            $proposalService->addServicesToProposal($proposal, $proposal->getServices());

            /** @var \AppBundle\Services\RepairDeviceInfo\Creator $repairDeviceInfoCreator */
            $repairDeviceInfoCreator = $this->get('app.device_info_creator.service');
            /** @var RepairDeviceInfo $repairDeviceInfo */
            $repairDeviceInfo = $form['deviceInfo']->getData();
            if ($repairDeviceInfo->getId()) {
                $repairDeviceInfoCreator->creating($device, $proposal, $repairDeviceInfo);
            } else {
                $repairDeviceInfoCreator->edit($repairDeviceInfo, $proposal, $device);
            }

            $proposalService->calculateAndSetAllProposalTotals($proposal);

            return $this->redirectToRoute('admin_repair_proposal_view', ['proposal' => $proposal->getId()]);
        }

        $serviceNames = $serializer->serialize(
            $serviceManager->getNonFrequencyServiceNames($device),
            'json'
        );

        return $this->render('@Admin/Proposal/create_repair_proposal.html.twig', [
            'device' => $deviceManager->serialize($device),
            'serviceNames' => $serviceNames,
            'form' =>$form->createView(),
            'proposal' => $proposal,
            'fee' => $municipalityRepository->getMunicipalityFee(
                $device->getAccount(),
                $divisionService->temporaryForAlarmAndPlumbingDivisions($device->getNamed()->getCategory())
            ),
            'totalFee' => $repairDeviceInfo->getSubtotalMunicAndProcFee(),
            'previousPage' =>$previousPage,
            'isNotSelectedServiceName' => $deviceManager->isExistNotSelectedService($device),
            'defaultDepartmentChannelWithFeesWillVary' =>
                $departmentChannelManager->checkIfDefaultDepartmentChannelHasFeesWillVary($municipality, $division)
        ]);
    }

    /**
     * @param Proposal $proposal
     * @return Response
     * @throws \Twig\Error\Error
     */
    public function viewPdfAction(Proposal $proposal)
    {
        $pdfService = $this->get('app.pdf_service');
        $proposalManager = $this->get('app.proposal.manager');

        $proposalManager->addDepartmentChannel($proposal);
        $PDF = $pdfService->create($pdfService::ACTION_VIEW, $proposal);

        return new Response(
            $PDF['output'],
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$PDF['name'].'.pdf"'
            )
        );
    }

    /**
     * @param $name
     * @return BinaryFileResponse
     */
    public function viewFrozenPdfAction($name)
    {
        $appPath = $this->getParameter('kernel.root_dir');
        $uploadsPath = realpath($appPath . '/../web/uploads/proposals');

        return new BinaryFileResponse("$uploadsPath/$name");
    }

    /**
     * @param Proposal $proposal
     * @param Device $device
     * @return Response or class, which extends it
     */
    public function deleteDeviceRepairFromProposalAction(
        Proposal $proposal,
        Device $device,
        Request $request
    ) : Response {
        $params['device'] = $device;
        $params['proposal'] = $proposal;
        if (!$this->isGranted(DeviceVoter::CAN_NOT_DELETE_DEVICE_FROM_PROPOSAL, $params)) {
            return $this->redirect($request->headers->get('referer'));
        }
        /** @var ProposalService $proposalService */
        $proposalService = $this->get('app.proposal_service.service');
        /** @var RepairDeviceInfoManager $repairDeviceInfoManager */
        $repairDeviceInfoManager = $this->get('app.repair_device_info.manager');

        $proposalService->excludeByDeviceFromProposal($device, $proposal);
        $repairDeviceInfoManager->delete($proposal, $device);
        $proposalService->calculateAndSetAllProposalTotals($proposal);

        return $this->redirectToRoute('admin_repair_proposal_view', ['proposal' => $proposal->getId()]);
    }

    /**
     * @param Proposal $proposal
     * @param Request $request
     * @return Response
     */
    public function replaceAction(Proposal $proposal, Request $request) : Response
    {
        if (!$this->isGranted(ProposalVoter::CHECK_AUTHORIZER_FOR_PROPOSAL, $proposal)) {
            return $this->redirect($request->headers->get('referer'));
        }

        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->get('app.proposal_log.manager');
        $proposalTypeAlias = $proposal->getType()->getAlias();

        $comment = $request->request->get('comment');

        /** @var Replace $replaceProposalService */
        $replaceProposalService = $this->get('app.proposal.replace.service');
        $replacingProposal = $replaceProposalService->replace($proposal, $comment);

        $proposalLogMessages = [
            "retest" => "Retest Notice was replaced",
            "repair" => "Proposal was replaced",
            "custom" => "Proposal was replaced",
        ];

        $proposalLogManager->create($proposal, $proposalLogMessages["$proposalTypeAlias"], $comment);

        $proposalRoute = "admin_".$proposalTypeAlias."_proposal_view";
        return $this->redirectToRoute("$proposalRoute", ['proposal' => $replacingProposal->getId()]);
    }

    /**
     * @param Service $service
     * @param Request $request
     * @return RedirectResponse
     */
    public function addInspectionToProposalAction(Service $service, Request $request)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->get('app.entity_manager');
        /** @var ProposalRepository $proposalRepository */
        $proposalRepository = $objectManager->getRepository('AppBundle:Proposal');
        /** @var Authorizer $accountAuthorizerService */
        $accountAuthorizerService = $this->get('app.account_authorizer.service');
        /** @var Creator $proposalCreator */
        $proposalCreator = $this->get('app.proposal.creator.service');
        /** @var ProposalService $proposalService */
        $proposalService = $this->get('app.proposal_service.service');
        /** @var EstimationTime $estimationTimeService */
        $estimationTimeService = $this->get('app.estimation_time');
        /** @var ProposalManager $proposalManager */
        $proposalManager = $this->get('app.proposal.manager');
        /** @var Device $device */
        $device = $service->getDevice();
        /** @var DeviceNamed $deviceNamed */
        $deviceNamed = $device->getNamed();
        /** @var DeviceCategory $division */
        $division = $params['division'] = $deviceNamed->getCategory();
        /** @var Account $account */
        $account = $device->getAccount();
        /** @var Account $ownerAccount */
        $ownerAccount = $params['ownerAccount'] =
            $accountAuthorizerService->getAccountWithAuthorizerForDivision($account, $division);
        /** @var ProposalLogManager $proposalLogManager */
        $proposalLogManager = $this->get('app.proposal_log.manager');
        /** @var \AppBundle\Services\RepairDeviceInfo\Creator $repairDeviceInfoCreator */
        $repairDeviceInfoCreator = $this->get('app.device_info_creator.service');
        $params['service'] = $service;

        $isNeedCreateInspectionForProposal = true;
        $serviceArray[] = $service;
        $isNewProposal = false;

        if (!$this->isGranted(ErrorByAccountVoter::ADD_INSPECTION_TO_PROPOSAL, $params)) {
            return $this->redirect($request->headers->get('referer'));
        }

        if (!$this->isGranted(ServiceVoter::CAN_NOT_ADD_INSPECTION_TO_PROPOSAL, $service)) {
            return $this->redirect($request->headers->get('referer'));
        }

        /** @var Proposal $proposal */
        $proposal = $proposalRepository->findDraftRepairProposal($ownerAccount, $division);
        if (empty($proposal)) {
            $proposal = $proposalCreator->createProposal($ownerAccount, $division);
            $proposalManager->save($proposal);
            $isNewProposal = true;
        }

        if ($isNewProposal) {
            // TODO: NEED REFACTORING
            /** @var ProposalCreateEvent $event */
            $event = new ProposalCreateEvent($proposal);
            /** @var EventDispatcherInterface */
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch('proposal.create', $event);
        }

        $proposal = $proposalService->addServicesToProposal($proposal, $serviceArray);

        $repairDeviceInfoCreator->creatingForInspection($proposal, $isNeedCreateInspectionForProposal);

        $estimationTimeService->saveNewTotalEstimationTime($proposal);

        $proposalService->calculateAndSetAllProposalTotals($proposal);

        $proposalLogManager->addInspectionToProposal($proposal, $service);

        return $this->redirectToRoute("admin_repair_proposal_view", ['proposal' => $proposal->getId()]);
    }
}
