<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Fungio\OutlookCalendarBundle\Service\OutlookCalendar;

class MailController extends Controller
{
    /**
     * @return Response
     */
    public function getEventsAction() : Response
    {
        $tas = $this->get('app.technician_assignment_sync.service');
        $outlookCalendar = $tas->getOutlookCalendarInstance();

        $request = $this->get('request_stack')->getMasterRequest();
        $session = new Session();

        $redirectUri = $tas->getURIToRedirectToAfterAuthorization();

        if (!$session->has('fungio_outlook_calendar_access_token')) {
            if ($request->query->has('code') && $request->get('code')) {
                $token = $outlookCalendar->getTokenFromAuthCode($request->get('code'), $redirectUri);
                $access_token = $token['access_token'];
                $session->set('fungio_outlook_calendar_access_token', $access_token);
            } else {
                return new RedirectResponse($outlookCalendar->getLoginUrl($redirectUri));
            }
        } else {
            $access_token = $session->get('fungio_outlook_calendar_access_token');
        }

        $events = $tas->getEvents($access_token, new \DateTime('now'));
        return $this->render(
            'AdminBundle:Mail:getEvents.html.twig',
            [
                'events' => $events,
            ]
        );
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function setEventAction()
    {
        $tas = $this->get('app.technician_assignment_sync.service');
        $outlookCalendar = $tas->getOutlookCalendarInstance();

        $request = $this->get('request_stack')->getMasterRequest();
        $session = new Session();

        $redirectUri = $tas->getURIToRedirectToAfterAuthorization();

        if (!$session->has('fungio_outlook_calendar_access_token')) {
            if ($request->query->has('code') && $request->get('code')) {
                $token = $outlookCalendar->getTokenFromAuthCode($request->get('code'), $redirectUri);
                $access_token = $token['access_token'];
                $session->set('fungio_outlook_calendar_access_token', $access_token);
            } else {
                return new RedirectResponse($outlookCalendar->getLoginUrl($redirectUri));
            }
        } else {
            $access_token = $session->get('fungio_outlook_calendar_access_token');
        }

        $eventData = [
            'subject' => 'Some test for subject',
            'content' => 'Some test content',
            'start_time' => new \DateTime('now'),
            'end_time' => new \DateTime('+7 days'),
        ];
        $eventId = $tas->setEvent($access_token, $eventData);
        return $this->render(
            'AdminBundle:Mail:getEvents.html.twig',
            [
                'events' => $eventId,
            ]
        );
    }

    /**
     * @param string $eventId
     * @return Response or class, which extends it
     * @throws \Exception
     */
    /*public function updateEventAction(string $eventId) : Response
    {
        $outlookCalendar = $this->get('fungio.outlook_calendar');

        $request = $this->get('request_stack')->getMasterRequest();
        $session = new Session();

        $redirectUri = 'https://erp/app_dev.php/admin/mail/get-events';

        if (!$session->has('fungio_outlook_calendar_access_token')) {
            if ($request->query->has('code') && $request->get('code')) {
                $token = $outlookCalendar->getTokenFromAuthCode($request->get('code'), $redirectUri);
                $access_token = $token['access_token'];
                $session->set('fungio_outlook_calendar_access_token', $access_token);
            } else {
                return new RedirectResponse($outlookCalendar->getLoginUrl($redirectUri));
            }
        } else {
            $access_token = $session->get('fungio_outlook_calendar_access_token');
        }

        $form = $this->createFormBuilder([])
            ->add('subject', TextType::class)
            ->add('content', TextType::class)
            ->add('start_time', TextType::class)
            ->add('end_time', TextType::class)
            ->add('submitEvent', SubmitType::class)
            ->getForm();

        $request = $this->get('request_stack')->getMasterRequest();
        $form->handleRequest($request);
        $newEventFieldValues = $request->request->all();

        if ($form->isSubmitted() && $form->isValid()) {
            $newEventFieldValues = [
                'subject' => $newEventFieldValues['form']['subject'],
                'content' => $newEventFieldValues['form']['content'],
                'start_time' => \DateTime::createFromFormat('m/d/Y', $newEventFieldValues['form']['start_time']),
                'end_time' => \DateTime::createFromFormat('m/d/Y', $newEventFieldValues['form']['end_time']),
            ];

            $result = $outlookCalendar->updateEvent(
                $access_token,
                $eventId,
                $newEventFieldValues['subject'],
                $newEventFieldValues['content'],
                $newEventFieldValues['start_time'],
                $newEventFieldValues['end_time']
            );

            if (!empty($result['errorNumber'])) {
                throw new \Exception('Error #'.$result['errorNumber'].' '.$result['error']);
            }

            return $this->redirectToRoute('admin_mail_get_events');
        }

        return $this->render('@Admin/Mail/form.html.twig',[
            'form' => $form->createView(),
        ]);
    }*/

    /**
     * @param string $eventId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function deleteEventAction(string $eventId) : RedirectResponse
    {
        $tas = $this->get('app.technician_assignment_sync.service');
        $outlookCalendar = $tas->getOutlookCalendarInstance();

        $request = $this->get('request_stack')->getMasterRequest();
        $session = new Session();

        $redirectUri = $tas->getURIToRedirectToAfterAuthorization();

        if (!$session->has('fungio_outlook_calendar_access_token')) {
            if ($request->query->has('code') && $request->get('code')) {
                $token = $outlookCalendar->getTokenFromAuthCode($request->get('code'), $redirectUri);
                $access_token = $token['access_token'];
                $session->set('fungio_outlook_calendar_access_token', $access_token);
            } else {
                return new RedirectResponse($outlookCalendar->getLoginUrl($redirectUri));
            }
        } else {
            $access_token = $session->get('fungio_outlook_calendar_access_token');
        }

        $result = $tas->deleteEvent($access_token, $eventId);

        if (!empty($result['errorNumber'])) {
            throw new \Exception('Error #'.$result['errorNumber'].' '.$result['error']);
        }

        return $this->redirectToRoute('admin_mail_get_events');
    }

    public function logoutAction()
    {
        $tas = $this->get('app.technician_assignment_sync.service');
        $tas->logout();

//        $session = new Session();
//        if ($session->has('fungio_outlook_calendar_access_token')) {
//            $session->remove('fungio_outlook_calendar_access_token');
//        }
//        $session->save();
    }

    /**
     * @param OutlookCalendar $outlookCalendar
     * @return mixed|RedirectResponse
     */
    private function getAccessToken(OutlookCalendar $outlookCalendar)
    {
        $request = $this->get('request_stack')->getMasterRequest();
        $session = new Session();

        $redirectUri = 'https://erp/app_dev.php/admin/mail/get-events';

        if (!$session->has('fungio_outlook_calendar_access_token')) {
            if ($request->query->has('code') && $request->get('code')) {
                $token = $outlookCalendar->getTokenFromAuthCode($request->get('code'), $redirectUri);
                $access_token = $token['access_token'];
                $session->set('fungio_outlook_calendar_access_token', $access_token);
            } else {
                return new RedirectResponse($outlookCalendar->getLoginUrl($redirectUri));
            }
        } else {
            $access_token = $session->get('fungio_outlook_calendar_access_token');
        }

        return $access_token;
    }
}
