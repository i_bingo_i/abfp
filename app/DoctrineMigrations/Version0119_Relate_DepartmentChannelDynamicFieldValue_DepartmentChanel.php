<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0119_Relate_DepartmentChannelDynamicFieldValue_DepartmentChanel extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel_dynamic_field_value ADD department_chanel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE department_channel_dynamic_field_value ADD CONSTRAINT FK_64EFA3FD76E0D2 FOREIGN KEY (department_chanel_id) REFERENCES department_channel (id)');
        $this->addSql('CREATE INDEX IDX_64EFA3FD76E0D2 ON department_channel_dynamic_field_value (department_chanel_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel_dynamic_field_value DROP FOREIGN KEY FK_64EFA3FD76E0D2');
        $this->addSql('DROP INDEX IDX_64EFA3FD76E0D2 ON department_channel_dynamic_field_value');
        $this->addSql('ALTER TABLE department_channel_dynamic_field_value DROP department_chanel_id');
    }
}
