<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0042_Create_Account_Contact_Person_History_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE account_contact_person_history (id INT AUTO_INCREMENT NOT NULL, owner_entity_id INT DEFAULT NULL, author INT DEFAULT NULL, contact_person INT DEFAULT NULL, account INT DEFAULT NULL, role INT DEFAULT NULL, responsibility INT DEFAULT NULL, deleted TINYINT(1) DEFAULT \'0\', date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_950F5F58835E0EEE (owner_entity_id), INDEX IDX_950F5F58BDAFD8C8 (author), INDEX IDX_950F5F58A44EE6F7 (contact_person), INDEX IDX_950F5F587D3656A4 (account), INDEX IDX_950F5F5857698A6A (role), INDEX IDX_950F5F58694E8A08 (responsibility), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES account_contact_person (id)');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58BDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58A44EE6F7 FOREIGN KEY (contact_person) REFERENCES contact_person (id)');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F587D3656A4 FOREIGN KEY (account) REFERENCES account (id)');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F5857698A6A FOREIGN KEY (role) REFERENCES contact_person_role (id)');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58694E8A08 FOREIGN KEY (responsibility) REFERENCES contact_person_responsibility (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE account_contact_person_history');
    }
}
