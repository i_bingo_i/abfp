<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0351_Add_lifeCycle_Field_To_Proposal_Entity_To_Relate_It_To_ProposalLifeCycle_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal ADD life_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE594726C4CA05D FOREIGN KEY (life_cycle_id) REFERENCES proposal_life_cycle (id)');
        $this->addSql('CREATE INDEX IDX_BFE594726C4CA05D ON proposal (life_cycle_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE594726C4CA05D');
        $this->addSql('DROP INDEX IDX_BFE594726C4CA05D ON proposal');
        $this->addSql('ALTER TABLE proposal DROP life_cycle_id');
    }
}
