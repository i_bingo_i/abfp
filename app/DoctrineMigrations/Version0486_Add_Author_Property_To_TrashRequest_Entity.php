<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0486_Add_Author_Property_To_TrashRequest_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trash_requests ADD author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE trash_requests ADD CONSTRAINT FK_5D9B5BD8F675F31B FOREIGN KEY (author_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_5D9B5BD8F675F31B ON trash_requests (author_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trash_requests DROP FOREIGN KEY FK_5D9B5BD8F675F31B');
        $this->addSql('DROP INDEX IDX_5D9B5BD8F675F31B ON trash_requests');
        $this->addSql('ALTER TABLE trash_requests DROP author_id');
    }
}
