<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0209_Delete_Relation_Opportunity_To_ServiceHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D78389C3D7');
        $this->addSql('DROP INDEX IDX_E83E22D78389C3D7 ON service_history');
        $this->addSql('ALTER TABLE service_history DROP opportunity');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history ADD opportunity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D78389C3D7 FOREIGN KEY (opportunity) REFERENCES opportunity (id)');
        $this->addSql('CREATE INDEX IDX_E83E22D78389C3D7 ON service_history (opportunity)');
    }
}
