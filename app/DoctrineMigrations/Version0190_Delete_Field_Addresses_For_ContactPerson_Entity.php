<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0190_Delete_Field_Addresses_For_ContactPerson_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contact_persons_addresses');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact_persons_addresses (contact_person_id INT NOT NULL, address_id INT NOT NULL, UNIQUE INDEX UNIQ_6FC80759F5B7AF75 (address_id), INDEX IDX_6FC807594F8A983C (contact_person_id), PRIMARY KEY(contact_person_id, address_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact_persons_addresses ADD CONSTRAINT FK_6FC807594F8A983C FOREIGN KEY (contact_person_id) REFERENCES contact_person (id)');
        $this->addSql('ALTER TABLE contact_persons_addresses ADD CONSTRAINT FK_6FC80759F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
    }
}
