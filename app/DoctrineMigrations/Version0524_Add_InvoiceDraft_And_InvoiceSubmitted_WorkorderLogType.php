<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0524_Add_InvoiceDraft_And_InvoiceSubmitted_WorkorderLogType extends AbstractMigration
{

    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO workorder_log_type (`name`, `alias`) VALUES ('Invoice draft was save', 'invoice_draft_was_save')");
        $this->addSql("INSERT INTO workorder_log_type (`name`, `alias`) VALUES ('Invoice submitted', 'invoice_submitted')");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM workorder_log_type WHERE `alias` = 'invoice_draft_was_save'");
        $this->addSql("DELETE FROM workorder_log_type WHERE `alias` = 'invoice_submitted'");
    }
}