<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0331_Add_PaymentTerms_To_AccountHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_history ADD payment_term_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE91640317653B16 FOREIGN KEY (payment_term_id) REFERENCES payment_term (id)');
        $this->addSql('CREATE INDEX IDX_EE91640317653B16 ON account_history (payment_term_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_history DROP FOREIGN KEY FK_EE91640317653B16');
        $this->addSql('DROP INDEX IDX_EE91640317653B16 ON account_history');
        $this->addSql('ALTER TABLE account_history DROP payment_term_id');
    }
}
