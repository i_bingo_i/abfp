<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0240_Refactor_Workorder_Entity_Add_ScheduledFrom_And_ScheduledTo_Properties extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder ADD scheduled_from DATETIME DEFAULT NULL, ADD scheduled_to DATETIME DEFAULT NULL, DROP start_scheduled_date, DROP finish_scheduled_date');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder ADD start_scheduled_date DATETIME DEFAULT NULL, ADD finish_scheduled_date DATETIME DEFAULT NULL, DROP scheduled_from, DROP scheduled_to');
    }
}
