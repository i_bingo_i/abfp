<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0114_Create_ChannelDynamicField_Entity_And_Relate extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE channel_dynamic_field (id INT AUTO_INCREMENT NOT NULL, channel_id INT DEFAULT NULL, type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(100) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_5D1EFF9772F5A1AA (channel_id), INDEX IDX_5D1EFF97C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE channel_dynamic_field ADD CONSTRAINT FK_5D1EFF9772F5A1AA FOREIGN KEY (channel_id) REFERENCES channel_named (id)');
        $this->addSql('ALTER TABLE channel_dynamic_field ADD CONSTRAINT FK_5D1EFF97C54C8C93 FOREIGN KEY (type_id) REFERENCES dynamic_field_type (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE channel_dynamic_field');
    }
}
