<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0174_Add_Fields_Authorizer_Access_AccessPrimary_Payment_PaymentPrimary extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person ADD authorizer TINYINT(1) DEFAULT \'0\', ADD access TINYINT(1) DEFAULT \'0\', ADD access_primary TINYINT(1) DEFAULT \'0\', ADD payment TINYINT(1) DEFAULT \'0\', ADD payment_primary TINYINT(1) DEFAULT \'0\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person DROP authorizer, DROP access, DROP access_primary, DROP payment, DROP payment_primary');
    }
}
