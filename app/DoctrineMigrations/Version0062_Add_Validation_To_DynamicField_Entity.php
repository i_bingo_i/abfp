<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0062_Add_Validation_To_DynamicField_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dynamic_field ADD validation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dynamic_field ADD CONSTRAINT FK_FC831C23A2274850 FOREIGN KEY (validation_id) REFERENCES dynamic_field_validation (id)');
        $this->addSql('CREATE INDEX IDX_FC831C23A2274850 ON dynamic_field (validation_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dynamic_field DROP FOREIGN KEY FK_FC831C23A2274850');
        $this->addSql('DROP INDEX IDX_FC831C23A2274850 ON dynamic_field');
        $this->addSql('ALTER TABLE dynamic_field DROP validation_id');
    }
}
