<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0183_Remove_SendingAdress_And_SendingAdressDescription_From_Proposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE594726510ABA8');
        $this->addSql('DROP INDEX IDX_BFE594726510ABA8 ON proposal');
        $this->addSql('ALTER TABLE proposal DROP sending_address_id, DROP sending_address_description');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal ADD sending_address_id INT DEFAULT NULL, ADD sending_address_description VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE594726510ABA8 FOREIGN KEY (sending_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_BFE594726510ABA8 ON proposal (sending_address_id)');
    }
}
