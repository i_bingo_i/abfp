<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0073_Add_Relationship_To_ContractorService extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor_service ADD named_id INT DEFAULT NULL, ADD contractor_id INT DEFAULT NULL, ADD device_category INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contractor_service ADD CONSTRAINT FK_8DCF8195457EB27E FOREIGN KEY (named_id) REFERENCES service_named (id)');
        $this->addSql('ALTER TABLE contractor_service ADD CONSTRAINT FK_8DCF8195B0265DC7 FOREIGN KEY (contractor_id) REFERENCES contractor (id)');
        $this->addSql('ALTER TABLE contractor_service ADD CONSTRAINT FK_8DCF8195887840E1 FOREIGN KEY (device_category) REFERENCES device_category (id)');
        $this->addSql('CREATE INDEX IDX_8DCF8195457EB27E ON contractor_service (named_id)');
        $this->addSql('CREATE INDEX IDX_8DCF8195B0265DC7 ON contractor_service (contractor_id)');
        $this->addSql('CREATE INDEX IDX_8DCF8195887840E1 ON contractor_service (device_category)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor_service DROP FOREIGN KEY FK_8DCF8195457EB27E');
        $this->addSql('ALTER TABLE contractor_service DROP FOREIGN KEY FK_8DCF8195B0265DC7');
        $this->addSql('ALTER TABLE contractor_service DROP FOREIGN KEY FK_8DCF8195887840E1');
        $this->addSql('DROP INDEX IDX_8DCF8195457EB27E ON contractor_service');
        $this->addSql('DROP INDEX IDX_8DCF8195B0265DC7 ON contractor_service');
        $this->addSql('DROP INDEX IDX_8DCF8195887840E1 ON contractor_service');
        $this->addSql('ALTER TABLE contractor_service DROP named_id, DROP contractor_id, DROP device_category');
    }
}
