<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0037_Add_Field_Company_For_ContactPersonHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_person_history ADD company INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact_person_history ADD CONSTRAINT FK_76D94D024FBF094F FOREIGN KEY (company) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_76D94D024FBF094F ON contact_person_history (company)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_person_history DROP FOREIGN KEY FK_76D94D024FBF094F');
        $this->addSql('DROP INDEX IDX_76D94D024FBF094F ON contact_person_history');
        $this->addSql('ALTER TABLE contact_person_history DROP company');
    }
}
