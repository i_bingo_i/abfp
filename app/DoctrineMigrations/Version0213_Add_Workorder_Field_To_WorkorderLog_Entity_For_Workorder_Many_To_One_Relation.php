<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0213_Add_Workorder_Field_To_WorkorderLog_Entity_For_Workorder_Many_To_One_Relation extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log ADD workorder_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder_log ADD CONSTRAINT FK_3711E8992C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
        $this->addSql('CREATE INDEX IDX_3711E8992C1C3467 ON workorder_log (workorder_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log DROP FOREIGN KEY FK_3711E8992C1C3467');
        $this->addSql('DROP INDEX IDX_3711E8992C1C3467 ON workorder_log');
        $this->addSql('ALTER TABLE workorder_log DROP workorder_id');
    }
}
