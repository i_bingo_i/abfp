<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0484_Create_EntityType_And_Customer_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entity_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, entity_type_id INT DEFAULT NULL, address_id INT DEFAULT NULL, custom_address_id INT DEFAULT NULL, accounting_system_id VARCHAR(255) DEFAULT NULL, entity_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_81398E095681BEB0 (entity_type_id), INDEX IDX_81398E09F5B7AF75 (address_id), INDEX IDX_81398E09CA4893DF (custom_address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E095681BEB0 FOREIGN KEY (entity_type_id) REFERENCES entity_type (id)');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09CA4893DF FOREIGN KEY (custom_address_id) REFERENCES address (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E095681BEB0');
        $this->addSql('DROP TABLE entity_type');
        $this->addSql('DROP TABLE customer');
    }
}
