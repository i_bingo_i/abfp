<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0299_Add_servicePatern_Field_To_RangeQuestion_Entity_To_Relate_It_To_ServicePatern_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE range_question ADD service_patern_id INT DEFAULT NULL, CHANGE date_create date_cretea DATETIME NOT NULL');
        $this->addSql('ALTER TABLE range_question ADD CONSTRAINT FK_F8A3E6EDD38F0878 FOREIGN KEY (service_patern_id) REFERENCES service_patern (id)');
        $this->addSql('CREATE INDEX IDX_F8A3E6EDD38F0878 ON range_question (service_patern_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE range_question DROP FOREIGN KEY FK_F8A3E6EDD38F0878');
        $this->addSql('DROP INDEX IDX_F8A3E6EDD38F0878 ON range_question');
        $this->addSql('ALTER TABLE range_question DROP service_patern_id, CHANGE date_cretea date_create DATETIME NOT NULL');
    }
}
