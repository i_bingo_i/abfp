<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0020_Add_Null_Default_Value_For_Fields_ContactPerson  extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_person CHANGE last_name last_name VARCHAR(100) DEFAULT NULL, CHANGE title title VARCHAR(100) DEFAULT NULL, CHANGE ext ext VARCHAR(10) DEFAULT NULL, CHANGE cell cell VARCHAR(50) DEFAULT NULL, CHANGE cod cod TINYINT(1) DEFAULT \'0\', CHANGE notes notes VARCHAR(150) DEFAULT NULL, CHANGE email email VARCHAR(50) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_person CHANGE last_name last_name VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, CHANGE title title VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, CHANGE email email VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, CHANGE ext ext VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, CHANGE cell cell VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, CHANGE cod cod TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE notes notes VARCHAR(150) NOT NULL COLLATE utf8_unicode_ci');
    }
}
