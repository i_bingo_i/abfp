<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0251_Add_Default_Zero_Value_To_Upload_And_Processing_Fees_Of_DepartmentChannel_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel CHANGE upload_fee upload_fee VARCHAR(50) DEFAULT \'0\', CHANGE processing_fee processing_fee VARCHAR(50) DEFAULT \'0\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel CHANGE upload_fee upload_fee VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE processing_fee processing_fee VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
