<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0478_Create_LetterAttachment_Entity_And_Relate extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE letter_attachment (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, letter_id INT DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_B9D7F3EB93CB796C (file_id), INDEX IDX_B9D7F3EB4525FF26 (letter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE letter_attachment ADD CONSTRAINT FK_B9D7F3EB93CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE letter_attachment ADD CONSTRAINT FK_B9D7F3EB4525FF26 FOREIGN KEY (letter_id) REFERENCES letter (id)');
        $this->addSql('ALTER TABLE letter ADD workorder_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE letter ADD CONSTRAINT FK_8E02EE0A2C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
        $this->addSql('CREATE INDEX IDX_8E02EE0A2C1C3467 ON letter (workorder_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE letter_attachment');
        $this->addSql('ALTER TABLE letter DROP FOREIGN KEY FK_8E02EE0A2C1C3467');
        $this->addSql('DROP INDEX IDX_8E02EE0A2C1C3467 ON letter');
        $this->addSql('ALTER TABLE letter DROP workorder_id');
    }
}
