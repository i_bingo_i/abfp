<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0503_Step_Entity_With_Relation_To_StepStatus_And_StepNamed_And_Workorder_Created extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE workorder_step (id INT AUTO_INCREMENT NOT NULL, status_id INT DEFAULT NULL, named_id INT DEFAULT NULL, workorder_id INT DEFAULT NULL, INDEX IDX_21DCCD636BF700BD (status_id), INDEX IDX_21DCCD63457EB27E (named_id), INDEX IDX_21DCCD632C1C3467 (workorder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workorder_step ADD CONSTRAINT FK_21DCCD636BF700BD FOREIGN KEY (status_id) REFERENCES workorder_step_status (id)');
        $this->addSql('ALTER TABLE workorder_step ADD CONSTRAINT FK_21DCCD63457EB27E FOREIGN KEY (named_id) REFERENCES workorder_step_named (id)');
        $this->addSql('ALTER TABLE workorder_step ADD CONSTRAINT FK_21DCCD632C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE workorder_step');
    }
}
