<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0134_Create_Many_To_Many_Relation_Between_Agent_And_Contact_Entities_With_Agent_Master extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE agents_contacts (agent_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_BFB535093414710B (agent_id), INDEX IDX_BFB53509E7A1254A (contact_id), PRIMARY KEY(agent_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE agents_contacts ADD CONSTRAINT FK_BFB535093414710B FOREIGN KEY (agent_id) REFERENCES agent (id)');
        $this->addSql('ALTER TABLE agents_contacts ADD CONSTRAINT FK_BFB53509E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE agents_contacts');
    }
}
