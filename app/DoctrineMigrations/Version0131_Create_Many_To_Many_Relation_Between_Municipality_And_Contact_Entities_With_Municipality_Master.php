<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0131_Create_Many_To_Many_Relation_Between_Municipality_And_Contact_Entities_With_Municipality_Master extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE municipalities_contacts (municipality_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_20E5CE8CAE6F181C (municipality_id), INDEX IDX_20E5CE8CE7A1254A (contact_id), PRIMARY KEY(municipality_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE municipalities_contacts ADD CONSTRAINT FK_20E5CE8CAE6F181C FOREIGN KEY (municipality_id) REFERENCES municipality (id)');
        $this->addSql('ALTER TABLE municipalities_contacts ADD CONSTRAINT FK_20E5CE8CE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE municipalities_contacts');
    }
}
