<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0324_Fix_ServiceHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D71F5066A4');
        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D78389C3D7');
        $this->addSql('DROP INDEX IDX_E83E22D71F5066A4 ON service_history');
        $this->addSql('DROP INDEX IDX_E83E22D78389C3D7 ON service_history');
        $this->addSql('ALTER TABLE service_history ADD opportunity_id INT DEFAULT NULL, ADD department_channel_id INT DEFAULT NULL, DROP department_channel, DROP opportunity');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D79A34590F FOREIGN KEY (opportunity_id) REFERENCES opportunity (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D7E0FCAD46 FOREIGN KEY (department_channel_id) REFERENCES department_channel (id)');
        $this->addSql('CREATE INDEX IDX_E83E22D79A34590F ON service_history (opportunity_id)');
        $this->addSql('CREATE INDEX IDX_E83E22D7E0FCAD46 ON service_history (department_channel_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D79A34590F');
        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D7E0FCAD46');
        $this->addSql('DROP INDEX IDX_E83E22D79A34590F ON service_history');
        $this->addSql('DROP INDEX IDX_E83E22D7E0FCAD46 ON service_history');
        $this->addSql('ALTER TABLE service_history ADD department_channel INT DEFAULT NULL, ADD opportunity INT DEFAULT NULL, DROP opportunity_id, DROP department_channel_id');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D71F5066A4 FOREIGN KEY (department_channel) REFERENCES department_channel (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D78389C3D7 FOREIGN KEY (opportunity) REFERENCES opportunity (id)');
        $this->addSql('CREATE INDEX IDX_E83E22D71F5066A4 ON service_history (department_channel)');
        $this->addSql('CREATE INDEX IDX_E83E22D78389C3D7 ON service_history (opportunity)');
    }
}
