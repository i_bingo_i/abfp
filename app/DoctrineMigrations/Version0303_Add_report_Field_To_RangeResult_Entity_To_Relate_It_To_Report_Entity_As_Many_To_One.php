<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0303_Add_report_Field_To_RangeResult_Entity_To_Relate_It_To_Report_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE range_result ADD report_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE range_result ADD CONSTRAINT FK_3AEC1F634BD2A4C0 FOREIGN KEY (report_id) REFERENCES report (id)');
        $this->addSql('CREATE INDEX IDX_3AEC1F634BD2A4C0 ON range_result (report_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE range_result DROP FOREIGN KEY FK_3AEC1F634BD2A4C0');
        $this->addSql('DROP INDEX IDX_3AEC1F634BD2A4C0 ON range_result');
        $this->addSql('ALTER TABLE range_result DROP report_id');
    }
}
