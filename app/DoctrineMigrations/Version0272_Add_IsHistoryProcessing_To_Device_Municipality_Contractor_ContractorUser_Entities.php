<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0272_Add_IsHistoryProcessing_To_Device_Municipality_Contractor_ContractorUser_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor ADD is_history_processed TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE contractor_user ADD is_history_processed TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE device ADD is_history_processed TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE municipality ADD is_history_processed TINYINT(1) DEFAULT \'0\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor DROP is_history_processed');
        $this->addSql('ALTER TABLE contractor_user DROP is_history_processed');
        $this->addSql('ALTER TABLE device DROP is_history_processed');
        $this->addSql('ALTER TABLE municipality DROP is_history_processed');
    }
}
