<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0126_Add_Relations_For_AgentChannelDynamicFieldValue_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value ADD agent_channel INT DEFAULT NULL, ADD field INT DEFAULT NULL');
        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value ADD CONSTRAINT FK_EAC5B2F870AAECED FOREIGN KEY (agent_channel) REFERENCES agent_channel (id)');
        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value ADD CONSTRAINT FK_EAC5B2F85BF54558 FOREIGN KEY (field) REFERENCES channel_dynamic_field (id)');
        $this->addSql('CREATE INDEX IDX_EAC5B2F870AAECED ON agent_channel_dynamic_field_value (agent_channel)');
        $this->addSql('CREATE INDEX IDX_EAC5B2F85BF54558 ON agent_channel_dynamic_field_value (field)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value DROP FOREIGN KEY FK_EAC5B2F870AAECED');
        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value DROP FOREIGN KEY FK_EAC5B2F85BF54558');
        $this->addSql('DROP INDEX IDX_EAC5B2F870AAECED ON agent_channel_dynamic_field_value');
        $this->addSql('DROP INDEX IDX_EAC5B2F85BF54558 ON agent_channel_dynamic_field_value');
        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value DROP agent_channel, DROP field');
    }
}
