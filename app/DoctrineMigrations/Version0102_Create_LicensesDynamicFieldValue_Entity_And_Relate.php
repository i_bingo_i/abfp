<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0102_Create_LicensesDynamicFieldValue_Entity_And_Relate extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE licenses_dynamic_field_value (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, license_id INT DEFAULT NULL, value VARCHAR(255) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_9D146CEE443707B0 (field_id), INDEX IDX_9D146CEE460F904B (license_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE licenses_dynamic_field_value ADD CONSTRAINT FK_9D146CEE443707B0 FOREIGN KEY (field_id) REFERENCES licenses_dynamic_field (id)');
        $this->addSql('ALTER TABLE licenses_dynamic_field_value ADD CONSTRAINT FK_9D146CEE460F904B FOREIGN KEY (license_id) REFERENCES licenses (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE licenses_dynamic_field_value');
    }
}
