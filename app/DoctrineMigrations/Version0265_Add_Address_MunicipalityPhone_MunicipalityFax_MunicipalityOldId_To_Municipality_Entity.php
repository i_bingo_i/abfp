<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0265_Add_Address_MunicipalityPhone_MunicipalityFax_MunicipalityOldId_To_Municipality_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE municipality ADD address_id INT DEFAULT NULL, ADD municipality_phone VARCHAR(255) DEFAULT NULL, ADD municipality_fax VARCHAR(255) DEFAULT NULL, ADD old_municipality_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE municipality ADD CONSTRAINT FK_C6F56628F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_C6F56628F5B7AF75 ON municipality (address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE municipality DROP FOREIGN KEY FK_C6F56628F5B7AF75');
        $this->addSql('DROP INDEX IDX_C6F56628F5B7AF75 ON municipality');
        $this->addSql('ALTER TABLE municipality DROP address_id, DROP municipality_phone, DROP municipality_fax, DROP old_municipality_id');
    }
}
