<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0075_Add_Relationship_To_Service extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service ADD named INT DEFAULT NULL, ADD device_id INT DEFAULT NULL, ADD account_id INT DEFAULT NULL, ADD company_last_tested INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD271E0CC87 FOREIGN KEY (named) REFERENCES service_named (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD294A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD29B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD29B52D956 FOREIGN KEY (company_last_tested) REFERENCES contractor (id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD271E0CC87 ON service (named)');
        $this->addSql('CREATE INDEX IDX_E19D9AD294A4C7D4 ON service (device_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD29B6B5FBA ON service (account_id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD29B52D956 ON service (company_last_tested)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD271E0CC87');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD294A4C7D4');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD29B6B5FBA');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD29B52D956');
        $this->addSql('DROP INDEX IDX_E19D9AD271E0CC87 ON service');
        $this->addSql('DROP INDEX IDX_E19D9AD294A4C7D4 ON service');
        $this->addSql('DROP INDEX IDX_E19D9AD29B6B5FBA ON service');
        $this->addSql('DROP INDEX IDX_E19D9AD29B52D956 ON service');
        $this->addSql('ALTER TABLE service DROP named, DROP device_id, DROP account_id, DROP company_last_tested');
    }
}
