<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0314_Add_division_Field_To_Relate_It_To_DeviceCategory_Entity_As_Many_To_One_Add_proposals_Field_To_Relate_It_To_Proposal_Entity_As_Many_To_Many extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE special_notification ADD division_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE special_notification ADD CONSTRAINT FK_6792D8C341859289 FOREIGN KEY (division_id) REFERENCES device_category (id)');
        $this->addSql('CREATE INDEX IDX_6792D8C341859289 ON special_notification (division_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE special_notification DROP FOREIGN KEY FK_6792D8C341859289');
        $this->addSql('DROP INDEX IDX_6792D8C341859289 ON special_notification');
        $this->addSql('ALTER TABLE special_notification DROP division_id');
    }
}
