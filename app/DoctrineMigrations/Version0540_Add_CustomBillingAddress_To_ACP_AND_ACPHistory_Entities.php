<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0540_Add_CustomBillingAddress_To_ACP_AND_ACPHistory_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person ADD custom_billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person ADD CONSTRAINT FK_F6B922FDEA2B701D FOREIGN KEY (custom_billing_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_F6B922FDEA2B701D ON account_contact_person (custom_billing_address_id)');
        $this->addSql('ALTER TABLE account_contact_person_history ADD custom_billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58EA2B701D FOREIGN KEY (custom_billing_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_950F5F58EA2B701D ON account_contact_person_history (custom_billing_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person DROP FOREIGN KEY FK_F6B922FDEA2B701D');
        $this->addSql('DROP INDEX IDX_F6B922FDEA2B701D ON account_contact_person');
        $this->addSql('ALTER TABLE account_contact_person DROP custom_billing_address_id');
        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F58EA2B701D');
        $this->addSql('DROP INDEX IDX_950F5F58EA2B701D ON account_contact_person_history');
        $this->addSql('ALTER TABLE account_contact_person_history DROP custom_billing_address_id');
    }
}
