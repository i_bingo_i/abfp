<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0198_Create_ProposalHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proposal_history (id INT AUTO_INCREMENT NOT NULL, division_id INT DEFAULT NULL, account_id INT DEFAULT NULL, type_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, status_id INT DEFAULT NULL, contact_person_id INT DEFAULT NULL, sending_address_id INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, author INT DEFAULT NULL, reference_id VARCHAR(255) NOT NULL, services_count INT NOT NULL, devices_count INT NOT NULL, sites_count INT NOT NULL, date_of_proposal DATE DEFAULT NULL, earliest_due_date DATE DEFAULT NULL, grand_total DOUBLE PRECISION DEFAULT NULL, service_total_fee DOUBLE PRECISION DEFAULT NULL, sending_address_description VARCHAR(255) DEFAULT NULL, report VARCHAR(255) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_721E400041859289 (division_id), INDEX IDX_721E40009B6B5FBA (account_id), INDEX IDX_721E4000C54C8C93 (type_id), INDEX IDX_721E400061220EA6 (creator_id), INDEX IDX_721E40006BF700BD (status_id), INDEX IDX_721E40004F8A983C (contact_person_id), INDEX IDX_721E40006510ABA8 (sending_address_id), INDEX IDX_721E4000835E0EEE (owner_entity_id), INDEX IDX_721E4000BDAFD8C8 (author), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E400041859289 FOREIGN KEY (division_id) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E40009B6B5FBA FOREIGN KEY (account_id) REFERENCES account_history (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E4000C54C8C93 FOREIGN KEY (type_id) REFERENCES proposal_type (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E400061220EA6 FOREIGN KEY (creator_id) REFERENCES contractor_user_history (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E40006BF700BD FOREIGN KEY (status_id) REFERENCES proposal_status (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E40004F8A983C FOREIGN KEY (contact_person_id) REFERENCES account_contact_person_history (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E40006510ABA8 FOREIGN KEY (sending_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E4000835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES proposal (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E4000BDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE proposal_history');
    }
}
