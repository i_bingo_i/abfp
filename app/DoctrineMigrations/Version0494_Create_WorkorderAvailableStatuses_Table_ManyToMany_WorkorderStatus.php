<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0494_Create_WorkorderAvailableStatuses_Table_ManyToMany_WorkorderStatus extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE workorder_available_statuses (status_id INT NOT NULL, available_status_id INT NOT NULL, INDEX IDX_231F272A6BF700BD (status_id), INDEX IDX_231F272A87DBBA (available_status_id), PRIMARY KEY(status_id, available_status_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workorder_available_statuses ADD CONSTRAINT FK_231F272A6BF700BD FOREIGN KEY (status_id) REFERENCES workorder_status (id)');
        $this->addSql('ALTER TABLE workorder_available_statuses ADD CONSTRAINT FK_231F272A87DBBA FOREIGN KEY (available_status_id) REFERENCES workorder_status (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE workorder_available_statuses');
    }
}
