<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0011_Update_AccountHistory_Entity_Author_Nullable_True extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_history DROP FOREIGN KEY FK_EE916403F675F31B');
        $this->addSql('DROP INDEX IDX_EE916403F675F31B ON account_history');
        $this->addSql('ALTER TABLE account_history CHANGE author_id author INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE916403BDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_EE916403BDAFD8C8 ON account_history (author)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_history DROP FOREIGN KEY FK_EE916403BDAFD8C8');
        $this->addSql('DROP INDEX IDX_EE916403BDAFD8C8 ON account_history');
        $this->addSql('ALTER TABLE account_history CHANGE author author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE916403F675F31B FOREIGN KEY (author_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_EE916403F675F31B ON account_history (author_id)');
    }
}
