<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0157_Add_ParentAccount_Field_Opportunity_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE opportunity ADD parent_account INT DEFAULT NULL');
        $this->addSql('ALTER TABLE opportunity ADD CONSTRAINT FK_8389C3D7F7E22E2 FOREIGN KEY (parent_account) REFERENCES account (id)');
        $this->addSql('CREATE INDEX IDX_8389C3D7F7E22E2 ON opportunity (parent_account)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE opportunity DROP FOREIGN KEY FK_8389C3D7F7E22E2');
        $this->addSql('DROP INDEX IDX_8389C3D7F7E22E2 ON opportunity');
        $this->addSql('ALTER TABLE opportunity DROP parent_account');
    }
}
