<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0197_Change_From_To_Fields_Letter_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE letter DROP FOREIGN KEY FK_8E02EE0A30354A65');
        $this->addSql('ALTER TABLE letter DROP FOREIGN KEY FK_8E02EE0A78CED90B');
        $this->addSql('ALTER TABLE letter ADD CONSTRAINT FK_8E02EE0A30354A65 FOREIGN KEY (to_id) REFERENCES account_contact_person_history (id)');
        $this->addSql('ALTER TABLE letter ADD CONSTRAINT FK_8E02EE0A78CED90B FOREIGN KEY (from_id) REFERENCES contractor_user_history (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE letter DROP FOREIGN KEY FK_8E02EE0A78CED90B');
        $this->addSql('ALTER TABLE letter DROP FOREIGN KEY FK_8E02EE0A30354A65');
        $this->addSql('ALTER TABLE letter ADD CONSTRAINT FK_8E02EE0A78CED90B FOREIGN KEY (from_id) REFERENCES contractor_user (id)');
        $this->addSql('ALTER TABLE letter ADD CONSTRAINT FK_8E02EE0A30354A65 FOREIGN KEY (to_id) REFERENCES account_contact_person (id)');
    }
}
