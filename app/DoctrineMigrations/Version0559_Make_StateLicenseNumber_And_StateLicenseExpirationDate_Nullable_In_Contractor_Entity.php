<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0559_Make_StateLicenseNumber_And_StateLicenseExpirationDate_Nullable_In_Contractor_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor CHANGE state_license_number state_license_number VARCHAR(255) DEFAULT NULL, CHANGE state_license_expiration_date state_license_expiration_date DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor CHANGE state_license_number state_license_number VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE state_license_expiration_date state_license_expiration_date DATETIME NOT NULL');
    }
}
