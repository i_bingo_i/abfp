<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0271_Update_MunicipalityHistory_Entity_To_Actual_State extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE municipality_history ADD address_id INT DEFAULT NULL, ADD municipality_phone VARCHAR(255) DEFAULT NULL, ADD municipality_fax VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE municipality_history ADD CONSTRAINT FK_B20D04DBF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_B20D04DBF5B7AF75 ON municipality_history (address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE municipality_history DROP FOREIGN KEY FK_B20D04DBF5B7AF75');
        $this->addSql('DROP INDEX IDX_B20D04DBF5B7AF75 ON municipality_history');
        $this->addSql('ALTER TABLE municipality_history DROP address_id, DROP municipality_phone, DROP municipality_fax');
    }
}
