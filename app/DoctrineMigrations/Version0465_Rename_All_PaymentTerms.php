<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0465_Rename_All_PaymentTerms extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE payment_term SET name="Net 15 Days." WHERE alias="net_fifteen_days"');
        $this->addSql('UPDATE payment_term SET name="Net 30 Days." WHERE alias="net_thirty_days"');
        $this->addSql('UPDATE payment_term SET name="50% Deposit Due Before Work Can Start." WHERE alias="fifty_percent_deposit"');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE payment_term SET name="Net 15 days. Our price is firm for 30 days." WHERE alias="net_fifteen_days"');
        $this->addSql('UPDATE payment_term SET name="Net 30 days. Our price is firm for 30 days." WHERE alias="net_fifteen_days"');
    }
}
