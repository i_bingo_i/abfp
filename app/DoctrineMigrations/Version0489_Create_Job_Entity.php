<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0489_Create_Job_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE job (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, account_id INT DEFAULT NULL, division_id INT DEFAULT NULL, accounting_system_id VARCHAR(255) DEFAULT NULL, edit_sequence VARCHAR(255) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_FBD8E0F89395C3F3 (customer_id), INDEX IDX_FBD8E0F89B6B5FBA (account_id), INDEX IDX_FBD8E0F841859289 (division_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F89395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F89B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F841859289 FOREIGN KEY (division_id) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE invoices ADD job_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE invoices ADD CONSTRAINT FK_6A2F2F95BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('CREATE INDEX IDX_6A2F2F95BE04EA9 ON invoices (job_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invoices DROP FOREIGN KEY FK_6A2F2F95BE04EA9');
        $this->addSql('DROP TABLE job');
        $this->addSql('DROP INDEX IDX_6A2F2F95BE04EA9 ON invoices');
        $this->addSql('ALTER TABLE invoices DROP job_id');
    }
}
