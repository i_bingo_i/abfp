<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0121_Refactor_DepartmentChannel_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A45D83CC1');
        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A4CB3CC8D2');
        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A4457EB27E');
        $this->addSql('DROP INDEX IDX_1F5066A45D83CC1 ON department_channel');
        $this->addSql('DROP INDEX IDX_1F5066A4CB3CC8D2 ON department_channel');
        $this->addSql('ALTER TABLE department_channel ADD owner_agent_id INT DEFAULT NULL, ADD fees_basis_id INT DEFAULT NULL, ADD department_id INT DEFAULT NULL, ADD devision_id INT DEFAULT NULL, DROP state_id, DROP contractor_user_id');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A422F4C8C8 FOREIGN KEY (owner_agent_id) REFERENCES agent (id)');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A4C6944BCB FOREIGN KEY (fees_basis_id) REFERENCES fees_basis (id)');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A4AE80F5DF FOREIGN KEY (department_id) REFERENCES department (id)');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A45BB97207 FOREIGN KEY (devision_id) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A4457EB27E FOREIGN KEY (named_id) REFERENCES channel_named (id)');
        $this->addSql('CREATE INDEX IDX_1F5066A422F4C8C8 ON department_channel (owner_agent_id)');
        $this->addSql('CREATE INDEX IDX_1F5066A4C6944BCB ON department_channel (fees_basis_id)');
        $this->addSql('CREATE INDEX IDX_1F5066A4AE80F5DF ON department_channel (department_id)');
        $this->addSql('CREATE INDEX IDX_1F5066A45BB97207 ON department_channel (devision_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A422F4C8C8');
        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A4C6944BCB');
        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A4AE80F5DF');
        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A45BB97207');
        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A4457EB27E');
        $this->addSql('DROP INDEX IDX_1F5066A422F4C8C8 ON department_channel');
        $this->addSql('DROP INDEX IDX_1F5066A4C6944BCB ON department_channel');
        $this->addSql('DROP INDEX IDX_1F5066A4AE80F5DF ON department_channel');
        $this->addSql('DROP INDEX IDX_1F5066A45BB97207 ON department_channel');
        $this->addSql('ALTER TABLE department_channel ADD state_id INT DEFAULT NULL, ADD contractor_user_id INT DEFAULT NULL, DROP owner_agent_id, DROP fees_basis_id, DROP department_id, DROP devision_id');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A45D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A4CB3CC8D2 FOREIGN KEY (contractor_user_id) REFERENCES contractor_user (id)');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A4457EB27E FOREIGN KEY (named_id) REFERENCES licenses_named (id)');
        $this->addSql('CREATE INDEX IDX_1F5066A45D83CC1 ON department_channel (state_id)');
        $this->addSql('CREATE INDEX IDX_1F5066A4CB3CC8D2 ON department_channel (contractor_user_id)');
    }
}
