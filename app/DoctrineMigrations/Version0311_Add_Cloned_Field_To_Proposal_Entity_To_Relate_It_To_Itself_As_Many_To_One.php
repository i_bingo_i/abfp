<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0311_Add_Cloned_Field_To_Proposal_Entity_To_Relate_It_To_Itself_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal ADD cloned_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE594723735CA56 FOREIGN KEY (cloned_id) REFERENCES proposal (id)');
        $this->addSql('CREATE INDEX IDX_BFE594723735CA56 ON proposal (cloned_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE594723735CA56');
        $this->addSql('DROP INDEX IDX_BFE594723735CA56 ON proposal');
        $this->addSql('ALTER TABLE proposal DROP cloned_id');
    }
}
