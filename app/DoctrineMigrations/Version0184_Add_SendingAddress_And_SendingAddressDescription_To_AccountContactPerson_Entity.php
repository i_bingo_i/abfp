<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0184_Add_SendingAddress_And_SendingAddressDescription_To_AccountContactPerson_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person ADD sending_address_id INT DEFAULT NULL, ADD sending_address_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person ADD CONSTRAINT FK_F6B922FD6510ABA8 FOREIGN KEY (sending_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_F6B922FD6510ABA8 ON account_contact_person (sending_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person DROP FOREIGN KEY FK_F6B922FD6510ABA8');
        $this->addSql('DROP INDEX IDX_F6B922FD6510ABA8 ON account_contact_person');
        $this->addSql('ALTER TABLE account_contact_person DROP sending_address_id, DROP sending_address_description');
    }
}
