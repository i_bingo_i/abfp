<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0153_Create_Proposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proposal (id INT AUTO_INCREMENT NOT NULL, division_id INT DEFAULT NULL, account_id INT DEFAULT NULL, type_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, date_of_proposal DATETIME NOT NULL, grand_total DOUBLE PRECISION DEFAULT NULL, INDEX IDX_BFE5947241859289 (division_id), INDEX IDX_BFE594729B6B5FBA (account_id), INDEX IDX_BFE59472C54C8C93 (type_id), INDEX IDX_BFE5947261220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE5947241859289 FOREIGN KEY (division_id) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE594729B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE59472C54C8C93 FOREIGN KEY (type_id) REFERENCES proposal_type (id)');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE5947261220EA6 FOREIGN KEY (creator_id) REFERENCES contractor_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE proposal');
    }
}
