<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0087_Change_Fixed_And_Hourly_Price_And_LastTest_For_Service extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service CHANGE fixed_price fixed_price DOUBLE PRECISION DEFAULT NULL, CHANGE hourly_price hourly_price DOUBLE PRECISION DEFAULT NULL, CHANGE last_tested last_tested DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service CHANGE fixed_price fixed_price DOUBLE PRECISION NOT NULL, CHANGE hourly_price hourly_price DOUBLE PRECISION NOT NULL, CHANGE last_tested last_tested DATETIME NOT NULL');
    }
}
