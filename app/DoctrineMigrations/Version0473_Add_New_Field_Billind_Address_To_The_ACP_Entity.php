<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0473_Add_New_Field_Billind_Address_To_The_ACP_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person ADD billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person ADD CONSTRAINT FK_F6B922FD79D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_F6B922FD79D0C0E4 ON account_contact_person (billing_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person DROP FOREIGN KEY FK_F6B922FD79D0C0E4');
        $this->addSql('DROP INDEX IDX_F6B922FD79D0C0E4 ON account_contact_person');
        $this->addSql('ALTER TABLE account_contact_person DROP billing_address_id');
    }
}
