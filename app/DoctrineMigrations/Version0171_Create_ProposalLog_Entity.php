<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0171_Create_ProposalLog_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proposal_log (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, proposal_id INT DEFAULT NULL, author_id INT DEFAULT NULL, date DATETIME NOT NULL, comment VARCHAR(255) DEFAULT NULL, INDEX IDX_81AFA0B0C54C8C93 (type_id), INDEX IDX_81AFA0B0F4792058 (proposal_id), INDEX IDX_81AFA0B0F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proposal_log ADD CONSTRAINT FK_81AFA0B0C54C8C93 FOREIGN KEY (type_id) REFERENCES proposal_log_type (id)');
        $this->addSql('ALTER TABLE proposal_log ADD CONSTRAINT FK_81AFA0B0F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('ALTER TABLE proposal_log ADD CONSTRAINT FK_81AFA0B0F675F31B FOREIGN KEY (author_id) REFERENCES contractor_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE proposal_log');
    }
}
