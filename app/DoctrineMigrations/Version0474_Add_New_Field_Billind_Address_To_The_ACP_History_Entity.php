<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0474_Add_New_Field_Billind_Address_To_The_ACP_History_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person_history ADD billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F5879D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address_history (id)');
        $this->addSql('CREATE INDEX IDX_950F5F5879D0C0E4 ON account_contact_person_history (billing_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F5879D0C0E4');
        $this->addSql('DROP INDEX IDX_950F5F5879D0C0E4 ON account_contact_person_history');
        $this->addSql('ALTER TABLE account_contact_person_history DROP billing_address_id');
    }
}
