<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0103_Create_LicensesDropboxChoices_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE licenses_dropbox_choices (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, option_value VARCHAR(255) DEFAULT NULL, select_default TINYINT(1) DEFAULT \'0\', date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_EF4F4CF4443707B0 (field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE licenses_dropbox_choices ADD CONSTRAINT FK_EF4F4CF4443707B0 FOREIGN KEY (field_id) REFERENCES licenses_dynamic_field (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE licenses_dropbox_choices');
    }
}
