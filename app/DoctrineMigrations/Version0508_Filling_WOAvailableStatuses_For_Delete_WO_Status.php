<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0508_Filling_WOAvailableStatuses_For_Delete_WO_Status extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'to_be_scheduled\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'scheduled\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'to_be_done_today\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'not_completed\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'pending_payment\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_reports\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_bill_create_invoice\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'reschedule\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'verify_job_results\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'submit_invoice\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_invoice\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_reports_to_client\'))'
        );
        $this->addSql(
            'INSERT INTO workorder_available_statuses (status_id, available_status_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_reports_to_ahj\'))'
        );
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE * FROM workorder_available_statuses');
    }
}