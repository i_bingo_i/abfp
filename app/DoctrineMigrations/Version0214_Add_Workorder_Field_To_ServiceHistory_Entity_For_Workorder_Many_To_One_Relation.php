<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0214_Add_Workorder_Field_To_ServiceHistory_Entity_For_Workorder_Many_To_One_Relation extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history ADD workorder_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D72C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
        $this->addSql('CREATE INDEX IDX_E83E22D72C1C3467 ON service_history (workorder_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D72C1C3467');
        $this->addSql('DROP INDEX IDX_E83E22D72C1C3467 ON service_history');
        $this->addSql('ALTER TABLE service_history DROP workorder_id');
    }
}
