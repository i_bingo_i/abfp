<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0053_Create_DynamicFieldValueHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dynamic_field_value_history (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, value VARCHAR(255) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_68704F9C443707B0 (field_id), INDEX IDX_68704F9C835E0EEE (owner_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dynamic_field_value_history ADD CONSTRAINT FK_68704F9C443707B0 FOREIGN KEY (field_id) REFERENCES dynamic_field (id)');
        $this->addSql('ALTER TABLE dynamic_field_value_history ADD CONSTRAINT FK_68704F9C835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES dynamic_field_value (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE dynamic_field_value_history');
    }
}
