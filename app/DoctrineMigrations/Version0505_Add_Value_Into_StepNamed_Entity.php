<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0505_Add_Value_Into_StepNamed_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Created\', \'created\')');
        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Scheduled\', \'scheduled\')');
        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Finished\', \'finished\')');
        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Verify Job Results\', \'verify_job_results\')');
        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Submit Invoice\', \'submit_invoice\')');
        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Send Invoice\', \'send_invoice\')');
        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Send Reports To Client\', \'send_reports_to_client\')');
        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Send Reports to AHJ\', \'send_reports_to_ahj\')');
        $this->addSql('INSERT INTO workorder_step_named (name, alias) VALUES (
            \'Pending Payment\', \'pending_payment\')');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE * FROM workorder_step_named');
    }
}