<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0326_Create_RepairDeviceInfo_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE repair_device_info (id INT AUTO_INCREMENT NOT NULL, special_notification_id INT DEFAULT NULL, proposal_id INT DEFAULT NULL, device_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, comments VARCHAR(255) DEFAULT NULL, estimation_time DOUBLE PRECISION NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_97B48F4CA2485031 (special_notification_id), INDEX IDX_97B48F4CF4792058 (proposal_id), INDEX IDX_97B48F4C94A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE repair_device_info ADD CONSTRAINT FK_97B48F4CA2485031 FOREIGN KEY (special_notification_id) REFERENCES special_notification (id)');
        $this->addSql('ALTER TABLE repair_device_info ADD CONSTRAINT FK_97B48F4CF4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('ALTER TABLE repair_device_info ADD CONSTRAINT FK_97B48F4C94A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE59472B60DF54F');
        $this->addSql('DROP INDEX IDX_BFE59472B60DF54F ON proposal');
        $this->addSql('ALTER TABLE proposal DROP special_notifications_id, DROP price, DROP estimation_time, DROP comments');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE repair_device_info');
        $this->addSql('ALTER TABLE proposal ADD special_notifications_id INT DEFAULT NULL, ADD price DOUBLE PRECISION DEFAULT NULL, ADD estimation_time DOUBLE PRECISION DEFAULT NULL, ADD comments LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE59472B60DF54F FOREIGN KEY (special_notifications_id) REFERENCES special_notification (id)');
        $this->addSql('CREATE INDEX IDX_BFE59472B60DF54F ON proposal (special_notifications_id)');
    }
}
