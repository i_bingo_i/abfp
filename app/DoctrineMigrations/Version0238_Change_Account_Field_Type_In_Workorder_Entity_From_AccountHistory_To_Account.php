<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0238_Change_Account_Field_Type_In_Workorder_Entity_From_AccountHistory_To_Account extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder DROP FOREIGN KEY FK_51CF52BB9B6B5FBA');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BB9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder DROP FOREIGN KEY FK_51CF52BB9B6B5FBA');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BB9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_history (id)');
    }
}
