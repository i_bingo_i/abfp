<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0268_Change_BillingAddress_To_OldSystemMasLevelAddress_Account_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A479D0C0E4');
        $this->addSql('DROP INDEX UNIQ_7D3656A479D0C0E4 ON account');
        $this->addSql('ALTER TABLE account CHANGE billing_address_id old_system_mas_level_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A4E5DE27B0 FOREIGN KEY (old_system_mas_level_address_id) REFERENCES address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D3656A4E5DE27B0 ON account (old_system_mas_level_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A4E5DE27B0');
        $this->addSql('DROP INDEX UNIQ_7D3656A4E5DE27B0 ON account');
        $this->addSql('ALTER TABLE account CHANGE old_system_mas_level_address_id billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A479D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D3656A479D0C0E4 ON account (billing_address_id)');
    }
}
