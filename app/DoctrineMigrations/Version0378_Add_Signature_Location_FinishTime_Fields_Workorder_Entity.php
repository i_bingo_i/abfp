<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0378_Add_Signature_Location_FinishTime_Fields_Workorder_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder ADD location_id INT DEFAULT NULL, ADD signature VARCHAR(255) DEFAULT NULL, ADD finish_time DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BB64D218E FOREIGN KEY (location_id) REFERENCES coordinate (id)');
        $this->addSql('CREATE INDEX IDX_51CF52BB64D218E ON workorder (location_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder DROP FOREIGN KEY FK_51CF52BB64D218E');
        $this->addSql('DROP INDEX IDX_51CF52BB64D218E ON workorder');
        $this->addSql('ALTER TABLE workorder DROP location_id, DROP signature, DROP finish_time');
    }
}
