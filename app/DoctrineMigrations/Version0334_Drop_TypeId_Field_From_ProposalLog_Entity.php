<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0334_Drop_TypeId_Field_From_ProposalLog_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_log DROP FOREIGN KEY FK_81AFA0B0C54C8C93');
        $this->addSql('DROP INDEX IDX_81AFA0B0C54C8C93 ON proposal_log');
        $this->addSql('ALTER TABLE proposal_log DROP type_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_log ADD type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal_log ADD CONSTRAINT FK_81AFA0B0C54C8C93 FOREIGN KEY (type_id) REFERENCES proposal_log_type (id)');
        $this->addSql('CREATE INDEX IDX_81AFA0B0C54C8C93 ON proposal_log (type_id)');
    }
}
