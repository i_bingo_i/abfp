<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0139_Add_entityValue_Field_To_AgentChannelDynamicFieldValue_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value ADD entity_value_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value ADD CONSTRAINT FK_EAC5B2F85BD407E2 FOREIGN KEY (entity_value_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_EAC5B2F85BD407E2 ON agent_channel_dynamic_field_value (entity_value_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value DROP FOREIGN KEY FK_EAC5B2F85BD407E2');
        $this->addSql('DROP INDEX IDX_EAC5B2F85BD407E2 ON agent_channel_dynamic_field_value');
        $this->addSql('ALTER TABLE agent_channel_dynamic_field_value DROP entity_value_id');
    }
}
