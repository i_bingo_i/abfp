<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0089_Update_ContractorService_Entity_Change_Price_Fields_Nullable_True_Add_Field_State_Many_To_One_Relation extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor_service ADD state_id INT DEFAULT NULL, CHANGE fixed_price fixed_price DOUBLE PRECISION DEFAULT NULL, CHANGE hours_price hours_price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE contractor_service ADD CONSTRAINT FK_8DCF81955D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('CREATE INDEX IDX_8DCF81955D83CC1 ON contractor_service (state_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor_service DROP FOREIGN KEY FK_8DCF81955D83CC1');
        $this->addSql('DROP INDEX IDX_8DCF81955D83CC1 ON contractor_service');
        $this->addSql('ALTER TABLE contractor_service DROP state_id, CHANGE fixed_price fixed_price DOUBLE PRECISION NOT NULL, CHANGE hours_price hours_price DOUBLE PRECISION NOT NULL');
    }
}
