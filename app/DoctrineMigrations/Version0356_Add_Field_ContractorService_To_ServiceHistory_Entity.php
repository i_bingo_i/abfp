<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0356_Add_Field_ContractorService_To_ServiceHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history ADD contractor_service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D7B4DAC98D FOREIGN KEY (contractor_service_id) REFERENCES contractor_service (id)');
        $this->addSql('CREATE INDEX IDX_E83E22D7B4DAC98D ON service_history (contractor_service_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D7B4DAC98D');
        $this->addSql('DROP INDEX IDX_E83E22D7B4DAC98D ON service_history');
        $this->addSql('ALTER TABLE service_history DROP contractor_service_id');
    }
}
