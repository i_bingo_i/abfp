<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0182_Change_ContactPerson_On_AccountContactPerson_Proposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2F4792058');
        $this->addSql('DROP INDEX IDX_E19D9AD2F4792058 ON service');
        $this->addSql('ALTER TABLE service DROP proposal_id');
        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE594724F8A983C');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE594724F8A983C FOREIGN KEY (contact_person_id) REFERENCES account_contact_person_history (id)');
        $this->addSql('ALTER TABLE service_history ADD proposal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D7F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('CREATE INDEX IDX_E83E22D7F4792058 ON service_history (proposal_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE594724F8A983C');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE594724F8A983C FOREIGN KEY (contact_person_id) REFERENCES contact_person_history (id)');
        $this->addSql('ALTER TABLE service ADD proposal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD2F4792058 ON service (proposal_id)');
        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D7F4792058');
        $this->addSql('DROP INDEX IDX_E83E22D7F4792058 ON service_history');
        $this->addSql('ALTER TABLE service_history DROP proposal_id');
    }
}
