<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0248_Create_Invoices_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE invoices (id INT AUTO_INCREMENT NOT NULL, workorder_id INT DEFAULT NULL, file VARCHAR(255) NOT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_6A2F2F952C1C3467 (workorder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE invoices ADD CONSTRAINT FK_6A2F2F952C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
        $this->addSql('ALTER TABLE workorder_log ADD invoice_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder_log ADD CONSTRAINT FK_3711E8992989F1FD FOREIGN KEY (invoice_id) REFERENCES invoices (id)');
        $this->addSql('CREATE INDEX IDX_3711E8992989F1FD ON workorder_log (invoice_id)');
        $this->addSql('ALTER TABLE workorder DROP invoice');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log DROP FOREIGN KEY FK_3711E8992989F1FD');
        $this->addSql('DROP TABLE invoices');
        $this->addSql('ALTER TABLE workorder ADD invoice VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX IDX_3711E8992989F1FD ON workorder_log');
        $this->addSql('ALTER TABLE workorder_log DROP invoice_id');
    }
}
