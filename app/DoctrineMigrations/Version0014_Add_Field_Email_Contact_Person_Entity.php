<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0014_Add_Field_Email_Contact_Person_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_person ADD email VARCHAR(50) NOT NULL, CHANGE phone phone VARCHAR(50) NOT NULL, CHANGE cell cell VARCHAR(50) NOT NULL, CHANGE fax fax VARCHAR(50) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_person DROP email, CHANGE phone phone VARCHAR(30) NOT NULL COLLATE utf8_unicode_ci, CHANGE cell cell VARCHAR(30) NOT NULL COLLATE utf8_unicode_ci, CHANGE fax fax VARCHAR(30) NOT NULL COLLATE utf8_unicode_ci');
    }
}
