<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0175_Add_ManyToMany_Field_Responsibilities_To_DeviceCategory_For_AccountContactPerson_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE account_contact_persons_responsibilities (account_contact_person_id INT NOT NULL, device_category_id INT NOT NULL, INDEX IDX_199EC2CECE52684D (account_contact_person_id), INDEX IDX_199EC2CE60C6C924 (device_category_id), PRIMARY KEY(account_contact_person_id, device_category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE account_contact_persons_responsibilities ADD CONSTRAINT FK_199EC2CECE52684D FOREIGN KEY (account_contact_person_id) REFERENCES account_contact_person (id)');
        $this->addSql('ALTER TABLE account_contact_persons_responsibilities ADD CONSTRAINT FK_199EC2CE60C6C924 FOREIGN KEY (device_category_id) REFERENCES device_category (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE account_contact_persons_responsibilities');
    }
}
