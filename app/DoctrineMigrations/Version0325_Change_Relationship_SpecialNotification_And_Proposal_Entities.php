<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0325_Change_Relationship_SpecialNotification_And_Proposal_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE special_notifications_for_proposals');
        $this->addSql('ALTER TABLE proposal ADD special_notifications_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE59472B60DF54F FOREIGN KEY (special_notifications_id) REFERENCES special_notification (id)');
        $this->addSql('CREATE INDEX IDX_BFE59472B60DF54F ON proposal (special_notifications_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE special_notifications_for_proposals (proposal_id INT NOT NULL, special_notification_id INT NOT NULL, INDEX IDX_C2844A82F4792058 (proposal_id), INDEX IDX_C2844A82A2485031 (special_notification_id), PRIMARY KEY(proposal_id, special_notification_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE special_notifications_for_proposals ADD CONSTRAINT FK_C2844A82A2485031 FOREIGN KEY (special_notification_id) REFERENCES special_notification (id)');
        $this->addSql('ALTER TABLE special_notifications_for_proposals ADD CONSTRAINT FK_C2844A82F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE59472B60DF54F');
        $this->addSql('DROP INDEX IDX_BFE59472B60DF54F ON proposal');
        $this->addSql('ALTER TABLE proposal DROP special_notifications_id');
    }
}
