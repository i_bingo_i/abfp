<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0301_Add_rangeQuestion_Field_To_RangeResult_Entity_To_Relate_It_To_RangeQuestion_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE range_result ADD range_question_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE range_result ADD CONSTRAINT FK_3AEC1F6380802003 FOREIGN KEY (range_question_id) REFERENCES range_question (id)');
        $this->addSql('CREATE INDEX IDX_3AEC1F6380802003 ON range_result (range_question_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE range_result DROP FOREIGN KEY FK_3AEC1F6380802003');
        $this->addSql('DROP INDEX IDX_3AEC1F6380802003 ON range_result');
        $this->addSql('ALTER TABLE range_result DROP range_question_id');
    }
}
