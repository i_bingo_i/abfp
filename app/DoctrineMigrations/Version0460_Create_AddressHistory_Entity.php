<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0460_Create_AddressHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE address_history (id INT AUTO_INCREMENT NOT NULL, state_id INT DEFAULT NULL, address_type_id INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, city VARCHAR(100) DEFAULT NULL, zip VARCHAR(50) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_7C5F87D35D83CC1 (state_id), INDEX IDX_7C5F87D39EA97B0B (address_type_id), INDEX IDX_7C5F87D3835E0EEE (owner_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE address_history ADD CONSTRAINT FK_7C5F87D35D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('ALTER TABLE address_history ADD CONSTRAINT FK_7C5F87D39EA97B0B FOREIGN KEY (address_type_id) REFERENCES address_type (id)');
        $this->addSql('ALTER TABLE address_history ADD CONSTRAINT FK_7C5F87D3835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES address (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE address_history');
    }
}
