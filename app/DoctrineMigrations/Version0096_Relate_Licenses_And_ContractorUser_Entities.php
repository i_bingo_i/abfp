<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0096_Relate_Licenses_And_ContractorUser_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE licenses ADD contractor_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE licenses ADD CONSTRAINT FK_7F320F3FCB3CC8D2 FOREIGN KEY (contractor_user_id) REFERENCES contractor_user (id)');
        $this->addSql('CREATE INDEX IDX_7F320F3FCB3CC8D2 ON licenses (contractor_user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE licenses DROP FOREIGN KEY FK_7F320F3FCB3CC8D2');
        $this->addSql('DROP INDEX IDX_7F320F3FCB3CC8D2 ON licenses');
        $this->addSql('ALTER TABLE licenses DROP contractor_user_id');
    }
}
