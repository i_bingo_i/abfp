<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0368_Change_Link_Proposal_To_ProposalHistory_For_ServiceHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D7F4792058');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D7F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal_history (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D7F4792058');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D7F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
    }
}
