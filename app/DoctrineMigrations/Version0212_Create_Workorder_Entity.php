<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0212_Create_Workorder_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE workorder (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, status_id INT DEFAULT NULL, division_id INT DEFAULT NULL, proposal_id INT DEFAULT NULL, grand_total DOUBLE PRECISION DEFAULT NULL, total_service_fee DOUBLE PRECISION DEFAULT NULL, directions LONGTEXT DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_51CF52BB9B6B5FBA (account_id), INDEX IDX_51CF52BB6BF700BD (status_id), INDEX IDX_51CF52BB41859289 (division_id), INDEX IDX_51CF52BBF4792058 (proposal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BB9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_history (id)');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BB6BF700BD FOREIGN KEY (status_id) REFERENCES workorder_status (id)');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BB41859289 FOREIGN KEY (division_id) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BBF4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE workorder');
    }
}
