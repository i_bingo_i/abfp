<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0068_Add_Field_Type_For_Contractor extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor ADD type INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contractor ADD CONSTRAINT FK_437BD2EF8CDE5729 FOREIGN KEY (type) REFERENCES contractor_type (id)');
        $this->addSql('CREATE INDEX IDX_437BD2EF8CDE5729 ON contractor (type)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor DROP FOREIGN KEY FK_437BD2EF8CDE5729');
        $this->addSql('DROP INDEX IDX_437BD2EF8CDE5729 ON contractor');
        $this->addSql('ALTER TABLE contractor DROP type');
    }
}
