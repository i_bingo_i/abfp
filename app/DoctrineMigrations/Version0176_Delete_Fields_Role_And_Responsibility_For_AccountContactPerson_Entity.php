<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0176_Delete_Fields_Role_And_Responsibility_For_AccountContactPerson_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person DROP FOREIGN KEY FK_F6B922FD57698A6A');
        $this->addSql('ALTER TABLE account_contact_person DROP FOREIGN KEY FK_F6B922FD694E8A08');
        $this->addSql('DROP INDEX IDX_F6B922FD57698A6A ON account_contact_person');
        $this->addSql('DROP INDEX IDX_F6B922FD694E8A08 ON account_contact_person');
        $this->addSql('ALTER TABLE account_contact_person DROP role, DROP responsibility');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person ADD role INT DEFAULT NULL, ADD responsibility INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person ADD CONSTRAINT FK_F6B922FD57698A6A FOREIGN KEY (role) REFERENCES contact_person_role (id)');
        $this->addSql('ALTER TABLE account_contact_person ADD CONSTRAINT FK_F6B922FD694E8A08 FOREIGN KEY (responsibility) REFERENCES contact_person_responsibility (id)');
        $this->addSql('CREATE INDEX IDX_F6B922FD57698A6A ON account_contact_person (role)');
        $this->addSql('CREATE INDEX IDX_F6B922FD694E8A08 ON account_contact_person (responsibility)');
    }
}
