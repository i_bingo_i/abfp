<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0379_Add_ManyToMany_Field_notIncludedItems_To_WorkOrderEntity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE notIncludedItems_workorders (workorder_id INT NOT NULL, not_included_id INT NOT NULL, INDEX IDX_E384424C2C1C3467 (workorder_id), INDEX IDX_E384424CC1CB14A1 (not_included_id), PRIMARY KEY(workorder_id, not_included_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE notIncludedItems_workorders ADD CONSTRAINT FK_E384424C2C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notIncludedItems_workorders ADD CONSTRAINT FK_E384424CC1CB14A1 FOREIGN KEY (not_included_id) REFERENCES not_included (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE notIncludedItems_workorders');
    }
}
