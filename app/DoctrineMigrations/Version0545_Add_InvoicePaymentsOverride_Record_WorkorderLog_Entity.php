<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0545_Add_InvoicePaymentsOverride_Record_WorkorderLog_Entity extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO workorder_log_type (name, alias) VALUES ('INVOICE PAYMENTS OVERRIDE WAS EDITED', 'invoice_payments_override_was_edited')");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM workorder_log_type WHERE alias = 'invoice_payments_override_was_edited'");
    }
}