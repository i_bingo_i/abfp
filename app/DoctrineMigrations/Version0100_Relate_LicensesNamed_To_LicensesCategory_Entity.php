<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0100_Relate_LicensesNamed_To_LicensesCategory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE licenses_named ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE licenses_named ADD CONSTRAINT FK_892B1DF512469DE2 FOREIGN KEY (category_id) REFERENCES licenses_category (id)');
        $this->addSql('CREATE INDEX IDX_892B1DF512469DE2 ON licenses_named (category_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE licenses_named DROP FOREIGN KEY FK_892B1DF512469DE2');
        $this->addSql('DROP INDEX IDX_892B1DF512469DE2 ON licenses_named');
        $this->addSql('ALTER TABLE licenses_named DROP category_id');
    }
}
