<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0555_Add_Precision_And_Scale_To_Decimal_Invoices_InvoiceLine_Item_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invoices CHANGE total total NUMERIC(10, 2) DEFAULT NULL, CHANGE payment_applied payment_applied NUMERIC(10, 2) DEFAULT NULL, CHANGE balance_due balance_due NUMERIC(10, 2) DEFAULT NULL, CHANGE override_payment override_payment NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE invoice_line CHANGE rate rate NUMERIC(10, 2) DEFAULT NULL, CHANGE amount amount NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE item CHANGE rate rate NUMERIC(10, 2) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invoice_line CHANGE rate rate NUMERIC(10, 10) DEFAULT NULL, CHANGE amount amount NUMERIC(10, 10) DEFAULT NULL');
        $this->addSql('ALTER TABLE invoices CHANGE total total NUMERIC(10, 10) DEFAULT NULL, CHANGE payment_applied payment_applied NUMERIC(10, 10) DEFAULT NULL, CHANGE balance_due balance_due NUMERIC(10, 10) DEFAULT NULL, CHANGE override_payment override_payment NUMERIC(10, 10) DEFAULT NULL');
        $this->addSql('ALTER TABLE item CHANGE rate rate NUMERIC(10, 10) DEFAULT NULL');
    }
}
