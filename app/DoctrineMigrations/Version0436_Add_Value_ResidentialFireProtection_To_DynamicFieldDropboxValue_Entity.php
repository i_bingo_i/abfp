<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0436_Add_Value_ResidentialFireProtection_To_DynamicFieldDropboxValue_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO dynamic_field_dropbox_value (field_id, option_value, date_create, date_update, alias) VALUES (6, \'Residential Fire Protection\', NOW(), NOW(), \'backflow_device_hazard_residential_fire_protection\')');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM dynamic_field_dropbox_value WHERE field_id = 6 AND option_value = \'Residential Fire Protection\' AND alias = \'backflow_device_hazard_residential_fire_protection\'');
    }
}
