<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0242_Add_FrozenServiceForWorkorder_Entity_And_Linked_To_Other_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE frozen_service_for_workorder (id INT AUTO_INCREMENT NOT NULL, service_history_id INT DEFAULT NULL, service_id INT DEFAULT NULL, workorder_id INT DEFAULT NULL, date_create DATETIME NOT NULL, INDEX IDX_3C682E908854BDE6 (service_history_id), INDEX IDX_3C682E90ED5CA9E6 (service_id), INDEX IDX_3C682E902C1C3467 (workorder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE frozen_service_for_workorder ADD CONSTRAINT FK_3C682E908854BDE6 FOREIGN KEY (service_history_id) REFERENCES service_history (id)');
        $this->addSql('ALTER TABLE frozen_service_for_workorder ADD CONSTRAINT FK_3C682E90ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id)');
        $this->addSql('ALTER TABLE frozen_service_for_workorder ADD CONSTRAINT FK_3C682E902C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE frozen_service_for_workorder');
    }
}
