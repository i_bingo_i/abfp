<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0549_Add_ShipToAddress_Type_AddressType_Entity extends AbstractMigration
{

    public function up(Schema $schema)
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("INSERT INTO address_type (name, alias, date_create, date_update, sort) VALUES ('Ship to Address', 'ship_to_address', '{$now}', '{$now}', 8)");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("DELETE FROM address_type WHERE alias = 'ship_to_address'");
    }
}