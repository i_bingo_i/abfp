<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0193_Create_Letter_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE letter (id INT AUTO_INCREMENT NOT NULL, proposal_id INT DEFAULT NULL, from_id INT DEFAULT NULL, to_id INT DEFAULT NULL, subject VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL, attachment VARCHAR(255) NOT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_8E02EE0AF4792058 (proposal_id), INDEX IDX_8E02EE0A78CED90B (from_id), INDEX IDX_8E02EE0A30354A65 (to_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE letter ADD CONSTRAINT FK_8E02EE0AF4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('ALTER TABLE letter ADD CONSTRAINT FK_8E02EE0A78CED90B FOREIGN KEY (from_id) REFERENCES contractor_user (id)');
        $this->addSql('ALTER TABLE letter ADD CONSTRAINT FK_8E02EE0A30354A65 FOREIGN KEY (to_id) REFERENCES account_contact_person (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE letter');
    }
}
