<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0249_ReRelate_WorkorderFrozenContent_Entity_To_DeviceHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_frozen_content DROP FOREIGN KEY FK_AE06C4E79936C794');
        $this->addSql('ALTER TABLE workorder_frozen_content ADD CONSTRAINT FK_AE06C4E79936C794 FOREIGN KEY (device_history_id) REFERENCES device_history (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_frozen_content DROP FOREIGN KEY FK_AE06C4E79936C794');
        $this->addSql('ALTER TABLE workorder_frozen_content ADD CONSTRAINT FK_AE06C4E79936C794 FOREIGN KEY (device_history_id) REFERENCES device (id)');
    }
}
