<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0283_Add_Report_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, status_id INT DEFAULT NULL, service_id INT DEFAULT NULL, contractor_id INT DEFAULT NULL, tester_id INT DEFAULT NULL, workorder_id INT DEFAULT NULL, comment VARCHAR(255) DEFAULT NULL, file_report VARCHAR(255) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_C42F77846BF700BD (status_id), INDEX IDX_C42F7784ED5CA9E6 (service_id), INDEX IDX_C42F7784B0265DC7 (contractor_id), INDEX IDX_C42F7784979A21C1 (tester_id), INDEX IDX_C42F77842C1C3467 (workorder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F77846BF700BD FOREIGN KEY (status_id) REFERENCES report_status (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784B0265DC7 FOREIGN KEY (contractor_id) REFERENCES contractor (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784979A21C1 FOREIGN KEY (tester_id) REFERENCES contractor_user (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F77842C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE report');
    }
}
