<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0328_Add_Properties_Field_To_Proposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal ADD after_hours_work_cost VARCHAR(100) DEFAULT NULL, ADD discount VARCHAR(100) DEFAULT NULL, ADD additional_comment LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal_history ADD after_hours_work_cost VARCHAR(100) DEFAULT NULL, ADD discount VARCHAR(100) DEFAULT NULL, ADD additional_comment LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal DROP after_hours_work_cost, DROP discount, DROP additional_comment');
        $this->addSql('ALTER TABLE proposal_history DROP after_hours_work_cost, DROP discount, DROP additional_comment');
    }
}
