<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0519_Filling_WOAvailableActions_For_SendInvoice_WO_Status extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(
            'INSERT INTO workorder_available_actions (action_id, available_action_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_invoice\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_reports_to_client\'))'
        );

        $this->addSql(
            'INSERT INTO workorder_available_actions (action_id, available_action_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_invoice\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_reports_to_ahj\'))'
        );

        $this->addSql(
            'INSERT INTO workorder_available_actions (action_id, available_action_id)
                  VALUES ((SELECT id FROM workorder_status WHERE workorder_status.alias=\'send_invoice\'),
                  (SELECT id FROM workorder_status WHERE workorder_status.alias=\'delete\'))'
        );
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE * FROM workorder_available_actions');
    }
}