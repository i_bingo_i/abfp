<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0088_Add_ServiceHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE service_history (id INT AUTO_INCREMENT NOT NULL, named INT DEFAULT NULL, device_id INT DEFAULT NULL, account_id INT DEFAULT NULL, company_last_tested INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, author INT DEFAULT NULL, fixed_price DOUBLE PRECISION DEFAULT NULL, hourly_price DOUBLE PRECISION DEFAULT NULL, last_tested DATETIME DEFAULT NULL, inspection_due DATETIME NOT NULL, deleted TINYINT(1) DEFAULT \'0\', comment VARCHAR(20) DEFAULT NULL, INDEX IDX_E83E22D771E0CC87 (named), INDEX IDX_E83E22D794A4C7D4 (device_id), INDEX IDX_E83E22D79B6B5FBA (account_id), INDEX IDX_E83E22D79B52D956 (company_last_tested), INDEX IDX_E83E22D7835E0EEE (owner_entity_id), INDEX IDX_E83E22D7BDAFD8C8 (author), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D771E0CC87 FOREIGN KEY (named) REFERENCES service_named (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D794A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D79B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D79B52D956 FOREIGN KEY (company_last_tested) REFERENCES contractor (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D7835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES service (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D7BDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE service_history');
    }
}
