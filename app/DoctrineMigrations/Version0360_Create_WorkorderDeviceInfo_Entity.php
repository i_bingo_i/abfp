<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0360_Create_WorkorderDeviceInfo_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE workorder_device_info (id INT AUTO_INCREMENT NOT NULL, repair_special_notification_id INT DEFAULT NULL, workorder_id INT DEFAULT NULL, device_id INT DEFAULT NULL, repair_comments VARCHAR(255) DEFAULT NULL, repair_estimation_time DOUBLE PRECISION DEFAULT NULL, inspection_estimation_time DOUBLE PRECISION DEFAULT NULL, subtotal_inspections_fee DOUBLE PRECISION DEFAULT NULL, repair_parts_and_labour_price DOUBLE PRECISION DEFAULT NULL, subtotal_munic_and_proc_fee DOUBLE PRECISION DEFAULT NULL, device_total DOUBLE PRECISION DEFAULT NULL, INDEX IDX_D53ECF8555272D6 (repair_special_notification_id), INDEX IDX_D53ECF852C1C3467 (workorder_id), INDEX IDX_D53ECF8594A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workorder_device_info ADD CONSTRAINT FK_D53ECF8555272D6 FOREIGN KEY (repair_special_notification_id) REFERENCES special_notification (id)');
        $this->addSql('ALTER TABLE workorder_device_info ADD CONSTRAINT FK_D53ECF852C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
        $this->addSql('ALTER TABLE workorder_device_info ADD CONSTRAINT FK_D53ECF8594A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE workorder_device_info');
    }
}
