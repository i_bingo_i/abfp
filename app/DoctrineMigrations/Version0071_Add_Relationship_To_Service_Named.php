<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0071_Add_Relationship_To_Service_Named extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE service_named_device_named (service_named_id INT NOT NULL, device_named_id INT NOT NULL, INDEX IDX_ADE484A8BC08E943 (service_named_id), INDEX IDX_ADE484A8CB4AEB5E (device_named_id), PRIMARY KEY(service_named_id, device_named_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE service_named_device_named ADD CONSTRAINT FK_ADE484A8BC08E943 FOREIGN KEY (service_named_id) REFERENCES service_named (id)');
        $this->addSql('ALTER TABLE service_named_device_named ADD CONSTRAINT FK_ADE484A8CB4AEB5E FOREIGN KEY (device_named_id) REFERENCES device_named (id)');
        $this->addSql('ALTER TABLE service_named ADD frequency_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_named ADD CONSTRAINT FK_D892216094879022 FOREIGN KEY (frequency_id) REFERENCES service_frequency (id)');
        $this->addSql('CREATE INDEX IDX_D892216094879022 ON service_named (frequency_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE service_named_device_named');
        $this->addSql('ALTER TABLE service_named DROP FOREIGN KEY FK_D892216094879022');
        $this->addSql('DROP INDEX IDX_D892216094879022 ON service_named');
        $this->addSql('ALTER TABLE service_named DROP frequency_id');
    }
}
