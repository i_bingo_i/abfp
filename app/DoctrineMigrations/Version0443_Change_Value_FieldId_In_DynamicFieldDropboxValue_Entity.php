<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0443_Change_Value_FieldId_In_DynamicFieldDropboxValue_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE dynamic_field_dropbox_value SET field_id=(SELECT df.id FROM dynamic_field AS df LEFT JOIN device_named dn ON df.device_id = dn.id WHERE dn.alias=\'backflow_device\' and df.alias=\'hazard\') WHERE alias=\'backflow_device_hazard_residential_fire_protection\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE dynamic_field_dropbox_value SET field_id=6 WHERE alias=\'backflow_device_hazard_residential_fire_protection\'');
    }
}
