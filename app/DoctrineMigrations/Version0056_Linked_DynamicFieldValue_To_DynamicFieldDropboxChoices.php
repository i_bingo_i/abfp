<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0056_Linked_DynamicFieldValue_To_DynamicFieldDropboxChoices extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dynamic_field_value ADD option_value_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dynamic_field_value ADD CONSTRAINT FK_F0552AD2D957CA06 FOREIGN KEY (option_value_id) REFERENCES dynamic_field_dropbox_value (id)');
        $this->addSql('CREATE INDEX IDX_F0552AD2D957CA06 ON dynamic_field_value (option_value_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dynamic_field_value DROP FOREIGN KEY FK_F0552AD2D957CA06');
        $this->addSql('DROP INDEX IDX_F0552AD2D957CA06 ON dynamic_field_value');
        $this->addSql('ALTER TABLE dynamic_field_value DROP option_value_id');
    }
}
