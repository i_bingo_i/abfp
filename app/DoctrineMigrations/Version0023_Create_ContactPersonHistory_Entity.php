<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0023_Create_ContactPersonHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact_person_history (id INT AUTO_INCREMENT NOT NULL, owner_entity_id INT DEFAULT NULL, author INT DEFAULT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) DEFAULT NULL, title VARCHAR(100) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, phone VARCHAR(50) DEFAULT NULL, ext VARCHAR(10) DEFAULT NULL, cell VARCHAR(50) DEFAULT NULL, fax VARCHAR(50) DEFAULT NULL, cod TINYINT(1) DEFAULT \'0\', notes VARCHAR(150) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_76D94D02835E0EEE (owner_entity_id), INDEX IDX_76D94D02BDAFD8C8 (author), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact_person_history ADD CONSTRAINT FK_76D94D02835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES contact_person (id)');
        $this->addSql('ALTER TABLE contact_person_history ADD CONSTRAINT FK_76D94D02BDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contact_person_history');
    }
}
