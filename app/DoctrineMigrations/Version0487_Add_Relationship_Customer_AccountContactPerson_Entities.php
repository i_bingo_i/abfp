<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0487_Add_Relationship_Customer_AccountContactPerson_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person ADD customer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person ADD CONSTRAINT FK_F6B922FD9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_F6B922FD9395C3F3 ON account_contact_person (customer_id)');
        $this->addSql('ALTER TABLE account_contact_person DROP FOREIGN KEY FK_F6B922FD79D0C0E4');
        $this->addSql('DROP INDEX IDX_F6B922FD79D0C0E4 ON account_contact_person');
        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F5879D0C0E4');
        $this->addSql('DROP INDEX IDX_950F5F5879D0C0E4 ON account_contact_person_history');
        $this->addSql('ALTER TABLE account_contact_person_history DROP billing_address_id');
        $this->addSql('ALTER TABLE account_contact_person DROP billing_address_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person DROP FOREIGN KEY FK_F6B922FD9395C3F3');
        $this->addSql('DROP INDEX IDX_F6B922FD9395C3F3 ON account_contact_person');
        $this->addSql('ALTER TABLE account_contact_person CHANGE customer_id billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person ADD CONSTRAINT FK_F6B922FD79D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_F6B922FD79D0C0E4 ON account_contact_person (billing_address_id)');
        $this->addSql('ALTER TABLE account_contact_person_history ADD billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F5879D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address_history (id)');
        $this->addSql('CREATE INDEX IDX_950F5F5879D0C0E4 ON account_contact_person_history (billing_address_id)');
    }
}
