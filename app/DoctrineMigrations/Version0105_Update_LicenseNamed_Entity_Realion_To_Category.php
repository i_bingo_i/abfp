<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0105_Update_LicenseNamed_Entity_Realion_To_Category extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE licenses_category_relations (licensed_named_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_4FAEBA5D43EC7B4F (licensed_named_id), INDEX IDX_4FAEBA5D12469DE2 (category_id), PRIMARY KEY(licensed_named_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE licenses_category_relations ADD CONSTRAINT FK_4FAEBA5D43EC7B4F FOREIGN KEY (licensed_named_id) REFERENCES licenses_named (id)');
        $this->addSql('ALTER TABLE licenses_category_relations ADD CONSTRAINT FK_4FAEBA5D12469DE2 FOREIGN KEY (category_id) REFERENCES licenses_category (id)');
        $this->addSql('ALTER TABLE licenses_named DROP FOREIGN KEY FK_892B1DF512469DE2');
        $this->addSql('DROP INDEX IDX_892B1DF512469DE2 ON licenses_named');
        $this->addSql('ALTER TABLE licenses_named DROP category_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE licenses_category_relations');
        $this->addSql('ALTER TABLE licenses_named ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE licenses_named ADD CONSTRAINT FK_892B1DF512469DE2 FOREIGN KEY (category_id) REFERENCES licenses_category (id)');
        $this->addSql('CREATE INDEX IDX_892B1DF512469DE2 ON licenses_named (category_id)');
    }
}
