<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0320_Add_PaymentTerm_Field_To_Workorder_Entity_To_Relate_It_To_PaymentTerm_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder ADD payment_term_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BB17653B16 FOREIGN KEY (payment_term_id) REFERENCES payment_term (id)');
        $this->addSql('CREATE INDEX IDX_51CF52BB17653B16 ON workorder (payment_term_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder DROP FOREIGN KEY FK_51CF52BB17653B16');
        $this->addSql('DROP INDEX IDX_51CF52BB17653B16 ON workorder');
        $this->addSql('ALTER TABLE workorder DROP payment_term_id');
    }
}
