<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0158_Add_Oldest_Opp_Divicion_Fields_Account_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account ADD oldest_opportunity_backflow_date DATE DEFAULT NULL, ADD oldest_opportunity_fire_date DATE DEFAULT NULL, ADD oldest_opportunity_plumbing_date DATE DEFAULT NULL, ADD oldest_opportunity_alarm_date DATE DEFAULT NULL, CHANGE oldest_opportunity_date oldest_opportunity_date DATE DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account DROP oldest_opportunity_backflow_date, DROP oldest_opportunity_fire_date, DROP oldest_opportunity_plumbing_date, DROP oldest_opportunity_alarm_date, CHANGE oldest_opportunity_date oldest_opportunity_date DATETIME DEFAULT NULL');
    }
}
