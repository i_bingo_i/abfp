<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0534_Insert_EntityTypes_For_Customer_Entity extends AbstractMigration
{

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("INSERT INTO entity_type (`name`, `alias`) VALUES ('Contact Person', 'contact_person')");
        $this->addSql("INSERT INTO entity_type (`name`, `alias`) VALUES ('Company', 'company')");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM entity_type WHERE alias = 'contact_person'");
        $this->addSql("DELETE FROM entity_type WHERE alias = 'company'");
    }
}