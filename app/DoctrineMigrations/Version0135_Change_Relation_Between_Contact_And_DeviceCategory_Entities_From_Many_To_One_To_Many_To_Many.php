<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0135_Change_Relation_Between_Contact_And_DeviceCategory_Entities_From_Many_To_One_To_Many_To_Many extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contacts_divisions (contact_id INT NOT NULL, division_id INT NOT NULL, INDEX IDX_767E9ABE7A1254A (contact_id), INDEX IDX_767E9AB41859289 (division_id), PRIMARY KEY(contact_id, division_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contacts_divisions ADD CONSTRAINT FK_767E9ABE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id)');
        $this->addSql('ALTER TABLE contacts_divisions ADD CONSTRAINT FK_767E9AB41859289 FOREIGN KEY (division_id) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E63841859289');
        $this->addSql('DROP INDEX IDX_4C62E63841859289 ON contact');
        $this->addSql('ALTER TABLE contact DROP division_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contacts_divisions');
        $this->addSql('ALTER TABLE contact ADD division_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E63841859289 FOREIGN KEY (division_id) REFERENCES device_category (id)');
        $this->addSql('CREATE INDEX IDX_4C62E63841859289 ON contact (division_id)');
    }
}
