<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0353_Add_Relationship_Between_Proposal_And_NotIncluded_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE notIncludedItems_proposals (proposal_id INT NOT NULL, not_included_id INT NOT NULL, INDEX IDX_1C5646E9F4792058 (proposal_id), INDEX IDX_1C5646E9C1CB14A1 (not_included_id), PRIMARY KEY(proposal_id, not_included_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE notIncludedItems_proposals ADD CONSTRAINT FK_1C5646E9F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notIncludedItems_proposals ADD CONSTRAINT FK_1C5646E9C1CB14A1 FOREIGN KEY (not_included_id) REFERENCES not_included (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE notIncludedItems_proposals');
    }
}
