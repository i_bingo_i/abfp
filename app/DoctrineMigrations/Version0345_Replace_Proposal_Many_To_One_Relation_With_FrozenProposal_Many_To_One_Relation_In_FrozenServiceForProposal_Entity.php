<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0345_Replace_Proposal_Many_To_One_Relation_With_FrozenProposal_Many_To_One_Relation_In_FrozenServiceForProposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE frozen_service_for_proposal DROP FOREIGN KEY FK_C57D5467F4792058');
        $this->addSql('DROP INDEX IDX_C57D5467F4792058 ON frozen_service_for_proposal');
        $this->addSql('ALTER TABLE frozen_service_for_proposal CHANGE proposal_id frozen_proposal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE frozen_service_for_proposal ADD CONSTRAINT FK_C57D5467C5A6CB48 FOREIGN KEY (frozen_proposal_id) REFERENCES frozen_proposal (id)');
        $this->addSql('CREATE INDEX IDX_C57D5467C5A6CB48 ON frozen_service_for_proposal (frozen_proposal_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE frozen_service_for_proposal DROP FOREIGN KEY FK_C57D5467C5A6CB48');
        $this->addSql('DROP INDEX IDX_C57D5467C5A6CB48 ON frozen_service_for_proposal');
        $this->addSql('ALTER TABLE frozen_service_for_proposal CHANGE frozen_proposal_id proposal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE frozen_service_for_proposal ADD CONSTRAINT FK_C57D5467F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('CREATE INDEX IDX_C57D5467F4792058 ON frozen_service_for_proposal (proposal_id)');
    }
}
