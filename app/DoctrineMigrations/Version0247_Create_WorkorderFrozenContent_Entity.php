<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0247_Create_WorkorderFrozenContent_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE workorder_frozen_content (id INT AUTO_INCREMENT NOT NULL, account_history_id INT DEFAULT NULL, device_history_id INT DEFAULT NULL, service_history_id INT DEFAULT NULL, workorder_id INT DEFAULT NULL, municipality_fee VARCHAR(10) DEFAULT NULL, processing_fee VARCHAR(10) DEFAULT NULL, date_create DATETIME NOT NULL, INDEX IDX_AE06C4E7E29E76F6 (account_history_id), INDEX IDX_AE06C4E79936C794 (device_history_id), INDEX IDX_AE06C4E78854BDE6 (service_history_id), INDEX IDX_AE06C4E72C1C3467 (workorder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workorder_frozen_content ADD CONSTRAINT FK_AE06C4E7E29E76F6 FOREIGN KEY (account_history_id) REFERENCES account_history (id)');
        $this->addSql('ALTER TABLE workorder_frozen_content ADD CONSTRAINT FK_AE06C4E79936C794 FOREIGN KEY (device_history_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE workorder_frozen_content ADD CONSTRAINT FK_AE06C4E78854BDE6 FOREIGN KEY (service_history_id) REFERENCES service_history (id)');
        $this->addSql('ALTER TABLE workorder_frozen_content ADD CONSTRAINT FK_AE06C4E72C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
        $this->addSql('DROP TABLE frozen_service_for_workorder');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE frozen_service_for_workorder (id INT AUTO_INCREMENT NOT NULL, workorder_id INT DEFAULT NULL, service_history_id INT DEFAULT NULL, service_id INT DEFAULT NULL, date_create DATETIME NOT NULL, INDEX IDX_3C682E908854BDE6 (service_history_id), INDEX IDX_3C682E90ED5CA9E6 (service_id), INDEX IDX_3C682E902C1C3467 (workorder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE frozen_service_for_workorder ADD CONSTRAINT FK_3C682E902C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
        $this->addSql('ALTER TABLE frozen_service_for_workorder ADD CONSTRAINT FK_3C682E908854BDE6 FOREIGN KEY (service_history_id) REFERENCES service_history (id)');
        $this->addSql('ALTER TABLE frozen_service_for_workorder ADD CONSTRAINT FK_3C682E90ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id)');
        $this->addSql('DROP TABLE workorder_frozen_content');
    }
}
