<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0230_Create_FrozenServiceForProposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE frozen_service_for_proposal (id INT AUTO_INCREMENT NOT NULL, service_history_id INT DEFAULT NULL, service_id INT DEFAULT NULL, proposal_id INT DEFAULT NULL, INDEX IDX_C57D54678854BDE6 (service_history_id), INDEX IDX_C57D5467ED5CA9E6 (service_id), INDEX IDX_C57D5467F4792058 (proposal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE frozen_service_for_proposal ADD CONSTRAINT FK_C57D54678854BDE6 FOREIGN KEY (service_history_id) REFERENCES service_history (id)');
        $this->addSql('ALTER TABLE frozen_service_for_proposal ADD CONSTRAINT FK_C57D5467ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id)');
        $this->addSql('ALTER TABLE frozen_service_for_proposal ADD CONSTRAINT FK_C57D5467F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE frozen_service_for_proposal');
    }
}
