<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0148_Add_Relationship_Opportunity_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service ADD opportunity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD28389C3D7 FOREIGN KEY (opportunity) REFERENCES opportunity (id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD28389C3D7 ON service (opportunity)');
        $this->addSql('ALTER TABLE opportunity ADD division INT DEFAULT NULL, ADD type INT DEFAULT NULL, ADD status INT DEFAULT NULL');
        $this->addSql('ALTER TABLE opportunity ADD CONSTRAINT FK_8389C3D710174714 FOREIGN KEY (division) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE opportunity ADD CONSTRAINT FK_8389C3D78CDE5729 FOREIGN KEY (type) REFERENCES opportunity_type (id)');
        $this->addSql('ALTER TABLE opportunity ADD CONSTRAINT FK_8389C3D77B00651C FOREIGN KEY (status) REFERENCES opportunity_status (id)');
        $this->addSql('CREATE INDEX IDX_8389C3D710174714 ON opportunity (division)');
        $this->addSql('CREATE INDEX IDX_8389C3D78CDE5729 ON opportunity (type)');
        $this->addSql('CREATE INDEX IDX_8389C3D77B00651C ON opportunity (status)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE opportunity DROP FOREIGN KEY FK_8389C3D710174714');
        $this->addSql('ALTER TABLE opportunity DROP FOREIGN KEY FK_8389C3D78CDE5729');
        $this->addSql('ALTER TABLE opportunity DROP FOREIGN KEY FK_8389C3D77B00651C');
        $this->addSql('DROP INDEX IDX_8389C3D710174714 ON opportunity');
        $this->addSql('DROP INDEX IDX_8389C3D78CDE5729 ON opportunity');
        $this->addSql('DROP INDEX IDX_8389C3D77B00651C ON opportunity');
        $this->addSql('ALTER TABLE opportunity DROP division, DROP type, DROP status');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD28389C3D7');
        $this->addSql('DROP INDEX IDX_E19D9AD28389C3D7 ON service');
        $this->addSql('ALTER TABLE service DROP opportunity');
    }
}
