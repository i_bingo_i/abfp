<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0396_Add_CreatedBy_Field_To_Workorder_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder ADD created_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BBB03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_51CF52BBB03A8386 ON workorder (created_by_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder DROP FOREIGN KEY FK_51CF52BBB03A8386');
        $this->addSql('DROP INDEX IDX_51CF52BBB03A8386 ON workorder');
        $this->addSql('ALTER TABLE workorder DROP created_by_id');
    }
}
