<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0466_Add_Text_Descriptions_To_PaymentTerms extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE payment_term SET description="Any 30 day past due amount will be charged 10 percent interest per month. Customer is responsible for all legal fees in the event this is not paid after 90 days and goes to collection. All hearings to be held in Lake County Illinois." WHERE alias IN 
                      ("net_fifteen_days", "net_thirty_days", "cod", "fifty_percent_deposit")');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE payment_term SET description=""');
    }
}
