<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0078_Refactor_Account_Entity_Add_Address_Relation extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A45D83CC1');
        $this->addSql('DROP INDEX IDX_7D3656A45D83CC1 ON account');
        $this->addSql('ALTER TABLE account DROP address, DROP city, DROP zip, CHANGE state_id address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A4F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D3656A4F5B7AF75 ON account (address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A4F5B7AF75');
        $this->addSql('DROP INDEX UNIQ_7D3656A4F5B7AF75 ON account');
        $this->addSql('ALTER TABLE account ADD address VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD city VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, ADD zip VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, CHANGE address_id state_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A45D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('CREATE INDEX IDX_7D3656A45D83CC1 ON account (state_id)');
    }
}
