<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0118_Create_DepartmentChannel_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE department_channel (id INT AUTO_INCREMENT NOT NULL, named_id INT DEFAULT NULL, state_id INT DEFAULT NULL, contractor_user_id INT DEFAULT NULL, upload_fee VARCHAR(50) DEFAULT NULL, processing_fee VARCHAR(50) DEFAULT NULL, `default` TINYINT(1) DEFAULT \'0\', deleted TINYINT(1) DEFAULT \'0\', date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_1F5066A4457EB27E (named_id), INDEX IDX_1F5066A45D83CC1 (state_id), INDEX IDX_1F5066A4CB3CC8D2 (contractor_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A4457EB27E FOREIGN KEY (named_id) REFERENCES licenses_named (id)');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A45D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A4CB3CC8D2 FOREIGN KEY (contractor_user_id) REFERENCES contractor_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE department_channel');
    }
}
