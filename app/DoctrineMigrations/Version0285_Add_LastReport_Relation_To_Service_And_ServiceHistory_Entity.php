<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0285_Add_LastReport_Relation_To_Service_And_ServiceHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service ADD last_report_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD25C1D7BC6 FOREIGN KEY (last_report_id) REFERENCES report (id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD25C1D7BC6 ON service (last_report_id)');
        $this->addSql('ALTER TABLE service_history ADD last_report_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D75C1D7BC6 FOREIGN KEY (last_report_id) REFERENCES report (id)');
        $this->addSql('CREATE INDEX IDX_E83E22D75C1D7BC6 ON service_history (last_report_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD25C1D7BC6');
        $this->addSql('DROP INDEX IDX_E19D9AD25C1D7BC6 ON service');
        $this->addSql('ALTER TABLE service DROP last_report_id');
        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D75C1D7BC6');
        $this->addSql('DROP INDEX IDX_E83E22D75C1D7BC6 ON service_history');
        $this->addSql('ALTER TABLE service_history DROP last_report_id');
    }
}
