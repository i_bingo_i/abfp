<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0101_Create_LicensesDynamicField_Entity_And_Relate extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE licenses_dynamic_field (id INT AUTO_INCREMENT NOT NULL, license_id INT DEFAULT NULL, type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(50) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_1C2977D4460F904B (license_id), INDEX IDX_1C2977D4C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE licenses_dynamic_field ADD CONSTRAINT FK_1C2977D4460F904B FOREIGN KEY (license_id) REFERENCES licenses_named (id)');
        $this->addSql('ALTER TABLE licenses_dynamic_field ADD CONSTRAINT FK_1C2977D4C54C8C93 FOREIGN KEY (type_id) REFERENCES dynamic_field_type (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE licenses_dynamic_field');
    }
}
