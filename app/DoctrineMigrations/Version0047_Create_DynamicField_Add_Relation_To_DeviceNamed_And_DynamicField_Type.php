<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0047_Create_DynamicField_Add_Relation_To_DeviceNamed_And_DynamicField_Type extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dynamic_field (id INT AUTO_INCREMENT NOT NULL, device_id INT DEFAULT NULL, type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(100) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_FC831C2394A4C7D4 (device_id), INDEX IDX_FC831C23C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dynamic_field ADD CONSTRAINT FK_FC831C2394A4C7D4 FOREIGN KEY (device_id) REFERENCES device_named (id)');
        $this->addSql('ALTER TABLE dynamic_field ADD CONSTRAINT FK_FC831C23C54C8C93 FOREIGN KEY (type_id) REFERENCES dynamic_field_type (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE dynamic_field');
    }
}
