<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0208_Align_Service_And_ServiceHistory_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service_history ADD opportunity INT DEFAULT NULL, ADD department_channel INT DEFAULT NULL, ADD status_id INT DEFAULT NULL, ADD test_date DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D78389C3D7 FOREIGN KEY (opportunity) REFERENCES opportunity (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D71F5066A4 FOREIGN KEY (department_channel) REFERENCES department_channel (id)');
        $this->addSql('ALTER TABLE service_history ADD CONSTRAINT FK_E83E22D76BF700BD FOREIGN KEY (status_id) REFERENCES service_status (id)');
        $this->addSql('CREATE INDEX IDX_E83E22D78389C3D7 ON service_history (opportunity)');
        $this->addSql('CREATE INDEX IDX_E83E22D71F5066A4 ON service_history (department_channel)');
        $this->addSql('CREATE INDEX IDX_E83E22D76BF700BD ON service_history (status_id)');
        $this->addSql('ALTER TABLE service ADD proposal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('CREATE INDEX IDX_E19D9AD2F4792058 ON service (proposal_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2F4792058');
        $this->addSql('DROP INDEX IDX_E19D9AD2F4792058 ON service');
        $this->addSql('ALTER TABLE service DROP proposal_id');
        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D78389C3D7');
        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D71F5066A4');
        $this->addSql('ALTER TABLE service_history DROP FOREIGN KEY FK_E83E22D76BF700BD');
        $this->addSql('DROP INDEX IDX_E83E22D78389C3D7 ON service_history');
        $this->addSql('DROP INDEX IDX_E83E22D71F5066A4 ON service_history');
        $this->addSql('DROP INDEX IDX_E83E22D76BF700BD ON service_history');
        $this->addSql('ALTER TABLE service_history DROP opportunity, DROP department_channel, DROP status_id, DROP test_date');
    }
}
