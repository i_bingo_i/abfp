<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0336_Create_ProposalLogsActions_Table extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proposal_logs_actions (proposal_log_id INT NOT NULL, proposal_action_id INT NOT NULL, INDEX IDX_BD2367AE4B713931 (proposal_log_id), INDEX IDX_BD2367AEA86603F3 (proposal_action_id), PRIMARY KEY(proposal_log_id, proposal_action_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proposal_logs_actions ADD CONSTRAINT FK_BD2367AE4B713931 FOREIGN KEY (proposal_log_id) REFERENCES proposal_log (id)');
        $this->addSql('ALTER TABLE proposal_logs_actions ADD CONSTRAINT FK_BD2367AEA86603F3 FOREIGN KEY (proposal_action_id) REFERENCES proposal_log_action (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE proposal_logs_actions');
    }
}
