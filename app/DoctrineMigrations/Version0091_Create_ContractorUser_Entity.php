<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0091_Create_ContractorUser_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contractor_user (id INT AUTO_INCREMENT NOT NULL, address_id INT DEFAULT NULL, user INT DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, signature VARCHAR(255) DEFAULT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, title VARCHAR(100) DEFAULT NULL, personal_email VARCHAR(100) DEFAULT NULL, personal_phone VARCHAR(15) DEFAULT NULL, work_phone VARCHAR(15) DEFAULT NULL, fax VARCHAR(15) DEFAULT NULL, UNIQUE INDEX UNIQ_759F1C39F5B7AF75 (address_id), INDEX IDX_759F1C398D93D649 (user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contractor_user ADD CONSTRAINT FK_759F1C39F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE contractor_user ADD CONSTRAINT FK_759F1C398D93D649 FOREIGN KEY (user) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contractor_user');
    }
}
