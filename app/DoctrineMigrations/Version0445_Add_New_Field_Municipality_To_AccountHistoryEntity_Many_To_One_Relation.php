<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0445_Add_New_Field_Municipality_To_AccountHistoryEntity_Many_To_One_Relation extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_history ADD municipality INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE916403C6F56628 FOREIGN KEY (municipality) REFERENCES municipality (id)');
        $this->addSql('CREATE INDEX IDX_EE916403C6F56628 ON account_history (municipality)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_history DROP FOREIGN KEY FK_EE916403C6F56628');
        $this->addSql('DROP INDEX IDX_EE916403C6F56628 ON account_history');
        $this->addSql('ALTER TABLE account_history DROP municipality');
    }
}
