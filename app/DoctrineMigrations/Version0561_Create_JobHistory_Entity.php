<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0561_Create_JobHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE job_history (id INT AUTO_INCREMENT NOT NULL, owner_entity_id INT DEFAULT NULL, accounting_system_id VARCHAR(255) DEFAULT NULL, edit_sequence VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, date_update DATETIME NOT NULL, INDEX IDX_BD55C7D0835E0EEE (owner_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE job_history ADD CONSTRAINT FK_BD55C7D0835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES job (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE job_history');
    }
}
