<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0357_Changing_Relation_Between_Workorder_And_Proposal_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal ADD workorder_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE594722C1C3467 FOREIGN KEY (workorder_id) REFERENCES workorder (id)');
        $this->addSql('CREATE INDEX IDX_BFE594722C1C3467 ON proposal (workorder_id)');
        $this->addSql('ALTER TABLE workorder DROP FOREIGN KEY FK_51CF52BBF4792058');
        $this->addSql('DROP INDEX IDX_51CF52BBF4792058 ON workorder');
        $this->addSql('ALTER TABLE workorder DROP proposal_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE594722C1C3467');
        $this->addSql('DROP INDEX IDX_BFE594722C1C3467 ON proposal');
        $this->addSql('ALTER TABLE proposal DROP workorder_id');
        $this->addSql('ALTER TABLE workorder ADD proposal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BBF4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('CREATE INDEX IDX_51CF52BBF4792058 ON workorder (proposal_id)');
    }
}
