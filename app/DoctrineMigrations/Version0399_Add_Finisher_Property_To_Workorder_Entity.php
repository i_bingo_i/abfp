<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0399_Add_Finisher_Property_To_Workorder_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder ADD finisher_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BB95D2E802 FOREIGN KEY (finisher_id) REFERENCES contractor_user (id)');
        $this->addSql('CREATE INDEX IDX_51CF52BB95D2E802 ON workorder (finisher_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder DROP FOREIGN KEY FK_51CF52BB95D2E802');
        $this->addSql('DROP INDEX IDX_51CF52BB95D2E802 ON workorder');
        $this->addSql('ALTER TABLE workorder DROP finisher_id');
    }
}
