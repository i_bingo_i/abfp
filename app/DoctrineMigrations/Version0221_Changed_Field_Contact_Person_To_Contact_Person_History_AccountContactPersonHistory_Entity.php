<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0221_Changed_Field_Contact_Person_To_Contact_Person_History_AccountContactPersonHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F58A44EE6F7');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58A44EE6F7 FOREIGN KEY (contact_person) REFERENCES contact_person_history (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F58A44EE6F7');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58A44EE6F7 FOREIGN KEY (contact_person) REFERENCES contact_person (id)');
    }
}
