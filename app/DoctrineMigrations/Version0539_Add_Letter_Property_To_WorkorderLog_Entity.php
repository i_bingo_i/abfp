<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0539_Add_Letter_Property_To_WorkorderLog_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log ADD letter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder_log ADD CONSTRAINT FK_3711E8994525FF26 FOREIGN KEY (letter_id) REFERENCES letter (id)');
        $this->addSql('CREATE INDEX IDX_3711E8994525FF26 ON workorder_log (letter_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log DROP FOREIGN KEY FK_3711E8994525FF26');
        $this->addSql('DROP INDEX IDX_3711E8994525FF26 ON workorder_log');
        $this->addSql('ALTER TABLE workorder_log DROP letter_id');
    }
}
