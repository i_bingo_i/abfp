<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0010_Add_AccountHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE account_history (id INT AUTO_INCREMENT NOT NULL, type INT DEFAULT NULL, state_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, client_type_id INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, author_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, city VARCHAR(50) NOT NULL, zip VARCHAR(50) NOT NULL, notes LONGTEXT DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, date_save DATETIME NOT NULL, INDEX IDX_EE9164038CDE5729 (type), INDEX IDX_EE9164035D83CC1 (state_id), INDEX IDX_EE916403727ACA70 (parent_id), INDEX IDX_EE9164039771C8EE (client_type_id), INDEX IDX_EE916403835E0EEE (owner_entity_id), INDEX IDX_EE916403F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE9164038CDE5729 FOREIGN KEY (type) REFERENCES account_type (id)');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE9164035D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE916403727ACA70 FOREIGN KEY (parent_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE9164039771C8EE FOREIGN KEY (client_type_id) REFERENCES client_type (id)');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE916403835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE916403F675F31B FOREIGN KEY (author_id) REFERENCES fos_user (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE account_history');
    }
}
