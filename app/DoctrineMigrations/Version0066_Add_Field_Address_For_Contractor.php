<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0066_Add_Field_Address_For_Contractor extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor ADD address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contractor ADD CONSTRAINT FK_437BD2EFF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_437BD2EFF5B7AF75 ON contractor (address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor DROP FOREIGN KEY FK_437BD2EFF5B7AF75');
        $this->addSql('DROP INDEX UNIQ_437BD2EFF5B7AF75 ON contractor');
        $this->addSql('ALTER TABLE contractor DROP address_id');
    }
}
