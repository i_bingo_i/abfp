<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0054_Create_DeviceHistory_Entity_And_Relate_With_DynamicFieldHistory extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE device_history (id INT AUTO_INCREMENT NOT NULL, named_id INT DEFAULT NULL, status_id INT DEFAULT NULL, account_id INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, author INT DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, comments LONGTEXT DEFAULT NULL, note_to_tester LONGTEXT DEFAULT NULL, deleted TINYINT(1) DEFAULT \'0\', date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_AD0CA9D6457EB27E (named_id), INDEX IDX_AD0CA9D66BF700BD (status_id), INDEX IDX_AD0CA9D69B6B5FBA (account_id), INDEX IDX_AD0CA9D6835E0EEE (owner_entity_id), INDEX IDX_AD0CA9D6BDAFD8C8 (author), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE device_history ADD CONSTRAINT FK_AD0CA9D6457EB27E FOREIGN KEY (named_id) REFERENCES device_named (id)');
        $this->addSql('ALTER TABLE device_history ADD CONSTRAINT FK_AD0CA9D66BF700BD FOREIGN KEY (status_id) REFERENCES device_status (id)');
        $this->addSql('ALTER TABLE device_history ADD CONSTRAINT FK_AD0CA9D69B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE device_history ADD CONSTRAINT FK_AD0CA9D6835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE device_history ADD CONSTRAINT FK_AD0CA9D6BDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE dynamic_field_value_history ADD device_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dynamic_field_value_history ADD CONSTRAINT FK_68704F9C94A4C7D4 FOREIGN KEY (device_id) REFERENCES device_history (id)');
        $this->addSql('CREATE INDEX IDX_68704F9C94A4C7D4 ON dynamic_field_value_history (device_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dynamic_field_value_history DROP FOREIGN KEY FK_68704F9C94A4C7D4');
        $this->addSql('DROP TABLE device_history');
        $this->addSql('DROP INDEX IDX_68704F9C94A4C7D4 ON dynamic_field_value_history');
        $this->addSql('ALTER TABLE dynamic_field_value_history DROP device_id');
    }
}
