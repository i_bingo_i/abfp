<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0159_Add_Some_Fields_Proposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal ADD reference_id VARCHAR(255) NOT NULL, ADD services_count INT NOT NULL, ADD devices_count INT NOT NULL, ADD sites_count INT NOT NULL, ADD earliest_due_date DATE DEFAULT NULL, ADD service_total_fee DOUBLE PRECISION DEFAULT NULL, CHANGE date_of_proposal date_of_proposal DATE DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal DROP reference_id, DROP services_count, DROP devices_count, DROP sites_count, DROP earliest_due_date, DROP service_total_fee, CHANGE date_of_proposal date_of_proposal DATETIME NOT NULL');
    }
}
