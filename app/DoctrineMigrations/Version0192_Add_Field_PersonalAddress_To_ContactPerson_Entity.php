<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0192_Add_Field_PersonalAddress_To_ContactPerson_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_person ADD personal_address INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact_person ADD CONSTRAINT FK_A44EE6F78E6966AA FOREIGN KEY (personal_address) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_A44EE6F78E6966AA ON contact_person (personal_address)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_person DROP FOREIGN KEY FK_A44EE6F78E6966AA');
        $this->addSql('DROP INDEX IDX_A44EE6F78E6966AA ON contact_person');
        $this->addSql('ALTER TABLE contact_person DROP personal_address');
    }
}
