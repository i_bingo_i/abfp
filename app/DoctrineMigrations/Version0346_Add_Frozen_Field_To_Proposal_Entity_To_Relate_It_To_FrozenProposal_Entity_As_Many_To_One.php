<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0346_Add_Frozen_Field_To_Proposal_Entity_To_Relate_It_To_FrozenProposal_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal ADD frozen_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal ADD CONSTRAINT FK_BFE59472E3690CE9 FOREIGN KEY (frozen_id) REFERENCES frozen_proposal (id)');
        $this->addSql('CREATE INDEX IDX_BFE59472E3690CE9 ON proposal (frozen_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal DROP FOREIGN KEY FK_BFE59472E3690CE9');
        $this->addSql('DROP INDEX IDX_BFE59472E3690CE9 ON proposal');
        $this->addSql('ALTER TABLE proposal DROP frozen_id');
    }
}
