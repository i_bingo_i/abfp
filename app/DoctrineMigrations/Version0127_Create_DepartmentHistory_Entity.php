<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0127_Create_DepartmentHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE department_history (id INT AUTO_INCREMENT NOT NULL, municipality_id INT DEFAULT NULL, division_id INT DEFAULT NULL, agent_id INT DEFAULT NULL, author INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, date_save DATETIME NOT NULL, INDEX IDX_9A1398A8AE6F181C (municipality_id), INDEX IDX_9A1398A841859289 (division_id), INDEX IDX_9A1398A83414710B (agent_id), INDEX IDX_9A1398A8BDAFD8C8 (author), INDEX IDX_9A1398A8835E0EEE (owner_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE department_history ADD CONSTRAINT FK_9A1398A8AE6F181C FOREIGN KEY (municipality_id) REFERENCES municipality (id)');
        $this->addSql('ALTER TABLE department_history ADD CONSTRAINT FK_9A1398A841859289 FOREIGN KEY (division_id) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE department_history ADD CONSTRAINT FK_9A1398A83414710B FOREIGN KEY (agent_id) REFERENCES agent (id)');
        $this->addSql('ALTER TABLE department_history ADD CONSTRAINT FK_9A1398A8BDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE department_history ADD CONSTRAINT FK_9A1398A8835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES department (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE department_history');
    }
}
