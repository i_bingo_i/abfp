<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0383_Create_WorkorderType_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE workorder_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, alias VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workorder ADD type_id INT DEFAULT NULL, ADD pdf VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder ADD CONSTRAINT FK_51CF52BBC54C8C93 FOREIGN KEY (type_id) REFERENCES workorder_type (id)');
        $this->addSql('CREATE INDEX IDX_51CF52BBC54C8C93 ON workorder (type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder DROP FOREIGN KEY FK_51CF52BBC54C8C93');
        $this->addSql('DROP TABLE workorder_type');
        $this->addSql('DROP INDEX IDX_51CF52BBC54C8C93 ON workorder');
        $this->addSql('ALTER TABLE workorder DROP type_id, DROP pdf');
    }
}
