<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0476_Create_InvoiceLine_Modify_Invoice_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE invoice_line (id INT AUTO_INCREMENT NOT NULL, item_id INT DEFAULT NULL, invoice_id INT DEFAULT NULL, accounting_system_id VARCHAR(50) DEFAULT NULL, description TEXT DEFAULT NULL, quantity INT DEFAULT NULL, rate NUMERIC(10, 0) DEFAULT NULL, amount NUMERIC(10, 0) DEFAULT NULL, serial_number INT DEFAULT NULL, INDEX IDX_D3D1D693126F525E (item_id), INDEX IDX_D3D1D6932989F1FD (invoice_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE invoice_line ADD CONSTRAINT FK_D3D1D693126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE invoice_line ADD CONSTRAINT FK_D3D1D6932989F1FD FOREIGN KEY (invoice_id) REFERENCES invoices (id)');
        $this->addSql('ALTER TABLE invoices ADD acp_id INT DEFAULT NULL, ADD payment_term_id INT DEFAULT NULL, ADD user_id INT DEFAULT NULL, ADD bill_to_address_id INT DEFAULT NULL, ADD ship_to_address_id INT DEFAULT NULL, ADD edit_sequence INT DEFAULT NULL, ADD total NUMERIC(10, 0) DEFAULT NULL, ADD payment_applied NUMERIC(10, 0) DEFAULT NULL, ADD balance_due NUMERIC(10, 0) DEFAULT NULL, ADD override_payment NUMERIC(10, 0) DEFAULT NULL, ADD accounting_system_id VARCHAR(50) DEFAULT NULL, ADD memo VARCHAR(255) DEFAULT NULL, ADD save DATETIME DEFAULT NULL, ADD refresh DATETIME DEFAULT NULL, CHANGE file file VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE invoices ADD CONSTRAINT FK_6A2F2F955EC7CC7F FOREIGN KEY (acp_id) REFERENCES account_contact_person (id)');
        $this->addSql('ALTER TABLE invoices ADD CONSTRAINT FK_6A2F2F9517653B16 FOREIGN KEY (payment_term_id) REFERENCES payment_term (id)');
        $this->addSql('ALTER TABLE invoices ADD CONSTRAINT FK_6A2F2F95A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE invoices ADD CONSTRAINT FK_6A2F2F95A29079E4 FOREIGN KEY (bill_to_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE invoices ADD CONSTRAINT FK_6A2F2F959F09B3E3 FOREIGN KEY (ship_to_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_6A2F2F955EC7CC7F ON invoices (acp_id)');
        $this->addSql('CREATE INDEX IDX_6A2F2F9517653B16 ON invoices (payment_term_id)');
        $this->addSql('CREATE INDEX IDX_6A2F2F95A76ED395 ON invoices (user_id)');
        $this->addSql('CREATE INDEX IDX_6A2F2F95A29079E4 ON invoices (bill_to_address_id)');
        $this->addSql('CREATE INDEX IDX_6A2F2F959F09B3E3 ON invoices (ship_to_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE invoice_line');
        $this->addSql('ALTER TABLE invoices DROP FOREIGN KEY FK_6A2F2F955EC7CC7F');
        $this->addSql('ALTER TABLE invoices DROP FOREIGN KEY FK_6A2F2F9517653B16');
        $this->addSql('ALTER TABLE invoices DROP FOREIGN KEY FK_6A2F2F95A76ED395');
        $this->addSql('ALTER TABLE invoices DROP FOREIGN KEY FK_6A2F2F95A29079E4');
        $this->addSql('ALTER TABLE invoices DROP FOREIGN KEY FK_6A2F2F959F09B3E3');
        $this->addSql('DROP INDEX IDX_6A2F2F955EC7CC7F ON invoices');
        $this->addSql('DROP INDEX IDX_6A2F2F9517653B16 ON invoices');
        $this->addSql('DROP INDEX IDX_6A2F2F95A76ED395 ON invoices');
        $this->addSql('DROP INDEX IDX_6A2F2F95A29079E4 ON invoices');
        $this->addSql('DROP INDEX IDX_6A2F2F959F09B3E3 ON invoices');
        $this->addSql('ALTER TABLE invoices DROP acp_id, DROP payment_term_id, DROP user_id, DROP bill_to_address_id, DROP ship_to_address_id, DROP edit_sequence, DROP total, DROP payment_applied, DROP balance_due, DROP override_payment, DROP accounting_system_id, DROP memo, DROP save, DROP refresh, CHANGE file file VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
