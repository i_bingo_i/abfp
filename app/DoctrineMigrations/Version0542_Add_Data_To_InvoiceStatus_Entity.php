<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0542_Add_Data_To_InvoiceStatus_Entity extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO invoice_status (name, alias) VALUES ('Draft', 'draft')");
        $this->addSql("INSERT INTO invoice_status (name, alias) VALUES ('Custom', 'custom')");
        $this->addSql("INSERT INTO invoice_status (name, alias) VALUES ('In QB', 'in_qb')");
        $this->addSql("INSERT INTO invoice_status (name, alias) VALUES ('Voided', 'voided')");
        $this->addSql("INSERT INTO invoice_status (name, alias) VALUES ('Deleted', 'deleted')");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM invoice_status WHERE alias = 'draft'");
        $this->addSql("DELETE FROM invoice_status WHERE alias = 'custom'");
        $this->addSql("DELETE FROM invoice_status WHERE alias = 'in_qb'");
        $this->addSql("DELETE FROM invoice_status WHERE alias = 'voided'");
        $this->addSql("DELETE FROM invoice_status WHERE alias = 'deleted'");
    }
}