<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0218_Create_DepartmentChannelHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE department_channel_history (id INT AUTO_INCREMENT NOT NULL, named_id INT DEFAULT NULL, owner_agent_id INT DEFAULT NULL, fees_basis_id INT DEFAULT NULL, department_id INT DEFAULT NULL, devision_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, author INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, upload_fee VARCHAR(50) DEFAULT NULL, processing_fee VARCHAR(50) DEFAULT NULL, is_default TINYINT(1) DEFAULT \'0\', active TINYINT(1) DEFAULT \'1\', deleted TINYINT(1) DEFAULT \'0\', date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_7BAD093B457EB27E (named_id), INDEX IDX_7BAD093B22F4C8C8 (owner_agent_id), INDEX IDX_7BAD093BC6944BCB (fees_basis_id), INDEX IDX_7BAD093BAE80F5DF (department_id), INDEX IDX_7BAD093B5BB97207 (devision_id), INDEX IDX_7BAD093B7E3C61F9 (owner_id), INDEX IDX_7BAD093BBDAFD8C8 (author), INDEX IDX_7BAD093B835E0EEE (owner_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE department_channel_history ADD CONSTRAINT FK_7BAD093B457EB27E FOREIGN KEY (named_id) REFERENCES channel_named (id)');
        $this->addSql('ALTER TABLE department_channel_history ADD CONSTRAINT FK_7BAD093B22F4C8C8 FOREIGN KEY (owner_agent_id) REFERENCES agent (id)');
        $this->addSql('ALTER TABLE department_channel_history ADD CONSTRAINT FK_7BAD093BC6944BCB FOREIGN KEY (fees_basis_id) REFERENCES fees_basis (id)');
        $this->addSql('ALTER TABLE department_channel_history ADD CONSTRAINT FK_7BAD093BAE80F5DF FOREIGN KEY (department_id) REFERENCES department (id)');
        $this->addSql('ALTER TABLE department_channel_history ADD CONSTRAINT FK_7BAD093B5BB97207 FOREIGN KEY (devision_id) REFERENCES device_category (id)');
        $this->addSql('ALTER TABLE department_channel_history ADD CONSTRAINT FK_7BAD093B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES agent_channel (id)');
        $this->addSql('ALTER TABLE department_channel_history ADD CONSTRAINT FK_7BAD093BBDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE department_channel_history ADD CONSTRAINT FK_7BAD093B835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES department_channel (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE department_channel_history');
    }
}
