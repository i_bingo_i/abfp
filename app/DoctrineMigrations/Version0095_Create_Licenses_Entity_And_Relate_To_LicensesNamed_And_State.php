<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0095_Create_Licenses_Entity_And_Relate_To_LicensesNamed_And_State extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE licenses (id INT AUTO_INCREMENT NOT NULL, named_id INT DEFAULT NULL, state_id INT DEFAULT NULL, code VARCHAR(255) DEFAULT NULL, renewal_date DATETIME DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_7F320F3F457EB27E (named_id), INDEX IDX_7F320F3F5D83CC1 (state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE licenses ADD CONSTRAINT FK_7F320F3F457EB27E FOREIGN KEY (named_id) REFERENCES licenses_named (id)');
        $this->addSql('ALTER TABLE licenses ADD CONSTRAINT FK_7F320F3F5D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE licenses');
    }
}
