<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0117_Create_DepartmentChannelDynamicFieldValue_Entity_And_Relate extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE department_channel_dynamic_field_value (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, option_value_id INT DEFAULT NULL, entity_value_id INT DEFAULT NULL, value VARCHAR(255) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_64EFA3443707B0 (field_id), INDEX IDX_64EFA3D957CA06 (option_value_id), INDEX IDX_64EFA35BD407E2 (entity_value_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE department_channel_dynamic_field_value ADD CONSTRAINT FK_64EFA3443707B0 FOREIGN KEY (field_id) REFERENCES channel_dynamic_field (id)');
        $this->addSql('ALTER TABLE department_channel_dynamic_field_value ADD CONSTRAINT FK_64EFA3D957CA06 FOREIGN KEY (option_value_id) REFERENCES channel_dropbox_choices (id)');
        $this->addSql('ALTER TABLE department_channel_dynamic_field_value ADD CONSTRAINT FK_64EFA35BD407E2 FOREIGN KEY (entity_value_id) REFERENCES address (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE department_channel_dynamic_field_value');
    }
}
