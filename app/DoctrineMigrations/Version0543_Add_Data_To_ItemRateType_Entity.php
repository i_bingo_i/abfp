<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0543_Add_Data_To_ItemRateType_Entity extends AbstractMigration
{

    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO item_rate_type (name, alias) VALUES ('Percent', 'percent')");
        $this->addSql("INSERT INTO item_rate_type (name, alias) VALUES ('Float', 'float')");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM item_rate_type WHERE alias = 'percent'");
        $this->addSql("DELETE FROM item_rate_type WHERE alias = 'float'");
    }
}