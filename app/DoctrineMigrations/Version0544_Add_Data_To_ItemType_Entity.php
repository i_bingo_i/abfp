<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version0544_Add_Data_To_ItemType_Entity extends AbstractMigration
{

    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO item_type (name, alias) VALUES ('Service', 'service')");
        $this->addSql("INSERT INTO item_type (name, alias) VALUES ('Other Charge', 'other_charge')");
        $this->addSql("INSERT INTO item_type (name, alias) VALUES ('Subtotal', 'subtotal')");
        $this->addSql("INSERT INTO item_type (name, alias) VALUES ('Discount', 'discount')");
        $this->addSql("INSERT INTO item_type (name, alias) VALUES ('Payment', 'payment')");
        $this->addSql("INSERT INTO item_type (name, alias) VALUES ('Group', 'group')");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM item_type WHERE alias = 'service'");
        $this->addSql("DELETE FROM item_type WHERE alias = 'other_charge'");
        $this->addSql("DELETE FROM item_type WHERE alias = 'subtotal'");
        $this->addSql("DELETE FROM item_type WHERE alias = 'discount'");
        $this->addSql("DELETE FROM item_type WHERE alias = 'payment'");
        $this->addSql("DELETE FROM item_type WHERE alias = 'group'");
    }
}