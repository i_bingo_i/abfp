<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0516_Create_WorkorderAvailableActions_Table_ManyToMany_WorkorderStatus extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE workorder_available_actions (action_id INT NOT NULL, available_action_id INT NOT NULL, INDEX IDX_37C2323D9D32F035 (action_id), INDEX IDX_37C2323DF6422B32 (available_action_id), PRIMARY KEY(action_id, available_action_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workorder_available_actions ADD CONSTRAINT FK_37C2323D9D32F035 FOREIGN KEY (action_id) REFERENCES workorder_status (id)');
        $this->addSql('ALTER TABLE workorder_available_actions ADD CONSTRAINT FK_37C2323DF6422B32 FOREIGN KEY (available_action_id) REFERENCES workorder_status (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE workorder_available_actions');
    }
}
