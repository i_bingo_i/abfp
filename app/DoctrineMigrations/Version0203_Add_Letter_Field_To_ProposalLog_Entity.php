<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0203_Add_Letter_Field_To_ProposalLog_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_log ADD letter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal_log ADD CONSTRAINT FK_81AFA0B04525FF26 FOREIGN KEY (letter_id) REFERENCES letter (id)');
        $this->addSql('CREATE INDEX IDX_81AFA0B04525FF26 ON proposal_log (letter_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_log DROP FOREIGN KEY FK_81AFA0B04525FF26');
        $this->addSql('DROP INDEX IDX_81AFA0B04525FF26 ON proposal_log');
        $this->addSql('ALTER TABLE proposal_log DROP letter_id');
    }
}
