<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0281_Add_SourceEntity_Parent_To_Company_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device ADD source_entity VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD parent_id INT DEFAULT NULL, ADD source_entity VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F727ACA70 FOREIGN KEY (parent_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_4FBF094F727ACA70 ON company (parent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094F727ACA70');
        $this->addSql('DROP INDEX IDX_4FBF094F727ACA70 ON company');
        $this->addSql('ALTER TABLE company DROP parent_id, DROP source_entity');
        $this->addSql('ALTER TABLE device DROP source_entity');
    }
}
