<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0227_Add_Field_Billing_Address_To_Account_AccountHistory_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account ADD billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A479D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D3656A479D0C0E4 ON account (billing_address_id)');
        $this->addSql('ALTER TABLE account_history ADD billing_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE91640379D0C0E4 FOREIGN KEY (billing_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_EE91640379D0C0E4 ON account_history (billing_address_id)');
        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F58A44EE6F7');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58A44EE6F7 FOREIGN KEY (contact_person) REFERENCES contact_person_history (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A479D0C0E4');
        $this->addSql('DROP INDEX UNIQ_7D3656A479D0C0E4 ON account');
        $this->addSql('ALTER TABLE account DROP billing_address_id');
        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F58A44EE6F7');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58A44EE6F7 FOREIGN KEY (contact_person) REFERENCES contact_person (id)');
        $this->addSql('ALTER TABLE account_history DROP FOREIGN KEY FK_EE91640379D0C0E4');
        $this->addSql('DROP INDEX IDX_EE91640379D0C0E4 ON account_history');
        $this->addSql('ALTER TABLE account_history DROP billing_address_id');
    }
}
