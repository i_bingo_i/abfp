<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0201_Relate_ProcessingStats_And_ProcessingStatsType_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE processing_stats ADD type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE processing_stats ADD CONSTRAINT FK_2935D2C7C54C8C93 FOREIGN KEY (type_id) REFERENCES processing_stats_type (id)');
        $this->addSql('CREATE INDEX IDX_2935D2C7C54C8C93 ON processing_stats (type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE processing_stats DROP FOREIGN KEY FK_2935D2C7C54C8C93');
        $this->addSql('DROP INDEX IDX_2935D2C7C54C8C93 ON processing_stats');
        $this->addSql('ALTER TABLE processing_stats DROP type_id');
    }
}
