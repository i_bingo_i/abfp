<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0344_Create_FrozenProposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE frozen_proposal (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, recipient_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, INDEX IDX_C26948E59B6B5FBA (account_id), INDEX IDX_C26948E5E92F8F78 (recipient_id), INDEX IDX_C26948E561220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE frozen_proposal ADD CONSTRAINT FK_C26948E59B6B5FBA FOREIGN KEY (account_id) REFERENCES account_history (id)');
        $this->addSql('ALTER TABLE frozen_proposal ADD CONSTRAINT FK_C26948E5E92F8F78 FOREIGN KEY (recipient_id) REFERENCES account_contact_person_history (id)');
        $this->addSql('ALTER TABLE frozen_proposal ADD CONSTRAINT FK_C26948E561220EA6 FOREIGN KEY (creator_id) REFERENCES contractor_user_history (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE frozen_proposal');
    }
}
