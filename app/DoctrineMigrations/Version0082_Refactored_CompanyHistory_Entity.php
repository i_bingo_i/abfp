<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0082_Refactored_CompanyHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_history DROP FOREIGN KEY FK_7E86A9C5D83CC1');
        $this->addSql('DROP INDEX IDX_7E86A9C5D83CC1 ON company_history');
        $this->addSql('ALTER TABLE company_history DROP address_line1, DROP city, DROP zip, CHANGE state_id address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company_history ADD CONSTRAINT FK_7E86A9CF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7E86A9CF5B7AF75 ON company_history (address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_history DROP FOREIGN KEY FK_7E86A9CF5B7AF75');
        $this->addSql('DROP INDEX UNIQ_7E86A9CF5B7AF75 ON company_history');
        $this->addSql('ALTER TABLE company_history ADD address_line1 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD city VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci, ADD zip VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE address_id state_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company_history ADD CONSTRAINT FK_7E86A9C5D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('CREATE INDEX IDX_7E86A9C5D83CC1 ON company_history (state_id)');
    }
}
