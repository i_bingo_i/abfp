<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0335_Drop_ProposalAction_Entity_Drop_ProposalLogsActionTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_logs_actions DROP FOREIGN KEY FK_BD2367AEA86603F3');
        $this->addSql('CREATE TABLE proposal_log_action (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(100) NOT NULL, route VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE proposal_action');
        $this->addSql('DROP TABLE proposal_logs_actions');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proposal_action (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, alias VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, route VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proposal_logs_actions (proposal_log_id INT NOT NULL, proposal_action_id INT NOT NULL, INDEX IDX_BD2367AE4B713931 (proposal_log_id), INDEX IDX_BD2367AEA86603F3 (proposal_action_id), PRIMARY KEY(proposal_log_id, proposal_action_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proposal_logs_actions ADD CONSTRAINT FK_BD2367AE4B713931 FOREIGN KEY (proposal_log_id) REFERENCES proposal_log (id)');
        $this->addSql('ALTER TABLE proposal_logs_actions ADD CONSTRAINT FK_BD2367AEA86603F3 FOREIGN KEY (proposal_action_id) REFERENCES proposal_action (id)');
        $this->addSql('DROP TABLE proposal_log_action');
    }
}
