<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0049_Create_Device_Entity_And_Relate_To_All extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE device (id INT AUTO_INCREMENT NOT NULL, named_id INT DEFAULT NULL, status_id INT DEFAULT NULL, account_id INT DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_92FB68E457EB27E (named_id), INDEX IDX_92FB68E6BF700BD (status_id), INDEX IDX_92FB68E9B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68E457EB27E FOREIGN KEY (named_id) REFERENCES device_named (id)');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68E6BF700BD FOREIGN KEY (status_id) REFERENCES device_status (id)');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68E9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE dynamic_field_value ADD device_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dynamic_field_value ADD CONSTRAINT FK_F0552AD294A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('CREATE INDEX IDX_F0552AD294A4C7D4 ON dynamic_field_value (device_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dynamic_field_value DROP FOREIGN KEY FK_F0552AD294A4C7D4');
        $this->addSql('DROP TABLE device');
        $this->addSql('DROP INDEX IDX_F0552AD294A4C7D4 ON dynamic_field_value');
        $this->addSql('ALTER TABLE dynamic_field_value DROP device_id');
    }
}
