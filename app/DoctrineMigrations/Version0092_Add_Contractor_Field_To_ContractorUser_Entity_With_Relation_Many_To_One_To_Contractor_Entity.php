<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0092_Add_Contractor_Field_To_ContractorUser_Entity_With_Relation_Many_To_One_To_Contractor_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor_user ADD contractor INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contractor_user ADD CONSTRAINT FK_759F1C39437BD2EF FOREIGN KEY (contractor) REFERENCES contractor (id)');
        $this->addSql('CREATE INDEX IDX_759F1C39437BD2EF ON contractor_user (contractor)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contractor_user DROP FOREIGN KEY FK_759F1C39437BD2EF');
        $this->addSql('DROP INDEX IDX_759F1C39437BD2EF ON contractor_user');
        $this->addSql('ALTER TABLE contractor_user DROP contractor');
    }
}
