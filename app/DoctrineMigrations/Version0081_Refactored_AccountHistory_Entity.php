<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0081_Refactored_AccountHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_history DROP FOREIGN KEY FK_EE9164035D83CC1');
        $this->addSql('DROP INDEX IDX_EE9164035D83CC1 ON account_history');
        $this->addSql('ALTER TABLE account_history DROP address, DROP city, DROP zip, CHANGE state_id address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE916403F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EE916403F5B7AF75 ON account_history (address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_history DROP FOREIGN KEY FK_EE916403F5B7AF75');
        $this->addSql('DROP INDEX UNIQ_EE916403F5B7AF75 ON account_history');
        $this->addSql('ALTER TABLE account_history ADD address VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD city VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, ADD zip VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, CHANGE address_id state_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_history ADD CONSTRAINT FK_EE9164035D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('CREATE INDEX IDX_EE9164035D83CC1 ON account_history (state_id)');
    }
}
