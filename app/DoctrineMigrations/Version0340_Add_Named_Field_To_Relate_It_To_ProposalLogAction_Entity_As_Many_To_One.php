<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0340_Add_Named_Field_To_Relate_It_To_ProposalLogAction_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_log_action ADD named_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal_log_action ADD CONSTRAINT FK_2004EC46457EB27E FOREIGN KEY (named_id) REFERENCES proposal_log_action_named (id)');
        $this->addSql('CREATE INDEX IDX_2004EC46457EB27E ON proposal_log_action (named_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_log_action DROP FOREIGN KEY FK_2004EC46457EB27E');
        $this->addSql('DROP INDEX IDX_2004EC46457EB27E ON proposal_log_action');
        $this->addSql('ALTER TABLE proposal_log_action DROP named_id');
    }
}
