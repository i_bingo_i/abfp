<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0315_Add_specialNotification_Field_To_Relate_It_To_Proposal_Entity_And_SpecialNotification_Entity_As_Many_To_Many extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE special_notifications_for_proposals (proposal_id INT NOT NULL, special_notification_id INT NOT NULL, INDEX IDX_C2844A82F4792058 (proposal_id), INDEX IDX_C2844A82A2485031 (special_notification_id), PRIMARY KEY(proposal_id, special_notification_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE special_notifications_for_proposals ADD CONSTRAINT FK_C2844A82F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('ALTER TABLE special_notifications_for_proposals ADD CONSTRAINT FK_C2844A82A2485031 FOREIGN KEY (special_notification_id) REFERENCES special_notification (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE special_notifications_for_proposals');
    }
}
