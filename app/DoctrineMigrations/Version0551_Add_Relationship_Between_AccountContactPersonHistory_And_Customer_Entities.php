<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0551_Add_Relationship_Between_AccountContactPersonHistory_And_Customer_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person_history ADD customer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F589395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_950F5F589395C3F3 ON account_contact_person_history (customer_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F589395C3F3');
        $this->addSql('DROP INDEX IDX_950F5F589395C3F3 ON account_contact_person_history');
        $this->addSql('ALTER TABLE account_contact_person_history DROP customer_id');
    }
}
