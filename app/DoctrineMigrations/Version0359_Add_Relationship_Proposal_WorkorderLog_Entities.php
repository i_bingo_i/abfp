<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0359_Add_Relationship_Proposal_WorkorderLog_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log ADD proposal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder_log ADD CONSTRAINT FK_3711E899F4792058 FOREIGN KEY (proposal_id) REFERENCES proposal (id)');
        $this->addSql('CREATE INDEX IDX_3711E899F4792058 ON workorder_log (proposal_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log DROP FOREIGN KEY FK_3711E899F4792058');
        $this->addSql('DROP INDEX IDX_3711E899F4792058 ON workorder_log');
        $this->addSql('ALTER TABLE workorder_log DROP proposal_id');
    }
}
