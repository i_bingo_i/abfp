<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0125_Add_Relations_For_AgentChannel_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agent_channel ADD named INT DEFAULT NULL, ADD agent INT DEFAULT NULL, ADD device_category INT DEFAULT NULL');
        $this->addSql('ALTER TABLE agent_channel ADD CONSTRAINT FK_70AAECED71E0CC87 FOREIGN KEY (named) REFERENCES channel_named (id)');
        $this->addSql('ALTER TABLE agent_channel ADD CONSTRAINT FK_70AAECED268B9C9D FOREIGN KEY (agent) REFERENCES agent (id)');
        $this->addSql('ALTER TABLE agent_channel ADD CONSTRAINT FK_70AAECED887840E1 FOREIGN KEY (device_category) REFERENCES device_category (id)');
        $this->addSql('CREATE INDEX IDX_70AAECED71E0CC87 ON agent_channel (named)');
        $this->addSql('CREATE INDEX IDX_70AAECED268B9C9D ON agent_channel (agent)');
        $this->addSql('CREATE INDEX IDX_70AAECED887840E1 ON agent_channel (device_category)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agent_channel DROP FOREIGN KEY FK_70AAECED71E0CC87');
        $this->addSql('ALTER TABLE agent_channel DROP FOREIGN KEY FK_70AAECED268B9C9D');
        $this->addSql('ALTER TABLE agent_channel DROP FOREIGN KEY FK_70AAECED887840E1');
        $this->addSql('DROP INDEX IDX_70AAECED71E0CC87 ON agent_channel');
        $this->addSql('DROP INDEX IDX_70AAECED268B9C9D ON agent_channel');
        $this->addSql('DROP INDEX IDX_70AAECED887840E1 ON agent_channel');
        $this->addSql('ALTER TABLE agent_channel DROP named, DROP agent, DROP device_category');
    }
}
