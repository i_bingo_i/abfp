<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0347_Change_History_Relations_To_Actual_Relations_In_ProposalHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_history DROP FOREIGN KEY FK_721E40004F8A983C');
        $this->addSql('ALTER TABLE proposal_history DROP FOREIGN KEY FK_721E400061220EA6');
        $this->addSql('ALTER TABLE proposal_history DROP FOREIGN KEY FK_721E40009B6B5FBA');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E40004F8A983C FOREIGN KEY (contact_person_id) REFERENCES account_contact_person (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E400061220EA6 FOREIGN KEY (creator_id) REFERENCES contractor_user (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E40009B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_history DROP FOREIGN KEY FK_721E40009B6B5FBA');
        $this->addSql('ALTER TABLE proposal_history DROP FOREIGN KEY FK_721E400061220EA6');
        $this->addSql('ALTER TABLE proposal_history DROP FOREIGN KEY FK_721E40004F8A983C');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E40009B6B5FBA FOREIGN KEY (account_id) REFERENCES account_history (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E400061220EA6 FOREIGN KEY (creator_id) REFERENCES contractor_user_history (id)');
        $this->addSql('ALTER TABLE proposal_history ADD CONSTRAINT FK_721E40004F8A983C FOREIGN KEY (contact_person_id) REFERENCES account_contact_person_history (id)');
    }
}
