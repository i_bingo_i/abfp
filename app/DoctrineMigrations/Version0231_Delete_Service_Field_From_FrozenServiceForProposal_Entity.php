<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0231_Delete_Service_Field_From_FrozenServiceForProposal_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE frozen_service_for_proposal DROP FOREIGN KEY FK_C57D5467ED5CA9E6');
        $this->addSql('DROP INDEX IDX_C57D5467ED5CA9E6 ON frozen_service_for_proposal');
        $this->addSql('ALTER TABLE frozen_service_for_proposal DROP service_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE frozen_service_for_proposal ADD service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE frozen_service_for_proposal ADD CONSTRAINT FK_C57D5467ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id)');
        $this->addSql('CREATE INDEX IDX_C57D5467ED5CA9E6 ON frozen_service_for_proposal (service_id)');
    }
}
