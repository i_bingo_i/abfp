<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0027_Create_CompanyHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_history (id INT AUTO_INCREMENT NOT NULL, state_id INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, author INT DEFAULT NULL, name VARCHAR(255) NOT NULL, address_line1 VARCHAR(255) NOT NULL, city VARCHAR(50) NOT NULL, zip VARCHAR(50) NOT NULL, website VARCHAR(100) NOT NULL, notes LONGTEXT DEFAULT NULL, deleted TINYINT(1) DEFAULT \'0\', date_create DATETIME NOT NULL, date_update DATETIME NOT NULL, INDEX IDX_7E86A9C5D83CC1 (state_id), INDEX IDX_7E86A9C835E0EEE (owner_entity_id), INDEX IDX_7E86A9CBDAFD8C8 (author), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_history ADD CONSTRAINT FK_7E86A9C5D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('ALTER TABLE company_history ADD CONSTRAINT FK_7E86A9C835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE company_history ADD CONSTRAINT FK_7E86A9CBDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_history');
    }
}
