<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0237_Add_Status_Property_To_WorkorderLog_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log ADD type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workorder_log ADD CONSTRAINT FK_3711E899C54C8C93 FOREIGN KEY (type_id) REFERENCES workorder_log_type (id)');
        $this->addSql('CREATE INDEX IDX_3711E899C54C8C93 ON workorder_log (type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workorder_log DROP FOREIGN KEY FK_3711E899C54C8C93');
        $this->addSql('DROP INDEX IDX_3711E899C54C8C93 ON workorder_log');
        $this->addSql('ALTER TABLE workorder_log DROP type_id');
    }
}
