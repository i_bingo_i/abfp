<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0138_Add_ForiginKey_Cascade_DepartmentChannelDynamicFieldValue_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel_dynamic_field_value DROP FOREIGN KEY FK_64EFA3FD76E0D2');
        $this->addSql('ALTER TABLE department_channel_dynamic_field_value ADD CONSTRAINT FK_64EFA3FD76E0D2 FOREIGN KEY (department_chanel_id) REFERENCES department_channel (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel_dynamic_field_value DROP FOREIGN KEY FK_64EFA3FD76E0D2');
        $this->addSql('ALTER TABLE department_channel_dynamic_field_value ADD CONSTRAINT FK_64EFA3FD76E0D2 FOREIGN KEY (department_chanel_id) REFERENCES department_channel (id)');
    }
}
