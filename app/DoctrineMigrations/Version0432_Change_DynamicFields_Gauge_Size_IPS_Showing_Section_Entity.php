<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0432_Change_DynamicFields_Gauge_Size_IPS_Showing_Section_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE dynamic_field AS df SET df.is_show = FALSE WHERE df.device_id IN (SELECT id FROM device_named AS dn WHERE dn.alias = \'section\') AND df.alias = \'gauge_size_ips\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE dynamic_field AS df SET df.is_show = TRUE WHERE df.device_id IN (SELECT id FROM device_named AS dn WHERE dn.alias = \'section\') AND df.alias = \'gauge_size_ips\'');
    }
}