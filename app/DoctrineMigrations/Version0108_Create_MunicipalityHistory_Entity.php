<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0108_Create_MunicipalityHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE municipality_history (id INT AUTO_INCREMENT NOT NULL, author INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, date_save DATETIME NOT NULL, INDEX IDX_B20D04DBBDAFD8C8 (author), INDEX IDX_B20D04DB835E0EEE (owner_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE municipality_history ADD CONSTRAINT FK_B20D04DBBDAFD8C8 FOREIGN KEY (author) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE municipality_history ADD CONSTRAINT FK_B20D04DB835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES municipality (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE municipality_history');
    }
}
