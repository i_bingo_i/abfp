<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0149_Add_Filds_And_Relationship_Service_And_Opportunity_Entities extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE service ADD test_date DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE opportunity ADD account INT DEFAULT NULL, CHANGE execution_services_date execution_services_date DATE NOT NULL');
        $this->addSql('ALTER TABLE opportunity ADD CONSTRAINT FK_8389C3D77D3656A4 FOREIGN KEY (account) REFERENCES account (id)');
        $this->addSql('CREATE INDEX IDX_8389C3D77D3656A4 ON opportunity (account)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE opportunity DROP FOREIGN KEY FK_8389C3D77D3656A4');
        $this->addSql('DROP INDEX IDX_8389C3D77D3656A4 ON opportunity');
        $this->addSql('ALTER TABLE opportunity DROP account, CHANGE execution_services_date execution_services_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE service DROP test_date');
    }
}
