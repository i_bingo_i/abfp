<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0061_Add_Parent_Relation_To_DeviceNamed_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device_named ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE device_named ADD CONSTRAINT FK_B7652CE0727ACA70 FOREIGN KEY (parent_id) REFERENCES device_named (id)');
        $this->addSql('CREATE INDEX IDX_B7652CE0727ACA70 ON device_named (parent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device_named DROP FOREIGN KEY FK_B7652CE0727ACA70');
        $this->addSql('DROP INDEX IDX_B7652CE0727ACA70 ON device_named');
        $this->addSql('ALTER TABLE device_named DROP parent_id');
    }
}
