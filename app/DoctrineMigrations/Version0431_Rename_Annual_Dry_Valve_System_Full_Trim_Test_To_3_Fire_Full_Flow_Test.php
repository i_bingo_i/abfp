<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0431_Rename_Annual_Dry_Valve_System_Full_Trim_Test_To_3_Fire_Full_Flow_Test extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE service_named 
                            SET name=\'3 Year Full Flow Test\' 
                            WHERE name=\'Annual Dry Valve System Full Trip Test\'
                            ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE service_named 
                            SET name=\'Annual Dry Valve System Full Trip Test\' 
                            WHERE name=\'3 Year Full Flow Test\'
                            ');
    }
}