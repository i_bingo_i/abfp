<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0060_Added_Parent_And_Children_Relation_To_DeviceHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device_history ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE device_history ADD CONSTRAINT FK_AD0CA9D6727ACA70 FOREIGN KEY (parent_id) REFERENCES device (id)');
        $this->addSql('CREATE INDEX IDX_AD0CA9D6727ACA70 ON device_history (parent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device_history DROP FOREIGN KEY FK_AD0CA9D6727ACA70');
        $this->addSql('DROP INDEX IDX_AD0CA9D6727ACA70 ON device_history');
        $this->addSql('ALTER TABLE device_history DROP parent_id');
    }
}
