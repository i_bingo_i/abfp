<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0177_Add_Fields_Authorizer_Access_AccessPrimary_Payment_PaymentPrimary_For_History extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F5857698A6A');
        $this->addSql('ALTER TABLE account_contact_person_history DROP FOREIGN KEY FK_950F5F58694E8A08');
        $this->addSql('DROP INDEX IDX_950F5F5857698A6A ON account_contact_person_history');
        $this->addSql('DROP INDEX IDX_950F5F58694E8A08 ON account_contact_person_history');
        $this->addSql('ALTER TABLE account_contact_person_history ADD authorizer TINYINT(1) DEFAULT \'0\', ADD access TINYINT(1) DEFAULT \'0\', ADD access_primary TINYINT(1) DEFAULT \'0\', ADD payment TINYINT(1) DEFAULT \'0\', ADD payment_primary TINYINT(1) DEFAULT \'0\', DROP role, DROP responsibility');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_contact_person_history ADD role INT DEFAULT NULL, ADD responsibility INT DEFAULT NULL, DROP authorizer, DROP access, DROP access_primary, DROP payment, DROP payment_primary');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F5857698A6A FOREIGN KEY (role) REFERENCES contact_person_role (id)');
        $this->addSql('ALTER TABLE account_contact_person_history ADD CONSTRAINT FK_950F5F58694E8A08 FOREIGN KEY (responsibility) REFERENCES contact_person_responsibility (id)');
        $this->addSql('CREATE INDEX IDX_950F5F5857698A6A ON account_contact_person_history (role)');
        $this->addSql('CREATE INDEX IDX_950F5F58694E8A08 ON account_contact_person_history (responsibility)');
    }
}
