<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0104_Relate_LicensesDynamicFieldValue_To_LicensesDropboxChoices extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE licenses_dynamic_field_value ADD option_value_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE licenses_dynamic_field_value ADD CONSTRAINT FK_9D146CEED957CA06 FOREIGN KEY (option_value_id) REFERENCES licenses_dropbox_choices (id)');
        $this->addSql('CREATE INDEX IDX_9D146CEED957CA06 ON licenses_dynamic_field_value (option_value_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE licenses_dynamic_field_value DROP FOREIGN KEY FK_9D146CEED957CA06');
        $this->addSql('DROP INDEX IDX_9D146CEED957CA06 ON licenses_dynamic_field_value');
        $this->addSql('ALTER TABLE licenses_dynamic_field_value DROP option_value_id');
    }
}
