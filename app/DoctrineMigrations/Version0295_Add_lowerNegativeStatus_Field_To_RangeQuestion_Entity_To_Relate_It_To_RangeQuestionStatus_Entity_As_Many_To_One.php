<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0295_Add_lowerNegativeStatus_Field_To_RangeQuestion_Entity_To_Relate_It_To_RangeQuestionStatus_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE range_question ADD lower_negative_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE range_question ADD CONSTRAINT FK_F8A3E6EDDAA3558D FOREIGN KEY (lower_negative_status_id) REFERENCES range_question_status (id)');
        $this->addSql('CREATE INDEX IDX_F8A3E6EDDAA3558D ON range_question (lower_negative_status_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE range_question DROP FOREIGN KEY FK_F8A3E6EDDAA3558D');
        $this->addSql('DROP INDEX IDX_F8A3E6EDDAA3558D ON range_question');
        $this->addSql('ALTER TABLE range_question DROP lower_negative_status_id');
    }
}
