<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0136_Add_Owner_Field_To_DepartmentChannel_Entity_Related_To_AgentChannel_Entity_As_Many_To_One extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE department_channel ADD CONSTRAINT FK_1F5066A47E3C61F9 FOREIGN KEY (owner_id) REFERENCES agent_channel (id)');
        $this->addSql('CREATE INDEX IDX_1F5066A47E3C61F9 ON department_channel (owner_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE department_channel DROP FOREIGN KEY FK_1F5066A47E3C61F9');
        $this->addSql('DROP INDEX IDX_1F5066A47E3C61F9 ON department_channel');
        $this->addSql('ALTER TABLE department_channel DROP owner_id');
    }
}
