<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version0562_Create_CustomerHistory_Entity extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customer_history (id INT AUTO_INCREMENT NOT NULL, entity_type_id INT DEFAULT NULL, address_id INT DEFAULT NULL, owner_entity_id INT DEFAULT NULL, accounting_system_id VARCHAR(255) DEFAULT NULL, entity_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, edit_sequence VARCHAR(255) DEFAULT NULL, modification_date DATETIME NOT NULL, INDEX IDX_3B67D4F55681BEB0 (entity_type_id), INDEX IDX_3B67D4F5F5B7AF75 (address_id), INDEX IDX_3B67D4F5835E0EEE (owner_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer_history ADD CONSTRAINT FK_3B67D4F55681BEB0 FOREIGN KEY (entity_type_id) REFERENCES entity_type (id)');
        $this->addSql('ALTER TABLE customer_history ADD CONSTRAINT FK_3B67D4F5F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE customer_history ADD CONSTRAINT FK_3B67D4F5835E0EEE FOREIGN KEY (owner_entity_id) REFERENCES customer (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE customer_history');
    }
}
